package com.adssdk;

import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Amit on 9/27/2018.
 */

public class NativeUnifiedAdsViewHolder extends RecyclerView.ViewHolder {

    RelativeLayout parent;
    public UnifiedNativeAdView unifiedNativeAdView;

    public NativeUnifiedAdsViewHolder(View itemView) {
        super(itemView);
        parent = (RelativeLayout) itemView;
        unifiedNativeAdView = (UnifiedNativeAdView) itemView.findViewWithTag("unifiedNativeAdView");
        if (unifiedNativeAdView != null) {
            if (itemView.findViewWithTag("ad_media") != null ) {
                MediaView mediaView = itemView.findViewWithTag("ad_media");
                unifiedNativeAdView.setMediaView(mediaView);
            }else if (itemView.findViewWithTag("ad_media") != null) {
                unifiedNativeAdView.setMediaView( (MediaView) itemView.findViewWithTag("ad_media"));
            }
            if (itemView.findViewWithTag("ad_headline") != null ) {
                unifiedNativeAdView.setHeadlineView(itemView.findViewWithTag("ad_headline"));
            }
            if (itemView.findViewWithTag("ad_call_to_action") != null ) {
                unifiedNativeAdView.setCallToActionView(itemView.findViewWithTag("ad_call_to_action"));
            }
        }
    }
}
