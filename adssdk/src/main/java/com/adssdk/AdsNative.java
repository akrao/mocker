package com.adssdk;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.adssdk.util.UnifiedNativeAdCache;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import static com.adssdk.AdsSDK.getAdRequest;

/**
 * Created by Amit on 3/14/2018.
 */

public class AdsNative {

    private Context context;
    private String adId;
    private boolean isInstallAd, isContentAd, isVideoAd;

    public AdsNative(Context context, String adId, boolean isInstallAd, boolean isContentAd, boolean isVideoAd) {
        this.context = context;
        this.adId = adId;
        this.isInstallAd = isInstallAd;
        this.isContentAd = isContentAd;
        this.isVideoAd = isVideoAd;
        unifiedNativeAdCache = new UnifiedNativeAdCache();
    }

    public boolean isNativeAdCache() {
        return isNativeAdCache;
    }

    public void bindUnifiedNativeAd(NativeUnifiedAdsViewHolder adsViewHolder, boolean isSmallNative) {
        if (AdsSDK.getInstance().isIS_ADS_ENABLED()) {
            if (adsViewHolder.unifiedNativeAdView != null) {
//                if (isVideoAdSupport) {
                if (unifiedNativeAdCache.getCacheVideoUNA() != null
                        && unifiedNativeAdCache.isCachedVideoUNAValid()) {
                    populateUnifiedNativeAdView(unifiedNativeAdCache.getCacheVideoUNA(), adsViewHolder.unifiedNativeAdView, isSmallNative);
                    adsViewHolder.unifiedNativeAdView.setVisibility(View.VISIBLE);
                    adsViewHolder.parent.setVisibility(View.VISIBLE);
                } else if (unifiedNativeAdCache.getCachedUNA() != null
                        && unifiedNativeAdCache.isCachedUNAValid()) {
                    populateUnifiedNativeAdView(unifiedNativeAdCache.getCachedUNA(), adsViewHolder.unifiedNativeAdView, isSmallNative);
                    adsViewHolder.unifiedNativeAdView.setVisibility(View.VISIBLE);
                    adsViewHolder.parent.setVisibility(View.VISIBLE);
                } else {
                    //                    cacheNativeAd(true);
                    adsViewHolder.parent.setVisibility(View.GONE);
                }
/*
                } else {
                    if (unifiedNativeAdCache.getCachedUNA() != null
                            && unifiedNativeAdCache.isCachedUNAValid()) {
                        populateUnifiedNativeAdView(unifiedNativeAdCache.getCachedUNA(), adsViewHolder.unifiedNativeAdView);
                        adsViewHolder.unifiedNativeAdView.setVisibility(View.VISIBLE);
                        adsViewHolder.parent.setVisibility(View.VISIBLE);
                    } else {
    //                    cacheNativeAd(false);
                        adsViewHolder.parent.setVisibility(View.GONE);
                    }
                }
*/
            } else {
                adsViewHolder.parent.setVisibility(View.GONE);
            }
        } else {
            adsViewHolder.parent.setVisibility(View.GONE);
        }
    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd,
                                             UnifiedNativeAdView adView, boolean isSmallNative) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        if (adView != null && nativeAd != null) {
            if (adView.getHeadlineView() != null && adView.getHeadlineView() instanceof TextView) {
                ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
            }
            if (adView.getBodyView() != null && adView.getBodyView() instanceof TextView) {
                ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
            }
            if (adView.getCallToActionView() != null && adView.getCallToActionView() instanceof Button) {
                ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
            }

            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
            // check before trying to display them.
            NativeAd.Image icon = nativeAd.getIcon();

            if (adView.getIconView() != null && adView.getIconView() instanceof ImageView) {
                if (icon == null) {
                    adView.getIconView().setVisibility(View.GONE);
                } else {
                    ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
                    adView.getIconView().setVisibility(View.VISIBLE);
                }
            }
            if (isSmallNative) {
                if ((nativeAd.getVideoController() != null && nativeAd.getVideoController().hasVideoContent())
                        || (adView.getIconView() == null || adView.getIconView().getVisibility() == View.GONE)) {
                    if (adView.getMediaView() != null) {
                        adView.getMediaView().setVisibility(View.VISIBLE);
                    }
                    if (adView.getIconView() != null) {
                        adView.getIconView().setVisibility(View.GONE);
                    }
                } else {
                    if (adView.getMediaView() != null) {
                        adView.getMediaView().setVisibility(View.GONE);
                    }
                    if (adView.getIconView() != null) {
                        adView.getIconView().setVisibility(View.VISIBLE);
                    }
                }
            }

            if (adView.getPriceView() != null && adView.getPriceView() instanceof TextView) {
                if (nativeAd.getPrice() == null) {
                    adView.getPriceView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getPriceView().setVisibility(View.VISIBLE);
                    ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
                }
            }

            if (adView.getStoreView() != null && adView.getStoreView() instanceof TextView) {
                if (nativeAd.getStore() == null) {
                    adView.getStoreView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getStoreView().setVisibility(View.VISIBLE);
                    ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
                }
            }
            if (adView.getStarRatingView() != null && adView.getStarRatingView() instanceof RatingBar) {
                if (nativeAd.getStarRating() == null) {
                    adView.getStarRatingView().setVisibility(View.INVISIBLE);
                } else {
                    ((RatingBar) adView.getStarRatingView())
                            .setRating(nativeAd.getStarRating().floatValue());
                    adView.getStarRatingView().setVisibility(View.VISIBLE);
                }
            }

            if (adView.getAdvertiserView() != null && adView.getAdvertiserView() instanceof TextView) {
                if (nativeAd.getAdvertiser() == null) {
                    adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                } else {
                    ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
                    adView.getAdvertiserView().setVisibility(View.VISIBLE);
                }
            }

            // Assign native ad object to the native view.
            adView.setNativeAd(nativeAd);
        }
    }

    private UnifiedNativeAdCache unifiedNativeAdCache;
    private AdLoader adLoader;
    private boolean isNativeAdCache = false;

    public void cacheNativeAd(boolean isVideoAd) {
        if (adId != null && AdsSDK.getInstance().isIS_ADS_ENABLED()) {
            AdLoader.Builder builder = new AdLoader.Builder(context, adId);
            builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    if (unifiedNativeAd.getVideoController().hasVideoContent()) {
                        if (unifiedNativeAdCache.getCacheVideoUNA() != null ) {
//                            unifiedNativeAdCache.getCacheVideoUNA().destroy();
                        }
                        unifiedNativeAdCache.setCacheVideoUNA(unifiedNativeAd);
                        unifiedNativeAdCache.setCacheVideoUNATime(System.currentTimeMillis());
                    } else {
                        if (unifiedNativeAdCache.getCachedUNA() != null) {
//                            unifiedNativeAdCache.getCachedUNA().destroy();
                        }
                        unifiedNativeAdCache.setCachedUNA(unifiedNativeAd);
                        unifiedNativeAdCache.setCachedUNATime(System.currentTimeMillis());
                    }
                    isNativeAdCache = true;
                    AdsSDK.getInstance().refreshAdsCallBack();
//                    cachedUnifiedNativeAd = unifiedNativeAd ;
                }
            });
            if (isVideoAd) {
                VideoOptions videoOptions = new VideoOptions.Builder()
                        .setStartMuted(AdsSDK.getInstance().isStartVideoAdsMuted())
                        .build();

                NativeAdOptions adOptions = new NativeAdOptions.Builder()
                        .setVideoOptions(videoOptions)
                        .build();

                builder.withNativeAdOptions(adOptions);
            }
            initAd(builder);
        }
    }

    public void bindAndCacheNativeAd(final NativeUnifiedAdsViewHolder nativeUnifiedAdsViewHolder, final boolean isSmallNative) {
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null && nativeUnifiedAdsViewHolder != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AdsSDK.getInstance().getAdsNative().bindUnifiedNativeAd(nativeUnifiedAdsViewHolder, isSmallNative);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
                        }
                    }, 100);
                }
            }, 10);
        }
    }

    private void initAd(final AdLoader.Builder builder) {
        adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
//                initAd(builder);
                Log.i("Nativeload failed", "" + i);
            }
        })
                .build();
        adLoader.loadAd(getAdRequest());
    }
}
