package com.adssdk;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

/**
 * Created by Amit on 4/3/2018.
 */

public abstract class NativePagerAdapter extends PagerAdapter {

    private List list;
    private int layoutInstall , layoutContent ;
    private RelativeLayout relativeLayout ;
    private Context context  ;

    private NativePagerAdapter(List list, int layoutInstall, int layoutContent, Context context) {
        this.list = list;
        this.layoutInstall = layoutInstall;
        this.layoutContent = layoutContent;
        relativeLayout = new RelativeLayout( context );
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null  ) {
            AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
//            AdsSDK.getInstance().getAdsNative().requestNativeAds(layoutInstall, layoutContent, relativeLayout);
            initAdsView(context);
        }
    }

    /**
     *
     * @param list
     * @param layoutInstall
     * @param context
     */
    public NativePagerAdapter(List list, int layoutInstall, Context context) {
        this.list = list;
        this.layoutInstall = layoutInstall;
        this.context = context;
        relativeLayout = new RelativeLayout( context );
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null  ) {
            AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
//            AdsSDK.getInstance().getAdsNative().requestNativeAds(layoutInstall, layoutContent, relativeLayout);
            initAdsView(context);
        }
    }

    NativeUnifiedAdsFullViewHolder adsViewHolder ;
    public void initAdsView( Context context ){
        RelativeLayout relativeLayout = new RelativeLayout(context);
        View view = LayoutInflater.from(context).inflate( layoutInstall, null, false);
        relativeLayout.removeAllViews();
        relativeLayout.addView( view );
        adsViewHolder = new NativeUnifiedAdsFullViewHolder(relativeLayout);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final BaseAdModelClass bean = (BaseAdModelClass) list.get(position);
        View view = null ;
        if (bean != null && bean.getModelId() != AdsSDK.NATIVE_ADS_MODEL_ID) {
            view = customInstantiateItem(container,position);
        }else {
            try {
                if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null  ) {
                    AdsSDK.getInstance().getAdsNative().bindUnifiedNativeAd(adsViewHolder,false);
                    view = adsViewHolder.parent ;
                    AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
    /*
                    AdsSDK.getInstance().getAdsNative().loadNativeAds( relativeLayout , layoutInstall , layoutContent  );
                    view = relativeLayout ;
                    AdsSDK.getInstance().getAdsNative().requestNativeAds(layoutInstall , layoutContent , relativeLayout );
    */
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if ( view != null ) {
            container.addView(view);
        }
        return view;
    }

    protected abstract View customInstantiateItem(ViewGroup container, int position);

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    protected void loadFullAd(final RelativeLayout relativeLayout ){
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null  ) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if ( relativeLayout != null ) {
                        AdsSDK.getInstance().getAdsNative().bindAndCacheNativeAd(getNative(relativeLayout),false);
                    }
                }
            }, 100);
        }
    }

    private NativeUnifiedAdsViewHolder getNative(RelativeLayout rlNativeAd){
        View view = LayoutInflater.from(context).inflate(R.layout.native_pager_ad_app_install, null, false);
        rlNativeAd.removeAllViews();
        rlNativeAd.addView(view);
        return new NativeUnifiedAdsFullViewHolder(rlNativeAd);
    }

}
