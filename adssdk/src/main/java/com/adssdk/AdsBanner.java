package com.adssdk;

import android.content.Context;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import static com.adssdk.AdsSDK.getAdRequest;

/**
 * Created by Amit on 3/9/2018.
 */

public class AdsBanner {

    private int failCount = 0 ;
    public static final int FAILED_MAX_COUNT = 3 ;
    void initAds(final RelativeLayout view, String id, AdSize adSize , Context context) {
        if ( !AdsId.isEmptyOrNull(id) && context != null && view != null ) {
            failCount = 0 ;
            final AdView adView = new AdView(context);
            adView.setAdUnitId(id);
            adView.setAdSize(adSize);
            adView.loadAd(getAdRequest());
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    if ( failCount < FAILED_MAX_COUNT && AdsSDK.getInstance().isConnected() ) {
                        failCount++;
                        adView.loadAd(getAdRequest());
                    }
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    failCount = 0 ;
                    if ( view != null ) {
                        view.removeAllViews();
                        view.addView(adView);
                    }
                }
            });
            if ( view != null ) {
                view.removeAllViews();
                view.addView(adView);
            }
        }
    }

}
