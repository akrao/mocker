package com.adssdk;

import android.view.View;

public class NativeUnifiedAdsFullViewHolder extends NativeUnifiedAdsViewHolder {
    public NativeUnifiedAdsFullViewHolder(View itemView) {
        super(itemView);
        if (itemView.findViewWithTag("ad_stars") != null ) {
            unifiedNativeAdView.setStarRatingView(itemView.findViewWithTag("ad_stars"));
        }
        if (itemView.findViewWithTag("ad_body") != null) {
            unifiedNativeAdView.setBodyView(itemView.findViewWithTag("ad_body"));
        }
        if (itemView.findViewWithTag("ad_icon") != null) {
            unifiedNativeAdView.setIconView(itemView.findViewWithTag("ad_icon"));
        }
        if (itemView.findViewWithTag("ad_price") != null) {
            unifiedNativeAdView.setPriceView(itemView.findViewWithTag("ad_price"));
        }
        if (itemView.findViewWithTag("ad_store") != null) {
            unifiedNativeAdView.setStoreView(itemView.findViewWithTag("ad_store"));
        }
        if (itemView.findViewWithTag("ad_advertiser") != null) {
            unifiedNativeAdView.setAdvertiserView(itemView.findViewWithTag("ad_advertiser"));
        }
    }
}
