package com.adssdk.util;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public class UnifiedNativeAdCache {

    private UnifiedNativeAd cachedUNA , cacheVideoUNA ;
    private long cachedUNATime = 0 , cacheVideoUNATime = 0 ;

    public boolean isCachedVideoUNAValid(){
        return isExpired( cacheVideoUNATime ) ;
    }
    public boolean isCachedUNAValid(){
        return isExpired(cachedUNATime);
    }

    private boolean isExpired(long preLoadTime){
        long seconds = (System.currentTimeMillis() - preLoadTime )/(1000) ;
        return ( seconds < (59*60) );
    }

    public UnifiedNativeAd getCachedUNA() {
        return cachedUNA;
    }

    public void setCachedUNA(UnifiedNativeAd cachedUNA) {
        this.cachedUNA = cachedUNA;
    }

    public UnifiedNativeAd getCacheVideoUNA() {
        return cacheVideoUNA;
    }

    public void setCacheVideoUNA(UnifiedNativeAd cacheVideoUNA) {
        this.cacheVideoUNA = cacheVideoUNA;
    }

    public long getCachedUNATime() {
        return cachedUNATime;
    }

    public void setCachedUNATime(long cachedUNATime) {
        this.cachedUNATime = cachedUNATime;
    }

    public long getCacheVideoUNATime() {
        return cacheVideoUNATime;
    }

    public void setCacheVideoUNATime(long cacheVideoUNATime) {
        this.cacheVideoUNATime = cacheVideoUNATime;
    }
}
