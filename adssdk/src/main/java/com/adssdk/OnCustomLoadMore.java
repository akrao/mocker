package com.adssdk;

/**
 * Created by Amit on 4/21/2018.
 */

public interface OnCustomLoadMore {
    void onLoadMore();
}
