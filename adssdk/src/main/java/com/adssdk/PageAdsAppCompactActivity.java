package com.adssdk;

import android.os.Bundle;
import androidx.annotation.Nullable;

/**
 * Created by Amit on 3/12/2018.
 */

public class PageAdsAppCompactActivity extends AdsAppCompactActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( AdsSDK.getInstance() != null ) {
            AdsSDK.getInstance().setPageCount( AdsSDK.getInstance().getPageCount() + 1 );
        }
    }

    @Override
    public void onBackPressed() {
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getPAGE_COUNT_SHOW_ADS() <= AdsSDK.getInstance().getPageCount() ) {
            super.onBackPressed();
            AdsSDK.getInstance().setPageCount(0);
        }else {
            finish();
        }
    }
}
