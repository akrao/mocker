package com.adssdk;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.adssdk.AdsSDK.NATIVE_ADS_MODEL_ID;

/**
 * Created by Amit on 4/14/2018.
 */

abstract public class SingleNativeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List list;

    private OnCustomLoadMore onLoadMore ;

    /**
     * @param list
     * @param layoutInstall
     * @param layoutContent
     */
    public SingleNativeAdapter(List list, int layoutInstall, int layoutContent , OnCustomLoadMore onLoadMore ) {
        this.list = list;
        this.onLoadMore = onLoadMore ;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_NORMAL) {
            return onAbstractCreateViewHolder(parent, viewType);
        } else {
            return new AdsViewHolder(new RelativeLayout(parent.getContext()));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AdsViewHolder) {
        } else {
            onAbstractBindViewHolder(holder, position);
        }
        if (onLoadMore != null && position == list.size() - 1)
            onLoadMore.onLoadMore();
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    abstract protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType);

    /**
     * @param holder
     * @param position
     */
    abstract protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position);


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static final int ITEM_NORMAL = 0;
    public static final int ITEM_NATIVE_AD = 1;

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
//        return ((BaseAdModelClass) list.get(position)).getModelId() == NATIVE_ADS_MODEL_ID ? ITEM_NATIVE_AD : ITEM_NORMAL;
        if ( list != null && list.size() > position && list.get(position) instanceof BaseAdModelClass ) {
            return ((BaseAdModelClass) list.get(position)).getModelId() == NATIVE_ADS_MODEL_ID ? ITEM_NATIVE_AD : ITEM_NORMAL;
        } else {
            return ITEM_NORMAL ;
        }
    }

    class AdsViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;

        public AdsViewHolder(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout) itemView;
            if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
                AdsSDK.getInstance().getAdsNative().bindAndCacheNativeAd(null,false);
            }
        }
    }

}