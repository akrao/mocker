package com.adssdk;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.adssdk.AdsSDK.NATIVE_ADS_MODEL_ID;

/**
 * Created by Amit on 3/14/2018.
 */

abstract public class NativeAdsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List list;
    private int layoutInstall, layoutContent;

    private OnCustomLoadMore onLoadMore ;

    /**
     * @param list
     * @param layoutInstall
     * @param layoutContent
     * @param context
     */
    private NativeAdsAdapter(List list, int layoutInstall, int layoutContent, Context context, OnCustomLoadMore onLoadMore ) {
        this.list = list;
        this.layoutInstall = layoutInstall;
        this.layoutContent = layoutContent;
        this.onLoadMore = onLoadMore ;
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
            AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
        }

    }

    /**
     *
     * @param list
     * @param layoutInstall
     * @param onLoadMore
     */
    public NativeAdsAdapter(List list, int layoutInstall, OnCustomLoadMore onLoadMore ) {
        this.list = list;
        this.layoutInstall = layoutInstall;
        this.onLoadMore = onLoadMore ;
/*
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
            AdsSDK.getInstance().getAdsNative().cacheNativeAd();
        }
*/

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_NORMAL) {
            return onAbstractCreateViewHolder(parent, viewType);
        } else {
            RelativeLayout relativeLayout = new RelativeLayout(parent.getContext());
            View view = LayoutInflater.from(parent.getContext()).inflate( layoutInstall, parent, false);
            relativeLayout.removeAllViews();
            relativeLayout.addView( view );
            return new NativeUnifiedAdsFullViewHolder(relativeLayout);
        }
    }

    Handler handler = new Handler();
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NativeUnifiedAdsFullViewHolder) {
            final NativeUnifiedAdsFullViewHolder adsViewHolder = (NativeUnifiedAdsFullViewHolder) holder;
            if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                AdsSDK.getInstance().getAdsNative().bindUnifiedNativeAd(adsViewHolder , true);
                    }
                }, 100);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
                    }
                }, 100);
            }
        } else{
            onAbstractBindViewHolder(holder, position);
        }
        if (onLoadMore != null && position == list.size() - 1)
            onLoadMore.onLoadMore();
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    abstract protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType);

    /**
     * @param holder
     * @param position
     */
    abstract protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position);


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static final int ITEM_NORMAL = 0;
    public static final int ITEM_NATIVE_AD = 1;

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        if ( list != null && list.size() > position && list.get(position) instanceof BaseAdModelClass ) {
            BaseAdModelClass baseAdModelClass = (BaseAdModelClass) list.get(position) ;
            if (baseAdModelClass != null) {
                return baseAdModelClass.getModelId() == NATIVE_ADS_MODEL_ID ? ITEM_NATIVE_AD : ITEM_NORMAL;
            } else {
                return ITEM_NORMAL ;
            }
        } else {
            return ITEM_NORMAL ;
        }
    }
}
