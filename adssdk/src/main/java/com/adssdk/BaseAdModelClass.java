package com.adssdk;

/**
 * Created by Amit on 3/14/2018.
 */

public class BaseAdModelClass {

    private int modelId ;

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }
}
