package com.adssdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import androidx.annotation.NonNull;

/**
 * Created by Amit on 3/9/2018.
 */

public class AdsId {

    private String bannerAdId = "banner_id"  , interstitialId = "interstitial_id" , nativeId = "naitve_id"
            , FIRST_LOAD = "first_load" , rewardedVideoId = "reward_id" , appId = "app_id" ;
    private Context context ;
    private SharedPreferences preferences ;

    String TEST_ID_APP              = "ca-app-pub-3940256099942544~3347511713";
    String TEST_ID_BANNER           = "ca-app-pub-3940256099942544/6300978111";
    String TEST_ID_INTERSTITIAL     = "ca-app-pub-3940256099942544/1033173712";
    String TEST_ID_REWARDED_VIDEO   = "ca-app-pub-3940256099942544/5224354917";
    String TEST_ID_NATIVE_ADVANCED  = "ca-app-pub-3940256099942544/2247696110";
    String TEST_ID_NATIVE_UNIFIED_VIDEO  = "ca-app-pub-3940256099942544/1044960115";


    interface OnAdsLoadListener {
        void OnAdsLoad();
    }

    private OnAdsLoadListener onAdsLoadListener;
    private boolean isAdsPreLoad ;

    private final FirebaseRemoteConfig remoteConfig ;
    private String packageName ;
    public AdsId(Context context , OnAdsLoadListener onAdsLoadListener  , String packageName) {
        this.context = context;
        this.packageName = packageName ;
        this.onAdsLoadListener = onAdsLoadListener;
        preferences = getDefaultSharedPref();
        isAdsPreLoad = preferences.getBoolean( FIRST_LOAD , false );
        remoteConfig = FirebaseRemoteConfig.getInstance();
    }

    public boolean isAdsPreLoad() {
        return preferences.getBoolean( FIRST_LOAD , false );
    }

    public void startLoadingId(){
        if ( isAdsPreLoad )
            onAdsLoadListener.OnAdsLoad();
        initAdsId();
    }

    public String getTEST_ID_APP() {
        return TEST_ID_APP;
    }

    public String getTEST_ID_BANNER() {
        return TEST_ID_BANNER;
    }

    public String getTEST_ID_INTERSTITIAL() {
        return TEST_ID_INTERSTITIAL;
    }

    public String getTEST_ID_REWARDED_VIDEO() {
        return TEST_ID_REWARDED_VIDEO;
    }

    public String getTEST_ID_NATIVE_ADVANCED() {
        return TEST_ID_NATIVE_UNIFIED_VIDEO;
//        return TEST_ID_NATIVE_ADVANCED;
    }
    long cacheExpiration = 3600; // 1 hour in seconds.

    // Sync Ads From FireBase Remote Config
    private void initAdsId(){
        if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 100L;
        }
        remoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if ( task.isSuccessful() ){
                    remoteConfig.activateFetched();
/*
                    Set<String> keys = remoteConfig.getKeysByPrefix("gk");
                    Log.e( "Amit" , "remoteConfig : " + Arrays.toString(keys.toArray()) );
*/
                    handleRemoteConfig(remoteConfig);
                    if ( !isAdsPreLoad )
                        onAdsLoadListener.OnAdsLoad();
                }else {
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static boolean isEmptyOrNull(String s) {
        return (s == null || TextUtils.isEmpty(s));
    }

    private void handleRemoteConfig(FirebaseRemoteConfig remoteConfig){
        if ( !isEmptyOrNull( getString( remoteConfig , appId ) ) ) {
            setAppId( getString( remoteConfig , appId ) );
        }
        if ( !isEmptyOrNull( getString( remoteConfig , rewardedVideoId ) ) ) {
            setRewardedVideoId( getString( remoteConfig , rewardedVideoId ) );
        }
        if ( !isEmptyOrNull( getString( remoteConfig , bannerAdId ) ) ) {
            setBannerAdId( getString( remoteConfig , bannerAdId ) );
        }
        if ( !isEmptyOrNull( getString( remoteConfig , interstitialId ) ) ) {
            setInterstitialId( getString( remoteConfig , interstitialId ) );
        }
        if ( !isEmptyOrNull( getString( remoteConfig , nativeId ) ) ) {
            setNativeId( getString( remoteConfig , nativeId ) );
        }
        preferences.edit().putBoolean( FIRST_LOAD , true ).commit();
    }

    private String getString(FirebaseRemoteConfig remoteConfig , String key){
        String p = packageName.replaceAll("\\." , "_") ;
            return remoteConfig.getString( p + "_" + key );
    }

    public SharedPreferences getDefaultSharedPref() {
        return context.getSharedPreferences(context.getPackageName() + ".ads.sdk" , Context.MODE_PRIVATE);
    }


    public String getAppId() {
        return preferences.getString( this.appId , null );
    }

    public void setAppId(String appId) {
        preferences.edit().putString( this.appId , appId ).commit() ;
    }
    public String getBannerAdId() {
//     return TEST_ID_BANNER_1;
        return preferences.getString( this.bannerAdId , null );
    }

    public void setBannerAdId(String bannerAdId) {
        preferences.edit().putString( this.bannerAdId , bannerAdId ).commit() ;
    }

    public String getInterstitialId() {
//        return TEST_ID_INTERSTITIAL_1;
        return preferences.getString( this.interstitialId , null );
    }

    public void setInterstitialId(String interstitialId) {
        preferences.edit().putString( this.interstitialId , interstitialId ).commit() ;
    }

    public String getNativeId() {
        return preferences.getString( this.nativeId , null );
    }

    public void setNativeId(String nativeId) {
        preferences.edit().putString( this.nativeId  , nativeId  ).commit() ;
    }

    public String getRewardedVideoId() {
        return preferences.getString( this.rewardedVideoId , null );
    }

    public void setRewardedVideoId(String rewardedVideoId) {
        preferences.edit().putString( this.rewardedVideoId , rewardedVideoId  ).commit() ;
    }
}
