package com.adssdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.RelativeLayout;

import com.adssdk.util.AdsCallBack;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;

import java.util.HashMap;

/**
 * Created by Amit on 3/9/2018.
 */

public class AdsSDK implements AdsId.OnAdsLoadListener {

    private Context context;
    private boolean IS_ADS_ENABLED = true;
    private AdsId adId;
    private boolean isTestAds = false;
    private static AdsSDK adsSDK ;
    private AdsInterstitial adsInterstitial ;
    private int pageCount = 0 ;
    private int PAGE_COUNT_SHOW_ADS = 8 ;

    public boolean isIS_ADS_ENABLED() {
        return IS_ADS_ENABLED;
    }

    public int getPAGE_COUNT_SHOW_ADS() {
        return PAGE_COUNT_SHOW_ADS;
    }

    public void setPAGE_COUNT_SHOW_ADS(int PAGE_COUNT_SHOW_ADS) {
        this.PAGE_COUNT_SHOW_ADS = PAGE_COUNT_SHOW_ADS;
    }

    public static final int NATIVE_ADS_MODEL_ID = 1 ;
    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public AdsInterstitial getAdsInterstitial() {
        return adsInterstitial;
    }

    public static AdsSDK getInstance() {
        return adsSDK;
    }

    public void setIS_ADS_ENABLED(boolean IS_ADS_ENABLED) {
        this.IS_ADS_ENABLED = IS_ADS_ENABLED;
    }

    public void setTestAds(boolean testAds) {
        this.isTestAds = testAds;
    }

    public void setBannerAdsOnView(RelativeLayout view) {
        handleNetwork();
        if (IS_ADS_ENABLED) {
            adsBanner.initAds( view, isTestAds ? adId.getTEST_ID_BANNER() : adId.getBannerAdId(), AdSize.BANNER , context );
        }
    }

    public void setSmartBannerAdsOnView(RelativeLayout view) {
        handleNetwork();
        if (IS_ADS_ENABLED) {
            adsBanner.initAds( view, isTestAds ? adId.getTEST_ID_BANNER() : adId.getBannerAdId(), AdSize.SMART_BANNER , context );
        }
    }

    public  boolean isStartVideoAdsMuted(){
        return getDefaultSharedPref(context).getBoolean( VIDEO_ADS_MUTE_SETTING , true )  ;
    }

    static AdRequest getAdRequest(){
        AdRequest.Builder builder = new AdRequest.Builder();
        builder.addTestDevice("5E254AC1CF02E640413645E46C8A1A64");
        builder.addTestDevice("2C374DC7F25FC3474B235578C446D36F");
        builder.addTestDevice("A3B72905D6C2F896FBDB8A01F186A77D");
//        builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        return builder.build();
    }

    public void showVideoAdsMutedSettings(final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Video Ads Audio Setting");
/*
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
*/
        final int choice = getDefaultSharedPref(activity).getBoolean( VIDEO_ADS_MUTE_SETTING , true ) ? 0 : 1 ;
        builder.setSingleChoiceItems( new String[]{"Play with No Sound","Play with Sound"} , choice , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if ( i != choice ){
                    getDefaultSharedPref(activity).edit().putBoolean(VIDEO_ADS_MUTE_SETTING , i == 0 ).commit();
                }
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public static final String VIDEO_ADS_MUTE_SETTING = "video_ads_mute_setting" ;

    public SharedPreferences getDefaultSharedPref(Context activity) {
        return PreferenceManager.getDefaultSharedPreferences(activity);
    }

    boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    static boolean isEmptyOrNull(String s) {
        return (s == null || TextUtils.isEmpty(s));
    }


    private AdsBanner adsBanner ;
    private String packageName ;
    public AdsSDK(Context context , String packageName) {
        init(context , packageName);
    }

    private AdsNative adsNative ;

    public AdsNative getAdsNative() {
        return adsNative;
    }

    public void setAdsNative(AdsNative adsNative) {
        this.adsNative = adsNative;
    }
    public AdsSDK(Context context, boolean isTestAds  , String packageName) {
        this.isTestAds = isTestAds ;
        init(context, packageName);
    }

    private HashMap<String , AdsCallBack > adsCallBackHashMap = new HashMap<>();
    public void setAdsCallBack(String className , AdsCallBack adsCallBack) {
        adsCallBackHashMap.put( className , adsCallBack );
    }

    public void removeAdsCallBack( String className ) {
        adsCallBackHashMap.remove( className );
    }

    public void refreshAdsCallBack() {
        if ( adsCallBackHashMap != null && adsCallBackHashMap.size() > 0 ) {
            String[] keys = adsCallBackHashMap.keySet().toArray(new String[adsCallBackHashMap.keySet().size()]);
            for ( String key : keys ) {
                if ( adsCallBackHashMap.get(key) != null )
                    adsCallBackHashMap.get(key).onNativeAdsCache();
            }

        }
    }

    private boolean isVideoNativeAdMuteStart = true ;

    public boolean isVideoNativeAdMuteStart() {
        return isVideoNativeAdMuteStart;
    }

    public void setVideoNativeAdMuteStart(boolean videoNativeAdMuteStart) {
        isVideoNativeAdMuteStart = videoNativeAdMuteStart;
    }

    private void init(Context context , String packageName){
        this.adsSDK = this ;
        this.context = context;
        this.packageName = packageName ;
        this.adId = new AdsId(context, this , packageName);
        this.adId.startLoadingId();
        adsBanner = new AdsBanner();
    }

    private AdsSDK() {
    }

    private void handleNetwork(){
        if ( !adId.isAdsPreLoad() ){
            adId.startLoadingId();
        }
    }

    @Override
    public void OnAdsLoad() {
         initAds();
    }

    private boolean isInstallAd = true , isContentAd = true , isVideoAd = true;

    public void setInstallAd(boolean installAd) {
        isInstallAd = installAd;
    }

    public void setContentAd(boolean contentAd) {
        isContentAd = contentAd;
    }

    public void setVideoAd(boolean videoAd) {
        isVideoAd = videoAd;
    }

    private void initAds() {
        MobileAds.initialize(context, isTestAds ? adId.getTEST_ID_APP() : adId.getAppId());
        adsInterstitial = new AdsInterstitial(context , isTestAds ? adId.getTEST_ID_INTERSTITIAL() : adId.getInterstitialId() );
        adsNative = new AdsNative( context , isTestAds ? adId.getTEST_ID_NATIVE_ADVANCED() : adId.getNativeId() , isInstallAd , isContentAd , isVideoAd );
        adsNative.cacheNativeAd(true);
    }
}
