package com.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Amit on 2/26/2018.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final int SIGN_UP = 0;
    private final int SIGN_IN = 1;
    private final int SEND_MAIL = 7;
    private final int FACEBOOK = 2;
    private final int GOOGLE = 3;
    private final int TV_LOGIN = 4;
    private final int TV_SIGN_UP = 5;
    private final int TV_SKIP = 6;
    private final int TV_FORGOT_PASSWORD = 8;
    private TextView tvLogin, tvCreateAccount, tvHeader, tvTitle, tvForgotPassword;
    private Button btSignUp;
    private View vLogin, vFbDivider;
    private String titlePre;
    private boolean openEditProfile;
    private Activity activity ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lib_activity_login);
        activity = this ;
        initDataFromArgs();
        initView();
        setListenerOnView();
        mAuth = FirebaseAuth.getInstance();
        facebookInit();
        initDialog();
        setViewLogin(false);
        handleViewSettings();
    }

    private void handleViewSettings() {
        if (LoginSdk.getInstance() != null) {
            if (!LoginSdk.getInstance().isEmailLogin()) {
                findViewById(R.id.ll_email).setVisibility(View.GONE);
            }
            if (!LoginSdk.getInstance().isFacebookLogin()) {
                vFbDivider.setVisibility(View.GONE);
                findViewById(R.id.bt_facebook).setVisibility(View.GONE);
            }
            if (!LoginSdk.getInstance().isGoogleLogin()) {
                vFbDivider.setVisibility(View.GONE);
                findViewById(R.id.bSignUpGPlus).setVisibility(View.GONE);
            }
        }

    }

    private void initDataFromArgs() {
        if (getIntent() != null) {
            openEditProfile = getIntent().getBooleanExtra(AppConstant.OPEN_EDIT_PROFILE, false);
        }
    }

    private void initView() {
        tvEmail = (AutoCompleteTextView) findViewById(R.id.email);
        tvPassword = (AutoCompleteTextView) findViewById(R.id.password);
        tvHeader = (TextView) findViewById(R.id.tv_header);
        tvLogin = (TextView) findViewById(R.id.tv_login);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);
        tvCreateAccount = (TextView) findViewById(R.id.tvCreateNewAccount);
        String text = "Have an account? <font color=#3b5998>Login</font>";
        tvLogin.setText(Html.fromHtml(text));
        vLogin = findViewById(R.id.v_login);
        vFbDivider = findViewById(R.id.v_fb_divider);
        btSignUp = (Button) findViewById(R.id.bt_sign_up);
    }

    private void setListenerOnView() {
        tvLogin.setOnClickListener(this);
        tvCreateAccount.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        btSignUp.setOnClickListener(this);
        findViewById(R.id.bt_facebook).setOnClickListener(this);
        findViewById(R.id.bLogin).setOnClickListener(this);
        findViewById(R.id.b_send_mail).setOnClickListener(this);
        findViewById(R.id.tv_skip).setOnClickListener(this);
        findViewById(R.id.bSignUpGPlus).setOnClickListener(this);
    }

    private CallbackManager mCallbackManager;

    private void facebookInit() {
        mCallbackManager = CallbackManager.Factory.create();

        LoginButton loginButton = (LoginButton) findViewById(R.id.bt_facebook);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
                if (credential != null) {
                    fireBaseAuth(credential);
                }
            }

            @Override
            public void onCancel() {
                Util.showToastCentre(LoginActivity.this, "Login Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Util.showToastCentre(LoginActivity.this, "Error in login. Please try later.");
            }
        });
    }

    private FirebaseAuth mAuth;

    private void fireBaseAuth(AuthCredential credential) {
        showProgressDialog("Authorizing...");
        mAuth.signInWithCredential(credential)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        if (provider.equalsIgnoreCase(AuthUI.FACEBOOK_PROVIDER)) {
                            provider = AuthUI.GOOGLE_PROVIDER;
                            openDropInUi();
                        }
                    }
                })
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if ( activity != null && activity.hasWindowFocus() ) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                handleFireBaseUser(user);
                            } else {
                                Util.showToastCentre(LoginActivity.this, "Authentication failed.");
                            }
                            hideProgressDialog();
                        }
                    }
                });
    }

    private ProgressDialog progressDialog;

    private void initDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Authenticating...");
        progressDialog.setCancelable(false);
    }

    private void showProgressDialog(String msg) {
        try {
            if (progressDialog != null) {
                progressDialog.setMessage(msg);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && data != null ) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK && mAuth != null) {
                // Successfully signed in
                FirebaseUser user = mAuth.getCurrentUser();
                handleFireBaseUser(user);
                // ...
            } else {
                Util.showToastCentre(this, "Sign in failed");
                int error = response.getErrorCode();
                // Sign in failed, check response for error code
                // ...
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private static final int RC_SIGN_IN = 123;
    private String provider;

    private void openDropInUi() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.Builder(provider).build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    public void onClick(View view) {
        switch (Integer.parseInt(view.getTag().toString())) {
            case SIGN_UP:
                provider = AuthUI.EMAIL_PROVIDER;
                handleEmailPassword(true);
                break;
            case SIGN_IN:
                provider = AuthUI.EMAIL_PROVIDER;
                handleEmailPassword(false);
                break;
            case SEND_MAIL:
                provider = AuthUI.EMAIL_PROVIDER;
                sendVerificationEmail();
                break;
            case FACEBOOK:
                provider = AuthUI.FACEBOOK_PROVIDER;
                break;
            case GOOGLE:
                provider = AuthUI.GOOGLE_PROVIDER;
                openDropInUi();
                break;
            case TV_LOGIN:
                setViewLogin(true);
                break;
            case TV_SIGN_UP:
                setViewLogin(false);
                break;
            case TV_SKIP:
                onBackPressed();
                break;
            case TV_FORGOT_PASSWORD:
                sendResetPasswordMail();
                break;
        }
    }

    private void sendResetPasswordMail() {
        email = tvEmail.getText().toString();
        if (!Util.isEmptyOrNull(email)) {
            showProgressDialog("Sending Reset Password Mail....");
            mAuth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (activity != null && activity.hasWindowFocus()) {
                                hideProgressDialog();
                                Util.showToastCentre(LoginActivity.this, task.isSuccessful() ? "Email is send to your Email : " + email : "Error in sending.");
                            }
                        }
                    });
        } else {
            handleEmptyView("Email", tvEmail);
        }
    }

    private AutoCompleteTextView tvEmail, tvPassword;

    private void handleEmailPassword(boolean isCreate) {
        if (isTextEmailPassword()) {
            if (isCreate) {
                createUserWithEmailPassword(email, password);
            } else {
                signInWithEmailPassword(email, password, false);
            }
        }
    }

    private String email, password;

    private boolean isTextEmailPassword() {
        email = tvEmail.getText().toString();
        password = tvPassword.getText().toString();
        if (Util.isEmptyOrNull(email)) {
            handleEmptyView("Email", tvEmail);
            return false;
        } else if (Util.isEmptyOrNull(password)) {
            handleEmptyView("Password", tvPassword);
            return false;
        } else {
            return true;
        }
    }

    private void handleEmptyView(String text, AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setError("Invalid " + text);
    }

    private void createUserWithEmailPassword(String email, String password) {
        showProgressDialog(titlePre + "....");
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (activity != null && activity.hasWindowFocus()) {
                            if (task.isSuccessful() && task.getResult() != null && task.getResult().getUser() != null) {
                                Util.showToastCentre(LoginActivity.this, "Account is successfully created. Please verify Email to Login Successfully.");
                                sendVerificationEmail();
                            } else {
                                hideProgressDialog();
                                Util.showToastCentre(LoginActivity.this, task.getException().getMessage());
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void sendVerificationEmail() {
        showProgressDialog("Sending Verification Mail....");
        final FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if ( activity != null && activity.hasWindowFocus() ) {
                        hideProgressDialog();
                        if (task.isSuccessful()) {
                            setViewLogin(true);
                            Util.showToastCentre(LoginActivity.this, "Verification email sent to " + user.getEmail());
                        } else {
                            Util.showToastCentre(LoginActivity.this, "Failed to send verification email.");
                        }
                    }
                }
            });
        } else {
            if (isTextEmailPassword()) {
                signInWithEmailPassword(email, password, false);
            }
        }
    }

    private void signInWithEmailPassword(String email, String password, final boolean isSendMail) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if ( activity != null && activity.hasWindowFocus() ) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                if (user.isEmailVerified()) {
                                    handleFireBaseUser(user);
                                } else if (isSendMail) {
                                    sendVerificationEmail();
                                } else {
                                    showAlertEmailVerify();
                                }
                            } else {
                                Util.showToastCentre(LoginActivity.this, "Wrong Email or Password.");
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void showAlertEmailVerify() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Email Verification")
                .setMessage("Please verify the Email by click the link sended to your Email ID.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void handleFireBaseUser(FirebaseUser user) {
//        Util.showToastCentre(this, "Sign in Successful : " + user.getUid());
        saveData(user);
        Util.syncDataForUserId(new Util.OnRegistrationSync() {
            @Override
            public void OnRegistrationSync(boolean isSuccess) {
                if (openEditProfile) {
                    openProfile();
                } else {
                    setResult(RESULT_OK);
                    onBackPressed();
                }
            }
        }, this, true);
    }

    private void openProfile() {
        startActivity(new Intent(this, ProfileActivity.class));
        if ( activity != null ) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (activity != null) {
                        activity.finish();
                    }
                }
            }, 10);
        }
    }

    private void saveData(FirebaseUser user) {
        SharedPrefUtil.setString(AppConstant.SharedPref.USER_EMAIL, user.getEmail());
        SharedPrefUtil.setString(AppConstant.SharedPref.USER_UID, user.getUid());
        SharedPrefUtil.setString(AppConstant.SharedPref.USER_NAME, user.getDisplayName());
        SharedPrefUtil.setString(AppConstant.SharedPref.LOGIN_PROVIDER, provider);
        SharedPrefUtil.setInt(AppConstant.SharedPref.USER_EMAIL_VERIFIED, (user.isEmailVerified() ? 1 : 0));
        if (user.getPhotoUrl() != null) {
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHOTO_URL, user.getPhotoUrl().toString());
        }
        SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_LOGIN_COMPLETE, true);
    }

    private void setViewLogin(boolean isLogin) {
        tvCreateAccount.setVisibility(isLogin ? View.VISIBLE : View.GONE);
        vLogin.setVisibility(isLogin ? View.VISIBLE : View.GONE);
        tvLogin.setVisibility(!isLogin ? View.VISIBLE : View.GONE);
        tvForgotPassword.setVisibility(isLogin ? View.VISIBLE : View.GONE);
        btSignUp.setVisibility(!isLogin ? View.VISIBLE : View.GONE);
        titlePre = isLogin ? "Login" : "Sign Up";
        tvTitle.setText(titlePre);
        tvHeader.setText(titlePre + titlePost);
    }

    private final String titlePost = " to see your Rank";

}
