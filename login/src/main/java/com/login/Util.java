package com.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.login.model.LoginUser;
import com.login.model.LoginUserResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Amit on 2/26/2018.
 */

public class Util {

    public static void showToastCentre(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static boolean isEmptyOrNull(String s) {
        return (s == null || TextUtils.isEmpty(s));
    }

    public static String getDeviceId() {
        return Settings.Secure.getString(LoginSdk.getInstance().getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public interface OnRegistrationSync {
        void OnRegistrationSync(boolean isSuccess);
    }

    public static void updateProfileData(final OnRegistrationSync onRegistrationSync, final Context context, boolean showProgress) {
        MultipartBody.Part fileToUpload = null;
        String filePath = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
        if (!isEmptyOrNull(filePath)) {
            File file;
            if (filePath.startsWith("file")) {
                String selectedFilePath = FilePath.getPath(context, Uri.parse(filePath));
                file = new File(selectedFilePath);
            } else {
                file = new File(filePath);
            }
            if (file.exists()) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            }
        }
        if (LoginSdk.getInstance().getApiInterface() != null) {
            if (showProgress) {
                initDialog(context);
                showProgressDialog("Updating Your Info.....");
            }
            LoginSdk.getInstance().getApiInterface().updateUserProfile(
                    SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL)
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO)
                    , SharedPrefUtil.getInt(AppConstant.SharedPref.USER_EMAIL_VERIFIED)
                    , RequestBody.create(MediaType.parse("text/plain"), SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME))
                    , fileToUpload
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHONE)
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_ADDRESS)
                    , SharedPrefUtil.getInt(AppConstant.SharedPref.USER_STATE)
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_POSTAL_CODE)
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_DATE_OF_BIRTH)
                    , SharedPrefUtil.getInt(AppConstant.SharedPref.USER_GENDER)
                    , getDeviceId()
                    , SharedPrefUtil.getString(AppConstant.SharedPref.PLAYER_ID)
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_ABOUT_ME)
                    , LoginSdk.getInstance().getAppPackageName()
            ).enqueue(new ResponseCallBack(new ResponseCallBack.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    if (!Util.isEmptyOrNull(data)) {
                        try {
                            LoginUserResponse loginUserResponse = LoginSdk.getGson().fromJson(data, LoginUserResponse.class);
                            if (loginUserResponse != null) {
                                if (loginUserResponse.getLoginUser() != null && isEmptyOrNull(loginUserResponse.getLoginUser().getPhotoUrl())) {
                                    SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHOTO_URL, loginUserResponse.getLoginUser().getPhotoUrl());
                                }
                                showToastCentre(context, "Success in syncing Data.");
                                onRegistrationSync.OnRegistrationSync(true);
                            } else {
                                showToastCentre(context, "Error in syncing Data. Please try again.");
                                onRegistrationSync.OnRegistrationSync(false);
                            }
                        } catch (JsonSyntaxException e) {
                            showToastCentre(context, "Error in syncing Data. Please try again.");
                            onRegistrationSync.OnRegistrationSync(false);
                            e.printStackTrace();
                        }

                    } else {
                        showToastCentre(context, "Error in syncing Data. Please try again.");
                        onRegistrationSync.OnRegistrationSync(false);
                    }
                    hideProgressDialog();

                }
            }, "update-user-profile"));
        } else {
            showToastCentre(context, "Error in syncing Data. Please try again.");
            onRegistrationSync.OnRegistrationSync(false);
        }
    }

    public static void updatePref(String key , String value){
        SharedPrefUtil.setString( key , value );
    }

    public static void syncDataForUserId(final OnRegistrationSync onRegistrationSync, final Context context, boolean showProgress) {
        final int STATUS_CODE_LOGIN = 3;
        final int STATUS_CODE_SIGN_UP = 1;
        String playerID = SharedPrefUtil.getString(AppConstant.SharedPref.PLAYER_ID);
        String photoUrl = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
        String uid = SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID);
        String email = SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL);
        String provider = SharedPrefUtil.getString(AppConstant.SharedPref.LOGIN_PROVIDER);
        String name = SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
        int emailVerify = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_EMAIL_VERIFIED);

        if (LoginSdk.getInstance() != null && LoginSdk.getInstance().getApiInterface() != null) {
            if (showProgress && context != null) {
                initDialog(context);
                showProgressDialog("Completing Registration Process...");
            }
            LoginSdk.getInstance().getApiInterface().loginSignUp(email, uid, emailVerify, name
                    , photoUrl, provider, Util.getDeviceId(), null, 1, playerID
                    , LoginSdk.getInstance().getAppPackageName()).enqueue(new ResponseCallBack(new ResponseCallBack.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    hideProgressDialog();
                    if (!Util.isEmptyOrNull(data)) {
                        try {
                            LoginUserResponse loginUserResponse = LoginSdk.getGson().fromJson(data, LoginUserResponse.class);
                            if (loginUserResponse != null
                                    && (loginUserResponse.getStatus() == STATUS_CODE_LOGIN || loginUserResponse.getStatus() == STATUS_CODE_SIGN_UP)) {
                                if ( onRegistrationSync != null ) {
                                    onRegistrationSync.OnRegistrationSync(true);
                                }
                                saveUserSyncData(loginUserResponse.getLoginUser());
                                showToastCentre(context, "Account Creation Successful.");
                                SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_REGISTRATION_COMPLETE, true);
                            } else {
                                showToastCentre(context, "Account Sync failed.");
                                if ( onRegistrationSync != null ) {
                                    onRegistrationSync.OnRegistrationSync(false);
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            showToastCentre(context, "Account Sync failed.");
                            if ( onRegistrationSync != null ) {
                                onRegistrationSync.OnRegistrationSync(false);
                            }
                            e.printStackTrace();
                        }
                    } else {
                        showToastCentre(context, "Account Sync failed.");
                        if ( onRegistrationSync != null ) {
                            onRegistrationSync.OnRegistrationSync(false);
                        }
                    }
                }
            }, "login-signup"));
        }else {
            showToastCentre(context, "Account Sync failed.");
            if ( onRegistrationSync != null ) {
                onRegistrationSync.OnRegistrationSync(false);
            }
        }
    }

    static void saveUserSyncData(LoginUser loginUser) {
        if (loginUser != null) {
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_ID_AUTO, getEmptyData(loginUser.getId() + ""));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_NAME, getEmptyData(loginUser.getName()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHOTO_URL, getEmptyData(loginUser.getPhotoUrl()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHONE, getEmptyData(loginUser.getMobile()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_EMAIL, getEmptyData(loginUser.getEmail()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_ABOUT_ME, getEmptyData(loginUser.getAbout_me()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_DATE_OF_BIRTH, getEmptyData(loginUser.getDob()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_POSTAL_CODE, getEmptyData(loginUser.getPostal()));
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_ADDRESS, getEmptyData(loginUser.getAddress()));
            SharedPrefUtil.setInt(AppConstant.SharedPref.USER_STATE, loginUser.getState());
            SharedPrefUtil.setInt(AppConstant.SharedPref.USER_GENDER, loginUser.getGender());
        }
    }

    private static String getEmptyData(String data) {
        return isEmptyOrNull(data) ? "" : data;
    }

    public static void loadUserImage(String photoUrl, ImageView ivProfilePic) {
        loadUserImage(photoUrl, ivProfilePic, R.drawable.place__holder);
    }

    public static void loadUserImage(String photoUrl, ImageView ivProfilePic, int res) {
        if (!isEmptyOrNull(photoUrl) && ivProfilePic != null) {
            if (photoUrl.startsWith("http://") || photoUrl.startsWith("https://")) {
            } else if (photoUrl.startsWith("file://")) {
            } else
                photoUrl = LoginSdk.getInstance().getUserImageUrl() + photoUrl;
            LoginSdk.getInstance().getPicasso().load(photoUrl)
                    .resize(240, 240)
                    .centerCrop()
                    .placeholder(res)
                    .transform(new CircleTransform())
                    .into(ivProfilePic);

        } else if ( ivProfilePic != null )
            ivProfilePic.setImageResource(res);
    }


    private static ProgressDialog progressDialog;

    static void initDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }

    static void showProgressDialog(String msq) {
        try {
            if (progressDialog != null) {
                progressDialog.setMessage(msq);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
