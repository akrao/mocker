package com.login;

import com.login.model.BaseModel;

import org.json.JSONException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.login.LoginSdk.JSON_EXCEPTION;
/**
 * Created by Amit on 4/10/2018.
 */

public class ResponseCallBack implements Callback<BaseModel> {

    public interface OnNetworkCall {
        void onComplete(boolean status, String data);
    }
    private OnNetworkCall onNetworkCall ;
    private String endPoint ;

    public ResponseCallBack(OnNetworkCall onNetworkCall, String endPoint ) {
        this.onNetworkCall = onNetworkCall;
        this.endPoint = endPoint;
    }

    private ResponseCallBack() {
    }

    @Override
    public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
        if (response != null & response.code() != 0) {
            int responseCode = response.code();
            if (responseCode == 200) {
                if ( response.body() != null  && response.body() instanceof BaseModel ) {
                    BaseModel baseModel = ( BaseModel ) response.body();
                    boolean callConfig = !Util.isEmptyOrNull(baseModel.getCall_config())
                            && baseModel.getCall_config().equals(ConfigConstant.TRUE);
                    if (callConfig && !LoginSdk.isCallConfig()) {
                        LoginSdk.setIsCallConfig(true);
                        String bug = LoginSdk.getInstance().getApiConfigError(responseCode ,
                                "Call_config=" + baseModel.getCall_config()
                                , LoginSdk.getInstance().getRequestBody(call));
                        LoginSdk.getInstance().getCallConfig().callConfig(true, true, bug);
                    }
                    boolean status = !callConfig && !Util.isEmptyOrNull(baseModel.getStatus())
                            && baseModel.getStatus().equals(ConfigConstant.SUCCESS);
                    String s = LoginSdk.getGson().toJson( baseModel.getData() ) ;
                    if ( onNetworkCall != null ) {
                        onNetworkCall.onComplete(status, status ? s : "");
                    }
                }
            } else if (responseCode == INTERNAL_SERVER_ERROR || responseCode == NOT_FOUND
                    || responseCode == BAD_GATEWAY || responseCode == SERVICE_UNAVAILABLE
                    || responseCode == GATEWAY_TIMEOUT) {
                if ( onNetworkCall != null ) {
                    onNetworkCall.onComplete(false, "");
                }
                String apiCall = LoginSdk.getInstance().getApiErrorCode().get(endPoint);
                if (apiCall == null) {
                    String bug = LoginSdk.getInstance().getApiConfigError(responseCode, response.message()
                            , LoginSdk.getInstance().getRequestBody(call));
                    LoginSdk.getInstance().getApiErrorCode().put(endPoint, bug);
                    LoginSdk.getInstance().getCallConfig().callConfig(true, true, bug);
                }
            }
        }else {
            if ( onNetworkCall != null ) {
                onNetworkCall.onComplete(false, "");
            }
        }
    }

    @Override
    public void onFailure(Call<BaseModel> call, Throwable t) {
        if ( onNetworkCall != null ) {
            onNetworkCall.onComplete(false, "");
        }
        if (t instanceof JSONException) {
            String apiCall = LoginSdk.getInstance().getApiJsonException().get(endPoint);
            if (apiCall == null) {
                String bug = LoginSdk.getInstance().getApiConfigError(JSON_EXCEPTION, t.getMessage()
                        , LoginSdk.getInstance().getRequestBody(call));
                LoginSdk.getInstance().getApiJsonException().put(endPoint, bug);
                LoginSdk.getInstance().getCallConfig().callConfig(true, true, bug);
            }

        }
    }

    private final int NOT_FOUND = 404;
    private final int INTERNAL_SERVER_ERROR = 500;
    private final int BAD_GATEWAY = 502;
    private final int SERVICE_UNAVAILABLE = 503;
    private final int GATEWAY_TIMEOUT = 504;

}
