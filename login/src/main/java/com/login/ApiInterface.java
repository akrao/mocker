package com.login;

import com.login.model.BaseModel;
import com.login.model.LoginUserResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Amit on 2/28/2018.
 */

public interface ApiInterface {

    /*
     * Update user profile
     */
    @Multipart
    @POST("update-user-profile")
    Call<BaseModel> updateUserProfile(
            @Query("email") String email,
            @Query("id") String auto_id,
            @Query("email_verified") int email_verified,
            @Part("name") RequestBody name,
            @Part MultipartBody.Part image,
            @Query("phone") String mobile,
            @Query("address") String address,
            @Query("state") int state,
            @Query("postal") String postal ,
            @Query("dob")           String dob,
            @Query("gender")        int gender ,
            @Query("device_id") String device_token ,
            @Query("player_id")     String player_id,
            @Query("about_me")      String about_me,
            @Query("application_id") String app_id
    );

    /*
     *Login User so that it can generate User on server
     * get User Auto ID
     */
    @POST("login-signup")
    Call<BaseModel> loginSignUp(
            @Query("email") String email,
            @Query("firebase_id") String uid,
            @Query("is_verified") int email_verified,
            @Query("name") String name,
            @Query("photo_url") String photo_url,
            @Query("provider_id") String provider_id,
            @Query("device_id") String device_token ,
            @Query("phone") String phone ,
            @Query("device_type") int device_type ,
            @Query("player_id") String player_id ,
            @Query("application_id") String app_id
    );

}
