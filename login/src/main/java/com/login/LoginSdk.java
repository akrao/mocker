package com.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by Amit on 2/26/2018.
 */

public class LoginSdk {
    private static LoginSdk ourInstance;
    private static Context context;
    private ApiInterface apiInterface;
    private Picasso picasso;

    SimpleDateFormat simpleDateFormat;

    private String getFormatDate(long time) {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(time));
    }


    public String getApiConfigError(int code, String msg, String apiMsg) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt("response_code", code);
            object.putOpt("response_msg", getErrorTypeMsg(code));
            object.putOpt("timestamp", DateFormat.getInstance().format(new Date(System.currentTimeMillis())));
            object.putOpt("device_id", Util.getDeviceId());
            object.putOpt("msg", msg);
            object.putOpt("api_failure", apiMsg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    private static Gson gson = new Gson();

    public static Gson getGson() {
        return gson;
    }

    private String getErrorTypeMsg(int code) {
        String msg;
        switch (code) {
            case INTERNAL_SERVER_ERROR:
                msg = "INTERNAL_SERVER_ERROR";
                break;
            case NOT_FOUND:
                msg = "NOT_FOUND";
                break;
            case BAD_GATEWAY:
                msg = "BAD_GATEWAY";
                break;
            case SERVICE_UNAVAILABLE:
                msg = "SERVICE_UNAVAILABLE";
                break;
            case GATEWAY_TIMEOUT:
                msg = "GATEWAY_TIMEOUT";
                break;
            case JSON_EXCEPTION:
                msg = "JSON_EXCEPTION";
                break;
            default:
                msg = "error";
                break;
        }
        return msg;
    }

    private HashMap<String, String> apiJsonException;
    private HashMap<String, String> apiErrorCode;
    private static boolean isCallConfig = false;

    public static boolean isCallConfig() {
        return isCallConfig;
    }

    public static void setIsCallConfig(boolean isCallConfigV) {
        isCallConfig = isCallConfigV;
    }

    public static final int INTENT_LOGIN = 1000;
    public static final int JSON_EXCEPTION = 1000;
    private final int NOT_FOUND = 404;
    private final int INTERNAL_SERVER_ERROR = 500;
    private final int BAD_GATEWAY = 502;
    private final int SERVICE_UNAVAILABLE = 503;
    private final int GATEWAY_TIMEOUT = 504;

    public HashMap<String, String> getApiJsonException() {
        return apiJsonException;
    }

    public HashMap<String, String> getApiErrorCode() {
        return apiErrorCode;
    }

    public static SharedPreferences getDefaultSharedPref() {
        if ( context != null ) {
            return context.getSharedPreferences(context.getPackageName() + ".login.sdk", Context.MODE_PRIVATE);
        } else {
            return null ;
        }
    }

    public static LoginSdk getInstance(Context context, String appPackageName) {
        if (ourInstance == null) {
            ourInstance = new LoginSdk(context, appPackageName);
        }
        return ourInstance;
    }

    public static LoginSdk getInstance(Context context, Retrofit retrofit, boolean isFacebookLogin, boolean isGoogleLogin, boolean isEmailLogin, String appPackageName) {
        if (ourInstance == null) {
            ourInstance = new LoginSdk(context, appPackageName);
            ourInstance.setApiInterface(retrofit);
            ourInstance.isFacebookLogin = isFacebookLogin;
            ourInstance.isGoogleLogin = isGoogleLogin;
            ourInstance.isEmailLogin = isEmailLogin;
            ourInstance.appPackageName = appPackageName;
        }
        return ourInstance;
    }

    ApiInterface getApiInterface() {
        return apiInterface;
    }

    public void setApiInterface(Retrofit retrofit) {
        if (retrofit != null) {
            this.apiInterface = retrofit.create(ApiInterface.class);
        }
    }

    public static LoginSdk getInstance() {
        return ourInstance;
    }

    private String userImageUrl;

    public void setUserImageUrl(String userImageUrl) {
        ourInstance.userImageUrl = userImageUrl;
    }

    static String getUserImageUrl() {
        return ourInstance.userImageUrl;
    }

    Picasso getPicasso() {
        return picasso;
    }

    boolean isHeaderTitle() {
        return ourInstance.isHeaderTitle;
    }

    boolean isFacebookLogin() {
        return ourInstance.isFacebookLogin;
    }

    boolean isGoogleLogin() {
        return ourInstance.isGoogleLogin;
    }

    boolean isEmailLogin() {
        return ourInstance.isEmailLogin;
    }

    String getAppPackageName() {
        return appPackageName;
    }

    static Context getContext() {
        return context;
    }

    private LoginSdk(Context context, String appPackageName) {
        this.context = context;
        this.appPackageName = appPackageName;
        picasso = Picasso.get();
        Util.initDialog(context);
        apiJsonException = new HashMap<>();
        apiErrorCode = new HashMap<>();
    }

    public String getRequestBody(Call call) {
        String api = "";
        if (call != null && call.request() != null)
            api = call.request().toString();
        return api;

    }

    private CallConfig callConfig;

    public CallConfig getCallConfig() {
        return callConfig;
    }

    public void setCallConfig(CallConfig callConfig) {
        this.callConfig = callConfig;
    }

    public interface CallConfig {
        public void callConfig(final boolean isMain, boolean isBug, final String bug);
    }

    private LoginSdk() {
    }

    public void initFacebook(String appId) {
        FacebookSdk.setApplicationId(appId);
        FacebookSdk.sdkInitialize(context);
    }

    public boolean isRegComplete() {
        return SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_REGISTRATION_COMPLETE);
    }

    public void setRegComplete(boolean flag) {
        SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_REGISTRATION_COMPLETE, flag);
    }

    public void setLoginComplete(boolean flag) {
        SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_LOGIN_COMPLETE, flag);
    }

    public String getUserName() {
        return SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
    }

    public String getUserImage() {
        return SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
    }

    public String getUserId() {
        return SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO);
    }

    public String getEmailId() {
        return SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL);
    }

    public void openProfile(final Activity context, boolean isOpenEditProfile) {
        openLoginPageCallBack( context , isOpenEditProfile , 0 , isOpenEditProfile );
    }
    public void openLoginPage(final Activity context, final boolean isOpenEditProfile) {
        openLoginPageCallBack( context , isOpenEditProfile , 0 , false );
    }

    public void openLoginPageCallBack(final Activity context, final boolean isOpenEditProfile, int requestCode , boolean isProfileEdit) {
        Intent intent;
        boolean isRegComplete = SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_REGISTRATION_COMPLETE);
        boolean isLoginComplete = SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_LOGIN_COMPLETE);
        if (isRegComplete) {
            intent = new Intent(context, ProfileActivity.class);
            intent.putExtra(AppConstant.OPEN_EDIT_PROFILE, isProfileEdit);
            context.startActivity(intent);
        } else if (isLoginComplete) {
            Util.syncDataForUserId(new Util.OnRegistrationSync() {
                @Override
                public void OnRegistrationSync(boolean isSuccess) {
                    if (isSuccess) {
                        openLoginPage(context, isOpenEditProfile);
                    }
                }
            }, context, true);
        } else {
            intent = new Intent(context, LoginActivity.class);
            intent.putExtra(AppConstant.OPEN_EDIT_PROFILE, isOpenEditProfile);
            if ( requestCode == 0 ) {
                context.startActivity(intent);
            }else {
                context.startActivityForResult(intent , requestCode);
            }
        }
    }

    private boolean isHeaderTitle = true;
    private boolean isFacebookLogin = true;
    private boolean isGoogleLogin = true;
    private boolean isEmailLogin = true;
    private String appPackageName;

    public void setHeaderTitleLoginEnabled(boolean isEnable) {
        ourInstance.isHeaderTitle = isEnable;
    }

    public void setFacebookLoginEnabled(boolean isEnable) {
        ourInstance.isHeaderTitle = isEnable;
    }

    public void setGoogleLoginEnabled(boolean isEnable) {
        ourInstance.isHeaderTitle = isEnable;
    }

    public void setEmailLoginEnabled(boolean isEnable) {
        ourInstance.isHeaderTitle = isEnable;
    }

}
