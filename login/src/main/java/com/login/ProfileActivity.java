package com.login;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Calendar;

/**
 * Created by Amit on 2/28/2018.
 */

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, CalendarView.OnDateChangeListener {

    private AutoCompleteTextView tvEmail, tvPhone, tvName, tvPinCode, tvAddress, tvBirth, tvGender, tvState, tvAboutMe;
    private RadioButton rbMale, rbFemale;
    private Spinner spinnerState;
    private ImageView ivProfilePic, ivEditImage;
    private View vEditProfile, vBottomAction;
    private RadioGroup radioGroup;
    private String photoUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lib_activity_profile);
        setupToolbar();
        initView();
        setListener();
        setDataInViews();
        disableTouch();
        disableViews();
        handleRegProcess();
        initDataFromArgs();
    }

    private void initDataFromArgs(){
        boolean isEdit = getIntent().getBooleanExtra(AppConstant.OPEN_EDIT_PROFILE , false);
        if ( isEdit ){
            enableTouch();
            enableViews();
        }
    }

    private void handleRegProcess() {
        if (!SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_REGISTRATION_COMPLETE)) {
            Util.syncDataForUserId(new Util.OnRegistrationSync() {
                @Override
                public void OnRegistrationSync(boolean isSuccess) {
                }
            }, this, false);
        }
    }

    private void initView() {
        tvEmail = (AutoCompleteTextView) findViewById(R.id.email);
        tvPhone = (AutoCompleteTextView) findViewById(R.id.mobile);
        tvName = (AutoCompleteTextView) findViewById(R.id.name);
        tvAddress = (AutoCompleteTextView) findViewById(R.id.address);
        tvPinCode = (AutoCompleteTextView) findViewById(R.id.pincode);
        tvBirth = (AutoCompleteTextView) findViewById(R.id.birth);
        tvGender = (AutoCompleteTextView) findViewById(R.id.gender);
        tvAboutMe = (AutoCompleteTextView) findViewById(R.id.about_me);
        tvState = (AutoCompleteTextView) findViewById(R.id.state);
        spinnerState = (Spinner) findViewById(R.id.spinnerState);
        ivProfilePic = (ImageView) findViewById(R.id.ivProfilePic);
        ivEditImage = (ImageView) findViewById(R.id.iv_edit_image);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);
        rbMale = (RadioButton) findViewById(R.id.rbMale);
        vEditProfile = findViewById(R.id.tv_edit_profile);
        vBottomAction = findViewById(R.id.bottom_action);
        radioGroup = (RadioGroup) findViewById(R.id.rg_gender);
//        findViewById(R.id.tv_submit).setOnClickListener(this);
        photoUrl = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
    }

    private String state;

    private void setState() {
        String[] s = getResources().getStringArray(R.array.state_names);
        int position = 1;
        for (int i = 0; i < s.length; i++) {
            if (state.toLowerCase().contains(s[i].toLowerCase()))
                position = i;
        }
        spinnerState.setSelection(position);
    }

    private void setListener() {
        vEditProfile.setOnClickListener(this);
        tvBirth.setOnClickListener(this);
        findViewById(R.id.cancel_button).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
        findViewById(R.id.iv_edit_image).setOnClickListener(this);

        rbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    userGender = GENDER_MALE;
                } else {
                    userGender = GENDER_FEMALE;
                }
            }
        });
        rbFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    userGender = GENDER_FEMALE;
                } else {
                    userGender = GENDER_MALE;
                }
            }
        });
    }

    private String GENDER_MALE = "MALE";
    private String GENDER_FEMALE = "FEMALE";
    private String userGender;

    private void setDataInViews() {
        setDataInTextView(tvEmail, AppConstant.SharedPref.USER_EMAIL);
        setDataInTextView(tvName, AppConstant.SharedPref.USER_NAME);
        setDataInTextView(tvPhone, AppConstant.SharedPref.USER_PHONE);
        setDataInTextView(tvBirth, AppConstant.SharedPref.USER_DATE_OF_BIRTH);
        setDataInTextView(tvAboutMe, AppConstant.SharedPref.USER_ABOUT_ME);
        setDataInTextView(tvAddress, AppConstant.SharedPref.USER_ADDRESS);
        setDataInTextView(tvPinCode, AppConstant.SharedPref.USER_POSTAL_CODE);
        handleSpinner();
        handleGender();
        setImage();
    }

    private void handleSpinner() {
        int stateId = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_STATE);
        tvState.setText(getResources().getStringArray(R.array.state_names)[stateId]);
        spinnerState.setSelection(stateId);
    }

    private void handleGender() {
        int value = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_GENDER);
//        userGender = value > 1 ? GENDER_FEMALE : GENDER_MALE;
        userGender = getGender(value);
        tvGender.setText(userGender);
        if (value > 1) {
            rbFemale.setChecked(true);
            rbMale.setChecked(false);
        } else {
            rbFemale.setChecked(false);
            rbMale.setChecked(true);
        }
    }

    private String getGender(int val) {
        String gen;
        switch (val) {
            case 0:
                gen = "";
                break;
            case 1:
                gen = GENDER_MALE;
                break;
            case 2:
                gen = GENDER_FEMALE;
                break;
            default:
                gen = "";
                break;
        }
        return gen ;
    }

    private void setDataInTextView(TextView textView, String key) {
        String value = SharedPrefUtil.getString(key);
        if (!Util.isEmptyOrNull(value))
            textView.setText(value);
    }

    private void getDataInView() {
        getDataFromTextView(tvEmail, AppConstant.SharedPref.USER_EMAIL);
        getDataFromTextView(tvName, AppConstant.SharedPref.USER_NAME);
        getDataFromTextView(tvPhone, AppConstant.SharedPref.USER_PHONE);
        getDataFromTextView(tvBirth, AppConstant.SharedPref.USER_DATE_OF_BIRTH);
        getDataFromTextView(tvAboutMe, AppConstant.SharedPref.USER_ABOUT_ME);
        getDataFromTextView(tvAddress, AppConstant.SharedPref.USER_ADDRESS);
        getDataFromTextView(tvPinCode, AppConstant.SharedPref.USER_POSTAL_CODE);
        handleGenderForSaving();
        SharedPrefUtil.setInt(AppConstant.SharedPref.USER_STATE, spinnerState.getSelectedItemPosition());
        if (photoUrl != null) {
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHOTO_URL, photoUrl);
        }
    }

    private void handleGenderForSaving() {
        userGender = rbFemale.isChecked() ? GENDER_FEMALE : GENDER_MALE;
        tvGender.setText(userGender);
        SharedPrefUtil.setInt(AppConstant.SharedPref.USER_GENDER, rbFemale.isChecked() ? 2 : 1);
    }

    private void getDataFromTextView(TextView textView, String key) {

        String value = textView.getText().toString();
        if (!Util.isEmptyOrNull(value))
            SharedPrefUtil.setString(key, value);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_edit_image) {
            CropImage.startPickImageActivity(this);
        } else if (view.getId() == R.id.tv_edit_profile) {
            enableTouch();
            enableViews();
        } else if (view.getId() == R.id.save) {
            saveData();
        } else if (view.getId() == R.id.cancel_button) {
            disableTouch();
            disableViews();
            setDataInViews();
        } else if (view.getId() == R.id.birth) {
            if (vEditProfile.getVisibility() == View.GONE) {
                openDateDialog();
            }
        }
    }

    private void saveData() {
        getDataInView();
        Util.updateProfileData(new Util.OnRegistrationSync() {
            @Override
            public void OnRegistrationSync(boolean isSuccess) {
                if (isSuccess) {
                    disableTouch();
                    disableViews();
                    setDataInViews();
                }
            }
        }, this, true);
    }

    private void disableViews() {
        vEditProfile.setVisibility(View.VISIBLE);
        vBottomAction.setVisibility(View.GONE);
        tvGender.setVisibility(View.VISIBLE);
        tvState.setVisibility(View.VISIBLE);
        radioGroup.setVisibility(View.GONE);
        ivEditImage.setVisibility(View.GONE);
        spinnerState.setVisibility(View.GONE);
    }

    private void enableViews() {
        ivEditImage.setVisibility(View.VISIBLE);
        vEditProfile.setVisibility(View.GONE);
        vBottomAction.setVisibility(View.VISIBLE);
        tvGender.setVisibility(View.GONE);
        tvState.setVisibility(View.GONE);
        radioGroup.setVisibility(View.VISIBLE);
        spinnerState.setVisibility(View.VISIBLE);
    }

    private void disableTouch() {
        getSupportActionBar().setTitle("Profile");
        disableViewTouch(tvEmail);
        disableViewTouch(tvName);
        disableViewTouch(tvPhone);
        disableViewTouch(tvPinCode);
        disableViewTouch(tvAboutMe);
        disableViewTouch(tvAddress);
        disableViewTouch(tvGender);
        disableViewTouch(tvState);
        disableViewTouch(tvBirth);
    }

    private void disableViewTouch(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setFocusable(false);
        autoCompleteTextView.setFocusableInTouchMode(false);
    }

    private void enableViewTouch(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setFocusable(true);
        autoCompleteTextView.setFocusableInTouchMode(true);
    }

    private void enableTouch() {
        getSupportActionBar().setTitle("Edit Profile");
        if (Util.isEmptyOrNull(tvEmail.getText().toString())) {
            enableViewTouch(tvEmail);
        }
        if (Util.isEmptyOrNull(tvName.getText().toString())) {
            enableViewTouch(tvName);
        }
        enableViewTouch(tvPhone);
        enableViewTouch(tvPinCode);
        enableViewTouch(tvAboutMe);
        enableViewTouch(tvAddress);
        enableViewTouch(tvGender);
    }

    private void openDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                builder = new StringBuilder();
                builder.append(year).append(String.format("-%02d-", month + 1)).append(String.format("%02d", dayOfMonth));
                selectedDate = builder.toString();
                tvBirth.setText(selectedDate);
            }
        }, year, month, day).show();
    }

    private StringBuilder builder;
    private String selectedDate;

    @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
        builder = new StringBuilder();
        builder.append(year).append(String.format("-%02d-", month + 1)).append(String.format("%02d", dayOfMonth));
        selectedDate = builder.toString();
        tvBirth.setText(selectedDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                }
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mCropImageUri = result.getUri();
                photoUrl = mCropImageUri.toString();
                setImage();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void setImage() {
        Util.loadUserImage(photoUrl, ivProfilePic);
    }

    private Uri mCropImageUri;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Util.showToastCentre(this, "Cancelling, required permissions are not granted");
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Util.showToastCentre(this, "Cancelling, required permissions are not granted");
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(this);
    }

    private void setupToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
