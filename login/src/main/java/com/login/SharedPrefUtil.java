package com.login;

import android.content.SharedPreferences;

public class SharedPrefUtil {

    private static final String TAG = "SharedPrefUtil";

    public static String getString(String key) {
        if (getDefaultSharedPref() != null) {
            return decrypt(getDefaultSharedPref().getString(encrypt(key), ""));
        } else {
            return decrypt("");
        }
    }

    public static int getInt(String key) {
        if (getDefaultSharedPref() != null) {
            return getDefaultSharedPref().getInt(encrypt(key), 0);
        } else {
            return (0);
        }
    }

    public static int getIntDef(String key , int def ) {
        return getDefaultSharedPref().getInt(encrypt(key), def);
    }

    public static float getFloat(String key) {
        return getDefaultSharedPref().getFloat(encrypt(key), 0);

    }

    public static long getLong(String key) {
        return getDefaultSharedPref().getLong(encrypt(key), 0);
    }
    public static boolean getBoolean(String key) {
        if (getDefaultSharedPref() != null) {
            return getDefaultSharedPref().getBoolean(key, false);
        } else {
            return false;
        }
    }


    private static String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return input ;
//        if (SupportUtil.isEmptyOrNull( input )) {
//            return input ;
//        } else {
//            return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
//        }
    }

    private static String decrypt(String input) {
        return input;
//        if (SupportUtil.isEmptyOrNull( input )) {
//            return input ;
//        } else {
//            return new String(Base64.decode(input, Base64.DEFAULT));
//        }
    }
    /**
     * Set String value for a particular key.
     *
     * @param key   The string resource Id of the key
     * @param value The value to set for the key
     */
    public static void setString(String key, String value) {
        if (getDefaultSharedPref() != null && !Util.isEmptyOrNull(key) ) {
            final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
            if ( editor != null ) {
                editor.putString(encrypt(key), encrypt(value));
                editor.apply();
            }
        }
    }

    /**
     * Set int value for key.
     *
     * @param key   The string resource Id of the key
     * @param value The value to set for the key
     */
    public static void setInt(String key, int value) {
        if (getDefaultSharedPref() != null && !Util.isEmptyOrNull(key)) {
            final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
            if ( editor != null ) {
                editor.putInt(encrypt(key), value);
                editor.apply();
            }
        }
    }

    /**
     * Set float value for a key.
     *
     * @param key   The string resource Id of the key
     * @param value The value to set for the key
     */
    public static void setFloat(String key, float value) {
        final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
        editor.putFloat(encrypt(key), value);
        editor.apply();
    }

    /**
     * Set long value for key.
     *
     * @param key   The string resource Id of the key
     * @param value The value to set for the key
     */
    public static void setLong(String key, long value) {
        final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
        editor.putLong(encrypt(key), value);
        editor.apply();
    }

    /**
     * Set boolean value for key.
     *
     * @param key   The string resource Id of the key
     * @param value The value to set for the key
     */
    public static void setBoolean(String key, boolean value) {
        if (getDefaultSharedPref() != null && !Util.isEmptyOrNull(key) ) {
            final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
            if ( editor != null ) {
                editor.putBoolean(encrypt(key), value);
                editor.apply();
            }
        }
    }

    /**
     * Clear all preferences.
     *
     */
    public static void clearPreferences() {
        final SharedPreferences.Editor editor = getDefaultSharedPref().edit();
        editor.clear();
        editor.apply();
    }

    private static SharedPreferences sharedPreferences ;
    private static SharedPreferences getDefaultSharedPref() {
        if ( sharedPreferences == null ){
            sharedPreferences = LoginSdk.getDefaultSharedPref();
        }
        return sharedPreferences;
    }

}