# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Amit\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class android.support.** { *; }
-keep interface android.support.** { *; }

-keep class com.flurry.** { *; }
-dontwarn com.flurry.**

-dontwarn rx.**
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-dontwarn retrofit.appengine.UrlFetchClient
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-keepattributes Signature
-keepattributes *Annotation*


-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

########--------Retrofit + RxJava--------#########

-dontwarn sun.misc.Unsafe

-dontwarn com.octo.android.robospice.retrofit.RetrofitJackson**

-keepattributes Exceptions

-keep class com.google.gson.** { *; }

-keep class com.google.inject.** { *; }

-keep class org.apache.http.** { *; }

-keep class org.apache.james.mime4j.** { *; }

-keep class javax.inject.** { *; }

-dontwarn org.apache.http.**

-dontwarn android.net.http.AndroidHttpClient

-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
   long producerNode;
   long consumerNode;
}
-keep class gk.gkcurrentaffairs.bean.** { *; }
-keep class gk.gkcurrentaffairs.config.** { *; }
-keep class gk.gkcurrentaffairs.** { *; }


#######################################

-dontnote android.net.http.*
-dontnote org.apache.http.params.*
-dontnote org.apache.http.conn.*


########################################


-keep class com.google.ads.mediation.admob.AdMobAdapter {
  *;
}

-keep class com.google.ads.mediation.AdUrlAdapter {
  *;
}

-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.**


###########################################

-dontwarn com.itextpdf.**
-keep class com.itextpdf.** { *; }


-keep class com.artifex.mupdfdemo.** {*;}

#########################################
# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn javax.annotation.**
# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

#########################################
# PayTM

-keepclassmembers class com.paytm.pgsdk.PaytmWebView$PaytmJavaScriptInterface {public *;}


#-keep com.firebase.ui.auth.**
#-dontwarn com.firebase.ui.auth.**

-dontwarn okhttp3.internal.platform.*