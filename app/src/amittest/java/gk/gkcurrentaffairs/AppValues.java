package gk.gkcurrentaffairs;

import gk.gkcurrentaffairs.util.DbHelper;

/**
 * Created by Amit on 3/1/2017.
 */

public interface AppValues {

    int[] CATEGORY_EXIST = {
            DbHelper.INT_TRUE,
            DbHelper.INT_FALSE,
            DbHelper.INT_FALSE,
            DbHelper.INT_FALSE
    };

    int[] NOTES_CAT_ID = { 580 , 596 , 481 , 520 };
    int[] MOCK_CAT_ID = { 2 , 6 , 482 , 521 };

    int MOCK_ID = 218 ;
    int CAQ_ID = 16 ;
    int CAA_ID = 3 ;
    int PRE_ASK_QUE_ID = 473 ;
    int GOVT_JOBS_ID = 17 ;
    int IMP_NOTES_ID = 41 ;

    String HOSTNAME = "http://35.154.88.232/v3/android/hindi/" ;
    String HOSTNAME_2 = "http://52.207.220.99/admin2/public/v1/android/translator/" ;


}
