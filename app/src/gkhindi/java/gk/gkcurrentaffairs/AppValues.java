package gk.gkcurrentaffairs;

import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_ARTICLE;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_CALENDER;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_CATEGORY;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK_ARTICLE;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK_ARTICLE_ONLY;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_NCERT_CLASS;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_NEWS;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_PDF;
import static gk.gkcurrentaffairs.util.DbHelper.INT_FALSE;
import static gk.gkcurrentaffairs.util.DbHelper.INT_TRUE;

/**
 * Created by Amit on 3/1/2017.
 */

public interface AppValues {

    String[] dailyBoosterTitleArray = {"Mock Test" ,"Current Affairs Articles" ,"Current Affairs Quiz" ,"Govt Jobs" ,"Editorial"
            ,"Important Notes" };
    String[] examTargetQueTitleArray = {"SSC" ,"Bank" ,"RRB", "View All" };
    String[] examMasterTitleArray = { "GK" , "English" ,"Aptitude" , "Reasoning" , "Computer" , "Banking" };
    String[] ncertTitleArray = {"NCERT Books" , "NCERT Solutions" , "NCERT MCQ" };
    String[] easyLearningTitleArray = { "GK Tricks" , "Math Tricks" , "English Tips" , "Previously Asked Questions" , "News" , "PDF Section"};
    String[] stateTitleArray = {"State GK"};

    int[] dailyBoosterIdArray ={ 218 , 3 , 16 , 17 , 553 , 41 };
    int[] examTargetQueIdArray = { 658 , 659 , 660 , 657 };
    int[] examMasterIdArray = { 0 , 0 , 480 , 519 , 0 , 0 } ;
    int[] ncertIdArray = { 9999 , 9998 , 1972 };
    int[] easyLearningIdArray = { 40 , 56 , 616 , 473 , 0 , 617 };
    int[] stateIdArray = { 737 };

    int[] dailyBoosterImageArray ={ R.drawable.mock_test , R.drawable.ca_articles , R.drawable.current_affairs_quiz
            , R.drawable.govt_jobs , R.drawable.editorial , R.drawable.important_notes };
    int[] examTargetQueImageArray = { R.drawable.ssc_h , R.drawable.bank , R.drawable.railway_rrb , R.drawable.plus_blue };
    int[] examMasterImageArray = { R.drawable.gk , R.drawable.english , R.drawable.aptitude , R.drawable.reasoning
            , R.drawable.ic_computer , R.drawable.ic_banking } ;
    int[] ncertImageArray = { R.drawable.ncert_book , R.drawable.ncert_solution , R.drawable.mock_test };
    int[] easyLearningImageArray = { R.drawable.gk_tricks , R.drawable.maths_tricks , R.drawable.english_tricks , R.drawable.previous_asked_question
            , R.drawable.news , R.drawable.pdf_section };
    int[] stateImageArray = { R.drawable.state_gk };

    int[] dailyBoosterTypeArray ={ CATEGORY_TYPE_MOCK , CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_CALENDER
            , CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_ARTICLE };
    int[] examTargetQueTypeArray = { CATEGORY_TYPE_MOCK , CATEGORY_TYPE_MOCK , CATEGORY_TYPE_MOCK , CATEGORY_TYPE_CATEGORY };
    int[] examMasterTypeArray = { CATEGORY_TYPE_MOCK_ARTICLE , CATEGORY_TYPE_MOCK_ARTICLE , CATEGORY_TYPE_MOCK_ARTICLE
            , CATEGORY_TYPE_MOCK_ARTICLE , CATEGORY_TYPE_MOCK_ARTICLE_ONLY , CATEGORY_TYPE_MOCK_ARTICLE_ONLY } ;
    int[] ncertTypeArray = { CATEGORY_TYPE_NCERT_CLASS , CATEGORY_TYPE_NCERT_CLASS , CATEGORY_TYPE_CATEGORY };
    int[] easyLearningTypeArray = { CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_PDF , CATEGORY_TYPE_NEWS , CATEGORY_TYPE_PDF };
    int[] stateTypeArray = { CATEGORY_TYPE_CATEGORY };

    boolean[] dailyBoosterCatExistArray ={ true , true , false , true , true , true };
    boolean[] examTargetQueCatExistArray = { true , true , true , true };
    boolean[] examMasterCatExistArray = { true , true , true , true , false , false  } ;
    boolean[] ncertCatExistArray = { false , false , true };
    boolean[] easyLearningCatExistArray = { false , false , false , true , false , true };
    boolean[] stateCatExistArray = { false };

    boolean[] dailyBoosterWebViewArray ={ true , false , false , true , false , true };
    boolean[] examTargetQueWebViewArray = { true , true , true , false };
    boolean[] examMasterWebViewArray = { true , true , true , true , true , true } ;
    boolean[] ncertWebViewArray = { false , false , false };
    boolean[] easyLearningWebViewArray = { true , true , true , true , false , true };
//    boolean[] easyLearningWebViewArray = { false , false , false , true , false , true };
    boolean[] stateWebViewArray = { true };


    int[] CATEGORY_EXIST = {
            INT_TRUE,
            INT_FALSE,
            INT_FALSE,
            INT_FALSE
    };


    int[] NOTES_CAT_ID = { 580 , 596 , 481 , 520 , 588 , 589 };
    int[] MOCK_CAT_ID = { 2 , 5274 , 482 , 521 , 69 , 59 };
//    int[] MOCK_CAT_ID = { 2 , 6 , 482 , 521 , 69 , 59 };

    int SSC_ID = 658 ;
    int BANK_ID = 659 ;
    int RRB_ID = 660 ;
    int MOCK_ID = 218 ;
    int CAQ_ID = 16 ;
    int CAA_ID = 3 ;
    int PRE_ASK_QUE_ID = 473 ;
    int GOVT_JOBS_ID = 17 ;
    int IMP_NOTES_ID = 41 ;
    int PDF_SEC_ID = 617 ;
    int ID_IMP_UPDATES = 731 ;
    int ID_EDITORIAL = 553 ; ;

    String APP_COLOR = "#3F51B5";
//    String HOSTNAME = "http://topcoaching.in:8081/v3/android/hindi/" ;
    String HOSTNAME = "https://topcoaching.in/v3/android/hindi/" ;
    String HOSTNAME_2 = "http://52.207.220.99/admin2/public/v1/android/translator/" ;
    String HOSTNAME_3 = "http://172.104.52.148/api/v7/" ;

    String HOST_PAYMENT = "https://topcoaching.in/api/" ;
//    String HOST_PAYMENT = "http://topcoaching.in:8081/api/" ;
}
