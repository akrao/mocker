package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

import gk.gkcurrentaffairs.R;


/**
 * Created by amit on 23/4/17.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private String[] newslist;
    private Context context;


    public DataAdapter(Context context, String[] newslist) {
        this.newslist = newslist;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view =LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_classes, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.tv_class_circle_text.setText(replaceText(newslist[i]));
        viewHolder.tv_class_text.setText(newslist[i]);
        ((GradientDrawable)viewHolder.ll_circle_color.getBackground()).setColor(Color.parseColor( colors[ getRandomNum() ] ));
    }

    @Override
    public int getItemCount() {
        return newslist.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_class_circle_text,tv_class_text;
        private LinearLayout ll_circle_color;
        public ViewHolder(View view) {
            super(view);
            tv_class_circle_text = (TextView)view.findViewById(R.id.tv_class_circle_text);
            tv_class_text = (TextView)view.findViewById(R.id.tv_class_text);
            ll_circle_color = (LinearLayout) view.findViewById(R.id.ll_circle_color);
        }
    }

    private int getRandomNum(){
        Random r = new Random();
        return r.nextInt(7);
    }

    private String[] colors = {
            "#13c4a5","#10a4b8","#8a63b3","#3b5295","#fdbd57","#f6624e","#e7486b","#9c4274"};

    private String replaceText(String textreplace){
        return textreplace.toString().replace("Class ", "");
    }

}