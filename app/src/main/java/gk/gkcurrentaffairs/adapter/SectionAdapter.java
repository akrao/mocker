package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.Child;
import gk.gkcurrentaffairs.payment.model.PaidSlidersBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/5/2017.
 */

public class SectionAdapter extends NativeAdsAdapter {

    private Context context ;
    private List<Child> children ;
    private String imagePath ;
    List<PaidSlidersBean> slidersBeanList ;
    private final boolean isSlider ;
    private final int count ;
    private boolean isActive = true , isSubscribe ;

    public SectionAdapter(Context context, List<Child> children , String imagePath , List<PaidSlidersBean> slidersBeanList  ) {
        super(children , R.layout.ads_native_unified_card , null );
        this.context = context;
        this.children = children;
        this.imagePath = imagePath ;
        this.slidersBeanList = slidersBeanList ;
        isSlider = slidersBeanList != null && slidersBeanList.size() > 0 ;
        count = isSlider ? children.size() + 1 : children.size() ;
    }

    public boolean isSubscribe() {
        return isSubscribe;
    }

    public void setSubscribe(boolean subscribe) {
        isSubscribe = subscribe;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_section, parent, false);
        return new ViewHolder( view );
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if ( viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            if ( isSlider ){
                if ( position == 0 ){
                    holder.tvTitle.setVisibility( View.GONE );
                    holder.viewHeader.setVisibility( View.GONE );
                    holder.recyclerView.setAdapter( new PaidSliderAdapter( context , slidersBeanList , imagePath ));
                    maxCount = slidersBeanList.size() ;
                    startAutoScroll( holder.recyclerView );
                    return;
                }
                position--;
            }
            holder.position = position ;
            Child child = children.get( position );
            if ( child != null && child.getPackages() != null && child.getPackages().size() > 0 ){
                holder.viewHeader.setVisibility( View.VISIBLE );
                holder.tvTitle.setText( child.getTitle() );
                PackageAdapter adapter = new PackageAdapter( context , child.getPackages() , imagePath , true , isSubscribe ) ;
                holder.recyclerView.setAdapter( adapter );
            }
        }
    }

    int countPos = 0;
    int maxCount ;

    private void startAutoScroll(final RecyclerView recyclerView ){
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
//                Logger.e("startAutoScroll -- run");
                if ( recyclerView != null && isActive ) {
//                    Logger.e("startAutoScroll -- run before -- " + countPos);
                    countPos++ ;
                    if ( countPos >= maxCount ){
                        countPos = 0 ;
                    }
//                    Logger.e("startAutoScroll -- run after -- " + countPos);
                    recyclerView.smoothScrollToPosition(countPos);
                    handler.postDelayed(this,speedScroll);
                }
           }
        };
        handler.postDelayed(runnable,speedScroll);

    }

    final int speedScroll = AppConstant.SLIDER_TIME;
    final Handler handler = new Handler();

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        RecyclerView recyclerView ;
        TextView tvTitle , tvViewAll ;
        View viewHeader ;

        @Override
        public void onClick(View v) {
            if ( v.getId() == R.id.tv_view_all ){
                SupportUtil.openExamDetail(context , children.get(position).getId() , false);
            }
        }

        public ViewHolder(View itemView ) {
            super(itemView);
            recyclerView = ( RecyclerView ) itemView.findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager( new LinearLayoutManager( context , LinearLayoutManager.HORIZONTAL , false ) );
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvViewAll = ( TextView ) itemView.findViewById( R.id.tv_view_all );
            viewHeader = itemView.findViewById( R.id.ll_header );
            tvViewAll.setOnClickListener( this );
        }
    }
}
