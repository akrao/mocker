package gk.gkcurrentaffairs.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.NotificationListBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/29/2018.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NotificationListBean> children ;
    private OnCustomClick onCustomClick ;

    public NotificationListAdapter(List<NotificationListBean> children , OnCustomClick onCustomClick ) {
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position , boolean isDelete);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_list, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if ( holder1 instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) holder1 ;
            holder.position = position ;
            NotificationListBean child = children.get( position );
            if ( child != null ){
                holder.tvTitle.setText( child.getTitle() );
                holder.tvDate.setText( child.getDate() );
                holder.vIndicator.setBackgroundColor( SupportUtil.getColor(
                        ( child.isClicked() ? R.color.green_mcq_paid : R.color.wrong_red  ) ,
                        AppApplication.getInstance()) );
            }
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle , tvDate ;
        View vIndicator ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position , v.getId() == R.id.adp_iv_delete );
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvDate = ( TextView ) itemView.findViewById( R.id.tv_date );
            vIndicator = itemView.findViewById( R.id.adp_v_indicator );
            itemView.findViewById( R.id.adp_iv_delete ).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
    }

}
