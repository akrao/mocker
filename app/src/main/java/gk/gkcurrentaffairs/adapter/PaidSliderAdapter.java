package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.PackageActivity;
import gk.gkcurrentaffairs.payment.model.PaidSlidersBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/14/2018.
 */

public class PaidSliderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context context ;
    private List<PaidSlidersBean> children ;
    private String imagePath ;

    public PaidSliderAdapter(Context context, List<PaidSlidersBean> children, String imagePath) {
        this.context = context;
        this.children = children;
        this.imagePath = imagePath;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_slider, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if ( holder1 instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) holder1 ;
            holder.position = position ;
            PaidSlidersBean child = children.get( position );
            holder.tvCount.setText( (position+1) +"/" + children.size() );
            if ( child != null ){
                SupportUtil.loadImage( holder.ivProfile , child.getImage() , imagePath );
            }
        }

    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        ImageView ivProfile ;
        TextView tvCount ;

        @Override
        public void onClick(View v) {
            if ( children != null && children.size() > position ) {
                switch (children.get(position).getPageType()){
                    case Constants.SLIDER_PACKAGE :
                        openPackage( children.get(position).getPageId() );
                        break;
                    case Constants.SLIDER_SUBSCRIPTION_PLAN_ALL :
                        SupportUtil.openSubscriptionPlan(context);
                        break;
                    case Constants.SLIDER_SUBSCRIPTION_PLAN_SINGLE :
                        SupportUtil.openSubscriptionPlanDesc(context ,
                                children.get(position).getAndroidAppId() ,
                                children.get(position).getPageId() );
                        break;
                    case Constants.SLIDER_EXAM_PAGE :
                        SupportUtil.openExamDetail(context ,
                                children.get(position).getPageId() , true );
                        break;
                    case Constants.SLIDER_PLAY_STORE :
                        SupportUtil.openAppInPlayStore(context, children.get(position).getAndroidAppId());
                        break;
                    case Constants.SLIDER_WEB_URL :
                        SupportUtil.openLinkInWebView(context, children.get(position).getWebUrl() , children.get(position).getAndroidAppId());
                        break;
                }
            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ivProfile = ( ImageView ) itemView.findViewById( R.id.iv_profile );
            tvCount = ( TextView ) itemView.findViewById( R.id.tv_count );
            itemView.setOnClickListener(this);
        }
    }

    private void openPackage(int packageId){
        Intent intent = new Intent( context , PackageActivity.class );
        intent.putExtra(AppConstant.IMAGE , imagePath);
        intent.putExtra( AppConstant.CAT_ID , packageId );
        context.startActivity(intent);
    }

}
