package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.payment.model.PaidSlidersBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_DB;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_EL;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_EM;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_ETQ;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_NCERT;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_SGK;
import static gk.gkcurrentaffairs.adapter.HomeListAdapter.TYPE.TYPE_SLIDER;
import static gk.gkcurrentaffairs.util.AppConstant.HOME_TITLE_ARRAY;

/**
 * Created by Amit on 5/31/2018.
 */

public class HomeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> children ;
    private OnCustomClick onCustomClick ;
    private HashMap<String , List<CategoryProperty>> listHashMap ;
    private Context context ;
    List<PaidSlidersBean> slidersBeanList ;
    private boolean isActive = true ;
    private String imagePath ;
    public void setActive(boolean active) {
        isActive = active;
    }

    public HomeListAdapter(List<String> children , OnCustomClick onCustomClick , Context context) {
        this.children = children;
        this.onCustomClick = onCustomClick;
        this.context = context ;
    }

    public void setListHashMap(HashMap<String, List<CategoryProperty>> listHashMap) {
        this.listHashMap = listHashMap;
    }

    public void setSlidersBeanList(List<PaidSlidersBean> slidersBeanList) {
        this.slidersBeanList = slidersBeanList;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public interface OnCustomClick{
        void onCustomClick(int positionParent ,int position , boolean isChangePosition );
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null ;
        View view ;
        switch ( viewType ){
            case TYPE_SLIDER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_section, parent, false);
                viewHolder = new SliderViewHolder( view );
                break;
            case TYPE_DB:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_daily_booster, parent, false);
                viewHolder = new CategoryViewHolder( view , listHashMap.get(HOME_TITLE_ARRAY[1]).size() , "db" );
                break;
            case TYPE_ETQ:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_exam_target_que, parent, false);
                viewHolder = new CategoryViewHolder( view , listHashMap.get(HOME_TITLE_ARRAY[2]).size() , "etq" );
                break;
            case TYPE_EM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_exam_master, parent, false);
                viewHolder = new CategoryViewHolder( view , listHashMap.get(HOME_TITLE_ARRAY[3]).size() , "em" );
                break;
            case TYPE_NCERT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ncert, parent, false);
                viewHolder = new CategoryViewHolder( view , listHashMap.get(HOME_TITLE_ARRAY[4]).size() , "ncert" );
                break;
            case TYPE_EL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_easy_learning, parent, false);
                viewHolder = new CategoryViewHolder( view , listHashMap.get(HOME_TITLE_ARRAY[5]).size() , "el" );
                break;
            case TYPE_SGK:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_state_gk, parent, false);
                viewHolder = new CategoryViewHolder( view , 6 , "sgk" );
                break;
        }
        return viewHolder ;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if ( holder1 instanceof SliderViewHolder ){
            final SliderViewHolder holder = (SliderViewHolder) holder1 ;
            holder.tvTitle.setVisibility( View.GONE );
            holder.viewHeader.setVisibility( View.GONE );
            if ( slidersBeanList != null && slidersBeanList.size() > 0 ) {
                holder.recyclerView.setAdapter( new PaidSliderAdapter( context , slidersBeanList , imagePath ));
                maxCount = slidersBeanList.size() ;
                if ( slidersBeanList.size() > 0 ) {
                    startAutoScroll( holder.recyclerView );
                }
            }
        }else if ( holder1 instanceof CategoryViewHolder ){
            CategoryViewHolder holder = (CategoryViewHolder) holder1 ;
            holder.positionParent = position ;
            List<CategoryProperty> list = listHashMap.get(children.get(position)) ;
            holder.tvTitleCat.setText(children.get(position));
            if( list != null && list.size() > 0 ){
                for ( int i = 0 ; i < list.size() ; i++ ){
                    holder.tvTitle[i].setText( list.get(i).getTitle() );
                    if (SupportUtil.isEmptyOrNull( list.get(i).getImageUrl() ) & list.get(i).getImageResId() != 0 ) {
                        holder.ivTitle[i].setImageResource(list.get(i).getImageResId());
                    }else if (!SupportUtil.isEmptyOrNull( list.get(i).getImageUrl() )) {
                        AppApplication.getInstance().getPicasso().load(list.get(i).getImageUrl())
                                .placeholder(R.drawable.place_holder_cat).into(holder.ivTitle[i]);
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    interface TYPE {
        int TYPE_SLIDER = 0 ;
        int TYPE_DB = 1 ;
        int TYPE_ETQ = 2 ;
        int TYPE_EM = 3 ;
        int TYPE_NCERT = 4 ;
        int TYPE_EL = 5 ;
        int TYPE_SGK = 6 ;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
    class CategoryViewHolder extends ViewHolder implements View.OnClickListener{
        int positionParent ;
        TextView[] tvTitle ;
        ImageView[] ivTitle ;
        TextView tvTitleCat ;
        String pre ;

        @Override
        public void onClick(View v) {
            if ( v.getTag() != null ) {
                boolean b = v.getId() == R.id.db_tv_change_position;
                int positionChild = 0 ;
                String s = null;
                if ( !b ) {
                    s = v.getTag().toString();
                    if ( !SupportUtil.isEmptyOrNull(s)) {
                        s = s.replaceAll(pre + "_ll_" , "");
                        positionChild = (Integer.parseInt(s) - 1) ;
                    }
                }
                onCustomClick.onCustomClick(positionParent , positionChild , b );
            }
        }

        public CategoryViewHolder(View itemView , int maxCount , String pre) {
            super(itemView);
            this.pre = pre ;
            tvTitle = new TextView[maxCount];
            ivTitle = new ImageView[maxCount];
            tvTitleCat = (TextView) itemView.findViewWithTag(pre + "_title" );
            itemView.findViewById(R.id.db_tv_change_position).setOnClickListener(this);
            for (int i = 1; i <= maxCount; i++) {
                tvTitle[i-1] = (TextView) itemView.findViewWithTag(pre + "_tv_" + i);
                ivTitle[i-1] = (ImageView) itemView.findViewWithTag(pre+ "_iv_" + i);
                if ( itemView.findViewWithTag(pre + "_ll_"+ i) != null )
                    itemView.findViewWithTag(pre + "_ll_"+ i).setOnClickListener(this);
            }
        }
    }

    class SliderViewHolder extends ViewHolder {
        int position ;
        RecyclerView recyclerView ;
        TextView tvTitle , tvViewAll ;
        View viewHeader ;

        public SliderViewHolder(View itemView ) {
            super(itemView);
            recyclerView = ( RecyclerView ) itemView.findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager( new LinearLayoutManager( context , LinearLayoutManager.HORIZONTAL , false ) );
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvViewAll = ( TextView ) itemView.findViewById( R.id.tv_view_all );
            viewHeader = itemView.findViewById( R.id.ll_header );
        }
    }

    int countPos = 0;
    int maxCount ;

    private void startAutoScroll(final RecyclerView recyclerView ){
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
//                Logger.e("startAutoScroll -- run");
                if ( recyclerView != null && isActive ) {
//                    Logger.e("startAutoScroll -- run before -- " + countPos);
                    countPos++ ;
                    if ( countPos > maxCount ){
                        countPos = 0 ;
                    }
//                    Logger.e("startAutoScroll -- run after -- " + countPos);
                    recyclerView.smoothScrollToPosition(countPos);
                    handler.postDelayed(this,speedScroll);
                }
            }
        };

        handler.postDelayed(runnable,speedScroll);

    }
    final int speedScroll = AppConstant.SLIDER_TIME;
    final Handler handler = new Handler();

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        int type = TYPE.TYPE_SLIDER ;
        if ( position != 0 ) {
            switch ( children.get(position) ){
                case "Slider":
                    type = TYPE_SLIDER ;
                    break;
                case "Daily Booster":
                    type = TYPE_DB ;
                    break;
                case "Exam Target Questions":
                    type = TYPE_ETQ ;
                    break;
                case "Exam Master":
                    type = TYPE_EM ;
                    break;
                case "NCERT":
                    type = TYPE_NCERT ;
                    break;
                case "Easy Learning":
                    type = TYPE_EL ;
                    break;
                case "State GK":
                    type = TYPE_SGK ;
                    break;
            }
        }
        return type ;
    }

}
