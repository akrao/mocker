package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;
import java.util.Random;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.activity.PackageActivity;
import gk.gkcurrentaffairs.payment.model.Package;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/5/2017.
 */

public class PackageAdapter extends NativeAdsAdapter {

    private Context context;
    private List<Package> children;
    private String imagePath;
    private int layoutRes;
    private boolean isSubscribe;

    public PackageAdapter(Context context, List<Package> children, String imagePath, boolean isHorizontal, boolean isSubscribe) {
        super(children , R.layout.ads_native_unified_card , null );
        this.context = context;
        this.children = children;
        this.imagePath = imagePath;
        this.isSubscribe = isSubscribe;
        layoutRes = isHorizontal ? R.layout.adapter_package : R.layout.adapter_package_vertical;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new PackageAdapter.ViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if ( viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position;
            Package child = children.get(position);
            if (child != null) {
//                SupportUtil.loadImage(holder.ivProfile, child.getImage(), imagePath,R.drawable.place_holder_package);
//                holder.tvTitle.setText(child.getTitle());
    //            holder.tvPrice.setText( "Rs. " + child.getPrice() + "/-" );
                if (!SupportUtil.isEmptyOrNull(child.getTitle())) {
                    holder.tvHeadTitle.setText(child.getTitle());
                    holder.tvHeadTitle.setBackground(getGradDrawable());
                }
                if (!SupportUtil.isEmptyOrNull(child.getShortDescription())) {
                    holder.tvDesc.setText(Html.fromHtml(removePadding(child.getShortDescription())));
                }
            }
//            holder.viewSubscribe.setVisibility(PaidHomeActivity.getInstance().isSubscribed() ? View.GONE : View.VISIBLE);
        }
    }

    private String removePadding(String s) {
        if (s != null) {
            if (s.contains("<p>")) {
                s = s.replaceAll("<p>", "");
                s = s.replaceAll("</p>", "");

            }
        }
        return s ;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
//        TextView tvTitle, tvPrice;
        TextView tvHeadTitle , tvDesc;
//        ImageView ivProfile;
//        View viewSubscribe;

        @Override
        public void onClick(View v) {
//            if (v.getId() == R.id.adp_bt_subscribe) {
//                SupportUtil.openSubscriptionPlan(context);
//            } else {
                Intent intent = new Intent(context, PackageActivity.class);
                intent.putExtra(AppConstant.IMAGE, imagePath);
                intent.putExtra(AppConstant.DATA, children.get(position));
                context.startActivity(intent);
//            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
//            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
//            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            tvHeadTitle = (TextView) itemView.findViewById(R.id.tv_title_head);
            tvDesc = (TextView) itemView.findViewById(R.id.tv_desc);
//            ivProfile = (ImageView) itemView.findViewById(R.id.iv_profile);
//            viewSubscribe = itemView.findViewById(R.id.adp_bt_subscribe);
//            itemView.findViewById(R.id.adp_bt_subscribe).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
    }
    private Drawable getGradDrawable(){
        int position = getRandomNumberInRange(0,7);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[] {
                        Color.parseColor(colorPrimary[position])
                        ,Color.parseColor(colorSecondary[position])
                });
        gd.setCornerRadius(0f);
        return gd ;
    }
    private int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    private String[] colorPrimary = {
            "#8E2DE2" , "#396afc" , "#C33764" , "#43C6AC" , "#71B280" , "#c94b4b" , "#C33764" , "#44A08D"
    };
    private String[] colorSecondary = {
            "#4A00E0" , "#2948ff" , "#1D2671" , "#191654" , "#134E5E" , "#4b134f" , "#1D2671" , "#093637" ,
    };

}

