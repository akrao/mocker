package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/18/2017.
 */

public class MCQQueAdapter extends PagerAdapter implements View.OnTouchListener {

    private ArrayList<MockTestBean> data;
    private LayoutInflater inflater;
    private OnCustomOnTouch onCustomOnTouch;
//    private HashMap<Integer, Integer> dataAns;
    private ArrayList<Integer> dataAns;
    private ArrayList<String> dataSelect ;
    private ViewPager viewPager;
    private HashMap<Integer , Integer> webViewHashMap ;
    private String color ;
    private boolean isSolution = false ;

    public boolean isSolution() {
        return isSolution;
    }

    public void setSolution(boolean solution) {
        isSolution = solution;
    }

    //    public void setDataAns(HashMap<Integer, Integer> dataAns) {
    public void setDataAns(ArrayList<Integer> dataAns) {
        this.dataAns = dataAns;
    }

    public ArrayList<String> getDataSelect() {
        return dataSelect;
    }

    public void setDataSelect(ArrayList<String> dataSelect) {
        this.dataSelect = dataSelect;
    }

    public MCQQueAdapter(ArrayList<MockTestBean> data, Context context, OnCustomOnTouch onCustomOnTouch) {
        this.data = data;
        this.onCustomOnTouch = onCustomOnTouch;
        inflater = LayoutInflater.from(context);
        webViewHashMap = new HashMap<>(data.size());
        color = AppValues.APP_COLOR ;
        colorGreen = SupportUtil.getColor(R.color.green_mcq_paid , context);
        colorRed = SupportUtil.getColor(R.color.wrong_red , context);
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    public interface OnCustomOnTouch {
//        void onCustomOnTouch(View view, boolean flag, HashMap<Integer, Integer> dataAns);
        void onCustomOnTouch(View view, boolean flag, ArrayList<Integer> dataAns);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.item_pager_mcq, container, false);
        view.setTag(position);
        container.addView(view);
        initViews(view, position);
        return view;
    }

    private void initViews(View view, int count) {
        WebView wvQue = (WebView) view.findViewById(R.id.wv_que);
        WebView wvOne = (WebView) view.findViewById(R.id.wv_ans_one);
        WebView wvTwo = (WebView) view.findViewById(R.id.wv_ans_two);
        WebView wvThree = (WebView) view.findViewById(R.id.wv_ans_three);
        WebView wvFour = (WebView) view.findViewById(R.id.wv_ans_four);
        WebView wvFive = (WebView) view.findViewById(R.id.wv_ans_five);
        if ( !isSolution) {
            wvOne.setOnTouchListener(this);
            wvTwo.setOnTouchListener(this);
            wvThree.setOnTouchListener(this);
            wvFour.setOnTouchListener(this);
            wvFive.setOnTouchListener(this);
        }
        setDataWebView(wvQue, "<b>Que : </b>" + data.get(count).getOptionQuestion(), true);
        setDataWebView(wvOne, "A : " + data.get(count).getOptionA(), false);
        setDataWebView(wvTwo, "B : " + data.get(count).getOptionB(), false);
        setDataWebView(wvThree, "C : " + data.get(count).getOptionC(), false);
        setDataWebView(wvFour, "D : " + data.get(count).getOptionD(), false);
        if ( !SupportUtil.isEmptyOrNull(data.get(count).getOptionE()) ) {
            wvFive.setVisibility(View.VISIBLE);
            setDataWebView(wvFive, "E : " + data.get(count).getOptionE(), false);
        }
        handleAns(count , view );
    }

    private int colorGreen , colorRed ;
    private void handleAns(int position, View view) {
        if ( isSolution ) {
            int id = 0;
            if ( !SupportUtil.isEmptyOrNull(dataSelect.get(position)) ) {
                id = getWebViewId( dataSelect.get(position).toUpperCase() );
                if (id != 0 && viewPager.findViewWithTag( position ).findViewById( id ) != null ) {
                    viewPager.findViewWithTag( position ).findViewById( id ).setBackgroundColor( colorRed );
                }
            }
            if ( !SupportUtil.isEmptyOrNull(data.get(position).getOptionAns()) ) {
                id = getWebViewId(data.get(position).getOptionAns().toUpperCase());
                if (id != 0 && viewPager.findViewWithTag(position).findViewById(id) != null) {
                    viewPager.findViewWithTag(position).findViewById(id).setBackgroundColor(colorGreen);
                }
            }
        } else if (dataAns.get(position) != AppConstant.QUE_NOT_ATTEMPTED) {
            view.findViewById(webViewHashMap.get(position)).setBackgroundColor( dataAns.get(position) == AppConstant.QUE_ATTEMPTED_CORRECT ? colorGreen : colorRed );
        }
    }

    public void setDataWebView(WebView webView, String data, boolean isQue) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("http://localhost", isQue ? htmlData(data, "#FFF; background-color: " + color ) : htmlData(data, "#000"), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: " + color + "; font-size:large; }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            int position = viewPager.getCurrentItem() ;
            if (dataAns.get(position) == AppConstant.QUE_NOT_ATTEMPTED) {
                String ans = null;
                switch (v.getId()) {
                    case R.id.wv_ans_one:
                        ans = "A" ;
                        break;
                    case R.id.wv_ans_two:
                        ans = "B";
                        break;
                    case R.id.wv_ans_three:
                        ans = "C" ;
                        break;
                    case R.id.wv_ans_four:
                        ans = "D" ;
                        break;
                    case R.id.wv_ans_five:
                        ans = "E" ;
                        break;
                }
                webViewHashMap.put( position , v.getId() );
                if ( !SupportUtil.isEmptyOrNull( ans )) {
                    dataSelect.set( position , ans );
                    if ( ans.equalsIgnoreCase( data.get(position).getOptionAns() ) ){
//                        dataAns.put( position , AppConstant.QUE_ATTEMPTED_CORRECT );
                        dataAns.set( position , AppConstant.QUE_ATTEMPTED_CORRECT );
                        v.setBackgroundColor(colorGreen);
                    }else {
//                        dataAns.put( position , AppConstant.QUE_ATTEMPTED_WRONG );
                        dataAns.set( position , AppConstant.QUE_ATTEMPTED_WRONG );
                        v.setBackgroundColor(colorRed);
                        if ( data.get(position) != null && !SupportUtil.isEmptyOrNull(data.get(position).getOptionAns()) ) {
                            int id = getWebViewId( data.get(position).getOptionAns().toUpperCase() ) ;
                            if (id != 0 && viewPager.findViewWithTag( position ).findViewById( id ) != null ) {
                                viewPager.findViewWithTag( position ).findViewById( id ).setBackgroundColor( colorGreen );
                            }
                        }
                    }
                }
                onCustomOnTouch.onCustomOnTouch(v , true , dataAns);
            } else {
                onCustomOnTouch.onCustomOnTouch(v , false , dataAns);
            }
        }
        return false;
    }

    private int getWebViewId( String ans ){
        int id = 0 ;
        switch ( ans ){
            case "A":
                id = R.id.wv_ans_one ;
                break;
            case "B":
                id = R.id.wv_ans_two ;
                break;
            case "C":
                id = R.id.wv_ans_three ;
                break;
            case "D":
                id = R.id.wv_ans_four ;
                break;
            case "E":
                id = R.id.wv_ans_five ;
                break;
        }
        return id ;
    }
}
