package gk.gkcurrentaffairs.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;
import com.adssdk.OnCustomLoadMore;

import java.util.List;
import java.util.concurrent.Callable;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.DescActivity;
import gk.gkcurrentaffairs.bean.ImpCatListBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/29/2017.
 */

public class ImpCatListAdapter extends NativeAdsAdapter {

    private List<ImpCatListBean> children;
    private OnCustomClick onCustomClick;
    private OnLoadMore onLoadMore ;
    private int width = 0 , catId;
    private DbHelper dbHelper ;

    public ImpCatListAdapter(List<ImpCatListBean> children, OnCustomClick onCustomClick , final OnLoadMore onLoadMore) {
        super(children, R.layout.ads_native_unified_card,  new OnCustomLoadMore() {
            @Override
            public void onLoadMore() {
                if (onLoadMore != null )
                    onLoadMore.onLoadMore();
            }
        });
        this.children = children;
        this.onCustomClick = onCustomClick;
        this.onLoadMore = onLoadMore;
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public interface OnCustomClick {
        void onCustomClick(int position, int type);
    }

    public interface OnLoadMore {
        void onLoadMore();
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adap_imp_cat, parent, false);
        return new ImpCatListAdapter.ViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if ( viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position;
            ImpCatListBean child = children.get(position);
            if (child != null) {
                holder.tvTitle.setText(child.getTitle());
    //            holder.tvDesc.setText(SupportUtil.fromHtml(child.getDesc()));
                holder.tvDate.setText(child.getDate());

                if ( holder.ivFav != null ) {
                    holder.ivFav.setImageResource( child.isFav() ? R.drawable.bookmark_imp_mark: R.drawable.fav );
                }
                holder.tvTitle.setTextColor(child.isRead() ? Color.GRAY : Color.BLACK);
                holder.tvDate.setTextColor(child.isRead() ? Color.GRAY : Color.BLACK);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        TextView tvTitle, tvDate ;
        ImageView ivFav;

        @Override
        public void onClick(View v) {
            int type = AppConstant.CLICK_ARTICLE ;
            switch ( v.getId() ){
              /*  case R.id.iv_share:
                    type = AppConstant.CLICK_SHARE ;
                    break;*/
                case R.id.iv_option:
                    openMenu(position , v);
                    return;
                case R.id.iv_fav:
                    type = AppConstant.CLICK_SAVE ;
                    break;
                default:
                    openDesc(position);
                    break;
            }
            if ( onCustomClick != null ) {
                onCustomClick.onCustomClick(position,type);
            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            ivFav = (ImageView) itemView.findViewById(R.id.iv_fav);
            /*if ( itemView.findViewById(R.id.iv_share) != null ) {
                itemView.findViewById(R.id.iv_share).setOnClickListener(this);
            }*/
            if ( itemView.findViewById(R.id.iv_option) != null ) {
                itemView.findViewById(R.id.iv_option).setOnClickListener(this);
            }
            if ( ivFav != null ) {
                ivFav.setOnClickListener(this);
            }
            itemView.setOnClickListener(this);
            if ( width > 0 ){
                itemView.getLayoutParams().width = width ;
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams()
            }
        }
    }

    private Activity activity ;
    private String query ;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    private void openDesc(int position){
        Intent intent = new Intent(activity, DescActivity.class);
        intent.putExtra( AppConstant.DATA, children.get(position).getId() );
        intent.putExtra( AppConstant.QUERY, query );
        intent.putExtra( AppConstant.CAT_ID, catId );
        intent.putExtra( AppConstant.WEB_VIEW , true);
        activity.startActivity(intent);

    }


    private void openMenu(final int position , View v){
        PopupMenu menu = new PopupMenu (AppApplication.getInstance(), v);
        menu.setOnMenuItemClickListener (new PopupMenu.OnMenuItemClickListener ()
        {
            @Override
            public boolean onMenuItemClick (MenuItem item)
            {
                int id = item.getItemId();
                switch (id)
                {
                    case R.id.action_bookmark:
                        updateFavStatus(position);
                        break;
                    case R.id.action_share:
                        SupportUtil.share( children.get(position).getTitle() , AppApplication.getInstance());
                        break;
                }
                return true;
            }
        });
        menu.inflate (R.menu.menu_pop_imp_list);
        MenuItem item = menu.getMenu().findItem( R.id.action_bookmark );
        if ( item != null ){
            item.setTitle( children.get(position).isFav() ? "Remove from saved Articles." : "Save this" );
        }
        menu.show();
    }

    private void updateFavStatus(final int position) {
        final boolean status = children.get(position).isFav();
        children.get(position).setFav(!status);
        notifyDataSetChanged();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.updateArticle(children.get(position).getId(), dbHelper.COLUMN_FAV,
                        status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
                return null;
            }
        });
        SupportUtil.showToastCentre(activity , "Article Saved");
    }

}