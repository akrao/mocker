package gk.gkcurrentaffairs.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.ResultBean;

/**
 * Created by Amit on 3/3/2017.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder>{

    private ArrayList<ResultBean> resultBeen ;
    private OnCustomClick onCustomClick ;
    private String catName ;

    public interface OnCustomClick{
        void onCustomItemClick(int position , boolean isResultTrue);
    }

    public ResultAdapter(ArrayList<ResultBean> resultBeen, OnCustomClick onCustomClick , String catName ) {
        this.resultBeen = resultBeen;
        this.onCustomClick = onCustomClick;
        this.catName = catName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate( R.layout.item_result , parent, false);
        ViewHolder holder = new ArticleViewHolder( view , onCustomClick );
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if ( holder instanceof ArticleViewHolder){
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder ;
            ResultBean bean = resultBeen.get( position );
            viewHolder.position = position ;
            viewHolder.title.setText( bean.getTitle().trim() );
            viewHolder.category.setText( catName + " | " + bean.getDate());
            if ( bean.isCompleted() ){
                viewHolder.score.setText( bean.getCorrectAns() + "/" + bean.getNumberQue() );
                viewHolder.time.setText( bean.getTimeTaken() );
                viewHolder.resultData.setVisibility( View.VISIBLE );
                viewHolder.notCompleted.setVisibility( View.GONE );
            }else{
                viewHolder.resultData.setVisibility( View.GONE );
                viewHolder.notCompleted.setVisibility( View.VISIBLE );
            }
        }
    }

    @Override
    public int getItemCount() {
        return resultBeen.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener{

        TextView category , title , score , time ;
        View notCompleted , resultData ;
        int position ;
        OnCustomClick onClick ;

        public ArticleViewHolder(View itemView , OnCustomClick onClick) {
            super(itemView);
            this.onClick = onClick ;
            title = (TextView) itemView.findViewById( R.id.item_tv_title );
            category = (TextView) itemView.findViewById( R.id.item_tv_cat );
            score = (TextView) itemView.findViewById( R.id.item_tv_score );
            time = (TextView) itemView.findViewById( R.id.item_tv_time );
            notCompleted = itemView.findViewById( R.id.item_tv_not_complete );
            resultData = itemView.findViewById( R.id.ll_result_data );
            itemView.setOnClickListener( this );
            itemView.findViewById( R.id.bt_retake ).setOnClickListener( this );
        }

        @Override
        public void onClick(View view) {
            if ( view.getId() == R.id.bt_retake )
                onClick.onCustomItemClick( position , false );
            else if ( resultBeen.get( position ).isCompleted() )
                onClick.onCustomItemClick( position , true );

        }
    }

}
