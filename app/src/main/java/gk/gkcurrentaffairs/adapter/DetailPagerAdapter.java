package gk.gkcurrentaffairs.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.NativePagerAdapter;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.BrowserActivity;
import gk.gkcurrentaffairs.bean.HomeBean;
import gk.gkcurrentaffairs.bean.TranslaterBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.RetrofitGenerator;
import gk.gkcurrentaffairs.network.ApiEndpointInterface;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gk.gkcurrentaffairs.config.ConfigConstant.CONFIG_HOST_URL;

/**
 * Created by amit on 18/9/16.
 */
public class DetailPagerAdapter extends NativePagerAdapter {

    private LayoutInflater inflater;
    private ArrayList<HomeBean> articleBeans;
    private int textSize;
    private boolean isDayMode, isWebView;
    private HashMap<Integer, String> categoryNames;
    private View viewAds = null;
    private String colorWhite = "#fff", colorBlack = "#000";
    private Context context;
    private ProgressDialog progressDialog;
    private String toConvert = null;
    private Typeface typeface = null;

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }

    public void setToConvert(String toConvert) {
        this.toConvert = toConvert;
    }

    public DetailPagerAdapter(Context context, ArrayList<HomeBean> articleBeans, int textSize,HashMap<Integer, String> categoryNames) {
        super(articleBeans, R.layout.native_pager_ad_app_install, context);
        this.context = context;
        this.articleBeans = articleBeans;
        inflater = LayoutInflater.from(context);
        this.textSize = textSize;
//        categoryNames = AppData.getInstance().getCategoryNames();
        this.categoryNames = categoryNames;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Translating...");
    }

    public void setWebView(boolean webView) {
        isWebView = webView;
    }

    @Override
    protected View customInstantiateItem(ViewGroup container, int position) {
        final HomeBean bean = articleBeans.get(position);
        View view;
        view = inflater.inflate(R.layout.layout_desc_item, container, false);

        View vDetail = view.findViewById(R.id.ll_detail);
        vDetail.setBackgroundColor(isDayMode ? Color.WHITE : Color.BLACK);
        if (!isWebView) {
            view.findViewById(R.id.v_bottom).setVisibility(View.VISIBLE);
            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.full_ad);
            loadFullAd(relativeLayout);
            TextView textView = (TextView) view.findViewById(R.id.tv_desc);
            textView.setVisibility(View.VISIBLE);

            if (!SupportUtil.isEmptyOrNull(bean.getDesc()))
                setTextData(textView, bean, position);
            else
                textView.setText("Error. Please read other articles.");
        } else {
            WebView webView = (WebView) view.findViewById(R.id.wv_desc);
            webView.setVisibility(View.VISIBLE);
            setDataWebView(webView, bean.getDesc(), bean);
        }
        String cat = "";
        TextView tvCategory = (TextView) view.findViewById(R.id.item_tv_category);
        if (categoryNames != null && categoryNames.size() > 0) {
            String catName = categoryNames.get(bean.getSubCatId());
            cat = SupportUtil.isEmptyOrNull(catName) ? "" : "  |  " + catName.trim();
        }
        tvCategory.setText(bean.getDate() + cat );
//        tvCategory.setTextColor(isDayMode ? Color.DKGRAY : Color.WHITE);

        view.setTag("pager_item" + position);
        view.setTag(position);
//        container.addView(view);
        return view;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public void setDayMode(boolean dayMode) {
        isDayMode = dayMode;
    }

    private String htmlData(String myContent, String text, String bg) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">"
                + "@font-face { font-family: 'roboto_regular'; src: url('rr.ttf'); } "
                + "body{color: " + text + "; font-family:roboto_regular; background-color: " + bg + ";}img{display: inline;height: auto;max-width: 100%;}"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    private void setDataWebView(WebView webView, String data, final HomeBean bean) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.setInitialScale(AppPreferences.getZoomSize(context));
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        String s = htmlData(data, isDayMode ? colorBlack : colorWhite, isDayMode ? colorWhite : colorBlack);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                super.shouldOverrideUrlLoading(view, request);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (isUrlNotPdfType(request.getUrl().toString())) {
                        openUrl(request.getUrl().toString(), bean.getTitle());
                    } else
                        openPDF(request.getUrl().toString());
                    ;
                }
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    if (isUrlNotPdfType(url)) {
                        openUrl(url, bean.getTitle());
                    } else {
                        openPDF(url);
                    }
                }
                return true;
            }
        });

//        webView.loadDataWithBaseURL( "http://localhost", s , "text/html", "UTF-8", null );
        webView.loadDataWithBaseURL("file:///android_asset/", s, "text/html", "UTF-8", null);
    }

    private boolean isUrlNotPdfType(String url) {
        return !url.toLowerCase().endsWith(".pdf");
    }

    private void openPDF(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    private void openUrl(String url, String title) {
        Intent intent = new Intent(context, BrowserActivity.class);
        intent.putExtra(AppConstant.DATA, url);
        intent.putExtra(AppConstant.TITLE, title);
        intent.putExtra(AppConstant.TYPE, true);
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, title);
        context.startActivity(intent);

    }

    private void setTextData(TextView textView, HomeBean bean, int position) {
        try {
            try {
                if (!SupportUtil.isEmptyOrNull(bean.getDesc())) {
                    textView.setText(Html.fromHtml(bean.getDesc()));
                }
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                textView.setTextColor(isDayMode ? Color.BLACK : Color.WHITE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (typeface != null)
                textView.setTypeface(typeface);
            if (!SupportUtil.isEmptyOrNull(toConvert)) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                Spannable spans = (Spannable) textView.getText();
                BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
                try {
                    if (!SupportUtil.isEmptyOrNull(bean.getDesc())) {
                        String datatoHTMLid = Html.fromHtml(bean.getDesc()).toString();
                        iterator.setText(datatoHTMLid);
                        int start = iterator.first();
                        for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator
                                .next()) {
                            String possibleWord = datatoHTMLid.substring(start, end);
                            if (Character.isLetterOrDigit(possibleWord.charAt(0))) {
                                ClickableSpan clickSpan = getClickableSpan(possibleWord, position);
                                spans.setSpan(clickSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private ClickableSpan getClickableSpan(final String word, final int position) {
        return new ClickableSpan() {
            final String mWord;

            {
                mWord = word;
            }

            @Override
            public void onClick(View widget) {
                checkWord(mWord);
            }

            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(isDayMode ? Color.BLACK : Color.WHITE);
            }
        };
    }

    public void checkWord(final String word) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(Html.fromHtml(String.format(sen, word)))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        getTranslateWord(word);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void openMeaningDialog(String word, String trans) {
        if (!((Activity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(word + " : " + trans)
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void getTranslateWord(final String word) {
        progressDialog.show();
        Call<TranslaterBean> call = apiEndpointInterface().customUrlTranslate(
                String.format(url, AppPreferences.getKeyTrans(context), word, toConvert)
        );
        call.enqueue(new Callback<TranslaterBean>() {
            @Override
            public void onResponse(Call<TranslaterBean> call, Response<TranslaterBean> response) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (response.body() != null && response.body().getCode() == 200
                        && response.body().getText() != null && response.body().getText().size() > 0
                        && !SupportUtil.isEmptyOrNull(response.body().getText().get(0))) {
                    openMeaningDialog(word, response.body().getText().get(0));
                } else {
                    Toast.makeText(context, "Error, please try later.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TranslaterBean> call, Throwable t) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "Error, please try later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    ApiEndpointInterface apiEndpointInterface ;
    private ApiEndpointInterface apiEndpointInterface(){
        if ( apiEndpointInterface == null ) {
            if (AppApplication.getInstance().getConfigManager() != null
                    && AppApplication.getInstance().getConfigManager().getHostAlias() != null) {
                apiEndpointInterface = RetrofitGenerator.getClient(AppApplication.getInstance().getConfigManager()
                        .getHostAlias().get(ConfigConstant.HOST_MAIN)).create(ApiEndpointInterface.class);
            }else {
                apiEndpointInterface = RetrofitGenerator.getClient(CONFIG_HOST_URL).create(ApiEndpointInterface.class);
            }
        }
        return apiEndpointInterface ;
    }

    private String url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s&format=plain&options=1";

    private String sen = "आप <b>%s</b> शब्द का अर्थ पता करना चाहते हैं?";
}
