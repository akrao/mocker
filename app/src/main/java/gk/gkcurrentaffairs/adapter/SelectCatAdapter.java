package gk.gkcurrentaffairs.adapter;

/**
 * Created by Amit on 3/10/2017.
 */

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.util.SupportUtil;


/**
 * Created by Amit on 1/13/2017.
 */

public class SelectCatAdapter extends RecyclerView.Adapter<SelectCatAdapter.ViewHolder>{

    private ArrayList<CategoryBean> categoryBeen ;
    private OnCustomClick onCustomClick ;
    private boolean isMultiSelect = false ;
    private boolean[] catSel ;

    public void setMultiSelect(boolean multiSelect) {
        isMultiSelect = multiSelect;
    }

    public interface OnCustomClick{
        void onCustomItemClick(int position);
    }

    private String imageUrl ;
    public SelectCatAdapter(ArrayList<CategoryBean> categoryBeen, OnCustomClick onCustomClick , boolean[] catSel , String imageUrl ) {
        this.categoryBeen = categoryBeen;
        this.onCustomClick = onCustomClick;
        this.catSel = catSel;
        this.imageUrl = imageUrl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate( R.layout.item_select_cat , parent, false);
        ViewHolder holder = new ArticleViewHolder( view , onCustomClick );
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if ( holder instanceof ArticleViewHolder){
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder ;
            CategoryBean bean = categoryBeen.get( position );
            viewHolder.position = position ;
            viewHolder.category.setText( bean.getCategoryName().trim() );
            if (SupportUtil.isEmptyOrNull( imageUrl )) {
                viewHolder.mainImage.setImageResource(bean.getCategoryImage());
            }else {
                AppApplication.getInstance().getPicasso().load(imageUrl)
                        .placeholder(R.drawable.exam_place_holder).into(viewHolder.mainImage);
            }
//            viewHolder.mainImage.setImageResource(bean.getCategoryImage());
            if ( isMultiSelect ){
                viewHolder.checkBox.setChecked( catSel[ position ] );
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoryBeen.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener{

        TextView category ;
        ImageView mainImage ;
        CheckBox checkBox ;
        int position ;
        OnCustomClick onClick ;

        public ArticleViewHolder(View itemView , OnCustomClick onClick) {
            super(itemView);
            this.onClick = onClick ;
            category = (TextView) itemView.findViewById( R.id.item_cat_tv );
            mainImage = (ImageView) itemView.findViewById( R.id.item_cat_iv );
            checkBox = (CheckBox) itemView.findViewById( R.id.item_cat_cb );
            itemView.setOnClickListener( this );
            if ( isMultiSelect )
                checkBox.setVisibility( View.VISIBLE );
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick( position );
        }
    }
}
