package gk.gkcurrentaffairs.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.util.SupportUtil;


/**
 * Created by Amit on 1/13/2017.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder>{

    private ArrayList<CategoryBean> categoryBeen ;
    private OnCustomClick onCustomClick ;
    private boolean isTextual = false ;
    private int layoutRes ;

    public void setTextual(boolean textual) {
        isTextual = textual;
    }

    public interface OnCustomClick{
        void onCustomItemClick(int position);
    }

    public CategoryListAdapter(ArrayList<CategoryBean> categoryBeen, OnCustomClick onCustomClick , int layoutRes ) {
        this.categoryBeen = categoryBeen;
        this.onCustomClick = onCustomClick;
        this.layoutRes = layoutRes ;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate( layoutRes , parent, false);
        ViewHolder holder = new ArticleViewHolder( view , onCustomClick );
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if ( holder instanceof ArticleViewHolder){
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder ;
            CategoryBean bean = categoryBeen.get( position );
            viewHolder.position = position ;
            viewHolder.category.setText( bean.getCategoryName().trim() );
            if ( isTextual ){
                viewHolder.mainImage.setVisibility( View.GONE );
                viewHolder.tvBack.setVisibility( View.VISIBLE );
                viewHolder.tvBack.setBackgroundColor( Color.parseColor( colors[ getRandomNum() ] ) );
                viewHolder.tvBack.setText( getFilterString( bean.getCategoryName() ) );
            }else {
                if (SupportUtil.isEmptyOrNull( bean.getImageUrl() )) {
                    viewHolder.mainImage.setImageResource(bean.getCategoryImage());
                }else {
                    AppApplication.getInstance().getPicasso().load(bean.getImageUrl())
                            .placeholder(R.drawable.exam_place_holder).into(((ArticleViewHolder) holder).mainImage);
                }
            }
        }
    }

    private String getFilterString(String s ){
        String text ;
        if ( s.contains(" ") ){
            String[] arr = s.split(" ");
            text = arr[0];
        }else {
            text = s ;
        }
        s = text.charAt(0)+"";
        return s ;
    }

    @Override
    public int getItemCount() {
        return categoryBeen.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener{

        TextView category , tvBack ;
        ImageView mainImage ;
        int position ;
        OnCustomClick onClick ;

        public ArticleViewHolder(View itemView , OnCustomClick onClick) {
            super(itemView);
            this.onClick = onClick ;
            category = (TextView) itemView.findViewById( R.id.item_cat_tv );
            tvBack = ( TextView ) itemView.findViewById( R.id.tv_cat_back );
            mainImage = (ImageView) itemView.findViewById( R.id.item_cat_iv );
            itemView.setOnClickListener( this );
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick( position );
        }
    }

    private int getRandomNum(){
        Random r = new Random();
        return r.nextInt(9);
    }

    private String[] colors = {
            "#add68a","#64c195","#73bd40","#03a45e","#569834","#00864b","#417729","#006c3b","#7dcf51","#00c26e" };

}
