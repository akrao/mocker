package gk.gkcurrentaffairs.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 7/22/2018.
 */

public class MCQQueAdapterNotSwipe implements View.OnTouchListener {

    private ArrayList<MockTestBean> data;
    private LayoutInflater inflater;
    private OnCustomOnTouch onCustomOnTouch;
    //    private HashMap<Integer, Integer> dataAns;
    private ArrayList<Integer> dataAns;
    private ArrayList<String> dataSelect ;
    private HashMap<Integer , Integer> webViewHashMap ;
    private String color ;
    private boolean isSolution = false ;
    private boolean isFullDesc = false ;
    private boolean seeAnswer = true ;
    private int position ;

    public void setSeeAnswer(boolean seeAnswer) {
        this.seeAnswer = seeAnswer;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSolution() {
        return isSolution;
    }

    public void setSolution(boolean solution) {
        isSolution = solution;
    }

    public void setDataAns(ArrayList<Integer> dataAns) {
        this.dataAns = dataAns;
    }

    public ArrayList<String> getDataSelect() {
        return dataSelect;
    }

    public void setDataSelect(ArrayList<String> dataSelect) {
        this.dataSelect = dataSelect;
    }

    @SuppressLint({"UseSparseArrays", "ResourceType"})
    public MCQQueAdapterNotSwipe(ArrayList<MockTestBean> data, Context context, MCQQueAdapterNotSwipe.OnCustomOnTouch onCustomOnTouch) {
        this.data = data;
        this.onCustomOnTouch = onCustomOnTouch;
        inflater = LayoutInflater.from(context);
        webViewHashMap = new HashMap<>(data.size());
        color = context.getResources().getString( R.string.colorPrimary );
        colorSelected = SupportUtil.getColor( R.color.mcq_select , context );
        colorGreen = SupportUtil.getColor( R.color.green_mcq_paid , context );
        colorRed = SupportUtil.getColor( R.color.wrong_red , context );;
    }

    public void setFullDesc(boolean fullDesc) {
        isFullDesc = fullDesc;
    }
    public interface OnCustomOnTouch {
        //        void onCustomOnTouch(View view, boolean flag, HashMap<Integer, Integer> dataAns);
        void onCustomOnTouch(View view, boolean flag, ArrayList<Integer> dataAns);
    }

    private View view ;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void createView(){
        initViews(view);
    }

    public void loadData(){
        clearView();
//        setDataWebView(wvQue, "<b>Que : </b>" + data.get(position).getOptionQuestion(), true);
        setDataWebView(wvQue, data.get(position).getOptionQuestion(), true);
        setDataWebView(wvOne, "A : " + data.get(position).getOptionA(), false);
        setDataWebView(wvTwo, "B : " + data.get(position).getOptionB(), false);
        setDataWebView(wvThree, "C : " + data.get(position).getOptionC(), false);
        setDataWebView(wvFour, "D : " + data.get(position).getOptionD(), false);
        if ( !SupportUtil.isEmptyOrNull(data.get(position).getOptionE()) ) {
            wvFive.setVisibility(View.VISIBLE);
            setDataWebView(wvFive, "E : " + data.get(position).getOptionE(), false);
        }
        handleAns(position , view );
        if ( !isFullDesc ) {
            hideFullDesc();
        } else {
            showFullDesc();
        }
        handleDirection(position);
        if ( isSolution ){
            if ( SupportUtil.isEmptyOrNull(data.get(position).getDescription()) ){
                wvSolution.setVisibility(View.GONE);
            }else {
                wvSolution.setVisibility(View.VISIBLE);
                setDataWebView(wvSolution, "<b>Solution : </b>" + data.get(position).getDescription(), false);
            }
        }
    }

    private void clearView(){
        wvOne.setBackgroundColor(Color.WHITE);
        wvTwo.setBackgroundColor(Color.WHITE);
        wvThree.setBackgroundColor(Color.WHITE);
        wvFour.setBackgroundColor(Color.WHITE);
        wvFive.setBackgroundColor(Color.WHITE);
    }

    TextView tvDir ;
    WebView wvDir , wvQue , wvOne , wvTwo , wvThree , wvFour , wvFive , wvSolution ;
    private void initViews(View view) {
        tvDir = (TextView) view.findViewById(R.id.tv_direction);
        tvDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFullDesc = true ;
                showFullDesc();
            }
        });
        wvDir = (WebView) view.findViewById(R.id.wv_direction);
        wvQue = (WebView) view.findViewById(R.id.wv_que);
        wvOne = (WebView) view.findViewById(R.id.wv_ans_one);
        wvTwo = (WebView) view.findViewById(R.id.wv_ans_two);
        wvThree = (WebView) view.findViewById(R.id.wv_ans_three);
        wvFour = (WebView) view.findViewById(R.id.wv_ans_four);
        wvFive = (WebView) view.findViewById(R.id.wv_ans_five);
        wvSolution = (WebView) view.findViewById(R.id.wv_solution);
        wvDir.setOnTouchListener(this);
        if ( !isSolution) {
            wvOne.setOnTouchListener(this);
            wvTwo.setOnTouchListener(this);
            wvThree.setOnTouchListener(this);
            wvFour.setOnTouchListener(this);
            wvFive.setOnTouchListener(this);
        }
    }

    private void handleDirection(int count){
        if (!SupportUtil.isEmptyOrNull(data.get(count).getDirection())) {
//            if (!isFullDesc) {
                tvDir.setText(SupportUtil.fromHtml(AppApplication.getInstance().getDBObject().removePadding("Dir : "+data.get(count).getDirection())));
                makeTextViewResizable(tvDir, 2, "View More", true);
//            } else {
                setDataWebView(wvDir, "<b>Dir : </b>"+data.get(count).getDirection() + " <span style='color:#FFFF00;' >-- View Less </span>", true);
//            }
//            makeTextViewResizable(tvDir, 2, "Read More");
        } else {
            wvDir.setVisibility(View.GONE);
            tvDir.setVisibility(View.GONE);
        }

    }

    private int colorGreen , colorRed , colorSelected ;
    private void handleAns(int position, View view) {
        if ( data.size() > position && dataSelect.size() > position && dataAns.size() > position ) {
            if ( isSolution ) {
                int id = 0;
                if ( !SupportUtil.isEmptyOrNull(dataSelect.get(position)) ) {
                    id = getWebViewId( dataSelect.get(position).toUpperCase() );
                    if (id != 0 && view.findViewById( id ) != null ) {
                        view.findViewById( id ).setBackgroundColor( colorRed );
                    }
                }
                if ( !SupportUtil.isEmptyOrNull(data.get(position).getOptionAns()) ) {
                    id = getWebViewId(data.get(position).getOptionAns().toUpperCase());
                    if (id != 0 && view.findViewById(id) != null) {
                        view.findViewById(id).setBackgroundColor(colorGreen);
                    }
                }
            } else if (dataAns.get(position) != AppConstant.QUE_NOT_ATTEMPTED) {
                if (seeAnswer) {
                    if ( dataAns.get(position) == AppConstant.QUE_ATTEMPTED_CORRECT ) {
                        view.findViewById(webViewHashMap.get(position)).setBackgroundColor( colorGreen );
                    }else {
                        view.findViewById(webViewHashMap.get(position)).setBackgroundColor( colorRed );
                        if ( !SupportUtil.isEmptyOrNull(data.get(position).getOptionAns()) ) {
                            int id = getWebViewId( data.get(position).getOptionAns().toUpperCase() );
                            if (id != 0 && view.findViewById( id ) != null ) {
                                view.findViewById( id ).setBackgroundColor( colorGreen );
                            }
                        }
                    }
    //                view.findViewById(webViewHashMap.get(position)).setBackgroundColor( dataAns.get(position) == AppConstant.QUE_ATTEMPTED_CORRECT ? colorGreen : colorRed );
                } else {
                    view.findViewById(webViewHashMap.get(position)).setBackgroundColor( colorSelected );
                }
            }
        }
    }

    public void setDataWebView(WebView webView, String data, boolean isQue) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("http://localhost", isQue ? htmlData(data, "#FFF; background-color: " + color ) : htmlData(data, "#000"), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: " + color + "; font-size:large; }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (v.getId() != R.id.wv_direction) {
                if (dataAns.get(position) == AppConstant.QUE_NOT_ATTEMPTED) {
                    String ans = null;
                    switch (v.getId()) {
                        case R.id.wv_ans_one:
                            ans = "A";
                            break;
                        case R.id.wv_ans_two:
                            ans = "B";
                            break;
                        case R.id.wv_ans_three:
                            ans = "C";
                            break;
                        case R.id.wv_ans_four:
                            ans = "D";
                            break;
                        case R.id.wv_ans_five:
                            ans = "E";
                            break;
                    }
                    webViewHashMap.put(position, v.getId());
                    if (!SupportUtil.isEmptyOrNull(ans)) {
                        dataSelect.set(position, ans);
                        if (ans.equalsIgnoreCase(data.get(position).getOptionAns())) {
//                        dataAns.put( position , AppConstant.QUE_ATTEMPTED_CORRECT );
                            dataAns.set(position, AppConstant.QUE_ATTEMPTED_CORRECT);
                            if (seeAnswer) {
                                v.setBackgroundColor(colorGreen);
                            } else {
                                v.setBackgroundColor(colorSelected);
                            }
                        } else {
//                        dataAns.put( position , AppConstant.QUE_ATTEMPTED_WRONG );
                            dataAns.set(position, AppConstant.QUE_ATTEMPTED_WRONG);
                            v.setBackgroundColor(colorSelected);
                            if (seeAnswer) {
                                v.setBackgroundColor(colorRed);
                                if (data.get(position) != null && !SupportUtil.isEmptyOrNull(data.get(position).getOptionAns())) {
                                    int id = getWebViewId(data.get(position).getOptionAns().toUpperCase());
                                    if (id != 0 && view.findViewById(id) != null) {
                                        view.findViewById(id).setBackgroundColor(colorGreen);
                                    }
                                }
                            } else {
                                v.setBackgroundColor(colorSelected);
                            }
                        }
                    }
                    onCustomOnTouch.onCustomOnTouch(v, true, dataAns);
                } else {
                    onCustomOnTouch.onCustomOnTouch(v, false, dataAns);
                }
            }else {
                isFullDesc = false ;
                hideFullDesc();
            }
        }
        return false;
    }

    private int getWebViewId( String ans ){
        int id = 0 ;
        switch ( ans ){
            case "A":
                id = R.id.wv_ans_one ;
                break;
            case "B":
                id = R.id.wv_ans_two ;
                break;
            case "C":
                id = R.id.wv_ans_three ;
                break;
            case "D":
                id = R.id.wv_ans_four ;
                break;
            case "E":
                id = R.id.wv_ans_five ;
                break;
        }
        return id ;
    }

    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text = null;
                int lineEndIndex = 0;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if ( tv != null && tv.getLayout() != null ){
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                if ( text != null ) {
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                     final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {

                ds.setUnderlineText(false);
                ds.setColor(Color.WHITE);

            }

            @Override
            public void onClick(View widget) {
                showFullDesc();
            }
        }, 0, str.indexOf(spanableText) + spanableText.length(), 0);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint ds) {

                    ds.setUnderlineText(true);
//                    ds.setColor(Color.parseColor("#303F9F"));
                    ds.setColor(Color.YELLOW);

                }

                @Override
                public void onClick(View widget) {
                    showFullDesc();
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    private void hideFullDesc() {
        wvDir.setVisibility(View.GONE);
        tvDir.setVisibility(View.VISIBLE);
    }

    private void showFullDesc() {
        wvDir.setVisibility(View.VISIBLE);
        tvDir.setVisibility(View.GONE);
    }

}
