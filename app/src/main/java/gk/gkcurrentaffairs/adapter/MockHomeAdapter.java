package gk.gkcurrentaffairs.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;
import com.adssdk.OnCustomLoadMore;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/10/2017.
 */

public class MockHomeAdapter extends NativeAdsAdapter {

    private ArrayList<MockHomeBean> homeBeen;
    private OnClick onClick;
    private OnLoadMore onLoadMore;
    public static final int ITEM_NORMAL = 0;
    public static final int ITEM_NATIVE_AD = 1;
    private HashMap<Integer, String> categoryNames;
    private int imageRes = 0 , colorGray , colorAttempt;
    private Activity context;
    private Typeface typeface ;
    private String imageUrl ;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

//    public MockHomeAdapter(ArrayList<MockHomeBean> homeBeen, OnClick onClick, final OnLoadMore onLoadMore, Activity context, boolean isEngMock) {
    public MockHomeAdapter(ArrayList<MockHomeBean> homeBeen, OnClick onClick, final OnLoadMore onLoadMore, Activity context, boolean isEngMock, HashMap<Integer, String> categoryNames) {
        super(homeBeen, R.layout.native_list_ad_app_install ,new OnCustomLoadMore() {
                    @Override
                    public void onLoadMore() {
                        if ( onLoadMore != null )
                            onLoadMore.onCustomLoadMore();
                    }
                });
        this.homeBeen = homeBeen;
        this.onClick = onClick;
        this.context = context;
        this.onLoadMore = onLoadMore;
//        categoryNames = isEngMock ? AppData.getInstance().getCategoryNamesEngMock() : AppData.getInstance().getCategoryNames();
        this.categoryNames = categoryNames;
        colorGray = SupportUtil.getColor( android.R.color.darker_gray , context );
        colorAttempt = SupportUtil.getColor( R.color.green_mcq_paid , context );
        typeface = AppApplication.getInstance().getTypeface();

    }

    public interface OnClick {
        void onCustomItemClick(int position, int type);
    }

    public interface OnLoadMore {
        void onCustomLoadMore();
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mock_test, parent, false);
        return new ArticleViewHolder(view, onClick);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            if (homeBeen.size() > position) {
                MockHomeBean bean = homeBeen.get(position);
                viewHolder.position = position;
                viewHolder.title.setText(bean.getTitle());
                String cat = "";
                if (categoryNames != null && categoryNames.size() > 0) {
                    String catName = categoryNames.get(bean.getSubCatId());
                    if ( !SupportUtil.isEmptyOrNull(catName) )
                        viewHolder.category.setText(catName);
//                        viewHolder.category.setText(" *  " + catName);
                    else {
                        viewHolder.category.setText("");
                    }
                    viewHolder.category.setTextColor(colorGray);
                }
                if (bean.isAttempted()) {
                    viewHolder.attempt.setText( "Attempted" );
                    viewHolder.attempt.setTextColor( colorAttempt );
                } else {
                    viewHolder.attempt.setText( "Not Attempted" );
                    viewHolder.attempt.setTextColor( colorGray );
                }
                if (SupportUtil.isEmptyOrNull( imageUrl )) {
                    viewHolder.mainImage.setImageResource(imageRes);
                }else {
                    SupportUtil.loadImage(viewHolder.mainImage , imageUrl , R.drawable.exam_place_holder);
/*
                    AppApplication.getInstance().getPicasso().load(imageUrl)
                            .placeholder(R.drawable.exam_place_holder).into(viewHolder.mainImage);
*/
                }
//                viewHolder.mainImage.setImageResource(imageRes);
                viewHolder.rank.setVisibility( bean.isSync() ? View.VISIBLE : View.GONE );
                switch (bean.getDownload()) {
                    case DbHelper.INT_TRUE:
                        handleDownloadUI(viewHolder, R.drawable.mc_downloded, false);
                        break;
                    case DbHelper.INT_FALSE:
                        handleDownloadUI(viewHolder, R.drawable.mc_not_download, false);
                        break;
                    default:
                        handleDownloadUI(viewHolder, R.drawable.mc_downloded, true);
                        break;

                }
            }
        }

        if (onLoadMore != null && position == homeBeen.size() - 1)
            onLoadMore.onCustomLoadMore();
    }

    private void handleDownloadUI(ArticleViewHolder viewHolder, int imageRes, boolean isProgress) {
        viewHolder.download.setImageResource(imageRes);
//        viewHolder.progressBar.setVisibility(isProgress ? View.VISIBLE : View.GONE);
        viewHolder.download.setVisibility(isProgress ? View.GONE : View.VISIBLE);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class AdsViewHolder extends ViewHolder {
        RelativeLayout relativeLayout;

        public AdsViewHolder(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rl_ads);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

        TextView title, category , attempt ;
        ImageView mainImage, download ;
        View rank;
        int position;
        OnClick onClick;
//        ProgressBar progressBar;

        public ArticleViewHolder(View itemView, OnClick onClick) {
            super(itemView);
            this.onClick = onClick;
            title = (TextView) itemView.findViewById(R.id.item_tv_title);
            attempt = (TextView) itemView.findViewById(R.id.item_tv_attempt);
            rank = itemView.findViewById(R.id.adapter_tv_rank);
            category = (TextView) itemView.findViewById(R.id.item_tv_category);
            mainImage = (ImageView) itemView.findViewById(R.id.item_iv_main);
            download = (ImageView) itemView.findViewById(R.id.item_iv_download);
//            progressBar = (ProgressBar) itemView.findViewById(R.id.item_pb_download);
            rank.setOnClickListener(this);
            itemView.setOnClickListener(this);
            download.setOnClickListener(this);
            title.setTypeface(typeface);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
/*
                case R.id.item_iv_download:
                    onClick.onCustomItemClick(position, false);
                    break;
*/
                case R.id.adapter_tv_solution:
                    onClick.onCustomItemClick(position, TYPE_MCQ_SOLUTION);
//                    onClick.onCustomItemClick(position, homeBeen.get(position).isSync());
                    break;
                case R.id.adapter_tv_instruction:
                    onClick.onCustomItemClick(position, TYPE_MCQ_INSTRUCTION);
                    break;
                case R.id.adapter_tv_rank:
                    onClick.onCustomItemClick(position, TYPE_MCQ_RANK);
                    break;
                default:
                    onClick.onCustomItemClick(position, TYPE_MCQ_OPEN);
//                    onClick.onCustomItemClick(position, false);
                    break;
            }
        }
/*
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.item_iv_download:
                    onClick.onCustomItemClick(position, false);
                    break;
                case R.id.adapter_tv_rank:
                    onClick.onCustomItemClick(position, homeBeen.get(position).isSync());
                    break;
                default:
                    onClick.onCustomItemClick(position, false);
                    break;
            }
        }
*/
    }

    public static final int TYPE_MCQ_OPEN = 0 ;
    public static final int TYPE_MCQ_INSTRUCTION = 1 ;
    public static final int TYPE_MCQ_SOLUTION = 2 ;
    public static final int TYPE_MCQ_RANK = 3 ;

}
