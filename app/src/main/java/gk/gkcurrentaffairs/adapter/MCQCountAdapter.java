package gk.gkcurrentaffairs.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;


/**
 * Created by @Amit on 30/9/15.
 */
public class MCQCountAdapter extends RecyclerView.Adapter<MCQCountAdapter.MyViewHolder> {

    int count , totalQue;
    OnItemClick onItemClick ;
//    private HashMap<Integer, Integer> dataAns;
    private ArrayList<Integer> dataAns;

//    public MCQCountAdapter(int count , int totalQue , OnItemClick onItemClick,HashMap<Integer, Integer> dataAns) {
    public MCQCountAdapter(int count , int totalQue , OnItemClick onItemClick,ArrayList<Integer> dataAns) {
        this.count = count ;
        this.totalQue = totalQue ;
        this.onItemClick = onItemClick ;
        this.dataAns = dataAns ;
    }

    public void setCount(int count) {
        this.count = count;
    }

//    public void setDataAns(HashMap<Integer, Integer> dataAns) {
//        this.dataAns = dataAns;
//    }
    public void setDataAns(ArrayList<Integer> dataAns) {
        this.dataAns = dataAns;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mcq_count,
                viewGroup, false);
        return new MyViewHolder(view , i , onItemClick);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        String title = i + 1 + "" ;
        myViewHolder.title.setText( title );
        myViewHolder.vPosition.setVisibility( i == ( count - 1 ) ? View.VISIBLE : View.GONE );
        handleMode( i , myViewHolder.title );
//        if ( count >= ( i + 1 ) ) {
//            myViewHolder.title.setBackgroundResource(R.drawable.circle_red);
//            myViewHolder.title.setTextColor(Color.WHITE);
//        }else{
//            myViewHolder.title.setBackgroundResource(R.drawable.circle_white);
//            myViewHolder.title.setTextColor( myViewHolder.color );
//        }
        myViewHolder.position = i ;
    }

    private void handleMode( int position , View view ){
        switch ( dataAns.get( position ) ){
            case 0 :
                view.setBackgroundResource( count > totalQue ? R.drawable.circle_white_green_border : R.drawable.circle_white_green_border );
                break;
            case 1 :
                view.setBackgroundResource( R.drawable.circle_green );
                break;
            case 2 :
                view.setBackgroundResource( R.drawable.circle_red );
                break;
        }
    }

    @Override
    public int getItemCount() {
        return totalQue;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        View vPosition;
        public int position;
        int color;
        OnItemClick onItemClick ;
        public MyViewHolder(View itemView , int i , OnItemClick onItemClick) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_adp_mcq_count);
            vPosition = itemView.findViewById(R.id.v_position);
            this.onItemClick = onItemClick ;
            itemView.setOnClickListener( this );
            if ( i == 0 )
                color = title.getCurrentTextColor() ;
        }

        @Override
        public void onClick(View v) {
            if ( onItemClick != null ) {
                this.onItemClick.OnItemClick(position);
            }
        }
    }

    public interface OnItemClick {
        void OnItemClick(int position);
    }
}

