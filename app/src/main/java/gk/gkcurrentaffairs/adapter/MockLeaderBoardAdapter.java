package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/24/2018.
 */

public class MockLeaderBoardAdapter extends RecyclerView.Adapter<MockLeaderBoardAdapter.ViewHolder> {

    private List<UserLeaderBoardBean> children;
    Picasso picasso;
    private int userId, colorBlue , totalCount;

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public MockLeaderBoardAdapter(Context context, List<UserLeaderBoardBean> children, int userId) {
        this.children = children;
        picasso = AppApplication.getInstance().getPicasso();
        this.userId = userId;
//        colorBlue = SupportUtil.getColor(R.color.color_primary_dark_status, context);
        colorBlue = Color.parseColor("#c4e5eb");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_mock_leader_board, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        UserLeaderBoardBean child = children.get(position);
        if (child != null) {
            SupportUtil.loadUserImage(child.getPhoto_url(), holder.ivMain , R.drawable.profile_black);
            String name = child.getName() ;
            if ( SupportUtil.isEmptyOrNull( name ) )
                name = "User" ;
            holder.tvTitle.setText(name);
            holder.tvRank.setText(child.getRank() + "/" + totalCount );
            holder.tvPoint.setText(child.getPoints() + "");
            holder.view.setBackgroundColor(child.getUser_id() == userId ? colorBlue : Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        int position;
        TextView tvTitle, tvRank, tvPoint;
        ImageView ivMain;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.item_tv_title);
            tvRank = (TextView) itemView.findViewById(R.id.item_tv_rank);
            tvPoint = (TextView) itemView.findViewById(R.id.item_tv_points);
            ivMain = (ImageView) itemView.findViewById(R.id.item_iv_main);
            view = itemView.findViewById(R.id.ll_holder);
        }
    }
}
