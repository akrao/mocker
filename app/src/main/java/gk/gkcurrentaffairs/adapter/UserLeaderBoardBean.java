package gk.gkcurrentaffairs.adapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 1/24/2018.
 */

public class UserLeaderBoardBean {

    @SerializedName("name")
    @Expose
    private String name ;
    @SerializedName("photo_url")
    @Expose
    private String photo_url ;
    @SerializedName("user_id")
    @Expose
    private int user_id ;
    @SerializedName("points")
    @Expose
    private int points ;
/*
    @SerializedName("time_taken")
    @Expose
    private int time_taken ;
*/
    private int rank ;

    private double pointTime ;

    public double getPointTime() {
        return pointTime;
    }

    public void setPointTime() {
//        this.pointTime = points + ( ( Math.pow( 10 , AppConstant.TIME_MILLI_LENGTH ) - time_taken ) / Math.pow( 10 , AppConstant.TIME_MILLI_LENGTH ) ) ;
    }

/*
    public int getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(int time_taken) {
        this.time_taken = time_taken;
    }
*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
