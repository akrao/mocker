package gk.gkcurrentaffairs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;
import com.adssdk.OnCustomLoadMore;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ListBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/10/2017.
 */

public class HomeAdapter extends NativeAdsAdapter {

    private ArrayList<ListBean> homeBeen;
    private OnClick onClick;
    private final OnLoadMore onLoadMore;
    private OnNextLoad onNextLoad;
    private HashMap<Integer, String> categoryNames;
    private int imageRes = 0;
    private Context context;
    private int maxId = 0;
    private CategoryProperty categoryProperty;
    private boolean isDate = false, isTypePdf = false;
    private Typeface typeface ;

    public void setTypePdf(boolean typePdf) {
        isTypePdf = typePdf;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public HomeAdapter(ArrayList<ListBean> homeBeen, OnClick onClick, final OnLoadMore onLoadMore, OnNextLoad onNextLoad, Context context , HashMap<Integer, String> categoryNames) {
        super(homeBeen, R.layout.ads_native_unified_card, new OnCustomLoadMore() {
            @Override
            public void onLoadMore() {
                if (onLoadMore != null )
                    onLoadMore.onCustomLoadMore();
            }
        });
        this.homeBeen = homeBeen;
        this.onClick = onClick;
        this.context = context;
        this.onLoadMore = onLoadMore;
        this.onNextLoad = onNextLoad;
//        this.categoryNames = AppData.getInstance().getCategoryNames();
        this.categoryNames = categoryNames ;
        typeface = AppApplication.getInstance().getTypeface();
    }

    public void setCategoryProperty(CategoryProperty categoryProperty) {
        this.categoryProperty = categoryProperty;
        if (categoryProperty != null) {
            isDate = categoryProperty.isDate();
        }
    }

    public interface OnClick {
        int TYPE_ARTICLE = 0;
        int TYPE_BOOKMARK = 1;
        void onCustomItemClick(int position, int type);
    }

    public interface OnLoadMore {
        void onCustomLoadMore();
    }

    public void setMaxId(int maxId) {
        this.maxId = maxId;
    }

    public interface OnNextLoad {
        void onNextLoad();
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                isTypePdf ? R.layout.item_article_pdf : R.layout.item_article, parent, false);
        return new ArticleViewHolder(view, onClick);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            if (homeBeen.size() > position) {
                ListBean bean = homeBeen.get(position);
                viewHolder.position = position;
                viewHolder.title.setText(bean.getText());
                String cat = "";
                if (categoryNames != null && categoryNames.size() > 0) {
                    String catName = categoryNames.get(bean.getSubCatId());
                    cat = SupportUtil.isEmptyOrNull(catName) ? "" : (isDate ? "  |  " : "") + catName.trim();
                }
                viewHolder.category.setText(isDate ? bean.getDate() + cat : cat);
                if (imageRes != 0)
                    viewHolder.mainImage.setImageResource(imageRes);

                viewHolder.title.setTextColor(bean.isTrue() ? Color.GRAY : Color.BLACK);
                viewHolder.ivFav.setImageResource(bean.isFav() ? R.drawable.bookmark_imp_mark : R.drawable.fav);

                if (maxId > bean.getId() && onNextLoad != null)
                    onNextLoad.onNextLoad();
            }
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

        TextView title, category;
        ImageView mainImage;
        int position;
        View mainView, viewTrans;
        OnClick onClick;
        ImageView ivFav;

        public ArticleViewHolder(View itemView, OnClick onClick) {
            super(itemView);
            this.onClick = onClick;
            title = (TextView) itemView.findViewById(R.id.item_tv_title);
            category = (TextView) itemView.findViewById(R.id.item_tv_category);
            mainImage = (ImageView) itemView.findViewById(R.id.item_iv_main);
            ivFav = (ImageView) itemView.findViewById(R.id.iv_fav);
            ivFav.setOnClickListener(this);
            if (isTypePdf) {
                itemView.findViewById(R.id.ll_pdf).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.bt_basic).setOnClickListener(this);
                itemView.findViewById(R.id.bt_advance).setOnClickListener(this);
            }
            itemView.setOnClickListener(this);
            title.setTypeface(typeface);
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick(position, view.getId() == R.id.iv_fav ? OnClick.TYPE_BOOKMARK : OnClick.TYPE_ARTICLE);
//            onClick.onCustomItemClick(position, view.getId() == R.id.bt_basic);
        }
    }
}
