package gk.gkcurrentaffairs.adapter;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gk.gkcurrentaffairs.R;

/**
 * Created by Amit on 7/22/2017.
 */

public class PDFViewerAdapter extends RecyclerView.Adapter<PDFViewerAdapter.ViewHolder>{

    private OnCustomClick onCustomClick ;
    private int pageCount , currentPosition ;
    public interface OnCustomClick{
        void onCustomItemClick(int position);
    }

    public PDFViewerAdapter(OnCustomClick onCustomClick , int currentPosition , int pageCount ) {
        this.onCustomClick = onCustomClick;
        this.pageCount = pageCount ;
        this.currentPosition = currentPosition ;
    }

    @Override
    public PDFViewerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate( R.layout.item_pdf , parent, false);
        ViewHolder holder = new ArticleViewHolder( view , onCustomClick );
        return holder;
    }

    @Override
    public void onBindViewHolder(PDFViewerAdapter.ViewHolder holder, int position) {
        if ( holder instanceof ArticleViewHolder){
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder ;
            viewHolder.position = position ;
            viewHolder.textView.setText( position + 1 );
            if ( position == currentPosition ){
                viewHolder.textView.setBackgroundColor( Color.YELLOW );
            }else {
                viewHolder.textView.setBackgroundColor( Color.WHITE );
            }
        }
    }

    @Override
    public int getItemCount() {
        return pageCount;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener{

        TextView textView ;
        int position ;
        OnCustomClick onClick ;

        public ArticleViewHolder(View itemView , OnCustomClick onClick) {
            super(itemView);
            this.onClick = onClick ;
            textView = (TextView) itemView.findViewById( R.id.tv_page );
            itemView.setOnClickListener( this );
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick( position );
        }
    }

}
