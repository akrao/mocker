package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.SuccessBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.SupportUtil.showToastCentre;

/**
 * Created by Amit on 3/7/2017.
 */

public class BugReportActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText bugReport , email ;
    private ProgressDialog progressDialog ;
    private String que ;
    private int id ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_bug_report );
        initView();
        initData();
    }

    private void initData(){
        que = getIntent().getStringExtra( AppConstant.TITLE );
        String queId = getIntent().getStringExtra( AppConstant.CAT_ID );
        id = SupportUtil.isEmptyOrNull( queId ) ? 0 : Integer.parseInt( queId ) ;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Bug Report");
    }

    private String bugTitle = "Found a <b><font color='#3F51B5'>bug?</font></b><br>" +
            "Have a <b><font color='#3F51B5'>suggestion?</font></b><br>" +
            "Else <b><font color='#3F51B5'>share</font></b> your feeling here!";

    private void initView(){
        ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml( bugTitle ));
        bugReport = ( EditText ) findViewById( R.id.et_bug_report );
        email = ( EditText ) findViewById( R.id.et_email );
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null &&
                !SupportUtil.isEmptyOrNull(AppApplication.getInstance().getLoginSdk().getEmailId() ) ){
            email.setText( AppApplication.getInstance().getLoginSdk().getEmailId() );
        }
        findViewById( R.id.bt_submit ).setOnClickListener( this );
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Submitting..." );
    }

    @Override
    public void onClick(View v) {
        String bug = bugReport.getText().toString();
        if (SupportUtil.isEmptyOrNull( bug )){
            Toast.makeText( this , "Please enter the error details." , Toast.LENGTH_LONG ).show();
            return;
        }
        String emailId = email.getText().toString();
        if (SupportUtil.isEmptyOrNull( emailId )){
            Toast.makeText( this , "Please enter the email id." , Toast.LENGTH_LONG ).show();
            return;
        }
        handleSubmit( bug , emailId );
    }

    private void handleSubmit( String bug , String emailID ){
        progressDialog.show();
        Map<String, String> map = new HashMap<>(4);
        map.put("message", bug);
        map.put("email_id", emailID);
        map.put("question_id", id+"");
        map.put("app_id", getPackageName());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.SEND_ERROR
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        progressDialog.dismiss();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            showToastCentre(AppApplication.getInstance(), "Your Report is submitted successfully");
                            try {
                                final SuccessBean statusBean = ConfigManager.getGson().fromJson(data, SuccessBean.class);
                                if (statusBean != null && !SupportUtil.isEmptyOrNull( statusBean.getIsinsert() )
                                        && statusBean.getIsinsert().equalsIgnoreCase( "Success" )) {
                                    showToastCentre(AppApplication.getInstance(), "Your Report is submitted successfully");
                                } else {
                                    showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                                }
                            } catch (JsonSyntaxException e) {
                                showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                                e.printStackTrace();
                            }
                        }else {
                            showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                        }
                    }
                });

/*
        Call<SuccessBean> call = AppApplication.getInstance().getNetworkObject().submitBugReport( bug , emailID , id , getPackageName() );
        call.enqueue(new Callback<SuccessBean>() {
            @Override
            public void onResponse(Call<SuccessBean> call, Response<SuccessBean> response) {
                progressDialog.dismiss();
                if(response.body() != null && !SupportUtil.isEmptyOrNull( response.body().getIsinsert() ) && response.body().getIsinsert().equalsIgnoreCase( "1" ) )
                    Toast.makeText( BugReportActivity.this , "Your Report is submitted successfully" , Toast.LENGTH_SHORT ).show();
                else
                    Toast.makeText( BugReportActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onFailure(Call<SuccessBean> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText( BugReportActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
            }
        });
*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            onBackPressed();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

}
