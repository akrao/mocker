package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.IdBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.fragment.CategoryFragment;
import gk.gkcurrentaffairs.fragment.CategoryListFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class CategoryActivity extends PageAdsAppCompactActivity implements ViewPager.OnPageChangeListener {

    private int catID;
    private String title = "";
    private int position, image;
    private boolean isCategory, isWebView, isNotification;
    private boolean isRepeat, isSecond = true, isGovt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initDataFromArg();
        initRepeatTask();
        setupToolbar();
        setupViewPager();
        setupCollapsingToolbar();
        if (!isGovt) {
            fetchLatestIds(System.currentTimeMillis());
        }
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }

    private CategoryProperty categoryProperty ;
    private void initDataFromArg() {
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null ) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
            initObjects();
        }else {
            initDataForUnStructure();
        }
    }

    private void initObjects(){
        catID = categoryProperty.getId() ;
        isCategory = categoryProperty.isSubCat() ;
        isWebView = categoryProperty.isWebView() ;
        title = categoryProperty.getTitle() ;
        image = categoryProperty.getImageResId() ;
    }

    private void initDataForUnStructure(){
        position = getIntent().getIntExtra(AppConstant.POSITION, 1);
        catID = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        isCategory = getIntent().getBooleanExtra(AppConstant.DATA, true);
        isWebView = getIntent().getBooleanExtra(AppConstant.WEB_VIEW, true);
        if ( !SupportUtil.isEmptyOrNull( getIntent().getStringExtra( AppConstant.TITLE ) ) ) {
            title = getIntent().getStringExtra( AppConstant.TITLE );
        } else {
            title = getResources().getStringArray( R.array.main_cat_name )[ position ] ;
        }
        image = AppConstant.IMAGE_RES[position];
        isNotification = getIntent().getBooleanExtra(AppConstant.CATEGORY, false);
        isGovt = catID == AppValues.GOVT_JOBS_ID;
        if (isNotification) {
            int articleId = getIntent().getIntExtra(AppConstant.CLICK_ITEM_ARTICLE, 0);
            if (articleId != 0) {
                Intent intent = new Intent(this, DescActivity.class);
                intent.putExtra(AppConstant.DATA, articleId);
                intent.putExtra(AppConstant.QUERY, DbHelper.COLUMN_ID + "=" + articleId);
                intent.putExtra(AppConstant.CAT_ID, catID);
                intent.putExtra(AppConstant.CATEGORY, isNotification);
                intent.putExtra(AppConstant.WEB_VIEW, isWebView);
                startActivity(intent);
            }
        }
    }


    private void setupCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(false);
    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        CategoryFragment latest = new CategoryFragment();
        CategoryFragment favourite = new CategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.CAT_ID, catID);
        bundle.putInt(AppConstant.IMAGE, image);
        bundle.putInt(AppConstant.POSITION, position);
        bundle.putBoolean(AppConstant.WEB_VIEW, isWebView);
        bundle.putBoolean(AppConstant.TYPE, isNotification);
        bundle.putSerializable(AppConstant.CAT_DATA, AppData.getInstance().getCategoryPropertyHashMap().get(catID));
        if (catID != 0)
            bundle.putString(AppConstant.QUERY, DbHelper.COLUMN_CAT_ID + "=" + catID);

        latest.setArguments(bundle);
        adapter.addFrag(latest, "Latest");

        if (isCategory) {
            CategoryListFragment categoryListFragment = new CategoryListFragment();
            categoryListFragment.setArguments(bundle);
            adapter.addFrag(categoryListFragment, "Category");
        }

        bundle = new Bundle();
        bundle.putInt(AppConstant.CAT_ID, catID);
        bundle.putInt(AppConstant.IMAGE, image);
        bundle.putBoolean(AppConstant.WEB_VIEW, isWebView);

        if (catID != 0) {
            String q = DbHelper.COLUMN_FAV + "=" + DbHelper.INT_TRUE
                    + " AND " + DbHelper.COLUMN_CAT_ID + "=" + catID;
            bundle.putString(AppConstant.QUERY, q);
        }
        favourite.setArguments(bundle);
        adapter.addFrag(favourite, "Fav");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof CategoryFragment) {
            ((CategoryFragment) fragment).refreshFragment();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>(3);
        private final List<String> mFragmentTitleList = new ArrayList<>(3);

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra(AppConstant.DATA, catID);
            intent.putExtra(AppConstant.IMAGE, image);
            intent.putExtra(AppConstant.WEB_VIEW, isWebView);
            intent.putExtra(AppConstant.PROPERTY, AppData.getInstance().getCategoryPropertyHashMap().get(catID));
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initRepeatTask() {
        isRepeat = catID == AppValues.CAA_ID || catID == AppValues.GOVT_JOBS_ID;
    }

    public void fetchLatestIds(long id) {
        boolean isCall = isGovt || (AppData.getInstance().getNetworkQue().get(catID) != null
                && !AppData.getInstance().getNetworkQue().get(catID));
        if (!isNotification && isCall) {
            Map<String, String> map = new HashMap<>(2);
            map.put("id", catID + "");
            map.put("max_content_id", id + "");

            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN
                    , ApiEndPoint.GET_CONTENT_AND_SUB_CAT_IDS_BY_CAT_ID
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    List<IdBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<IdBean>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        new DataInsert(list).execute();
                                        if (isRepeat && isSecond) {
                                            isSecond = false;
                                            fetchLatestIds(list.get(list.size() - 1).getId());
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    });

/*
                            final Call<List<IdBean>> call = AppApplication.getInstance().getNetworkObject().getLatestIdByCatId(catID , id);
            AppData.getInstance().getNetworkQue().put( catID , true );
            call.enqueue(new Callback<List<IdBean>>() {
                @Override
                public void onResponse(Call<List<IdBean>> call, Response<List<IdBean>> response) {
                    if (response != null && response.body() != null && response.body().size() > 0) {
                        new DataInsert(response.body()).execute();
                        if ( isRepeat && isSecond ) {
                            isSecond = false ;
                            fetchLatestIds(Long.parseLong(response.body().get(response.body().size() - 1).getId()));
                        }
                    }
                }
                @Override
                public void onFailure(Call<List<IdBean>> call, Throwable t) {
                    AppApplication.getInstance().invalidateApiUrl( null , true );
                }
            });
*/
        }
    }

    class DataInsert extends AsyncTask<Void, Void, Void> {
        private List<IdBean> idBeen;

        public DataInsert(List<IdBean> idBeen) {
            this.idBeen = idBeen;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.insertArticleId(idBeen, catID);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
                Fragment fragment = adapter.getItem(0);
                if (fragment != null && fragment instanceof CategoryFragment) {
                    ((CategoryFragment) fragment).callIdFunc();
                }
        }
    }
}
