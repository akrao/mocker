package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/1/2017.
 */

public class NotesMCQActivity extends PageAdsAppCompactActivity implements View.OnClickListener{

    private int position , positionMargin = AppConstant.POSITION_MARGIN_MIDDLE , idFirst , idSec ,catId ;
//    private int position , positionMargin = 0 , idFirst , idSec ,catId ;
    private String query ;
    private ProgressDialog progressDialog ;
    private boolean isServerOne;
//    private ApiEndpointInterface apiEndpointInterface ;
    private String HOST;
    private DbHelper dbHelper ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_notes_mcq );
        initDataFromArg();
        initDataObjects();
        initView();
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    private CategoryProperty categoryProperty ;
    private void initDataFromArg() {
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null ) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
            initObjects();
        }else {
            initDataForUnStructure();
        }
    }

    private String title ;
    private void initObjects(){
        position = categoryProperty.getPosition() ;
        title = categoryProperty.getTitle() ;
    }

    private void initDataForUnStructure() {
        position = getIntent().getIntExtra( AppConstant.POSITION , AppConstant.ENGLISH_MOCK_TEST_ID);
        title = getResources().getStringArray( R.array.main_cat_name )[ position ] ;
    }

    private void initDataObjects(){
        if ( categoryProperty != null ){
            positionMargin = 0 ;
        }else {
            positionMargin = AppConstant.POSITION_MARGIN_MIDDLE ;
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle( title );
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Downloading..." );
        dbHelper = AppApplication.getInstance().getDBObject();
        catId = AppValues.MOCK_CAT_ID[ position - positionMargin ] ;
        isServerOne = catId != AppConstant.ENGLISH_MOCK_TEST_ID ;
//        apiEndpointInterface = isServerOne ? AppApplication.getInstance().getNetworkObject() :
//                AppApplication.getInstance().getNetworkObject_1();
        HOST = isServerOne ? ConfigConstant.HOST_MAIN : ConfigConstant.HOST_TRANSLATOR ;

        boolean isNotification = getIntent().getBooleanExtra( AppConstant.CATEGORY , false );
        if ( isNotification ) {
            int articleId = getIntent().getIntExtra(AppConstant.CLICK_ITEM_ARTICLE, 0);
            int catId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);

            if ( AppValues.MOCK_CAT_ID[ position - positionMargin] == catId ) {
                callNextAct( MockCategoryActivity.class , AppValues.MOCK_CAT_ID[ position - positionMargin] , true , articleId );
            } else {
                callNextAct( CategoryActivity.class , AppValues.NOTES_CAT_ID[ position - positionMargin] , true , articleId );
            }
        }
    }

    private void initView(){
        findViewById( R.id.ll_study ).setOnClickListener( this );
        findViewById( R.id.ll_notes ).setOnClickListener( this );
        findViewById( R.id.ll_training ).setOnClickListener( this );
        findViewById( R.id.ll_challenge ).setOnClickListener( this );
        findViewById( R.id.ll_play ).setOnClickListener( this );

    }

    @Override
    protected void onStart() {
        super.onStart();
        TextView textView = ( TextView ) findViewById( R.id.play_game );
        textView.setText( "Current level : " + AppPreferences.getPlayLevel( this , catId ) );
    }

    @Override
    public void onClick(View v) {
        if ( categoryProperty != null ) {
            Class aClass = null ;
            int id = 0 ;
            switch ( v.getId() ){
                case R.id.ll_notes :
                    id = AppValues.NOTES_CAT_ID[ position - positionMargin ] ;
                    categoryProperty.setId(id);
                    if ( categoryProperty.getId() != AppValues.NOTES_CAT_ID[0] ) {
                        categoryProperty.setSubCat(false);
                    }
                    aClass = CategoryActivity.class ;
                    callNextAct( aClass , id , false , 0 );
                    break;
                case R.id.ll_study :
                    id = AppValues.MOCK_CAT_ID[ position - positionMargin ] ;
                    aClass = MockCategoryActivity.class ;
                    categoryProperty.setId(id);
                    if ( categoryProperty.getId() == AppValues.MOCK_CAT_ID[1] ) {
                        categoryProperty.setHost(ConfigConstant.HOST_TRANSLATOR);
                    }
                    callNextAct( aClass , id , false , 0);
                    break;
                case R.id.ll_training :
                    id = AppValues.MOCK_CAT_ID[ position - positionMargin ] ;
                    categoryProperty.setId(id);
                    if ( categoryProperty.getId() == AppValues.MOCK_CAT_ID[1] ) {
                        categoryProperty.setHost(ConfigConstant.HOST_TRANSLATOR);
                    }
                    callNextAct( true , "Training" , id );
                    break;
                case R.id.ll_challenge :
                    id = AppValues.MOCK_CAT_ID[ position - positionMargin ] ;
                    categoryProperty.setId(id);
                    if ( categoryProperty.getId() == AppValues.MOCK_CAT_ID[1] ) {
                        categoryProperty.setHost(ConfigConstant.HOST_TRANSLATOR);
                    }
                    callNextAct( false , "Challenge" , id );
                    break;
                case R.id.ll_play :
                    id = AppValues.MOCK_CAT_ID[ position - positionMargin ] ;
                    categoryProperty.setId(id);
                    if ( categoryProperty.getId() == AppValues.MOCK_CAT_ID[1] ) {
                        categoryProperty.setHost(ConfigConstant.HOST_TRANSLATOR);
                    }
                    handleMCQ();
                    break;
            }
        }
    }

    private void callNextAct( Class aClass , int id , boolean isNotification , int articleId ){
        Intent intent = new Intent( this , aClass );
        intent.putExtra( AppConstant.CAT_DATA , categoryProperty );
        intent.putExtra( AppConstant.TITLE , title );
        intent.putExtra( AppConstant.POSITION , position );
        intent.putExtra( AppConstant.WEB_VIEW , true );
        if ( categoryProperty == null ) {
            if ( ( position - positionMargin ) > 3 ){
                intent.putExtra( AppConstant.DATA , false );
            }else {
                if (aClass == MockCategoryActivity.class ) {
                    intent.putExtra( AppConstant.DATA , true );
                } else {
                    intent.putExtra( AppConstant.DATA , ( position - positionMargin ) == 0 ? true : false );
                }
//                intent.putExtra( AppConstant.DATA , true );
            }
//            intent.putExtra( AppConstant.DATA , AppValues.CATEGORY_EXIST[ position - positionMargin ] == DbHelper.INT_TRUE );
//            intent.putExtra( AppConstant.WEB_VIEW , AppConstant.CATEGORY_WEB[ position ] == DbHelper.INT_TRUE  );
        } else if ( categoryProperty != null) {
          if ( position > 3 ){
              intent.putExtra( AppConstant.DATA , false );
          }else {
              if (aClass == MockCategoryActivity.class ) {
                  intent.putExtra( AppConstant.DATA , true );
              } else {
                  intent.putExtra( AppConstant.DATA , ( position - positionMargin ) == 0 ? true : false );
              }
//              intent.putExtra( AppConstant.DATA , true );
          }
        }
        intent.putExtra( AppConstant.CAT_ID , id );
        intent.putExtra( AppConstant.CATEGORY , isNotification );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , articleId );
        startActivity( intent );
    }

    private void callNextAct( boolean isTraining , String title , int id  ){
        Intent intent = new Intent( this , MCQIntermediateActivity.class );
        intent.putExtra( AppConstant.DATA , isTraining );
        intent.putExtra( AppConstant.CAT_ID , id );
        intent.putExtra( AppConstant.CAT_DATA , categoryProperty );
        intent.putExtra( AppConstant.TITLE , title );
        intent.putExtra( AppConstant.POSITION , position );
        startActivity( intent );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleMCQ(){
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                query = DbHelper.COLUMN_CAT_ID + "=" + catId ;
                if (dbHelper.getMockTestCatId(query)) {
                    openMCQ(query);
                } else {
                    SupportUtil.downloadNormalMcqQue20(categoryProperty.getHost(), catId, NotesMCQActivity.this, new SupportUtil.DownloadNormalMcqQue20() {
                        @Override
                        public void onResult(boolean result, String query) {
                            if ( result ){
                                openMCQ(query);
                            }
                        }
                    });
//                    downloadMCQOpen();
                }

/*
                if ( dbHelper.getMockTestCatId( query ) ){
                    openMCQ();
                }else {
                    downloadMCQOpen();
                }
*/
                return null;
            }
        });
    }

/*
    private void downloadMCQOpen(){
        progressDialog.show();
        Map<String, String> map = new HashMap<>(1);
        map.put("id", catId + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, HOST
                , ApiEndPoint.GET_LATEST_MOCK_TEST
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {}
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            List<MockTestBean> list = null;
                            try {
                                list = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockTestBean>>() {
                                }.getType());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }

                            if (list != null && list.size() > 0 && list.size() >= dbHelper.QUE_NUM ) {
                                dbHelper.callDBFunction(new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        ArrayList<MockTestBean> list = new ArrayList<>(10);
                                        setDataList( list , list , 0 );
                                        idFirst =  Integer.parseInt(list.get( 9 ).getId()) ;
                                        dbHelper.insertMockTest( list , idFirst , catId);

                                        setDataList( list , list , 10 );
                                        idSec = Integer.parseInt(list.get( 9 ).getId()) ;
                                        dbHelper.insertMockTest( list , idSec , catId);

                                        query = DbHelper.COLUMN_MOCK_TEST_ID + " IN (" + idFirst + "," + idSec + ")" ;
                                        openMCQ();
                                        return null;
                                    }
                                });
                            }else
                                Toast.makeText( NotesMCQActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                        }else
                            Toast.makeText( NotesMCQActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();

                    }
                });
*/
/*
        Call<List<MockTestBean>> call;
        if (isServerOne) {
            call = apiEndpointInterface.downloadLatestMockTestBycatId( catId );
        } else {
            call = apiEndpointInterface.downloadLatestMockTestBycatIdServer( catId );
        }
        call.enqueue(new Callback<List<MockTestBean>>() {
            @Override
            public void onResponse(Call<List<MockTestBean>> call,final Response<List<MockTestBean>> response) {
                if ( response.body() != null && response.body().size() >= dbHelper.QUE_NUM ){
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            ArrayList<MockTestBean> list = new ArrayList<>(10);
                            setDataList( response.body() , list , 0 );
                            idFirst = list.get( 9 ).getId() ;
                            dbHelper.insertMockTest( list , idFirst , catId);

                            setDataList( response.body() , list , 0 );
                            idSec = list.get( 9 ).getId() ;
                            dbHelper.insertMockTest( list , idSec , catId );

                            query = DbHelper.COLUMN_MOCK_TEST_ID + " IN (" + idFirst + "," + idSec + ")" ;
                            openMCQ();
                            return null;
                        }
                    });

                }else
                    Toast.makeText( NotesMCQActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<List<MockTestBean>> call, Throwable t) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                }
                Toast.makeText( NotesMCQActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
            }
        });
*/
//    }

/*
    private void setDataList(List<MockTestBean> dataList , ArrayList<MockTestBean> list , int index){
        list.clear();
        for ( int i = index ; i < index + 10 ; i++ )
            list.add( dataList.get( i ) );
    }

    private void openMCQ(){
        Intent intent = new Intent( this , MCQActivity.class);
        intent.putExtra( AppConstant.CAT_ID , catId );
        intent.putExtra( AppConstant.CATEGORY , false );
        intent.putExtra( AppPreferences.PLAY_LEVEL , true );
        intent.putExtra( AppConstant.QUERY , query );
        intent.putExtra( AppConstant.TITLE , "Level " + AppPreferences.getPlayLevel( this , catId ) );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , AppPreferences.getPlayLevel( this , catId ) );
        startActivity( intent );
    }


    private CategoryProperty categoryProperty ;
*/

    private void openMCQ(String s) {
        Intent intent = new Intent(this, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, getMockHomeBean());
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.CATEGORY, false);
        intent.putExtra(AppConstant.QUERY, s);
        startActivity(intent);
        finish();
    }

    private MockHomeBean getMockHomeBean(){
        MockHomeBean mockHomeBean = new MockHomeBean();
        mockHomeBean.setNegativeMarking(0);
        mockHomeBean.setTestMarkes(0);
        mockHomeBean.setSeeAnswer(true);
        mockHomeBean.setTestTime(0);
        mockHomeBean.setQuestMarks(1);
        mockHomeBean.setTitle(title);
        mockHomeBean.setId(catId);
        return mockHomeBean ;
    }

}
