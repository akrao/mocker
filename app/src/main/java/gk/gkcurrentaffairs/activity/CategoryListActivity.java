package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ServerCatListBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/7/2018.
 */

public class CategoryListActivity extends PageAdsAppCompactActivity implements CategoryListAdapter.OnCustomClick {

    private CategoryProperty categoryProperty;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private View llNoData;
    private ArrayList<CategoryBean> categoryBeen;
    private boolean isServerTranslator ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_article_list);
        initViews();
        initDataFromIntent();
    }

    private int type;

    private void initDataFromIntent() {
        categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
        type = getIntent().getIntExtra(AppConstant.TYPE, Constants.CATEGORY_TYPE_MOCK);
        host = getIntent().getStringExtra(AppConstant.HOST);
        if ( SupportUtil.isEmptyOrNull(host) )
            host = ConfigConstant.HOST_MAIN ;
        if (categoryProperty != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(categoryProperty.getTitle());
            initData();
        }
    }

    private void initData() {
        String s = AppPreferences.getCatData(this, categoryProperty.getId());
        if (!SupportUtil.isEmptyOrNull(s))
            handleData(s);
        fetchData();
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        SwipeRefreshLayout swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setEnabled(false);
        llNoData = findViewById(R.id.ll_no_data);
    }

    private void setUpList() {
        llNoData.setVisibility(View.GONE);
        mAdapter = new CategoryListAdapter(categoryBeen, this, R.layout.item_grid);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomItemClick(int position) {
        if ( type == Constants.CATEGORY_TYPE_MOCK ){
            CategoryProperty property = new CategoryProperty();
            property.setId( categoryBeen.get(position).getCategoryId() );
            property.setTitle( categoryBeen.get(position).getCategoryName() );
            property.setImageResId( categoryBeen.get(position).getCategoryImage() );
            property.setImageUrl( categoryBeen.get(position).getImageUrl() );
            property.setSubCat( categoryProperty.isSubCat() );
            property.setHost( categoryProperty.getHost() );
            property.setSeeAnswer( categoryProperty.isSeeAnswer() );
            Intent intent = new Intent( this , MockCategoryActivity.class );
            intent.putExtra(AppConstant.CAT_DATA , property);
            startActivity(intent);
        }else if ( type == Constants.CATEGORY_TYPE_MOCK_ARTICLE_PDF  ){
            Intent intent = new Intent( this , NotesMcqPdfActivity.class );
            intent.putExtra(AppConstant.CAT_SERVER_DATA , list.get( position ));
            intent.putExtra(AppConstant.CAT_DATA , categoryProperty);
            intent.putExtra(AppConstant.IMAGE , imageUrl);
            startActivity(intent);
        }
    }

    List<ServerCatListBean> list ;
    private void handleData(String data) {
        try {
            list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerCatListBean>>() {
            }.getType());
            if (list != null && list.size() > 0) {
                initList(list);
                setUpList();
                AppPreferences.setCatData( this , categoryProperty.getId() , data );
            } else if (isFetch)
                showNoData();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private String imageUrl , host;
    private boolean isFetch = false;

    private void fetchData() {
        Map<String, String> map = new HashMap<>(1);
        map.put("id", categoryProperty.getId() + "");
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, host
                , Constants.GET_SUB_CAT_TREE, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        isFetch = true;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                JSONObject object = new JSONObject(data);
                                if (object != null && !SupportUtil.isEmptyOrNull(object.optString("data"))) {
                                    imageUrl = object.optString("image_path");
                                    handleData(object.optString("data"));
                                } else
                                    showNoData();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                showNoData();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        } else
                            showNoData();
                    }
                });
    }

    private void initList(List<ServerCatListBean> list) {
        Collections.sort(list, new Comparator<ServerCatListBean>() {
            @Override
            public int compare(ServerCatListBean t2, ServerCatListBean t1) {
                return Double.compare(t1.getRanking(), t2.getRanking());
            }
        });
        categoryBeen = new ArrayList<>(list.size());
        CategoryBean categoryBean;
        for (ServerCatListBean bean : list) {
            categoryBean = new CategoryBean();
            categoryBean.setCategoryId(bean.getId());
            categoryBean.setCategoryName(bean.getTitle());
            if (SupportUtil.isEmptyOrNull(bean.getImage()) && categoryProperty.getImageResId() != 0)
                categoryBean.setCategoryImage(categoryProperty.getImageResId());
            else if (SupportUtil.isEmptyOrNull(bean.getImage()))
                categoryBean.setCategoryImage(R.drawable.place_holder_cat);
            else
                categoryBean.setImageUrl(imageUrl + bean.getImage());
            categoryBeen.add(categoryBean);
        }
    }

    private void showNoData() {
        if (findViewById(R.id.player_progressbar) != null
                && findViewById(R.id.tv_no_data) != null) {
            findViewById(R.id.player_progressbar).setVisibility(View.GONE);
            findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
        }
    }
}
