package gk.gkcurrentaffairs.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.SupportUtil.showToastCentre;

/**
 * Created by amit on 19/12/16.
 */
public class CalenderActivity extends PageAdsAppCompactActivity implements CalendarView.OnDateChangeListener,
        View.OnClickListener {

    private CalendarView calenderView;
    String selectedDate;
    private DbHelper dbData;
    private Activity activity;
    StringBuilder builder;
    private int catId = AppValues.CAQ_ID;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        activity = this;
        initDataFromArg();
        initViews();
        getSupportActionBar().setTitle("Select Date");
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }

    private void initViews() {
        dbData = AppApplication.getInstance().getDBObject();
        calenderView = (CalendarView) findViewById(R.id.calenderView);
        calenderView.setOnDateChangeListener(this);
        findViewById(R.id.btn_test).setOnClickListener(this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        selectedDate = sdf.format(new Date(calenderView.getDate()));
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Downloading...");
    }

    private CategoryProperty categoryProperty ;
    private void initDataFromArg() {
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null ) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
            initObjects();
        }else {
            initDataForUnStructure();
        }
    }

    private String title ;
    private void initObjects(){
        catId = categoryProperty.getId() ;
        title = categoryProperty.getTitle() ;
    }

    private void initDataForUnStructure() {
        int position = getIntent().getIntExtra(AppConstant.POSITION, AppConstant.ENGLISH_MOCK_TEST_ID);
        title = getResources().getStringArray(R.array.main_cat_name)[position];
    }

        @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
        builder = new StringBuilder();
        builder.append(year).append(String.format("%02d", month + 1)).append(String.format("%02d", dayOfMonth));
        selectedDate = builder.toString();
    }

    @Override
    public void onClick(View v) {
        updateDateDB();
    }


    private void updateDateDB() {
        dbData.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (!dbData.getMockTestTitle(getQuery())) {
                    if (SupportUtil.isConnected(activity)) {
                        try {
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                        } catch (Exception e) {
                        }
                        downloadMCQ();
                    } else {
                        SupportUtil.showToastInternet(activity);
                    }


                } else {
                    nextActivity();
                }
                return null;
            }
        });
    }
    private void downloadMCQ(){
        Map<String, String> map = new HashMap<>(2);
        map.put("id", catId+"");
        map.put("date", selectedDate);
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_CONTENT_FOR_DATE_BY_SUB_CAT_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {}
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final List<MockTestBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockTestBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    dbData.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbData.insertMockTest(list, getDate(), catId);
                                            return null;
                                        }
                                    });
                                    nextActivity();
                                }
                            } catch (JsonSyntaxException e) {
                                showToastCentre(AppApplication.getInstance(), "Sorry, No data found.");
                                e.printStackTrace();
                            }
                        } else
                            showToastCentre(AppApplication.getInstance(), "Sorry, No data found.");
                    }
                });

    }

    private int getDate() {
        return Integer.parseInt(selectedDate);
    }

    private void nextActivity() {
        if (selectedDate != null) {
            Intent intent = new Intent(activity, MCQActivityWithoutSwipe.class);
            intent.putExtra(AppConstant.MOCK_DATA, getMockHomeBean());
            intent.putExtra(AppConstant.CAT_ID, catId);
            intent.putExtra(AppConstant.TITLE, selectedDate);
            intent.putExtra(AppConstant.DATA, getDate());
            intent.putExtra(AppConstant.QUERY, getQuery());
            activity.startActivity(intent);
        }
    }

    private MockHomeBean getMockHomeBean(){
        MockHomeBean mockHomeBean = new MockHomeBean();
        mockHomeBean.setNegativeMarking(0);
        mockHomeBean.setTestMarkes(0);
        mockHomeBean.setSeeAnswer(true);
        mockHomeBean.setTestTime(0);
        mockHomeBean.setQuestMarks(1);
        mockHomeBean.setTitle(selectedDate);
        mockHomeBean.setId(getDate());
        return mockHomeBean ;
    }

    private String getQuery() {
        return DbHelper.COLUMN_CAT_ID + "=" + catId + " AND " + DbHelper.COLUMN_MOCK_TEST_ID + "=" + selectedDate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        } else if (id == R.id.action_share) {
            SupportUtil.share("", this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
