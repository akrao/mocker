package gk.gkcurrentaffairs.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;

/**
 * Created by Amit on 8/4/2017.
 */

public class RewardAdActivity extends AppCompatActivity implements RewardedVideoAdListener {

    private RewardedVideoAd mAd;
    private AdRequest.Builder builder ;
    private boolean IS_RV_REQUESTED = false ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadRewardAds();
    }

    @Override
    public void onRewardedVideoCompleted() {

    }

    private void loadRewardAds(){
        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);
        builder = new AdRequest.Builder();
        builder.addTestDevice("5E254AC1CF02E640413645E46C8A1A64");
        reloadAd();
    }

    private void reloadAd(){
        mAd.loadAd( getString(R.string.ad_reward
        ) , builder.build() );
    }

    protected void openRewardDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle( "1 DAY AD Free " )
        .setMessage(R.string.dlg_msg_reward_video )
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        showRewardVideo();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        IS_RV_REQUESTED = false ;
                    }
                });
        builder.setIcon( R.drawable.rewarded_video );
        builder.setCancelable(false);
        builder.create().show();
    }

    private void showRewardVideo(){
        if ( mAd != null && mAd.isLoaded() ){
            mAd.show();
        }else {
            openNotLoadedRewardDialog();
        }
    }

    protected void openNotLoadedRewardDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle( "1 DAY AD Free " )
                .setMessage(R.string.dlg_msg_reward_video_not_loaded )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        IS_RV_REQUESTED = true ;
                        dialog.dismiss();
                    }
                });
        builder.setIcon( R.drawable.rewarded_video );
        builder.setCancelable(false);
        builder.create().show();
    }


    @Override
    public void onRewardedVideoAdLoaded() {
        if ( IS_RV_REQUESTED ){
            openRewardDialog();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {
        IS_RV_REQUESTED = false ;
    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        reloadAd();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        AppPreferences.setCOINS( this , AppPreferences.getCOINS(this) + AppConstant.RV_POINTS);
        reloadAd();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        reloadAd();
    }
}
