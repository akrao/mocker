package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/17/2017.
 */

public class SettingActivity extends PageAdsAppCompactActivity implements CategoryListAdapter.OnCustomClick , SupportUtil.OnCustomResponse{

    private ProgressDialog progressDialog ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_settings );
        setUpToolBar();
        initDataSets();
        initView();
    }

    private void initView(){
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Updating ..." );
    }

    private void initDataSets(){
        String[] data = getResources().getStringArray( R.array.setting );
        ArrayList<CategoryBean> list = new ArrayList<>( data.length );
        CategoryBean bean ;
        for ( int i = 0 ; i < data.length ; i++ ){
            bean = new CategoryBean();
            bean.setCategoryName( data[ i ] );
            bean.setCategoryImage( img[ i ] );
            bean.setCategoryId( i );
            list.add( bean );
        }
        setUpList( list );
    }

    private void setUpList( ArrayList<CategoryBean> categoryBeen ){
        RecyclerView mRecyclerView = ( RecyclerView ) findViewById( R.id.itemsRecyclerView );
        mRecyclerView.setLayoutManager( new LinearLayoutManager( this ) );
        mRecyclerView.setAdapter( new CategoryListAdapter( categoryBeen , this , R.layout.item_list_image_text ) );
    }

    private void setUpToolBar(){
        getSupportActionBar().setTitle( "Settings" );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    @Override
    public void onCustomItemClick(int position) {
        switch ( position ){
            case 0 :
                if ( SupportUtil.isNotConnected( this ) )
                    SupportUtil.showToastInternet( this );
                else {
                    try {
                        progressDialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AppApplication.getInstance().invalidateApiUrl( this , true );
                }
                break;
            case 1 :
                Intent intent = new Intent( this , BrowserActivity.class );
                intent.putExtra( AppConstant.DATA , policyUrl );
                intent.putExtra( AppConstant.TITLE , "Privacy Policy" );
                startActivity( intent );
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.share_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    private int[] img = {
            R.drawable.help ,
            R.drawable.privacy_policy
    };

    private String policyUrl = "https://topcoaching.in/privacy-policy" ;

    @Override
    public void onCustomResponse(boolean result) {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText( this , result ?  "Congrats, Network is Reset." : "Error, Please try later" , Toast.LENGTH_LONG ).show();
    }
}
