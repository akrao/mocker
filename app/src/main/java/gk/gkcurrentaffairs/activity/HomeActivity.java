package gk.gkcurrentaffairs.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.login.LoginSdk;
import com.login.Util;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.fragment.HomeListFragment;
import gk.gkcurrentaffairs.fragment.NotificationListFragment;
import gk.gkcurrentaffairs.ncert.activity.ClassesActivity;
import gk.gkcurrentaffairs.payment.activity.PackageActivity;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/11/2018.
 */

public class HomeActivity extends BaseActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener
        , SupportUtil.OnImpCount {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    int INTENT_IMP_UPDATE = 2244;
    int INTENT_MY_EXAM = 2245;
    public static boolean active = false;
    private static HomeActivity homeActivity;

    public static HomeActivity getInstance() {
        return homeActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homeActivity = this;
        active = true;
        setupToolbar();
        initViews();
        initFrag();
        setupBottomNavigation();

        if (savedInstanceState == null) {
            loadHomeFragment();
        }
        registerReceiverCallBacks();
        initConfig();
    }

    private BottomNavigationView mBottomNavigationView;

    private void setupBottomNavigation() {

        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        loadHomeFragment();
                        return true;
                    case R.id.navigation_paid:
                        loadPaidModule();
                        return true;
                    case R.id.navigation_notification:
                        loadNotificationFragment();
                        return true;
                }
                return false;
            }
        });
    }

    private void initFrag() {
        homeFragment = new HomeListFragment();
        notificationListFragment = new NotificationListFragment();
//        notificationListFragment = new HomeFragment();
    }

    HomeListFragment homeFragment;
    boolean isHomeAdded = true;
    boolean isCurrentFragHome = false;

    private void loadHomeFragment() {
        if (!isCurrentFragHome) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    if (isHomeAdded) {
                        transaction.add(R.id.frameLayout, homeFragment).commitAllowingStateLoss();
                        isHomeAdded = false;
                    } else {
                        transaction.replace(R.id.frameLayout, homeFragment).commitAllowingStateLoss();
                    }
                    isCurrentFragHome = true;
                }
            });
        }
    }

    private void loadPaidModule() {
        openPayActivity(false, PaidHomeActivity.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mBottomNavigationView.setSelectedItemId(mBottomNavigationView.getMenu().getItem(0).getItemId());
            }
        }, 100);
    }

    private Fragment notificationListFragment;

    private void loadNotificationFragment() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, notificationListFragment).commitAllowingStateLoss();
        isCurrentFragHome = false;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    public void finish() {
        super.finish();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            unregisterReceiver(broadcastReceiverConnectionFailed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private TextView tvConnection;

    private void registerReceiverCallBacks() {
        registerReceiver(receiver, new IntentFilter(getPackageName() + AppConstant.APP_UPDATE));
        registerReceiver(broadcastReceiver, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_LOADED));
        registerReceiver(broadcastReceiverConnectionFailed, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_FAILURE));
    }

    private void initConfig() {
        if (SupportUtil.isConnected(this)) {
            if (AppApplication.getInstance() != null &&
                    (AppApplication.getInstance().getConfigManager() == null
                            || !AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
                callConfig();
            } else {
                initDataFromArgs();
            }
        } else
            showNotConnectTextView();
    }

    private void callConfig() {
        if (SupportUtil.isConnected(this)) {
            if (AppApplication.getInstance() != null &&
                    (AppApplication.getInstance().getConfigManager() == null
                            || !AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
                showConnectingTextView();
                AppApplication.getInstance().initOperations();
            } else if (AppApplication.getInstance().getConfigManager() != null
                    && AppApplication.getInstance().getConfigManager().isConfigLoaded())
                showSuccessConnectionTextView();
        } else
            showNotConnectTextView();
    }

    private void showConnectingTextView() {
        tvConnection.setText("Connecting...");
        tvConnection.setVisibility(View.VISIBLE);
        tvConnection.setBackgroundColor(SupportUtil.getColor(R.color.graph_yellow, this));
    }

    private void showNotConnectTextView() {
        tvConnection.setText("Click here to Connect");
        tvConnection.setVisibility(View.VISIBLE);
        tvConnection.setBackgroundColor(SupportUtil.getColor(R.color.wrong_red, this));
    }

    private void showSuccessConnectionTextView() {
        tvConnection.setVisibility(View.GONE);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showSuccessConnectionTextView();
            if (AppApplication.getInstance() != null) {
                AppApplication.getInstance().syncData();
                initSync();
            }
        }
    };

    BroadcastReceiver broadcastReceiverConnectionFailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showNotConnectTextView();
        }
    };

    private void initSync() {
        initDataFromArgs();
//        SupportUtil.getImpCount(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (SupportUtil.isConnected(this)) {
            if (!AppApplication.getInstance().getConfigManager().isConfigLoaded()) {
                callConfig();
            } else {
                initDataFromArgs();
            }
        }
    }

    private boolean isUserLogin;
    private MenuItem miProfile;
    private TextView tvProfile;
    private ImageView ivProfile;

    private void handleLoginData() {
        if (AppApplication.getInstance().getLoginSdk() != null) {
            isUserLogin = AppApplication.getInstance().getLoginSdk().isRegComplete();
            if (isUserLogin && miProfile != null) {
                miProfile.setTitle("Profile");
                miProfile.setIcon(R.drawable.profile);
                String name = AppApplication.getInstance().getLoginSdk().getUserName();
                if (!SupportUtil.isEmptyOrNull(name))
                    tvProfile.setText(name);
                Util.loadUserImage(AppApplication.getInstance().getLoginSdk().getUserImage(), ivProfile);

            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        handleLoginData();
        active = true;
        callConfig();
/*
        isUserLogin = !SupportUtil.isEmptyOrNull(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO));
        if ( isUserLogin && miProfile != null ){
            miProfile.setTitle( "Profile" );
            miProfile.setIcon( R.drawable.profile );
            String name = SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
            if ( !SupportUtil.isEmptyOrNull(name) )
                tvProfile.setText( name );
            SupportUtil.loadUserImage( SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL) , ivProfile );

        }
*/
    }

    private void initDataFromArgs() {
        if (getIntent() != null) {
            initBundle(getIntent().getExtras());
        }
    }

    public void initBundle(Bundle bundle) {
        if (bundle != null) {
            String s = bundle.getString(AppConstant.TYPE);
            int type = SupportUtil.isEmptyOrNull(s) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt(s);
            if (type == AppConstant.NOTIFICATION_OPEN_ARTICLE) {
                s = bundle.getString(AppConstant.CATEGORY);
                int id = SupportUtil.isEmptyOrNull(s) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt(s);
                s = bundle.getString(AppConstant.DATA);
                int articleId = SupportUtil.isEmptyOrNull(s) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt(s);

                if (id != 0 && articleId != 0) {
                    int position = getCatPosition(id);
                    if (id == AppValues.ID_IMP_UPDATES) {
                        openImpCatActivity("Important Updates", false, articleId);
                    } else if (position == 19 || position == 20) {
                        Intent categoryIntent = new Intent(this, ClassesActivity.class);
                        categoryIntent.putExtra(AppConstant.BOOKTYPE, position == 19 ?
                                AppConstant.NCERTBOOKS : AppConstant.NCERTSOLUTIONENGLISH);
                        startActivity(categoryIntent);
                    } else
                        openCategory(getCatClass(position), position, true, articleId, id);
                }
            } else if (type == AppConstant.NOTIFICATION_API_URL) {
                AppApplication.getInstance().invalidateApiUrl(null, true);
            } else if (type == AppConstant.NOTIFICATION_OPEN_WEB_URL) {
                String webUrl = getIntent().getStringExtra(AppConstant.WEB_URL);
                SupportUtil.openLinkInWebView(this, webUrl, getIntent().getStringExtra(AppConstant.DATA));
            } else if (type == AppConstant.NOTIFICATION_OPEN_PLAY_STORE) {
                String appLink = getIntent().getStringExtra(AppConstant.APP_ID);
                SupportUtil.openAppInPlayStore(this, appLink);
            } else if (type == AppConstant.NOTIFICATION_VERSION_1) {
                handleNotificationVersion1(bundle);
            }
        }
        if (getIntent() != null) {
            getIntent().putExtras(new Bundle());
        }
    }

    private void handleNotificationVersion1(Bundle bundle) {
        String data = bundle.getString(AppConstant.NOTIFICATION_VERSION_1_TYPE);
        if (!SupportUtil.isEmptyOrNull(data)) {
            int version1Type = Integer.parseInt(data);
            switch (version1Type) {
                case AppConstant.NOTIFICATION_VERSION_1_HOME_LIST_CLICK:
                    handleNotificationHomeListClick(bundle);
                    break;
                case AppConstant.NOTIFICATION_VERSION_1_OPEN_ARTICLE:
                    handleNotificationOpenArticle(bundle);
                    break;
                case AppConstant.NOTIFICATION_VERSION_1_PAID_HOME_PAGE:
                    loadPaidModule();
                    break;
                case AppConstant.NOTIFICATION_VERSION_1_PAID_OPEN_PACKAGE:
                    handleNotificationOpenPackage(bundle);
                    break;
                case AppConstant.NOTIFICATION_VERSION_1_OPEN_MOCK_LIST:
                    handleNotificationOpenMockList(bundle);
                    break;
            }
        }
    }

    private void handleNotificationOpenMockList(Bundle bundle) {
        String title = bundle.getString(AppConstant.TITLE);
        if (!SupportUtil.isEmptyOrNull(title)) {
            String data = bundle.getString(AppConstant.CAT_ID);
            if (!SupportUtil.isEmptyOrNull(data)) {
                int id = Integer.parseInt(data);
                String imageUrl = bundle.getString(AppConstant.IMAGE_URL);
                String catTitle = bundle.getString(AppConstant.CATEGORY);
                String isSubCat = bundle.getString(AppConstant.IS_SUB_CAT);
                String host = bundle.getString(AppConstant.HOST);
                if (id != -1 && !SupportUtil.isEmptyOrNull(imageUrl) && !SupportUtil.isEmptyOrNull(catTitle)
                        && !SupportUtil.isEmptyOrNull(isSubCat) && !SupportUtil.isEmptyOrNull(host)) {
                    handleNotificationMockList(catTitle, id, imageUrl, 0
                            , Integer.parseInt(isSubCat) == 0, host);
                }
            }
        }
    }

    private void handleNotificationOpenPackage(Bundle bundle) {
        String data = bundle.getString(AppConstant.CAT_ID);
        if (!SupportUtil.isEmptyOrNull(data)) {
            int id = Integer.parseInt(data);
            String imageUrl = bundle.getString(AppConstant.IMAGE_URL);
            if (id != -1 && !SupportUtil.isEmptyOrNull(imageUrl)) {
                Intent intent = new Intent(this, PackageActivity.class);
                intent.putExtra(AppConstant.IMAGE, imageUrl);
                intent.putExtra(AppConstant.CAT_ID, id);
                startActivity(intent);
            }
        }
    }

    private void handleNotificationHomeListClick(Bundle bundle) {
        String title = bundle.getString(AppConstant.TITLE);
        if (!SupportUtil.isEmptyOrNull(title)) {
            if (title.equalsIgnoreCase(AppConstant.HOME_TITLE_ARRAY[2])) {
                String data = bundle.getString(AppConstant.CAT_ID);
                if (!SupportUtil.isEmptyOrNull(data)) {
                    int id = Integer.parseInt(data);
                    String imageUrl = bundle.getString(AppConstant.IMAGE_URL);
                    String catTitle = bundle.getString(AppConstant.CATEGORY);
                    if (id != -1 && !SupportUtil.isEmptyOrNull(imageUrl) && !SupportUtil.isEmptyOrNull(catTitle)) {
                        handleNotification(catTitle, id, imageUrl);
                    }
                }
            } else {
                String data = bundle.getString(AppConstant.POSITION);
                if (!SupportUtil.isEmptyOrNull(data)) {
                    int position = Integer.parseInt(data);
                    if (position != -1) {
                        handleNotification(title, position);
                    }
                }
            }
        }
    }

    public void handleNotification(String title, int position) {
        int positionParent = AppData.getInstance().getRankList().indexOf(title);
        if (positionParent != -1) {
            SupportUtil.openCategory(positionParent, position - 1, this);
        }
    }

    public void handleNotification(String title, int id, String imageUrl) {
        Class aClass = SupportUtil.getCatClass(AppValues.examTargetQueTypeArray[0]);
        if (aClass != null) {
            Intent intent = new Intent(this, aClass);
            intent.putExtra(AppConstant.CAT_DATA, SupportUtil.getExamTargetCatProperty(title, id, imageUrl, 0));
            startActivity(intent);
        }
    }

    public void handleNotificationMockList(String title, int id, String imageUrl, int position
            , boolean isSubCat, String host) {
        Class aClass = SupportUtil.getCatClass(AppValues.examTargetQueTypeArray[0]);
        if (aClass != null) {
            Intent intent = new Intent(this, aClass);
            intent.putExtra(AppConstant.CAT_DATA, SupportUtil.getProperty(title, id, imageUrl, position, isSubCat, host, true));
            startActivity(intent);
        }
    }


    private void handleNotificationOpenArticle(Bundle bundle) {
        String data = bundle.getString(AppConstant.DATA);
        if (!SupportUtil.isEmptyOrNull(data)) {
            int id = Integer.parseInt(data);
            data = bundle.getString(AppConstant.CAT_ID);
            if (!SupportUtil.isEmptyOrNull(data)) {
                int catId = Integer.parseInt(data);
                data = bundle.getString(AppConstant.WEB_VIEW);
                if (id != -1 && catId != -1 && !SupportUtil.isEmptyOrNull(data)) {
                    String query = DbHelper.COLUMN_CAT_ID + "=" + catId + " AND " + DbHelper.COLUMN_ID + "=" + id;
                    boolean isWebView = Integer.parseInt(data) == 0;
                    Intent intent = new Intent(this, DescActivity.class);
                    intent.putExtra(AppConstant.DATA, id);
                    intent.putExtra(AppConstant.QUERY, query);
                    intent.putExtra(AppConstant.CATEGORY, true);
                    intent.putExtra(AppConstant.CAT_ID, catId);
                    intent.putExtra(AppConstant.WEB_VIEW, isWebView);
                    startActivity(intent);
                }
            }
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            openUpdateDialog(intent.getStringExtra(AppConstant.TITLE), intent.getBooleanExtra(AppConstant.TYPE, false));
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void initViews() {
        tvConnection = (TextView) findViewById(R.id.tv_connection);
        tvConnection.setOnClickListener(this);
        ids = getResources().getIntArray(R.array.main_cat_id);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView.setNavigationItemSelectedListener(this);
        miProfile = navigationView.getMenu().findItem(R.id.nav_login);
        View header = navigationView.getHeaderView(0).findViewById(R.id.ll_header);
        header.setOnClickListener(this);
        tvProfile = (TextView) header.findViewById(R.id.tv_profile);
        ivProfile = (ImageView) header.findViewById(R.id.iv_profile);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_header:
                openProfile(true, 0);
                return;
            case R.id.tv_connection:
                if (SupportUtil.isConnected(this))
                    callConfig();
                else
                    SupportUtil.showToastCentre(this, "Please check your internet connection.");
                return;
            case R.id.rl_menu_imp_update:
                openImpCatActivity("Important Updates", false);
                return;
            default:
                return;

        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.drawer_q);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
    }

    private TextView tvMenuImpUpdateCount;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        View view = menu.findItem(R.id.action_imp_update).getActionView();
        view.setOnClickListener(this);
        tvMenuImpUpdateCount = (TextView) view.findViewById(R.id.tv_menu_imp_update);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                SupportUtil.share("", this);
                return true;
            case R.id.action_imp_update:
                openImpCatActivity("Important Updates", false);
                return true;
            case R.id.action_rate:
                rateUs();
                return true;
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void rateUs() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
        startActivity(rateIntent);
    }

    private void facebook() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/GK-Current-Affairs-1215139171889124/?skip_nax_wizard=true"));
        startActivity(rateIntent);
    }

    private void moreApps() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Mukesh+Kaushik"));
        startActivity(rateIntent);
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openPayActivity(boolean isAdFree, Class aClass) {
        if (SupportUtil.isNotConnected(this)) {
            SupportUtil.showToastInternet(this);
        } else if (AppApplication.getInstance().getLoginSdk() != null && !AppApplication.getInstance().getLoginSdk().isRegComplete()
                && !AppPreferences.getBoolean(this, AppConstant.IS_FIRST_PAID_LOGIN)) {
            openLoginDialog(false, LoginSdk.INTENT_LOGIN);
            AppPreferences.setBoolean(this, AppConstant.IS_FIRST_PAID_LOGIN, true);
        } else if (AppApplication.getInstance().getLoginSdk() != null && AppApplication.getInstance().getLoginSdk().isRegComplete()
                && !AppPreferences.getBoolean(this, AppConstant.IS_FIRST_PAID_EXAM_SELECT)) {
            SupportUtil.openExamPage(this);
        } else {
            Intent intent = new Intent(this, aClass);
            intent.putExtra(AppConstant.TYPE, isAdFree);
            startActivity(intent);
        }
    }

    protected void openLoginDialog(final boolean isOpenEditProfile, final int reqCode) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Login")
                .setMessage("You are not logged in. Please login before enable this  service.")
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openProfile(isOpenEditProfile, reqCode);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setIcon(R.drawable.profile_black);
        builder.setCancelable(false);
        builder.create().show();
    }

    private void openProfile(boolean isOpenEditProfile, int reqCode) {
        LoginSdk.getInstance(this, getPackageName()).openLoginPageCallBack(this, isOpenEditProfile, reqCode , false);
    }

    private void openCategory(Class aClass, int position, boolean isNotification, int articleId, int catId) {
        Intent intent = new Intent(this, aClass);
        intent.putExtra(AppConstant.POSITION, position);
        intent.putExtra(AppConstant.WEB_VIEW, AppConstant.CATEGORY_WEB[position] == AppConstant.INT_TRUE);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.DATA, AppConstant.CATEGORY_EXIST[position] == AppConstant.INT_TRUE);
        if (isNotification) {
            intent.putExtra(AppConstant.CATEGORY, isNotification);
            intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, articleId);
        }
        startActivity(intent);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        drawerLayout.closeDrawers();
//        String s = SharedPrefUtil.getString(AppConstant.SharedPref.PLAYER_ID);
        switch (item.getItemId()) {
            case R.id.nav_share:
                SupportUtil.share("", this);
                break;
            case R.id.nav_rate_us:
            case R.id.nav_update:
                rateUs();
                break;
            case R.id.nav_more_apps:
                moreApps();
                break;
/*
            case R.id.nav_gk :
                openGKApp();
                break;
*/
            case R.id.nav_facebook:
                facebook();
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_leader_board:
                SupportUtil.openMockLeaderBoard(this);
                break;
            case R.id.nav_login:
                openProfile(true, 0);
                break;
        }
        return true;
    }

    private void openGKApp() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getString(R.string.package_other)));
        startActivity(rateIntent);
    }

    private void openImpCatActivity(String s, boolean type) {
        Intent intent = new Intent(this, ImpCategoryActivity.class);
        intent.putExtra(AppConstant.CAT_ID, AppValues.ID_IMP_UPDATES);
        intent.putExtra(AppConstant.TITLE, s);
        intent.putExtra(AppConstant.TYPE, type);
        startActivityForResult(intent, INTENT_IMP_UPDATE);
    }

    private void openImpCatActivity(String s, boolean type, int articleId) {
        Intent intent = new Intent(this, ImpCategoryActivity.class);
        intent.putExtra(AppConstant.CAT_ID, AppValues.ID_IMP_UPDATES);
        intent.putExtra(AppConstant.TITLE, s);
        intent.putExtra(AppConstant.TYPE, type);
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, articleId);
        intent.putExtra(AppConstant.CATEGORY, true);
        startActivityForResult(intent, INTENT_IMP_UPDATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_IMP_UPDATE) {
            SupportUtil.getImpCountFromDB(this);
        } else if (requestCode == INTENT_MY_EXAM && resultCode == RESULT_OK) {
            loadPaidModule();
        } else if (requestCode == LoginSdk.INTENT_LOGIN && resultCode == RESULT_OK) {
            SupportUtil.openExamPage(this, INTENT_MY_EXAM);
        }
    }

    private int[] ids;

    private int getCatPosition(int id) {
        int pos = 0;
        boolean isCat = true;
        for (int c = 0; c < ids.length; c++) {
            if (id == ids[c]) {
                pos = c;
                isCat = false;
            }
        }
        if (isCat) {
            for (int i = 0; i < AppValues.MOCK_CAT_ID.length; i++) {
                if (id == AppValues.MOCK_CAT_ID[i] || id == AppValues.NOTES_CAT_ID[i])
                    pos = AppConstant.POSITION_MARGIN_MIDDLE + i;
            }
        }
        return pos;
    }

    private Class getCatClass(int position) {
        if (position > 5 && position < 10)
            return NotesMCQActivity.class;
        else if (position == 0 || position == 16 || position == 17 || position == 18)
            return MockCategoryActivity.class;
        else if (position == 2)
            return CalenderActivity.class;
        else
            return (position == 13 || position == 15) ? PDFListActivity.class : CategoryActivity.class;
    }


    private void openUpdateDialog(String msg, boolean isTypeSkip) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Update ");
        builder.setMessage(msg)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SupportUtil.rateUs(HomeActivity.this);
                        dialog.dismiss();
                    }
                });
        if (isTypeSkip) {
            builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    public void onImpCount(int count) {
        if (count > 0 && tvMenuImpUpdateCount != null) {
            tvMenuImpUpdateCount.setVisibility(View.VISIBLE);
            tvMenuImpUpdateCount.setText(count + "");
        } else {
            tvMenuImpUpdateCount.setVisibility(View.GONE);
        }
    }
}

