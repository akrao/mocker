package gk.gkcurrentaffairs.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.RetrofitGenerator;
import gk.gkcurrentaffairs.network.ApiEndpointInterface;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by amitsharma on 02/04/16.
 */
public class BrowserActivity extends PageAdsAppCompactActivity {

    private String url = "http://www.bbc.com/hindi/india";
    private ProgressBar progressBar ;
    private boolean isPdf ;
    private ProgressDialog progressDialog ;
    private WebView webView ;
    private String title , baseUrl , baseTitle ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser );
        initDataFromIntent();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    private void initDataFromIntent(){
        Intent intent = getIntent();
        String newsUrl = "";
        if ( intent != null ){
            newsUrl = intent.getStringExtra( AppConstant.DATA );
            isPdf = intent.getBooleanExtra( AppConstant.CATEGORY , false );
            baseTitle = intent.getStringExtra( AppConstant.CLICK_ITEM_ARTICLE );
            if ( !isPdf )
                isPdf = !isPdf( newsUrl ) ;
            title = intent.getStringExtra( AppConstant.TITLE );
            getSupportActionBar().setTitle( title );
        }
        if ( newsUrl != null && !TextUtils.isEmpty( newsUrl ) )
            url = newsUrl ;

        baseUrl = url ;
        if ( isPdf )
            url = AppPreferences.getUrlPdfOpen( this ) + url ;

        progressBar = (ProgressBar) findViewById( R.id.progressBar );
        webView = (WebView) findViewById( R.id.webView );
//        webView.setPictureListener( this );

        webView.setWebChromeClient( new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                super.onConsoleMessage(consoleMessage);
                if (consoleMessage.message().startsWith( TAG ) )
                {
                    if ( consoleMessage.message().toLowerCase().contains("viewable only") && consoleMessage.message().toLowerCase().contains("landscape") ){
                        setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE );
                    }
                }
                return false;
            }
        } );
        webView.setWebViewClient( new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                if ( url.endsWith("viewer.action=download") ) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                    return false;
                }

                view.loadUrl( url );
                return true;
            }
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                progressBar.setVisibility( View.GONE );
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if ( ! url.contains(".pdf") )
                    view.loadUrl("javascript:console.log('" + TAG + "'+document.getElementsByTagName('html')[0].innerHTML);");
            }
        });

        webView.getSettings().setJavaScriptEnabled( true );
        if ( ! url.contains(".pdf") ) {
//            webView.getSettings().setBuiltInZoomControls( true );
            String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
            webView.getSettings().setUserAgentString(newUA);
//            webView.getSettings().setCacheMode( WebSettings.LOAD_NO_CACHE );
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);

            webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            webView.setScrollbarFadingEnabled(false);
        }
        webView.loadUrl( url );
    }

    private String TAG = "AMIT" ;

    private boolean isPdf( String s ){
        return s.toLowerCase().contains("http");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( isPdf ? R.menu.menu_pdf_save :  R.menu.share_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }else if( id == R.id.action_pdf_save ){
            handlePermission();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initSavePDF1(){
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Downloading...." );
        progressDialog.show();
        if ( SupportUtil.isNotConnected( this ) )
            SupportUtil.showToastInternet( this );
        else
            downloadFile();
    }

    private void initSavePDF(){
        if ( SupportUtil.isNotConnected( this ) )
            SupportUtil.showToastInternet( this );
        else
            downloadFile1();
    }

    private void downloadFile1(){
        DownloadManager mManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        String url = AppPreferences.getUrlPdfDownload( this ) + baseUrl;
//        String filename = ( SupportUtil.isEmptyOrNull( baseTitle ) ? title : title + "-" + baseTitle ) +".pdf" ;
        String filename = "GA POWER CAPSULE 2017 FOR BANK And SSC EXAM" +".pdf" ;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
                .setTitle( filename )
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOCUMENTS, filename)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDescription("Downloading...")
                .setMimeType("application/pdf");
        request.allowScanningByMediaScanner();
        mManager.enqueue(request);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            openDialog();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter( AppConstant.CLICK_ITEM_ARTICLE );
        registerReceiver( receiver , intentFilter );
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver( receiver );
    }

    final private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123 ;

    private void handlePermission(){
        if (ContextCompat.checkSelfPermission(BrowserActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BrowserActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }else{
            initSavePDF();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSavePDF();
                } else {
                    // Permission Denied
                    Toast.makeText(BrowserActivity.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setMessage(R.string.pdf_saved )
                .setPositiveButton("See Downloads", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openFile1();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }

    private void openFile1(){
        startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
    }

    private void openFile(){
        File file = new File(filePath());
        String localUri = Uri.fromFile(file).toString() ;
        if (localUri.substring(0, 7).matches("file://")) {
            localUri =  localUri.substring(7);
        }
        file = new File(localUri);
        Uri uri = FileProvider.getUriForFile( this , getPackageName() + ".provider", file );
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType( uri , "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private String filePath(){
        File direct = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + getString( R.string.app_name ) ) ;
        if (!direct.exists()) {
            if (direct.mkdir()) {
            }
        }
        title = SupportUtil.isEmptyOrNull( baseTitle ) ? title : title + "-" + baseTitle ;
        return direct.getAbsolutePath() + "/" + title + ".pdf" ;
    }

    private void downloadFile(){

        ApiEndpointInterface apiEndpointInterface = RetrofitGenerator.getClient(AppApplication.getInstance().getConfigManager().getHostAlias().get(ConfigConstant.HOST_MAIN)).create(ApiEndpointInterface.class);
        Call<ResponseBody> call = apiEndpointInterface.downloadFileWithDynamicUrlSync( AppPreferences.getUrlPdfDownload( this ) + baseUrl );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if ( response.body() != null ) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            boolean writtenToDisk = writeResponseBodyToDisk(response.body());
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            progressDialog.dismiss();
                            openDialog();
                        }
                    }.execute();
                } else {
                    Toast.makeText( BrowserActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText( BrowserActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                progressDialog.dismiss();
            }
        });

    }

    private boolean writeResponseBodyToDisk(ResponseBody body) {
        try {
            File futureStudioIconFile = new File(filePath());
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[1024];
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
               }
               outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

}
