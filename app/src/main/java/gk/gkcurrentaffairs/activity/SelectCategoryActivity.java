package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.SelectCatAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/10/2017.
 */

public class SelectCategoryActivity extends PageAdsAppCompactActivity implements SelectCatAdapter.OnCustomClick, View.OnClickListener {

    private DbHelper dbHelper;
    private RecyclerView.Adapter mAdapter;
    private int catId = 0, image, idFirst, idSec;
    private RecyclerView mRecyclerView;
    private ArrayList<CategoryBean> categoryBeen;
    private boolean isMultiple, isServerOne;
//    private ApiEndpointInterface apiEndpointInterface;
    private String HOST;

    private ArrayList<Integer> integers = new ArrayList<>();
    private String query;
    private ProgressDialog progressDialog;
    private boolean[] catSel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_cat);
        getDataFromArg();
        initViews();
        new DataFromDB().execute();
    }

    private String title , imageUrl ;
    private void getDataFromArg() {
        categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
        isMultiple = getIntent().getBooleanExtra(AppConstant.DATA, true);
        catId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        int position = getIntent().getIntExtra(AppConstant.POSITION, 0);
//        image = AppConstant.IMAGE_RES[position];
        image = categoryProperty.getImageResId();
        imageUrl = categoryProperty.getImageUrl();
        title = getIntent().getStringExtra(AppConstant.TITLE) ;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dbHelper = AppApplication.getInstance().getDBObject();
        isServerOne = catId != AppConstant.ENGLISH_MOCK_TEST_ID;
/*
        apiEndpointInterface = isServerOne ? AppApplication.getInstance().getNetworkObject() :
                AppApplication.getInstance().getNetworkObject_1();
*/
        HOST = isServerOne ? ConfigConstant.HOST_MAIN : ConfigConstant.HOST_TRANSLATOR ;
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading...");
        if (isMultiple) {
            findViewById(R.id.bt_submit).setOnClickListener(this);
            findViewById(R.id.bt_submit).setVisibility(View.VISIBLE);
        }
    }

    private void setUpList() {
        catSel = new boolean[categoryBeen.size()];
        for (int i = 0; i < catSel.length; i++) {
            catSel[i] = false;
        }
        mAdapter = new SelectCatAdapter(categoryBeen, this, catSel , imageUrl);
        ((SelectCatAdapter) mAdapter).setMultiSelect(isMultiple);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCustomItemClick(int position) {
        if (isMultiple) {
            catSel[position] = !catSel[position];
            mAdapter.notifyDataSetChanged();
        } else {
            integers.add(categoryBeen.get(position).getCategoryId());
            handleMCQ();
        }
    }

    private String getQuery() {
        return DbHelper.COLUMN_SUB_CAT_ID + " IN " + toString(integers.toArray());
    }

    private void handleMCQ() {
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                query = getQuery();
                if (dbHelper.getMockTestCatId(query)) {
                    openMCQ(query);
                } else {
                    SupportUtil.downloadNormalMcqQue20(categoryProperty.getHost(), catId, SelectCategoryActivity.this, new SupportUtil.DownloadNormalMcqQue20() {
                        @Override
                        public void onResult(boolean result, String query) {
                            if ( result ){
                                openMCQ(query);
                            }
                        }
                    });
//                    downloadMCQOpen();
                }
                return null;
            }
        });
    }

    private CategoryProperty categoryProperty ;

    private void openMCQ(String s) {
        Intent intent = new Intent(this, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, getMockHomeBean());
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.CATEGORY, false);
        intent.putExtra(AppConstant.QUERY, s);
        startActivity(intent);
        finish();
    }

    private MockHomeBean getMockHomeBean(){
        MockHomeBean mockHomeBean = new MockHomeBean();
        mockHomeBean.setNegativeMarking(0);
        mockHomeBean.setTestMarkes(0);
        mockHomeBean.setSeeAnswer(true);
        mockHomeBean.setTestTime(0);
        mockHomeBean.setQuestMarks(1);
        mockHomeBean.setTitle(title);
        mockHomeBean.setId(catId);
        return mockHomeBean ;
    }


    private void openMCQ() {
        Intent intent = new Intent(this, MCQActivity.class);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.CATEGORY, false);
        intent.putExtra(AppConstant.QUERY, query);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {

        if (catSel != null && catSel.length > 0) {
            for (int i = 0; i < catSel.length; i++) {
                if (catSel[i])
                    integers.add(categoryBeen.get(i).getCategoryId());
            }
        }
        if (integers.size() > 0) {
            handleMCQ();
        } else {
            Toast.makeText(this, "Please select Category", Toast.LENGTH_SHORT).show();
        }
    }


    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    categoryBeen = dbHelper.fetchCategoryData(catId, AppData.getInstance().getImageRes().get(catId), image);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (categoryBeen != null && categoryBeen.size() > 0)
                setUpList();
        }
    }

    public String toString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "()";

        StringBuilder b = new StringBuilder();
        b.append('(');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.append(')').toString();
            b.append(",");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        } else if (id == R.id.action_share) {
            SupportUtil.share("", this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
