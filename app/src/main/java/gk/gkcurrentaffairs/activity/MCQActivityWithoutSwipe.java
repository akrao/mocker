package gk.gkcurrentaffairs.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.MCQCountAdapter;
import gk.gkcurrentaffairs.adapter.MCQQueAdapterNotSwipe;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.bean.ResultDataBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by dennis-01 on 10/23/2015.
 */
public class MCQActivityWithoutSwipe extends PageAdsAppCompactActivity implements View.OnClickListener, MCQQueAdapterNotSwipe.OnCustomOnTouch, MCQCountAdapter.OnItemClick, ViewPager.OnPageChangeListener {

    View llExplanation, vDlg, vOtherOptions, vDlgAnswer , vDlgDivider , vDlgUnAnswer;
    private TextView tvTime, tvCount, tvQuestionMarks, tvNegativeMarks, tvDlgCorrect;
    private ArrayList<MockTestBean> data;
    private int count = 0, correctAns = 0, wrongAns = 0, numberOfQue = 20, catId, minute = AppConstant.MCQ_TIme;
    private long resultId = 0;
    private String query, title, random = DbHelper.COLUMN_ID +  " ASC";
    private DbHelper dbData;
    private boolean isPlay, isDesc, isSolution = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    //    private HashMap<Integer, Integer> dataAns;
    private ArrayList<Integer> dataAns;
    private MCQQueAdapterNotSwipe mcqQueAdapter;
    static boolean active = false;
    private ResultDataBean resultDataBean, preResultDataBean = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq_without_swipe);
        initViews();
        getDataFromIntent();
        if ( mockHomeBean != null ) {
            setupToolbar();
            getModelFromDB();
            SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad) , this , AppConstant.ADS_BANNER );
        } else {
            SupportUtil.showToastCentre(this, "Error in Mock Test. please open the other Test.");
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    private void setAdapterForCount() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
//        mLayoutManager = new LinearLayoutManager(this ,  LinearLayoutManager.HORIZONTAL , false);
        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MCQCountAdapter(count + 1, numberOfQue, this, dataAns);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setElevation(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mcq, menu);
        if (isSolution) {
            menu.findItem(R.id.action_submit).setVisible(false);
            menu.findItem(R.id.action_pause).setVisible(false);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (!isSolution) {
            openDialog(1);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_submit:
                openDialog(0);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_pause:
                openDialog(1);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvCount = (TextView) findViewById(R.id.tv_count);
        tvQuestionMarks = (TextView) findViewById(R.id.tv_question_mark);
        tvNegativeMarks = (TextView) findViewById(R.id.tv_negative_marks);
        llExplanation = findViewById(R.id.ll_explanation);
        vDlg = findViewById(R.id.ll_dlg);
        vOtherOptions = findViewById(R.id.ll_option_holder);
        vDlgAnswer = findViewById(R.id.v_dlg_answer);
        tvDlgCorrect = findViewById(R.id.tv_dlg_correct);
        vDlgDivider = findViewById(R.id.v_dlg_divider);
        vDlgUnAnswer = findViewById(R.id.v_dlg_un_answer);

        findViewById(R.id.ll_prev).setOnClickListener(this);
        findViewById(R.id.ll_next).setOnClickListener(this);
        findViewById(R.id.ll_explanation).setOnClickListener(this);
        findViewById(R.id.ll_rough).setOnClickListener(this);
        findViewById(R.id.ll_que_list).setOnClickListener(this);
        findViewById(R.id.ll_option_extra).setOnClickListener(this);
        findViewById(R.id.ll_bug).setOnClickListener(this);
        findViewById(R.id.dlg_fb_close).setOnClickListener(this);
    }

    private void setDataView() {
        numberOfQue = data.size();
//        dataAns = new HashMap<>(numberOfQue);
        dataAns = new ArrayList<>(numberOfQue);
        ArrayList<String> list;
        if (preResultDataBean != null) {
            list = preResultDataBean.getDataSelect();
            dataAns = preResultDataBean.getDataAns();
        } else {
            list = new ArrayList<>(numberOfQue);
            for (int i = 0; i < numberOfQue; i++) {
                dataAns.add(AppConstant.QUE_NOT_ATTEMPTED);
                    list.add("");
//            dataAns.put( i , AppConstant.QUE_NOT_ATTEMPTED );
            }
        }
        setAdapterForCount();
        setDataOneTime();
        mcqQueAdapter = new MCQQueAdapterNotSwipe(data, this, this);
        mcqQueAdapter.setDataSelect(list);
        mcqQueAdapter.setDataAns(dataAns);
        mcqQueAdapter.setSeeAnswer(mockHomeBean.isSeeAnswer());
        mcqQueAdapter.setSolution(isSolution);
        llMCQParent = findViewById( R.id.ll_mcq );
        mcqQueAdapter.setView(llMCQParent);
        mcqQueAdapter.createView();
        mcqQueAdapter.setPosition(count);
        mcqQueAdapter.loadData();

//        viewPager.setAdapter(mcqQueAdapter);
    }

    private void handleViewVisible(){
        if ( !mockHomeBean.isSeeAnswer() ){
            tvDlgCorrect.setText("Attempted");
            vDlgAnswer.setBackgroundResource(R.drawable.circle_mcq_select);
            vDlgDivider.setVisibility(View.GONE);
            vDlgUnAnswer.setVisibility(View.GONE);
        }
    }
    private View llMCQParent ;

    private void setDataOneTime() {
        tvNegativeMarks.setText("-" + mockHomeBean.getNegativeMarking() + "");
        tvQuestionMarks.setText(mockHomeBean.getQuestMarks() + "");
    }

    private void setQueCount() {
        tvCount.setText((count + 1) + "/" + numberOfQue);
    }

    private void setData() {
        if (data.size() > count && count >= 0) {
            mcqQueAdapter.setPosition(count);
            mcqQueAdapter.loadData();
            refreshList();
            setQueCount();
        }
    }

    private void refreshList() {
        mAdapter = new MCQCountAdapter(count + 1, numberOfQue, this, dataAns);
        mRecyclerView.setAdapter(mAdapter);
        if (count > 6)
            mLayoutManager.scrollToPosition(count - 2);
        if (isDesc && dataAns.get(count) != AppConstant.QUE_NOT_ATTEMPTED && mockHomeBean.isSeeAnswer())
            llExplanation.setVisibility(SupportUtil.isEmptyOrNull(data.get(count).getDescription()) ? View.GONE : View.VISIBLE);
        else
            llExplanation.setVisibility(View.GONE);
    }

    private void bugReport() {
        Intent intent = new Intent(this, BugReportActivity.class);
        intent.putExtra(AppConstant.TITLE, data.get(count).getOptionQuestion());
        intent.putExtra(AppConstant.CAT_ID, data.get(count).getId());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_explanation:
                openExplanation();
                break;
            case R.id.ll_rough:
                startActivity(new Intent(this, RoughEditorActivity.class));
                break;
            case R.id.dlg_fb_close:
                vDlg.setVisibility(View.GONE);
                break;
            case R.id.ll_bug:
                bugReport();
                break;
            case R.id.ll_option_extra:
                vOtherOptions.setVisibility(vOtherOptions.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
            case R.id.ll_que_list:
                vDlg.setVisibility(vDlg.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
            case R.id.ll_prev:
                if (count == 0) {
                    showToast("There is no previous Questions.");
                } else {
                    count--;
                    setData();
                }
//                refreshList();
                break;
            case R.id.ll_next:
                if (count == numberOfQue - 1) {
                    openDialog(0);
                } else {
                    count++;
                    setData();
                }
//                refreshList();
                break;
        }
    }

    private void openExplanation() {
        if (data != null && data.get(count) != null && data.get(count).getDescription() != null &&
                !TextUtils.isEmpty(data.get(count).getDescription()))
            openExplanationDialog();
        else
            Toast.makeText(this, "No Explanation", Toast.LENGTH_SHORT).show();
    }

    private void openExplanationDialog() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_explanation);
            WebView webView = (WebView) dialog.findViewById(R.id.dlg_wv_exp);
            mcqQueAdapter.setDataWebView(webView, data.get(count).getDescription(), false);
            (dialog.findViewById(R.id.dlg_tv_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
        }
    }

    private void openDialog(final int flag) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setMessage((flag == 0 && !isSolution) ? R.string.mcq_submission : R.string.mcq_back)
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (flag == 0 && !isSolution)
                            proceedForResult();
                        else
                            insertDataAndFinish();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }

    private void insertDataAndFinish(){
        finalizeData();
        new UpdateResult().execute();
    }

    private class UpdateResult extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            dbData.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    Gson gson = new Gson();
                    String json = gson.toJson(resultDataBean);
                    dbData.updateResult(resultId, json);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }

    private void proceedForResult() {
        finalizeData();
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(AppConstant.MOCK_DATA, mockHomeBean);
        intent.putExtra(AppConstant.CORRECT_ANS, correctAns);
        intent.putExtra(AppConstant.NUM_QUE, numberOfQue);
        intent.putExtra(AppConstant.WRONG_ANS, wrongAns);
        intent.putExtra(AppConstant.TITLE, title);
        intent.putExtra(AppConstant.DATA , resultId);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.QUERY, true);
        intent.putExtra(AppPreferences.PLAY_LEVEL, isPlay);
        intent.putExtra(AppConstant.RESULT_DATA, resultDataBean);
        startActivity(intent);
        finish();
    }

    private void finalizeData() {
        int length = dataAns.size();
        for (int i = 0; i < length; i++) {
            if (dataAns.get(i) == AppConstant.QUE_ATTEMPTED_CORRECT)
                correctAns++;
            else if (dataAns.get(i) == AppConstant.QUE_ATTEMPTED_WRONG)
                wrongAns++;
        }
        resultDataBean.setMockHomeBean(mockHomeBean);
        resultDataBean.setCatId(catId);
        resultDataBean.setCorrectAns(correctAns);
        resultDataBean.setWrongAns(wrongAns);
        resultDataBean.setNumberOfQue(numberOfQue);
        resultDataBean.setTestTitle(title);
        resultDataBean.setMockTestId(mockTestId);
        resultDataBean.setQuery(query);
        resultDataBean.setDataAns(dataAns);
        resultDataBean.setDataSelect(mcqQueAdapter.getDataSelect());
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private int mockTestId;
    MockHomeBean mockHomeBean;

    private void getDataFromIntent() {
        dbData = AppApplication.getInstance().getDBObject();
        Intent intent = getIntent();
        if (intent != null) {
            resultDataBean = new ResultDataBean();
            mockHomeBean = (MockHomeBean) intent.getSerializableExtra(AppConstant.MOCK_DATA);
            if (mockHomeBean != null) {
                catId = intent.getIntExtra(AppConstant.CAT_ID, 0);
//            isDesc = catId == AppValues.CAQ_ID ;
                isDesc = true;
                query = intent.getStringExtra(AppConstant.QUERY);
                title = intent.getStringExtra(AppConstant.TITLE);
                mockTestId = intent.getIntExtra(AppConstant.DATA, 0);
                boolean isResult = intent.getBooleanExtra(AppConstant.CATEGORY, true);
                isPlay = intent.getBooleanExtra(AppPreferences.PLAY_LEVEL, false);
                isSolution = intent.getBooleanExtra(AppConstant.SOLUTION, false);
                int que = intent.getIntExtra(AppConstant.COUNT_QUE, 0);
                if (intent.getSerializableExtra(AppConstant.RESULT_DATA) != null
                        && intent.getSerializableExtra(AppConstant.RESULT_DATA).getClass() == ResultDataBean.class) {
                    preResultDataBean = (ResultDataBean) intent.getSerializableExtra(AppConstant.RESULT_DATA);
                }
                if (isResult && !isSolution)
                    new InsertResult().execute();
                else if ( !isSolution )
                    random = "RANDOM() LIMIT " + dbData.QUE_NUM;
/*
                else
                    random = "RANDOM() LIMIT " + dbData.QUE_NUM;
*/
                int time = intent.getIntExtra(AppConstant.CLICK_ITEM_ARTICLE, -1);
                resultDataBean.setTime(time);
//                if (time != -1 && !isSolution) {
                if (!isSolution) {
                    minute = minute - time;
                    startCountDown();
                }
                handleViewVisible();
               } else {
                SupportUtil.showToastCentre(this, "Error in Mock Test. please open the other Test.");
                finish();
            }
        }

    }

    private void getModelFromDB() {
        data = new ArrayList<>();
        new DownloadDataFromDB(this).execute();
    }

    @Override
    public void onCustomOnTouch(View v, boolean flag, ArrayList<Integer> dataAns) {
//    public void onCustomOnTouch(View v , boolean flag , HashMap<Integer, Integer> dataAns ) {
        this.dataAns = dataAns;
        if (flag)
            refreshList();
        else
            showToast("You cannot change your answer.");
    }

    @Override
    public void OnItemClick(int position) {
        count = position;
        vDlg.setVisibility(View.GONE);
        setData();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mcqQueAdapter.setFullDesc(false);
        mcqQueAdapter.setPosition(position);
        mcqQueAdapter.loadData();
//        mcqQueAdapter.notifyDataSetChanged();
        count = position;
        setData();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class DownloadDataFromDB extends AsyncTask<Void, Void, Void> {

        private ProgressDialog progressDialog;
        private Context context;

        public DownloadDataFromDB(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Progressing...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbData.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    data = dbData.fetchMockTestByTestId(query, random);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (data != null && data.size() > 0) {
                setDataView();
                setData();
            } else {
                Toast.makeText(MCQActivityWithoutSwipe.this, "Error, Please try later", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private class InsertResult extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            dbData.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    resultId = dbData.insertResult(title, catId, System.currentTimeMillis(), mockTestId);
                    return null;
                }
            });
            return null;
        }
    }

    private void startCountDown() {
        tvTime.setVisibility(View.VISIBLE);
        if (mockHomeBean.getTestTime() == 0) {
            startCountDownAsc();
        } else {
            minute = mockHomeBean.getTestTime() ;
            startCountDownDsc();
        }
    }

    int secCount = 0;

    private void startCountDownAsc() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (active) {
                    secCount++;
                    int sec = secCount % 60 ;
                    int minuteCount = secCount / 60;
//                    String min = (minuteCount < 10) ? "0" + minuteCount : minuteCount + "";
                    tvTime.setText(String.format("%02d" , minuteCount) + ":" + String.format("%02d" , sec));
                } else {
                    this.cancel();
                }
            }

            public void onFinish() {
                if (active) {
                    startCountDownAsc();
                }
            }
        }.start();

    }

    private void startCountDownDsc() {
        if (minute >= 0) {
            new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                    if (active) {
                        String sec = (millisUntilFinished / 1000 < 10) ? "0" + millisUntilFinished / 1000 : millisUntilFinished / 1000 + "";
                        String min = (minute < 10) ? "0" + (minute-1) : (minute-1) + "";
//                    tvTime.setText( "Time Remaining : " + min + ":" + sec );
                        tvTime.setText(min + ":" + sec);
                    } else {
                        this.cancel();
                    }
                }

                public void onFinish() {
                    if (active) {
                        if (minute == 0) {
                            proceedForResult();
                        } else {
                            minute--;
                            startCountDown();
                        }
                    }
                }
            }.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }
}
