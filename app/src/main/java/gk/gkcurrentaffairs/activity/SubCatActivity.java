package gk.gkcurrentaffairs.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;

import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.fragment.CategoryFragment;
import gk.gkcurrentaffairs.fragment.MockCategoryFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/13/2017.
 */

public class SubCatActivity extends PageAdsAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_cat);
        int catID = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        int subCatID = getIntent().getIntExtra(AppConstant.DATA, 0);
        int image = getIntent().getIntExtra(AppConstant.IMAGE, 0);
        String imageUrl = getIntent().getStringExtra(AppConstant.IMAGE_URL);
        boolean isMock = getIntent().getBooleanExtra(AppConstant.CATEGORY, false);
        boolean isTypePDF = getIntent().getBooleanExtra(AppConstant.CLICK_ITEM_ARTICLE, false);
        boolean isWebView = getIntent().getBooleanExtra(AppConstant.WEB_VIEW, true);
        CategoryProperty categoryProperty = null ;
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null
                && getIntent().getSerializableExtra(AppConstant.CAT_DATA).getClass() == CategoryProperty.class ){
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
        }

        String title = getIntent().getStringExtra(AppConstant.TITLE);
        setupToolbar(title);
        setCategoryData(catID, subCatID, image, isMock, isTypePDF, isWebView , categoryProperty , imageUrl);
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }

    private void setCategoryData(int catID, int subCatID, int image, boolean isMock, boolean isTypePDF, boolean isWebView
            , CategoryProperty categoryProperty , String imageUrl) {
        if (catID != 0) {
            Fragment categoryFragment;
            if (!isMock) {
                categoryFragment = new CategoryFragment();
            } else {
                categoryFragment = new MockCategoryFragment();
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstant.CAT_DATA, categoryProperty);
            bundle.putInt(AppConstant.CAT_ID, catID);
            bundle.putInt(AppConstant.DATA, subCatID);
            bundle.putInt(AppConstant.IMAGE, image);
            bundle.putString(AppConstant.IMAGE_URL, imageUrl);
//            bundle.putSerializable(AppConstant.PROPERTY, categoryProperty);
            if (!isMock) {
                bundle.putBoolean(AppConstant.CLICK_ITEM_ARTICLE, isTypePDF);
            } else {
                bundle.putBoolean(AppConstant.CLICK_ITEM_ARTICLE, isExam(catID));
            }
            bundle.putString(AppConstant.QUERY, DbHelper.COLUMN_SUB_CAT_ID + "=" + subCatID);
            bundle.putBoolean(AppConstant.WEB_VIEW, isWebView);
            categoryFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, categoryFragment).commit();
        }

    }

    private boolean isExam(int catId) {
        return catId == AppValues.SSC_ID || catId == AppValues.BANK_ID || catId == AppValues.RRB_ID;
    }

    private void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!SupportUtil.isEmptyOrNull(title)) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
