package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.ImpCatListAdapter;
import gk.gkcurrentaffairs.bean.ImpCatListBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/29/2017.
 */

public class ImpCategoryActivity extends PageAdsAppCompactActivity implements ImpCatListAdapter.OnCustomClick, ImpCatListAdapter.OnLoadMore ,
        SwipeRefreshLayout.OnRefreshListener{

    private DbHelper dbHelper;
    private boolean isFirst = true , isBookmark ;
    private List<ImpCatListBean> list = new ArrayList<>();
    private int catId ;
    private String query , title ;
    private ImpCatListAdapter adapter ;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View viewLoadMore;
    private boolean canLoadMore ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list );
        initDataFromArg();
        setupToolbar();
        initObjects();
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }

    private void openImpCatActivity( String s , boolean type ){
        Intent intent = new Intent( this , ImpCategoryActivity.class );
        intent.putExtra( AppConstant.CAT_ID , AppValues.ID_IMP_UPDATES );
        intent.putExtra( AppConstant.TITLE , s );
        intent.putExtra( AppConstant.TYPE , type );
        startActivity(intent );
    }

    @Override
    protected void onStart() {
        super.onStart();
        new DataFromDb(null,false).execute();
    }

    private void initObjects(){
        dbHelper = AppApplication.getInstance().getDBObject();
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        viewLoadMore = findViewById(R.id.ll_load_more);
    }

    private void loadMoreData() {
        if ( viewLoadMore.getVisibility() == View.GONE && list != null && list.size() > 19 && canLoadMore) {
            viewLoadMore.setVisibility(View.VISIBLE);
            fetchData(false);
        }
    }


    private void updateFavStatus(final int position) {
        final boolean status = list.get(position).isFav();
        list.get(position).setFav(!status);
        adapter.notifyDataSetChanged();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.updateArticle(list.get(position).getId(), dbHelper.COLUMN_FAV,
                        status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
                return null;
            }
        });
    }


    @Override
    public void onRefresh() {
        if ( !isBookmark )
            fetchData(true);
    }

    private void showData(){
        viewLoadMore.setVisibility(View.GONE);
        canLoadMore = true ;
        if ( adapter == null ) {
            findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
            RecyclerView recyclerView = (RecyclerView) findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new ImpCatListAdapter( list , null , this );
            adapter.setCatId(catId);
            adapter.setActivity(this);
            adapter.setQuery(query);
            recyclerView.setAdapter( adapter );
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void showNoData(){
        viewLoadMore.setVisibility(View.GONE);
        canLoadMore = false ;
        findViewById( R.id.player_progressbar ).setVisibility(View.GONE);
        findViewById( R.id.tv_no_data ).setVisibility(View.VISIBLE);
    }

    private void fetchData(final boolean isLatest){
        Map<String, String> map = new HashMap<>(2);
        map.put("id", catId + "");
        map.put("max_content_id", (isLatest ? AppConstant.MAX_VALUE : list.get(list.size()-1).getId()) + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_PREVIOUS_CONTENT_BY_CAT_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        isFirst = false;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            List<ServerBean> list = null;
                            try {
                                list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerBean>>() {
                                }.getType());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }

                            if (list != null && list.size() > 0) {
                                new DataFromDb(list,isLatest).execute();
                            } else if (!(list != null && list.size() > 0)) {
                                showNoData();
                            }
                            swipeRefreshLayout.setRefreshing(false);
                        }else if (!(list != null && list.size() > 0)) {
                            showNoData();
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if ( !isBookmark ) {
            getMenuInflater().inflate(R.menu.menu_imp_notes, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void initDataFromArg(){
        if ( getIntent() != null ){
            isBookmark = getIntent().getBooleanExtra(AppConstant.TYPE , false );
            title = getIntent().getStringExtra(AppConstant.TITLE );
            catId = getIntent().getIntExtra( AppConstant.CAT_ID , AppValues.ID_IMP_UPDATES);
            boolean isNotification = getIntent().getBooleanExtra( AppConstant.CATEGORY , false );
            if ( isNotification ){
                int articleId = getIntent().getIntExtra( AppConstant.CLICK_ITEM_ARTICLE , 0 );
                if ( articleId != 0 ){
                    Intent intent = new Intent( this , DescActivity.class);
                    intent.putExtra( AppConstant.DATA, articleId );
                    intent.putExtra( AppConstant.QUERY, DbHelper.COLUMN_ID +"=" + articleId + " AND " + DbHelper.COLUMN_CAT_ID +"=" + catId );
                    intent.putExtra( AppConstant.CAT_ID, catId );
                    intent.putExtra( AppConstant.CATEGORY, isNotification );
                    intent.putExtra( AppConstant.WEB_VIEW , true);
                    startActivity(intent);
                }
            }
        }
        query = DbHelper.COLUMN_CAT_ID +"=" + catId ;
        if ( isBookmark )
            query += " AND " + DbHelper.COLUMN_FAV + "=" + AppConstant.INT_TRUE ;
    }

    @Override
    public void onCustomClick(int position, int type) {
        switch ( type ){
            case AppConstant.CLICK_SHARE:
                SupportUtil.share( list.get(position).getTitle() , this);
                break;
            case AppConstant.CLICK_ARTICLE:
                openDesc(position);
                break;
            case AppConstant.CLICK_SAVE:
                updateFavStatus(position);
                break;

        }
    }

    private void openDesc(int position){
        Intent intent = new Intent(this, DescActivity.class);
        intent.putExtra( AppConstant.DATA, list.get(position).getId() );
        intent.putExtra( AppConstant.QUERY, query );
        intent.putExtra( AppConstant.CAT_ID, catId );
        intent.putExtra( AppConstant.WEB_VIEW , true);
        startActivity(intent);

    }

    @Override
    public void onLoadMore() {
        if ( SupportUtil.isConnected(this) ) {
            loadMoreData();
        }
    }

    class DataFromDb extends AsyncTask<Void , Void , Void> {

        private List<ServerBean> listBeen ;
        private boolean isLatest ;

        public DataFromDb(List<ServerBean> listBeen,boolean isLatest) {
            this.listBeen = listBeen;
            this.isLatest = isLatest;
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if ( listBeen != null && listBeen.size() > 0 ){
                        dbHelper.insertArticle( listBeen , catId , false , isLatest );
                    }
                    dbHelper.fetchImpCatData( list , query , catId , true );
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( list != null && list.size() > 0 ){
                showData();
            }else if ( isBookmark ){
                showNoData();
            }
            if ( isFirst && !isBookmark )
                fetchData(true);
        }
    }

    private void setupToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!SupportUtil.isEmptyOrNull(title)) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }else if (id == R.id.action_important_notes) {
            openImpCatActivity( getSupportActionBar().getTitle().toString() , true );
        }
        return super.onOptionsItemSelected(item);
    }

}
