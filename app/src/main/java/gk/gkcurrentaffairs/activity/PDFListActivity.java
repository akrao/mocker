package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.concurrent.Callable;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ListBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.fragment.CategoryFragment;
import gk.gkcurrentaffairs.fragment.CategoryListFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/6/2017.
 */

public class PDFListActivity extends PageAdsAppCompactActivity implements ViewPager.OnPageChangeListener {

    private int catID, articleId;
    private String title = "";
    private int position, image;
    private boolean isCategory, isWebView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initDataFromArg();
        setupToolbar();
        setupViewPager();
        setupCollapsingToolbar();
//        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    private CategoryProperty categoryProperty ;
    private void initDataFromArg() {
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null ) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
            initObjects();
        }else {
            initDataForUnStructure();
        }
    }

    private void initObjects(){
        catID = categoryProperty.getId() ;
        isCategory = categoryProperty.isSubCat() ;
        isWebView = categoryProperty.isWebView() ;
        title = categoryProperty.getTitle() ;
        image = categoryProperty.getImageResId() ;
    }

    private void initDataForUnStructure(){
        position = getIntent().getIntExtra(AppConstant.POSITION, 1);
        catID = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        isCategory = getIntent().getBooleanExtra(AppConstant.DATA, true);
        title = getResources().getStringArray(R.array.main_cat_name)[position];
        image = AppConstant.IMAGE_RES[position];
        boolean isNotification = getIntent().getBooleanExtra(AppConstant.CATEGORY, false);
        if (isNotification) {
            articleId = getIntent().getIntExtra(AppConstant.CLICK_ITEM_ARTICLE, 0);
            if (articleId != 0) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Downloading ...");
                handleNotificationArticle();
            }
        }
    }

    private void setupCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(false);
    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    CategoryActivity.ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new CategoryActivity.ViewPagerAdapter(getSupportFragmentManager());
        CategoryFragment latest = new CategoryFragment();
        CategoryFragment favourite = new CategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.CAT_ID, catID);
        bundle.putInt(AppConstant.IMAGE, image);
        bundle.putInt(AppConstant.POSITION, position);
        bundle.putBoolean(AppConstant.WEB_VIEW, isWebView);
        bundle.putBoolean(AppConstant.CLICK_ITEM_ARTICLE, true);
        if (catID != 0)
            bundle.putString(AppConstant.QUERY, DbHelper.COLUMN_CAT_ID + "=" + catID);

        latest.setArguments(bundle);
        adapter.addFrag(latest, "Latest");

        if (isCategory) {
            CategoryListFragment categoryListFragment = new CategoryListFragment();
            categoryListFragment.setArguments(bundle);
            adapter.addFrag(categoryListFragment, "Category");
        }

        Bundle bundle1 = new Bundle();
        bundle1.putInt(AppConstant.CAT_ID, catID);
        bundle1.putInt(AppConstant.IMAGE, image);
        bundle1.putBoolean(AppConstant.WEB_VIEW, isWebView);
        bundle1.putBoolean(AppConstant.CLICK_ITEM_ARTICLE, true);
        bundle1.putInt(AppConstant.POSITION, position);

        if (catID != 0) {
            String q = DbHelper.COLUMN_FAV + "=" + DbHelper.INT_TRUE
                    + " AND " + DbHelper.COLUMN_CAT_ID + "=" + catID;
            bundle1.putString(AppConstant.QUERY, q);
        }
        favourite.setArguments(bundle1);
        adapter.addFrag(favourite, "Fav");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof CategoryFragment) {
            ((CategoryFragment) fragment).refreshFragment();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void handleNotificationArticle() {
        AppApplication.getInstance().getDBObject().callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ListBean listBean = AppApplication.getInstance().getDBObject().getArticle(articleId, catID);
                if (listBean == null) {
                    downloadArticle();
                } else {
                    openPdf(listBean);
                }
                return null;
            }
        });
    }

    private void downloadArticle() {
        try {
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SupportUtil.downloadArticle(articleId + "", new SupportUtil.onArticleResponse() {
            @Override
            public void onCustomResponse(boolean result, ServerBean serverBean) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result) {
                    ListBean listBean = new ListBean();
                    listBean.setText(serverBean.getTitle());
                    listBean.setDate(serverBean.getId() + "/" + serverBean.getPdf());
                    openPdf(listBean);
                } else {
                    Toast.makeText(PDFListActivity.this, "Error please try later", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }, catID, false);
    }

    private void openPdf(ListBean listBean) {
        if (!SupportUtil.isEmptyOrNull(listBean.getDate())) {
            Intent intent = new Intent(this, BrowserActivity.class);
            intent.putExtra(AppConstant.DATA, listBean.getDate());
            intent.putExtra(AppConstant.CATEGORY, true);
            intent.putExtra(AppConstant.TITLE, listBean.getText());
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
