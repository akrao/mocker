package gk.gkcurrentaffairs.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.print.PdfPrint;
import android.print.PrintAttributes;
import android.text.Html;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.DetailPagerAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.HomeBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

public class DescActivity extends PageAdsAppCompactActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private DbHelper dbHelper;
    private ViewPager viewPager;
    private DetailPagerAdapter pagerAdapter;
    private ArrayList<HomeBean> beanArrayList = new ArrayList<>();
    private int clickedId, catId;
    private int position;
    private boolean isDayMode, isWebView;
    private FloatingActionButton fbMain, fbShare, fbFont, fbMode;
    private View mainView;
    private String query = "";
    private MenuItem menuFav , menuOption;
    private RelativeLayout rlAds;
    private LinearLayout llMore;
    private TextView tvReadDetails, tvApplyOnline;
    private boolean isNotification = false, isFirst = true;
    private ProgressDialog progressDialog;
    private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desc);
        setupToolbar();
        initView();
        initDataSet();
        SupportUtil.initAds(rlAds, this, AppConstant.ADS_BANNER);

        if (SupportUtil.isNotConnected(this))
            SupportUtil.showToastInternet(this);
        initReceiver();
    }

    private void initReceiver(){
        if ( isNotification ) {
            registerReceiver( broadcastReceiver, new IntentFilter(getPackageName()+ ConfigConstant.CONFIG_LOADED));
        }
    }
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (beanArrayList.size() > 0)
                downloadArticle();

                try {
                unregisterReceiver(broadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Article");
    }


    private void initView() {
        viewPager = (ViewPager) findViewById(R.id.vp_detail);
        fbMain = (FloatingActionButton) findViewById(R.id.fb_main);
        fbShare = (FloatingActionButton) findViewById(R.id.fb_share);
        fbFont = (FloatingActionButton) findViewById(R.id.fb_font);
        fbMode = (FloatingActionButton) findViewById(R.id.fb_mode);
        fbMain.setOnClickListener(this);
        fbShare.setOnClickListener(this);
        fbFont.setOnClickListener(this);
        fbMode.setOnClickListener(this);
        mainView = findViewById(R.id.content_main);
        viewPager.addOnPageChangeListener(this);
        rlAds = (RelativeLayout) findViewById(R.id.ll_ad);

        tvApplyOnline = (TextView) findViewById(R.id.tv_apply_online);
        tvReadDetails = (TextView) findViewById(R.id.tv_read_details);
        llMore = (LinearLayout) findViewById(R.id.ll_more);
        tvApplyOnline.setOnClickListener(this);
        tvReadDetails.setOnClickListener(this);
    }

    HashMap<Integer, String> hashMapCategoryNames = null;
    private void initCategoryNames() {
        if (dbHelper != null) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    ArrayList<CategoryBean> categoryBeen = dbHelper.MockFetchCategoryData(DbHelper.COLUMN_CAT_ID + "=" + catId, null, R.mipmap.ic_launcher);
                    if (categoryBeen != null && categoryBeen.size() > 0) {
                        hashMapCategoryNames = new HashMap<>(categoryBeen.size());
                        for (CategoryBean beanData : categoryBeen) {
                            hashMapCategoryNames.put(beanData.getCategoryId(), beanData.getCategoryName());
                        }
                    }
                    return null;
                }
            });
        }
    }

    private void initDataSet() {
        dbHelper = AppApplication.getInstance().getDBObject();
        isDayMode = AppPreferences.isDayMode(this);
        query = getIntent().getStringExtra(AppConstant.QUERY);
        clickedId = getIntent().getIntExtra(AppConstant.DATA, 0);
        catId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        isWebView = getIntent().getBooleanExtra(AppConstant.WEB_VIEW, false);
        isNotification = getIntent().getBooleanExtra(AppConstant.CATEGORY, false);
        mainView.setBackgroundColor(isDayMode ? Color.WHITE : Color.BLACK);
        initCategoryNames();
        new DownloadDataFromDB().execute();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading...");
        typeface = AppApplication.getInstance().getTypeface();
    }

    private void setData() {
        pagerAdapter = new DetailPagerAdapter(this, beanArrayList, AppPreferences.getFontSize(this) , hashMapCategoryNames);
        pagerAdapter.setDayMode(isDayMode);
        pagerAdapter.setWebView(isWebView);
        if (catId == AppConstant.EDITORIAL_ID || SupportUtil.isEnglish(this))
            pagerAdapter.setTypeface(typeface);
        String lang = null;
        if (catId == AppValues.CAA_ID)
            lang = getString(R.string.trans_type);
        else if (catId == AppConstant.EDITORIAL_ID)
            lang = getString(R.string.trans_type_eng);
        pagerAdapter.setToConvert(lang);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(position);
        checkFav();
        checkRead();
        handleMoreData();
        setModeColorTextView(AppPreferences.isDayMode(this) ? Color.DKGRAY : Color.WHITE);
        if (!SupportUtil.isEmptyOrNull(lang) && AppPreferences.getDescOverlay(this))
            findViewById(R.id.overlay).setVisibility(View.VISIBLE);

    }

    private void checkFav() {
        boolean status = beanArrayList.get(position).isFav();
        if (menuFav != null) {
            menuFav.setIcon(status ? R.drawable.like_fill : R.drawable.like_non_fill);
        }
    }

    private void checkRead() {
        if (!beanArrayList.get(position).isRead())
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle(beanArrayList.get(position).getId(), dbHelper.COLUMN_READ, dbHelper.INT_TRUE, catId);
                    return null;
                }
            });
    }

    private void handleMoreData() {
        HomeBean bean = beanArrayList.get(position);
        if (SupportUtil.isEmptyOrNull(bean.getApplyOnline()) && SupportUtil.isEmptyOrNull(bean.getReadDetails()))
            llMore.setVisibility(View.GONE);
        else
            llMore.setVisibility(View.VISIBLE);

        tvApplyOnline.setVisibility(SupportUtil.isEmptyOrNull(bean.getApplyOnline()) ? View.GONE : View.VISIBLE);
        tvReadDetails.setVisibility(SupportUtil.isEmptyOrNull(bean.getReadDetails()) ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_desc, menu);
        menuFav = menu.findItem(R.id.action_fav);
        menuOption = menu.findItem(R.id.action_option);
        if (beanArrayList.size() > position) {
            boolean status = beanArrayList.get(position).isFav();
            if (menuFav != null)
                menuFav.setIcon(status ? R.drawable.like_fill : R.drawable.like_non_fill);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_fav) {
            updateFavStatus();
            return true;
        }else if (id == R.id.action_option) {
            handleFloatingVisibility();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateFavStatus() {
        if ( beanArrayList != null && beanArrayList.size() > position && dbHelper != null && menuFav != null ) {
            final boolean status = beanArrayList.get(position).isFav();
            menuFav.setIcon(status ? R.drawable.like_non_fill : R.drawable.like_fill);
            beanArrayList.get(position).setFav(!status);
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle(beanArrayList.get(
                            position).getId(), dbHelper.COLUMN_FAV,
                            status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
                    return null;
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fb_main:
                handleFloatingVisibility();
                break;
            case R.id.fb_share:
                share();
                break;
            case R.id.fb_font:
                openFontCustomizeDialog();
                break;
            case R.id.fb_mode:
                openModeCustomizeDialog();
                break;
            case R.id.dlg_minus_font:
                handleFontSize(AppPreferences.getFontSize(this) - 1, AppPreferences.getZoomSize(this) - 10);
                break;
            case R.id.dlg_plus_font:
                handleFontSize(AppPreferences.getFontSize(this) + 1, AppPreferences.getZoomSize(this) + 10);
                break;
            case R.id.tv_apply_online:
                if (beanArrayList != null && beanArrayList.size() > position
                        && !SupportUtil.isEmptyOrNull(tvApplyOnline.getText().toString())) {
                    openBrowser(beanArrayList.get(position).getApplyOnline(), tvApplyOnline.getText().toString());
                }
                break;
            case R.id.tv_read_details:
                if (beanArrayList != null && beanArrayList.size() > position && !SupportUtil.isEmptyOrNull(tvReadDetails.getText().toString()) ) {
                    openBrowser(beanArrayList.get(position).getReadDetails(), tvReadDetails.getText().toString());
                }
                break;
        }
    }

    private void openBrowser(String url, String title) {
        if ( !SupportUtil.isEmptyOrNull(url) ) {
            Intent intent;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !url.contains("http") ) {
            if (!url.contains("http") ) {
    /*
                intent = new Intent(this, PDFViewerActivity.class);
                intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, AppPreferences.getUrlPdfDownload(this) + url);
                intent.putExtra(AppConstant.DATA, catId + "-" + beanArrayList.get(position).getId() + ".pdf");
                intent.putExtra(AppConstant.TITLE, title);
    */
                intent = new Intent(this, AndroidDownloadFileByProgressBarActivity.class);
                String[] parts = url.split("/");
                intent.putExtra(AppConstant.imageName, parts[1]);
                intent.putExtra(AppConstant.BOOKID, Integer.parseInt(parts[0]));
                intent.putExtra(AppConstant.isAdvanced, false);
                intent.putExtra(AppConstant.CATEGORY, AppPreferences.getBaseUrl(this));
                intent.putExtra(AppConstant.HOST, ConfigConstant.HOST_DOWNLOAD_PDF);
            } else {
                intent = new Intent(this, BrowserActivity.class);
                intent.putExtra(AppConstant.DATA, url);
                intent.putExtra(AppConstant.CATEGORY, false);
                intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, beanArrayList.get(position).getTitle());
            }
//        Intent intent = new Intent( this , BrowserActivity.class );
//        intent.putExtra( AppConstant.DATA ,url );
//        intent.putExtra( AppConstant.TITLE , title );
//        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , beanArrayList.get( position ).getTitle() );
            startActivity(intent);
        }
    }

    private void handleFontSize(int fontSize, int zoom) {
        if (isWebView) {
            AppPreferences.setZoomSize(this, zoom);
        } else {
            AppPreferences.setFontSize(this, fontSize);
        }
        updatePager();
    }

    private void handleMode(boolean flag) {
        AppPreferences.setDayMode(this, flag);
        isDayMode = flag;
//        mainView.setBackgroundColor(isDayMode ? Color.WHITE : Color.BLACK);
        if ( pagerAdapter != null ) {
            pagerAdapter.setDayMode(isDayMode);
        }else {
            return;
        }
        updatePager();
        setModeColorTextView(flag ? Color.DKGRAY : Color.WHITE);
    }

    public void overlayOnClick(View v) {
        v.setVisibility(View.GONE);
        AppPreferences.setDescOverlay(this, false);
    }

    private void setModeColorTextView(int color) {
        tvReadDetails.setTextColor(color);
        tvApplyOnline.setTextColor(color);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public static final int REQUEST_EXTERNAL_STORAGE_PERMISSION = 11;

    private void share() {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                shareContent();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_PERMISSION);
            }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareContent();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void openFontCustomizeDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_font, null);
        dialogView.findViewById(R.id.dlg_minus_font).setOnClickListener(this);
        dialogView.findViewById(R.id.dlg_plus_font).setOnClickListener(this);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void openModeCustomizeDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dlg_select_mode, null);
        final CheckBox cbDay = (CheckBox) dialogView.findViewById(R.id.dlg_cb_day);
        final CheckBox cbNight = (CheckBox) dialogView.findViewById(R.id.dlg_cb_night);
        (AppPreferences.isDayMode(this) ? cbDay : cbNight).setChecked(true);
        dialogView.findViewById(R.id.dlg_rl_day).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!AppPreferences.isDayMode(DescActivity.this)) {
                    handleMode(true);
                    cbDay.setChecked(true);
                    cbNight.setChecked(false);
                }
            }
        });
        dialogView.findViewById(R.id.dlg_rl_night).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppPreferences.isDayMode(DescActivity.this)) {
                    handleMode(false);
                    cbDay.setChecked(false);
                    cbNight.setChecked(true);
                }
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void handleFloatingVisibility() {
        if (fbShare.getVisibility() == View.VISIBLE) {
            fbMain.setImageResource(R.drawable.plus_white);
            menuOption.setIcon(R.drawable.plus_white);
            setFloatingVisibility(View.GONE);
        } else {
            fbMain.setImageResource(R.drawable.cross);
            menuOption.setIcon(R.drawable.cross);
            setFloatingVisibility(View.VISIBLE);
        }
    }

    private void setFloatingVisibility(int visibility) {
        fbShare.setVisibility(visibility);
        fbFont.setVisibility(visibility);
        fbMode.setVisibility(visibility);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int index) {
        position = index;
        if (beanArrayList.get(index).getId() == 0) {
            rlAds.setVisibility(View.GONE);
            fbMain.setVisibility(View.GONE);
            fbMain.setImageResource(R.drawable.plus_white);
            setFloatingVisibility(View.GONE);
            if (menuFav != null)
                menuFav.setVisible(false);
        } else {
            if (menuFav != null)
                menuFav.setVisible(true);
            checkRead();
            checkFav();
            fbMain.setVisibility(View.GONE);
            rlAds.setVisibility(View.VISIBLE);
        }
        handleMoreData();
        sendGA(beanArrayList.get(index));
    }

    private void sendGA(HomeBean bean) {
//        AnalyticsUtil.generateGAItemClickArticle( bean.getTitle() , categoryNames.get( Integer.parseInt( bean.getCategoryId() ) )  , catId , false );
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void updatePager() {
        try {
            pagerAdapter.setTextSize(AppPreferences.getFontSize(this));
            pagerAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class DownloadDataFromDB extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    position = dbHelper.fetchDescData(beanArrayList, query, clickedId, catId , isWebView );
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (beanArrayList.size() > 0)
                setData();
            else if (isNotification && isFirst) {
                isFirst = !isFirst;
                downloadArticle();
            } else
                showErrorMessage();
        }
    }

    private ProgressDialog pDialog;

    private void initDialog() throws Exception {
        try {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Creating file");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareContent() {
        try {
            initDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isWebView) {
            if (Build.VERSION.SDK_INT > 20) {
                saveWebPageToPDF();
            } else {
                SupportUtil.share(beanArrayList.get(position).getTitle() + "\n", DescActivity.this);
            }
        } else {
            if (Build.VERSION.SDK_INT > 18) {
                saveViewToPDF();
            }else
                SupportUtil.share( getStringDesc() , this );
        }
        try {
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void saveViewToPDF(){
        try {
            if ( viewPager != null ) {
                PdfDocument document = new PdfDocument();
                View content = viewPager.findViewWithTag(position);
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int height = size.y;
                if ( content != null && content.findViewById( R.id.sv_detail ) != null ) {
                    ScrollView scrollView = (ScrollView) content.findViewById( R.id.sv_detail );

                    int cHeight = scrollView.getChildAt(0).getHeight();
                    int count = cHeight > height ? height / cHeight : 1 ;
                    PdfDocument.PageInfo info = new PdfDocument.PageInfo.Builder(content.getMeasuredWidth() , cHeight , count).create();

                    PdfDocument.Page page = document.startPage(info);
                    content.draw(page.getCanvas());
                    document.finishPage(page);
                    final String fileName = catId + "-" + beanArrayList.get(position).getId() + ".pdf";
                    final String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory;
                    File file = new File(filePath, fileName);
                    document.writeTo(new FileOutputStream(file));
                    document.close();
                    share(file);
                }
            }
//            SupportUtil.share(DescActivity.this, Uri.fromFile(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getStringDesc(){
        String s ;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            s = Html.fromHtml(beanArrayList.get(position).getDesc(), Html.FROM_HTML_MODE_LEGACY).toString() ;
        } else {
            s = Html.fromHtml(beanArrayList.get(position).getDesc()).toString() ;
        }
        return s ;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void saveWebPageToPDF() {
        if ( beanArrayList != null && beanArrayList.size() > position ) {
            final String fileName = catId + "-" + beanArrayList.get(position).getId() + ".pdf";
            final String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory;
            final File path = new File(filePath);
            String jobName = getString(R.string.app_name);
            WebView webView = (WebView) viewPager.findViewWithTag(position).findViewById(R.id.wv_desc);
            PrintAttributes attributes = new PrintAttributes.Builder()
                    .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                    .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 600, 600))
                    .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();
            PdfPrint pdfPrint = new PdfPrint(attributes);
            pdfPrint.print(webView.createPrintDocumentAdapter(jobName), path, fileName, new PdfPrint.PDFSaveInterface() {
                @Override
                public void onSaveFinished() {
                    //sharePDF(fullPath);
                    share(new File(filePath, fileName));
                    //                showPDFSaveDialog("PDF saved to Documents", filePath);
                }
            });
        }
    }

    private void share( File file ){
        SupportUtil.share(DescActivity.this,
                FileProvider.getUriForFile(AppApplication.getInstance() , getPackageName() +".provider" , file ));
    }

    public void showPDFSaveDialog(String message, final String pdfPath) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert");

        //alertDialogBuilder.setIcon(R.drawable.notification_template_icon_bg);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
            }
        });

        alertDialogBuilder.setNegativeButton("Share", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                share(new File(pdfPath));
//                SupportUtil.share(DescActivity.this, Uri.parse(pdfPath));
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showErrorMessage() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    private void downloadArticle() {
        if (SupportUtil.isConnected(this)) {
            try {
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ( AppApplication.getInstance().getConfigManager().isConfigLoaded() ) {
                SupportUtil.downloadArticle(clickedId + "", new SupportUtil.onArticleResponse() {
                    @Override
                    public void onCustomResponse(boolean result, ServerBean serverBean) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (result)
                            new DownloadDataFromDB().execute();
                        else {
                            finish();
                        }
                    }
                }, catId, false);
            }
        }
    }

}
