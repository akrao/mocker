package gk.gkcurrentaffairs.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SimpleDrawingView;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/7/2017.
 */

public class RoughEditorActivity extends PageAdsAppCompactActivity {

    RelativeLayout parentView ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rough_edit );
        initView();
    }

    private void initView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Rough Work");
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
        parentView = ( RelativeLayout ) findViewById( R.id.sdv_rough );
        invalidateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(  R.menu.menu_rough , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }else if( id == R.id.action_clear ){
            invalidateView();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    private void invalidateView(){
        parentView.removeAllViews();
        parentView.addView( new SimpleDrawingView( this ) );
    }

}
