package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/3/2017.
 */

public class NewsPaperActivity extends PageAdsAppCompactActivity implements CategoryListAdapter.OnCustomClick{

    private String[] names ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_list );
        initDataFromArg();
        setupToolBar();
        initData();
    }

    private CategoryProperty categoryProperty ;
    private void initDataFromArg() {
        if ( getIntent().getSerializableExtra( AppConstant.CAT_DATA ) != null ) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
            initObjects();
        }else {
            initDataForUnStructure();
        }
    }

    private void initObjects(){
        title = categoryProperty.getTitle() ;
    }

    String title ;
    private void initDataForUnStructure(){
        int position = getIntent().getIntExtra( AppConstant.POSITION, 14 );
        title = getResources().getStringArray( R.array.main_cat_name )[ position ];
    }

    private void setupToolBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle( title );
    }

    private void initData(){
        names = getResources().getStringArray( R.array.newspaper_list );
        ArrayList<CategoryBean> categoryBeen = new ArrayList<>( names.length );
        CategoryBean bean ;
        for ( String name : names ){
            bean = new CategoryBean();
            bean.setCategoryImage( R.drawable.news );
            bean.setCategoryName( name );
            categoryBeen.add( bean );
        }
        initView( categoryBeen );
    }

    private void initView( ArrayList<CategoryBean> categoryBeen ){
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager( new LinearLayoutManager( this ) );
        CategoryListAdapter mAdapter = new CategoryListAdapter( categoryBeen, this , R.layout.item_list_image_text );
        mRecyclerView.setAdapter( mAdapter );
    }

    @Override
    public void onCustomItemClick(int position) {
        Intent intent = new Intent( this , BrowserActivity.class );
        intent.putExtra( AppConstant.DATA , getResources().getStringArray( R.array.newspaperUrl )[ position ] );
        intent.putExtra( AppConstant.TITLE , names[ position ] );
        startActivity( intent );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

}
