package gk.gkcurrentaffairs.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.ncert.CheckForSDCard;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.ncert.activity.NCERTBrowserActivity.progress_bar_type;


public class AndroidDownloadFileByProgressBarActivity extends PageAdsAppCompactActivity {

    Button btnShowProgress;
    LinearLayout ll_download;
    TextView tv_download_percentage;
    private static final String TAG = "Download Task";
    File apkStorage = null;
    File outputFile = null;
    String downloadFileName = "", baseUrl;
    int BookId = 0;
    Boolean isAdvanced = true;
    // File url to download
    private String file_url = "";
    private boolean isActive ;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maindownlaod);
        // Ads here
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.adViewtop), this, AppConstant.ADS_BANNER);
        //get data from intent
        getDataIntent();
        init();
        if (shouldAskPermissions()) {
            askPermissions();
        } else {
            afterPermissionCode();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false ;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true ;
    }

    void init() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("PDF");
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Make gradient actionbar color
        ActionBar actionBar = getSupportActionBar();
       // SupportUtil.actionBarColor(this,actionBar);*/

        btnShowProgress = (Button) findViewById(R.id.btnProgressBar);
        ll_download = findViewById(R.id.ll_download);
        tv_download_percentage = findViewById(R.id.tv_download_percentage);
    }


    private void afterPermissionCode() {
        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + downloadFileName;
        File file = new File(filePath);

        if (file.exists() && checkFileIsCompleteDownloaded(filePath)) {
            checkvisibility(true);
            downloadAndOpen();
        } else {
            RelativeLayout rlNativeAd = (RelativeLayout) findViewById(R.id.full_ad) ;
            SupportUtil.loadNativeAds(rlNativeAd , R.layout.native_pager_ad_app_install , false);
            checkvisibility(false);
            file_url = baseUrl + "download-pdf/" + BookId + "/" + downloadFileName;
            btnShowProgress.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // starting new Async Task
                    if (!SupportUtil.isNotConnected(AndroidDownloadFileByProgressBarActivity.this))
                        if (ConfigManager.getInstance().isConfigLoaded()
                                && ConfigManager.getInstance().getHostAlias() != null
                                && !SupportUtil.isEmptyOrNull(ConfigManager.getInstance().getHostAlias().get(HOST))) {
                        Logger.e( "File Download Url : " + file_url );
                            new DownloadFileFromURL().execute(file_url);
                        } else {
                            SupportUtil.showToastCentre(AndroidDownloadFileByProgressBarActivity.this, "Please try later");
                        }
                    else
                        SupportUtil.showToastInternet(AndroidDownloadFileByProgressBarActivity.this);

                }
            });
        }
    }

    void checkvisibility(Boolean isPdfDownloaded) {

        if (isPdfDownloaded) {
            btnShowProgress.setVisibility(View.GONE);
            //btnonlinepdf.setVisibility(View.GONE);
//            wv_onlinepdf.setVisibility(View.VISIBLE);
        } else {
            btnShowProgress.setVisibility(View.VISIBLE);
            //btnonlinepdf.setVisibility(View.VISIBLE);
//            wv_onlinepdf.setVisibility(View.GONE);
        }

    }

/*
    void OnlinePdfView(){
        wv_onlinepdf.getSettings().setJavaScriptEnabled(true);
        wv_onlinepdf.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv_onlinepdf.setWebViewClient( new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                view.loadUrl(url);
                return true ;
            }

        });
        wv_onlinepdf.loadUrl(AppPreferences.getBaseUrl(AppApplication.getInstance())+"show-pdf/"+BookId+"/"+downloadFileName);
        checkvisibility(true);

    }
*/

    private void grantAllUriPermissions(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    void downloadAndOpen() {
        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + downloadFileName;
        File file = new File(filePath);
        if (file.exists()) {
            Intent intent = null;
            if (isAdvanced) {
//            intent = new Intent(this,MuPDFActivity.class);
            } else {
                intent = new Intent(Intent.ACTION_VIEW);
                Uri path = Uri.fromFile(file);
            }

            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkURI = FileProvider.getUriForFile(AndroidDownloadFileByProgressBarActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                intent.setDataAndType(apkURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                grantAllUriPermissions(AndroidDownloadFileByProgressBarActivity.this, intent, apkURI);

            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {

                if (isAdvanced) {
                    Uri uri = Uri.fromFile(file);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(uri);
                    startActivity(intent);
                    finish();
                } else {
                    startActivity(intent);
                }
                this.finish();
            } catch (ActivityNotFoundException e) {
                showLocationDialog();
                Toast.makeText(AndroidDownloadFileByProgressBarActivity.this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Showing Dialog
     */
    private void showLocationDialog() {
        if ( AndroidDownloadFileByProgressBarActivity.this != null && isActive ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AndroidDownloadFileByProgressBarActivity.this, R.style.MyDialogTheme);
            builder.setTitle(getString(R.string.dialog_title));
            builder.setMessage(getString(R.string.dialog_message));

            String positiveText = getString(android.R.string.ok);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // positive button logic
                            SupportUtil.downloadPdfReader(AndroidDownloadFileByProgressBarActivity.this);
                        }
                    });

            String negativeText = getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // negative button logic
                            checkvisibility(false);
                        }
                    });

            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnShowProgress.setVisibility(View.GONE);
            ll_download.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                if (new CheckForSDCard().isSDCardPresent()) {
                    apkStorage = new File(Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory);
                } else
                    Toast.makeText(AndroidDownloadFileByProgressBarActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    // Log.e(TAG, "Directory Created.");
                }
                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File
                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                Logger.e("File Download : " + f_url[0]);
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(outputFile);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                //Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @SuppressLint("SetTextI18n")
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            tv_download_percentage.setText("" + Integer.parseInt(progress[0]) + " % ");
        }

        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            try {
                dismissDialog(progress_bar_type);
            } catch (Exception e) {
                // e.printStackTrace();
            }
            downloadAndOpen();
            checkvisibility(true);
            Toast.makeText(AndroidDownloadFileByProgressBarActivity.this, "download complete", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected boolean shouldAskPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                afterPermissionCode();
            } else {
                finish();
            }
        }
    }

    @TargetApi(23)
    protected void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    private String HOST;

    public void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            BookId = bundle.getInt(AppConstant.BOOKID);
            downloadFileName = bundle.getString(AppConstant.imageName);
            HOST = bundle.getString(AppConstant.HOST);
            baseUrl = bundle.getString(AppConstant.CATEGORY);
            isAdvanced = bundle.getBoolean(AppConstant.isAdvanced, true);
        } else {
            finish();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_classes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkFileIsCompleteDownloaded(String path) {
/*
        core = openFile(path);
        if(core==null){
            File fdelete = new File(path);
            if (fdelete.exists()) {
                fdelete.delete();
            }
            return false;
        }else if (core != null && core.countPages() == 0){
            core = null;
            File fdelete = new File(path);
            if (fdelete.exists()) {
                fdelete.delete();
            }
            return false;
        }
*/
        return true;
    }

/*
    private MuPDFCore openFile(String path)
    {
        int lastSlashPos = path.lastIndexOf('/');
       String mFileName = new String(lastSlashPos == -1
                ? path
                : path.substring(lastSlashPos+1));
        System.out.println("Trying to open " + path);
        try
        {
            core = new MuPDFCore(this, path);
            // New file: drop the old outline data
            OutlineActivityData.set(null);
        }
        catch (Exception e)
        {
            System.out.println(e);
            return null;
        }
        catch (OutOfMemoryError e)
        {
            System.out.println(e);
            return null;
        }
        return core;
    }
*/

}
