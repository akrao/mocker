package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.adssdk.PageAdsAppCompactActivity;

import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/7/2018.
 */

public class NotesMcqOnlyActivity extends PageAdsAppCompactActivity implements View.OnClickListener {

    private CategoryProperty categoryProperty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_mcq_only);
        initDataFromIntent();
        initView();
    }

    private void initDataFromIntent() {
        categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
        parentId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        if (categoryProperty != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(categoryProperty.getTitle());
        } else {
            SupportUtil.showToastCentre(this, "Error, please try later.");
            onBackPressed();
        }
    }

    private void initView() {
        findViewById(R.id.ll_notes).setOnClickListener(this);
        findViewById(R.id.ll_mcq).setOnClickListener(this);
    }

    private int parentId;

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, SubCatActivity.class);
        int id = 0;
        switch (view.getId()) {
            case R.id.ll_notes:
                id = AppValues.NOTES_CAT_ID[categoryProperty.getPosition()];
                parentId = AppValues.NOTES_CAT_ID[0];
                categoryProperty.setId(parentId);
//                parentId = AppValues.NOTES_CAT_ID[categoryProperty.getPosition()-4];
                break;
            case R.id.ll_mcq:
                intent.putExtra(AppConstant.CATEGORY, true);
                id = AppValues.MOCK_CAT_ID[categoryProperty.getPosition()];
                parentId = AppValues.MOCK_CAT_ID[0];
                categoryProperty.setId(parentId);
//                parentId = AppValues.MOCK_CAT_ID[categoryProperty.getPosition()-4];
                break;
        }
        intent.putExtra(AppConstant.CAT_DATA, categoryProperty);
        intent.putExtra(AppConstant.CAT_ID, parentId);
        intent.putExtra(AppConstant.DATA, id);
        intent.putExtra(AppConstant.IMAGE, categoryProperty.getImageResId());
        intent.putExtra(AppConstant.TITLE, categoryProperty.getTitle());
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, false);
        intent.putExtra(AppConstant.WEB_VIEW, true);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

