package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.AdsAppCompactActivity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.login.LoginSdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.MCQCountAdapter;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.ResultDataBean;
import gk.gkcurrentaffairs.bean.TestRankBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.piechart.PieHelper;
import gk.gkcurrentaffairs.piechart.PieView;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.config.ConfigConstant.HOST_LEADER_BOARD;

public class ResultActivity extends AdsAppCompactActivity implements View.OnClickListener {

    private PieView pieViewBrief, pvAccuracy, pvScore;
    private int correctAns, wrongAns, remainingAns, numberQue, catId;
    private long resultId, totalTimeLong = 0;
    double marks = 0;
    private TextView tvCorrect, tvWrong, tvUnattended, tvTimeTaken, tvTitle, tvAccuracy, tvScore, tvListCorrect, tvListWrong, tvListSkip, tvRankScore, tvScoreCount;
    private String result, timeTaken, title;
    private boolean isPlay;
    private ResultDataBean resultDataBean;
    private View llQueAnalysisBottom , vPolicy;
    private TestRankBean testRankBean = null;
    private DbHelper dbHelper;
    private Button btLeaderBoard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setupToolbar("Result");
        initViews();
        initDataFromIntent();
//        handleRanking();
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }


    private void initViews() {
        pieViewBrief = (PieView) findViewById(R.id.pie_view_brief);
        pvAccuracy = (PieView) findViewById(R.id.pie_view_accuracy);
        pvScore = (PieView) findViewById(R.id.pie_view_score);
        tvCorrect = (TextView) findViewById(R.id.tv_correct);
        tvWrong = (TextView) findViewById(R.id.tv_wrong);
        tvTitle = (TextView) findViewById(R.id.item_tv_title);
        tvTimeTaken = (TextView) findViewById(R.id.item_tv_time_taken);
        tvUnattended = (TextView) findViewById(R.id.tv_unattended);
        tvAccuracy = (TextView) findViewById(R.id.tv_accuracy);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvListCorrect = (TextView) findViewById(R.id.tv_list_correct);
        tvListWrong = (TextView) findViewById(R.id.tv_list_wrong);
        tvListSkip = (TextView) findViewById(R.id.tv_list_skip);
        tvRankScore = (TextView) findViewById(R.id.tv_score_rank);
        tvScoreCount = (TextView) findViewById(R.id.tv_score_rank_count);
        llQueAnalysisBottom = findViewById(R.id.ll_que_analysis_bottom);

        findViewById(R.id.tv_reattempt).setOnClickListener(this);
        findViewById(R.id.tv_view_solution).setOnClickListener(this);
        findViewById(R.id.iv_close).setOnClickListener(this);
        vPolicy = findViewById(R.id.iv_privacy_policy);
        vPolicy.setOnClickListener(this);
        btLeaderBoard = (Button) findViewById(R.id.bt_leader_board);
        btLeaderBoard.setOnClickListener(this);

        dbHelper = AppApplication.getInstance().getDBObject();
        colorGreen = SupportUtil.getColor( R.color.green_mcq_paid , this );
        colorRed = SupportUtil.getColor( R.color.wrong_red , this );
        colorYellow = SupportUtil.getColor( R.color.graph_yellow , this );
    }

    private boolean isUserLogin;
    MockHomeBean mockHomeBean ;
    private void initDataFromIntent() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            mockHomeBean = (MockHomeBean) intent.getSerializableExtra(AppConstant.MOCK_DATA);
            correctAns = intent.getIntExtra(AppConstant.CORRECT_ANS, 0);
            wrongAns = intent.getIntExtra(AppConstant.WRONG_ANS, 0);
            numberQue = intent.getIntExtra(AppConstant.NUM_QUE, 20);
            resultId = intent.getLongExtra(AppConstant.DATA, 0);
            boolean isUpdate = intent.getBooleanExtra(AppConstant.QUERY, false);
            catId = intent.getIntExtra(AppConstant.CAT_ID, 0);

            title = intent.getStringExtra(AppConstant.TITLE);
            if (SupportUtil.isEmptyOrNull(title))
                title = "";
            timeTaken = intent.getStringExtra(AppConstant.CLICK_ITEM_ARTICLE);
            setTime();

            isPlay = intent.getBooleanExtra(AppPreferences.PLAY_LEVEL, false);
            resultDataBean = (ResultDataBean) intent.getSerializableExtra(AppConstant.RESULT_DATA);
            if (resultDataBean != null && resultDataBean.getDataAns() != null
                    && resultDataBean.getDataAns().size() > 0) {
                handleQueData();
            }

            remainingAns = numberQue - correctAns - wrongAns;
            if (resultId != 0 && isUpdate) {
                new UpdateResult().execute();
                llQueAnalysisBottom.setVisibility(View.VISIBLE);
            }
//            marks = ((5 * (float) correctAns) - (2.5 * (float) wrongAns));
            marks = (5 * (float) correctAns);

            if (isPlay) {
                int minMarks = (AppConstant.MCQ_PLAY_MIN_MARKS + (AppPreferences.getPlayLevel(this, catId) * 3));
                boolean isPass = (marks >= minMarks);
                result = isPass ? " - Pass. Unlocked new Level" : " - Fail. You need minimum "
                        + minMarks + "% to unlock new level.";
                if (isPass)
                    handlePlay();
                else
                    Toast.makeText(this, "Sorry , Try Again", Toast.LENGTH_LONG).show();
            }
            setScore();
            setDataInViews();
        } else {
            finish();
        }
    }

    private double score ;

    public void setScore() {
        if ( mockHomeBean != null ) {
            this.score = ( ( correctAns * mockHomeBean.getQuestMarks() ) - ( wrongAns * mockHomeBean.getNegativeMarking() ) ) ;
        }else {
            score = correctAns ;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if ( AppApplication.getInstance().getLoginSdk() != null ) {
            isUserLogin = AppApplication.getInstance().getLoginSdk().isRegComplete();
//            btLeaderBoard.setVisibility(isUserLogin ? View.VISIBLE : View.GONE );
            vPolicy.setVisibility(isUserLogin ? View.VISIBLE : View.GONE );
            if (!isUserLogin)
                handleUserLogin();
            else if (totalTimeLong > 0) {
                syncResult();
            }
        }
    }

    private void handleUserLogin() {
        boolean isOpenUserRegistration = !SharedPrefUtil.getBoolean(AppConstant.RESULT_DATA);
        if (isOpenUserRegistration) {
            if ( AppApplication.getInstance().getLoginSdk() != null ) {
                LoginSdk.getInstance(this , getPackageName()).openLoginPage(this , false );
            }
/*
            Intent intent = new Intent(this, SupportUtil.getUserProfileClass(this));
            intent.putExtra(AppConstant.TYPE, false);
            startActivity(intent);
*/
            SharedPrefUtil.setBoolean(AppConstant.RESULT_DATA, true);
        }
    }

    private void handleQueData() {
        findViewById(R.id.ll_holder_que_analysis).setVisibility(View.VISIBLE);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        ArrayList<Integer> dataAns = resultDataBean.getDataAns();
        mRecyclerView.setAdapter(new MCQCountAdapter(dataAns.size() + 1, dataAns.size(), null, dataAns));
    }

    private void handleRanking() {
        findViewById(R.id.ll_rank_holder).setVisibility(View.VISIBLE);
        findViewById(R.id.v_divider_score).setVisibility(View.VISIBLE);
        TextView tvRank = (TextView) findViewById(R.id.tv_rank);
        TextView tvRankTotal = (TextView) findViewById(R.id.tv_rank_total);
        tvRank.setText(testRankBean.getUserTestRank() + "");
        tvRankTotal.setText("Out of " + testRankBean.getTotalCount());
    }

    private void handlePlay() {
        Toast.makeText(this, "Congrats , You have unlocked the new level", Toast.LENGTH_LONG).show();
        if (AppPreferences.getPlayLevel(this, catId) < 16)
            AppPreferences.setPlayLevel(this, AppPreferences.getPlayLevel(this, catId) + 1, catId);
    }

    private void setChartBrief() {
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        pieHelperArrayList.add(new PieHelper(getPercentage(correctAns) , colorGreen));
        pieHelperArrayList.add(new PieHelper(getPercentage(wrongAns) , colorRed));
        pieHelperArrayList.add(new PieHelper(getPercentage(remainingAns) , colorYellow));
        pieViewBrief.setData(pieHelperArrayList);
    }

    private void setChartAccuracy() {
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        int accuracy = accuracyPercentage();
        pieHelperArrayList.add(new PieHelper(accuracy , colorGreen)  );
        pieHelperArrayList.add(new PieHelper(100 - accuracy, colorRed ) );
        pvAccuracy.setData(pieHelperArrayList);
        tvAccuracy.setText("Accuracy : " + accuracy + " %");
    }

    private int colorYellow , colorGreen , colorRed ;
    private void setChartScore() {
        if ( score > 0 ) {
            ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
            int score = getPercentage(correctAns);
            pieHelperArrayList.add(new PieHelper(score,colorGreen));
            pieHelperArrayList.add(new PieHelper(100 - getPercentage(correctAns) , colorRed));
            pvScore.setData(pieHelperArrayList);
            tvScore.setText("Score : " + score + " %");
        }else {
            pvScore.setVisibility(View.GONE);
            tvScore.setVisibility(View.GONE);
        }
    }
/*
    private void setChartBrief() {
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        pieHelperArrayList.add(new PieHelper(getPercentage(correctAns)));
        pieHelperArrayList.add(new PieHelper(getPercentage(wrongAns)));
        pieHelperArrayList.add(new PieHelper(getPercentage(remainingAns)));
        pieViewBrief.setData(pieHelperArrayList);
    }

    private void setChartAccuracy() {
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        int accuracy = accuracyPercentage();
        pieHelperArrayList.add(new PieHelper(accuracy));
        pieHelperArrayList.add(new PieHelper(100 - accuracy));
        pvAccuracy.setData(pieHelperArrayList);
        tvAccuracy.setText("Accuracy : " + accuracy + " %");
    }

    private void setChartScore() {
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        int score = getPercentage(correctAns);
        pieHelperArrayList.add(new PieHelper(score));
        pieHelperArrayList.add(new PieHelper(100 - getPercentage(correctAns)));
        pvScore.setData(pieHelperArrayList);
        tvScore.setText("Score : " + score + " %");
    }
*/

    private int accuracyPercentage() {
        float per = (correctAns / (float) (correctAns + wrongAns)) * 100;
        return (int) per;
    }

    private void setDataInViews() {
        tvTitle.setText(isPlay ? title + result : title);
        tvCorrect.setText("" + correctAns);
        tvWrong.setText("" + wrongAns);
        tvUnattended.setText("" + remainingAns);
        tvListCorrect.setText(tvListCorrect.getText().toString() + correctAns);
        tvListWrong.setText(tvListWrong.getText().toString() + wrongAns);
        tvListSkip.setText(tvListSkip.getText().toString() + remainingAns);

/*
        tvScoreCount.setText("Out of " + numberQue);
        tvRankScore.setText(correctAns + "");
*/
        if (mockHomeBean != null) {
            tvScoreCount.setText("Out of " + (numberQue * mockHomeBean.getQuestMarks() ));
        }else {
            tvScoreCount.setText("Out of " + numberQue );
        }

        tvRankScore.setText( score + "");

        setChartBrief();
        setChartAccuracy();
        setChartScore();
    }

    private void setupToolbar(String title) {
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle( title );
*/
    }


    private int getPercentage(float value) {
        float per = (value / (float) numberQue) * 100;
        return (int) per;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_share) {
            SupportUtil.share("", this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setTime() {
        if (!SupportUtil.isEmptyOrNull(timeTaken)) {
            tvTimeTaken.setVisibility(View.VISIBLE);
            tvTimeTaken.setText("Time Taken : " + timeTaken);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_reattempt :
                openMCQ(resultDataBean, false);
                break;
            case R.id.tv_view_solution :
                openMCQ(resultDataBean, true);
                break;
            case R.id.iv_privacy_policy :
                SupportUtil.openPolicy(this);
                break;
            case R.id.bt_leader_board :
                if ( AppApplication.getInstance().getLoginSdk()!= null && AppApplication.getInstance().getLoginSdk().isRegComplete() ) {
                    SupportUtil.openMockLeaderBoard(this);
                }else if ( AppApplication.getInstance().getLoginSdk() != null ) {
                        LoginSdk.getInstance(this , getPackageName()).openLoginPage(this , false );
                }
                break;
            case R.id.iv_close :
                onBackPressed();
                break;
        }
    }

    private void openMCQ(final ResultDataBean resultDataBean, boolean isSolution) {
        Intent intent = new Intent(this, MCQActivityWithoutSwipe.class);
        if ( mockHomeBean != null ) {
            intent.putExtra(AppConstant.MOCK_DATA, mockHomeBean);
            intent.putExtra(AppConstant.CAT_ID, catId);
            intent.putExtra(AppConstant.SOLUTION, isSolution);
            intent.putExtra(AppConstant.TITLE, resultDataBean.getTestTitle());
            intent.putExtra(AppConstant.DATA, resultDataBean.getMockTestId());
            intent.putExtra(AppConstant.QUERY, resultDataBean.getQuery());
            intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, resultDataBean.getTime());
            if (!isSolution) {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.upDateMockDb(dbHelper.COLUMN_ATTEMPTED, resultDataBean.getMockTestId(), catId);
                        return null;
                    }
                });
            } else
                intent.putExtra(AppConstant.RESULT_DATA, resultDataBean);

            startActivity(intent);
            if (!isSolution)
                finish();
        }else {
            SupportUtil.showToastCentre( this , "Sorry, This feature is not available on this." );
        }
    }

    private class UpdateResult extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    totalTimeLong = dbHelper.updateResult(resultId, numberQue, remainingAns, correctAns, wrongAns,
                            System.currentTimeMillis(), getQueData());
                    timeTaken = dbHelper.timeTaken(totalTimeLong);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setTime();
            syncResult();
        }
    }

    private class UpdateTestSync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.upDateMockDb(dbHelper.IS_SYNC, resultDataBean.getMockTestId(), catId);
                    return null;
                }
            });
            return null;
        }

    }

    private String getQueData() {
        Gson gson = new Gson();
        String json = gson.toJson(resultDataBean);
        return json;
    }

    private void syncResult() {
        if (isUserLogin) {
            if ( AppApplication.getInstance().getLoginSdk() != null ) {
                Map<String, String> map = new HashMap<>(9);
                map.put("all", numberQue + "");
                map.put("right", correctAns + "");
                map.put("wrong", wrongAns + "");
                map.put("time", totalTimeLong + "");
                map.put("cat_id", catId + "");
                map.put("test_id", resultDataBean.getMockTestId() + "");
                map.put("user_id", LoginSdk.getInstance().getUserId() + "");
                map.put("application_id", getPackageName() );
                map.put("raw_data", getQueData() );

                AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_POST, HOST_LEADER_BOARD
                        , ApiEndPoint.SAVE_GAME_RESULT
                        , map, new ConfigManager.OnNetworkCall() {
                            @Override
                            public void onComplete(boolean status, String data) {
                                if (status && !SupportUtil.isEmptyOrNull(data)) {
                                    try {
                                        TestRankBean testRankBean1 = ConfigManager.getGson().fromJson(data, TestRankBean.class);
                                        if ( testRankBean1 != null && testRankBean1.getStatus().equalsIgnoreCase("success")  ) {
                                            testRankBean = testRankBean1 ;
                                            handleRanking();
                                        }
                                    } catch (JsonSyntaxException e) {
                                        e.printStackTrace();
                                    }
                                    new UpdateTestSync().execute();
                                }
                            }
                        });
            }

/*
            AppApplication.getInstance().getNetworkObjectPay().getMockTestRanking(
                    numberQue, correctAns, wrongAns, totalTimeLong, catId, resultDataBean.getMockTestId()
                    , SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO), getPackageName(), getQueData()
            ).enqueue(new Callback<TestRankBean>() {
                @Override
                public void onResponse(Call<TestRankBean> call, Response<TestRankBean> response) {
                    if (response != null && response.body() != null
                            && !SupportUtil.isEmptyOrNull(response.body().getStatus())) {
                        if ( response.body().getStatus().equalsIgnoreCase("success") ) {
                            testRankBean = response.body();
                            handleRanking();
                        }
                        new UpdateTestSync().execute();
                    }
                }

                @Override
                public void onFailure(Call<TestRankBean> call, Throwable t) {

                }
            });
*/
        }
    }
}
