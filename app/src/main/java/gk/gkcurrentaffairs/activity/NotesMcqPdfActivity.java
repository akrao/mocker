package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.adssdk.PageAdsAppCompactActivity;

import androidx.annotation.Nullable;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ServerCatBean;
import gk.gkcurrentaffairs.bean.ServerCatListBean;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/1/2018.
 */

public class NotesMcqPdfActivity extends PageAdsAppCompactActivity implements View.OnClickListener{

    private ServerCatListBean categoryProperty;
    private CategoryProperty property ;
    private String imageUrl ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_mcq_pdf);
        initDataFromIntent();
        initView();
    }

    private void initDataFromIntent(){
        categoryProperty = (ServerCatListBean) getIntent().getSerializableExtra(AppConstant.CAT_SERVER_DATA);
        property = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
        imageUrl = getIntent().getStringExtra(AppConstant.IMAGE);
        if ( categoryProperty != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(categoryProperty.getTitle());
        }else {
            SupportUtil.showToastCentre(this , "Error, please try later.");
            onBackPressed();
        }
    }

    private void initView(){
        findViewById(R.id.ll_notes).setOnClickListener(this);
        findViewById(R.id.ll_mcq).setOnClickListener(this);
        findViewById(R.id.ll_pdf).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Class aClass = null;
        ServerCatBean serverCatBean = null ;
        switch ( view.getId() ){
            case R.id.ll_notes :
                aClass = CategoryActivity.class ;
                serverCatBean = getServerCatBean(Constants.CATEGORY_TYPE_ARTICLE);
                break;
            case R.id.ll_mcq :
                aClass = MockCategoryActivity.class ;
                serverCatBean = getServerCatBean(Constants.CATEGORY_TYPE_MOCK);
                break;
            case R.id.ll_pdf :
                aClass = PDFListActivity.class ;
                serverCatBean = getServerCatBean(Constants.CATEGORY_TYPE_PDF);
                break;
        }
        if ( aClass != null && serverCatBean != null ) {
            Intent intent = new Intent( this , aClass );
            intent.putExtra( AppConstant.CAT_DATA , getCategoryProperty(serverCatBean) );
            startActivity(intent);
        }
    }

    private ServerCatBean getServerCatBean(int type){
        ServerCatBean serverCatBean = null;
        for ( ServerCatBean bean : categoryProperty.getCatBeans() ){
            if ( bean.getCategoryType() == type )
                serverCatBean = bean ;
        }
        return serverCatBean ;
    }

    private CategoryProperty getCategoryProperty(ServerCatBean serverCatBean){
        CategoryProperty property = new CategoryProperty();
        property.setId( serverCatBean.getId() );
//        property.setTitle( serverCatBean.getTitle() );
        property.setTitle( categoryProperty.getTitle() );
        property.setImageResId( R.drawable.place_holder_cat );
        property.setImageUrl( imageUrl + serverCatBean.getImage() );
        property.setSubCat( this.property.isSubCat() );
        property.setWebView( true );
        property.setHost(this.property.getHost());
        property.setSeeAnswer(this.property.isSeeAnswer());
        return property ;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            onBackPressed();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

}
