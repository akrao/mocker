package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.model.AdsPointsSlabConfig;
import gk.gkcurrentaffairs.payment.utils.CircleTransform;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 8/3/2017.
 */

public class AdFreePointsActivity extends AppCompatActivity implements View.OnClickListener {

    private String photoUrl, userName;
    private AppCompatImageView ivProfilePic;
    private TextView tvUserName, tvPoints;
    private LinearLayout linearLayout;
    private LayoutInflater inflater;
    private AdsPointsSlabConfig adFreeConfig;
    private ProgressDialog pDialog;
    private View oneDayPoints, oneDayVideo, oneDayDivider;
    private boolean isAdFreeActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_free);
        initData();
        setupToolbar();
        initView();
        loadData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchData();
    }

    private void initView() {
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvPoints = (TextView) findViewById(R.id.tvUserPoints);
        ivProfilePic = (AppCompatImageView) findViewById(R.id.ivProfilePic);
        linearLayout = (LinearLayout) findViewById(R.id.ll_body);
        inflater = LayoutInflater.from(this);
        oneDayPoints = findViewById(R.id.ll_one_day_ads_points);
        oneDayPoints.setOnClickListener(this);
        oneDayVideo = findViewById(R.id.ll_one_day_ads_video);
        oneDayVideo.setOnClickListener(this);
        oneDayDivider = findViewById(R.id.divider);
        if (!isAdFreeActivity) {
            hideOneDayPoints();
        }
    }

    private void addRows(final int position) {
        View inflatedLayout = inflater.inflate(R.layout.item_ad_free_slab, linearLayout, false);
        ImageView imageView = (ImageView) inflatedLayout.findViewById(R.id.iv_ad_free_row);
        TextView title = (TextView) inflatedLayout.findViewById(R.id.tv_ad_free_row);
        TextView amount = (TextView) inflatedLayout.findViewById(R.id.tv_ad_free_row_amount);
        imageView.setImageResource(R.drawable.point);
        if ( isAdFreeActivity ) {
            title.setText(adFreeConfig.getSlab().get(position).getBenefits() + AppConstant.BENEFITS_AD_FREE);
            amount.setText(adFreeConfig.getSlab().get(position).getCost() + AppConstant.COST_AD_FREE);
        } else {
            title.setText(adFreeConfig.getSlab().get(position).getBenefits() + AppConstant.COST_AD_FREE );
            amount.setText(adFreeConfig.getSlab().get(position).getCost() + AppConstant.CURRENCY );
        }
        inflatedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSlab(position);
            }
        });
        linearLayout.addView(inflatedLayout);
    }

    private void onClickSlab(int position) {
/*
        Intent paymentMerchentIntent = new Intent(this, ConfirmationActivity.class);
        paymentMerchentIntent.putExtra(AppConstant.DATA, adFreeConfig.getSlab().get(position));
        paymentMerchentIntent.putExtra(AppConstant.TYPE, isAdFreeActivity ? AppConstant.SERVICE_ADS : AppConstant.SERVICE_POINT );
        startActivity(paymentMerchentIntent);
*/
    }


    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle( isAdFreeActivity ? "Ad Free" : "Earn Points");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void initData() {
        photoUrl = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
        userName = SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
        isAdFreeActivity = getIntent().getBooleanExtra(AppConstant.TYPE, true);
    }

    private void loadData() {
        if (!photoUrl.equalsIgnoreCase("")) {
            AppApplication.getInstance().getPicasso()
                    .load(photoUrl)
                    .resize(125, 125)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(ivProfilePic);
        } else {
            ivProfilePic.setImageResource(R.drawable.profile_black);
        }
        tvUserName.setText(userName);
    }

    private void fetchData() {
        try {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Fetching Data....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> map = new HashMap<>(1);
        map.put("uid", AppApplication.getInstance().getLoginSdk().getUserId());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID,
                isAdFreeActivity ? ApiEndPoint.ADS_FREE_SLAB_CONFIG: ApiEndPoint.POINTS_PLAN
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        try {
                            pDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final AdsPointsSlabConfig statusBean = ConfigManager.getGson().fromJson(data, AdsPointsSlabConfig.class);
                                if (statusBean != null ) {
                                    loadSlabData();
                                } else {
                                    showNoData();
                                }
                            } catch (JsonSyntaxException e) {
                                showNoData();
                                e.printStackTrace();
                            }
                        }else {
                            showNoData();
                        }
                    }
                });

/*
        ApiPayInterface apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
        Call<AdsPointsSlabConfig> call;
        if ( isAdFreeActivity ) {
            call = apiPayInterface.getAdFreeSlab(SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID));
        } else {
            call = apiPayInterface.getPointsSlab(SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID));
        }
        call.enqueue(new Callback<AdsPointsSlabConfig>() {
            @Override
            public void onResponse(Call<AdsPointsSlabConfig> call, Response<AdsPointsSlabConfig> response) {
                if (response != null && response.body() != null) {
                    adFreeConfig = response.body();
                    loadSlabData();
                } else
                    showNoData();
                try {
                    pDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AdsPointsSlabConfig> call, Throwable t) {
                try {
                    pDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showNoData();
            }
        });
*/
    }

    private void showNoData() {
        Toast.makeText(this, "Error, Pleas try later.", Toast.LENGTH_SHORT).show();
    }

    private void loadSlabData() {
        int l = adFreeConfig.getSlab().size();
        for (int i = 0; i < l; i++) {
            addRows(i);
        }
        tvPoints.setText("Available points : " + adFreeConfig.getUser().getUserPoints());
        if (l < 1 && adFreeConfig.getSlab().get(0).getBenefits() == 1) {
            hideOneDayPoints();
        }
        SharedPrefUtil.setInt(AppConstant.SharedPref.USER_POINTS , adFreeConfig.getUser().getUserPoints());
    }

    private void hideOneDayPoints() {
        oneDayDivider.setVisibility(View.GONE);
        oneDayPoints.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_one_day_ads_points:
                onClickSlab(0);
                break;
            case R.id.ll_one_day_ads_video:
                break;
        }
    }
}
