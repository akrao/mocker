package gk.gkcurrentaffairs.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.AdsAppCompactActivity;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.MockLeaderBoardAdapter;
import gk.gkcurrentaffairs.adapter.UserLeaderBoardBean;
import gk.gkcurrentaffairs.bean.LeaderBoardDataBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.SupportUtil.loadUserImage;

/**
 * Created by Amit on 1/24/2018.
 */

public class MockLeaderBoardActivity extends AdsAppCompactActivity implements View.OnClickListener {

    private TextView tvTitle, tvThisMonth, tvLastMonth, tvUser1, tvUser2, tvUser3, tvUserRank1, tvUserRank2, tvUserRank3, tvTimeLeft, tvRankingCount;
    private ImageView ivUser1, ivUser2, ivUser3;
    private MockLeaderBoardAdapter adapter;
    private List<UserLeaderBoardBean> userLeaderBoardBeen;
    private boolean isThisMonth = false;
    private int colorBlue, catId = 0, userId;
    private LeaderBoardDataBean leaderBoardDataBean;
    static boolean active = false;
    private View vTop1, vTop2, vTop3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_leaderboard);
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
            initView();
            initObjects();
            initList();
            setOnClick();
            refreshMonthData(true);
            startCountDown();
            AppApplication.setSessionAd(true);
            SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
        }
    }

    private void initView() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvRankingCount = (TextView) findViewById(R.id.tv_rank);
        tvThisMonth = (TextView) findViewById(R.id.tv_this_month);
        tvLastMonth = (TextView) findViewById(R.id.tv_last_month);
        tvUser1 = (TextView) findViewById(R.id.tv_user_name_1);
        tvUser2 = (TextView) findViewById(R.id.tv_user_name_2);
        tvUser3 = (TextView) findViewById(R.id.tv_user_name_3);
        tvUserRank1 = (TextView) findViewById(R.id.tv_user_rank_1);
        tvUserRank2 = (TextView) findViewById(R.id.tv_user_rank_2);
        tvUserRank3 = (TextView) findViewById(R.id.tv_user_rank_3);
        tvTimeLeft = (TextView) findViewById(R.id.tv_time_left);

        ivUser1 = (ImageView) findViewById(R.id.iv_user_1);
        ivUser2 = (ImageView) findViewById(R.id.iv_user_2);
        ivUser3 = (ImageView) findViewById(R.id.iv_user_3);

        vTop1 = findViewById(R.id.ll_1);
        vTop2 = findViewById(R.id.ll_2);
        vTop3 = findViewById(R.id.ll_3);
    }

    private void initObjects() {
//        userId = Integer.parseInt(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO));
        userId = Integer.parseInt(AppApplication.getInstance().getLoginSdk().getUserId());
        colorBlue = SupportUtil.getColor(R.color.color_primary_status, this);
    }

    private void initList() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));/*{
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });*/
        userLeaderBoardBeen = new ArrayList<>();
        adapter = new MockLeaderBoardAdapter(this, userLeaderBoardBeen, userId);
        recyclerView.setAdapter(adapter);
    }

    private void setOnClick() {
        tvThisMonth.setOnClickListener(this);
        tvLastMonth.setOnClickListener(this);
        findViewById(R.id.iv_close).setOnClickListener(this);
        findViewById(R.id.iv_privacy_policy).setOnClickListener(this);

    }

    private void fetchDataFromServer() {
        if (!SupportUtil.isNotConnected(this)) {
            findViewById(R.id.ll_no_data).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_holder).setVisibility(View.GONE);
            if (AppApplication.getInstance().getLoginSdk() != null) {
                Map<String, String> map = new HashMap<>(4);
                map.put("cat_id", catId + "");
                map.put("is_last_month", (isThisMonth ? 1 : 0) + "");
                map.put("user_id", userId + "");
                map.put("application_id", getPackageName());

                AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_LEADER_BOARD
                        , ApiEndPoint.GET_USER_LEADER_BOARD_RANKING
                        , map, new ConfigManager.OnNetworkCall() {
                            @Override
                            public void onComplete(boolean status, String data) {
                                hideDialog();
                                if (status && !SupportUtil.isEmptyOrNull(data)) {
                                    try {
                                        leaderBoardDataBean = ConfigManager.getGson().fromJson(data, LeaderBoardDataBean.class);
                                        if (leaderBoardDataBean != null) {
                                            setDataInView();
                                        } else {
                                            showErrorMessage();
                                        }
                                    } catch (JsonSyntaxException e) {
                                        showErrorMessage();
                                        e.printStackTrace();
                                    }
                                }else {
                                    showErrorMessage();
                                }
                            }
                        });
            }

/*
            apiPayInterface.getLeaderBoardData(catId, isThisMonth ? 1 : 0, userId, getPackageName()).enqueue(new Callback<LeaderBoardDataBean>() {
                @Override
                public void onResponse(Call<LeaderBoardDataBean> call, Response<LeaderBoardDataBean> response) {
                    if (response != null && response.body() != null) {
                        leaderBoardDataBean = response.body();
                        hideDialog();
                        setDataInView();
                    } else {
                        showErrorMessage();
                    }
                }

                @Override
                public void onFailure(Call<LeaderBoardDataBean> call, Throwable t) {
                    showErrorMessage();
                }
            });
*/
        } else
            SupportUtil.showToastInternet(this);
    }

    private void showErrorMessage() {
        SupportUtil.showToastCentre(this, "Error in fetching data. Please try after sometime.");
    }

    private void setDataInView() {
        showTopThreeData();
        computeDataUserRanking();
        adapter.setTotalCount(leaderBoardDataBean.getTotal_count());
        adapter.notifyDataSetChanged();
        calculateTimeLeft();
        tvRankingCount.setText("RANKING - Total User : " + leaderBoardDataBean.getTotal_count());
    }

    private void calculateTimeLeft() {
        long seconds = ((getCalendar().getTimeInMillis() - System.currentTimeMillis()) / 1000);

        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
        tvTimeLeft.setText(String.format("%d Days %d hr %d min %d sec", day, (int) hours, (int) minute, (int) second));
        Log.d("Amit", " Amit " + tvTimeLeft.getText().toString());
    }

    public Calendar getCalendar() {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.MONTH, 1);
        date.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), 1, 0, 0, 0);
        return date;
    }

    private void computeDataUserRanking() {
        userLeaderBoardBeen.clear();
        int userPlace = 0;
        List<UserLeaderBoardBean> userLeaderBoardBeenList = leaderBoardDataBean.getTopper();
        if (userLeaderBoardBeenList != null && userLeaderBoardBeenList.size() > 0) {
            userLeaderBoardBeen.addAll(userLeaderBoardBeenList);
        }

        if (userLeaderBoardBeen != null && userLeaderBoardBeen.size() > 0) {
            int l = userLeaderBoardBeen.size();
/*
            for (int i = 0; i < l; i++)
                userLeaderBoardBeen.get(i).setPointTime();

            sortUserRank();
*/
            userPlace = getUserPosition();
            int firstRank = leaderBoardDataBean.getRank() - userPlace;
            for (int i = 0; i < l; i++) {
                userLeaderBoardBeen.get(i).setRank(firstRank + i);
                Log.d("Amit", userLeaderBoardBeen.get(i).getUser_id() + " -- " + userLeaderBoardBeen.get(i).getRank() + " -- " + userLeaderBoardBeen.get(i).getPointTime());
            }

        }
    }

    private int getUserPosition() {
        int position = 0;
        int l = userLeaderBoardBeen.size();
        for (int i = 0; i < l; i++) {
            if (userLeaderBoardBeen.get(i).getUser_id() == userId)
                position = i;
        }

        return position;
    }

    private void sortUserRank() {
        Collections.sort(userLeaderBoardBeen, new Comparator<UserLeaderBoardBean>() {
            @Override
            public int compare(UserLeaderBoardBean t2, UserLeaderBoardBean t1) {
                return Double.compare(t1.getPointTime(), t2.getPointTime());
            }
        });
    }

    private void showTopThreeData() {
        List<UserLeaderBoardBean> userLeaderBoardBeen = leaderBoardDataBean.getTop_three_users();
        if (userLeaderBoardBeen != null && userLeaderBoardBeen.size() > 0) {
            vTop1.setVisibility(View.VISIBLE);
            tvUser1.setText(getUserName(userLeaderBoardBeen.get(0).getName()));
            tvUserRank1.setText(userLeaderBoardBeen.get(0).getPoints() + "");
            loadUserImage(userLeaderBoardBeen.get(0).getPhoto_url(), ivUser1);
            if (userLeaderBoardBeen.size() > 1) {
                vTop2.setVisibility(View.VISIBLE);
                tvUser2.setText(getUserName(userLeaderBoardBeen.get(1).getName()));
                tvUserRank2.setText(userLeaderBoardBeen.get(1).getPoints() + "");
                loadUserImage(userLeaderBoardBeen.get(1).getPhoto_url(), ivUser2);
                if (userLeaderBoardBeen.size() > 2) {
                    vTop3.setVisibility(View.VISIBLE);
                    tvUser3.setText(getUserName(userLeaderBoardBeen.get(2).getName()));
                    tvUserRank3.setText(userLeaderBoardBeen.get(2).getPoints() + "");
                    loadUserImage(userLeaderBoardBeen.get(2).getPhoto_url(), ivUser3);
                } else {
                    vTop3.setVisibility(View.GONE);
                }
            } else {
                vTop2.setVisibility(View.GONE);
                vTop3.setVisibility(View.GONE);
            }
        } else {
            vTop1.setVisibility(View.GONE);
            vTop2.setVisibility(View.GONE);
            vTop3.setVisibility(View.GONE);
            SupportUtil.showToastCentre(this, "No Ranking this month yet.");
        }
    }

    private String getUserName(String s) {
        if (SupportUtil.isEmptyOrNull(s))
            return "User";
        return s;

    }

    private void hideDialog() {
        findViewById(R.id.ll_no_data).setVisibility(View.GONE);
        findViewById(R.id.ll_holder).setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_this_month:
                refreshMonthData(true);
                break;
            case R.id.tv_last_month:
                refreshMonthData(false);
                break;
            case R.id.iv_close:
                onBackPressed();
                break;
            case R.id.iv_privacy_policy:
                SupportUtil.openPolicy(this);
                break;
        }
    }

    private void refreshMonthData(boolean thisMonth) {
        if (thisMonth != isThisMonth) {
            if (thisMonth) {
                tvLastMonth.setBackgroundResource(R.drawable.rounded_corner_white_only);
                tvLastMonth.setTextColor(Color.WHITE);
                tvThisMonth.setBackgroundResource(R.drawable.rounded_side_white_padding);
                tvThisMonth.setTextColor(colorBlue);
            } else {
                tvLastMonth.setBackgroundResource(R.drawable.rounded_side_white_padding);
                tvLastMonth.setTextColor(colorBlue);
                tvThisMonth.setBackgroundResource(R.drawable.rounded_corner_white_only);
                tvThisMonth.setTextColor(Color.WHITE);
            }
            fetchDataFromServer();
            isThisMonth = thisMonth;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    private void startCountDown() {
        new CountDownTimer(24 * 60 * 60, 1000) {

            public void onTick(long millisUntilFinished) {
                if (active) {
                    calculateTimeLeft();
                } else
                    this.cancel();
            }

            public void onFinish() {
                if (active) {
                    calculateTimeLeft();
                } else
                    this.cancel();
            }
        }.start();
    }

}
