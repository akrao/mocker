package gk.gkcurrentaffairs.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.fragment.MockCategoryFragment;
import gk.gkcurrentaffairs.fragment.MockCategoryListFragment;
import gk.gkcurrentaffairs.fragment.ResultListFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class MockCategoryActivity extends PageAdsAppCompactActivity {

    private int catID, position, image;
    private String title, imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initDataFromArg();
        setupToolbar();
        setupViewPager();
        setupCollapsingToolbar();

        RelativeLayout rlAds = (RelativeLayout) findViewById(R.id.ll_ad);
        SupportUtil.initAds(rlAds, this, AppConstant.ADS_BANNER);
    }

    private CategoryProperty categoryProperty;

    private void initDataFromArg() {
        if (getIntent().getSerializableExtra(AppConstant.CAT_DATA) != null) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
            initObjects();
        } else {
            initDataForUnStructure();
        }
    }

    private void initObjects() {
        if (categoryProperty != null) {
            catID = categoryProperty.getId();
            title = categoryProperty.getTitle();
            image = categoryProperty.getImageResId();
            isCategory = categoryProperty.isSubCat();
            imageUrl = categoryProperty.getImageUrl();
        }
    }

    private boolean isCategory;

    private void initDataForUnStructure() {
        position = getIntent().getIntExtra(AppConstant.POSITION, AppConstant.ENGLISH_MOCK_TEST_ID);
        catID = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        isCategory = getIntent().getBooleanExtra(AppConstant.DATA, true);
        if (!SupportUtil.isEmptyOrNull(getIntent().getStringExtra(AppConstant.TITLE))) {
            title = getIntent().getStringExtra(AppConstant.TITLE);
        } else {
            title = getResources().getStringArray(R.array.main_cat_name)[position];
        }
        image = AppConstant.IMAGE_RES[position];
    }


    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(false);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        MockCategoryListFragment categoryListFragment = new MockCategoryListFragment();
        ResultListFragment resultListFragment = new ResultListFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.CAT_DATA, categoryProperty);
        bundle.putString(AppConstant.QUERY, DbHelper.COLUMN_CAT_ID + "=" + catID);
        bundle.putInt(AppConstant.CAT_ID, catID);
        bundle.putInt(AppConstant.IMAGE, image);
        bundle.putString(AppConstant.QUERY, DbHelper.COLUMN_CAT_ID + "=" + catID);
        bundle.putString(AppConstant.IMAGE_URL, imageUrl);


        categoryListFragment.setArguments(bundle);
        resultListFragment.setArguments(bundle);

        boolean isLatest = isLatest();
        if (isLatest) {
            MockCategoryFragment latest = new MockCategoryFragment();
            latest.setArguments(bundle);
            adapter.addFrag(latest, "Latest");
        }

        bundle.putBoolean(AppConstant.TYPE, !isLatest || catID == AppValues.MOCK_ID);
        if (isCategory) {
            adapter.addFrag(categoryListFragment, "Category");
        }
        adapter.addFrag(resultListFragment, "Result");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    //    private boolean isLatest(){
//        return !( position == 16 || position == 17 || position == 18 );
//    }
    private boolean isLatest() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>(3);
        private final List<String> mFragmentTitleList = new ArrayList<>(3);

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
