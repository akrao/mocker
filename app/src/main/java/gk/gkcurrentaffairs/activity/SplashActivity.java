package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;

/**
 * Created by Amit on 2/15/2018.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Log.d( "Amit" , "PLAYER_ID : "+SharedPrefUtil.getString(AppConstant.SharedPref.PLAYER_ID) + " #" );
        initDataFromArgs();
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initDataFromArgs();
    }

    private void initDataFromArgs(){

        Intent intent = new Intent( this , PaidHomeActivity.class);
//        Intent intent = new Intent( this , MainActivity.class);
        intent.putExtras( getIntent() );
        startActivity(intent);
        finish();
    }
}
