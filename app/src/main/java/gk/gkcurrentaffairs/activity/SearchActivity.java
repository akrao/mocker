package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.HomeAdapter;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ListBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/18/2017.
 */

public class SearchActivity extends PageAdsAppCompactActivity implements HomeAdapter.OnClick , SearchView.OnQueryTextListener{

    private DbHelper dbHelper ;
    private HomeAdapter mAdapter;
    private ArrayList<ListBean> homeBeen = new ArrayList<>();
    private String catQuery = "" ;
    private String query ;
    private int image ;
    private boolean isWebView ;
    private CategoryProperty categoryProperty = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search );
        initData();
        setupToolbar();
        setupList();
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    int catID = 0 ;
    private void initData(){
        dbHelper = AppApplication.getInstance().getDBObject();
        catID = getIntent().getIntExtra(AppConstant.DATA , 0 );
        image = getIntent().getIntExtra(AppConstant.IMAGE , 0 );
        isWebView = getIntent().getBooleanExtra(AppConstant.WEB_VIEW , false );
        if (getIntent().getSerializableExtra(AppConstant.PROPERTY) != null && getIntent().getSerializableExtra(AppConstant.PROPERTY).getClass() == CategoryProperty.class) {
            categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.PROPERTY);
        }
        catQuery = DbHelper.COLUMN_CAT_ID + "=" + catID ;
    }
    private void setupList(){
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager( new LinearLayoutManager( this ) );
        mAdapter = new HomeAdapter(homeBeen, this , null , null , this , null );
        mAdapter.setImageRes( image );
        mAdapter.setCategoryProperty(categoryProperty);
        mRecyclerView.setAdapter( mAdapter );
    }

    @Override
    public void onCustomItemClick(int position, int type) {
        if ( type == HomeAdapter.OnClick.TYPE_ARTICLE ) {
        Intent intent = new Intent( this , DescActivity.class );
        intent.putExtra( AppConstant.DATA , homeBeen.get( position ).getId() );
        intent.putExtra( AppConstant.QUERY , DbHelper.COLUMN_ID + "=" + homeBeen.get( position ).getId() );


        intent.putExtra( AppConstant.CAT_ID, catID );
        intent.putExtra( AppConstant.WEB_VIEW , isWebView);
        startActivity( intent );
        }else {
            updateFavStatus(position);
        }
    }

    private void updateFavStatus(final int position) {
        final boolean status = homeBeen.get(position).isFav();
        homeBeen.get(position).setFav(!status);
        mAdapter.notifyDataSetChanged();
        SupportUtil.showToastCentre( this , status ? " Removed " : " Saved " );
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.updateArticle(homeBeen.get(position).getId(), dbHelper.COLUMN_FAV,
                        status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catID);
                return null;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_act , menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle( "Search" );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if ( !SupportUtil.isEmptyOrNull( newText )){
            query = catQuery ;
            if ( !SupportUtil.isEmptyOrNull( catQuery ) )
                query = catQuery + " AND ";
             query = query + DbHelper.COLUMN_TITLE + " LIKE '%" + newText +"%'" ;
            new DataFromDB().execute();
        }
        return false;
    }

    class DataFromDB extends AsyncTask<Void , Void , Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            homeBeen.clear();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.fetchHomeData( homeBeen , query , catID , false );
                    return null;
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mAdapter.notifyDataSetChanged();
            if ( homeBeen.size() == 0 )
                Toast.makeText( SearchActivity.this , "No Data Found" , Toast.LENGTH_SHORT ).show();
        }
    }

}
