/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gk.gkcurrentaffairs.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.adssdk.PageAdsAppCompactActivity;

import java.io.File;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.HomeBean;
import gk.gkcurrentaffairs.fragment.PdfRendererBasicFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

public class PDFViewerActivity extends PageAdsAppCompactActivity {

    public static final String FRAGMENT_PDF_RENDERER_BASIC = "pdf_renderer_basic";
    private String title , fileName , fileUrl ;
    public int id , catId ;
    private DbHelper dbHelper ;
    public HomeBean homeBean ;
    private MenuItem menuFav ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        initArgs();
        fetchDataFromDB();
    }

    private void initFrag(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PdfRendererBasicFragment fragment = new PdfRendererBasicFragment() ;
            Bundle bundle = new Bundle();
            bundle.putString( AppConstant.DATA , fileName );
            bundle.putString( AppConstant.CLICK_ITEM_ARTICLE , fileUrl );
            bundle.putBoolean( AppConstant.POSITION , homeBean != null ? homeBean.isRead() : false );
            bundle.putBoolean( AppConstant.TYPE , homeBean != null ? true : false );
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment ,
                            FRAGMENT_PDF_RENDERER_BASIC)
                    .commit();
        }


    }

    private void initArgs(){
        Intent intent = getIntent() ;
        if ( intent != null ){
            catId = intent.getIntExtra(AppConstant.CAT_ID , 0);
            id = intent.getIntExtra(AppConstant.CATEGORY , 0);
            title = intent.getStringExtra(AppConstant.TITLE);
            fileName = intent.getStringExtra(AppConstant.DATA);
            fileUrl = intent.getStringExtra(AppConstant.CLICK_ITEM_ARTICLE);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    private void fetchDataFromDB(){
        dbHelper = AppApplication.getInstance().getDBObject();
        new DataFromDB().execute();
    }

    class DataFromDB extends AsyncTask<Void , Void , Void>{
        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    homeBean = dbHelper.fetchArticle( id );
                    if ( homeBean != null && !homeBean.isRead() ) {
//                        dbHelper.updateArticle( id , dbHelper.COLUMN_READ , dbHelper.INT_TRUE , catId);
                    }
                    return null;
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateFavButton();
            initFrag();
        }
    }

    private void updateFavButton(){
        if ( homeBean != null && menuFav != null ){
            menuFav.setIcon( homeBean.isFav() ? R.drawable.like_fill : R.drawable.like_non_fill );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pdf_viewer, menu);
        menuFav = menu.findItem( R.id.action_fav );
        updateFavButton();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch ( id ){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_fav:
                updateFavStatus();
                break;
            case R.id.action_share:
                SupportUtil.share( this , getUri() );
                break;
            case R.id.action_pdf_open:
                openPDF();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openPDF(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(getUri(), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private Uri getUri(){
        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + fileName;
        File file = new File(filePath);
        return Uri.fromFile(file);
    }

    private void updateFavStatus(){
        if ( homeBean != null ) {
            menuFav.setIcon( homeBean.isFav() ? R.drawable.like_non_fill : R.drawable.like_fill );
            homeBean.setFav( !homeBean.isFav() );
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle( id , dbHelper.COLUMN_FAV ,
                            homeBean.isFav() ? dbHelper.INT_TRUE : dbHelper.INT_FALSE , catId);
                    return null;
                }
            });
        }else {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
            startActivity(rateIntent);
        }
    }

}
