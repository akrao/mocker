package gk.gkcurrentaffairs.activity;


import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amit on 21/12/16.
 */
public class BaseActivity extends RewardAdActivity {

    @Override
    protected void onStart() {
        super.onStart();
        try {
            FlurryAgent.onStartSession(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        try {
            FlurryAgent.onEndSession(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void generateGAMain( String mainCategory ){
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("Main category", mainCategory );
        FlurryAgent.logEvent("HomeClick", articleParams);
    }
}
