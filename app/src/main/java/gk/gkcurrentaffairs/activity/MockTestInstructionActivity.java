package gk.gkcurrentaffairs.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/1/2018.
 */

public class MockTestInstructionActivity extends AppCompatActivity {

    private CategoryProperty categoryProperty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_test_instruction);
        activity = this;
        setupToolBar();
        initDataFromIntent();
        initView();
    }

    private MockHomeBean mockHomeBean;
    private boolean isMockWithDirection;

    private void initDataFromIntent() {
        mockHomeBean = (MockHomeBean) getIntent().getSerializableExtra(AppConstant.DATA);
        catId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        categoryProperty = (CategoryProperty) getIntent().getSerializableExtra(AppConstant.CAT_DATA);
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    private void initView() {
        WebView webView = (WebView) findViewById(R.id.web_view);
        setDataWebView(webView, SupportUtil.isEmptyOrNull(mockHomeBean.getInstruction()) ? AppConstant.DEFAULT_INSTRUCTION : mockHomeBean.getInstruction());
        findViewById(R.id.tv_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mockHomeBean.getDownload() == DbHelper.INT_TRUE) {
                    openMcq();
                } else {
                    SupportUtil.downloadNormalMockTest(mockHomeBean.getId(), categoryProperty.getId(), 0, MockTestInstructionActivity.this
                            , categoryProperty.getHost(), new SupportUtil.DownloadNormalMCQ() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result) {
                                        openMcq();
                                    }
                                }
                            });
                }
            }
        });
    }

    public void setDataWebView(WebView webView, String data) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:large; font-family:roboto_regular;"
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    private DbHelper dbHelper;

    private Activity activity;
    private int catId;

    private void openMcq() {
        Intent intent = new Intent(activity, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, mockHomeBean);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.TITLE, mockHomeBean.getTitle());
        intent.putExtra(AppConstant.DATA, mockHomeBean.getId());
        intent.putExtra(AppConstant.QUERY, getQuery());
        activity.startActivity(intent);
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.upDateMockDb(dbHelper.COLUMN_ATTEMPTED, mockHomeBean.getId(), catId);
                dbHelper.upDateMockDb(dbHelper.COLUMN_DOWNLOADED, mockHomeBean.getId(), catId);
                return null;
            }
        });
        finish();
    }

    private ProgressDialog progressDialog;
/*
    private void downloadMocTest() {
        Call<List<MockTestBean>> call;
        if ( isMockWithDirection ) {
            call = AppApplication.getInstance().getHomeApiEnd().downloadMockTestWithDirection(mockHomeBean.getId());
        } else {
            call = AppApplication.getInstance().getHomeApiEnd().downloadMockTestById(mockHomeBean.getId(), catId);
        }
        showProgressDialog();
        call.enqueue(new Callback<List<MockTestBean>>() {
            @Override
            public void onResponse(Call<List<MockTestBean>> call, final Response<List<MockTestBean>> response) {
                if (response != null && response.body() != null && response.body().size() > 0) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            if ( isMockWithDirection ) {
                                dbHelper.insertMockTest(response.body(), catId);
                            }else
                                dbHelper.insertMockTest(response.body(), mockHomeBean.getId(), catId);

                            return null;
                        }
                    });
                    openMcq();
                } else {
                    SupportUtil.showToastCentre(activity, "Error in downloading");
                }
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<List<MockTestBean>> call, Throwable t) {
                hideProgressDialog();
                SupportUtil.showToastCentre(activity, "Error in downloading");
            }
        });
    }
*/

    private void showProgressDialog() {
        try {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Downloading...");
            progressDialog.show();
        } catch (Exception e) {
        }
    }

    private void hideProgressDialog() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
        }
    }


    private String getQuery() {
        return DbHelper.COLUMN_CAT_ID + "=" + catId + " AND " + DbHelper.COLUMN_MOCK_TEST_ID + "=" + mockHomeBean.getId();
    }

    private void setupToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Instruction");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
