package gk.gkcurrentaffairs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/10/2017.
 */

public class MCQIntermediateActivity extends PageAdsAppCompactActivity implements View.OnClickListener{

    private ImageView[] imageViews;
    private TextView[] textViews ;
    private boolean isTrainingMode , isServerOne;
    private DbHelper dbHelper ;
    private String HOST;
    private int catId , idFirst , idSec , time = 20 , position ;
    private ProgressDialog progressDialog ;
    private String query = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq_inter );
        getDataFromArg();
        initView();
        setOnClickOnView();
        setDataInView();
        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    private void initView(){
        imageViews = new ImageView[ 3 ];
        textViews = new TextView[ 3 ];
        textViews[ 0 ] = ( TextView ) findViewById( R.id.tv_mcq_one );
        textViews[ 1 ] = ( TextView ) findViewById( R.id.tv_mcq_two );
        textViews[ 2 ] = ( TextView ) findViewById( R.id.tv_mcq_three );
        imageViews[ 0 ] = ( ImageView ) findViewById( R.id.iv_mcq_one );
        imageViews[ 1 ] = ( ImageView ) findViewById( R.id.iv_mcq_two );
        imageViews[ 2 ] = ( ImageView ) findViewById( R.id.iv_mcq_three );
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Downloading..." );
    }

    private void setOnClickOnView(){
        findViewById( R.id.ll_one ).setOnClickListener( this );
        findViewById( R.id.ll_two ).setOnClickListener( this );
        findViewById( R.id.ll_three ).setOnClickListener( this );
    }

    private void setDataInView(){
        String[] data = isTrainingMode ? trainingMode : challengeMode ;
        int[] img = isTrainingMode ? trainingImg : challengeImg ;
        for ( int i = 0 ; i < data.length ; i++ ){
            textViews[ i ].setText( data[ i ] );
            imageViews[ i ].setImageResource( img[ i ] );
        }
    }

    private String title ;
    private void getDataFromArg(){
        categoryProperty = (CategoryProperty) getIntent().getSerializableExtra( AppConstant.CAT_DATA );
        isTrainingMode = getIntent().getBooleanExtra( AppConstant.DATA , true );
        catId = getIntent().getIntExtra( AppConstant.CAT_ID , 0 );
        position = getIntent().getIntExtra( AppConstant.POSITION , 0 );
        title = getIntent().getStringExtra(AppConstant.TITLE) ;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dbHelper = AppApplication.getInstance().getDBObject();
        isServerOne = catId != AppConstant.ENGLISH_MOCK_TEST_ID ;
        HOST = isServerOne ? ConfigConstant.HOST_MAIN : ConfigConstant.HOST_TRANSLATOR ;
//                AppApplication.getInstance().getNetworkObject_1();
        query = DbHelper.COLUMN_CAT_ID + "=" + catId ;
    }

    @Override
    public void onClick(View v) {
        switch ( v.getId() ){
            case R.id.ll_one:
                if ( isTrainingMode )
                    openCategory( false , trainingMode[ 0 ] );
                else {
                    time = 20 ;
                    handleMCQ();
                }
                break;
            case R.id.ll_two:
                if ( isTrainingMode )
                    openCategory( true , trainingMode[ 1 ] );
                else {
                    time = 15 ;
                    handleMCQ();
                }
                break;
            case R.id.ll_three:
                if ( isTrainingMode )
                    handleMCQ();
                else {
                    time = 10 ;
                    handleMCQ();
                }
                break;
        }
    }

    private void openCategory( boolean isMultiple , String title ){
        Intent intent = new Intent( this , SelectCategoryActivity.class);
        intent.putExtra( AppConstant.CAT_ID , catId );
        intent.putExtra( AppConstant.DATA , isMultiple );
        intent.putExtra( AppConstant.POSITION , position );
        intent.putExtra( AppConstant.CAT_DATA , categoryProperty );
        intent.putExtra( AppConstant.TITLE , title );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , time );
        startActivity( intent );
    }

    private CategoryProperty categoryProperty ;
    private void handleMCQ(){
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (dbHelper.getMockTestCatId(query)) {
                    openMCQ(query);
                } else {
                    SupportUtil.downloadNormalMcqQue20(categoryProperty.getHost(), catId, MCQIntermediateActivity.this, new SupportUtil.DownloadNormalMcqQue20() {
                        @Override
                        public void onResult(boolean result, String query) {
                            if ( result ){
                                openMCQ(query);
                            }
                        }
                    });
//                    downloadMCQOpen();
                }

/*
                if ( dbHelper.getMockTestCatId( query ) ){
                    openMCQ();
                }else {
                    downloadMCQOpen();
                }
*/
                return null;
            }
        });
    }

    private void openMCQ(String s) {
        Intent intent = new Intent(this, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, getMockHomeBean());
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.CATEGORY, false);
        intent.putExtra(AppConstant.QUERY, s);
        startActivity(intent);
        finish();
    }

    private MockHomeBean getMockHomeBean(){
        MockHomeBean mockHomeBean = new MockHomeBean();
        mockHomeBean.setNegativeMarking(0);
        mockHomeBean.setTestMarkes(0);
        mockHomeBean.setSeeAnswer(true);
        mockHomeBean.setTestTime(time);
        mockHomeBean.setQuestMarks(1);
        mockHomeBean.setTitle(title);
        mockHomeBean.setId(catId);
        return mockHomeBean ;
    }


    private void downloadMCQOpen(){
        progressDialog.show();
        Map<String, String> map = new HashMap<>(1);
        map.put("id", catId + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, HOST
                , ApiEndPoint.GET_LATEST_MOCK_TEST
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {}
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            List<MockTestBean> list = null;
                            try {
                                list = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockTestBean>>() {
                                }.getType());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }

                            if (list != null && list.size() > 0 && list.size() >= dbHelper.QUE_NUM ) {
                                dbHelper.callDBFunction(new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        ArrayList<MockTestBean> list = new ArrayList<>(10);
                                        setDataList( list , list , 0 );
                                        idFirst =  Integer.parseInt(list.get( 9 ).getId()) ;
                                        dbHelper.insertMockTest( list , idFirst , catId);

                                        setDataList( list , list , 10 );
                                        idSec = Integer.parseInt(list.get( 9 ).getId()) ;
                                        dbHelper.insertMockTest( list , idSec , catId);

                                        query = DbHelper.COLUMN_MOCK_TEST_ID + " IN (" + idFirst + "," + idSec + ")" ;
                                        openMCQ();
                                        return null;
                                    }
                                });
                            }else
                                Toast.makeText( MCQIntermediateActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                        }else
                            Toast.makeText( MCQIntermediateActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();

                    }
                });

/*
        Call<List<MockTestBean>> call;
        if (isServerOne) {
            call = apiEndpointInterface.downloadLatestMockTestBycatId( catId );
        } else {
            call = apiEndpointInterface.downloadLatestMockTestBycatIdServer( catId );
        }
        call.enqueue(new Callback<List<MockTestBean>>() {
            @Override
            public void onResponse(Call<List<MockTestBean>> call, final Response<List<MockTestBean>> response) {
                if ( response.body() != null && response.body().size() >= dbHelper.QUE_NUM ){
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            ArrayList<MockTestBean> list = new ArrayList<>(10);
                            setDataList( response.body() , list , 0 );
                            idFirst =  list.get( 9 ).getId() ;
                            dbHelper.insertMockTest( list , idFirst , catId);

                            setDataList( response.body() , list , 0 );
                            idSec = list.get( 9 ).getId() ;
                            dbHelper.insertMockTest( list , idSec , catId);

                            query = DbHelper.COLUMN_MOCK_TEST_ID + " IN (" + idFirst + "," + idSec + ")" ;
                            openMCQ();
                            return null;
                        }
                    });
                }else
                    Toast.makeText( MCQIntermediateActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<List<MockTestBean>> call, Throwable t) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                }
                Toast.makeText( MCQIntermediateActivity.this , "Error, please try later." , Toast.LENGTH_SHORT ).show();
            }
        });
*/
    }

    private void setDataList(List<MockTestBean> dataList , ArrayList<MockTestBean> list , int index){
        list.clear();
        for ( int i = index ; i < index + 10 ; i++ )
            list.add( dataList.get( i ) );
    }

    private void openMCQ(){
        Intent intent = new Intent( this , MCQActivity.class);
        intent.putExtra( AppConstant.CAT_ID , catId );
        intent.putExtra( AppConstant.CATEGORY , false );
        intent.putExtra( AppConstant.QUERY , query );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , time );
        startActivity( intent );
    }

    private String[] trainingMode = { "Individual" , "Group" , "All" };
    private String[] challengeMode = { "Beginner" , "Intermediate" , "Advanced" };

    private int[] trainingImg = {
            R.drawable.ic_individual ,
            R.drawable.ic_group ,
            R.drawable.ic_all
    };
    private int[] challengeImg = {
            R.drawable.ic_beginner ,
            R.drawable.ic_intermediate ,
            R.drawable.ic_advance
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }
}
