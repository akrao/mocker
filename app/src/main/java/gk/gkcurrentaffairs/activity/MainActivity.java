package gk.gkcurrentaffairs.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.login.Util;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.ncert.activity.ClassesActivity;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

public class MainActivity extends BaseActivity implements View.OnClickListener , NavigationView.OnNavigationItemSelectedListener
 , SupportUtil.OnImpCount{

    private TextView[] textViews ;
    private String[] titles ;
    private int[] ids ;
    private DrawerLayout drawerLayout ;
    private NavigationView navigationView ;
    int INTENT_IMP_UPDATE = 2244 ;
    public static boolean active = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        active = true ;
        setupToolbar();
        initViews();
        initDataFromArgs();

        initConfig();
//        SharedPrefUtil.setString( AppConstant.SharedPref.USER_UID , "G8LlsHn92QVkPYatYtbruxWY9Ns2" );
//        SharedPrefUtil.setString( AppConstant.SharedPref.USER_ID_AUTO , "13" );
//        SharedPrefUtil.setString( AppConstant.SharedPref.USER_EMAIL , "amitgroch@gmail.com" );
    }


    @Override
    public void finish() {
        super.finish();
        active = false ;
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initConfig(){
        registerReceiver(receiver,new IntentFilter(getPackageName() + AppConstant.APP_UPDATE));
        registerReceiver( broadcastReceiver, new IntentFilter(getPackageName()+ConfigConstant.CONFIG_LOADED));
        AppApplication.getInstance().initOperations();
    }
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ( AppApplication.getInstance() != null ) {
                AppApplication.getInstance().syncData();
                initSync();
            }
            try {
                unregisterReceiver(broadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void initSync(){
        SupportUtil.getImpCount(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initDataFromArgs();
    }

    private boolean isUserLogin ;
    private MenuItem miProfile ;
    private TextView tvProfile ;
    private ImageView ivProfile ;

    private void handleLoginData(){
        if ( AppApplication.getInstance().getLoginSdk() != null ) {
            isUserLogin = AppApplication.getInstance().getLoginSdk().isRegComplete();
            if ( isUserLogin && miProfile != null ){
                miProfile.setTitle( "Profile" );
                miProfile.setIcon( R.drawable.profile );
                String name = AppApplication.getInstance().getLoginSdk().getUserName();
                if ( !SupportUtil.isEmptyOrNull(name) )
                    tvProfile.setText( name );
                Util.loadUserImage( AppApplication.getInstance().getLoginSdk().getUserImage() , ivProfile );

            }
        }

    }
    @Override
    protected void onStart() {
        super.onStart();
        handleLoginData();

/*
        isUserLogin = !SupportUtil.isEmptyOrNull(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO));
        if ( isUserLogin && miProfile != null ){
            miProfile.setTitle( "Profile" );
            miProfile.setIcon( R.drawable.profile );
            String name = SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
            if ( !SupportUtil.isEmptyOrNull(name) )
                tvProfile.setText( name );
            SupportUtil.loadUserImage( SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL) , ivProfile );

        }
*/
    }

    private void initDataFromArgs(){
        Bundle bundle = getIntent().getExtras();
        if ( bundle != null ){
            String s = bundle.getString( AppConstant.TYPE );
            int type = SupportUtil.isEmptyOrNull( s ) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt( s ) ;
            if ( type == AppConstant.NOTIFICATION_OPEN_ARTICLE ){
                s = bundle.getString( AppConstant.CATEGORY );
                int id  = SupportUtil.isEmptyOrNull( s ) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt( s ) ;
                s = bundle.getString( AppConstant.DATA );
                int articleId = SupportUtil.isEmptyOrNull( s ) ? AppConstant.NOTIFICATION_DEFAULT : Integer.parseInt( s ) ;

                if ( id != 0 && articleId != 0 ){
                    int position = getCatPosition( id );
                    if ( id == AppValues.ID_IMP_UPDATES ){
                        openImpCatActivity( ((TextView)findViewById( R.id.tv_important_updates )).getText().toString() ,
                                false, articleId );
                    }else if ( position == 19 || position == 20 ){
                        Intent categoryIntent= new Intent(MainActivity.this, ClassesActivity.class);
                        categoryIntent.putExtra(AppConstant.BOOKTYPE, position == 19 ?
                                AppConstant.NCERTBOOKS : AppConstant.NCERTSOLUTIONENGLISH );
                        startActivity(categoryIntent);
                    }else
                        openCategory( getCatClass( position ) , position , true , articleId , id );
                }
            }else if ( type == AppConstant.NOTIFICATION_API_URL ){
                AppApplication.getInstance().invalidateApiUrl( null , true );
            }else if ( type == AppConstant.NOTIFICATION_OPEN_WEB_URL ){
                String webUrl = getIntent().getStringExtra(AppConstant.WEB_URL);
                SupportUtil.openLinkInWebView(this, webUrl , getIntent().getStringExtra(AppConstant.DATA));
            }else if ( type == AppConstant.NOTIFICATION_OPEN_PLAY_STORE ){
                String appLink = getIntent().getStringExtra(AppConstant.APP_ID);
                SupportUtil.openAppInPlayStore(this, appLink);
            }
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            openUpdateDialog( intent.getStringExtra( AppConstant.TITLE ) , intent.getBooleanExtra( AppConstant.TYPE , false ) );
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void initViews(){
        textViews = new TextView[ textViewIds.length ];
        titles = getResources().getStringArray( R.array.main_cat_name ) ;
        ids = getResources().getIntArray( R.array.main_cat_id );
        for (int i = 0; i < textViewIds.length ; i ++ ){
            textViews[ i ] = ( TextView ) findViewById( textViewIds[ i ] );
            textViews[ i ].setText( titles [ i ] );
            findViewById( linearLayoutIds[ i ] ).setOnClickListener( this );
        }
        navigationView = ( NavigationView ) findViewById( R.id.nav_view );
        drawerLayout = ( DrawerLayout ) findViewById( R.id.drawer_layout );

        navigationView.setNavigationItemSelectedListener( this );
        miProfile = navigationView.getMenu().findItem( R.id.nav_login );
        View header = navigationView.getHeaderView(0).findViewById( R.id.ll_header );
        header.setOnClickListener( this );
        tvProfile = (TextView) header.findViewById( R.id.tv_profile );
        ivProfile = (ImageView) header.findViewById( R.id.iv_profile );
        findViewById( R.id.tv_all_india_test ).setOnClickListener(this);
        findViewById( R.id.tv_important_updates ).setOnClickListener(this);
        findViewById( R.id.quizzo ).setOnClickListener(this);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon( R.drawable.drawer_q );
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle( "Home" );
    }

    private TextView tvMenuImpUpdateCount ;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        View view = menu.findItem(R.id.action_imp_update).getActionView() ;
        view.setOnClickListener(this);
        tvMenuImpUpdateCount = (TextView) view.findViewById(R.id.tv_menu_imp_update);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ){
            case R.id.action_share:
                SupportUtil.share( "" , this );
                return true ;
            case R.id.action_imp_update:
                openImpCatActivity( ((TextView)findViewById( R.id.tv_important_updates )).getText().toString() , false );
                return true ;
            case R.id.action_rate:
                rateUs();
                return true ;
            case android.R.id.home:
                drawerLayout.openDrawer( GravityCompat.START );
                return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    private void rateUs(){
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
        startActivity(rateIntent);
    }
    private void facebook(){
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse( "https://www.facebook.com/GK-Current-Affairs-1215139171889124/?skip_nax_wizard=true" ));
        startActivity(rateIntent);
    }
    private void moreApps(){
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse( "https://play.google.com/store/apps/developer?id=Mukesh+Kaushik" ));
        startActivity(rateIntent);
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
            } else {
            super.onBackPressed();
            }
        }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
    private int[] textViewIds = {
            R.id.tv_home_one ,
            R.id.tv_home_two ,
            R.id.tv_home_three ,
            R.id.tv_home_four ,
            R.id.tv_home_five ,
            R.id.tv_home_six ,
            R.id.tv_home_seven ,
            R.id.tv_home_eight ,
            R.id.tv_home_nine ,
            R.id.tv_home_ten ,
            R.id.tv_home_eleven ,
            R.id.tv_home_twelve ,
            R.id.tv_home_thirteen ,
            R.id.tv_home_forteen ,
            R.id.tv_home_fifteen ,
            R.id.tv_home_sixteen ,
            R.id.tv_home_17 ,
            R.id.tv_home_18 ,
            R.id.tv_home_19 ,
            R.id.tv_home_20 ,
            R.id.tv_home_21 ,
            R.id.tv_home_22 ,
            R.id.tv_home_23 ,
            R.id.tv_home_24
    };

    private static final int[] linearLayoutIds = {
            R.id.ll_home_one ,
            R.id.ll_home_two ,
            R.id.ll_home_three ,
            R.id.ll_home_four ,
            R.id.ll_home_five ,
            R.id.ll_home_six ,
            R.id.ll_home_seven ,
            R.id.ll_home_eight ,
            R.id.ll_home_nine ,
            R.id.ll_home_ten ,
            R.id.ll_home_eleven ,
            R.id.ll_home_twelve ,
            R.id.ll_home_thirteen ,
            R.id.ll_home_forteen ,
            R.id.ll_home_fifteen ,
            R.id.ll_home_sixteen ,
            R.id.ll_home_17 ,
            R.id.ll_home_18 ,
            R.id.ll_home_19 ,
            R.id.ll_home_20 ,
            R.id.ll_home_21 ,
            R.id.ll_home_22 ,
            R.id.ll_home_23 ,
            R.id.ll_home_24
    };

    @Override
    public void onClick(View v) {
        int position = 0 ;
        Class aClass = null ;
        switch ( v.getId() ){
            case R.id.ll_home_one:
                position = 0 ;
                aClass = MockCategoryActivity.class ;
                break;
            case R.id.ll_home_two:
                position = 1 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_three:
                position = 2 ;
                aClass = CalenderActivity.class ;
                break;
            case R.id.ll_home_four:
                position = 3 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_five:
                position = 4 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_six:
                position = 5 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_seven:
                position = 6 ;
                aClass = NotesMCQActivity.class ;
                break;
            case R.id.ll_home_eight:
                position = 7 ;
                aClass = NotesMCQActivity.class ;
                break;
            case R.id.ll_home_nine:
                position = 8 ;
                aClass = NotesMCQActivity.class ;
                break;
            case R.id.ll_home_ten:
                aClass = NotesMCQActivity.class ;
                position = 9 ;
                break;
            case R.id.ll_home_eleven:
                position = 10 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_twelve:
                position = 11 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_thirteen:
                position = 12 ;
                aClass = CategoryActivity.class ;
                break;
            case R.id.ll_home_forteen:
                position = 13 ;
                aClass = PDFListActivity.class ;
                break;
            case R.id.ll_home_fifteen:
                position = 14 ;
                aClass = NewsPaperActivity.class ;
                break;
            case R.id.ll_home_sixteen:
                position = 15 ;
                aClass = PDFListActivity.class ;
                break;
            case R.id.ll_home_17:
                position = 16 ;
                aClass = MockCategoryActivity.class ;
                break;
            case R.id.ll_home_18:
                position = 17 ;
                aClass = MockCategoryActivity.class ;
                break;
            case R.id.ll_home_19:
                position = 18 ;
                aClass = MockCategoryActivity.class ;
                break;
            case R.id.ll_home_20:
                Intent categoryIntent= new Intent(MainActivity.this, ClassesActivity.class);
                categoryIntent.putExtra(AppConstant.BOOKTYPE, AppConstant.NCERTBOOKS);
                startActivity(categoryIntent);
                return;
            case R.id.ll_home_21:
                Intent categoryIntent1= new Intent(MainActivity.this, ClassesActivity.class);
                categoryIntent1.putExtra(AppConstant.BOOKTYPE, AppConstant.NCERTSOLUTIONENGLISH);
                startActivity(categoryIntent1);
                return;
            case R.id.ll_header:
                SupportUtil.openProfile(this , true);
                return;
            case R.id.ll_home_22:
                openRewardDialog();
                return;
            case R.id.ll_home_23:
                openPayActivity( true , AdFreePointsActivity.class );
                return;
            case R.id.ll_home_24:
                openPayActivity( false , AdFreePointsActivity.class );
                return;
            case R.id.tv_all_india_test:
                openPayActivity( false , PaidHomeActivity.class );
                return;
            case R.id.tv_important_updates:
            case R.id.rl_menu_imp_update:
                openImpCatActivity( ((TextView)findViewById( R.id.tv_important_updates )).getText().toString() , false );
                return;
            case R.id.quizzo:
//                startActivity(new Intent( this , GameActivity.class ));
                return;
        }

        openCategory( aClass , position , false , 0 );
        generateGAMain( getResources().getStringArray( R.array.main_cat_name )[ position ] );
        if( SupportUtil.isNotConnected( this ) )
            SupportUtil.showToastInternet( this );
    }

    private void openPayActivity( boolean isAdFree , Class aClass ){
        if ( AppApplication.getInstance().getLoginSdk() != null && !AppApplication.getInstance().getLoginSdk().isRegComplete() ){
            SupportUtil.openLoginDialog(this , false);
        }else if ( SupportUtil.isNotConnected(this) ){
            SupportUtil.showToastInternet(this);
        }else {
            Intent intent = new Intent(this, aClass );
            intent.putExtra( AppConstant.TYPE , isAdFree );
            startActivity( intent );
        }
    }

    private void openCategory( Class aClass , int position , boolean isNotification , int articleId ){
        try {
            Intent intent = new Intent( this , aClass );
            intent.putExtra( AppConstant.POSITION , position );
            intent.putExtra( AppConstant.WEB_VIEW , AppConstant.CATEGORY_WEB[ position ] == AppConstant.INT_TRUE );
            intent.putExtra( AppConstant.CAT_ID , ids[ position ] );
            intent.putExtra( AppConstant.DATA , AppConstant.CATEGORY_EXIST[ position ] == AppConstant.INT_TRUE );
            if ( isNotification ){
                intent.putExtra( AppConstant.CATEGORY , isNotification );
                intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , articleId );
            }
            startActivity( intent );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCategory( Class aClass , int position , boolean isNotification , int articleId , int catId ){
        Intent intent = new Intent( this , aClass );
        intent.putExtra( AppConstant.POSITION , position );
        intent.putExtra( AppConstant.WEB_VIEW , AppConstant.CATEGORY_WEB[ position ] == AppConstant.INT_TRUE );
        intent.putExtra( AppConstant.CAT_ID , catId );
        intent.putExtra( AppConstant.DATA , AppConstant.CATEGORY_EXIST[ position ] == AppConstant.INT_TRUE );
        if ( isNotification ){
            intent.putExtra( AppConstant.CATEGORY , isNotification );
            intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , articleId );
        }
        startActivity( intent );
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked( true );
        drawerLayout.closeDrawers();
//        String s = SharedPrefUtil.getString(AppConstant.SharedPref.PLAYER_ID);
        switch ( item.getItemId() ){
            case R.id.nav_share:
                SupportUtil.share( "" , this );
                break;
            case R.id.nav_rate_us:
            case R.id.nav_update:
                rateUs();
                break;
            case R.id.nav_more_apps :
                moreApps();
                break;
/*
            case R.id.nav_gk :
                openGKApp();
                break;
*/
            case R.id.nav_facebook :
                facebook();
                break;
            case R.id.nav_settings :
                Intent intent = new Intent( this , SettingActivity.class );
                startActivity( intent );
                break;
            case R.id.nav_leader_board :
                SupportUtil.openMockLeaderBoard(this);
                break;
            case R.id.nav_login :
                SupportUtil.openProfile(this , true);
                break;
        }
        return true;
    }

    private void openGKApp(){
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getString(R.string.package_other)));
        startActivity(rateIntent);
    }

    private void openImpCatActivity( String s , boolean type ){
        Intent intent = new Intent( this , ImpCategoryActivity.class );
        intent.putExtra( AppConstant.CAT_ID , AppValues.ID_IMP_UPDATES );
        intent.putExtra( AppConstant.TITLE , s );
        intent.putExtra( AppConstant.TYPE , type );
        startActivityForResult(intent , INTENT_IMP_UPDATE);
    }

    private void openImpCatActivity( String s , boolean type , int articleId ){
        Intent intent = new Intent( this , ImpCategoryActivity.class );
        intent.putExtra( AppConstant.CAT_ID , AppValues.ID_IMP_UPDATES );
        intent.putExtra( AppConstant.TITLE , s );
        intent.putExtra( AppConstant.TYPE , type );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , articleId );
        intent  .putExtra( AppConstant.CATEGORY , true );
        startActivityForResult(intent , INTENT_IMP_UPDATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( requestCode == INTENT_IMP_UPDATE ){
            SupportUtil.getImpCountFromDB(this);
        }
    }

    private int getCatPosition(int id ){
        int pos = 0 ;
        boolean isCat = true ;
        for ( int c = 0 ; c < ids.length ; c++ ){
            if ( id == ids[ c ] ){
                pos = c ;
                isCat= false ;
            }
        }
        if ( isCat ){
            for ( int i = 0 ; i < AppValues.MOCK_CAT_ID.length ; i++ ){
                if ( id == AppValues.MOCK_CAT_ID[i] || id == AppValues.NOTES_CAT_ID[i] )
                    pos = AppConstant.POSITION_MARGIN_MIDDLE + i ;
            }
        }
        return pos ;
    }

    private Class getCatClass( int position ){
        if ( position > 5 && position < 10 )
            return NotesMCQActivity.class ;
        else if ( position == 0 || position == 16|| position == 17|| position == 18 )
            return MockCategoryActivity.class ;
        else if ( position == 2 )
            return CalenderActivity.class ;
        else
            return ( position == 13 || position == 15 ) ? PDFListActivity.class : CategoryActivity.class ;
    }


    private void openUpdateDialog(String msg , boolean isTypeSkip) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Update ");
        builder.setMessage(msg)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SupportUtil.rateUs(MainActivity.this);
                        dialog.dismiss();
                    }
                });
        if ( isTypeSkip ) {
            builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    public void onImpCount(int count) {
        if ( count > 0 && tvMenuImpUpdateCount != null ){
            tvMenuImpUpdateCount.setVisibility(View.VISIBLE);
            tvMenuImpUpdateCount.setText(count + "");
        }else {
            tvMenuImpUpdateCount.setVisibility(View.GONE);
        }
    }
}
