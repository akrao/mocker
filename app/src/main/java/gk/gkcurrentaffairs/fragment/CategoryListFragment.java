package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.SubCatActivity;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CatBean;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class CategoryListFragment extends Fragment implements CategoryListAdapter.OnCustomClick{

    private DbHelper dbHelper ;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private View view;
    private Activity activity ;
    private int catId = 0 , image;
    private SwipeRefreshLayout swipeRefreshLayout ;
    private ArrayList<CategoryBean> categoryBeen ;
    private View llNoData ;
    private boolean isTypePdf , isWebView ;
    private CategoryProperty categoryProperty = null ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity() ;
        initObjects();
        return view ;
    }

    private void initObjects(){
        dbHelper = AppApplication.getInstance().getDBObject();
        Bundle bundle = getArguments();
        if ( bundle != null ) {
            catId = bundle.getInt( AppConstant.CAT_ID );
            image = bundle.getInt( AppConstant.IMAGE);
            isTypePdf = bundle.getBoolean( AppConstant.CLICK_ITEM_ARTICLE , false );
            isWebView = bundle.getBoolean( AppConstant.WEB_VIEW , false );
            if ( bundle.getSerializable( AppConstant.PROPERTY ) != null && bundle.getSerializable(AppConstant.PROPERTY).getClass() == CategoryProperty.class ){
                categoryProperty = (CategoryProperty) bundle.getSerializable(AppConstant.PROPERTY);
            }
            if ( catId != 0 ){
                initViews();
                new DataFromDB(null).execute();
            }

        }
    }

    private void initViews(){
        mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mLayoutManager = new GridLayoutManager( activity , 2 );
        mRecyclerView.setLayoutManager(mLayoutManager);
        swipeRefreshLayout = ( (SwipeRefreshLayout) view.findViewById( R.id.swipeRefreshLayout ));
        swipeRefreshLayout.setEnabled( false );
        llNoData = view.findViewById( R.id.ll_no_data );
    }

    private void setUpList(){
        llNoData.setVisibility( View.GONE );
        mAdapter = new CategoryListAdapter( categoryBeen, this , R.layout.item_grid );
        mRecyclerView.setAdapter( mAdapter );
    }


    @Override
    public void onCustomItemClick(int position) {
        Intent intent = new Intent( activity , SubCatActivity.class);
        intent.putExtra( AppConstant.CAT_ID , catId );
        intent.putExtra( AppConstant.DATA , categoryBeen.get( position ).getCategoryId() );
        intent.putExtra( AppConstant.IMAGE , categoryBeen.get( position ).getCategoryImage() );
        intent.putExtra( AppConstant.IMAGE_URL , categoryBeen.get( position ).getImageUrl() );
        intent.putExtra( AppConstant.TITLE , categoryBeen.get( position ).getCategoryName() );
        intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , isTypePdf );
        intent.putExtra( AppConstant.WEB_VIEW , isWebView);
        intent.putExtra( AppConstant.PROPERTY , categoryProperty);
        activity.startActivity( intent );
        SupportUtil.generateGASub( ((AppCompatActivity)activity).getSupportActionBar().getTitle().toString() , categoryBeen.get( position ).getCategoryName() );
    }

    class DataFromDB extends AsyncTask<Void , Void , Void> {

        private List<CatBean> catBeans ;

        public DataFromDB(List<CatBean> catBeans) {
            this.catBeans = catBeans;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if ( catBeans != null && catBeans.size() > 0 ){
                        dbHelper.insertCat(catBeans);
                    }
                    ArrayList<CategoryBean> list = dbHelper.fetchCategoryData( catId , AppData.getInstance().getImageRes().get( catId ) , image );
                    if ( list!= null && list.size() > 0) {
                        if ( catId == AppValues.CAA_ID )
                            categoryBeen = getCAAList( list ) ;
                        else
                            categoryBeen = list ;
                    }
//                    else
                        handleCat();

                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( categoryBeen != null && categoryBeen.size() > 0 )
                setUpList();
        }
    }

    private boolean isFirstHit = true ;
    private void handleCat(){
        if ( isFirstHit ) {
            isFirstHit = false ;
            Map<String, String> map = new HashMap<>(2);
            map.put("id", activity.getPackageName());
            map.put("parent_id", catId + "");
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN, ApiEndPoint.GET_HOME_DATA, map, new ConfigManager.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    if (status && !SupportUtil.isEmptyOrNull(data)) {
                        try {
                            final List<CatBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<CatBean>>() {
                            }.getType());
                            if (list != null && list.size() > 0) {
                                dbHelper.callDBFunction(new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        new DataFromDB(list).execute();
                                        return null;
                                    }
                                });
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

    }
    private void handleCat1(){
        if ( !SupportUtil.isNotConnected(activity)){
/*
            AppApplication.getInstance().getNetworkObject().getCatData(activity.getPackageName() , catId).enqueue(new Callback<List<CatBean>>() {
                @Override
                public void onResponse(Call<List<CatBean>> call, final Response<List<CatBean>> response) {
                    if ( response != null && response.body() != null && response.body().size() > 0 ) {
                        dbHelper.callDBFunction(new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                dbHelper.insertCat(response.body());
                                new DataFromDB().execute();
                                return null;
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<List<CatBean>> call, Throwable t) {

                }
            });
*/
        }
    }


    private ArrayList<CategoryBean> getCAAList( ArrayList<CategoryBean> categoryBeen ){
        ArrayList<CategoryBean> list = new ArrayList<>( categoryBeen.size() );
        CategoryBean catId = new CategoryBean();
        catId.setCategoryId( Integer.parseInt( activity.getString( R.string.ca_one_min_id ) ) );
        catId.setCategoryName( activity.getResources().getString( R.string.ca_one_minute ) );
        catId.setCategoryImage( R.drawable.ca_one_min );
        list.add(catId);
        catId = new CategoryBean();
        catId.setCategoryId( Integer.parseInt( getString( R.string.capsule_id )));
        catId.setCategoryName( activity.getResources().getString( R.string.capsule ) );
        catId.setCategoryImage( R.drawable.capsule );
        list.add(catId);
        int count = categoryBeen.size() - 2;
        for ( int i = 0 ; i < count ; i++ )
            list.add( categoryBeen.get( i ) );

        return list ;
    }

}
