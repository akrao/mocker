/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gk.gkcurrentaffairs.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.PDFViewerActivity;
import gk.gkcurrentaffairs.bean.HomeBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;
import gk.gkcurrentaffairs.util.TouchImageView;

/**
 * This fragment has a big {@ImageView} that shows PDF pages, and 2
 * {@link Button}s to move between pages. We use a
 * {@link PdfRenderer} to render PDF pages as
 * {@link Bitmap}s.
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class PdfRendererBasicFragment extends Fragment implements View.OnClickListener {

    /**
     * Key string for saving the state of current page index.
     */
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";

    /**
     * The filename of the PDF.
     */
    private static final String FILENAME = "sample.pdf";

    /**
     * File descriptor of the PDF.
     */
    private ParcelFileDescriptor mFileDescriptor;

    /**
     * {@link PdfRenderer} to render the PDF.
     */
    private PdfRenderer mPdfRenderer;

    /**
     * Page that is currently shown on the screen.
     */
    private PdfRenderer.Page mCurrentPage;

    /**
     * {@link ImageView} that shows a PDF page as a {@link Bitmap}
     */
    private TouchImageView mImageView;

    /**
     * {@link Button} to move to the previous page.
     */
    private Button mButtonPrevious;

    /**
     * {@link Button} to move to the next page.
     */
    private Button mButtonNext;

    /**
     * PDF page index
     */
    private int mPageIndex;

    public PdfRendererBasicFragment() {
    }

    private Activity activity ;
    private View view ;
    private String fileName , fileUrl ;
    private static final int  PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 256 ;
    private ProgressDialog pDialog;
    private ProgressBar progressBar ;
    private TextView tvIndex ;
    private int id , catId ;
    private HomeBean homeBean ;
    private DbHelper dbHelper ;
    private boolean isFileDownloaded , isTypeDownload ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pdf_renderer_basic, container, false);
        return view ;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Retain view references.
        mImageView = (TouchImageView) view.findViewById(R.id.image);
        mButtonPrevious = (Button) view.findViewById(R.id.previous);
        mButtonNext = (Button) view.findViewById(R.id.next);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tvIndex = (TextView) view.findViewById(R.id.tv_index);
        // Bind events.
        mButtonPrevious.setOnClickListener(this);
        mButtonNext.setOnClickListener(this);
        tvIndex.setOnClickListener(this);

        mPageIndex = 0;
        // If there is a savedInstanceState (screen orientations, etc.), we restore the page index.
        if (null != savedInstanceState) {
            mPageIndex = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
        }

        activity = getActivity();
        initArgs();
        initData();
        if (shouldAskPermissions() )
            askPermissions();
        else
            handleFile(false);


        SupportUtil.initAds( (RelativeLayout) view.findViewById( R.id.ll_ad ) , activity , AppConstant.ADS_BANNER );
    }

    private void initData(){
        try {
            dbHelper = AppApplication.getInstance().getDBObject();
            PDFViewerActivity pdfViewerActivity = ( PDFViewerActivity ) activity ;
            if ( pdfViewerActivity != null ){
                homeBean = pdfViewerActivity.homeBean ;
                id = pdfViewerActivity.id ;
                catId = pdfViewerActivity.catId ;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initArgs(){
        Bundle bundle = getArguments();
        if ( bundle != null ){
            fileName = bundle.getString( AppConstant.DATA );
            fileUrl = bundle.getString( AppConstant.CLICK_ITEM_ARTICLE );
            isFileDownloaded = bundle.getBoolean( AppConstant.POSITION , false );
            isTypeDownload = bundle.getBoolean( AppConstant.TYPE , false );
        }
    }

    protected boolean shouldAskPermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    handleFile(false);
                } else {
                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @TargetApi(23)
    protected void askPermissions() {
        String[] permissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        requestPermissions(permissions, PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
    }

    private void handleFile( boolean isFromDownload ){
        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + fileName;
        File file = new File(filePath);
        if ( !isFromDownload && !isFileDownloaded && file.exists() && isTypeDownload )
            file.delete();
        else if ( !isFileDownloaded && isFileDownloaded && !file.exists() )
            new updateStatus().execute();

        if ( file.exists()  ){
            openPDF(file);
        }else {
            if (isFromDownload )
                Toast.makeText( activity , "Error in downloading" , Toast.LENGTH_SHORT ).show();
            else
                new DownloadFileFromURL().execute(fileUrl);
        }
    }

    private void initDialog() throws Exception{
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Downloading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void openPDF( File file ){
        try {
            openPDFRenderer(file);
            new loadPDF().execute(mPageIndex);
//            showPage(mPageIndex);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openPDFRenderer( File file ) throws Exception{
            mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
            // This is the PdfRenderer we use to render the PDF.
            if (mFileDescriptor != null ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mPdfRenderer = new PdfRenderer(mFileDescriptor);
                }
            }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            closeRenderer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mCurrentPage) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                outState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
            }
        }
    }

    /**
     * Closes the {@link PdfRenderer} and related resources.
     *
     * @throws IOException When the PDF file cannot be closed.
     */
    private void closeRenderer() throws IOException {
        try {
            if (null != mCurrentPage ) {
                mCurrentPage.close();
            }
            if ( mPdfRenderer != null ) {
                mPdfRenderer.close();
            }
            if ( mFileDescriptor != null ) {
                mFileDescriptor.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the specified page of PDF to the screen.
     *
     * @param index The page index.
     */
    private Bitmap bitmap ;
    private void showPage(int index) {

        try {
            if (mPdfRenderer.getPageCount() <= index) {
                return;
            }
            // Make sure to close the current page before opening another one.
            if (null != mCurrentPage) {
                mCurrentPage.close();
            }
            // Use `openPage` to open a specific page in PDF.
            mCurrentPage = mPdfRenderer.openPage(index);
            // Important: the destination bitmap must be ARGB (not RGB).

//        Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
//                Bitmap.Config.ARGB_8888);
            bitmap = Bitmap.createBitmap(
                    activity.getResources().getDisplayMetrics().densityDpi / 72 * mCurrentPage.getWidth(),
                    activity.getResources().getDisplayMetrics().densityDpi / 72 * mCurrentPage.getHeight(),
                    Bitmap.Config.ARGB_8888);        // Here, we render the page onto the Bitmap.
            // To render a portion of the page, use the second and third parameter. Pass nulls to get
            // the default result.
            // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            // We are ready to show the Bitmap to user.
//        mImageView.setImageBitmap(bitmap);
//        updateUi();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the state of 2 control buttons in response to the current page index.
     */
    private void updateUi() {
        int index = mCurrentPage.getIndex();
        int pageCount = mPdfRenderer.getPageCount();
        mButtonPrevious.setEnabled(0 != index);
        mButtonNext.setEnabled(index + 1 < pageCount);
//        getActivity().setTitle(getString(R.string.app_name_with_index, index + 1, pageCount));
//        activity.setTitle( index + 1 +"/"+ pageCount);
        tvIndex.setText( index + 1 +"/"+ pageCount);

    }

    /**
     * Gets the number of pages in the PDF. This method is marked as public for testing.
     *
     * @return The number of pages.
     */
    public int getPageCount() {
        return mPdfRenderer.getPageCount();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.previous: {
                --mPageIndex;
                if ( mCurrentPage != null ) {
                    new loadPDF().execute(mCurrentPage.getIndex() - 1);
                }else {
                    new loadPDF().execute(mPageIndex );
                }
//                showPage(mCurrentPage.getIndex() - 1);
                break;
            }
            case R.id.next: {
                ++mPageIndex ;
                if ( mCurrentPage != null ) {
                    new loadPDF().execute(mCurrentPage.getIndex() + 1);
                }else {
                    new loadPDF().execute(mPageIndex);
                }
                break;
            }
            case R.id.tv_index: {
                openDialog();
                break;
            }
        }
    }

    private void openDialog(){
        try {
            if ( mPdfRenderer != null && mPdfRenderer.getPageCount() > 0 ) {
                int l = getPageCount() ;
                ArrayList<String> displayValues=new ArrayList<>();
                for ( int i = 0 ; i < l ; i++ ) {
                    int count = i + 1 ;
                    displayValues.add("Page : " + count);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,android.R.layout.simple_list_item_1,displayValues);
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Go to Page");
                builder.setSingleChoiceItems(adapter, mPageIndex, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        mPageIndex = position ;
                        new loadPDF().execute(position);
                        dialog.dismiss();
                    }
                });

                builder.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class loadPDF extends AsyncTask<Integer , Void , Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                mImageView.setImageBitmap(bitmap);
                updateUi();
                progressBar.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Integer... params) {
            showPage(params[0]);
            return null;
        }
    }

    class updateStatus extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle(id, DbHelper.COLUMN_READ, AppConstant.INT_FALSE, catId);
                    return null;
                }
            });
            return null;
        }
    }

        class DownloadFileFromURL extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            downloadFile(params[0]);
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle( id , DbHelper.COLUMN_READ , AppConstant.INT_TRUE , catId);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            try {
                if ( pDialog != null && pDialog.isShowing() )
                    pDialog.setProgress(Integer.parseInt(values[0]));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if ( pDialog != null && pDialog.isShowing() )
                    pDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            handleFile(true);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                initDialog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void downloadFile(String fileUrl){
            int count;
            try {

                File fileStorage = new File(
                        Environment.getExternalStorageDirectory() + "/"
                                + AppConstant.downloadDirectory);

                if (!fileStorage.exists()) {
                    fileStorage.mkdir();
                }

                File outputFile = new File(fileStorage, fileName);//Create Output file in Main File

                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                }

                URL url = new URL(fileUrl);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
               InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(outputFile);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

        }
    }


}
