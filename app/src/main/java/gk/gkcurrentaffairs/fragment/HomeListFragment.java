package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.adapter.HomeListAdapter;
import gk.gkcurrentaffairs.bean.CatRankBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.HomePageExtraData;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.AppConstant.HOME_TITLE_ARRAY;

/**
 * Created by Amit on 5/30/2018.
 */

public class HomeListFragment extends Fragment implements HomeListAdapter.OnCustomClick {

    private RecyclerView mRecyclerView;
    private Activity activity;
    HomeListAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
//        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity();
        mRecyclerView = new RecyclerView(activity);
        initView();
        registerReceiverConfig();
        return mRecyclerView;
    }

    private void registerReceiverConfig() {
        if (!AppApplication.getInstance().getConfigManager().isConfigLoaded()) {
            activity.registerReceiver(broadcastReceiver, new IntentFilter(activity.getPackageName() + ConfigConstant.CONFIG_LOADED));
        } else
            fetchHomePageData();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            fetchHomePageData();
        }
    };

    private void fetchHomePageData() {
        SupportUtil.getHomeData(new SupportUtil.OnHomePageExtraData() {
            @Override
            public void onHomePageExtraData(HomePageExtraData homePageExtraData) {
                if (homePageExtraData != null) {
                    handleHomePageData(homePageExtraData);
                }
            }
        });
    }

    private void handleHomePageData(HomePageExtraData homePageExtraData) {
        mAdapter.setImagePath(homePageExtraData.getImagePath());
        mAdapter.setSlidersBeanList(homePageExtraData.getSlidersBeanList());
        AppData.getInstance().getHomePageDataMap().put(HOME_TITLE_ARRAY[2], getExamTargetList(homePageExtraData));
        mAdapter.setListHashMap(AppData.getInstance().getHomePageDataMap());
        mAdapter.notifyDataSetChanged();
    }

    private List<CategoryProperty> getExamTargetList(HomePageExtraData homePageExtraData) {
        List<CategoryProperty> list = new ArrayList<>(4);
        CategoryProperty categoryProperty;
        for (int i = 0; i < 3; i++) {
            CatRankBean catRankBean = homePageExtraData.getCatRankBeans().get(i);
            list.add(SupportUtil.getExamTargetCatProperty(catRankBean.getTitle(), catRankBean.getId()
                    , homePageExtraData.getCategoryImagePath() + catRankBean.getImage(), i));
        }
        list.add(AppData.getInstance().getHomePageDataMap().get(HOME_TITLE_ARRAY[2]).get(3));
        return list;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null)
            mAdapter.setActive(false);

        try {
            if (activity != null) {
                activity.unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                try {
                    super.onLayoutChildren(recycler, state);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mAdapter = new HomeListAdapter(rankList, this, activity);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateList();
    }

    private List<String> rankList = new ArrayList<>(HOME_TITLE_ARRAY.length);

    private void updateList() {
        rankList.clear();
        if (AppPreferences.getHomeRank(activity, HOME_TITLE_ARRAY[1]) == 0) {
            rankList.addAll(Arrays.asList(HOME_TITLE_ARRAY));
        } else {
            HashMap<Integer, String> hashMap = new HashMap<>(HOME_TITLE_ARRAY.length);

            for (String t : HOME_TITLE_ARRAY) {
                Logger.e("AppPreferences.getHomeRank(activity, t) -- " + AppPreferences.getHomeRank(activity, t));
                Logger.e("AppPreferences.t -- " + t);
                hashMap.put(AppPreferences.getHomeRank(activity, t), t);
            }
            for (int i = 0; i < HOME_TITLE_ARRAY.length; i++) {
                rankList.add(hashMap.get(i));
            }

        }
        AppData.getInstance().setRankList(rankList);
        mAdapter.setListHashMap(AppData.getInstance().getHomePageDataMap());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCustomClick(int positionParent, int position, boolean isChangePosition) {
        if (isChangePosition) {
            openChangePositionDialog(positionParent);
        } else {
            if (SupportUtil.isNotConnected(activity)) {
                SupportUtil.showToastInternet(activity);
            }
            SupportUtil.openCategory(positionParent, position , activity);
        }
    }

    private void openChangePositionDialog(final int position) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(activity);
        }
        builder.setTitle(rankList.get(position) + " Current Position : " + position);
//        builder.setMessage(rankList.get(position) + " Current Position : " + position + ".\n Change Position");
        builder.setItems(getListSwap(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                SupportUtil.showToastCentre(activity, rankList.get(position) + " position is updated.");
                updatePositionPrefs(position, i + 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.create().show();

    }

    private String[] getListSwap() {
        String[] strings = new String[rankList.size() - 1];
        for (int i = 1; i < rankList.size(); i++) {
            strings[i - 1] = "Move to " + i + " Position";
        }
        return strings;
    }




    private void updatePositionPrefs(int currentPos, int newPos) {
        for (int i = 0; i < HOME_TITLE_ARRAY.length; i++) {
            AppPreferences.setHomeRank(activity, i, rankList.get(i));
        }
        AppPreferences.setHomeRank(activity, currentPos, rankList.get(newPos));
        AppPreferences.setHomeRank(activity, newPos, rankList.get(currentPos));
        updateList();
    }

}


