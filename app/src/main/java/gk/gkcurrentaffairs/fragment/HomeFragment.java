package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.CalenderActivity;
import gk.gkcurrentaffairs.activity.CategoryActivity;
import gk.gkcurrentaffairs.activity.MockCategoryActivity;
import gk.gkcurrentaffairs.activity.NewsPaperActivity;
import gk.gkcurrentaffairs.activity.NotesMCQActivity;
import gk.gkcurrentaffairs.activity.PDFListActivity;
import gk.gkcurrentaffairs.ncert.activity.ClassesActivity;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/11/2018.
 */

public class HomeFragment extends Fragment implements View.OnClickListener{

    private View view;
    private Activity activity;
    private TextView[] textViews;
    private String[] titles;
    private int[] ids;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_home, container, false);
        activity = getActivity();
        initViews();
        return view;
    }


    private void initViews() {
        textViews = new TextView[textViewIds.length];
        titles = activity.getResources().getStringArray(R.array.main_cat_name);
        ids = activity.getResources().getIntArray(R.array.main_cat_id);
        for (int i = 0; i < textViewIds.length; i++) {
            textViews[i] = (TextView) view.findViewById(textViewIds[i]);
            textViews[i].setText(titles[i]);
            view.findViewById(linearLayoutIds[i]).setOnClickListener(this);
        }
        view.findViewById(R.id.tv_all_india_test).setOnClickListener(this);
        view.findViewById(R.id.tv_important_updates).setOnClickListener(this);
        view.findViewById(R.id.quizzo).setOnClickListener(this);
    }

    private int[] textViewIds = {
            R.id.tv_home_one,
            R.id.tv_home_two,
            R.id.tv_home_three,
            R.id.tv_home_four,
            R.id.tv_home_five,
            R.id.tv_home_six,
            R.id.tv_home_seven,
            R.id.tv_home_eight,
            R.id.tv_home_nine,
            R.id.tv_home_ten,
            R.id.tv_home_eleven,
            R.id.tv_home_twelve,
            R.id.tv_home_thirteen,
            R.id.tv_home_forteen,
            R.id.tv_home_fifteen,
            R.id.tv_home_sixteen,
            R.id.tv_home_17,
            R.id.tv_home_18,
            R.id.tv_home_19,
            R.id.tv_home_20,
            R.id.tv_home_21,
            R.id.tv_home_22,
            R.id.tv_home_23,
            R.id.tv_home_24,
            R.id.tv_home_25,
            R.id.tv_home_26,
            R.id.tv_home_27
    };

    private static final int[] linearLayoutIds = {
            R.id.ll_home_one,
            R.id.ll_home_two,
            R.id.ll_home_three,
            R.id.ll_home_four,
            R.id.ll_home_five,
            R.id.ll_home_six,
            R.id.ll_home_seven,
            R.id.ll_home_eight,
            R.id.ll_home_nine,
            R.id.ll_home_ten,
            R.id.ll_home_eleven,
            R.id.ll_home_twelve,
            R.id.ll_home_thirteen,
            R.id.ll_home_forteen,
            R.id.ll_home_fifteen,
            R.id.ll_home_sixteen,
            R.id.ll_home_17,
            R.id.ll_home_18,
            R.id.ll_home_19,
            R.id.ll_home_20,
            R.id.ll_home_21,
            R.id.ll_home_22,
            R.id.ll_home_23,
            R.id.ll_home_24,
            R.id.ll_home_25,
            R.id.ll_home_26,
            R.id.ll_home_27
    };

    @Override
    public void onClick(View v) {
        int position = 0;
        Class aClass = null;
        switch (v.getId()) {
            case R.id.ll_home_one:
                position = 0;
                aClass = MockCategoryActivity.class;
                break;
            case R.id.ll_home_two:
                position = 1;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_three:
                position = 2;
                aClass = CalenderActivity.class;
                break;
            case R.id.ll_home_four:
                position = 3;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_five:
                position = 4;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_six:
                position = 5;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_seven:
                position = 6;
                aClass = NotesMCQActivity.class;
                break;
            case R.id.ll_home_eight:
                position = 7;
                aClass = NotesMCQActivity.class;
                break;
            case R.id.ll_home_nine:
                position = 8;
                aClass = NotesMCQActivity.class;
                break;
            case R.id.ll_home_ten:
                aClass = NotesMCQActivity.class;
                position = 9;
                break;
            case R.id.ll_home_eleven:
                position = 10;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_twelve:
                position = 11;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_thirteen:
                position = 12;
                aClass = CategoryActivity.class;
                break;
            case R.id.ll_home_forteen:
                position = 13;
                aClass = PDFListActivity.class;
                break;
            case R.id.ll_home_fifteen:
                position = 14;
                aClass = NewsPaperActivity.class;
                break;
            case R.id.ll_home_sixteen:
                position = 15;
                aClass = PDFListActivity.class;
                break;
            case R.id.ll_home_17:
                position = 16;
                aClass = MockCategoryActivity.class;
                break;
            case R.id.ll_home_18:
                position = 17;
                aClass = MockCategoryActivity.class;
                break;
            case R.id.ll_home_19:
                position = 18;
                aClass = MockCategoryActivity.class;
                break;
            case R.id.ll_home_20:
                Intent categoryIntent = new Intent(activity, ClassesActivity.class);
                categoryIntent.putExtra(AppConstant.BOOKTYPE, AppConstant.NCERTBOOKS);
                startActivity(categoryIntent);
                return;
            case R.id.ll_home_21:
                Intent categoryIntent1 = new Intent(activity, ClassesActivity.class);
                categoryIntent1.putExtra(AppConstant.BOOKTYPE, AppConstant.NCERTSOLUTIONENGLISH);
                startActivity(categoryIntent1);
                return;
            case R.id.quizzo:
//                activity.startActivity(new Intent(activity, GameActivity.class));
                return;
            default:
                return;
        }

        openCategory(aClass, position, false, 0);
//        generateGAMain( getResources().getStringArray( R.array.main_cat_name )[ position ] );
        if (SupportUtil.isNotConnected(activity))
            SupportUtil.showToastInternet(activity);
    }

    private void openCategory( Class aClass , int position , boolean isNotification , int articleId ){
        try {
            Intent intent = new Intent( activity , aClass );
            intent.putExtra( AppConstant.POSITION , position );
            intent.putExtra( AppConstant.WEB_VIEW , AppConstant.CATEGORY_WEB[ position ] == AppConstant.INT_TRUE );
            intent.putExtra( AppConstant.CAT_ID , ids[ position ] );
            intent.putExtra( AppConstant.DATA , AppConstant.CATEGORY_EXIST[ position ] == AppConstant.INT_TRUE );
            if ( isNotification ){
                intent.putExtra( AppConstant.CATEGORY , isNotification );
                intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , articleId );
            }
            startActivity( intent );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
