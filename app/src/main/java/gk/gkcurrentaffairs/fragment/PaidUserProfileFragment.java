package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.login.LoginSdk;
import com.login.Util;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.ImpCategoryActivity;
import gk.gkcurrentaffairs.payment.fragment.BaseFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 8/31/2018.
 */

public class PaidUserProfileFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private Activity activity;
    private AppCompatImageView ivProfilePic;
    private TextView tvUserName, tvPoints;
    private String userName;
    private int points ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_user_profile, container, false);
        activity = getActivity();
        return view;
    }

    private boolean isLoaded = false ;

    @Override
    public void onRefreshFragment() {
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
        loadData();
    }

    private void init(){
        if ( view != null ) {
            initViews();
            loadData();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
        loadData();
    }

    private void initViews(){
        tvUserName = (TextView) view.findViewById(R.id.tvUserName);
        tvPoints = (TextView) view.findViewById(R.id.tvUserPoints);
        ivProfilePic = (AppCompatImageView) view.findViewById(R.id.ivProfilePic);
        view.findViewById( R.id.ll_edit_profile ).setOnClickListener(this);
        view.findViewById( R.id.ll_view_profile ).setOnClickListener(this);
        view.findViewById( R.id.ll_share_app ).setOnClickListener(this);
        view.findViewById( R.id.ll_bookmark ).setOnClickListener(this);
        view.findViewById( R.id.ll_leader_board ).setOnClickListener(this);
        view.findViewById( R.id.ll_rate ).setOnClickListener(this);
        view.findViewById( R.id.ll_more_app ).setOnClickListener(this);
        view.findViewById( R.id.ll_update ).setOnClickListener(this);
        view.findViewById( R.id.ll_fb ).setOnClickListener(this);
    }

    private void loadData() {
        if ( isLoaded && AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null
                && AppApplication.getInstance().getLoginSdk().isRegComplete() && tvUserName != null ) {
            userName = AppApplication.getInstance().getLoginSdk().getUserName();
            Util.loadUserImage(AppApplication.getInstance().getLoginSdk().getUserImage(), ivProfilePic);
//            points = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_POINTS);
            tvUserName.setText(userName);
//            tvPoints.setText("Available points : " + points);
        }
    }

    @Override
    public void onClick(View v) {
        if ( SupportUtil.isNotConnected(activity) ) {
            SupportUtil.showToastInternet(activity);
        }
        switch ( v.getId() ){
            case R.id.ll_bookmark :
                openImpCatActivity("Important Updates", true);
                break;
            case R.id.ll_view_profile :
                LoginSdk.getInstance(activity , activity.getPackageName()).openProfile(activity , false );
                break;
            case R.id.ll_edit_profile :
                LoginSdk.getInstance(activity , activity.getPackageName()).openProfile(activity , true );
                break;
            case R.id.ll_share_app :
                SupportUtil.share( "" , activity );
                break;
            case R.id.ll_leader_board :
                SupportUtil.openMockLeaderBoard(activity);
                break;
            case R.id.ll_rate :
            case R.id.ll_update :
                rateUs();
                break;
            case R.id.ll_more_app :
                moreApps();
                break;
            case R.id.ll_fb :
                facebook();
                break;
        }
    }

    private void rateUs() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getPackageName()));
        startActivity(rateIntent);
    }

    private void facebook() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                "https://www.facebook.com/Mockers-299280660873730/?modal=admin_todo_tour"));
        startActivity(rateIntent);
    }

    private void moreApps() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Dexter_Dev"));
        startActivity(rateIntent);
    }


    private void openImpCatActivity(String s, boolean type) {
        Intent intent = new Intent(activity, ImpCategoryActivity.class);
        intent.putExtra(AppConstant.CAT_ID, AppValues.ID_IMP_UPDATES);
        intent.putExtra(AppConstant.TITLE, s);
        intent.putExtra(AppConstant.TYPE, type);
        startActivity(intent);
    }

}
