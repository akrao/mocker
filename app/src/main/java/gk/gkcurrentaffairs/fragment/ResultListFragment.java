package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.MCQActivity;
import gk.gkcurrentaffairs.activity.ResultActivity;
import gk.gkcurrentaffairs.adapter.ResultAdapter;
import gk.gkcurrentaffairs.bean.ResultBean;
import gk.gkcurrentaffairs.bean.ResultDataBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 3/3/2017.
 */

public class ResultListFragment extends Fragment implements ResultAdapter.OnCustomClick{

    private DbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private View view , llNoData ;
    private int catId , subCatID ;
    private String query ;
    private Activity activity ;
    private ArrayList<ResultBean> resultBeen = new ArrayList<>() ;
    ResultAdapter mAdapter ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity() ;
        initDataFromArg();
        initView(view);
        initData();
        return view ;
    }

    private void initDataFromArg(){
        Bundle bundle = getArguments();
        if (bundle != null) {
            catId = bundle.getInt( AppConstant.CAT_ID );
            subCatID = bundle.getInt( AppConstant.DATA , 0 );
            query = bundle.getString(AppConstant.QUERY);
        }
    }

    private void initData(){
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    private void initView( View view ){
        mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager( new LinearLayoutManager( activity ){
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                try {
                    super.onLayoutChildren(recycler, state);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } );
        SwipeRefreshLayout swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setEnabled(false);
        llNoData = view.findViewById(R.id.ll_no_data);
        mAdapter = new ResultAdapter( resultBeen , this , ((AppCompatActivity)activity).getSupportActionBar().getTitle().toString() );
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        new DataFromDB().execute();
    }

    @Override
    public void onCustomItemClick( int position , boolean isResultTrue ) {
        ResultBean bean = resultBeen.get( position ) ;
        Intent intent ;
        if ( isResultTrue ) {
            intent = new Intent(activity , ResultActivity.class);
            intent.putExtra(AppConstant.CORRECT_ANS, bean.getCorrectAns() );
            intent.putExtra(AppConstant.NUM_QUE, bean.getNumberQue() );
            intent.putExtra(AppConstant.WRONG_ANS, bean.getWrongAns() );
            intent.putExtra(AppConstant.DATA, bean.getId() );
            intent.putExtra(AppConstant.QUERY, true);
            intent.putExtra(AppConstant.TITLE,  resultBeen.get( position ).getTitle() );
            intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE,  resultBeen.get( position ).getTimeTaken() );
            if ( !SupportUtil.isEmptyOrNull(resultBeen.get(position).getData()) ){
                Gson gson = new Gson();
                ResultDataBean m = gson.fromJson(resultBeen.get( position ).getData(), ResultDataBean.class);
                intent.putExtra(AppConstant.RESULT_DATA, m );
            }
        }else {
            intent = new Intent( activity , MCQActivity.class);
            intent.putExtra( AppConstant.CAT_ID , catId );
            intent.putExtra( AppConstant.TITLE , resultBeen.get( position ).getTitle() );
            intent.putExtra( AppConstant.QUERY , getQuery( position ) );
            intent.putExtra( AppConstant.DATA , resultBeen.get( position ).getMockTestId() );
        }
        activity.startActivity(intent);
    }

    private String getQuery( int position ){
        return DbHelper.COLUMN_CAT_ID + "=" + catId + " AND " + DbHelper.COLUMN_MOCK_TEST_ID + "=" + resultBeen.get( position ).getMockTestId() ;
    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.getResultList( resultBeen , query );
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( resultBeen.size() > 0) {
                llNoData.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
            } else {
                view.findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                view.findViewById(R.id.player_progressbar).setVisibility(View.GONE);
            }
        }
    }

}
