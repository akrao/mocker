package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.SubCatActivity;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CatBean;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppData;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class MockCategoryListFragment extends Fragment implements CategoryListAdapter.OnCustomClick {

    private DbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private CategoryListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private View view;
    private Activity activity;
    private int catId;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<CategoryBean> categoryBeen;
    private int image;
    private String query, imageUrl;
    private boolean isMockTest, isServerOne;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity();
        initDataFromArg();
        initObjects();
        return view;
    }

    private void initObjects() {
        dbHelper = AppApplication.getInstance().getDBObject();
        initViews();
        new DataFromDB().execute();
    }

    CategoryProperty categoryProperty;

    private void initDataFromArg() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryProperty = (CategoryProperty) bundle.getSerializable(AppConstant.CAT_DATA);
            image = bundle.getInt(AppConstant.IMAGE);
            imageUrl = bundle.getString(AppConstant.IMAGE_URL);
            catId = bundle.getInt(AppConstant.CAT_ID);
            query = bundle.getString(AppConstant.QUERY);
            isMockTest = bundle.getBoolean(AppConstant.TYPE, false);
            isServerOne = SupportUtil.isServerOne(catId);
        }
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mLayoutManager = new GridLayoutManager(activity, isMockTest ? 2 : 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setEnabled(false);
        view.findViewById(R.id.ll_no_data).setVisibility(View.GONE);
    }

    private void setUpList() {
        mAdapter = new CategoryListAdapter(categoryBeen, this, isMockTest ? R.layout.item_grid : R.layout.item_list_image_text);
        mRecyclerView.setAdapter(mAdapter);
    }

    private String getApiHost() {
        if ( categoryProperty != null ) {
            return categoryProperty.getHost();
        } else {
            return ConfigConstant.HOST_MAIN ;
        }
//        return isServerOne ? ConfigConstant.HOST_MAIN : ConfigConstant.HOST_TRANSLATOR ;
    }

    @Override
    public void onCustomItemClick(int position) {
        Intent intent = new Intent(activity, SubCatActivity.class);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.CAT_DATA, categoryProperty);
        intent.putExtra(AppConstant.CATEGORY, true);
        intent.putExtra(AppConstant.DATA, categoryBeen.get(position).getCategoryId());
        intent.putExtra(AppConstant.TITLE, categoryBeen.get(position).getCategoryName());
        intent.putExtra(AppConstant.IMAGE, image);
        intent.putExtra(AppConstant.IMAGE_URL, imageUrl);
        activity.startActivity(intent);
        SupportUtil.generateGASub(((AppCompatActivity) activity).getSupportActionBar().getTitle().toString(), categoryBeen.get(position).getCategoryName());
    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    categoryBeen = dbHelper.MockFetchCategoryData(query, AppData.getInstance().getImageRes().get(catId), image);
                    return null;
                }
            });
            handleCat();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (categoryBeen != null && categoryBeen.size() > 0) {
                for (CategoryBean bean : categoryBeen) {
                    if (bean.getCategoryImage() == 0) {
                        bean.setImageUrl(imageUrl);
                    }
                }
                setUpList();
            }
/*
            else
                handleCat();
*/
        }
    }

    private boolean isFirstHit = true;

    private void handleCat() {
        if (isFirstHit) {
            isFirstHit = false;
            Map<String, String> map = new HashMap<>(2);
            map.put("id", activity.getPackageName());
            map.put("parent_id", catId + "");
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, getApiHost(), ApiEndPoint.GET_HOME_DATA, map, new ConfigManager.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    if (status && !SupportUtil.isEmptyOrNull(data)) {
                        try {
                            final List<CatBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<CatBean>>() {
                            }.getType());
                            if (list != null && list.size() > 0) {
                                dbHelper.callDBFunction(new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        dbHelper.insertCat(list);
                                        new DataFromDB().execute();
                                        return null;
                                    }
                                });
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void handleCat1() {
        if (!SupportUtil.isNotConnected(activity)) {
/*
            AppApplication.getInstance().getNetworkObject().getCatData(activity.getPackageName(), catId).enqueue(new Callback<List<CatBean>>() {
                @Override
                public void onResponse(Call<List<CatBean>> call, final Response<List<CatBean>> response) {
                    if ( response != null && response.body() != null && response.body().size() > 0 ) {
                        dbHelper.callDBFunction(new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                dbHelper.insertCat(response.body());
                                new DataFromDB().execute();
                                return null;
                            }
                        });
                    }
                    }

                @Override
                public void onFailure(Call<List<CatBean>> call, Throwable t) {

                }
            });
*/
        }
    }
}
