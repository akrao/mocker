package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.AndroidDownloadFileByProgressBarActivity;
import gk.gkcurrentaffairs.activity.CategoryActivity;
import gk.gkcurrentaffairs.activity.DescActivity;
import gk.gkcurrentaffairs.activity.PDFListActivity;
import gk.gkcurrentaffairs.adapter.HomeAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ListBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.CustomLinearLayoutManager;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class CategoryFragment extends Fragment implements HomeAdapter.OnLoadMore, HomeAdapter.OnClick, HomeAdapter.OnNextLoad,
        SwipeRefreshLayout.OnRefreshListener, SupportUtil.OnCustomResponse {

    private DbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private HomeAdapter mAdapter;
    private CustomLinearLayoutManager mLayoutManager;
    private ArrayList<ListBean> homeBeen = new ArrayList<>();
    private View llNoData;
    private View tvNoData , pbLoader ;
    private View viewLoadMore;
    private boolean canLoadMore = true, isNotFavTab = true, isFirstHit = true, isWebView, isTypePdf;
    private View view;
    private Activity activity;
    private int image;
    private boolean isMoreIds = true, isNotification;
    private String[] ids;
    private int catId = 0, subCatID;
    private String query = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    private final Handler handler = new Handler();
    private boolean isNetWorkCall = false;
    private CategoryProperty categoryProperty = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity();
        initObjects();
        initDataFromArg();
        initViews();
        initCategoryNames();
        setupList();
        return view;
    }

    HashMap<Integer, String> hashMapCategoryNames = null;
    private void initCategoryNames() {
        if (dbHelper != null) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    ArrayList<CategoryBean> categoryBeen = dbHelper.MockFetchCategoryData(DbHelper.COLUMN_CAT_ID + "=" + catId, null, image);
                    if (categoryBeen != null && categoryBeen.size() > 0) {
                        hashMapCategoryNames = new HashMap<>(categoryBeen.size());
                        for (CategoryBean beanData : categoryBeen) {
                            hashMapCategoryNames.put(beanData.getCategoryId(), beanData.getCategoryName());
                        }
                    }
                    return null;
                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        new DataFromDB().execute();
    }

    private void initObjects() {
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    private void initDataFromArg() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            catId = bundle.getInt(AppConstant.CAT_ID);
            subCatID = bundle.getInt(AppConstant.DATA, 0);
            isWebView = bundle.getBoolean(AppConstant.WEB_VIEW, false);
            isTypePdf = bundle.getBoolean(AppConstant.CLICK_ITEM_ARTICLE, false);
            isNotification = bundle.getBoolean(AppConstant.TYPE, false);
            image = bundle.getInt(AppConstant.IMAGE);
            if (catId != 0)
                query = bundle.getString(AppConstant.QUERY);
            if (query == null)
                query = "";
            isNotFavTab = !query.contains(DbHelper.COLUMN_FAV);
            if (bundle.getSerializable(AppConstant.CAT_DATA) != null && bundle.getSerializable(AppConstant.CAT_DATA).getClass() == CategoryProperty.class) {
                categoryProperty = (CategoryProperty) bundle.getSerializable(AppConstant.CAT_DATA);
            }
        }
    }

    private void initViews() {
        if ( view != null ) {
            mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
            mLayoutManager = new CustomLinearLayoutManager(activity);
            mRecyclerView.setLayoutManager(mLayoutManager);
            swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout));
            swipeRefreshLayout.setOnRefreshListener(this);
            llNoData = view.findViewById(R.id.ll_no_data);
            viewLoadMore = view.findViewById(R.id.ll_load_more);
            tvNoData = view.findViewById(R.id.tv_no_data);
            pbLoader = view.findViewById(R.id.player_progressbar);
        }
    }

    private void setupList() {
        mAdapter = new HomeAdapter(homeBeen, this, isNotFavTab ? this : null, isNotFavTab ? this : null, activity , hashMapCategoryNames);
        mAdapter.setImageRes(image);
        mAdapter.setCategoryProperty(categoryProperty);
//        mAdapter.setTypePdf(isTypePdf);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCustomItemClick(final int position, int type) {
        if ( type == HomeAdapter.OnClick.TYPE_ARTICLE ) {
            if (!isTypePdf) {
                Intent intent = new Intent(activity, DescActivity.class);
                intent.putExtra(AppConstant.DATA, homeBeen.get(position).getId());
                intent.putExtra(AppConstant.QUERY, query);
                intent.putExtra(AppConstant.CAT_ID, catId);
                intent.putExtra(AppConstant.WEB_VIEW, isWebView);
                activity.startActivity(intent);
            } else {
                positionLast = position;
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        String url = dbHelper.getUrlForPDF(homeBeen.get(position).getId());
                        if (!SupportUtil.isEmptyOrNull(url)) {
                            urllast = url;
                            openPdf(position, url, true);
//                            openPdf(position, url, isBasic);
                        }else {
                            SupportUtil.showToastCentre(activity , "Error, Please click on other PDF.");
                        }
                        return null;
                    }
                });
            }
        }else {
            updateFavStatus(position);
        }
    }

    private void updateFavStatus(final int position) {
        final boolean status = homeBeen.get(position).isFav();
        homeBeen.get(position).setFav(!status);
        mAdapter.notifyDataSetChanged();
        SupportUtil.showToastCentre( activity , status ? " Removed " : " Saved " );
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.updateArticle(homeBeen.get(position).getId(), dbHelper.COLUMN_FAV,
                        status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
                return null;
            }
        });
    }

    private void openPdf(int position, String url, boolean isBasic) {
            Intent intent;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /*intent = new Intent(activity, PDFViewerActivity.class);
            intent.putExtra(AppConstant.CAT_ID, catId);
            intent.putExtra(AppConstant.CATEGORY, homeBeen.get(position).getId());
            intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, AppPreferences.getUrlPdfDownload(activity) + url);
            intent.putExtra(AppConstant.DATA, catId + "-" + homeBeen.get(position).getId() + ".pdf");
            intent.putExtra(AppConstant.TITLE, homeBeen.get(position).getText());*/

            intent = new Intent(activity, AndroidDownloadFileByProgressBarActivity.class);
            String[] parts = url.split("/");
            intent.putExtra(AppConstant.imageName, parts[1]);
            intent.putExtra(AppConstant.BOOKID, Integer.parseInt(parts[0]));
//            intent.putExtra(AppConstant.isAdvanced, !isBasic);
            intent.putExtra(AppConstant.isAdvanced, false);
            intent.putExtra(AppConstant.HOST, ConfigConstant.HOST_DOWNLOAD_PDF);
            intent.putExtra(AppConstant.CATEGORY, AppPreferences.getBaseUrl(activity));
/*
        } else {
            intent = new Intent(activity, BrowserActivity.class);
            intent.putExtra(AppConstant.DATA, url);
            intent.putExtra(AppConstant.CATEGORY, true);
        }
*/
            startActivityForResult(intent, AppConstant.RESULT_PDF);

    }

    int positionLast;
    String urllast;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstant.RESULT_PDF && resultCode == Activity.RESULT_OK) {
            if (homeBeen != null && homeBeen.size() > positionLast) {
                openPdf(positionLast, urllast, false);
            }
        }
    }

    @Override
    public void onCustomLoadMore() {
        if ( SupportUtil.isConnected(activity) ) {
            loadMoreData();
        }
    }

    public void refreshFragment(){
        new DataFromDB().execute();
    }

    @Override
    public void onRefresh() {
        if (isNotFavTab && !isNotification)
            fetchLatestData(true);
    }

    String TAG = "amit";

    private void fetchLatestData(final boolean isLatest) {
        long id = (isLatest) ? AppConstant.MAX_VALUE : homeBeen.get(homeBeen.size() - 1).getId();
        if (!isNetWorkCall) {
            isNetWorkCall = true;
            Map<String, String> map = new HashMap<>(2);
            map.put("id", getCatId() + "");
            map.put("max_content_id", id + "");
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN, ApiEndPoint.GET_PREVIOUS_CONTENT_BY_CAT_ID
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            Logger.e( "fetchLatestData" );
                            isFirstHit = false;
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                List<ServerBean> list = null;
                                try {
                                    list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerBean>>() {
                                    }.getType());
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                                Logger.e( "fetchLatestData after convert" );
                                if (list != null && list.size() > 0) {
                                    new DataInsert(list, isCatDelete(isLatest)).execute();
                                    if (isCatDelete(isLatest) && activity instanceof CategoryActivity )
                                        ((CategoryActivity) activity).fetchLatestIds(AppConstant.MAX_VALUE);
                                    onCustomResponse(true);
                                } else {
                                    if (!isLatest)
                                        onCustomResponse(false);
                                    else {
                                        showNoData();
                                    }

                                }
                            }else
                                showNoData();
                            isNetWorkCall = false;
                            if ( swipeRefreshLayout != null ) {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    });
        }
    }

    private void showNoData() {
        if (!isFirstHit && (homeBeen == null || homeBeen.size() < 1) ) {
            showNoDataViews();
        }
    }

    private boolean isCatDelete(boolean isLat) {
        return isLat && ( activity instanceof CategoryActivity || activity instanceof PDFListActivity );
//        return isLat && catId == AppValues.GOVT_JOBS_ID && activity instanceof CategoryActivity;
    }

    public void fetchArticleForId(String[] ids) {
        if (!isNetWorkCall) {
            isNetWorkCall = true;
            Map<String, String> map = new HashMap<>(1);
            map.put("id_array", toString(ids));
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN
                    , ApiEndPoint.GET_CONTENT_BY_PRODUCT_IDS_ARRAY
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                List<ServerBean> list = null;
                                try {
                                    list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerBean>>() {
                                    }.getType());
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                                if (list != null && list.size() > 0) {
                                    new DataInsert(list, false).execute();
                                    onCustomResponse(true);
                                } else
                                    onCustomResponse(false);
                            } else
                                onCustomResponse(false);

                            isNetWorkCall = false;
                        }
                    });
        }
    }

    private void loadMoreData() {
        if (!SupportUtil.isNotConnected(activity)) {
            if (!isNetWorkCall && viewLoadMore != null && viewLoadMore.getVisibility() == View.GONE && canLoadMore) {
                showView(viewLoadMore);
//                new fetchIds( true ).execute();
                manageMoreData();
            }
        } else {
            hideView(viewLoadMore);
        }
    }

    private void manageMoreData() {
        if (!isNetWorkCall) {
            if (ids != null && ids.length > 0 && isMoreIds) {
                fetchArticleForId(ids);
            } else {
                isMoreIds = false;
                fetchLatestData(false);
            }
        } else {
            hideView(viewLoadMore);
        }
    }

    private void hideView(View view){
        if  ( view != null ) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view){
        if  ( view != null ) {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCustomResponse(boolean result) {
        hideView(viewLoadMore);
        if (!isMoreIds)
            canLoadMore = result;
    }

    @Override
    public void onNextLoad() {
        manageMoreData();
    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            if ( dbHelper != null ) {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.fetchHomeData(homeBeen, query, catId , isTypePdf);
                        if (!(isNotFavTab && isFirstHit && !isNotification))
                            getMoreIds();
                        return null;
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isNotFavTab && isFirstHit && !isNotification)
                fetchLatestData(true);
            handleData();
        }
    }

    public void callIdFunc() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new fetchIds(false).execute();
            }
        }, 0);
    }

    class DataInsert extends AsyncTask<Void, Void, Void> {

        private List<ServerBean> idBeen;
        private boolean isDelete;

        DataInsert(List<ServerBean> idBeen, boolean isDelete) {
            this.idBeen = idBeen;
            this.isDelete = isDelete;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.insertArticle(idBeen, catId, isTypePdf, isDelete);
                    dbHelper.fetchHomeData(homeBeen, query, catId , isTypePdf);
                    getMoreIds();
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            handleData();
        }
    }

    private void getMoreIds() {
        if (isMoreIds) {
            ids = dbHelper.fetchLatestEmptyIds(query, catId, activity);
            if ( mAdapter != null ) {
                if (ids != null && ids.length > 0)
                    mAdapter.setMaxId(Integer.parseInt(ids[0]));
                else
                    mAdapter.setMaxId(0);
            }
        }
    }

    private void handleData() {
        if (homeBeen.size() > 0 && llNoData != null && mRecyclerView != null && mAdapter != null ) {
            llNoData.setVisibility(View.GONE);
            mRecyclerView.getRecycledViewPool().clear();
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else if (!isFirstHit || !isNotFavTab) {
            showNoDataViews();
        }
    }

    private void showNoDataViews(){
        showView(llNoData);
        showView(tvNoData);
        hideView(pbLoader);
    }


    private int getCatId() {
        return subCatID == 0 ? catId : subCatID;
    }

    public String toString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.append(']').toString();
            b.append(",");
        }
    }

    class fetchIds extends AsyncTask<Void, Void, Void> {
        private boolean isNetWorkUpdate;

        public fetchIds(boolean isNetWorkUpdate) {
            this.isNetWorkUpdate = isNetWorkUpdate;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (isMoreIds) {
                final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        ids = dbHelper.fetchLatestEmptyIds(query, catId, activity);
                        if ( mAdapter != null ) {
                            if (ids != null && ids.length > 0)
                                mAdapter.setMaxId(Integer.parseInt(ids[0]));
                            else
                                mAdapter.setMaxId(0);
                        }
                        return null;
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isNetWorkUpdate) {
                if (ids != null && ids.length > 0 && isMoreIds) {
                    fetchArticleForId(ids);
                } else {
                    isMoreIds = false;
                    fetchLatestData(false);
                }
            } else {
                hideView(viewLoadMore);
            }
        }
    }
}
