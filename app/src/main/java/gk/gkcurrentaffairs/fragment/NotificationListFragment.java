package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.HomeActivity;
import gk.gkcurrentaffairs.adapter.NotificationListAdapter;
import gk.gkcurrentaffairs.bean.NotificationListBean;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/29/2018.
 */

public class NotificationListFragment extends Fragment implements NotificationListAdapter.OnCustomClick{

    private DbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private View view , llNoData ;
    private Activity activity ;
    private ArrayList<NotificationListBean> notificationListBeans = new ArrayList<>() ;
    NotificationListAdapter mAdapter ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity() ;
        initView(view);
        initData();
        return view ;
    }

    private void initData(){
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    private void initView( View view ){
        mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager( new LinearLayoutManager( activity ){
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                try {
                    super.onLayoutChildren(recycler, state);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } );
        SwipeRefreshLayout swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setEnabled(false);
        llNoData = view.findViewById(R.id.ll_no_data);
        mAdapter = new NotificationListAdapter(notificationListBeans , this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        new DataFromDB().execute();
    }

    @Override
    public void onCustomClick(final int position, boolean isDelete) {
        if ( isDelete ){
            deleteNotification(notificationListBeans.get(position).getId() );
            notificationListBeans.remove( position );
            mAdapter.notifyDataSetChanged();
            handleList();
         }else {
            if ( SupportUtil.isConnected(activity) ) {
                Intent intent = new Intent();
                try {
                    NotificationListBean bean = notificationListBeans.get(position) ;
                    SupportUtil.updateNotificationIntent( new JSONObject(bean.getData()) , intent );
                    HomeActivity.getInstance().initBundle(intent.getExtras());
                    if ( !bean.isClicked() ){
                        markReadNotification(bean.getTitle() , bean.getNotificationId());
                        notificationListBeans.get(position).setClicked(true);
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                SupportUtil.showToastInternet(activity);
            }
        }
    }

    private void markReadNotification(String title , int id){
        dbHelper.updateNotificationRead(title , id);
    }

    private void deleteNotification(final int id){
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.deleteNotification( id );
                return null;
            }
        });

    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.fetchNotificationList( notificationListBeans );
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            handleList();
        }
    }

    private void handleList(){
        if ( notificationListBeans.size() > 0) {
            llNoData.setVisibility(View.GONE);
            mAdapter.notifyDataSetChanged();
        } else { 
            llNoData.setVisibility(View.VISIBLE);
            ((TextView)view.findViewById(R.id.tv_no_data)).setText("No Data");
            view.findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
            view.findViewById(R.id.player_progressbar).setVisibility(View.GONE);
        }
    }
}
