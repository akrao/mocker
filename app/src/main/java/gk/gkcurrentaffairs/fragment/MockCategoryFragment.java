package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.login.LoginSdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.MCQActivityWithoutSwipe;
import gk.gkcurrentaffairs.activity.MockTestInstructionActivity;
import gk.gkcurrentaffairs.adapter.MockHomeAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.MockBean;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.ResultDataBean;
import gk.gkcurrentaffairs.bean.TestRankBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 1/12/2017.
 */

public class MockCategoryFragment extends Fragment implements MockHomeAdapter.OnLoadMore, MockHomeAdapter.OnClick,
        SwipeRefreshLayout.OnRefreshListener {

    private DbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private MockHomeAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<MockHomeBean> homeBeen = new ArrayList<>();
    private View llNoData;
    private View viewLoadMore;
    private boolean canLoadMore = true, isNotAttemptedTab = true, isFirstHit = true, isTimer;
    private View view;
    private Activity activity;
    private int catId = 0, subCatID, image;
    private String query = "", imageUrl;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog;
    private boolean isServerOne;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.layout_article_list, container, false);
        activity = getActivity();
        initDataFromArg();
        initObjects();
        initViews();
        initCategoryNames();
        setupList();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new DataFromDB(null).execute();
    }

    private void initObjects() {
        dbHelper = AppApplication.getInstance().getDBObject();
    }

    private CategoryProperty categoryProperty;

    private void initDataFromArg() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryProperty = (CategoryProperty) bundle.getSerializable(AppConstant.CAT_DATA);
            subCatID = bundle.getInt(AppConstant.DATA, 0);
            catId = bundle.getInt(AppConstant.CAT_ID);
            image = bundle.getInt(AppConstant.IMAGE);
            imageUrl = bundle.getString(AppConstant.IMAGE_URL);
            isTimer = bundle.getBoolean(AppConstant.CLICK_ITEM_ARTICLE, false);
            if (catId != 0)
                query = bundle.getString(AppConstant.QUERY);
            if (query == null)
                query = "";
            isNotAttemptedTab = !query.contains(dbHelper.COLUMN_ATTEMPTED);
            isServerOne = SupportUtil.isServerOne(catId);
        }
    }

    HashMap<Integer, String> hashMapCategoryNames = null;

    private void initCategoryNames() {
        if (dbHelper != null) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    ArrayList<CategoryBean> categoryBeen = dbHelper.MockFetchCategoryData(DbHelper.COLUMN_CAT_ID + "=" + catId, null, image);
                    if (categoryBeen != null && categoryBeen.size() > 0) {
                        hashMapCategoryNames = new HashMap<>(categoryBeen.size());
                        for (CategoryBean beanData : categoryBeen) {
                            hashMapCategoryNames.put(beanData.getCategoryId(), beanData.getCategoryName());
                        }
                    }
                    return null;
                }
            });
        }
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mLayoutManager = new LinearLayoutManager(activity) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                try {
                    super.onLayoutChildren(recycler, state);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean supportsPredictiveItemAnimations() {
//                return super.supportsPredictiveItemAnimations();
                return false;
            }
        };
        mRecyclerView.setLayoutManager(mLayoutManager);
        swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        llNoData = view.findViewById(R.id.ll_no_data);
        viewLoadMore = view.findViewById(R.id.ll_load_more);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Downloading...");
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoader = view.findViewById(R.id.player_progressbar);
    }

    private void setupList() {
        mAdapter = new MockHomeAdapter(homeBeen, this, isNotAttemptedTab ? this : null, activity, !isServerOne, hashMapCategoryNames);
        mAdapter.setImageRes(image);
        mAdapter.setImageUrl(imageUrl);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCustomItemClick(int position, int type) {
        if (categoryProperty != null && homeBeen != null
                && homeBeen.size() > position) {
            homeBeen.get(position).setSeeAnswer(categoryProperty.isSeeAnswer());
            if (type == MockHomeAdapter.TYPE_MCQ_OPEN) {
                if (homeBeen.get(position).isAttempted()) {
                    onCustomItemClick(position, false);
                } else {
                    if (SupportUtil.isConnected(activity)) {
                        openInstruction(position);
                    } else {
                        SupportUtil.showToastInternet(activity);
                    }
                }
            } else if (type == MockHomeAdapter.TYPE_MCQ_SOLUTION) {
                openSolution(position);
            } else if (type == MockHomeAdapter.TYPE_MCQ_INSTRUCTION) {
                openInstruction(position);
            } else if (type == MockHomeAdapter.TYPE_MCQ_RANK) {
                onCustomItemClick(position, true);
            }
        }

    }

    private void openMcqPage(final int position) {
        Intent intent = new Intent(activity, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, homeBeen.get(position));
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.TITLE, homeBeen.get(position).getTitle());
        intent.putExtra(AppConstant.DATA, homeBeen.get(position).getId());
        intent.putExtra(AppConstant.QUERY, getQuery(position));
        if (isTimer) {
            intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, 0);
        }
        activity.startActivity(intent);
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.upDateMockDb(dbHelper.COLUMN_ATTEMPTED, homeBeen.get(position).getId(), catId);
                dbHelper.upDateMockDb(dbHelper.COLUMN_DOWNLOADED, homeBeen.get(position).getId(), catId);
                return null;
            }
        });
    }

    public void onCustomItemClick(int position, boolean isDownloadOnly) {
        if (!isDownloadOnly) {
            if (homeBeen.get(position).getDownload() == DbHelper.INT_TRUE) {
                if (!isDownloadOnly)
                    openMcqPage(position);
            } else {
                if (isDownloadOnly) {
                    homeBeen.get(position).setDownload(2);
                    mAdapter.notifyDataSetChanged();
                } else {
                    //                try {
                    //                    progressDialog.show();
                    //                } catch (Exception e) {
                    //                }
                }
                if (SupportUtil.isNotConnected(activity))
                    SupportUtil.showToastInternet(activity);
                else
                    downloadMocTest(position, isDownloadOnly);
            }
        } else {
            checkRanking(homeBeen.get(position).getTitle());
            fetchRankFromServer(homeBeen.get(position).getId());
        }
    }


    private void openInstruction(int position) {
        Intent intent = new Intent(activity, MockTestInstructionActivity.class);
        intent.putExtra(AppConstant.CAT_DATA, categoryProperty);
        intent.putExtra(AppConstant.DATA, homeBeen.get(position));
        intent.putExtra(AppConstant.CAT_ID, catId);
        activity.startActivity(intent);
    }

    private void openSolution(int position) {
        Intent intent = new Intent(activity, MCQActivityWithoutSwipe.class);
        intent.putExtra(AppConstant.MOCK_DATA, homeBeen.get(position));
        intent.putExtra(AppConstant.SOLUTION, true);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.TITLE, homeBeen.get(position).getTitle());
        intent.putExtra(AppConstant.DATA, homeBeen.get(position).getId());
        intent.putExtra(AppConstant.QUERY, getQuery(position));
        intent.putExtra(AppConstant.RESULT_DATA, getResultDataBean(position));
        activity.startActivity(intent);
    }

    private ResultDataBean getResultDataBean(int position) {
        ResultDataBean resultDataBean = null;
        return resultDataBean;
    }

    private TestRankBean testRankBean = null;
    private Dialog dialog;

    private void checkRanking(String titleStr) {
        try {
            dialog = new Dialog(activity, R.style.MyDialog);
            dialog.setContentView(R.layout.dlg_test_ranking);
            TextView title = (TextView) dialog.findViewById(R.id.dlg_tv_title);
            title.setText(titleStr);
            (dialog.findViewById(R.id.dlg_tv_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
        }
    }

    private void updateRanking() {
        if (dialog != null) {
            dialog.findViewById(R.id.dlg_progress).setVisibility(View.GONE);
            TextView rankText = (TextView) dialog.findViewById(R.id.dlg_tv_rank_text);
            rankText.setText("Your Test Rank is :");
            TextView rank = (TextView) dialog.findViewById(R.id.dlg_tv_rank);
            rank.setText(getRank());
            rank.setVisibility(View.VISIBLE);
        }
        testRankBean = null;
    }

    private void fetchRankFromServer(int testID) {
        if (AppApplication.getInstance().getLoginSdk() != null) {
            Map<String, String> map = new HashMap<>(4);
            map.put("cat_id", catId + "");
            map.put("test_id", testID + "");
            map.put("user_id", LoginSdk.getInstance().getUserId() + "");
            map.put("application_id", activity.getPackageName());

            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_LEADER_BOARD
                    , ApiEndPoint.GET_USER_RANK
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    TestRankBean testRankBean1 = ConfigManager.getGson().fromJson(data, TestRankBean.class);
                                    if (testRankBean1 != null) {
                                        testRankBean = testRankBean1;
                                        if (dialog != null && dialog.isShowing()) {
                                            updateRanking();
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        }

    }

    private String getRank() {
        return String.format("%d/%d", testRankBean.getUserTestRank(), testRankBean.getTotalCount());
    }

    private String getQuery(int position) {
        return DbHelper.COLUMN_CAT_ID + "=" + catId + " AND " + DbHelper.COLUMN_MOCK_TEST_ID + "=" + homeBeen.get(position).getId();
    }

    @Override
    public void onCustomLoadMore() {
        if (SupportUtil.isConnected(activity)) {
            loadMoreData();
        }
    }

    @Override
    public void onRefresh() {
        if (isNotAttemptedTab)
            fetchMocTests(AppConstant.MAX_VALUE);
        else if ( swipeRefreshLayout != null )
            swipeRefreshLayout.setRefreshing(false);
    }

    private void loadMoreData() {
        if (viewLoadMore != null && viewLoadMore.getVisibility() == View.GONE && canLoadMore) {
            fetchMocTests(homeBeen.get(homeBeen.size() - 1).getId());
            showView(viewLoadMore);
        }
    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        private List<MockBean> list;

        public DataFromDB(List<MockBean> list) {
            this.list = list;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if (list != null && list.size() > 0) {
                        dbHelper.insertMockTestList(list, catId);
                    }
                    dbHelper.getMockTestList(homeBeen, query);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideView(viewLoadMore);

            if (isFirstHit)
                fetchMocTests(AppConstant.MAX_VALUE);

            if (homeBeen.size() > 0 && llNoData != null && mAdapter != null) {
                hideView(llNoData);
                mAdapter.notifyDataSetChanged();
            } else if (!isFirstHit || !isNotAttemptedTab) {
                showNoDataViews();
            }
        }
    }

    private void showNoDataViews() {
        showView(llNoData);
        showView(tvNoData);
        hideView(pbLoader);
    }

    private void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void fetchMocTests(long maxId) {
        Map<String, String> map = new HashMap<>(2);
        map.put("id", getCatId() + "");
        map.put("max_content_id", maxId + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, getApiHost()
                , ApiEndPoint.GET_UNIQUE_TITLE_BY_CAT_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        isFirstHit = false;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                List<MockBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    insertMockList(list);
                                } else {
                                    showNoData();
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                            isFirstHit = false;
                        } else {
                            showNoData();
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    private View tvNoData, pbLoader;

    private void showNoData() {
        hideView(viewLoadMore);
        canLoadMore = false;
        if (isFirstHit && homeBeen.size() < 1) {
            showNoDataViews();
        }
    }

    private void insertMockList(final List<MockBean> list) {
        new DataFromDB(list).execute();
    }

    private String getApiHost() {
        if (categoryProperty != null) {
            return categoryProperty.getHost();
        } else {
            return ConfigConstant.HOST_MAIN;
        }
    }

    private void downloadMocTest(final int position, final boolean isDownloadOnly) {
        SupportUtil.downloadNormalMockTest(homeBeen.get(position).getId(), categoryProperty.getId(), subCatID, activity
                , categoryProperty.getHost(), new SupportUtil.DownloadNormalMCQ() {
                    @Override
                    public void onResult(boolean result) {
                        isFirstHit = false;
                        if (result) {
                            homeBeen.get(position).setDownload(DbHelper.INT_TRUE);
                            openMcqPage(position);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    private int getCatId() {
        return subCatID == 0 ? catId : subCatID;
    }

}
