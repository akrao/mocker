package gk.gkcurrentaffairs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.adssdk.AdsSDK;
import com.adssdk.util.AdsCallBack;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.ImpCategoryActivity;
import gk.gkcurrentaffairs.adapter.ImpCatListAdapter;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.ImpCatListBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.fragment.BaseFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_ARTICLE;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_CALENDER;

/**
 * Created by Amit on 8/28/2018.
 */

public class GkHomeFragment extends BaseFragment implements View.OnClickListener , AdsCallBack {

    private View view;
    private Activity activity;
    private DbHelper dbHelper;
    private boolean isFirst = true;
    private int catId;
    private String query;
    private ImpCatListAdapter adapter;
    private List<ImpCatListBean> list = new ArrayList<>();
    private RelativeLayout rlNativeAd ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_home_gk, container, false);
        activity = getActivity();
        return view;
    }

    private boolean isLoaded = false;
    private boolean isNativeCallBack = false ;

    @Override
    public void onRefreshFragment() {
        if (!isLoaded) {
            init();
            isLoaded = true;
        }
    }

    private void init() {
        if (view != null) {
            initObjects();
            initViews();
            new DataFromDb(null, false).execute();
            loadNativeAds();
        }
    }

    private void loadNativeAds(){
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null ) {
            if (rlNativeAd != null && AdsSDK.getInstance().getAdsNative().isNativeAdCache() ) {
                SupportUtil.loadNativeAds(rlNativeAd , R.layout.ads_native_unified_card , true);
            }else if ( !isNativeCallBack ){
                AdsSDK.getInstance().setAdsCallBack(this.getClass().getName() , this);
            }
        }
    }

    private void initViews() {
        rlNativeAd = (RelativeLayout) view.findViewById(R.id.rl_native_ad_1);
        view.findViewById(R.id.tv_view_list).setOnClickListener(this);
        view.findViewById(R.id.rl_1).setOnClickListener(this);
        view.findViewById(R.id.rl_2).setOnClickListener(this);
        view.findViewById(R.id.rl_3).setOnClickListener(this);
    }

    private void initObjects() {
        catId = 731;
        query = DbHelper.COLUMN_CAT_ID + "=" + catId;
        dbHelper = AppApplication.getInstance().getDBObject();
        updateCategoryList( titles , ids , images , classType , isSubCat , isWebView , false );
    }


    private void updateCategoryList(String[] homeTitleArray , int[] idArray , int[] imageArr
            , int[] typeArray , boolean[] isSubCatArray
            , boolean[] isWebViewArray , boolean seeAns ){
        CategoryProperty categoryProperty ;
        for ( int i = 0 ; i < 3 ; i++ ){
            categoryProperty = new CategoryProperty();
            categoryProperty.setTitle( homeTitleArray[i] );
            categoryProperty.setPosition( i );
            categoryProperty.setDate( true );
            categoryProperty.setId( idArray[i] );
            categoryProperty.setImageResId( imageArr[i] );
            categoryProperty.setType( typeArray[i] );
            categoryProperty.setSubCat( isSubCatArray[i] );
            categoryProperty.setWebView( isWebViewArray[i] );
            categoryProperty.setHost(ConfigConstant.HOST_MAIN);
            categoryProperty.setSeeAnswer(seeAns);
            categoryProperties.add(categoryProperty);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_view_list:
                openImpCatActivity("Important Updates", false);
                break;
            case R.id.rl_1:
                openPage(0);
                break;
            case R.id.rl_2:
                openPage(1);
                break;
            case R.id.rl_3:
                openPage(2);
                break;
        }
    }

    private ArrayList<CategoryProperty> categoryProperties = new ArrayList<>(3);

    private void openPage(int position) {
        if (SupportUtil.isNotConnected(activity)) {
            SupportUtil.showToastInternet(activity);
        }
        Intent intent = new Intent(activity, SupportUtil.getCatClass(categoryProperties.get(position).getType()));
        intent.putExtra(AppConstant.CAT_DATA, categoryProperties.get(position));
        activity.startActivity(intent);

    }

    @Override
    public void onNativeAdsCache() {
        if ( !isNativeCallBack ) {
            isNativeCallBack = true;
            if ( AdsSDK.getInstance() != null ) {
                AdsSDK.getInstance().removeAdsCallBack(this.getClass().getName());
            }
            loadNativeAds();
        }
    }

    class DataFromDb extends AsyncTask<Void, Void, Void> {

        private List<ServerBean> listBeen;
        private boolean isLatest;

        public DataFromDb(List<ServerBean> listBeen, boolean isLatest) {
            this.listBeen = listBeen;
            this.isLatest = isLatest;
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if (listBeen != null && listBeen.size() > 0) {
                        dbHelper.insertArticle(listBeen, catId, false, isLatest);
                    }
                    dbHelper.fetchImpCatData(list, query, catId, false);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (list != null && list.size() > 0) {
                showData();
            }
            if (isFirst )
                fetchData(true);
        }
    }

    private void showData() {
        if (adapter == null) {
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
            recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            adapter = new ImpCatListAdapter(list, null, null);
            adapter.setWidth(SupportUtil.getScreenWidth(activity) - 100);
            adapter.setCatId(catId);
            adapter.setActivity(activity);
            adapter.setQuery(query);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void fetchData(final boolean isLatest) {
        Map<String, String> map = new HashMap<>(2);
        map.put("id", catId + "");
        map.put("max_content_id", (isLatest ? AppConstant.MAX_VALUE : list.get(list.size() - 1).getId()) + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_PREVIOUS_CONTENT_BY_CAT_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        isFirst = false;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            List<ServerBean> list = null;
                            try {
                                list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerBean>>() {
                                }.getType());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }

                            if (list != null && list.size() > 0) {
                                new DataFromDb(list, isLatest).execute();
                            } else if (!(list != null && list.size() > 0)) {
                            }
                        } else if (!(list != null && list.size() > 0)) {
                        }
                    }
                });
    }

    private void openImpCatActivity(String s, boolean type) {
        Intent intent = new Intent(activity, ImpCategoryActivity.class);
        intent.putExtra(AppConstant.CAT_ID, catId);
        intent.putExtra(AppConstant.TITLE, s);
        intent.putExtra(AppConstant.TYPE, type);
        activity.startActivity(intent);
    }

    private int[] images = {
            R.drawable.ic_ca_articles ,
            R.drawable.ca_quiz,
            R.drawable.ic_govt_jobs
    };

    private int[] ids = { 80,95,96 };

    private String[] titles = {"Current Affairs Article" , "Current Affairs Quiz" , "Government Jobs"};

    private int[] classType = {CATEGORY_TYPE_ARTICLE , CATEGORY_TYPE_CALENDER , CATEGORY_TYPE_ARTICLE };

    private boolean[] isSubCat = { true , false , true };
    private boolean[] isWebView = { false , true , true };
}
