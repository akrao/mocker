package gk.gkcurrentaffairs.onesignal;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.Logger;


/**
 * Created by joginder on 1/7/17.
 */

public class ResultNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(final OSNotification notification) {
        final JSONObject data = notification.payload.additionalData;
        if (data != null) {
            OneSignal.enableSound( data.optInt(AppConstant.SOUND, 0) == 1 );
            final DbHelper dbHelper = DbHelper.getInstance(AppApplication.getInstance());
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.insertNotification( notification.androidNotificationId , notification.payload.title , data.toString() );
                    return null;
                }
            });
        }
    }
}
