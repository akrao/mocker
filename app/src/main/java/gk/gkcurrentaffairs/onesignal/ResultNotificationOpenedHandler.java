package gk.gkcurrentaffairs.onesignal;

import android.content.Intent;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.activity.HomeActivity;
import gk.gkcurrentaffairs.activity.SplashActivity;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by joginder on 1/7/17.
 */

public class ResultNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(final OSNotificationOpenResult result) {
        JSONObject data = result.notification.payload.additionalData;
        if (data != null) {
            Intent intent = new Intent(AppApplication.getInstance(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);

            SupportUtil.updateNotificationIntent(data , intent);
            if (HomeActivity.getInstance() != null && HomeActivity.active ) {
                HomeActivity.getInstance().initBundle(intent.getExtras());
            }else {
                AppApplication.getInstance().startActivity(intent);
            }
            final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateNotificationRead(result.notification.payload.title , result.notification.androidNotificationId );
                    return null;
                }
            });
        }
    }
}