package gk.gkcurrentaffairs.bean;

import com.adssdk.BaseAdModelClass;

/**
 * Created by Amit on 1/30/2017.
 */

public class HomeBean extends BaseAdModelClass{

    private int id;

    private String date ;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getReadDetails() {
        return readDetails;
    }

    public void setReadDetails(String readDetails) {
        this.readDetails = readDetails;
    }

    public String getApplyOnline() {
        return applyOnline;
    }

    public void setApplyOnline(String applyOnline) {
        this.applyOnline = applyOnline;
    }

    private int subCatId ;
    private boolean isRead , isFav ;
    private String title , desc , readDetails , applyOnline ;
}
