package gk.gkcurrentaffairs.bean;

import java.io.Serializable;

/**
 * Created by Amit on 1/27/2018.
 */

public class CategoryProperty implements Serializable{

    private String title , imageUrl , host ;
    private int id , imageResId , type , position , subCatId ;
    private boolean isDate , isSubCat , isWebView , seeAnswer ;
    public CategoryProperty(boolean isDate) {
        this.isDate = isDate;
    }
    public CategoryProperty() {}

    public boolean isSeeAnswer() {
        return seeAnswer;
    }

    public void setSeeAnswer(boolean seeAnswer) {
        this.seeAnswer = seeAnswer;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isWebView() {
        return isWebView;
    }

    public void setWebView(boolean webView) {
        isWebView = webView;
    }

    public boolean isDate() {
        return isDate;
    }

    public void setDate(boolean date) {
        isDate = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSubCat() {
        return isSubCat;
    }

    public void setSubCat(boolean subCat) {
        isSubCat = subCat;
    }
}
