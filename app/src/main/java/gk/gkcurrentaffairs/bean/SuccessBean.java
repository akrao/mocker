package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 3/8/2017.
 */

public class SuccessBean {
    @SerializedName("reporting_status")
    @Expose
    private String isinsert;

    public String getIsinsert() {
        return isinsert;
    }

    public void setIsinsert(String isinsert) {
        this.isinsert = isinsert;
    }

}
