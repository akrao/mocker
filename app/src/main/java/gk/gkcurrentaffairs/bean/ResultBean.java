package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 3/3/2017.
 */

public class ResultBean {

    private String title , timeTaken , date , data ;
    private int id , correctAns , wrongAns , numberQue , catID , skipQue , mockTestId ;
    private boolean isCompleted ;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getMockTestId() {
        return mockTestId;
    }

    public void setMockTestId(int mockTestId) {
        this.mockTestId = mockTestId;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(int correctAns) {
        this.correctAns = correctAns;
    }

    public int getWrongAns() {
        return wrongAns;
    }

    public void setWrongAns(int wrongAns) {
        this.wrongAns = wrongAns;
    }

    public int getNumberQue() {
        return numberQue;
    }

    public void setNumberQue(int numberQue) {
        this.numberQue = numberQue;
    }

    public int getCatID() {
        return catID;
    }

    public void setCatID(int catID) {
        this.catID = catID;
    }

    public int getSkipQue() {
        return skipQue;
    }

    public void setSkipQue(int skipQue) {
        this.skipQue = skipQue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
