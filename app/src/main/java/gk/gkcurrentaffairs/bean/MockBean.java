package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 2/9/2017.
 */

public class MockBean {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("test_time")
    @Expose
    private String testTime;
    @SerializedName("test_markes")
    @Expose
    private String testMarkes;
    @SerializedName("quest_marks")
    @Expose
    private String questMarks;
    @SerializedName("negetive_marking")
    @Expose
    private String negativeMarking;
    @SerializedName("instructions")
    @Expose
    private String instruction;

    public String getTestTime() {
        return testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public String getTestMarkes() {
        return testMarkes;
    }

    public void setTestMarkes(String testMarkes) {
        this.testMarkes = testMarkes;
    }

    public String getQuestMarks() {
        return questMarks;
    }

    public void setQuestMarks(String questMarks) {
        this.questMarks = questMarks;
    }

    public String getNegativeMarking() {
        return negativeMarking;
    }

    public void setNegativeMarking(String negativeMarking) {
        this.negativeMarking = negativeMarking;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}
