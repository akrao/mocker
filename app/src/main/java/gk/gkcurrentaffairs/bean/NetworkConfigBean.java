package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 3/19/2017.
 */

public class NetworkConfigBean {
    @SerializedName("view")
    @Expose
    private String view;
    @SerializedName("download")
    @Expose
    private String download;
    @SerializedName("host")
    @Expose
    private String host;
    @SerializedName("host_v3")
    @Expose
    private String hostContent;
    @SerializedName("translator")
    @Expose
    private String translator;
    @SerializedName("app-version")
    @Expose
    private int versionCode;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getHostContent() {
        return hostContent;
    }

    public void setHostContent(String hostContent) {
        this.hostContent = hostContent;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

}