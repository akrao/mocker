package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 2/8/2018.
 */

public class CatCount {
    @SerializedName("count")
    @Expose
    private int count ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
