package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 2/10/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MockTestBean {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("parent_id")
    @Expose
    private String parent_id;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("small_description")
    @Expose
    private String small_description;
    @SerializedName("option_question")
    @Expose
    private String optionQuestion;
    @SerializedName("option_A")
    @Expose
    private String optionA;
    @SerializedName("option_B")
    @Expose
    private String optionB;
    @SerializedName("option_C")
    @Expose
    private String optionC;
    @SerializedName("option_D")
    @Expose
    private String optionD;
    @SerializedName("option_E")
    @Expose
    private String optionE;
    @SerializedName("option_ans")
    @Expose
    private String optionAns;

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOptionQuestion() {
        return optionQuestion;
    }

    public void setOptionQuestion(String optionQuestion) {
        this.optionQuestion = optionQuestion;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getOptionE() {
        return optionE;
    }

    public void setOptionE(String optionE) {
        this.optionE = optionE;
    }

    public String getOptionAns() {
        return optionAns;
    }

    public void setOptionAns(String optionAns) {
        this.optionAns = optionAns;
    }

    public String getSmall_description() {
        return small_description;
    }

    public void setSmall_description(String small_description) {
        this.small_description = small_description;
    }
}