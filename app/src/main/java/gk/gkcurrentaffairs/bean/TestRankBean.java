package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 1/22/2018.
 */

public class TestRankBean {

    @SerializedName("status")
    @Expose
    private String status ;
    @SerializedName("total_count")
    @Expose
    private int totalCount ;
    @SerializedName("user_test_rank")
    @Expose
    private int userTestRank ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getUserTestRank() {
        return userTestRank;
    }

    public void setUserTestRank(int userTestRank) {
        this.userTestRank = userTestRank;
    }
}
