package gk.gkcurrentaffairs.bean;

import com.adssdk.BaseAdModelClass;

import gk.gkcurrentaffairs.config.BaseModel;

/**
 * Created by Amit on 2/23/2017.
 */

public class ListBean extends BaseAdModelClass{

    private int subCatId , id ;
    private String text , date ;
    private boolean isTrue , isFav ;

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }


    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }
}
