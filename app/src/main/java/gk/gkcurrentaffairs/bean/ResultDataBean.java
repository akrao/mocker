package gk.gkcurrentaffairs.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Amit on 1/18/2018.
 */

public class ResultDataBean implements Serializable{

    private MockHomeBean mockHomeBean ;
    private String testTitle , query  ;
    private int mockTestId , correctAns , numberOfQue , wrongAns , catId , time ;
    private long timeTaken ;
    private ArrayList<Integer> dataAns;
    private ArrayList<String> dataSelect ;
    public ResultDataBean() {
    }

    public MockHomeBean getMockHomeBean() {
        return mockHomeBean;
    }

    public void setMockHomeBean(MockHomeBean mockHomeBean) {
        this.mockHomeBean = mockHomeBean;
    }

    public ArrayList<String> getDataSelect() {
        return dataSelect;
    }

    public void setDataSelect(ArrayList<String> dataSelect) {
        this.dataSelect = dataSelect;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTestTitle() {
        return testTitle;
    }

    public void setTestTitle(String testTitle) {
        this.testTitle = testTitle;
    }

    public int getMockTestId() {
        return mockTestId;
    }

    public void setMockTestId(int mockTestId) {
        this.mockTestId = mockTestId;
    }

    public int getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(int correctAns) {
        this.correctAns = correctAns;
    }

    public int getNumberOfQue() {
        return numberOfQue;
    }

    public void setNumberOfQue(int numberOfQue) {
        this.numberOfQue = numberOfQue;
    }

    public int getWrongAns() {
        return wrongAns;
    }

    public void setWrongAns(int wrongAns) {
        this.wrongAns = wrongAns;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public ArrayList<Integer> getDataAns() {
        return dataAns;
    }

    public void setDataAns(ArrayList<Integer> dataAns) {
        this.dataAns = dataAns;
    }
}
