package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 1/30/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerBean extends ServerBeanArticle {

    @SerializedName("pdf")
    @Expose
    private String pdf;
    @SerializedName("option_A")
    @Expose
    private String optionA;

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    @SerializedName("option_C")
    @Expose
    private String optionC;
    @SerializedName("option_B")
    @Expose
    private String optionB;
    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

}