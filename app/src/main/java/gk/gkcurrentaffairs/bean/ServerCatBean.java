package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 6/7/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServerCatBean implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("test_time")
    @Expose
    private String testTime;
    @SerializedName("test_markes")
    @Expose
    private String testMarkes;
    @SerializedName("instructions")
    @Expose
    private String instructions;
    @SerializedName("quest_marks")
    @Expose
    private String questMarks;
    @SerializedName("negetive_marking")
    @Expose
    private String negetiveMarking;
    @SerializedName("category_type")
    @Expose
    private Integer categoryType;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTestTime() {
        return testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public String getTestMarkes() {
        return testMarkes;
    }

    public void setTestMarkes(String testMarkes) {
        this.testMarkes = testMarkes;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getQuestMarks() {
        return questMarks;
    }

    public void setQuestMarks(String questMarks) {
        this.questMarks = questMarks;
    }

    public String getNegetiveMarking() {
        return negetiveMarking;
    }

    public void setNegetiveMarking(String negetiveMarking) {
        this.negetiveMarking = negetiveMarking;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

}