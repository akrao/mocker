package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 7/4/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocalMockData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("que")
    @Expose
    private String que;
    @SerializedName("opt_a")
    @Expose
    private String optA;
    @SerializedName("opt_b")
    @Expose
    private String optB;
    @SerializedName("opt_c")
    @Expose
    private String optC;
    @SerializedName("opt_d")
    @Expose
    private String optD;
    @SerializedName("ans")
    @Expose
    private String ans;
    @SerializedName("mock_test_id")
    @Expose
    private Integer mockTestId;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("sub_cat_id")
    @Expose
    private Integer subCatId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getQue() {
        return que;
    }

    public void setQue(String que) {
        this.que = que;
    }

    public String getOptA() {
        return optA;
    }

    public void setOptA(String optA) {
        this.optA = optA;
    }

    public String getOptB() {
        return optB;
    }

    public void setOptB(String optB) {
        this.optB = optB;
    }

    public String getOptC() {
        return optC;
    }

    public void setOptC(String optC) {
        this.optC = optC;
    }

    public String getOptD() {
        return optD;
    }

    public void setOptD(String optD) {
        this.optD = optD;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public Integer getMockTestId() {
        return mockTestId;
    }

    public void setMockTestId(Integer mockTestId) {
        this.mockTestId = mockTestId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(Integer subCatId) {
        this.subCatId = subCatId;
    }

}