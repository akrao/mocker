package gk.gkcurrentaffairs.bean;

import com.adssdk.BaseAdModelClass;

/**
 * Created by Amit on 9/29/2017.
 */

public class ImpCatListBean extends BaseAdModelClass{

    private int id ;
    private String title , desc , date ;
    private boolean isFav , isRead ;

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }
}
