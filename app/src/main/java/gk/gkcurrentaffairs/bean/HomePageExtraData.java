package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import gk.gkcurrentaffairs.payment.model.PaidSlidersBean;

/**
 * Created by Amit on 6/8/2018.
 */

public class HomePageExtraData {

    @SerializedName("category_image_path")
    @Expose
    private String categoryImagePath ;
    @SerializedName("image_path")
    @Expose
    private String imagePath ;
    @SerializedName("sliders")
    @Expose
    private List<PaidSlidersBean> slidersBeanList ;

    @SerializedName("important_update_ids")
    @Expose
    private List<ImpCatBean> impCatBeans ;

    @SerializedName("exam_target")
    @Expose
    private List<CatRankBean> catRankBeans ;

    public String getCategoryImagePath() {
        return categoryImagePath;
    }

    public void setCategoryImagePath(String categoryImagePath) {
        this.categoryImagePath = categoryImagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<PaidSlidersBean> getSlidersBeanList() {
        return slidersBeanList;
    }

    public void setSlidersBeanList(List<PaidSlidersBean> slidersBeanList) {
        this.slidersBeanList = slidersBeanList;
    }

    public List<ImpCatBean> getImpCatBeans() {
        return impCatBeans;
    }

    public void setImpCatBeans(List<ImpCatBean> impCatBeans) {
        this.impCatBeans = impCatBeans;
    }

    public List<CatRankBean> getCatRankBeans() {
        return catRankBeans;
    }

    public void setCatRankBeans(List<CatRankBean> catRankBeans) {
        this.catRankBeans = catRankBeans;
    }
}
