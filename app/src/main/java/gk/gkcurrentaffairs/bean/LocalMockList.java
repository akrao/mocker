package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 7/4/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocalMockList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sub_cat_id")
    @Expose
    private Integer subCatId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("is_downloaded")
    @Expose
    private Integer isDownloaded;
    @SerializedName("is_attempted")
    @Expose
    private Integer isAttempted;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(Integer subCatId) {
        this.subCatId = subCatId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(Integer isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    public Integer getIsAttempted() {
        return isAttempted;
    }

    public void setIsAttempted(Integer isAttempted) {
        this.isAttempted = isAttempted;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

}