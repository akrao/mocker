package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amit on 6/7/2018.
 */

public class ServerCatListBean extends ServerCatBean implements Serializable{

    @SerializedName("subcategories")
    @Expose
    private List<ServerCatBean> catBeans;

    public List<ServerCatBean> getCatBeans() {
        return catBeans;
    }

    public void setCatBeans(List<ServerCatBean> catBeans) {
        this.catBeans = catBeans;
    }
}
