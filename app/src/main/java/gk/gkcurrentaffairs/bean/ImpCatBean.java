package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 3/19/2018.
 */

public class ImpCatBean {
    @SerializedName("id")
    @Expose
    private int id ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
