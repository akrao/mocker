package gk.gkcurrentaffairs.bean;

import com.adssdk.BaseAdModelClass;

import java.io.Serializable;

/**
 * Created by Amit on 2/9/2017.
 */

public class MockHomeBean extends BaseAdModelClass implements Serializable {

    private String title , instruction;
    private int subCatId , id , download ;
    private boolean isAttempted , isSync , seeAnswer ;
    private int testTime;
    private double testMarkes;
    private double questMarks;
    private double negativeMarking;

    public boolean isSeeAnswer() {
        return seeAnswer;
    }

    public void setSeeAnswer(boolean seeAnswer) {
        this.seeAnswer = seeAnswer;
    }

    public int getTestTime() {
        return testTime;
    }

    public void setTestTime(int testTime) {
        this.testTime = testTime;
    }

    public double getTestMarkes() {
        return testMarkes;
    }

    public void setTestMarkes(double testMarkes) {
        this.testMarkes = testMarkes;
    }

    public double getQuestMarks() {
        return questMarks;
    }

    public void setQuestMarks(double questMarks) {
        this.questMarks = questMarks;
    }

    public double getNegativeMarking() {
        return negativeMarking;
    }

    public void setNegativeMarking(double negativeMarking) {
        this.negativeMarking = negativeMarking;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public int getDownload() {
        return download;
    }

    public void setDownload(int download) {
        this.download = download;
    }

    public boolean isAttempted() {
        return isAttempted;
    }

    public void setAttempted(boolean attempted) {
        isAttempted = attempted;
    }
}
