package gk.gkcurrentaffairs.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import gk.gkcurrentaffairs.adapter.UserLeaderBoardBean;

/**
 * Created by Amit on 1/24/2018.
 */

public class LeaderBoardDataBean {

    @SerializedName("total_count")
    @Expose
    private int total_count ;
    @SerializedName("rank")
    @Expose
    private int rank ;
    @SerializedName("top_three_users")
    @Expose
    private List<UserLeaderBoardBean> top_three_users ;
    @SerializedName("user_ranking")
    @Expose
    private UserLeaderBoardBean user_ranking ;
    @SerializedName("users_ranking")
    @Expose
    private List<UserLeaderBoardBean> topper ;
    @SerializedName("lower")
    @Expose
    private List<UserLeaderBoardBean> lower ;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public List<UserLeaderBoardBean> getTop_three_users() {
        return top_three_users;
    }

    public void setTop_three_users(List<UserLeaderBoardBean> top_three_users) {
        this.top_three_users = top_three_users;
    }

    public UserLeaderBoardBean getUser_ranking() {
        return user_ranking;
    }

    public void setUser_ranking(UserLeaderBoardBean user_ranking) {
        this.user_ranking = user_ranking;
    }

    public List<UserLeaderBoardBean> getTopper() {
        return topper;
    }

    public void setTopper(List<UserLeaderBoardBean> topper) {
        this.topper = topper;
    }

    public List<UserLeaderBoardBean> getLower() {
        return lower;
    }

    public void setLower(List<UserLeaderBoardBean> lower) {
        this.lower = lower;
    }
}
