package gk.gkcurrentaffairs.bean;

/**
 * Created by Amit on 2/23/2017.
 */

public class DescBean extends ListBean {

    private boolean isRead ;
    private String title ;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
