package gk.gkcurrentaffairs.bean;

import com.adssdk.BaseAdModelClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit on 3/6/17.
 */

public class SubjectModel extends BaseAdModelClass{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("pdf")
    @Expose
    private String pdf;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    private Boolean isDownloaded=false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean getisDownloaded() {
        return isDownloaded;
    }

    public void setisDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

}