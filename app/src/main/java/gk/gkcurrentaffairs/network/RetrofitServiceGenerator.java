package gk.gkcurrentaffairs.network;

import android.content.Context;

import java.io.IOException;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceGenerator {

    public static final String BASE_URL_BACK_UP = "http://jogindersharma.in";

    private static Retrofit retrofit = null;
    private static Retrofit retrofit_1 = null;
    private static Retrofit retrofit_2 = null;
    private static Retrofit retrofit_payment = null;


    public static Retrofit getClient( Context context , boolean refresh ) {
        if (retrofit == null || refresh ) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppPreferences.getBaseUrl( context ) )
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient().build())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getPaymentClient( Context context , boolean refresh ) {
        if (retrofit_payment == null || refresh ) {
            retrofit_payment = new Retrofit.Builder()
                    .baseUrl(AppPreferences.getHostPayment( context ) )
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient().build())
                    .build();
        }
        return retrofit_payment;
    }

    public static Retrofit getClient_2( Context context , boolean refresh  ) {
        if (retrofit_1 == null || refresh ) {
            retrofit_1 = new Retrofit.Builder()
                    .baseUrl(AppPreferences.getBaseUrl_2( context ) )
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient().build())
                    .build();
        }
        return retrofit_1;
    }
    public static Retrofit getClient_3( Context context , boolean refresh  ) {
        if (retrofit_2 == null || refresh ) {
            retrofit_2 = new Retrofit.Builder()
                    .baseUrl(AppPreferences.getBaseUrl_3( context ) )
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient().build())
                    .build();
        }
        return retrofit_2;
    }

    private static OkHttpClient.Builder getHttpClient() {
        if ( BuildConfig.DEBUG ) {
            return new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor( interceptor );
        } else {
            return new OkHttpClient.Builder()
                    .addInterceptor( interceptor );
        }
    }

    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    
    private static Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Authorization", SupportUtil.getSecurityCode(AppApplication.getInstance()))
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    };
}