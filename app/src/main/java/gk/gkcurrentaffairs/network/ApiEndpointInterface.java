package gk.gkcurrentaffairs.network;


import com.google.gson.JsonObject;
import java.util.List;

import gk.gkcurrentaffairs.bean.CatBean;
import gk.gkcurrentaffairs.bean.IdBean;
import gk.gkcurrentaffairs.bean.LocalMockData;
import gk.gkcurrentaffairs.bean.LocalMockList;
import gk.gkcurrentaffairs.bean.MockBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.bean.NetworkConfigBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.bean.SubjectModel;
import gk.gkcurrentaffairs.bean.SuccessBean;
import gk.gkcurrentaffairs.bean.TestRankBean;
import gk.gkcurrentaffairs.bean.TranslaterBean;
import gk.gkcurrentaffairs.payment.model.LoginSignUpResponse;
import gk.gkcurrentaffairs.payment.model.UserConfig;
import gk.gkcurrentaffairs.util.AppConstant;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Amit on 1/10/2017.
 */

public interface ApiEndpointInterface {

    /**
     * get Network Url Config.
     * @return
     */
    @GET
    Call<NetworkConfigBean> getNetworkConfig( @Url String url );

    /**
     * get Latest data for given Category ID.
     * @param id
     * @param max_content_id
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-previous-content-by-cat-id")
    Call<List<ServerBean>> getPreviousContentCatId(@Query("id") int id , @Query("max_content_id") long max_content_id );


    /**
     * get Latest ID for given Category ID.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-content-and-subcat-ids-by-cat-id")
    Call<List<IdBean>> getLatestIdByCatId(@Query("id") int id , @Query("max_content_id") long max_content_id );

    /**
     * get Articles by ID's.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-content-by-product-ids-array")
    Call<List<ServerBean>> getArticleForIds( @Query("id_array") String id );

    /**
     * get 200 Mock Test List base on cat and sub cat ID.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-unique-title-by-cat-id")
    Call<List<MockBean>> getLatestMockByCatId(@Query("id") int id , @Query("max_content_id") long max_content_id );

    /**
     * get 200 Mock Test List base on cat and sub cat ID.
     * @return
     */
    @GET("get-unique-title-by-cat-id")
    Call<List<MockBean>> getLatestMockByCatIdServer(@Query("id") int id , @Query("max_content_id") long max_content_id );

    /**
     * download mock test by title.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-mock-test-by-title")
    Call<List<MockTestBean>> downloadMockTestByTitle(@Query(value = "title" ) String title , @Query("id") int id );

    /**
     * download mock test by title.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-mock-test-by-min-id")
    Call<List<MockTestBean>> downloadMockTestById(@Query("id") int id , @Query("category_id") int category_id );

    /**
     * download mock test by title.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-latest-mock-test")
    Call<List<MockTestBean>> downloadLatestMockTestBycatId(@Query("id") int id );

    /**
     * download mock test by title.
     * @return
     */
    @GET("get-latest-mock-test")
    Call<List<MockTestBean>> downloadLatestMockTestBycatIdServer(@Query("id") int id );

    /**
     * server 2
     * download mock test by title.
     * @return
     */
    @GET("download-test-by-test-title")
    Call<List<MockTestBean>> downloadMockTestByTitle_Server(@Query("title") String title );

    /**
     * get Latest CAQ.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-content-for-date-by-sub-cat-id")
    Call<List<MockTestBean>> getDateData(@Query("id") int id, @Query("date") String date);

    /**
     * get Latest CAQ.
     * @return
     */
    @GET(AppConstant.VERSIONING +"get-content-by-id")
    Call<ServerBean> getArticle(@Query("id") String id);

    /**
     * bug report.
     * @return
     */
    @GET(AppConstant.VERSIONING +"send-error")
    Call<SuccessBean> submitBugReport(@Query("message") String bug, @Query("email_id") String email, @Query("question_id") int id , @Query("app_id") String appId);

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);

    @GET
    Call<TranslaterBean> customUrlTranslate(@Url String url);

    @GET(AppConstant.VERSIONING +"get-home-data")
    Call<List<CatBean>> getCatData(@Query("id") String id,@Query("parent_id") int parent_id);

    @GET
    Call<JsonObject> customUrlInvalidate(@Url String url);

    @GET("gk-mock-data")
    Call<List<LocalMockData>> localMockData(@Query("id") String id);

    @GET("gk-mock-list")
    Call<List<LocalMockList>> localMockList(@Query("id") String id);

    
    // Amit groch server 3 NCERT
    @GET("get-sub-cats-titles-with-ids")
    Call<List<SubjectModel>> getSubjectTittles(@Query("id") int id);

    @GET("get-ncrt-content")
    Call<List<SubjectModel>> getBooksPdfNames(@Query("id") int id,@Query("max_content_id") int max_content_id);

    // NCERT Ends


}
