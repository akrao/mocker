package gk.gkcurrentaffairs.network;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import gk.gkcurrentaffairs.bean.CatCount;
import gk.gkcurrentaffairs.bean.LeaderBoardDataBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.bean.TestRankBean;
import gk.gkcurrentaffairs.payment.adapter.PaidMockAdapter;
import gk.gkcurrentaffairs.payment.model.AdsPointsSlabConfig;
import gk.gkcurrentaffairs.payment.model.CatBean;
import gk.gkcurrentaffairs.payment.model.ExamBean;
import gk.gkcurrentaffairs.payment.model.HelpQuestionBean;
import gk.gkcurrentaffairs.payment.model.LoginSignUpResponse;
import gk.gkcurrentaffairs.payment.model.LoginUserResponse;
import gk.gkcurrentaffairs.payment.model.Package;
import gk.gkcurrentaffairs.payment.model.PackageInfo;
import gk.gkcurrentaffairs.payment.model.PackageList;
import gk.gkcurrentaffairs.payment.model.PaidHomeBean;
import gk.gkcurrentaffairs.payment.model.PaidMockResult;
import gk.gkcurrentaffairs.payment.model.PaymentResponse;
import gk.gkcurrentaffairs.payment.model.PaymentResult;
import gk.gkcurrentaffairs.payment.model.SectionData;
import gk.gkcurrentaffairs.payment.model.SectionResultBean;
import gk.gkcurrentaffairs.payment.model.ServerMockTestList;
import gk.gkcurrentaffairs.payment.model.ServerTestResultBean;
import gk.gkcurrentaffairs.payment.model.ServiceCost;
import gk.gkcurrentaffairs.payment.model.StatusBean;
import gk.gkcurrentaffairs.payment.model.TransactionHistoryBean;
import gk.gkcurrentaffairs.payment.model.UserConfig;
import gk.gkcurrentaffairs.util.AppConstant;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Amit on 8/5/2017.
 */

public interface ApiPayInterface {

    @GET
    Call<JSONObject> customUrlInvalidate(@Url String url);

    /**
     *
     * @param uid
     * @param deviceId
     * @return
     */
    // get user config
    @GET("v1/get-config")
    Call<UserConfig> getConfigEng(@Query("id") String uid, @Query("device_id") String deviceId , @Query("application_id") String pkg_id );

    // Get Service cost
    @GET("v1/service-cost")
    Call<ServiceCost> getServiceCost(@Query("user_id") String uid, @Query("id") int id , @Query("product_type") String product_type );


    @GET("gameresult/v1/get-user-leaderboard-rankingrank")
    Call<LeaderBoardDataBean> getLeaderBoardData(@Query("cat_id") int catId , @Query("is_last_month") int is_last_month
            , @Query("user_id") int userId , @Query("application_id") String pkg_id );

    /**
     * get Mock Test Rank
     */
    @POST("gameresult/v1/save-game-result")
    Call<TestRankBean> getMockTestRanking(@Query("all") int all , @Query("right") int right ,
                                          @Query("wrong") int wrong , @Query("time") long time ,
                                          @Query("cat_id") int cat_id , @Query("test_id") int test_id ,
                                          @Query("user_id") String user_id , @Query("application_id") String pkg_id , @Query("raw_data") String raw_data );

    /**
     * get Mock Test Rank
     */
    @GET("gameresult/v1/get-user-rank")
    Call<TestRankBean> getMockTestUserRanking(@Query("cat_id") int cat_id ,@Query("test_id") int test_id ,
                                              @Query("user_id") String user_id ,@Query("application_id") String pkg_id );

    // Ad Free slab -- ad free offers and user points
    // only for login users

    @GET("v1/ads-free-slab-config")
    Call<AdsPointsSlabConfig> getAdFreeSlab(@Query("uid") String uid );

    // Point slab -- ad free offers and user points
    // only for login users

    @GET("v1/points-plan")
    Call<AdsPointsSlabConfig> getPointsSlab(@Query("uid") String uid );

   // Point slab -- ad free offers and user points
    // only for login users

    @GET("v1/get-home-page-data")
    Call<PaidHomeBean> getPaidHomeData();

    @GET("api/v1/get-cat-contents-count")
    Call<CatCount> getCatCount(@Query("id") int id );


    @POST("v1/store-fcm-token")
    Call<String> syncToServer(
            @Query("device_id") String device_id,
            @Query("player_id") String player_id,
            @Query("apps") String apps ,
            @Query("application_id") String app_id

    );


   // List of my buy packages
    // only for login users

    @GET("v1/get-my-packages")
    Call<PackageList> getMyPackages(@Query("user_id") String userId );

   // List of my buy mock test
    // only for login users

    @GET("v1/get-my-mocktest")
    Call<List<ServerMockTestList>> getMyMockTests(@Query("user_id") String userId );

   // List of my trans history
    // only for login users
    @GET("v1/get-user-order-history")
    Call<List<TransactionHistoryBean>> getMyTransactionHistory(@Query("user_id") String userId , @Query("id") long id );

   // List of help and support cat
    @GET("v1/get-faq-categories")
    Call<List<CatBean>> getHelpSupportCat();

   // List of help and support cat
    @GET("v1/get-faq-by-category-id")
    Call<List<HelpQuestionBean>> getHelpSupportQueByCat(@Query("id") long id );

  // List of my buy mock test
    // only for login users

    @GET("v1/get-user-paid-categories")
    Call<List<ExamBean>> getMyExamList(@Query("user_id") String userId );

  // List of my buy mock test
    // only for login users

    @POST("v1/post-user-paid-categories")
    Call<StatusBean> getMyExamListSelected(@Query("user_id") String userId , @Query("data") String id );

    /*
     * id --- Package Id
     * Fetch package details
     */
    @GET("v1/get-package-details")
    Call<PackageInfo> getPackageDetails(@Query("id") int id );

    /*
     *  Download Mock Test
     *  id --- Mock Test Id
     *  user_id -- our server auto generated Id
     */
    @GET("v1/download-mock-test")
    Call<PaidMockResult> downloadMockTest(@Query("id") int id , @Query("user_id") String userId );

    /*
     *  Download Mock Test
     *  id --- Mock Test Id
     *  user_id -- our server auto generated Id
     */
    @POST("v1/save-user-test-result")
    Call<ServerTestResultBean> sendPaidTestResult(@Query("data") String data , @Query("user_id") String userId );

    // update user profile
    @Multipart
    @POST("v1/update-user-profile")
    Call<LoginUserResponse> updateUserProfile(
            @Query("email") String email,
            @Query("id") String auto_id,
            @Query("email_verified") int email_verified,
            @Part("name") RequestBody name,
            @Part MultipartBody.Part image,
//            @Query("photo_url") String photo_url,
            @Query("phone") String mobile,
            @Query("address") String address,
            @Query("state") int state,
            @Query("postal") String postal ,
            @Query("dob")           String dob,
            @Query("gender")        int gender ,
            @Query("device_id") String device_token ,
            @Query("player_id")     String player_id,
            @Query("about_me")      String about_me,
            @Query("application_id") String app_id
    );

    @POST("v1/login-signup")
    Call<LoginUserResponse> loginSignUp(
            @Query("email") String email,
            @Query("firebase_id") String uid,
            @Query("is_verified") int email_verified,
            @Query("name") String name,
            @Query("photo_url") String photo_url,
            @Query("provider_id") String provider_id,
            @Query("device_id") String device_token ,
            @Query("phone") String phone ,
            @Query("device_type") int device_type ,
            @Query("player_id") String player_id ,
            @Query("application_id") String app_id
//            ,
//            @Query("fcm_token") String fcm_token,
//            @Query("mobile") String mobile,
//            @Query("address") String address,
//            @Query("state") int state,
//            @Query("postal") String postal
    );

    @FormUrlEncoded
    @POST("top-coaching-payment")
    Call<PaymentResult> initWalletPaymentRequest(
            @Field("user_id") String user_id,
            @Field("device_id") String device_id,
            @Field("device_type") String device_type,
            @Field("email") String email,
            @Field("product_id") int serviceId,
            @Field("product_type") String serviceType,
            @Field("wallet_amount") int amount);


    @FormUrlEncoded
    @POST("init-payment")
    Call<PaymentResponse> initPaymentRequest(
            @Field("payment_brand_id") String payment_brand_id,
            @Field("user_id") String user_id,
            @Field("device_id") String device_id,
            @Field("device_type") String device_type,
            @Field("email") String email,
            @Field("amount") String amount);

    @FormUrlEncoded
    @POST("paytm-callback")
    Call<String> PaytmCallbackRequest(
            @Field("data") String data);


    @POST("init-payment" + AppConstant.TEST)
    Call<PaymentResponse> initPaymentRequestTest(
            @Query("payment_brand_id") String payment_brand_id,
            @Query("user_id") String user_id,
            @Query("device_id") String device_id,
            @Query("device_type") String device_type,
            @Query("email") String email,
            @Query("product_id") int serviceId,
            @Query("product_type") String serviceType,
            @Query("wallet_amount") int wallet_amount,
            @Query("amount") int amount);


    @FormUrlEncoded
    @POST("paytm-callback"+ AppConstant.TEST)
    Call<PaymentResult> PaytmCallbackRequestTest(
            @Field("data") String data);

}
