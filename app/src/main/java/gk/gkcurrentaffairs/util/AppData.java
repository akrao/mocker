package gk.gkcurrentaffairs.util;

import android.content.Context;
import android.util.SparseIntArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.config.ConfigConstant;

import static gk.gkcurrentaffairs.util.AppConstant.HOME_TITLE_ARRAY;

/**
 * Created by Amit on 1/11/2017.
 */
public class AppData {

    private SparseIntArray subCatList ;
    private HashMap<Integer , String> categoryNames ;
    private HashMap<Integer , String> mainCategoryNames ;
    private HashMap<Integer , String> categoryNamesEngMock ;
    private HashMap<Integer , int[]> imageRes ;
    private HashMap<Integer , Boolean> networkQue ;
    private Context context ;
    private HashMap<Integer , CategoryProperty> categoryPropertyHashMap ;

    private static AppData ourInstance ;

    public static AppData getInstance() {
        if ( ourInstance == null ) {
            ourInstance = new AppData(AppApplication.getInstance());
        }
        return ourInstance;
    }

    private AppData( Context context ) {
        this.context = context ;
        initData();
    }

    private void initData(){
        subCatList = new SparseIntArray();
        setHomePageData();
        refreshCatData();
        setImageRes();
        setNetworkQue();
        setMainCategoryNames();
        setCategoryPropertyHashMap();
    }

    private void setCategoryPropertyHashMap(){
        categoryPropertyHashMap = new HashMap<>();
        categoryPropertyHashMap.put( AppValues.CAA_ID , new CategoryProperty(true) );
        categoryPropertyHashMap.put( AppValues.GOVT_JOBS_ID , new CategoryProperty(true) );
        categoryPropertyHashMap.put( AppValues.IMP_NOTES_ID , new CategoryProperty(true) );
        categoryPropertyHashMap.put( AppValues.ID_EDITORIAL , new CategoryProperty(true) );
    }

    public void refreshCatData(){
        final DbHelper dbHelper = AppApplication.getInstance().getDBObject() ;
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                setCategoryNames( dbHelper.fetchCategoryData( subCatList , DbHelper.COLUMN_CAT_ID +"!=" + AppConstant.ENGLISH_MOCK_TEST_ID ) );
                setCategoryNamesEngMock( dbHelper.fetchCategoryData( subCatList , DbHelper.COLUMN_CAT_ID +"=" + AppConstant.ENGLISH_MOCK_TEST_ID ) );
                return null;
            }
        });
    }

    public HashMap<Integer, CategoryProperty> getCategoryPropertyHashMap() {
        return categoryPropertyHashMap;
    }

    public void setCategoryNamesEngMock(HashMap<Integer, String> categoryNamesEngMock) {
        this.categoryNamesEngMock = categoryNamesEngMock;
    }

    public HashMap<Integer, String> getCategoryNamesEngMock() {
        return categoryNamesEngMock;
    }

    public HashMap<Integer, String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(HashMap<Integer, String> categoryNames) {
        this.categoryNames = categoryNames;
    }

    public void setImageRes() {
        imageRes = new HashMap<>();
        imageRes.put( AppValues.CAA_ID , ImageRes.CAA_SUB_CAT );
        imageRes.put( AppValues.GOVT_JOBS_ID , ImageRes.GOVT_JOBS_SUB_CAT );
        imageRes.put( AppValues.IMP_NOTES_ID , ImageRes.IMP_NOTES_SUB_CAT );
        imageRes.put( AppValues.MOCK_ID , ImageRes.MOCK_TEST_SUB_CAT );
        imageRes.put( AppValues.PRE_ASK_QUE_ID , ImageRes.PRE_YEAR_SUB_CAT );
        imageRes.put( AppValues.NOTES_CAT_ID[0] , ImageRes.GK_NOTES_SUB_CAT );
        imageRes.put( AppValues.SSC_ID , ImageRes.SSC_CAT );
        imageRes.put( AppValues.BANK_ID , ImageRes.BANK_CAT );
        imageRes.put( AppValues.RRB_ID , ImageRes.RRB_CAT );
        imageRes.put( AppValues.PDF_SEC_ID , ImageRes.PDF_SEC_CAT );
    }

    public HashMap<Integer, int[]> getImageRes() {
        return imageRes;
    }

    public HashMap<Integer, Boolean> getNetworkQue() {
        return networkQue;
    }

    public void setNetworkQue() {
        networkQue = new HashMap<>();
        networkQue.put( AppValues.CAA_ID , false );
        networkQue.put( AppValues.GOVT_JOBS_ID , false );
        networkQue.put( AppConstant.EDITORIAL_ID , false );
        networkQue.put( AppValues.IMP_NOTES_ID , false );
        networkQue.put( AppValues.NOTES_CAT_ID[0] , false );
        networkQue.put( AppValues.NOTES_CAT_ID[1] , false );
        networkQue.put( AppValues.NOTES_CAT_ID[2] , false );
        networkQue.put( AppValues.NOTES_CAT_ID[3] , false );
    }

    public HashMap<Integer, String> getMainCategoryNames() {
        return mainCategoryNames;
    }

    public void setMainCategoryNames() {
        String[] cat = context.getResources().getStringArray( R.array.main_cat_name );
        int[] catId = context.getResources().getIntArray( R.array.main_cat_id );
        mainCategoryNames = new HashMap<>( cat.length );
        for ( int i = 0 ; i < cat.length ; i++ ){
            mainCategoryNames.put( catId[ i ] , cat[ i ] );
        }
    }

    private List<String> rankList = new ArrayList<>(HOME_TITLE_ARRAY.length);

    public List<String> getRankList() {
        return rankList;
    }

    public void setRankList(List<String> rankList) {
        this.rankList = rankList;
    }

    public HashMap<String, List<CategoryProperty>> getHomePageDataMap() {
        return homePageDataMap;
    }

    private HashMap<String , List<CategoryProperty>> homePageDataMap ;
    public void setHomePageData(){
        homePageDataMap = new HashMap<>(HOME_TITLE_ARRAY.length);
        homePageDataMap.put( HOME_TITLE_ARRAY[0] , new ArrayList<CategoryProperty>());
        homePageDataMap.put( HOME_TITLE_ARRAY[1] ,
                getCategoryList( AppValues.dailyBoosterTitleArray , AppValues.dailyBoosterIdArray , AppValues.dailyBoosterImageArray
                        , AppValues.dailyBoosterTypeArray , AppValues.dailyBoosterCatExistArray , AppValues.dailyBoosterWebViewArray , true) );
        homePageDataMap.put( HOME_TITLE_ARRAY[2] ,
                getCategoryList( AppValues.examTargetQueTitleArray , AppValues.examTargetQueIdArray , AppValues.examTargetQueImageArray
                        , AppValues.examTargetQueTypeArray , AppValues.examTargetQueCatExistArray , AppValues.examTargetQueWebViewArray, false) );
        homePageDataMap.put( HOME_TITLE_ARRAY[3] ,
                getCategoryList( AppValues.examMasterTitleArray , AppValues.examMasterIdArray , AppValues.examMasterImageArray
                        , AppValues.examMasterTypeArray , AppValues.examMasterCatExistArray , AppValues.examMasterWebViewArray, true) );
        homePageDataMap.put( HOME_TITLE_ARRAY[4] ,
                getCategoryList( AppValues.ncertTitleArray , AppValues.ncertIdArray , AppValues.ncertImageArray
                        , AppValues.ncertTypeArray , AppValues.ncertCatExistArray , AppValues.ncertWebViewArray, true) );
        homePageDataMap.put( HOME_TITLE_ARRAY[5] ,
                getCategoryList( AppValues.easyLearningTitleArray , AppValues.easyLearningIdArray , AppValues.easyLearningImageArray
                        , AppValues.easyLearningTypeArray , AppValues.easyLearningCatExistArray , AppValues.easyLearningWebViewArray, true) );
        homePageDataMap.put( HOME_TITLE_ARRAY[6] ,
                getCategoryList( AppValues.stateTitleArray , AppValues.stateIdArray , AppValues.stateImageArray
                        , AppValues.stateTypeArray , AppValues.stateCatExistArray , AppValues.stateWebViewArray , true ) );
    }

    private List<CategoryProperty> getCategoryList(String[] homeTitleArray , int[] idArray , int[] imageArr , int[] typeArray , boolean[] isSubCatArray
            , boolean[] isWebViewArray , boolean seeAns ){
        int length = homeTitleArray.length ;
        List<CategoryProperty> list = new ArrayList<>(length);
        CategoryProperty categoryProperty ;
        for ( int i = 0 ; i < length ; i++ ){
            categoryProperty = new CategoryProperty();
            categoryProperty.setTitle( homeTitleArray[i] );
            categoryProperty.setPosition( i );
            categoryProperty.setId( idArray[i] );
            categoryProperty.setImageResId( imageArr[i] );
            categoryProperty.setType( typeArray[i] );
            categoryProperty.setSubCat( isSubCatArray[i] );
            categoryProperty.setWebView( isWebViewArray[i] );
            categoryProperty.setHost(ConfigConstant.HOST_MAIN);
            categoryProperty.setSeeAnswer(seeAns);
            list.add(categoryProperty);
        }
        return list ;
    }

}
