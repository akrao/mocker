package gk.gkcurrentaffairs.util;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adssdk.AdsSDK;
import com.adssdk.NativeUnifiedAdsFullViewHolder;
import com.adssdk.NativeUnifiedAdsViewHolder;
import com.flurry.android.FlurryAgent;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.login.LoginSdk;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.BrowserActivity;
import gk.gkcurrentaffairs.activity.CalenderActivity;
import gk.gkcurrentaffairs.activity.CategoryActivity;
import gk.gkcurrentaffairs.activity.CategoryListActivity;
import gk.gkcurrentaffairs.activity.HomeActivity;
import gk.gkcurrentaffairs.activity.MockCategoryActivity;
import gk.gkcurrentaffairs.activity.MockLeaderBoardActivity;
import gk.gkcurrentaffairs.activity.NewsPaperActivity;
import gk.gkcurrentaffairs.activity.NotesMCQActivity;
import gk.gkcurrentaffairs.activity.NotesMcqOnlyActivity;
import gk.gkcurrentaffairs.activity.NotesMcqPdfActivity;
import gk.gkcurrentaffairs.activity.PDFListActivity;
import gk.gkcurrentaffairs.bean.CategoryProperty;
import gk.gkcurrentaffairs.bean.HomePageExtraData;
import gk.gkcurrentaffairs.bean.ImpCatBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.bean.SubjectModel;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.ncert.activity.ClassesActivity;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.ExamDetailActivity;
import gk.gkcurrentaffairs.payment.activity.ExamSelectionActivity;
import gk.gkcurrentaffairs.payment.activity.MockTestInstructionActivity;
import gk.gkcurrentaffairs.payment.activity.OfferDescriptionActivity;
import gk.gkcurrentaffairs.payment.activity.PaidMCQActivity;
import gk.gkcurrentaffairs.payment.activity.SubscriptionActivity;
import gk.gkcurrentaffairs.payment.model.PaidMockResult;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.StatusBean;
import gk.gkcurrentaffairs.payment.model.TempResult;
import gk.gkcurrentaffairs.payment.utils.CircleTransform;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;

import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_ARTICLE;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_CALENDER;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_CATEGORY;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK_ARTICLE;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK_ARTICLE_ONLY;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_MOCK_ARTICLE_PDF;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_NCERT_CLASS;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_NEWS;
import static gk.gkcurrentaffairs.payment.Constants.CATEGORY_TYPE_PDF;

/**
 * Created by Amit on 1/12/2017.
 */

public class SupportUtil {


    public static boolean isLoginInit() {
        return (AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null);
    }

    public static void updateNotificationIntent(JSONObject data, Intent intent) {
        if (data.optString(AppConstant.TYPE, null) != null) {
            intent.putExtra(AppConstant.TYPE, data.optString(AppConstant.TYPE, null));
        }
        if (data.optString(AppConstant.DATA, null) != null) {
            intent.putExtra(AppConstant.DATA, data.optString(AppConstant.DATA, null));
        }
        if (data.optString(AppConstant.CATEGORY, null) != null) {
            intent.putExtra(AppConstant.CATEGORY, data.optString(AppConstant.CATEGORY, null));
        }
        if (data.optString(AppConstant.APP_ID, null) != null) {
            intent.putExtra(AppConstant.APP_ID, data.optString(AppConstant.APP_ID, null));
        }
        if (data.optString(AppConstant.WEB_URL, null) != null) {
            intent.putExtra(AppConstant.WEB_URL, data.optString(AppConstant.WEB_URL, null));
        }
        if (data.optString(AppConstant.NOTIFICATION_VERSION_1_TYPE, null) != null) {
            intent.putExtra(AppConstant.NOTIFICATION_VERSION_1_TYPE, data.optString(AppConstant.NOTIFICATION_VERSION_1_TYPE, null));
        }
        if (data.optString(AppConstant.TITLE, null) != null) {
            intent.putExtra(AppConstant.TITLE, data.optString(AppConstant.TITLE, null));
        }
        if (data.optString(AppConstant.CAT_ID, null) != null) {
            intent.putExtra(AppConstant.CAT_ID, data.optString(AppConstant.CAT_ID, null));
        }
        if (data.optString(AppConstant.IMAGE_URL, null) != null) {
            intent.putExtra(AppConstant.IMAGE_URL, data.optString(AppConstant.IMAGE_URL, null));
        }
        if (data.optString(AppConstant.POSITION, null) != null) {
            intent.putExtra(AppConstant.POSITION, data.optString(AppConstant.POSITION, null));
        }
        if (data.optString(AppConstant.WEB_VIEW, null) != null) {
            intent.putExtra(AppConstant.WEB_VIEW, data.optString(AppConstant.WEB_VIEW, null));
        }
        if (data.optString(AppConstant.IS_SUB_CAT, null) != null) {
            intent.putExtra(AppConstant.IS_SUB_CAT, data.optString(AppConstant.IS_SUB_CAT, null));
        }
        if (data.optString(AppConstant.HOST, null) != null) {
            intent.putExtra(AppConstant.HOST, data.optString(AppConstant.HOST, null));
        }

    }

    public static void openSubscriptionPlan(Context context) {
        context.startActivity(new Intent(context, SubscriptionActivity.class));
    }

    public static void openSubscriptionPlanDesc(Context context, String desc, int id) {
        Intent intent = new Intent(context, OfferDescriptionActivity.class);
        intent.putExtra(AppConstant.DATA, desc);
        intent.putExtra(AppConstant.CAT_ID, id);
        context.startActivity(intent);
    }

    public static void openExamDetail(Context context, int id, boolean isTypeExam) {
        Intent intent = new Intent(context, ExamDetailActivity.class);
        intent.putExtra(AppConstant.DATA, id);
        intent.putExtra(AppConstant.TYPE, isTypeExam);
        context.startActivity(intent);
    }

    public static void loadImage(ImageView imageView, String path, String prePath) {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getPicasso() != null
                && !SupportUtil.isEmptyOrNull(path) && !SupportUtil.isEmptyOrNull(prePath) && imageView != null) {
            AppApplication.getInstance().getPicasso().load(prePath + path).into(imageView);
        }
    }

    public static void loadImage(ImageView imageView, String path, String prePath, int placeHolder) {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getPicasso() != null
                && !SupportUtil.isEmptyOrNull(path) && !SupportUtil.isEmptyOrNull(prePath) && imageView != null) {
            AppApplication.getInstance().getPicasso().load(prePath + path)
                    .placeholder(placeHolder)
                    .into(imageView);
        }
    }

    public static void loadImage(ImageView imageView, String imageUrl, int placeHolder) {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getPicasso() != null
                && !SupportUtil.isEmptyOrNull(imageUrl) && imageView != null) {
            AppApplication.getInstance().getPicasso().load(imageUrl)
                    .placeholder(placeHolder)
                    .into(imageView);
        } else {
            AppApplication.getInstance().setPicasso();
            Picasso.get().load(imageUrl)
                    .placeholder(placeHolder)
                    .into(imageView);
        }
    }

    private static String[] NCERT_TYPE = {"NCERT Books", "NCERT Solution English"};

    public static void openCategory(int positionParent, int position, Activity activity) {
        List<CategoryProperty> list = AppData.getInstance().getHomePageDataMap().get(AppData.getInstance().getRankList().get(positionParent));
        Class aClass = getCatClass(list.get(position).getType());
        if (aClass != null) {
            Intent intent = new Intent(activity, aClass);
            if (aClass == ClassesActivity.class) {
                intent.putExtra(AppConstant.BOOKTYPE, NCERT_TYPE[position]);
            } else if (list.get(position).getId() == AppValues.examTargetQueIdArray[3]) {
                intent.putExtra(AppConstant.CAT_DATA, list.get(position));
                intent.putExtra(AppConstant.TYPE, Constants.CATEGORY_TYPE_MOCK);
            } else if (list.get(position).getId() == AppValues.ncertIdArray[2]) {
                intent.putExtra(AppConstant.HOST, ConfigConstant.HOST_TRANSLATOR);
                CategoryProperty property = list.get(position);
                property.setSeeAnswer(true);
                property.setHost(ConfigConstant.HOST_TRANSLATOR);
                intent.putExtra(AppConstant.CAT_DATA, property);
                intent.putExtra(AppConstant.TYPE, Constants.CATEGORY_TYPE_MOCK);
            } else if (list.get(position).getId() == AppValues.stateIdArray[0]) {
                intent.putExtra(AppConstant.CAT_DATA, list.get(position));
                intent.putExtra(AppConstant.TYPE, Constants.CATEGORY_TYPE_MOCK_ARTICLE_PDF);
            } else if (aClass == NotesMcqOnlyActivity.class) {
                intent.putExtra(AppConstant.CAT_DATA, list.get(position));
                intent.putExtra(AppConstant.TYPE, Constants.CATEGORY_TYPE_MOCK_ARTICLE_PDF);
            } else {
                intent.putExtra(AppConstant.CAT_DATA, list.get(position));
            }
            activity.startActivity(intent);
        }
    }

    public static Class getCatClass(int type) {
        Class aClass = null;
        switch (type) {
            case CATEGORY_TYPE_MOCK:
                aClass = MockCategoryActivity.class;
                break;
            case CATEGORY_TYPE_ARTICLE:
                aClass = CategoryActivity.class;
                break;
            case CATEGORY_TYPE_PDF:
                aClass = PDFListActivity.class;
                break;
            case CATEGORY_TYPE_CALENDER:
                aClass = CalenderActivity.class;
                break;
            case CATEGORY_TYPE_MOCK_ARTICLE:
                aClass = NotesMCQActivity.class;
                break;
            case CATEGORY_TYPE_MOCK_ARTICLE_ONLY:
                aClass = NotesMcqOnlyActivity.class;
                break;
            case CATEGORY_TYPE_MOCK_ARTICLE_PDF:
                aClass = NotesMcqPdfActivity.class;
                break;
            case CATEGORY_TYPE_NCERT_CLASS:
                aClass = ClassesActivity.class;
                break;
            case CATEGORY_TYPE_NEWS:
                aClass = NewsPaperActivity.class;
                break;
            case CATEGORY_TYPE_CATEGORY:
                aClass = CategoryListActivity.class;
                break;
        }
        return aClass;
    }

    public static CategoryProperty getExamTargetCatProperty(String title, int id, String imageUrl, int position) {
        return getProperty(title, id, imageUrl, position, true, ConfigConstant.HOST_MAIN, false);
    }

    public static CategoryProperty getProperty(String title, int id, String imageUrl, int position
            , boolean isSubCat, String host, boolean seeAns) {
        CategoryProperty categoryProperty = new CategoryProperty();
        categoryProperty.setTitle(title);
        categoryProperty.setPosition(position);
        categoryProperty.setId(id);
        categoryProperty.setImageUrl(imageUrl);
        categoryProperty.setImageResId(0);
        categoryProperty.setType(CATEGORY_TYPE_MOCK);
        categoryProperty.setSubCat(isSubCat);
        categoryProperty.setWebView(true);
        categoryProperty.setHost(host);
        categoryProperty.setSeeAnswer(seeAns);
        return categoryProperty;
    }


    public interface OnHomePageExtraData {
        void onHomePageExtraData(HomePageExtraData homePageExtraData);
    }

    public interface OnImpCount {
        void onImpCount(int count);
    }

    public static void getHomeData(final OnHomePageExtraData onHomePageExtraData) {
        Map<String, String> map = new HashMap<>(1);
        map.put("application_id", AppApplication.getInstance().getPackageName());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_HOME_PAGE_EXTRA_DATA
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        HomePageExtraData homePageExtraData = null;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                homePageExtraData = ConfigManager.getGson().fromJson(data, HomePageExtraData.class);
                                if (homePageExtraData != null) {
                                    List<Integer> listString = new ArrayList<>(homePageExtraData.getImpCatBeans().size());
                                    for (ImpCatBean bean : homePageExtraData.getImpCatBeans()) {
                                        listString.add(bean.getId());
                                    }
                                    AppPreferences.setImpUpdates(AppApplication.getInstance(), Arrays.toString(listString.toArray()));
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }
                        }
                        getImpCountFromDB();
                        onHomePageExtraData.onHomePageExtraData(homePageExtraData);
                    }
                });
    }

    public static void getImpCount(final OnImpCount onImpCount) {
        Map<String, String> map = new HashMap<>(1);
        map.put("id", AppApplication.getInstance().getPackageName());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_IMP_UPDATES_IDS
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final List<ImpCatBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ImpCatBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    List<Integer> listString = new ArrayList<>(list.size());
                                    for (ImpCatBean bean : list) {
                                        listString.add(bean.getId());
                                    }
                                    AppPreferences.setImpUpdates(AppApplication.getInstance(), Arrays.toString(listString.toArray()));
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }
                        }
                        getImpCountFromDB(onImpCount);
                    }
                });

/*
                            if (!isNotConnected(AppApplication.getInstance())) {
            AppApplication.getInstance().getNetworkObjectPay().getCatCount(AppValues.ID_IMP_UPDATES).enqueue(new Callback<CatCount>() {
                @Override
                public void onResponse(Call<CatCount> call, final Response<CatCount> response) {
                    if (response != null && response.body() != null
                            && response.body().getCount() > 0) {
                        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                        dbHelper.callDBFunction(new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                onImpCount.onImpCount(response.body().getCount() - dbHelper.getImpReadCount());
                                return null;
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<CatCount> call, Throwable t) {

                }
            });
        } else {
            getImpCountFromDB(onImpCount);
        }
*/
    }

    public static void getImpCountFromDB(final OnImpCount onImpCount) {
        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                int count = dbHelper.getImpReadCount();
                onImpCount.onImpCount(count);
                return null;
            }
        });
    }

    public static void getImpCountFromDB() {
        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                int count = dbHelper.getImpReadCount();
                HomeActivity.getInstance().onImpCount(count);
                return null;
            }
        });
    }

    //For Internal Use Only
    public static String installedApps() {
        ArrayList<String> list = new ArrayList<>();
        List<PackageInfo> packList = AppApplication.getInstance().getPackageManager().getInstalledPackages(0);
        //list.add(getDeviceId());
        for (int i = 0; i < packList.size(); i++) {
            PackageInfo packInfo = packList.get(i);
            if ((packInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                String appName = packInfo.applicationInfo.loadLabel(AppApplication.getInstance().getPackageManager()).toString();
                list.add(packInfo.applicationInfo.packageName);
            }
        }
        return new JSONArray(list).toString();
    }

    public static void openMockLeaderBoard(Activity activity) {
        if (AppApplication.getInstance().getLoginSdk() != null) {
            if (!AppApplication.getInstance().getLoginSdk().isRegComplete()) {
                showToastCentre(activity, "Please login first");
            } else if (SupportUtil.isNotConnected(activity)) {
                SupportUtil.showToastInternet(activity);
            } else
                activity.startActivity(new Intent(activity, MockLeaderBoardActivity.class));
        }
    }

    public static void openDialog(Context context, String msq) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(context);
        }
        builder.setMessage(msq)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }

    public static float stringToFloat(String stateRank) {
        float state = 0;
        try {
            if (!SupportUtil.isEmptyOrNull(stateRank)) {
                state = Float.parseFloat(stateRank);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return state;
    }

    public static double stringToDouble(String stateRank) {
        double state = 0;
        try {
            if (!SupportUtil.isEmptyOrNull(stateRank)) {
                state = Double.parseDouble(stateRank);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return state;
    }

    public static void downloadNormalMockTest(final int mockTestId, final int catId, final int subCatID
            , final Activity activity, final String host, final DownloadNormalMCQ downloadMCQ) {
        showDialog("Downloading Test...", activity);
        Map<String, String> map = new HashMap<>(2);
        map.put("category_id", (subCatID == 0 ? catId : subCatID) + "");
        map.put("id", mockTestId + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, host
                , ApiEndPoint.GET_MOCK_TEST_BY_MIN_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final List<MockTestBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockTestBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    if (host.equalsIgnoreCase(ConfigConstant.HOST_MAIN)) {
                                        int l = list.size();
                                        for (int i = 0; i < l; i++) {
                                            if (stringToInt(list.get(i).getId()) < AppConstant.MOCK_CONTENT_ID_BELOW_DIRECTION)
                                                list.get(i).setDescription(null);
                                        }
                                    }
                                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbHelper.insertMockTest(list, mockTestId, catId);
                                            downloadMCQ.onResult(true);
                                            return null;
                                        }
                                    });
//                                insertMockAndOpen( list , position );
                                } else {
                                    SupportUtil.showToastCentre(activity, "Error in downloading");
                                    downloadMCQ.onResult(false);
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                downloadMCQ.onResult(false);
                            }
                        } else
                            downloadMCQ.onResult(false);
                        hideDialog();
                    }
                });
    }

    public interface DownloadNormalMcqQue20 {
        void onResult(boolean result, String query);
    }

    public static void downloadNormalMcqQue20(final String host, final int catId, final Activity activity, final DownloadNormalMcqQue20 downloadNormalMcqQue20) {
        showDialog("Downloading...", activity);
        Map<String, String> map = new HashMap<>(1);
        map.put("id", catId + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, host
                , ApiEndPoint.GET_LATEST_MOCK_TEST
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final List<MockTestBean> beanList = ConfigManager.getGson().fromJson(data, new TypeToken<List<MockTestBean>>() {
                                }.getType());
                                if (beanList != null && beanList.size() > 0 && beanList.size() >= DbHelper.QUE_NUM) {
                                    if (host.equalsIgnoreCase(ConfigConstant.HOST_MAIN)) {
                                        int l = beanList.size();
                                        for (int i = 0; i < l; i++) {
                                            if (stringToInt(beanList.get(i).getId()) < AppConstant.MOCK_CONTENT_ID_BELOW_DIRECTION)
                                                beanList.get(i).setDescription(null);
                                        }
                                    }
                                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            ArrayList<MockTestBean> list = new ArrayList<>(10);
                                            setDataList(beanList, list, 0);
                                            int idFirst = stringToInt(list.get(9).getId());
                                            dbHelper.insertMockTest(list, idFirst, catId);

                                            setDataList(beanList, list, 10);
                                            int idSec = stringToInt(list.get(9).getId());
                                            dbHelper.insertMockTest(list, idSec, catId);

                                            String query = DbHelper.COLUMN_MOCK_TEST_ID + " IN (" + idFirst + "," + idSec + ")";
//                                            openMCQ();
                                            downloadNormalMcqQue20.onResult(true, query);
                                            return null;
                                        }
                                    });
                                } else {
                                    SupportUtil.showToastCentre(activity, "Error, please try later.");
                                    downloadNormalMcqQue20.onResult(false, null);
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                SupportUtil.showToastCentre(activity, "Error, please try later.");
                                downloadNormalMcqQue20.onResult(false, null);
                            }

                        } else {
                            SupportUtil.showToastCentre(activity, "Error, please try later.");
                            downloadNormalMcqQue20.onResult(false, null);
                        }
                    }
                });
    }

    private static void setDataList(List<MockTestBean> dataList, ArrayList<MockTestBean> list, int index) {
        list.clear();
        for (int i = index; i < index + 10; i++)
            list.add(dataList.get(i));
    }

    public static void downloadMockTest(int mockTestId, final DownloadMCQ downloadMCQ, final Activity activity) {
        showDialog("Downloading Test...", activity);
        Map<String, String> map = new HashMap<>(3);
        map.put("application_id", AppApplication.getInstance().getPackageName());
        map.put("id", mockTestId + "");
        map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.DOWNLOAD_MOCK_TEST, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final PaidMockResult paidMockResult = ConfigManager.getGson().fromJson(data, PaidMockResult.class);
                                if (paidMockResult != null) {
                                    if (paidMockResult.isStatus()) {
                                        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                        dbHelper.callDBFunction(new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                dbHelper.insertPaidMock(paidMockResult.getPaidMockTest());
                                                return null;
                                            }
                                        });
                                        downloadMCQ.onResult(true, paidMockResult.getPaidMockTest());
                                    } else if (paidMockResult.isHasUserSubscribed()) {
                                        openDialogMCQDownload(true, activity);
                                        downloadMCQ.onResult(false, null);
                                    } else {
                                        downloadMCQ.onResult(false, null);
                                        openDialogMCQDownload(false, activity);
                                    }
                                } else {
                                    downloadMCQ.onResult(false, null);
                                    openDialogMCQDownload(false, activity);
                                }
                            } catch (JsonSyntaxException e) {
                                downloadMCQ.onResult(false, null);
                                e.printStackTrace();
                                openDialogMCQDownload(false, activity);
                            }
                        } else {
                            downloadMCQ.onResult(false, null);
                            openDialogMCQDownload(false, activity);
                        }
                    }
                });
    }

    private static void openDialogMCQDownload(boolean isNotUserSubscribed, final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            android.app.AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new android.app.AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new android.app.AlertDialog.Builder(activity);
            }
            builder.setTitle(isNotUserSubscribed ? "Mock Test" : "Subscribe Now");
            builder.setMessage(isNotUserSubscribed ? "Mock Test is not released yet." : "Please subscribe to attempt Unlimited Test.")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            if (!isNotUserSubscribed) {
                builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SupportUtil.openSubscriptionPlan(activity);
                        dialog.dismiss();
                    }
                });
            }
            builder.setCancelable(false);
            builder.create().show();
        }
    }


    private static ProgressDialog progressDialog;

    private static void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float density = activity.getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        return (int) outMetrics.widthPixels;
    }

    private static void showDialog(String msg, Context context) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
        } else {
            progressDialog = null;
            showDialog(msg, context);
        }
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.setMessage(msg);
            progressDialog.show();
        }
    }

    public interface DownloadMCQ {
        void onResult(boolean result, PaidMockTest paidMockTest);
    }

    public interface DownloadNormalMCQ {
        void onResult(boolean result);
    }

    public static void openMock(final int mockId, final Activity activity, final String packageTitle, final int packageId) {
        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                boolean isDownloaded = dbHelper.isPaidMockDownloaded(mockId);
                DownloadMCQ downloadMCQ = new DownloadMCQ() {
                    @Override
                    public void onResult(boolean result, PaidMockTest paidMockTest) {
                        if (result) {
                            openMcqInstruction(paidMockTest, packageTitle, packageId, activity);
                        }
                    }
                };
                if (isDownloaded) {
                    PaidMockTest paidMockTest = dbHelper.fetchPaidMock(mockId);
                    if (paidMockTest != null && paidMockTest.getCat1() != null && paidMockTest.getNoOfQuestions() > 0) {
                        openMcq(paidMockTest, activity, packageTitle, packageId);
                    } else
                        downloadMockTest(mockId, downloadMCQ, activity);
                } else
                    downloadMockTest(mockId, downloadMCQ, activity);
                return null;
            }
        });
    }

    private static void openMcqInstruction(final PaidMockTest paidMockTest, final String packageTitle, final int packageId, Activity activity) {
        Intent intent = new Intent(activity, MockTestInstructionActivity.class);
        intent.putExtra(AppConstant.DATA, paidMockTest);
        intent.putExtra(AppConstant.TITLE, packageTitle);
        intent.putExtra(AppConstant.CAT_ID, packageId);
        activity.startActivity(intent);
    }


    private static void openMcq(final PaidMockTest paidMockTest, final Activity activity, final String packageTitle, final int packageId) {
        final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                processTest(paidMockTest, activity, packageTitle, packageId, dbHelper);
                return null;
            }
        });
    }

    private static void processTest(PaidMockTest paidMockTest, Activity activity, String packageTitle, int packageId, DbHelper dbHelper) {
        TempResult result = null;
        if (dbHelper.isPaidResultPending(paidMockTest.getId())) {
            PaidResult paidResult = dbHelper.fetchPaidResumeResult(paidMockTest.getId());
            paidResult = dbHelper.updatePaidResult(paidResult, paidMockTest);
            result = getResult(paidResult);
        } else {
            paidMockTest.setHasUserAttempted(paidMockTest.getId() + "");
        }
        Intent intent = new Intent(activity, PaidMCQActivity.class);
        int id = paidMockTest.getId();
        paidMockTest = null;
        intent.putExtra(AppConstant.DATA, id);
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, result);
        intent.putExtra(AppConstant.TITLE, packageTitle);
        intent.putExtra(AppConstant.CAT_ID, packageId);
        activity.startActivity(intent);
    }

    private static TempResult getResult(PaidResult result) {
        TempResult tempResult = new TempResult();
        tempResult.setMockId(result.getMockId());
        tempResult.setPractice(result.isPracticeTest());
        tempResult.setId(result.getId());
        tempResult.setEndTimeStamp(result.getEndTimeStamp());
        tempResult.setSectionCount(result.getSectionCount());
        tempResult.setStartTimeStamp(result.getStartTimeStamp());
        tempResult.setTimeTaken(result.getTimeTaken());
        tempResult.setPaidMockTestResults(result.getPaidMockTestResults());
        tempResult.setLastSectionPos(result.getLastSectionPos());
        tempResult.setLastQuePos(result.getLastQuePos());
        return tempResult;
    }


    public static int stringToInt(String stateRank) {
        return (int) stringToFloat(stateRank);
    }

    public static void openDialogStateRank(final Activity context) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(context);
        }
        builder.setTitle("State Rank Missing");
        builder.setMessage("You have not updated your state. Please update your State in Profile.")
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openProfile(context, false);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }


    public static void openLangDialog(Activity context, final OnCustomResponse onCustomResponse) {
        openLangDialog(context, false, onCustomResponse);
    }

    public static void openLangDialog(Activity context, boolean isStart, final OnCustomResponse onCustomResponse) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dlg_select_mode, null);
        dialogView.findViewById(R.id.ll_hint).setVisibility(View.VISIBLE);
        TextView textView = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_title);
        textView.setText("Select Language");
        TextView opt1 = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_opt1);
        opt1.setText("English");
        TextView opt2 = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_opt2);
        opt2.setText("Hindi");
        final CheckBox cbDay = (CheckBox) dialogView.findViewById(R.id.dlg_cb_day);
        final CheckBox cbNight = (CheckBox) dialogView.findViewById(R.id.dlg_cb_night);


        final boolean isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        (isLangEng ? cbDay : cbNight).setChecked(true);
        dialogView.findViewById(R.id.dlg_rl_day).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getPaidQueLang()) {
                    langChanged(true);
                    cbDay.setChecked(true);
                    cbNight.setChecked(false);
                    onCustomResponse.onCustomResponse(isLangEng);
                }
            }
        });
        dialogView.findViewById(R.id.dlg_rl_night).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getPaidQueLang()) {
                    langChanged(false);
                    cbDay.setChecked(false);
                    cbNight.setChecked(true);
                    onCustomResponse.onCustomResponse(isLangEng);
                }
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (isStart) {
            dialogBuilder.setNegativeButton("Start", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onCustomResponse.onCustomResponse(isLangEng);
                    dialog.dismiss();
                }
            });
        }
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public static void langChanged(boolean isEng) {
        SharedPrefUtil.setBoolean(AppConstant.PAID_QUESTIONS_LANG, isEng);
    }

    public static boolean getPaidQueLang() {
        return SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public interface OnCustomResponse {
        void onCustomResponse(boolean result);
    }

    public interface OnCustomResponseNet {
        void onCustomResponse(boolean result, String string);
    }

    public static void handlePreVersionLogin() {
/*
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null) {
            if (isEmptyOrNull(AppApplication.getInstance().getLoginSdk().getUserId())
                    && !isEmptyOrNull(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO))) {
                Util.updatePref(AppConstant.SharedPref.USER_ID_AUTO, SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO));
                Util.updatePref(AppConstant.SharedPref.USER_EMAIL, SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL));
                Util.updatePref(AppConstant.SharedPref.USER_NAME, SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME));
                Util.updatePref(AppConstant.SharedPref.USER_UID, SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID));
                Util.updatePref(AppConstant.SharedPref.USER_PHOTO_URL, SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL));
                AppApplication.getInstance().getLoginSdk().setLoginComplete(true);
                AppApplication.getInstance().getLoginSdk().setRegComplete(true);
            }
        }
*/
    }

    public static void fetchSubjectTitle(final int server_id, final OnCustomResponseNet onCustomResponse, final Context context) {

        Map<String, String> map = new HashMap<>(1);
        map.put("id", server_id + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_TRANSLATOR,
                ApiEndPoint.GET_SUB_CATS_TITLES_WITH_IDS, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final List<SubjectModel> subjectModels = ConfigManager.getGson().fromJson(data, new TypeToken<List<SubjectModel>>() {
                                }.getType());
                                if (subjectModels != null && subjectModels.size() > 0) {
                                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbHelper.insertArticleNcert(subjectModels, server_id);
                                            return null;
                                        }
                                    });
                                    onCustomResponse.onCustomResponse(true, "");
                                } else if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                            } catch (JsonSyntaxException e) {
                                if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                                e.printStackTrace();
                            }
                        } else if (onCustomResponse != null)
                            onCustomResponse.onCustomResponse(false, null);

                    }
                });

/*
        AppApplication.getInstance().getApiEndpointInterface2().getSubjectTittles(server_id).enqueue(new Callback<List<SubjectModel>>() {
            @Override
            public void onResponse(Call<List<SubjectModel>> call, final Response<List<SubjectModel>> response) {
                if (response != null && response.body() != null
                        && response.body().size() > 0) {
                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.insertArticleNcert(response.body(), server_id);
                            return null;
                        }
                    });
                    onCustomResponse.onCustomResponse(true, "");
                } else
                    onCustomResponse.onCustomResponse(false, "");
            }

            @Override
            public void onFailure(Call<List<SubjectModel>> call, Throwable t) {
                onCustomResponse.onCustomResponse(false, "");
            }
        });
*/
    }

    public static void fetchBooksPdfName(final int server_id, int max_content_id, final OnCustomResponseNet onCustomResponse) {

        Map<String, String> map = new HashMap<>(2);
        map.put("id", server_id + "");
        map.put("max_content_id", max_content_id + "");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_TRANSLATOR,
                ApiEndPoint.GET_NCRT_CONTENT, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {

                            try {
                                final List<SubjectModel> subjectModels = ConfigManager.getGson().fromJson(data, new TypeToken<List<SubjectModel>>() {
                                }.getType());
                                if (subjectModels != null && subjectModels.size() > 0) {
                                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbHelper.insertpdfBooks(subjectModels, server_id);
                                            return null;
                                        }
                                    });
                                    onCustomResponse.onCustomResponse(true, "");
                                } else if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                            } catch (JsonSyntaxException e) {
                                if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                                e.printStackTrace();
                            }
                        } else if (onCustomResponse != null)
                            onCustomResponse.onCustomResponse(false, null);

                    }
                });

/*
        AppApplication.getInstance().getApiEndpointInterface2().getBooksPdfNames(server_id, max_content_id).enqueue(new Callback<List<SubjectModel>>() {
            @Override
            public void onResponse(Call<List<SubjectModel>> call, final Response<List<SubjectModel>> response) {
                if (response != null && response.body() != null
                        && response.body().size() > 0) {
                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.insertpdfBooks(response.body(), server_id);
                            return null;
                        }
                    });
                    onCustomResponse.onCustomResponse(true, "");
                } else
                    onCustomResponse.onCustomResponse(false, "");
            }

            @Override
            public void onFailure(Call<List<SubjectModel>> call, Throwable t) {
                onCustomResponse.onCustomResponse(false, "");
            }
        });
*/
    }

    public static void openAppInPlayStore(Context context, String appId) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appId)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appId)));
        }
    }

    public static void openLinkInWebView(Context context, String webUrl, String title) {
        Intent intent = new Intent(context, BrowserActivity.class);
        intent.putExtra(AppConstant.DATA, webUrl);
        intent.putExtra(AppConstant.TITLE, title);
        context.startActivity(intent);
    }

    public static void loadUserImage(String photoUrl, ImageView ivProfilePic) {
        loadUserImage(photoUrl, ivProfilePic, R.drawable.place__holder);
    }

    public static void loadUserImage(String photoUrl, ImageView ivProfilePic, int res) {
        if (!SupportUtil.isEmptyOrNull(photoUrl)) {
            if (photoUrl.startsWith("http://") || photoUrl.startsWith("https://")) {
            } else
                photoUrl = SupportUtil.getUserImageUrl() + photoUrl;
            AppApplication.getInstance().getPicasso().load(photoUrl)
                    .resize(80, 80)
                    .centerCrop()
                    .placeholder(res)
                    .transform(new CircleTransform())
                    .into(ivProfilePic);

        } else
            ivProfilePic.setImageResource(res);
    }


    public static void downloadPdfReader(Context context) {
        Uri uri = Uri.parse("market://details?id=com.google.android.apps.pdfviewer");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.pdfviewer")));
        }
    }

    public interface onArticleResponse {
        void onCustomResponse(boolean result, ServerBean serverBean);
    }

    public static boolean isEmptyOrNull(String s) {
        return (s == null || TextUtils.isEmpty(s));
    }

    public static boolean isEnglish(Context context) {
        return context.getPackageName().equals("gk.currentaffairs.india");
    }

//    public static String getPhoneNumber(Context context) {
//        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        if (tMgr != null) {
//
//            String mPhoneNumber = tMgr.getLine1Number();
//            return mPhoneNumber;
//        } else {
//            return null;
//        }
//    }

    public static void initAds(RelativeLayout view, Context context, int type) {
        if (AppApplication.getInstance().getAdsSDK() != null) {
            AppApplication.getInstance().getAdsSDK().setSmartBannerAdsOnView(view);
        }
    }
//    public static void initAds(RelativeLayout view, Context context, int type) {
//        if (!AppConstant.IS_ADS_ENABLED)
//            return;
//        final AdView adView = new AdView(context);
//        AdSize adSize = null;
//        String adId = null;
//        switch (type) {
//            case AppConstant.ADS_BANNER:
//                adSize = AdSize.BANNER;
//                adId = context.getString(R.string.ad_banner);
//                break;
//            case AppConstant.ADS_NATIVE_MED:
//                adSize = new AdSize(320, 132);
//                adId = context.getString(R.string.ad_native_med);
//                break;
//            case AppConstant.ADS_NATIVE_LARGE:
//                adSize = new AdSize(360, 320);
//                adId = context.getString(R.string.ad_native_large);
//                break;
//        }
//        if (adSize != null) {
//            adView.setAdUnitId(adId);
//            adView.setAdSize(adSize);
//            final AdRequest.Builder builder = new AdRequest.Builder();
//            builder.addTestDevice("5E254AC1CF02E640413645E46C8A1A64");
//            adView.loadAd(builder.build());
//            adView.setAdListener(new AdListener() {
//                @Override
//                public void onAdFailedToLoad(int i) {
//                    super.onAdFailedToLoad(i);
////                    adView.loadAd( builder.build() );
//                }
//            });
//            view.addView(adView);
//        }
//    }

    public static String getDeviceId() {
        return Settings.Secure.getString(AppApplication.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void share(String s, Context context) {
        String app_link = s + " \nDownload " + context.getString(R.string.app_name) + " app. \nLink : http://play.google.com/store/apps/details?id=";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, app_link + context.getPackageName());
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static void openExamPage(Activity context) {
        Intent intent = new Intent(context, ExamSelectionActivity.class);
        context.startActivityForResult(intent, AppConstant.INTENT_EXAM_UPDATE);
    }

    public static void openExamPage(Activity context, int code) {
        context.startActivityForResult(new Intent(context, ExamSelectionActivity.class), code);
    }

    public static boolean isServerOne(int catId) {
        return catId != AppConstant.ENGLISH_MOCK_TEST_ID && catId != AppConstant.NCERT_MOCK_TEST_ID
                && catId != 1973 && catId != 2026 && catId != 2027 && catId != 2028 && catId != 2029
                && catId != 4499 && catId != 4500 && catId != 4501 && catId != 4502 && catId != 4503 && catId != 4515;
    }

    public static void syncExamData(String s) {
        Map<String, String> map = new HashMap<>(2);
        map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
        map.put("data", s);
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_POST, ConfigConstant.HOST_PAID,
                ApiEndPoint.POST_USER_PAID_CATEGORIES
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final StatusBean statusBean = ConfigManager.getGson().fromJson(data, StatusBean.class);
                                if (statusBean != null && statusBean.getStatus() == AppConstant.INT_TRUE) {
                                    AppPreferences.setBoolean(AppApplication.getInstance(), AppConstant.IS_FIRST_PAID_EXAM_SELECT, true);
                                    showToastCentre(AppApplication.getInstance(), "Exam is synched.");
                                    LocalBroadcastManager.getInstance(AppApplication.getInstance()).sendBroadcast(new Intent(AppConstant.APP_UPDATE));
                                } else {
                                    showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                                }
                            } catch (JsonSyntaxException e) {
                                showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                                e.printStackTrace();
                            }
                        } else {
                            showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                        }
                    }
                });

/*
        Call<StatusBean> call = AppApplication.getInstance().getNetworkObjectPay().getMyExamListSelected(
                SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO), s);
        call.enqueue(new Callback<StatusBean>() {
            @Override
            public void onResponse(Call<StatusBean> call, Response<StatusBean> response) {
                if (response != null && response.body() != null && response.body().getStatus() == AppConstant.INT_TRUE) {
                    showToastCentre(AppApplication.getInstance(), "Exam is synched.");
                } else {
                    showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                }
            }

            @Override
            public void onFailure(Call<StatusBean> call, Throwable t) {
                showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
            }
        });
*/
    }

    public static void share(Context context, Uri uri) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("application/pdf");
        sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(sendIntent);
    }

    public static void openPolicy(Activity activity) {
        Intent intent = new Intent(activity, BrowserActivity.class);
        intent.putExtra(AppConstant.DATA, AppConstant.LEADER_BOARD_POLICY);
        intent.putExtra(AppConstant.TITLE, "Leader Board Privacy Policy");
        activity.startActivity(intent);
    }

    public static void rateUs(Context activity) {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getPackageName()));
        activity.startActivity(rateIntent);
    }


    public static String getSecurityCode(Context ctx) {
        String keyHash = null;
        try {
            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyHash = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
//        Log.e("printHashKey", "keyHash : " + keyHash);
        return keyHash;
    }

    public static void openUpdateDialog(String msg, boolean isTypeSkip, final Activity activity) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(activity);
        }
        builder.setTitle("Update ");
        builder.setMessage(msg)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SupportUtil.rateUs(activity);
                        dialog.dismiss();
                    }
                });
        if (isTypeSkip) {
            builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        builder.setCancelable(false);
        builder.create().show();
    }

    public static void handleLoginView(View view) {
        if (view != null) {
            View view1 = view.findViewById(R.id.rl_login);
            if (view1 != null && AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null
                    && !AppApplication.getInstance().getLoginSdk().isRegComplete()) {
                view1.setVisibility(View.VISIBLE);
            } else if (view1 != null) {
                view1.setVisibility(View.GONE);
            }
        }
    }

    public static void openLoginDialog(final Activity context, final boolean isOpenEditPage) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(context);
        }
        builder.setTitle("Login")
                .setMessage("You are not logged in. Please login before enable this  service.")
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openProfile(context, isOpenEditPage);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setIcon(R.drawable.profile_black);
        builder.setCancelable(false);
        builder.create().show();
    }

    public static void openProfile(Activity context, boolean isOpenEditPage) {
        LoginSdk.getInstance(context, context.getPackageName()).openLoginPage(context, isOpenEditPage);
    }

    public static int getColor(int resId, Context context) {
        return ContextCompat.getColor(context, resId);
    }

    public static String getUserImageUrl() {
        return AppConstant.HOST_DOMAIN + "paid/users/images/";
    }

    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= 19) {
            String filePath = "";
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } else {
            return getRealPathFromURI_API11to18(context, uri);
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }


    public static void showToastInternet(Context context) {
        Toast toast = Toast.makeText(context, "No internet connection. ", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void loadNativeAds(final RelativeLayout rlNativeAd , final int layoutRes , final boolean isSmall){
        if ( AppConstant.IS_ADS_ENABLED && AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null ) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() { if (rlNativeAd != null && AdsSDK.getInstance().getAdsNative().isNativeAdCache() ) {
                    AppApplication.getInstance().getAdsSDK().getAdsNative()
                            .bindAndCacheNativeAd(getNative(rlNativeAd , layoutRes),isSmall);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AdsSDK.getInstance().getAdsNative().cacheNativeAd(true);
                        }
                    } , 100);
                }}
            } , 10);
        }
    }

    private static NativeUnifiedAdsViewHolder getNative(RelativeLayout rlNativeAd, int layoutRes){
        View view = LayoutInflater.from(rlNativeAd.getContext()).inflate(layoutRes, null, false);
        rlNativeAd.removeAllViews();
        rlNativeAd.addView(view);
        return new NativeUnifiedAdsFullViewHolder(rlNativeAd);
    }

    public static void showToastCentre(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    public static boolean isNotConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return !isConnected;
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static void generateGASub(String mainCategory, String sub) {
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("Main category", mainCategory);
        articleParams.put("Main category", sub);
        FlurryAgent.logEvent("Sub Cat Click", articleParams);
    }

    public static void downloadArticle(String id, final onArticleResponse onCustomResponse, final int catId, final boolean isPdf) {
        Map<String, String> map = new HashMap<>(1);
        map.put("id", id);
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.GET_CONTENT_BY_ID, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {

                            try {
                                final ServerBean serverBean = ConfigManager.getGson().fromJson(data, ServerBean.class);
                                if (serverBean != null) {
                                    final List<ServerBean> serverBeen = new ArrayList<>(1);
                                    serverBeen.add(serverBean);
                                    final DbHelper dbHelper = AppApplication.getInstance().getDBObject();
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbHelper.insertArticle(serverBeen, catId, isPdf, false);
                                            if (onCustomResponse != null)
                                                onCustomResponse.onCustomResponse(true, serverBean);
                                            return null;
                                        }
                                    });

                                } else if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                            } catch (JsonSyntaxException e) {
                                if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                                e.printStackTrace();
                            }
                        } else if (onCustomResponse != null)
                            onCustomResponse.onCustomResponse(false, null);

                    }
                });
    }
}
