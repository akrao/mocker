package gk.gkcurrentaffairs.util;

import android.content.Context;
import android.content.SharedPreferences;

import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.network.RetrofitServiceGenerator;

/**
 * Created by Amit on 1/7/2017.
 */

public class AppPreferences {

    public static final String FONT_SIZE = "font_size";
    public static final String ZOOM_SIZE = "zoom_size";
    public static final String DAY_MODE = "dayMode";
    public static final String API_URL_PAYMENT = "api_url_payment";
    public static final String API_URL = "api_url";
    public static final String API_URL_2 = "api_url_2";
    public static final String API_URL_3 = "api_url_3";
    public static final String URL_PDF_OPEN = "pdf_open";
    public static final String URL_PDF_DOWNLOAD = "pdf_download";
    public static final String KEY_TRANS = "trans";
    public static final String PLAY_LEVEL = "play_level-";
    public static final String DESC_OVERLAY = "overlay";
    private static final String CATEGORY_LOADED = "category_loaded";
    private static final String HOSTNAME_FLUSH = "hostname_flush";
    private static final String LOCAL_DATA = "local_data";
    private static final String LOCAL_DATA_LIST = "local_data_list";
    private static final String COINS = "coins";
    private static final String IMP_UPDATES = "imp_update";
    private static final String HOME_RANK = "home_rank_";
    private static final String CAT_DATA = "cat_data_";

    private static SharedPreferences sharedPreferences;

    public static SharedPreferences getSharedPreferenceObj(Context context){
        if ( sharedPreferences == null )
            sharedPreferences = context.getSharedPreferences(context.getPackageName() , Context.MODE_PRIVATE);

        return sharedPreferences ;
    }


    public static String getCatData(Context context , int catId ) {
        return getSharedPreferenceObj(context).getString( CAT_DATA + catId, null );
    }

    public static void setCatData(Context context , int catId , String data) {
        getSharedPreferenceObj(context).edit().putString( CAT_DATA+catId, data).commit();
    }
    public static String getImpUpdates(Context context) {
        return getSharedPreferenceObj(context).getString( IMP_UPDATES, null );
    }

    public static void setImpUpdates(Context context , String impUpdates) {
        getSharedPreferenceObj(context).edit().putString( IMP_UPDATES, impUpdates).commit();
    }

    public static int getHomeRank(Context context , String key) {
        return getSharedPreferenceObj(context).getInt( HOME_RANK + key , 0 );
    }

    public static void setHomeRank(Context context , int position , String key) {
        getSharedPreferenceObj(context).edit().putInt( HOME_RANK + key, position).commit();
    }

    public static int getCOINS(Context context) {
        return getSharedPreferenceObj(context).getInt( COINS , 0);
    }

    public static void setCOINS(Context context , int coins){
        getSharedPreferenceObj(context).edit().putInt( COINS, coins).commit();
    }

    public static int getFontSize(Context context){
        return getSharedPreferenceObj(context).getInt( FONT_SIZE , 17);
    }

    public static void setFontSize(Context context , int index){
        getSharedPreferenceObj(context).edit().putInt( FONT_SIZE, index).commit();
    }

    public static int getZoomSize(Context context){
        return getSharedPreferenceObj(context).getInt( ZOOM_SIZE , 220);
    }

    public static void setZoomSize(Context context , int index){
        getSharedPreferenceObj(context).edit().putInt( ZOOM_SIZE, index).commit();
    }

    public static boolean isDayMode(Context context){
        return getSharedPreferenceObj(context).getBoolean( DAY_MODE, true );
    }

    public static void setDayMode(Context context , boolean flag){
        getSharedPreferenceObj(context).edit().putBoolean( DAY_MODE , flag).commit();
    }

    public static boolean isHostnameFlush(Context context ){
        return getSharedPreferenceObj(context).getBoolean( HOSTNAME_FLUSH + AppConstant.VERSIONING , false );
    }

    public static void setHostnameFlush(Context context , boolean flag){
        getSharedPreferenceObj(context).edit().putBoolean( HOSTNAME_FLUSH+ AppConstant.VERSIONING , flag).commit();
    }

    public static boolean isCategoryLoaded(Context context){
        return getSharedPreferenceObj(context).getBoolean( CATEGORY_LOADED, false );
    }

    public static void setCategoryLoaded(Context context , boolean flag){
        getSharedPreferenceObj(context).edit().putBoolean( CATEGORY_LOADED , flag).commit();
    }

    public static boolean isLOCAL_DATA(Context context){
        return getSharedPreferenceObj(context).getBoolean( LOCAL_DATA, false );
    }

    public static void setLOCAL_DATA(Context context , boolean flag){
        getSharedPreferenceObj(context).edit().putBoolean( LOCAL_DATA , flag).commit();
    }

    public static boolean isLOCAL_DATAList(Context context){
        return getSharedPreferenceObj(context).getBoolean( LOCAL_DATA_LIST, false );
    }

    public static void setLOCAL_DATAList(Context context , boolean flag){
        getSharedPreferenceObj(context).edit().putBoolean( LOCAL_DATA_LIST , flag).commit();
    }

    public static String getBaseUrl(Context context){
        return getSharedPreferenceObj(context).getString( API_URL, AppValues.HOSTNAME );
    }

    public static void setBaseUrl(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( API_URL, reg).commit();
    }

    public static boolean getBoolean(Context context , String key){
        return getSharedPreferenceObj(context).getBoolean( key, false );
    }

    public static void setBoolean(Context context , String key , boolean value){
        getSharedPreferenceObj(context).edit().putBoolean( key, value ).commit();
    }

    public static String getHostPayment(Context context){
        return getSharedPreferenceObj(context).getString( API_URL_PAYMENT, AppValues.HOST_PAYMENT );
    }

    public static void setHostPayment(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( API_URL_PAYMENT, reg).commit();
    }

    public static String getBaseUrl_2(Context context){
        return getSharedPreferenceObj(context).getString( API_URL_2, AppValues.HOSTNAME_2 );
    }

    public static void setBaseUrl_2(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( API_URL_2, reg).commit();
    }

    public static String getBaseUrl_3(Context context){
        return getSharedPreferenceObj(context).getString( API_URL_3, AppValues.HOSTNAME_3 );
    }

    public static void setBaseUrl_3(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( API_URL_3, reg).commit();
    }

    public static String getUrlPdfOpen(Context context){
        return getSharedPreferenceObj(context).getString( URL_PDF_OPEN, AppConstant.URL_PDF_OPEN );
    }

    public static void setUrlPdfOpen(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( URL_PDF_OPEN, reg).commit();
    }

    public static String getUrlPdfDownload(Context context){
        return getSharedPreferenceObj(context).getString( URL_PDF_DOWNLOAD, AppConstant.URL_PDF_DOWNLOAD );
    }

    public static void setUrlPdfDownload(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( URL_PDF_DOWNLOAD, reg).commit();
    }

    public static String getKeyTrans(Context context){
        return getSharedPreferenceObj(context).getString( KEY_TRANS, "trnsl.1.1.20170110T180216Z.c772c6647fe329ac.6ce5ab0ea75909690a90860770565258c66ebb2e");
    }

    public static void setKeyTrans(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( KEY_TRANS, reg).commit();
    }

    public static int getPlayLevel(Context context , int catId){
        return getSharedPreferenceObj(context).getInt( PLAY_LEVEL + catId , 1);
    }

    public static void setPlayLevel(Context context , int level , int catId){
        getSharedPreferenceObj(context).edit().putInt( PLAY_LEVEL + catId, level).commit();
    }

    public static boolean getDescOverlay(Context context){
        return getSharedPreferenceObj(context).getBoolean( DESC_OVERLAY, true);
    }

    public static void setDescOverlay(Context context , boolean level){
        getSharedPreferenceObj(context).edit().putBoolean( DESC_OVERLAY, level).commit();
    }
}
