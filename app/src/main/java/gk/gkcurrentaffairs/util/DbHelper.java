package gk.gkcurrentaffairs.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.util.SparseIntArray;

import com.adssdk.AdsSDK;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.AppValues;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.CatBean;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.bean.HomeBean;
import gk.gkcurrentaffairs.bean.IdBean;
import gk.gkcurrentaffairs.bean.ImpCatListBean;
import gk.gkcurrentaffairs.bean.ListBean;
import gk.gkcurrentaffairs.bean.LocalMockData;
import gk.gkcurrentaffairs.bean.LocalMockList;
import gk.gkcurrentaffairs.bean.MockBean;
import gk.gkcurrentaffairs.bean.MockHomeBean;
import gk.gkcurrentaffairs.bean.MockTestBean;
import gk.gkcurrentaffairs.bean.NotificationListBean;
import gk.gkcurrentaffairs.bean.ResultBean;
import gk.gkcurrentaffairs.bean.ServerBean;
import gk.gkcurrentaffairs.bean.ServerBeanArticle;
import gk.gkcurrentaffairs.bean.SubjectModel;
import gk.gkcurrentaffairs.payment.model.MockInfo;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidTestCat;

public class DbHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "gk.sqlite";
    private static final int DATABASE_VERSION = 15;

    static SQLiteDatabase db;
    Context context;

    public String TABLE_GOVT_ARTICLE = "govt_article_list";
    public String TABLE_MCQ_DATA = "mcq_list";
    public String TABLE_MCQ_LIST = "mcq_test_list";
    public String TABLE_ARTICLE = "article_list";
    private String TABLE_SUB_CAT = "category_list";
    private String TABLE_RESULT = "result";
    private String COLUMN_DESC = "description";
    private String COLUMN_DATE = "date";
    private String COLUMN_APPLY_ONLINE = "read_details";
    private String COLUMN_READ_DETAILS = "apply_online";
    public static String COLUMN_MOCK_TEST_ID = "mock_test_id";
    private String COLUMN_TOTAL_QUE = "total_que";
    private String COLUMN_SKIP_QUE = "skip_que";
    private String COLUMN_CORRECT_ANS = "correct_ans";
    private String COLUMN_WRONG_ANS = "wrong_ans";
    private String COLUMN_TIME_INIT = "time_init";
    private String COLUMN_TIME_FINISH = "time_finish";
    public static final String COLUMN_QUE = "que";
    public static final String COLUMN_ANS = "ans";
    public static final String COLUMN_OPT_A = "opt_a";
    public static final String COLUMN_OPT_B = "opt_b";
    public static final String COLUMN_OPT_C = "opt_c";
    public static final String COLUMN_OPT_D = "opt_d";
    public static final String COLUMN_OPT_E = "opt_e";
    public static final String COLUMN_DIRECTION = "direction";
    public static String COLUMN_TITLE = "title";
    public static String COLUMN_ID = "id";
    public static String COLUMN_CAT_ID = "cat_id";
    public static String COLUMN_SUB_CAT_ID = "sub_cat_id";
    private String COLUMN_SUB_CAT_NAME = "sub_cat_name";
    public static String COLUMN_READ = "is_read";
    public static String COLUMN_FAV = "is_fav";
    public static final int INT_TRUE = 1;
    public static final int INT_FALSE = 0;
    public static final String COLUMN_ATTEMPTED = "is_attempted";
    public static final String COLUMN_DOWNLOADED = "is_downloaded";
    public String COLUMN_INSTRUCTION = "instruction";
    public String COLUMN_TEST_TIME = "test_time";
    public String COLUMN_TEST_MARKS = "test_marks";
    public String COLUMN_QUEST_MARKS = "quest_marks";
    public String COLUMN_NEGATIVE_MARKING = "negative_marking";

    // Amit Groch NCert
    private String TABLE_SUBJECTS = "subjects";
    private String ID = "id";
    private String CLASSID = "class_id";
    private String TITTLE = "title";

    private String TABLE_BOOKS = "books";
    private String PDF = "pdf";
    private String UPDATEAT = "updated_at";
    private String ISDOWNLOAD = "is_dwonload";
    private String SUBJECTID = "subject_id";

    public String TABLE_PAID_MOCK_TEST = "paid_mock_test";
    public String TABLE_PAID_MOCK_CAT = "paid_mock_cat";
    public String TABLE_PAID_MOCK_QUE = "paid_mock_que";
    public String TABLE_PAID_CONTENT = "paid_content";
    public String TABLE_PAID_MOCK_RESULT = "paid_mock_result";
    public String TABLE_PAID_MOCK_RESULT_DATA = "paid_mock_result_data";
    public String TABLE_NOTIFICATION_LIST = "notification_list";

    private String ENG = "eng";
    private String HIN = "hin";
    private String LAST_QUE = "last_que";
    private String LAST_SEC = "last_sec";
    private String MOCK_ID = "mock_id";
    private String SEC_ID = "sec_id";
    private String SEC_TITLE = "sec_title";
    private String TITLE = "title_";
    private String PACKAGE_TITLE = "package_title";
    private String PACKAGE_ID = "package_id";
    private String SECTION_COUNT = "section_count";
    private String DESCRIPTION = "description_";
    private String OPT_QUE = "option_question_";
    private String OPT_ANS = "option_answer_";
    private String ANS_DESC = "answer_description_";
    private String IMAGE = "image";
    private String OPTION_1 = "option1_";
    private String OPTION_2 = "option2_";
    private String OPTION_3 = "option3_";
    private String OPTION_4 = "option4_";
    private String OPTION_5 = "option5_";
    private String INSTRUCTION = "instructions";
    private String NAME_INTERNAL_USE = "name_internal_use";
    private String NUMBER_OF_QUE = "no_of_questions";
    private String TOTAL_TIME = "total_time";
    private String TEST_MARKS = "test_marks";
    private String QUEST_MARKS = "quest_marks";
    private String NEGATIVE_MARKING = "negative_marking";
    public String CAT_TYPE = "cat_type";
    public String FIRST_VISIT = "first_view";
    public String LAST_VISIT = "last_visit";
    public String TAKEN_TIME = "taken_time";
    public String STATUS = "status";
    public String RESULT_ID = "result_id";
    public String ACTUAL_ANS = "actual_ans";
    private String IS_PRACTICE_TEST = "is_practice_test";
    public String IS_SYNC = "is_sync";
    private String IS_MARK_REVIEW = "is_mark_review";
    private String NOTIFICATION_ID = "notification_id";

    private String CREATE_TABLE_NOTIFICATION_LIST = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATION_LIST + " (" +
            "    id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    notification_id          INTEGER ," +
            "    title       VARCHAR," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    date        VARCHAR," +
            "    description        VARCHAR" +
            ");";
    private String CREATE_TABLE_PAID_CONTENT = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_CONTENT + " (" +
            "    id          INTEGER ," +
            "    type       INTEGER" +
            ");";
    private String CREATE_TABLE_PAID_MOCK_RESULT = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_MOCK_RESULT + " (" +
            "    id INTEGER PRIMARY KEY," +
            "    title       VARCHAR," +
            "    package_title       VARCHAR," +
            "    package_id       VARCHAR," +
            "    status INTEGER ," +
            "    section_count INTEGER ," +
            "    last_que INTEGER ," +
            "    last_sec INTEGER ," +
            "    is_practice_test INTEGER DEFAULT " + INT_TRUE + "," +
            "    is_sync INTEGER DEFAULT " + INT_FALSE + "," +
            "    mock_id INTEGER ," +
            "    first_view        VARCHAR," +
            "    taken_time        VARCHAR," +
            "    last_visit        VARCHAR" +
            ");";

    private String CREATE_TABLE_PAID_MOCK_RESULT_DATA = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_MOCK_RESULT_DATA + " (" +
            "    id INTEGER ," +
            "    first_view VARCHAR," +
            "    last_visit VARCHAR," +
            "    taken_time VARCHAR," +
            "    status INTEGER ," +
            "    actual_ans INTEGER ," +
            "    mock_id INTEGER ," +
            "    result_id INTEGER ," +
            "    sec_id  INTEGER," +
            "    is_mark_review INTEGER DEFAULT " + INT_FALSE + "," +
            "    sec_title  VARCHAR" +
            ");";

    private String CREATE_TABLE_PAID_MOCK_TEST = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_MOCK_TEST + " (" +
            "    id          INTEGER PRIMARY KEY," +
            "    title       VARCHAR," +
            "    description VARCHAR," +
            "    no_of_questions     INTEGER ," +
            "    total_time      INTEGER ," +
            "    sec_id  INTEGER," +
            "    date        VARCHAR" +
            ");";

    private String CREATE_TABLE_PAID_MOCK_CAT = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_MOCK_CAT + " (" +
            "    id          INTEGER ," +
            "    mock_id      INTEGER," +
            "    title       VARCHAR," +
            "    instructions VARCHAR," +
            "    name_internal_use VARCHAR ," +
            "    no_of_questions INTEGER ," +
            "    total_time INTEGER ," +
            "    test_marks      INTEGER," +
            "    quest_marks      INTEGER," +
            "    negative_marking VARCHAR," +
            "    cat_type      INTEGER," +
            "    date        VARCHAR" +
            ");";

    private String CREATE_TABLE_PAID_MOCK_QUE = "CREATE TABLE IF NOT EXISTS " + TABLE_PAID_MOCK_QUE + " (" +
            "    id          INTEGER ," +
            "    cat_id      INTEGER," +
            "    mock_id      INTEGER," +
            "    sec_id      INTEGER," +
            "    title_eng       VARCHAR," +
            "    title_hin       VARCHAR," +
            "    description_eng VARCHAR," +
            "    description_hin VARCHAR," +
            "    option_question_eng VARCHAR," +
            "    option_question_hin VARCHAR," +
            "    option1_eng VARCHAR," +
            "    option1_hin VARCHAR," +
            "    option2_eng VARCHAR," +
            "    option2_hin VARCHAR," +
            "    option3_eng VARCHAR," +
            "    option3_hin VARCHAR," +
            "    option4_eng VARCHAR," +
            "    option4_hin VARCHAR," +
            "    option5_eng VARCHAR," +
            "    option5_hin VARCHAR," +
            "    option_answer_eng  INTEGER," +
            "    option_answer_hin  INTEGER," +
            "    answer_description_eng  VARCHAR," +
            "    answer_description_hin  VARCHAR," +
            "    image VARCHAR" +
            ");";

    private String CREATE_TABLE_ARTICLE_LIST = "CREATE TABLE IF NOT EXISTS article_list (" +
            "    id          INTEGER PRIMARY KEY," +
            "    title       VARCHAR," +
            "    description VARCHAR," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    is_fav      INTEGER DEFAULT (0)," +
            "    sub_cat_id  INTEGER," +
            "    cat_id      INTEGER," +
            "    date        VARCHAR" +
            ");";


    private String CREATE_TABLE_CATEGORY_LIST = "CREATE TABLE IF NOT EXISTS category_list (" +
            "    cat_id       INTEGER," +
            "    sub_cat_id   INTEGER," +
            "    sub_cat_name VARCHAR," +
            "    order_id     INTEGER" +
            ");";
    private String CREATE_TABLE_GOVT_ARTICLE_LIST = "CREATE TABLE IF NOT EXISTS govt_article_list (" +
            "    id           INTEGER PRIMARY KEY," +
            "    title        VARCHAR," +
            "    description  VARCHAR," +
            "    read_details VARCHAR," +
            "    apply_online VARCHAR," +
            "    is_read      INTEGER DEFAULT (0)," +
            "    is_fav       INTEGER DEFAULT (0)," +
            "    sub_cat_id   INTEGER," +
            "    cat_id       INTEGER," +
            "    date         VARCHAR" +
            ");";
    private String CREATE_TABLE_MCQ_LIST = "CREATE TABLE IF NOT EXISTS mcq_list (" +
            "    id           INTEGER," +
            "    description  VARCHAR," +
            "    direction    VARCHAR," +
            "    que          VARCHAR," +
            "    opt_a        VARCHAR," +
            "    opt_b        VARCHAR," +
            "    opt_c        VARCHAR," +
            "    opt_d        VARCHAR," +
            "    opt_e        VARCHAR," +
            "    ans          VARCHAR," +
            "    mock_test_id INTEGER," +
            "    cat_id       INTEGER," +
            "    sub_cat_id   INTEGER" +
            ");";
    private String CREATE_TABLE_MCQ_TEST_LIST = "CREATE TABLE IF NOT EXISTS mcq_test_list (" +
            "    id            INTEGER," +
            "    sub_cat_id    INTEGER," +
            "    title         VARCHAR," +
            "    is_downloaded INTEGER DEFAULT (0)," +
            "    is_attempted  INTEGER DEFAULT (0)," +
            "    is_sync       INTEGER DEFAULT (0)," +
            "    instruction   VARCHAR," +
            "    test_time     VARCHAR," +
            "    test_marks    VARCHAR," +
            "    quest_marks   VARCHAR," +
            "    negative_marking VARCHAR," +
            "    cat_id        INTEGER" +
            ");";
    private String CREATE_TABLE_RESULT = "CREATE TABLE IF NOT EXISTS result (" +
            "    id           INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    title        VARCHAR," +
            "    data         VARCHAR," +
            "    cat_id       INTEGER," +
            "    total_que    INTEGER," +
            "    skip_que     INTEGER," +
            "    correct_ans  INTEGER," +
            "    wrong_ans    INTEGER," +
            "    time_init    VARCHAR," +
            "    time_finish  VARCHAR DEFAULT (0)," +
            "    mock_test_id INTEGER" +
            ");";

    private String CREATE_TABLE_BOOKS = "CREATE TABLE IF NOT EXISTS books (" +
            "    id          INTEGER PRIMARY KEY" +
            "                        NOT NULL," +
            "    subject_id  INTEGER," +
            "    title       VARCHAR," +
            "    pdf         VARCHAR," +
            "    updated_at  VARCHAR," +
            "    is_dwonload INTEGER DEFAULT 0," +
            "    status      INTEGER DEFAULT 0" +
            ");";

    private String CREATE_TABLE_SUBJECT = "CREATE TABLE IF NOT EXISTS subjects (" +
            "    id       INTEGER PRIMARY KEY" +
            "                     NOT NULL," +
            "    class_id INTEGER," +
            "    title    VARCHAR DEFAULT (NULL)," +
            "    status   INTEGER DEFAULT (0) " +
            ");";

    private DbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private static DbHelper instance;

    public static DbHelper getInstance(Context context) {
        if (instance == null)
            instance = new DbHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ARTICLE_LIST);
        db.execSQL(CREATE_TABLE_CATEGORY_LIST);
        db.execSQL(CREATE_TABLE_GOVT_ARTICLE_LIST);
        db.execSQL(CREATE_TABLE_MCQ_LIST);
        db.execSQL(CREATE_TABLE_MCQ_TEST_LIST);
        db.execSQL(CREATE_TABLE_RESULT);
        db.execSQL(CREATE_TABLE_BOOKS);
        db.execSQL(CREATE_TABLE_SUBJECT);
        db.execSQL(CREATE_TABLE_PAID_MOCK_TEST);
        db.execSQL(CREATE_TABLE_PAID_MOCK_CAT);
        db.execSQL(CREATE_TABLE_PAID_MOCK_QUE);
        db.execSQL(CREATE_TABLE_PAID_CONTENT);
        db.execSQL(CREATE_TABLE_PAID_MOCK_RESULT);
        db.execSQL(CREATE_TABLE_PAID_MOCK_RESULT_DATA);
        db.execSQL(CREATE_TABLE_NOTIFICATION_LIST);
        handleVersionColumn(db);
    }

    private void handleVersionColumn(SQLiteDatabase db) {
        Cursor dbCursor = db.query(TABLE_MCQ_DATA, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();
        boolean isTableDrop = true;
        for (String s : columnNames) {
            if (s.equalsIgnoreCase(COLUMN_OPT_E))
                isTableDrop = false;
        }
        if (isTableDrop) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MCQ_DATA);
            db.execSQL(CREATE_TABLE_MCQ_LIST);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        switch (oldVersion) {
            case 7:
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_MCQ_DATA);
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_MCQ_LIST);

                db.execSQL(CREATE_TABLE_ARTICLE_LIST);
                db.execSQL(CREATE_TABLE_CATEGORY_LIST);
                db.execSQL(CREATE_TABLE_GOVT_ARTICLE_LIST);
                db.execSQL(CREATE_TABLE_MCQ_LIST);
                db.execSQL(CREATE_TABLE_MCQ_TEST_LIST);
                db.execSQL(CREATE_TABLE_RESULT);
                db.execSQL(CREATE_TABLE_BOOKS);
                db.execSQL(CREATE_TABLE_SUBJECT);
                handleVersionColumn(db);
            case 8:
                db.execSQL(CREATE_TABLE_PAID_MOCK_TEST);
                db.execSQL(CREATE_TABLE_PAID_MOCK_CAT);
                db.execSQL(CREATE_TABLE_PAID_MOCK_QUE);
                db.execSQL(CREATE_TABLE_PAID_CONTENT);
                db.execSQL(CREATE_TABLE_PAID_MOCK_RESULT);
                db.execSQL(CREATE_TABLE_PAID_MOCK_RESULT_DATA);
            case 9:
                db.execSQL("ALTER TABLE result ADD COLUMN data VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN is_sync INTEGER DEFAULT (0)");
            case 10:
            case 11:
                db.execSQL(CREATE_TABLE_BOOKS);
                Logger.e("CREATE_TABLE_BOOKS called");
                db.execSQL(CREATE_TABLE_SUBJECT);
                Logger.e("CREATE_TABLE_SUBJECT called");
            case 12:
                db.execSQL(CREATE_TABLE_NOTIFICATION_LIST);
            case 13:
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAID_MOCK_RESULT);
                db.execSQL(CREATE_TABLE_PAID_MOCK_RESULT);
            case 14:
                db.execSQL("ALTER TABLE mcq_list ADD COLUMN direction VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN instruction VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN test_time VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN test_marks VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN quest_marks VARCHAR");
                db.execSQL("ALTER TABLE mcq_test_list ADD COLUMN negative_marking VARCHAR");
                break;
        }
    }

    private SQLiteDatabase openDataBase() {
        SQLiteDatabase database = getWritableDatabase();
        if (database == null)
            instance = new DbHelper(AppApplication.getInstance());
        database = getWritableDatabase();
        return database;
    }

    public void callDBFunction(Callable<Void> function) {
        try {
            db = getDB();
            db.beginTransaction();
            function.call();
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            db = getDB();
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    private SQLiteDatabase getDB() {
        SQLiteDatabase database = getWritableDatabase();
        if (database == null)
            instance = new DbHelper(AppApplication.getInstance());
        return database;
    }

    public void deletePaidMock(int id) {
        try {
            String where = ID + "=" + id;
            try {
                db.delete(TABLE_PAID_MOCK_TEST, where, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            where = MOCK_ID + "=" + id;
            try {
                db.delete(TABLE_PAID_MOCK_CAT, where, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            where = MOCK_ID + "=" + id;
            try {
                db.delete(TABLE_PAID_MOCK_QUE, where, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPaidMockDownloaded(int id) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_TEST, null, ID + "=" + id, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            PaidMockTest paidMockTest = fetchPaidMock(id);
            boolean isDownload = (paidMockTest != null && paidMockTest.getCat1() != null && paidMockTest.getNoOfQuestions() > 0);
            paidMockTest = null ;
            return isDownload ;
        }
        return false;
    }

    public void fetchPaidResult(List<PaidResult> list, boolean isGlobal) {
        String where = IS_PRACTICE_TEST + "=" + (isGlobal ? AppConstant.INT_FALSE : AppConstant.INT_TRUE);
        where += " AND " + STATUS + "=" + AppConstant.INT_TRUE;
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, where, null, null, null, ID + " DESC");
        parsePaidResult(list, cursor);
    }

    public void fetchPaidTestResume(List<PaidResult> list) {
        String where = STATUS + "=" + AppConstant.INT_FALSE;
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, where, null, null, null, ID + " DESC");
        parsePaidResult(list, cursor);
    }

    public void fetchPaidSolutionList(List<PaidResult> list) {
        String where = STATUS + "=" + AppConstant.INT_TRUE + " AND " + IS_PRACTICE_TEST + "=" + AppConstant.INT_FALSE;
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, where, null, null, null, ID + " DESC");
        parsePaidResult(list, cursor);
    }

    private void parsePaidResult(List<PaidResult> list, Cursor cursor) {
        List<PaidResult> resultList = null;
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            resultList = new ArrayList<>(cursor.getCount());
            PaidResult result;
            do {
                result = new PaidResult();
                result.setTitle(cursor.getString(cursor.getColumnIndex(TITTLE)));
                result.setPackageTitle(cursor.getString(cursor.getColumnIndex(PACKAGE_TITLE)));
                result.setPackageId(cursor.getInt(cursor.getColumnIndex(PACKAGE_ID)));
                result.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                result.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS)));
                result.setPracticeTest(cursor.getInt(cursor.getColumnIndex(IS_PRACTICE_TEST)) == INT_TRUE);
                result.setMockId(cursor.getInt(cursor.getColumnIndex(MOCK_ID)));
                result.setSectionCount(cursor.getInt(cursor.getColumnIndex(SECTION_COUNT)));
                result.setStartTimeStamp(Long.parseLong(cursor.getString(cursor.getColumnIndex(FIRST_VISIT))));
                result.setEndTimeStamp(Long.parseLong(cursor.getString(cursor.getColumnIndex(LAST_VISIT))));
                result.setTimeTaken(Long.parseLong(cursor.getString(cursor.getColumnIndex(TAKEN_TIME))));
                result.setDate(getDate(result.getStartTimeStamp()));
                resultList.add(result);
            } while (cursor.moveToNext());
        }

        list.clear();
        if (resultList != null && resultList.size() > 0) {
            list.addAll(resultList);
        }
    }

    public void updateDownloadedTestList(List<MockInfo> mockInfos) {
        int l = mockInfos.size();
        for (int i = 0; i < l; i++) {
            boolean isDownloaded = isPaidMockDownloaded(mockInfos.get(i).getId());
            mockInfos.get(i).setDownloaded(isDownloaded);
        }
    }

    public PaidResult fetchPaidResult(int mockId, boolean isGlobalResultData) {
        String query = MOCK_ID + "=" + mockId;
        if (isGlobalResultData)
            query += " AND " + IS_PRACTICE_TEST + "=" + INT_FALSE;
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, query, null, null, null, ID + " DESC");
        return getPaidResultParse(cursor);
    }

    public PaidResult fetchPaidResumeResult(int mockId) {
        String query = MOCK_ID + "=" + mockId;
        query += " AND " + STATUS + "=" + INT_FALSE;
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, query, null, null, null, ID + " DESC");
        return getPaidResultParse(cursor);
    }

    private PaidResult getPaidResultParse(Cursor cursor) {
        PaidResult result = null;
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            result = new PaidResult();
            result.setTitle(cursor.getString(cursor.getColumnIndex(TITTLE)));
            result.setPackageTitle(cursor.getString(cursor.getColumnIndex(PACKAGE_TITLE)));
            result.setPackageId(cursor.getInt(cursor.getColumnIndex(PACKAGE_ID)));
            result.setId(cursor.getInt(cursor.getColumnIndex(ID)));
            result.setPracticeTest(cursor.getInt(cursor.getColumnIndex(IS_PRACTICE_TEST)) == INT_TRUE);
            result.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS)));
            result.setMockId(cursor.getInt(cursor.getColumnIndex(MOCK_ID)));
            result.setLastSectionPos(cursor.getInt(cursor.getColumnIndex(LAST_SEC)));
            result.setLastQuePos(cursor.getInt(cursor.getColumnIndex(LAST_QUE)));
            result.setSectionCount(cursor.getInt(cursor.getColumnIndex(SECTION_COUNT)));
            result.setStartTimeStamp(Long.parseLong(cursor.getString(cursor.getColumnIndex(FIRST_VISIT))));
            result.setEndTimeStamp(Long.parseLong(cursor.getString(cursor.getColumnIndex(LAST_VISIT))));
            result.setTimeTaken(Long.parseLong(cursor.getString(cursor.getColumnIndex(TAKEN_TIME))));
            result.setDate(getDate(result.getStartTimeStamp()));
        }
        return result;
    }

    public PaidResult updatePaidResult(PaidResult result, PaidMockTest paidMockTest) {
        result.setPaidMockTestResults(new ArrayList<List<PaidMockTestResult>>(result.getSectionCount()));
        result.setTestCats(new ArrayList<PaidTestCat>(result.getSectionCount()));
        insertListForResult(result, paidMockTest.getCat1());
        insertListForResult(result, paidMockTest.getCat2());
        insertListForResult(result, paidMockTest.getCat3());
        insertListForResult(result, paidMockTest.getCat4());
        insertListForResult(result, paidMockTest.getCat5());
        return result;
    }

    private void insertListForResult(PaidResult result, PaidTestCat cat) {
        if (cat != null) {
            try {
                PaidTestCat paidTestCat = cat.getClone();
                paidTestCat.setPaidQuestions(null);
                result.getTestCats().add(paidTestCat);
                result.getPaidMockTestResults().add(getPaidMockTest(result, paidTestCat));
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }

    public PaidTestCat insertQueListForResult(int catId, int mockId) {
        PaidMockTest paidMockTest = new PaidMockTest();
        paidMockTest.setId(mockId);
        return getPaidTestCat(paidMockTest, catId);
    }

    private PaidTestCat getPaidTestCat(PaidMockTest paidMockTest, int catId) {
        PaidTestCat paidTestCat = null;
        Cursor cursor = db.query(TABLE_PAID_MOCK_CAT, null, MOCK_ID + "=" + paidMockTest.getId() + " AND " + ID + "=" + catId, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            paidTestCat = parsePaidTestCat(cursor, paidMockTest);
        }
        return paidTestCat;
    }

    private List<PaidMockTestResult> getPaidMockTest(PaidResult result, PaidTestCat cat) {
        List<PaidMockTestResult> list = null;
        String where = RESULT_ID + "=" + result.getId() + " AND " + SEC_ID + "=" + cat.getId();
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT_DATA, null, where, null, null, null, ID + " ASC");
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            list = new ArrayList<>(cursor.getCount());
            PaidMockTestResult mockTestResult;
            do {
                mockTestResult = new PaidMockTestResult(cursor.getInt(cursor.getColumnIndex(ID)), cursor.getInt(cursor.getColumnIndex(ACTUAL_ANS)));
                mockTestResult.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS)));
                mockTestResult.setMockId(cursor.getInt(cursor.getColumnIndex(MOCK_ID)));
                mockTestResult.setMarkReview(cursor.getInt(cursor.getColumnIndex(IS_MARK_REVIEW)) == INT_TRUE);
                mockTestResult.setFirstView(Double.parseDouble(cursor.getString(cursor.getColumnIndex(FIRST_VISIT))));
                mockTestResult.setLastVisit(Double.parseDouble(cursor.getString(cursor.getColumnIndex(LAST_VISIT))));
                mockTestResult.setTimeTaken(Double.parseDouble(cursor.getString(cursor.getColumnIndex(TAKEN_TIME))));
                list.add(mockTestResult);
            } while (cursor.moveToNext());

        }
        return list;
    }

    public void insertPaidResult(PaidResult result) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITTLE, result.getTitle());
        contentValues.put(PACKAGE_TITLE, result.getPackageTitle());
        contentValues.put(PACKAGE_ID, result.getPackageId());
        contentValues.put(STATUS, result.getStatus());
        contentValues.put(MOCK_ID, result.getMockId());
        contentValues.put(FIRST_VISIT, result.getStartTimeStamp());
        contentValues.put(LAST_VISIT, result.getEndTimeStamp());
        contentValues.put(TAKEN_TIME, result.getTimeTaken());
        contentValues.put(SECTION_COUNT, result.getSectionCount());
        contentValues.put(LAST_QUE, result.getLastQuePos());
        contentValues.put(LAST_SEC, result.getLastSectionPos());
        contentValues.put(IS_PRACTICE_TEST, result.isPracticeTest() ? INT_TRUE : INT_FALSE);
        try {

            if (result.getId() > 0) {
                int i = db.delete(TABLE_PAID_MOCK_RESULT, ID + "=" + result.getId(), null);
                int j = db.delete(TABLE_PAID_MOCK_RESULT_DATA, RESULT_ID + "=" + result.getId(), null);
                Logger.e("insertPaidResult", "Delete : TABLE_PAID_MOCK_RESULT : " + i);
                Logger.e("insertPaidResult", "Delete : TABLE_PAID_MOCK_RESULT_DATA : " + j);
            }
            long id = db.insertOrThrow(TABLE_PAID_MOCK_RESULT, null, contentValues);
            for (int i = 0; i < result.getTestCats().size(); i++) {
                List<PaidMockTestResult> list = result.getPaidMockTestResults().get(i);
                for (PaidMockTestResult mockTestResult : list) {
                    insertPaidResultRow(mockTestResult, (int) id, result.getTestCats().get(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return;
    }

    public boolean isPaidGlobalResultPending(int mockId) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, MOCK_ID + "=" + mockId
                + " AND " + IS_PRACTICE_TEST + "=" + INT_FALSE + " AND " + STATUS + "=" + INT_FALSE, null, null, null, null);
        return (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst());
    }

    public boolean isPaidResultPending(int mockId) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, MOCK_ID + "=" + mockId
                + " AND " + STATUS + "=" + INT_FALSE, null, null, null, null);
        return (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst());
    }

    public boolean isPaidGlobalResult(int mockId) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_RESULT, null, MOCK_ID + "=" + mockId, null, null, null, null);
        return (cursor != null && cursor.getCount() < 1);
    }

    public void resultIsSynced(int resultId) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(IS_SYNC, INT_TRUE);
        db.update(TABLE_PAID_MOCK_RESULT, contentValues, ID + "=" + resultId, null);
    }

    public void insertNotification(int id, String title, String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOTIFICATION_ID, id);
        contentValues.put(COLUMN_TITLE, title);
        contentValues.put(COLUMN_DESC, data);
        contentValues.put(COLUMN_DATE, System.currentTimeMillis());

        try {
            long l = db.insertOrThrow(TABLE_NOTIFICATION_LIST, null, contentValues);
            Logger.e("TABLE_NOTIFICATION_LIST : insertOrThrow -- " + l);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteNotification(int id) {
        try {
            db.delete(TABLE_NOTIFICATION_LIST, ID + "=" + id, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateNotificationRead(String title, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_READ, INT_TRUE);
        String where = NOTIFICATION_ID + "=" + id + " AND " + COLUMN_TITLE + "='" + title + "'";
        try {
            int i = db.update(TABLE_NOTIFICATION_LIST, contentValues, where, null);
            Logger.e("updateNotification Read i -- " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchNotificationList(List<NotificationListBean> been) {
        Cursor cursor = db.query(TABLE_NOTIFICATION_LIST, null, null, null, null, null, COLUMN_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            been.clear();
            NotificationListBean bean;
            do {
                bean = new NotificationListBean();
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setData(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setNotificationId(cursor.getInt(cursor.getColumnIndex(NOTIFICATION_ID)));
                bean.setDate(getDate(Long.parseLong(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)))));
                bean.setClicked(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                been.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();

    }

    private void insertPaidResultRow(PaidMockTestResult mockTestResult, int resultId, PaidTestCat paidTestCat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, mockTestResult.getId());
        contentValues.put(STATUS, mockTestResult.getStatus());
        contentValues.put(MOCK_ID, paidTestCat.getParentId());
        contentValues.put(RESULT_ID, resultId);
        contentValues.put(SEC_ID, paidTestCat.getId());
        contentValues.put(SEC_TITLE, paidTestCat.getTitle());
        contentValues.put(FIRST_VISIT, mockTestResult.getFirstView());
        contentValues.put(TAKEN_TIME, mockTestResult.getTimeTaken());
        contentValues.put(LAST_VISIT, mockTestResult.getLastVisit());
        contentValues.put(ACTUAL_ANS, mockTestResult.getActualAns());
        contentValues.put(IS_MARK_REVIEW, mockTestResult.isMarkReview() ? INT_TRUE : INT_FALSE);

        try {
            db.insertOrThrow(TABLE_PAID_MOCK_RESULT_DATA, null, contentValues);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PaidMockTest fetchPaidMock(int id) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_TEST, null, ID + "=" + id, null, null, null, null);
        PaidMockTest paidMockTest = null;
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            paidMockTest = new PaidMockTest();
            paidMockTest.setId(cursor.getInt(cursor.getColumnIndex(ID)));
            paidMockTest.setSecId(cursor.getInt(cursor.getColumnIndex(SEC_ID)));
            paidMockTest.setTestTime(cursor.getInt(cursor.getColumnIndex(TOTAL_TIME)));
            paidMockTest.setNoOfQuestions(cursor.getInt(cursor.getColumnIndex(NUMBER_OF_QUE)));
            paidMockTest.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
            paidMockTest.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
            paidMockTest.setReleaseDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
            fillMockTestCat(paidMockTest);
        }
        return paidMockTest;
    }

    private void fillMockTestCat(PaidMockTest paidMockTest) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_CAT, null, MOCK_ID + "=" + paidMockTest.getId(), null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            int i = 0;
            do {
                setMockTestCat(paidMockTest, parsePaidTestCat(cursor, paidMockTest), i);
                i++;
            } while (cursor.moveToNext());
        }
    }

    private PaidTestCat parsePaidTestCat(Cursor cursor, PaidMockTest paidMockTest) {
        PaidTestCat paidTestCat = new PaidTestCat();
        paidTestCat.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        paidTestCat.setParentId(cursor.getInt(cursor.getColumnIndex(MOCK_ID)));
        paidTestCat.setNoOfQuestions(cursor.getInt(cursor.getColumnIndex(NUMBER_OF_QUE)));
        paidTestCat.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
        paidTestCat.setNameInternalUse(cursor.getString(cursor.getColumnIndex(NAME_INTERNAL_USE)));
        paidTestCat.setInstructions(cursor.getString(cursor.getColumnIndex(INSTRUCTION)));
        paidTestCat.setCreatedAt(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
        paidTestCat.setTestMarkes(cursor.getInt(cursor.getColumnIndex(TEST_MARKS)));
        paidTestCat.setQuestMarks(cursor.getInt(cursor.getColumnIndex(QUEST_MARKS)));
        paidTestCat.setCategoryType(cursor.getInt(cursor.getColumnIndex(CAT_TYPE)));
        paidTestCat.setNegetiveMarking(Double.parseDouble(cursor.getString(cursor.getColumnIndex(NEGATIVE_MARKING))));
        fetchQuestion(paidMockTest, paidTestCat);
        return paidTestCat;
    }

    private void fetchQuestion(PaidMockTest paidMockTest, PaidTestCat paidTestCat) {
        Cursor cursor = db.query(TABLE_PAID_MOCK_QUE, null, MOCK_ID + "=" + paidMockTest.getId() + " AND " + SEC_ID + "=" + paidTestCat.getId()
                , null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            PaidQuestion paidQuestion;
            List<PaidQuestion> paidQuestions = new ArrayList<>(cursor.getCount());
            do {
                paidQuestion = new PaidQuestion();
                paidQuestion.setId(cursor.getInt(cursor.getColumnIndex(ID)));

                String post = ENG;
                paidQuestion.setTitleEng(cursor.getString(cursor.getColumnIndex(TITLE + post)));
                paidQuestion.setDescriptionEng(cursor.getString(cursor.getColumnIndex(DESCRIPTION + post)));
                paidQuestion.setOptionQuestionEng(cursor.getString(cursor.getColumnIndex(OPT_QUE + post)));
                paidQuestion.setOptionAnswerEng(cursor.getInt(cursor.getColumnIndex(OPT_ANS + post)));
                paidQuestion.setAnswerDescriptionEng(cursor.getString(cursor.getColumnIndex(ANS_DESC + post)));
                paidQuestion.setOption1Eng(cursor.getString(cursor.getColumnIndex(OPTION_1 + post)));
                paidQuestion.setOption2Eng(cursor.getString(cursor.getColumnIndex(OPTION_2 + post)));
                paidQuestion.setOption3Eng(cursor.getString(cursor.getColumnIndex(OPTION_3 + post)));
                paidQuestion.setOption4Eng(cursor.getString(cursor.getColumnIndex(OPTION_4 + post)));
                paidQuestion.setOption5Eng(cursor.getString(cursor.getColumnIndex(OPTION_5 + post)));

                post = HIN;
                paidQuestion.setTitleHin(cursor.getString(cursor.getColumnIndex(TITLE + post)));
                paidQuestion.setDescriptionHin(cursor.getString(cursor.getColumnIndex(DESCRIPTION + post)));
                paidQuestion.setOptionQuestionHin(cursor.getString(cursor.getColumnIndex(OPT_QUE + post)));
                paidQuestion.setOptionAnswerHin(cursor.getInt(cursor.getColumnIndex(OPT_ANS + post)));
                paidQuestion.setAnswerDescriptionHin(cursor.getString(cursor.getColumnIndex(ANS_DESC + post)));
                paidQuestion.setOption1Hin(cursor.getString(cursor.getColumnIndex(OPTION_1 + post)));
                paidQuestion.setOption2Hin(cursor.getString(cursor.getColumnIndex(OPTION_2 + post)));
                paidQuestion.setOption3Hin(cursor.getString(cursor.getColumnIndex(OPTION_3 + post)));
                paidQuestion.setOption4Hin(cursor.getString(cursor.getColumnIndex(OPTION_4 + post)));
                paidQuestion.setOption5Hin(cursor.getString(cursor.getColumnIndex(OPTION_5 + post)));

                paidQuestions.add(paidQuestion);
            } while (cursor.moveToNext());
            paidTestCat.setPaidQuestions(paidQuestions);
        }
    }

    private void setMockTestCat(PaidMockTest paidMockTest, PaidTestCat paidTestCat, int position) {
        switch (position) {
            case 0:
                paidMockTest.setCat1(paidTestCat);
                break;
            case 1:
                paidMockTest.setCat2(paidTestCat);
                break;
            case 2:
                paidMockTest.setCat3(paidTestCat);
                break;
            case 3:
                paidMockTest.setCat4(paidTestCat);
                break;
            case 4:
                paidMockTest.setCat5(paidTestCat);
                break;
        }
    }

    public void insertPaidMock(PaidMockTest paidMockTest) {
        String secId = "";
        try {
            secId = insertPaidMockCat(paidMockTest.getCat1(), paidMockTest.getId(), secId, paidMockTest);
            secId = insertPaidMockCat(paidMockTest.getCat2(), paidMockTest.getId(), secId, paidMockTest);
            secId = insertPaidMockCat(paidMockTest.getCat3(), paidMockTest.getId(), secId, paidMockTest);
            secId = insertPaidMockCat(paidMockTest.getCat4(), paidMockTest.getId(), secId, paidMockTest);
            secId = insertPaidMockCat(paidMockTest.getCat5(), paidMockTest.getId(), secId, paidMockTest);
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, paidMockTest.getId());
            contentValues.put(SEC_ID, secId);
            contentValues.put(COLUMN_TITLE, paidMockTest.getTitle());
            contentValues.put(COLUMN_DESC, paidMockTest.getDescription());
            contentValues.put(COLUMN_DATE, paidMockTest.getReleaseDate());
            contentValues.put(NUMBER_OF_QUE, paidMockTest.getNoOfQuestions());
            contentValues.put(TOTAL_TIME, paidMockTest.getTestTime());
            try {
                long l = db.insertOrThrow(TABLE_PAID_MOCK_TEST, null, contentValues);
                Logger.e("insertPaidMock " + l);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String insertPaidMockCat(PaidTestCat paidTestCat, int mockId, String secId, PaidMockTest paidMockTest) throws Exception {
        if (paidTestCat != null && paidTestCat.getPaidQuestions() != null && paidTestCat.getPaidQuestions().size() > 0) {
            for (PaidQuestion paidQuestion : paidTestCat.getPaidQuestions()) {
                insertPaidMockQue(paidQuestion, mockId, paidTestCat.getId());
            }
            if (!SupportUtil.isEmptyOrNull(secId)) {
                secId += ",";
            }
            secId += paidTestCat.getId();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, paidTestCat.getId());
            contentValues.put(MOCK_ID, mockId);
            contentValues.put(COLUMN_TITLE, paidTestCat.getTitle());
            contentValues.put(INSTRUCTION, paidTestCat.getInstructions());
            contentValues.put(NAME_INTERNAL_USE, paidTestCat.getNameInternalUse());
            contentValues.put(NUMBER_OF_QUE, paidTestCat.getPaidQuestions().size());
            contentValues.put(TEST_MARKS, paidTestCat.getTestMarkes());
            contentValues.put(QUEST_MARKS, paidTestCat.getQuestMarks());
            contentValues.put(NEGATIVE_MARKING, paidTestCat.getNegetiveMarking());
            contentValues.put(CAT_TYPE, paidTestCat.getCategoryType());
            contentValues.put(COLUMN_DATE, paidTestCat.getCreatedAt());
            paidMockTest.setTestTime(paidMockTest.getTestTime() + paidTestCat.getTestTime());
            try {
                long l = db.insertOrThrow(TABLE_PAID_MOCK_CAT, null, contentValues);
                Logger.e("insertPaidMockCat " + l);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return secId;
    }

    private void insertPaidMockQue(PaidQuestion paidQuestion, int mockId, int secId) throws Exception {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, paidQuestion.getId());
        contentValues.put(COLUMN_CAT_ID, paidQuestion.getCategoryId());
        contentValues.put(MOCK_ID, mockId);
        contentValues.put(SEC_ID, secId);
        contentValues.put(IMAGE, paidQuestion.getImage());

        String post = ENG;
        contentValues.put(TITLE + post, paidQuestion.getTitleEng());
        contentValues.put(DESCRIPTION + post, paidQuestion.getDescriptionEng());
        contentValues.put(OPT_QUE + post, paidQuestion.getOptionQuestionEng());
        contentValues.put(OPT_ANS + post, paidQuestion.getOptionAnswerEng());
        contentValues.put(ANS_DESC + post, paidQuestion.getAnswerDescriptionEng());
        contentValues.put(OPTION_1 + post, paidQuestion.getOption1Eng());
        contentValues.put(OPTION_2 + post, paidQuestion.getOption2Eng());
        contentValues.put(OPTION_3 + post, paidQuestion.getOption3Eng());
        contentValues.put(OPTION_4 + post, paidQuestion.getOption4Eng());
        contentValues.put(OPTION_5 + post, paidQuestion.getOption5Eng());

        post = HIN;
        contentValues.put(TITLE + post, paidQuestion.getTitleHin());
        contentValues.put(DESCRIPTION + post, paidQuestion.getDescriptionHin());
        contentValues.put(OPT_QUE + post, paidQuestion.getOptionQuestionHin());
        contentValues.put(OPT_ANS + post, paidQuestion.getOptionAnswerHin());
        contentValues.put(ANS_DESC + post, paidQuestion.getAnswerDescriptionHin());
        contentValues.put(OPTION_1 + post, paidQuestion.getOption1Hin());
        contentValues.put(OPTION_2 + post, paidQuestion.getOption2Hin());
        contentValues.put(OPTION_3 + post, paidQuestion.getOption3Hin());
        contentValues.put(OPTION_4 + post, paidQuestion.getOption4Hin());
        contentValues.put(OPTION_5 + post, paidQuestion.getOption5Hin());

        try {
            long l = db.insertOrThrow(TABLE_PAID_MOCK_QUE, null, contentValues);
            Logger.e("insertPaidMockQue " + l);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public HashMap<Integer, String> fetchCategoryData(SparseIntArray sparseIntArray, String where) {
        HashMap<Integer, String> hashMap = null;
        try {
            Cursor cursor = db.query(TABLE_SUB_CAT, null, where, null, null, null, null);
            hashMap = new HashMap<>();
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    int subCatId = cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID));
                    int catId = cursor.getInt(cursor.getColumnIndex(COLUMN_CAT_ID));
                    String subCatName = cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CAT_NAME));
                    hashMap.put(subCatId, subCatName);
                    sparseIntArray.put(subCatId, catId);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public ArrayList<CategoryBean> fetchCategoryData(int catId, int[] images, int defaultImg) {
        ArrayList<CategoryBean> categoryBeen = null;
        Cursor cursor = db.query(TABLE_SUB_CAT, null, COLUMN_CAT_ID + "=" + catId, null, null, null, COLUMN_SUB_CAT_ID + " ASC");
        categoryBeen = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            CategoryBean bean;
            int count = 0;
            do {
                bean = new CategoryBean();
                bean.setCategoryId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setCategoryName(cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CAT_NAME)));
                if (images != null && images.length > count)
                    bean.setCategoryImage(images[count++]);
                else
                    bean.setCategoryImage(defaultImg);
                categoryBeen.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categoryBeen;
    }

    public String getFullUrl(String id, String url) {
        return url;
//        return AppConstant.OPEN_PDF_URL + id + "/" + url;
    }

    private String getBaseUrl(String id, String url) {
        return id + "/" + url;
    }

    private String cleanUrl(String url) {
        if (!SupportUtil.isEmptyOrNull(url)) {
            if (url.contains("<p>"))
                url = url.replaceAll("<p>", "");
            if (url.contains("</p>"))
                url = url.replaceAll("</p>", "");
            if (url.contains("\r\n"))
                url = url.replaceAll("\r\n", "");
        }
        return url;
    }

    private String getUrl(String s) {
        s = cleanUrl(s);
        if (!SupportUtil.isEmptyOrNull(s)) {
            if (s.toLowerCase().endsWith("pdf"))
                s = preUrl + s;
        }

        return s;
    }

    private String preUrl = "http://drive.google.com/viewerng/viewer?embedded=true&url=";

    private Cursor getHomeArticleCursor(String query, int catId) {
        try {
            if (!SupportUtil.isEmptyOrNull(query))
                query = " AND " + query;
            return db.query(getArticleTableName(catId), null, COLUMN_TITLE + "!='NULL'" + query, null, null, null, COLUMN_ID + " DESC");
        } catch (SQLException e) {
            return null;
        }
    }

    public String[] fetchLatestEmptyIds(String catQuery, int catId, Context context) {
        String[] ids = null;
        if (!SupportUtil.isEmptyOrNull(catQuery))
            catQuery = catQuery + " AND ";
        Cursor cursor = db.query(getArticleTableName(catId), new String[]{COLUMN_ID}, catQuery + COLUMN_TITLE + " is null or " + COLUMN_TITLE + " = ''",
                null, null, null, COLUMN_ID + " DESC LIMIT 100 ");

        if (cursor != null && cursor.getCount() > 0) {
            ids = new String[cursor.getCount()];
            int i = 0;
            cursor.moveToFirst();
            do {
                ids[i++] = cursor.getInt(cursor.getColumnIndex(COLUMN_ID)) + "";
            } while (cursor.moveToNext());
        }
        cursor.close();
        return ids;
    }

    public int fetchLowestContentId(String query) {
        if (!db.isOpen())
            db = openDataBase();
        Cursor cursor = db.query(TABLE_ARTICLE, new String[]{COLUMN_ID}, COLUMN_TITLE + "!='NULL' AND " + query,
                null, null, null, COLUMN_ID + " ASC");
        int maxId = 0;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            maxId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        }
        return maxId;
    }

    public String convertToDateFormat(String args) {
        if (args != null && !TextUtils.isEmpty(args)) {
            args = args.split("\\s+")[0];
            if (args.contains(" "))
                args = args.split(" ")[0];
            String[] arr = args.split("-");
            StringBuilder stringBuilder = new StringBuilder(arr[2]);
            stringBuilder.append(" ").append(MONTHS_NAME[Integer.parseInt(arr[1]) - 1]).append(" ").append(arr[0]);
            return stringBuilder.toString();
        }

        return args;
    }

    public void insertArticle(List<ServerBean> idBeen, int id, boolean isTypePdf, boolean isDelete) {
        if (isDelete) {
            try {
                String query = COLUMN_CAT_ID + "=" + id + " AND " + COLUMN_FAV + "=" + INT_FALSE
                        + " AND " + COLUMN_READ + "=" + INT_FALSE;
                int i = db.delete(getArticleTableName(id), query, null);
//                    int i = db.delete(TABLE_GOVT_ARTICLE, query, null);
                Logger.e("insertArticle delete : " + i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String tableName = getArticleTableName(id);
        for (ServerBeanArticle homeBean : idBeen) {
            Cursor cursor = db.query(tableName, new String[]{COLUMN_ID}, COLUMN_TITLE + "!='NULL' AND " + COLUMN_ID + "='" + homeBean.getId() + "'",
                    null, null, null, null);
            if (!(cursor != null && cursor.getCount() > 0)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_ID, homeBean.getId());
                contentValues.put(COLUMN_TITLE, homeBean.getTitle());
                contentValues.put(COLUMN_DATE, convertToDateFormat(homeBean.getUpdatedAt()));
                contentValues.put(COLUMN_DESC, isTypePdf ? getBaseUrl(homeBean.getId() + "", ((ServerBean) homeBean).getPdf()) : homeBean.getDescription());
                if (isGovtJob(id)) {
                    ServerBean serverBean = (ServerBean) homeBean;
                    contentValues.put(COLUMN_APPLY_ONLINE, cleanUrl(SupportUtil.isEmptyOrNull(serverBean.getOptionC())
                            ? getUrl(serverBean.getOptionB()) : getBaseUrl(homeBean.getId() + "", serverBean.getOptionC())));
                    contentValues.put(COLUMN_READ_DETAILS, cleanUrl(SupportUtil.isEmptyOrNull(serverBean.getPdf())
                            ? getUrl(serverBean.getOptionA()) : getBaseUrl(homeBean.getId() + "", serverBean.getPdf())));
                }
                int c = db.update(tableName, contentValues, COLUMN_ID + "=" + homeBean.getId(), null);
                Logger.e("insertArticle update : " + c);
                if (c == 0) {
                    contentValues.put(COLUMN_CAT_ID, id);
                    contentValues.put(COLUMN_SUB_CAT_ID, homeBean.getCategoryId());
                    try {
                        long l = db.insertOrThrow(tableName, null, contentValues);
                        Logger.e("insertArticle insertOrThrow : " + l);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            cursor.close();
        }
    }

    public void insertCat(List<CatBean> catBeen) {
        for (CatBean bean : catBeen) {
            Context context = AppApplication.getInstance();
//            if ( bean.getParent_id() == AppValues.CAA_ID && ( bean.getId() == Integer.parseInt(AppApplication.getInstance().getString( R.string.ca_one_min_id ) )
//                    || bean.getId() == Integer.parseInt( AppApplication.getInstance().getString( R.string.capsule_id ) ) ) ) {
//            }else
            if (bean.getParent_id() == AppValues.NOTES_CAT_ID[0] && bean.getId() == Integer.parseInt(context.getString(R.string.history_id))) {
                String[] strings = context.getResources().getStringArray(R.array.history);
                int[] ints = context.getResources().getIntArray(R.array.history_ids);
                CatBean bean1;
                for (int i = 0; i < strings.length; i++) {
                    bean1 = new CatBean(strings[i], ints[i], bean.getParent_id());
                    insertCatData(bean1);
                }
            } else if (bean.getParent_id() == AppValues.NOTES_CAT_ID[0] && bean.getId() == Integer.parseInt(context.getString(R.string.science_id))) {
                String[] strings = context.getResources().getStringArray(R.array.science);
                int[] ints = context.getResources().getIntArray(R.array.scinece_ids);
                CatBean bean1;
                for (int i = 0; i < strings.length; i++) {
                    bean1 = new CatBean(strings[i], ints[i], bean.getParent_id());
                    insertCatData(bean1);
                }
            } else {
                insertCatData(bean);
            }
        }
    }

    private void insertCatData(CatBean bean) {
        String where = COLUMN_CAT_ID + "=" + bean.getParent_id() + " AND " + COLUMN_SUB_CAT_ID + "=" + bean.getId();
        Cursor cursor = db.query(TABLE_SUB_CAT, null, where, null, null, null, null);
        if (!(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())) {
            cursor.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_CAT_ID, bean.getParent_id());
            contentValues.put(COLUMN_SUB_CAT_ID, bean.getId());
            contentValues.put(COLUMN_SUB_CAT_NAME, bean.getTitle());
            try {
                db.insertOrThrow(TABLE_SUB_CAT, null, contentValues);
            } catch (Exception e) {
            }
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_SUB_CAT_NAME, bean.getTitle());
            db.update(TABLE_SUB_CAT, contentValues, where, null);
        }

    }

    private boolean isGovtJob(int catId) {
        return catId == AppValues.GOVT_JOBS_ID;
    }

    private boolean isNativeAd(int position) {
        return position == 2 || position == 12;
    }

    public void fetchHomeData(ArrayList<ListBean> list, String query, int catId, boolean isPdf) {
        ListBean bean;
        ArrayList<ListBean> been = new ArrayList<>();
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            int countAds = 0;
            int lastId = 0;
            do {
//    				if ( countAds % AppConstant.NATIVE_ADS_COUNT_MEDIUM == 0 ) {
                if (isNativeAd(countAds) && AppConstant.IS_ADS_ENABLED) {
                    bean = new ListBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    bean.setId(lastId);
                    been.add(bean);
//    					countAds = 0 ;
                    countAds++;
                }
                countAds++;

                bean = new ListBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                lastId = bean.getId();
                if (!isPdf) {
                    bean.setTrue(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                } else {
                    bean.setTrue(isPdfDownloaded(cursor.getString(cursor.getColumnIndex(COLUMN_DESC))));
                }
                bean.setText(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                bean.setSubCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) == INT_TRUE);
                been.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
    }

    private boolean isPdfDownloaded(String filePath) {
        if (!SupportUtil.isEmptyOrNull(filePath)) {
            try {
                return new File(Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + filePath.split("/")[1]).exists();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public void fetchImpCatData(List<ImpCatListBean> list, String query, int catId, boolean isAds) {
        ImpCatListBean bean;
        List<ImpCatListBean> been = new ArrayList<>();
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            int lastId = 0;
            int countAds = 0;
            do {
//    				if ( countAds % AppConstant.NATIVE_ADS_COUNT_MEDIUM == 0 ) {
                if (isAds && isNativeAd(countAds)) {
                    bean = new ImpCatListBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    bean.setId(lastId);
                    been.add(bean);
//    					countAds = 0 ;
                    countAds++;
                }
                countAds++;

                bean = new ImpCatListBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                lastId = bean.getId();
                bean.setDesc(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) == INT_TRUE);
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) == INT_TRUE);
                been.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        list.clear();
        if (been.size() > 0) {
            list.addAll(been);
        }
    }

    public int fetchDescData(ArrayList<HomeBean> list, String query, int id, int catId, boolean isWebView) {
        HomeBean bean;
        int position = 0;
        ArrayList<HomeBean> been = new ArrayList<>();
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0) {
            int count = 0;
            cursor.moveToFirst();
            int countAds = 0;
            do {
                if (countAds == AppConstant.NATIVE_ADS_COUNT_LARGE && AppConstant.IS_ADS_ENABLED) {
                    bean = new HomeBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    been.add(bean);
                    countAds = 0;
                    count++;
                } else
                    countAds++;
                bean = new HomeBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) != 0);
                bean.setSubCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDesc(isWebView ? cursor.getString(cursor.getColumnIndex(COLUMN_DESC)) :
                        removeBottomPadding(cursor.getString(cursor.getColumnIndex(COLUMN_DESC))));
                if (isGovtJob(catId)) {
                    bean.setReadDetails(cursor.getString(cursor.getColumnIndex(COLUMN_READ_DETAILS)));
                    bean.setApplyOnline(cursor.getString(cursor.getColumnIndex(COLUMN_APPLY_ONLINE)));
                }
                been.add(bean);
                if (bean.getId() == id)
                    position = count;
                count++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
        return position;
    }

    private String removeBottomPadding(String s) {
        if (s.contains("<p>&nbsp;</p>"))
            s = s.replaceAll("<p>&nbsp;</p>", "");
        return s;
    }

    public int getImpReadCount() {
        String s = AppPreferences.getImpUpdates(AppApplication.getInstance());
        if (!SupportUtil.isEmptyOrNull(s) && !emptyListString(s)) {
            int count = listStringSize(s);
            if (count > 0) {
                String q = ID + " IN " + listToString(s);
//                        + " AND " + COLUMN_CAT_ID + "=" + AppValues.ID_IMP_UPDATES
//                        + " AND " + COLUMN_READ + "=" + INT_TRUE ;
                return getImpCount(q, count);
            } else {
                return 0;
            }
        } else
            return 0;
    }

    private String listToString(String s) {
        s = s.replaceAll("\\[", "(");
        s = s.replaceAll("\\]", ")");
        return s;
    }

    private boolean emptyListString(String s) {
        String s1 = s.replaceAll("\\[", "");
        s1 = s1.replaceAll("\\]", "");
        return SupportUtil.isEmptyOrNull(s1);
    }

    private int listStringSize(String s) {
        String s1 = s.replaceAll("\\[", "");
        s1 = s1.replaceAll("\\]", "");
        if (!SupportUtil.isEmptyOrNull(s1)) {
            if (s1.contains(","))
                return s1.split(",").length;
            else
                return 1;
        } else {
            return 0;
        }
    }

    private int getImpCount(String q, int count) {
        Cursor cursor = db.query(getArticleTableName(AppValues.ID_IMP_UPDATES), null, q, null, null, null, COLUMN_ID + " DESC");
        int readCount = 0;
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            do {
                if (cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) == INT_TRUE) {
                    readCount++;
                }
            } while (cursor.moveToNext());
        }
        return count - readCount;
    }

    public HomeBean fetchArticle(int id) {
        HomeBean bean = null;
        Cursor cursor = db.query(TABLE_ARTICLE, null, COLUMN_ID + "=" + id, null, null, null, COLUMN_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                bean = new HomeBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) != 0);
                bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDesc(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return bean;
    }

    public void updateArticle(int id, String columnName, int value, int catId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(columnName, value);
        try {
            int i = db.update(getArticleTableName(catId), contentValues, COLUMN_ID + "=" + id, null);
            Logger.e("updateArticle : " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertArticleId(List<IdBean> idBeen, int catID) {
        for (IdBean bean : idBeen) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_ID, bean.getId());
            contentValues.put(COLUMN_SUB_CAT_ID, bean.getCategoryId());
            contentValues.put(COLUMN_CAT_ID, catID);
            Cursor cursor = db.query(getArticleTableName(catID), null, COLUMN_ID + "=" + bean.getId(), null, null, null, null);
            if (cursor == null || cursor.getCount() < 1)
                try {
                    db.insertOrThrow(getArticleTableName(catID), null, contentValues);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            cursor.close();
        }
    }

    public long insertResult(String title, int catId, long time, int mockTestId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_TITLE, title);
        contentValues.put(COLUMN_CAT_ID, catId);
        contentValues.put(COLUMN_MOCK_TEST_ID, mockTestId);
        contentValues.put(COLUMN_TIME_INIT, time + "");
        long id = db.insertOrThrow(TABLE_RESULT, null, contentValues);
        return id;
    }

    public long updateResult(long resultId, int totalQue, int skipQue, int correctAns, int wrongAns, long time, String data) {
//        String timeTaken = null ;
        long timeTaken = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_TOTAL_QUE, totalQue);
        contentValues.put(COLUMN_SKIP_QUE, skipQue);
        contentValues.put(COLUMN_CORRECT_ANS, correctAns);
        contentValues.put(COLUMN_WRONG_ANS, wrongAns);
        contentValues.put(COLUMN_TIME_FINISH, time + "");
        contentValues.put(AppConstant.DATA, data);

        db.update(TABLE_RESULT, contentValues, COLUMN_ID + "=" + resultId, null);
        Cursor cursor = db.query(TABLE_RESULT, null, COLUMN_ID + "=" + resultId, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            String initTIme = cursor.getString(cursor.getColumnIndex(COLUMN_TIME_INIT));
//                timeTaken = timeTaken( time  - Long.parseLong( initTIme ) );
            timeTaken = time - Long.parseLong(initTIme);
        }
        cursor.close();
        return timeTaken;
    }

    public void deleteResult(long resultId) {
        db.delete(TABLE_RESULT, COLUMN_ID + "=" + resultId, null);
    }

    public void deletePaidResult(long resultId) {
        try {
            db.delete(TABLE_PAID_MOCK_RESULT, COLUMN_ID + "=" + resultId, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String where = RESULT_ID + "=" + resultId;
        try {
            db.delete(TABLE_PAID_MOCK_RESULT_DATA, where, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateResult(long resultId, String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.DATA, data);
        db.update(TABLE_RESULT, contentValues, COLUMN_ID + "=" + resultId, null);
    }


    public ArrayList<MockTestBean> fetchMockTestByTestId(String query, String orderBy) {
        ArrayList<MockTestBean> been = new ArrayList<>();
        Cursor cursor = db.query(TABLE_MCQ_DATA, null, query, null, null, null, orderBy);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            MockTestBean bean;
            do {
                bean = new MockTestBean();
                bean.setDirection(cursor.getString(cursor.getColumnIndex(COLUMN_DIRECTION)));
                bean.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
                bean.setOptionQuestion(cursor.getString(cursor.getColumnIndex(COLUMN_QUE)));
//                    bean.setOptionQuestion( removePadding( cursor.getString( cursor.getColumnIndex( COLUMN_QUE ) ) ) );
                bean.setOptionAns(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_ANS))));
                bean.setOptionA(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_OPT_A))));
                bean.setOptionB(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_OPT_B))));
                bean.setOptionC(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_OPT_C))));
                bean.setOptionD(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_OPT_D))));
                bean.setOptionE(removePadding(cursor.getString(cursor.getColumnIndex(COLUMN_OPT_E))));
                bean.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                been.add(bean);
                Logger.e("Ans - " + bean.getOptionAns());
            } while (cursor.moveToNext());
            cursor.close();
        }
        return been;
    }

    public String removePadding(String s) {
        if (s != null) {
            if (s.contains("\r\n"))
                s = s.replaceAll("\r\n", "");
            if (s.contains("<p>")) {
                s = s.replaceAll("<p>", "");
                s = s.replaceAll("</p>", "");

            }
//            if ( s.contains("<strong>") ) {
//                s = s.replaceAll("<strong>", "");
//                s = s.replaceAll("</strong>", "");
//            }

        }
        return s;
    }

    public void upDateMockDb(String column, int id, int catId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(column, INT_TRUE);
        db.update(TABLE_MCQ_LIST, contentValues, COLUMN_ID + "=" + id + " AND " + COLUMN_CAT_ID + "=" + catId, null);
    }

    public void getResultList(ArrayList<ResultBean> been, String query) {
        Cursor cursor = db.query(TABLE_RESULT, null, query, null, null, null, COLUMN_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            been.clear();
            ResultBean bean;
            do {
                bean = new ResultBean();
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setData(cursor.getString(cursor.getColumnIndex(AppConstant.DATA)));
                bean.setCatID(cursor.getInt(cursor.getColumnIndex(COLUMN_CAT_ID)));
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setMockTestId(cursor.getInt(cursor.getColumnIndex(COLUMN_MOCK_TEST_ID)));
                bean.setNumberQue(cursor.getInt(cursor.getColumnIndex(COLUMN_TOTAL_QUE)));
                bean.setSkipQue(cursor.getInt(cursor.getColumnIndex(COLUMN_SKIP_QUE)));
                bean.setCorrectAns(cursor.getInt(cursor.getColumnIndex(COLUMN_CORRECT_ANS)));
                bean.setWrongAns(cursor.getInt(cursor.getColumnIndex(COLUMN_WRONG_ANS)));
                String timeInit = cursor.getString(cursor.getColumnIndex(COLUMN_TIME_INIT));
                String timeFinish = cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FINISH));
                bean.setTimeTaken(timeTaken(Long.parseLong(timeFinish) - Long.parseLong(timeInit)));
                bean.setDate(getDate(Long.parseLong(timeInit)));
                bean.setCompleted(!timeFinish.equals("0"));
                been.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private SimpleDateFormat simpleDateFormat;

    private String getDate(long time) {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat("dd MMM yyyy | hh:mm a");
        return simpleDateFormat.format(new Date(time));
    }

    public String timeTaken(long time) {
        return String.format("%02d min, %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))
        );
    }

    public void getMockTestList(ArrayList<MockHomeBean> been, String query) {
        Cursor cursor = getMockTestListCursor(query);
        ArrayList<MockHomeBean> mockHomeBeans = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            Logger.e("getMockTestList", "cursor.getCount : " + cursor.getCount());
            MockHomeBean bean;
            int countAds = 0;
            int lastId = 0;
            do {
                if (isNativeAd(countAds) && AppConstant.IS_ADS_ENABLED) {
                    bean = new MockHomeBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    bean.setId(lastId);
                    mockHomeBeans.add(bean);
                    countAds++;
                }
                countAds++;
                bean = new MockHomeBean();
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setSubCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                lastId = bean.getId();
                bean.setAttempted(cursor.getInt(cursor.getColumnIndex(COLUMN_ATTEMPTED)) != 0);
                bean.setSync(cursor.getInt(cursor.getColumnIndex(IS_SYNC)) != 0);
                bean.setDownload(cursor.getInt(cursor.getColumnIndex(COLUMN_DOWNLOADED)));
                String ins = cursor.getString(cursor.getColumnIndex(COLUMN_INSTRUCTION));
                bean.setInstruction(SupportUtil.isEmptyOrNull(ins) ? AppConstant.DEFAULT_INSTRUCTION : ins);
                bean.setTestTime(SupportUtil.stringToInt(cursor.getString(cursor.getColumnIndex(COLUMN_TEST_TIME))));
                bean.setTestMarkes(SupportUtil.stringToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_TEST_MARKS))));
                bean.setNegativeMarking(SupportUtil.stringToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_NEGATIVE_MARKING))));
                bean.setQuestMarks(SupportUtil.stringToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_QUEST_MARKS))));
                mockHomeBeans.add(bean);
                ;
            } while (cursor.moveToNext());
        }
        cursor.close();
        been.clear();
        if (mockHomeBeans != null && mockHomeBeans.size() > 0) {
            been.addAll(mockHomeBeans);
        }
    }

    public boolean getMockTestTitle(String query) {
        Cursor cursor = db.query(TABLE_MCQ_DATA, null, query, null, null, null, null);
        boolean b = (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst());
        cursor.close();
        return b;
    }

    public ListBean getArticle(int id, int catId) {
        ListBean listBean = null;
        Cursor cursor = db.query(getArticleTableName(catId), null, COLUMN_ID + "=" + id, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            listBean = new ListBean();
            listBean.setText(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
            listBean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
        }
        cursor.close();
        return listBean;
    }

    public static int QUE_NUM = 20;

    public boolean getMockTestCatId(String query) {
        Cursor cursor = db.query(TABLE_MCQ_DATA, null, query, null, null, null, null);
        boolean b = (cursor != null && cursor.getCount() >= QUE_NUM && cursor.moveToFirst());
        cursor.close();
        return b;
    }

    public String getUrlForPDF(int id) {
        String url = null;
        Cursor cursor = db.query(TABLE_ARTICLE, null, COLUMN_ID + "=" + id, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            url = cursor.getString(cursor.getColumnIndex(COLUMN_DESC));
        }
        cursor.close();
        return url;
    }

    private Cursor getMockTestListCursor(String query) {
        return db.query(TABLE_MCQ_LIST, null, query, null, null, null, COLUMN_ID + " DESC");
    }

    public void insertMockTest(List<MockTestBean> been, int mockTestId, int catId) {
        for (MockTestBean bean : been) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_ID, bean.getId());
            contentValues.put(COLUMN_CAT_ID, catId);
            contentValues.put(COLUMN_MOCK_TEST_ID, mockTestId);
            contentValues.put(COLUMN_QUE, bean.getOptionQuestion());
//                if ( catId != AppValues.MOCK_CAT_ID[ 0 ] ) {
//                    contentValues.put( COLUMN_DESC , SupportUtil.isEmptyOrNull( bean.getSmall_description() ) ? bean.getDescription() : bean.getSmall_description() );
            contentValues.put(COLUMN_DESC, bean.getSmall_description());
//                }
            contentValues.put(COLUMN_DIRECTION, bean.getDescription());
            contentValues.put(COLUMN_OPT_A, bean.getOptionA());
            contentValues.put(COLUMN_OPT_B, bean.getOptionB());
            contentValues.put(COLUMN_OPT_C, bean.getOptionC());
            contentValues.put(COLUMN_OPT_D, bean.getOptionD());
            contentValues.put(COLUMN_OPT_E, bean.getOptionE());
            contentValues.put(COLUMN_ANS, bean.getOptionAns());
            contentValues.put(COLUMN_SUB_CAT_ID, bean.getCategoryId());
            try {
                db.insertOrThrow(TABLE_MCQ_DATA, null, contentValues);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        upDateMockDb(COLUMN_DOWNLOADED, mockTestId, catId);
    }

    public void insertMockTestLocal(List<LocalMockData> been) {
        for (LocalMockData bean : been) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_ID, bean.getId());
            contentValues.put(COLUMN_CAT_ID, bean.getCatId());
            contentValues.put(COLUMN_MOCK_TEST_ID, bean.getMockTestId());
            contentValues.put(COLUMN_QUE, bean.getQue());
            contentValues.put(COLUMN_OPT_A, bean.getOptA());
            contentValues.put(COLUMN_OPT_B, bean.getOptB());
            contentValues.put(COLUMN_OPT_C, bean.getOptC());
            contentValues.put(COLUMN_OPT_D, bean.getOptD());
            contentValues.put(COLUMN_ANS, bean.getAns());
            contentValues.put(COLUMN_SUB_CAT_ID, bean.getSubCatId());
            try {
                db.insertOrThrow(TABLE_MCQ_DATA, null, contentValues);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertMockTestList(List<MockBean> been, int catId) {
        for (MockBean bean : been) {
            Cursor cursor = db.query(TABLE_MCQ_LIST, null, COLUMN_ID + "=" + bean.getId() +
                    " AND " + COLUMN_CAT_ID + "=" + catId, null, null, null, null);
            if (!(cursor != null && cursor.getCount() > 0)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_ID, bean.getId());
                contentValues.put(COLUMN_SUB_CAT_ID, bean.getCategoryId());
                contentValues.put(COLUMN_TITLE, bean.getTitle());
                contentValues.put(COLUMN_CAT_ID, catId);
                contentValues.put(COLUMN_INSTRUCTION, SupportUtil.isEmptyOrNull(bean.getInstruction()) ? AppConstant.DEFAULT_INSTRUCTION : bean.getInstruction());
                contentValues.put(COLUMN_TEST_TIME, SupportUtil.isEmptyOrNull(bean.getTestTime()) ? AppConstant.DEFAULT_TEST_TIME : bean.getTestTime());
                contentValues.put(COLUMN_TEST_MARKS, SupportUtil.isEmptyOrNull(bean.getTestMarkes()) ? AppConstant.DEFAULT_TEST_MARKS : bean.getTestMarkes());
                contentValues.put(COLUMN_NEGATIVE_MARKING, SupportUtil.isEmptyOrNull(bean.getNegativeMarking()) ? AppConstant.DEFAULT_NEGATIVE_MARKS : bean.getNegativeMarking());
                contentValues.put(COLUMN_QUEST_MARKS, SupportUtil.isEmptyOrNull(bean.getQuestMarks()) ? AppConstant.DEFAULT_QUEST_MARKS : bean.getQuestMarks());
                try {
                    long l = db.insertOrThrow(TABLE_MCQ_LIST, null, contentValues);
                    Logger.e("insertMockTestList", "insertOrThrow : " + l);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
        }
    }

    public void insertMockTestListLocal(List<LocalMockList> been) {
        for (LocalMockList bean : been) {
            Cursor cursor = db.query(TABLE_MCQ_LIST, null, COLUMN_ID + "=" + bean.getId() +
                    " AND " + COLUMN_CAT_ID + "=" + bean.getCatId(), null, null, null, null);
            if (!(cursor != null && cursor.getCount() > 0)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_ID, bean.getId());
                contentValues.put(COLUMN_SUB_CAT_ID, bean.getSubCatId());
                contentValues.put(COLUMN_TITLE, bean.getTitle());
                contentValues.put(COLUMN_CAT_ID, bean.getCatId());
                contentValues.put(COLUMN_DOWNLOADED, bean.getIsDownloaded());
                contentValues.put(COLUMN_INSTRUCTION, AppConstant.DEFAULT_INSTRUCTION);
                contentValues.put(COLUMN_TEST_TIME, AppConstant.DEFAULT_TEST_TIME);
                contentValues.put(COLUMN_TEST_MARKS, AppConstant.DEFAULT_TEST_MARKS);
                contentValues.put(COLUMN_NEGATIVE_MARKING, AppConstant.DEFAULT_NEGATIVE_MARKS);
                contentValues.put(COLUMN_QUEST_MARKS, AppConstant.DEFAULT_QUEST_MARKS);
                try {
                    long l = db.insertOrThrow(TABLE_MCQ_LIST, null, contentValues);
                    Logger.e("insertMockTestListLocal", "insertOrThrow : " + l);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
        }
    }

    public ArrayList<CategoryBean> MockFetchCategoryData(String query, int[] images, int defaultImg) {
        ArrayList<CategoryBean> list = new ArrayList<>();
        Cursor cursor = db.query(TABLE_SUB_CAT, null, query, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            list.clear();
            CategoryBean bean;
            int count = 0;
            do {
                bean = new CategoryBean();
                bean.setCategoryName(cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CAT_NAME)));
                bean.setCategoryId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                if (images != null && images.length > count)
                    bean.setCategoryImage(images[count++]);
                else
                    bean.setCategoryImage(defaultImg);
                list.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }


    private String getArticleTableName(int catID) {
        return catID == AppValues.GOVT_JOBS_ID ? TABLE_GOVT_ARTICLE : TABLE_ARTICLE;
    }


    public String[] MONTHS_NAME = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"
            , "Nov", "Dec"};

    // Amit Groch Ncert

    public void fetchQuoteData(ArrayList<SubjectModel> beanArrayList, int id) {
        ArrayList<SubjectModel> copybeanArrayList = new ArrayList<>();
        Cursor cursor = db.query(TABLE_SUBJECTS, null, CLASSID + " = " + id, null, null, null, ID + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            SubjectModel quoteBean;
            int countAds = 0;
            do {
//                if ( countAds % AppConstant.NATIVE_ADS_COUNT_MEDIUM == 0 ) {
                if (isNativeAd(countAds)) {
                    quoteBean = new SubjectModel();
                    quoteBean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    copybeanArrayList.add(quoteBean);
//                    countAds = 0 ;
                    countAds++;
                }

                countAds++;
                quoteBean = new SubjectModel();
                quoteBean.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                quoteBean.setTitle(cursor.getString(cursor.getColumnIndex(TITTLE)));
                copybeanArrayList.add(quoteBean);
            } while (cursor.moveToNext());
            beanArrayList.clear();
            beanArrayList.addAll(copybeanArrayList);
        }
        cursor.close();
    }

    public void insertArticleNcert(List<SubjectModel> homeBean, int catId) {
        try {
            for (SubjectModel bean : homeBean) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(ID, bean.getId());
                contentValues.put(TITTLE, bean.getTitle());
                contentValues.put(CLASSID, catId);
                int c = db.update(TABLE_SUBJECTS, contentValues, ID + "=" + bean.getId(), null);
                if (c == 0)
                    db.insert(TABLE_SUBJECTS, null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int fetchMaxBookId(int names) {
        Cursor cursor = db.query(TABLE_BOOKS, null, SUBJECTID + " = '" + names + "'", null, null, null, ID + " DESC ");
        int maxId = 0;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            maxId = cursor.getInt(cursor.getColumnIndex(ID));
        }
        cursor.close();
        return maxId;
    }

    public void insertpdfBooks(List<SubjectModel> homeBean, int catId) {
        try {
            for (SubjectModel bean : homeBean) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(ID, bean.getId());
                contentValues.put(TITTLE, bean.getTitle());
                contentValues.put(PDF, bean.getPdf());
                contentValues.put(UPDATEAT, bean.getUpdatedAt());
                contentValues.put(SUBJECTID, catId);
                int c = db.update(TABLE_BOOKS, contentValues, ID + "=" + bean.getId(), null);
                if (c == 0)
                    db.insert(TABLE_BOOKS, null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchBooksPdfNames(ArrayList<SubjectModel> beanArrayList, int id) {
        ArrayList<SubjectModel> copybeanArrayList = new ArrayList<>();
        Cursor cursor = db.query(TABLE_BOOKS, null, SUBJECTID + " = " + id, null, null, null, ID + " DESC");

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            SubjectModel quoteBean;
            int countAds = 0;
            do {
//                if ( countAds % AppConstant.NATIVE_ADS_COUNT_MEDIUM == 0 ) {
                if (isNativeAd(countAds)) {
                    quoteBean = new SubjectModel();
                    quoteBean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    copybeanArrayList.add(quoteBean);
//                    countAds = 0 ;
                    countAds++;
                }

                countAds++;
                quoteBean = new SubjectModel();
                quoteBean.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                quoteBean.setTitle(cursor.getString(cursor.getColumnIndex(TITTLE)));
                quoteBean.setPdf(cursor.getString(cursor.getColumnIndex(PDF)));
                quoteBean.setUpdatedAt(cursor.getString(cursor.getColumnIndex(UPDATEAT)));
                String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + quoteBean.getId() + "-" + quoteBean.getPdf();
                File file = new File(filePath);
                if (file.exists()) {
                    quoteBean.setisDownloaded(true);
                }
                copybeanArrayList.add(quoteBean);
            } while (cursor.moveToNext());
            beanArrayList.clear();
            beanArrayList.addAll(copybeanArrayList);
        }
        cursor.close();
        return;
    }

}
