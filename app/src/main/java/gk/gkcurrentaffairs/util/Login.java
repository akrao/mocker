package gk.gkcurrentaffairs.util;

import android.app.Activity;

import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;

/**
 * Created by Amit on 5/25/2018.
 */

public class Login {

    public static void handleLogin(Activity context , Callable<Void> function) {
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null
                && AppApplication.getInstance().getLoginSdk().isRegComplete() ) {
            try {
                function.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() == null ){
            AppApplication.getInstance().initOperations();
        }else {
            SupportUtil.openLoginDialog( context , false );
        }
    }
}
