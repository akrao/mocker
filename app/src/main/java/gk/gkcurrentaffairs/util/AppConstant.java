
package gk.gkcurrentaffairs.util;

import gk.gkcurrentaffairs.R;

/**
 * Created by Amit on 1/8/2017.
 */

public interface AppConstant {

    int SLIDER_TIME = 8000 ;
    String IS_OPEN_USER_LOGIN_FIRST_TIME = "isOpenUserRegistration_first_time" ;

    String[] HOME_TITLE_ARRAY = {
            "Slider" ,
            "Daily Booster" ,
            "Exam Target Questions",
            "Exam Master" ,
            "NCERT" ,
            "Easy Learning" ,
            "State GK"
    };

    String MOCK_DATA = "mock_data";
    String SOLUTION = "solution";
    String DEFAULT_INSTRUCTION = "1. Unlimited Test Time. Test Timer starts from 0<br>" +
            "<br>" +
            "2. Each question is of 1 marks.<br>" +
            "<br>" +
            "3. No Negative Marking<br>" +
            "<br>" +
            "4. You can view your Score & Rank after submitting the test.<br>" +
            "<br>" +
            "5. Rank is calculated on the basis of Marks Scored & Time";
    String DEFAULT_TEST_TIME = "0";
    String DEFAULT_TEST_MARKS = "0";
    String DEFAULT_QUEST_MARKS = "1";
    String DEFAULT_NEGATIVE_MARKS = "0";

    String IS_FIRST_PAID_LOGIN = "IS_FIRST_PAID_LOGIN" ;
    String IS_FIRST_PAID_EXAM_SELECT = "IS_FIRST_PAID_EXAM_SELECT" ;
    String HOST = "host";
//    String TEST = "-test";
    String isAdvanced="isAdvanced";
    String NO_TEST = "";
    String TEST = NO_TEST;
    String RESULT_DATA = "result_data" ;
    String APP_UPDATE = ".app_update";
    String downloadDirectory = "GK Current Affairs";
    String NCERTBOOKS="NCERT Books";
    String BOOKTYPE="booktype";
    String NCERTSOLUTIONENGLISH="NCERT Solution English";
    String LANG="langids";
    String PAID_QUESTIONS_LANG="paid_lang_eng";
    int NCERTEnglishId=387;
    int NCERTHindiId=388;
    int NCERTUrduId=389;
    int NCERTSOLUCTIONENGLISHId=505;
    String HINDI="HINDI";
    String ENGLUSH="ENGLISH";
    String URDU="URDU";
    String SUBJECTID="subjectId";
    String SUBJECTNAME="subjectName";
    String classId="classid";
    String imageName="imagename";
    String BOOKID="bookid";
    String LEADER_BOARD_POLICY = "https://topcoaching.in/lederboard-privacy-policy";

    String APP_ID = "app_id";
    String WEB_URL = "web_url";

    int CLICK_ARTICLE = 0 ;
    int CLICK_SHARE = 1 ;
    int CLICK_SAVE = 2 ;
    int MOCK_CONTENT_ID_BELOW_DIRECTION = 143317 ;

    String IS_SUB_CAT = "is_sub_cat" ;
    String CAT_DATA = "cat_data" ;
    String CAT_SERVER_DATA = "cat_server_data" ;
    String DATA = "data" ;
    String CAT_ID = "cat_id" ;
    String QUERY = "query" ;
    String IMAGE = "image" ;
    String IMAGE_URL = "image_url" ;
    String POSITION = "position" ;
    String WEB_VIEW = "web_view" ;

    String CLICK_ITEM_ARTICLE = "_Click_Article" ;
    String PRODUCT_VIEW_CLICK = "Product_View_Click" ;
    String PRODUCT_BUY_CLICK = "Product_Buy_Click" ;
    String SWIPE_ITEM_ARTICLE = "_Swipe_Article" ;
    String TITLE = "Title" ;
    String TYPE = "type" ;
    String SHOULD_LOAD = "should_load" ;
    String CATEGORY = "Category" ;
    String SOUND = "sound" ;
    String PRODUCT_ID = "Product_id" ;
    String COMPANY_NAME = "Company_Name" ;
    String SEARCH = "Search" ;
    String PRODUCT_URL = "product_url" ;
    String PAYMENT_BRAND_ID="1";
    String USER_AUTO_ID="user_auto_id";
    String DEVICE_ID="device_id";
    String DEVICE_TYPE_ID="1";
    String USER_EMAIL="user_email";
    String AMOUNT="amount";
    String PROPERTY ="property";

    public static final int INTENT_EXAM_UPDATE = 1012;
    String EMAIL_HELP_SUPPORT = "help@topcoaching.in";
    int NATIVE_ADS_ID = 0 ;
    long MAX_VALUE = 999999999 ;
    boolean IS_ADS_ENABLED = true ;
    int POSITION_MARGIN_MIDDLE = 6 ;
    int RV_POINTS = 1 ;
    int RESULT_PDF = 1991 ;
    String SERVICE_SUBSCRIBE = "1" ;
    String SERVICE_ADS = "2" ;
    String SERVICE_POINT = "3" ;
    String SERVICE_PACKAGE_MOCK_TEST = "4" ;
    String SERVICE_PACKAGE_SUB_TEST = "5" ;
    String SERVICE_MOCK_TEST = "6" ;
    String SERVICE_SUB_TEST = "7" ;

    String BENEFITS_AD_FREE = " DAYS";
    String COST_AD_FREE = " Points";
    String CURRENCY = " Rs";
    String OFFER_DAYS = "Days Pass";

    String URL_PDF_DOWNLOAD = "http://54.208.66.96/public/v2/android/download-pdf/" ;
    String URL_PDF_OPEN = "http://54.208.66.96/public/v1/android/govtjobs/show-pdf/" ;
//    String VERSIONING = "v3/";
    String VERSIONING = "";

    String CORRECT_ANS                                = "correct_ans";
    String NUM_QUE                                    = "num_que";
    String WRONG_ANS                                  = "wrong_ans";
    int ADS_BANNER = 0 ;
    int ADS_NATIVE_MED = 1 ;
    int ADS_NATIVE_SMALL = 3 ;
    int ADS_NATIVE_LARGE = 2 ;
    int NATIVE_ADS_COUNT_MEDIUM = 8 ;
    int NATIVE_ADS_COUNT_SMALL = 8 ;
    int NATIVE_ADS_COUNT_LARGE = 4 ;

    int NCERT_MOCK_TEST_ID = 1972 ;
    int ENGLISH_MOCK_TEST_ID = 6 ;
    int EDITORIAL_ID = 553 ;

    int NOTIFICATION_DEFAULT = 0 ;
    int NOTIFICATION_API_URL = 1 ;
    int NOTIFICATION_OPEN_ARTICLE = 2 ;
    int NOTIFICATION_OPEN_WEB_URL = 3 ;
    int NOTIFICATION_OPEN_PLAY_STORE = 4 ;
    int NOTIFICATION_VERSION_1 = 5 ;
    int NOTIFICATION_VERSION_1_HOME_LIST_CLICK = 1 ;
    int NOTIFICATION_VERSION_1_OPEN_ARTICLE = 2 ;
    int NOTIFICATION_VERSION_1_PAID_HOME_PAGE = 3 ;
    int NOTIFICATION_VERSION_1_PAID_OPEN_PACKAGE = 4 ;
    int NOTIFICATION_VERSION_1_OPEN_MOCK_LIST = 5 ;

    String NOTIFICATION_VERSION_1_TYPE = "nv_one" ;
    int QUE_NOT_ATTEMPTED = 0 ;
    int QUE_ATTEMPTED_CORRECT = 1 ;
    int QUE_ATTEMPTED_WRONG = 2 ;
    int MCQ_TIme = 19 ;
    int MCQ_PLAY_MIN_MARKS = 37 ;
    String COUNT_QUE = "que";

    String MSG_UPDATE = "Please update your App for the new feature.";

    int QUE_SELECT_1 = 5 ;
    int QUE_SELECT_2 = 6 ;
    int QUE_SELECT_3 = 7 ;
    int QUE_SELECT_4 = 8 ;
    int QUE_SELECT_5 = 9 ;

    int QUE_SELECT_DIFF = 5 ;

    int PAID_QUE_NOT_VISIT = 6 ;
    int PAID_QUE_VISIT = 7 ;
    int PAID_QUE_REVIEW = 10 ;

    int INT_TRUE = 1;
    int INT_FALSE = 0;

    int TIME_MILLI_LENGTH = 13 ;

    int[] CATEGORY_EXIST = {
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,

            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,

            INT_FALSE ,
            INT_FALSE ,
            INT_FALSE ,
            INT_TRUE ,
            INT_FALSE ,
            INT_TRUE ,

            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE
    };

    int[] CATEGORY_WEB = {
            INT_FALSE ,
            INT_FALSE ,
            INT_FALSE ,
            INT_TRUE ,
            INT_FALSE ,
            INT_TRUE ,

            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,

            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,
            INT_TRUE ,

            INT_FALSE ,
            INT_FALSE ,
            INT_FALSE ,
            INT_FALSE ,
            INT_FALSE
    };

    int[] IMAGE_RES = {
            R.drawable.mock_test ,
            R.drawable.ca_articles ,
            R.drawable.current_affairs_quiz ,
            R.drawable.govt_jobs ,
            R.drawable.editorial ,
            R.drawable.important_notes ,
            R.drawable.gk ,
            R.drawable.english ,
            R.drawable.aptitude ,
            R.drawable.reasoning ,
            R.drawable.gk_tricks ,
            R.drawable.maths_tricks ,
            R.drawable.english_tricks ,
            R.drawable.previous_asked_question ,
            R.drawable.news ,
            R.drawable.pdf_section ,
            R.drawable.railway_rrb ,
            R.drawable.bank ,
            R.drawable.ssc_h ,
            R.drawable.ncert_book ,
            R.drawable.ncert_solution
    };

    interface SharedPref {
        String IS_PAID_MCQ_SETTINGS = "IS_PAID_MCQ_SETTINGS";
        String IS_REGISTRATION_COMPLETE = "isRegComplete";
        String USER_NAME = "userName";
        String USER_PHOTO_URL = "photoUrl";
        String USER_ID_AUTO = "auto_id";
        String USER_UID = "userUid";
        String USER_EMAIL = "userEmail";
        String USER_PHONE = "userPhone";
        String USER_ADDRESS = "userAddress";
        String USER_STATE = "userState";
        String USER_POSTAL_CODE = "userPostalCode";
        String USER_POINTS = "userPoints";
        String APP_USER = "appUser";
        String FCM_TOKEN = "appUser";
        String PLAYER_ID = "PLAYER_ID";
        String IS_USER_NOT_VERIFIED = "isUserNotVerified";
        String IS_VERIFICATION_EMAIL_SENT = "isVerificationEmailSent";
        String USER_LOGIN_STATUS = "login_status" ;
        int USER_LOGIN_NOT = 0 ;
        int USER_LOGIN_HALF = 1 ;
        int USER_LOGIN_COMPLETE = 2 ;
    }

    String HOST_DOMAIN = "https://topcoaching.in/" ;

    interface Location_Constants {

        static final int SUCCESS_RESULT = 0;



        static final int FAILURE_RESULT = 1;

        static final String PACKAGE_NAME =

                "com.google.android.gms.location.sample.locationaddress";

        static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";



        static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";



        static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    }

}
