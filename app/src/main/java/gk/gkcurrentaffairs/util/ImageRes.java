package gk.gkcurrentaffairs.util;

import gk.gkcurrentaffairs.R;

/**
 * Created by Amit on 3/7/2017.
 */

public interface ImageRes {

    int[] CAA_SUB_CAT = {
            R.drawable.bharat_aur_viswa ,
            R.drawable.rastiya_ghatnakaram ,
            R.drawable.antarrastiyaghantakaram,
            R.drawable.economic,
            R.drawable.banking_ca,
            R.drawable.scienceandtech,
            R.drawable.paryavaranghatnakarma,
            R.drawable.charchitvayakti,
            R.drawable.defence_ca,
            R.drawable.person_in_news,
            R.drawable.places_in_news,
            R.drawable.puruskarandsamman,
            R.drawable.sport ,
            R.drawable.art_culture ,

    };

    int[] GOVT_JOBS_SUB_CAT = {
            R.drawable.banking ,
            R.drawable.ssc,
            R.drawable.upsc,
            R.drawable.railwaynotes,
            R.drawable.psc,
            R.drawable.teaching,
            R.drawable.engineeringjobs ,
            R.drawable.police,
            R.drawable.state_govt_jobs,
            R.drawable.graduation_jobs,
            R.drawable.governmentjobs,
            R.drawable.medical,
            R.drawable.diploma,
            R.drawable.iti,
            R.drawable.last_date,
            R.drawable.ten_jobs ,
            R.drawable.ic_result,
            R.drawable.ic_admit_card,
            R.drawable.ic_ans_key,
            R.drawable.ic_syllabus
    };

    int[] IMP_NOTES_SUB_CAT = {
            R.drawable.aptitude ,
            R.drawable.reasoning,
            R.drawable.english ,
            R.drawable.computer,
            R.drawable.banking,
            R.drawable.marketing,
            R.drawable.history ,
            R.drawable.politics,
            R.drawable.geography,
            R.drawable.economic,
            R.drawable.scienceandtech,
            R.drawable.railwaynotes,
            R.drawable.mixnotes,
            R.drawable.miscellaneous
    };

    int[] MOCK_TEST_SUB_CAT = {
            R.drawable.mock_gk ,
            R.drawable.mock_eng,
            R.drawable.mock_reasoning,
            R.drawable.mock_aptitude,
            R.drawable.mock_others,
            R.drawable.computer,
            R.drawable.banking ,
            R.drawable.ca_mock_test_
    };

    int[] PRE_YEAR_SUB_CAT = {
            R.drawable.banking ,
            R.drawable.ssc ,
            R.drawable.upsc ,
            R.drawable.railwaynotes ,
            R.drawable.rastiya_ghatnakaram ,
            R.drawable.state_govt_jobs
    };

    int[] GK_NOTES_SUB_CAT = {
            R.drawable.politics ,
            R.drawable.geography ,
            R.drawable.economic ,
            R.drawable.rastiya_ghatnakaram ,
            R.drawable.samajikmudde ,
            R.drawable.computer ,
            R.drawable.banking ,
            R.drawable.marketing ,
            R.drawable.miscellaneous ,
            R.drawable.history ,
            R.drawable.history ,
            R.drawable.history ,
            R.drawable.history ,
            R.drawable.scienceandtech ,
            R.drawable.scienceandtech ,
            R.drawable.scienceandtech ,
            R.drawable.paryavaranghatnakarma ,
            R.drawable.banking
    };

    int[] SSC_CAT ={
            R.drawable.gk ,
            R.drawable.english ,
            R.drawable.reasoning ,
            R.drawable.aptitude ,
            R.drawable.pdf_section
    };
    int[] BANK_CAT ={
            R.drawable.gk ,
            R.drawable.english ,
            R.drawable.reasoning ,
            R.drawable.aptitude ,
            R.drawable.computer ,
            R.drawable.pdf_section
    };
    int[] RRB_CAT ={
            R.drawable.gk ,
            R.drawable.reasoning ,
            R.drawable.maths_tricks ,
            R.drawable.gk_tricks ,
            R.drawable.scienceandtech ,
            R.drawable.pdf_section
    };

    int[] PDF_SEC_CAT ={
            R.drawable.gk ,
            R.drawable.english ,
            R.drawable.aptitude ,
            R.drawable.reasoning ,
            R.drawable.ca_articles ,
            R.drawable.news ,
            R.drawable.miscellaneous ,
            R.drawable.ssc_h ,
            R.drawable.bank ,
            R.drawable.railway_rrb ,
            R.drawable.upsc ,
            R.drawable.state_govt_jobs
    };

}
