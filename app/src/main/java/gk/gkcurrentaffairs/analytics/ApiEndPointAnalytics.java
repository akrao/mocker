package gk.gkcurrentaffairs.analytics;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiEndPointAnalytics {

    @POST("insert-analytics")
    Call<String> summitAnalyticsData(@Query("app_data") String app_data );

}
