package gk.gkcurrentaffairs.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import androidx.annotation.RequiresApi;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.config.RetrofitGenerator;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Amit on 9/18/2018.
 */

public class AppAnalyticsUtil {

    public static final String DATA = "data";
    private static boolean isSync = false ;

    public static void init(Context context , String host) {
            if ( !isSync ) {
                new AsyncLoadData(context,host).execute();
            }
    }

    private static boolean isSyncToServer ;

    static class AsyncLoadData extends AsyncTask<Void, Void , Void>{

        private Context context ;
        private String host ;
        JSONObject jsonObject ;

        public AsyncLoadData(Context context, String host) {
            this.context = context;
            this.host = host;
            jsonObject = new JSONObject();
            isSyncToServer = false ;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( isSyncToServer ) {
                sendDataToServer(jsonObject.toString(),host);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                isSyncToServer = startAnalytics(context , jsonObject);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidParameterSpecException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    static boolean startAnalytics(Context context, JSONObject jsonObject)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, InvalidAlgorithmParameterException
            , IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException, JSONException {
        ArrayList<JSONObject> list = new ArrayList<>();
        List<PackageInfo> packList = AppApplication.getInstance().getPackageManager().getInstalledPackages(0);

        jsonObject.put("source_app_package_name", AppApplication.getInstance().getPackageName()+"_"+ BuildConfig.BUILD_TYPE);
        jsonObject.putOpt("date_formatted", getDate(System.currentTimeMillis()));
        jsonObject.putOpt("source_app_version", getAppVersion());

        JSONObject appDataObject;

        for (int i = 0; i < packList.size(); i++) {
            PackageInfo packInfo = packList.get(i);
            if ((packInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                String appName = packInfo.applicationInfo.loadLabel(AppApplication.getInstance().getPackageManager()).toString();

                appDataObject = new JSONObject();
                appDataObject.putOpt("package_name", packInfo.applicationInfo.packageName);
                appDataObject.putOpt("app_installed_name", appName);
                appDataObject.putOpt("app_installed_date", packInfo.firstInstallTime);
                appDataObject.putOpt("app_installed_date_formatted", getDate(packInfo.firstInstallTime));
                Logger.e("startAnalytics ", appDataObject.toString());
                list.add(appDataObject);

            }
        }

        String preDataEncrypt = getDefaultSharedPref().getString(DATA, null);

        JSONObject dataObject = new JSONObject();
        dataObject.put("network_data", networkData(context));
        dataObject.put("device_id", getDeviceId());
        dataObject.putOpt("package_data", new JSONArray(list));
        String d = dataObject.toString();
        Logger.e("dataObject" , d);
        Log.e("generateKey" , generateKey().toString());
        String encryptString = bytesToHex(encryptMsg(d));

//        if (true) {
        if (SupportUtil.isEmptyOrNull( preDataEncrypt ) || !preDataEncrypt.equalsIgnoreCase( encryptString ) ){
            jsonObject.putOpt("data",encryptString);
            getDefaultSharedPref().edit().putString(DATA, encryptString).apply();
            return true ;
        }
//        Logger.e("JSON : " + jsonObject);
//        String decryptString = decryptMsg(hexStringToByteArray(encryptString));
//        Logger.e("decryptString : " + decryptString);
        return false ;
    }

    private static JSONObject networkData(Context context) {
        JSONObject jsonObject = new JSONObject();
        return jsonObject;
    }

    static byte[] hexStringToByteArray1(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private static String getAppVersion() {
        String appVersion = "0";
        try {
            PackageInfo pInfo = AppApplication.getInstance().getPackageManager().getPackageInfo(AppApplication.getInstance().getPackageName(), 0);
            appVersion = pInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersion;
    }

    private static SimpleDateFormat simpleDateFormat;

    private static String getDate(long time) {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy | hh:mm a");
        return simpleDateFormat.format(new Date(time));
    }

    private static void sendDataToServer(String data, String host) {
        try {
            isSync = true ;
            RetrofitGenerator.getClient(host)
                    .create(ApiEndPointAnalytics.class)
                    .summitAnalyticsData(Base64.encodeToString(data.getBytes("UTF-8"), Base64.DEFAULT))
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Logger.e("sendDataToServer : summitAnalyticsData ", "Response : " + response.body());
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Logger.e("sendDataToServer : summitAnalyticsData ", "onFailure : ");
                        }
                    });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String getDeviceId() {
        return Settings.Secure.getString(AppApplication.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    private static SharedPreferences sharedPreferences;

    private static SharedPreferences getDefaultSharedPref() {
        if (sharedPreferences == null) {
            sharedPreferences = AppApplication.getInstance().getDefaultSharedPref();
        }
        return sharedPreferences;
    }


    public static SecretKey generateKey()
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(getSecurityCodeByte(AppApplication.getInstance()), "ARC4");
    }

    public static byte[] encryptMsg(String message)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
        /* Encrypt the message. */
        Cipher cipher = null;
//        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher = Cipher.getInstance("ARC4");
        cipher.init(Cipher.ENCRYPT_MODE, generateKey());
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return cipherText;
    }

    public static String decryptMsg(byte[] cipherText)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException, InvalidKeySpecException {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("ARC4");
        cipher.init(Cipher.DECRYPT_MODE, generateKey());
        String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
        return decryptString;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void c(Context ctx) {
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    public static byte[] getSecurityCodeByte(Context ctx) {
        try {
            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(signature.toByteArray());
                byte[] bytes = md.digest() ;
//                Log.e("printHashKey ff", Base64.encodeToString(bytes, Base64.URL_SAFE));
//                Log.e("printHashKey HEXA", bytesToHex(bytes));
                return bytes;
            }
        } catch (PackageManager.NameNotFoundException e) {
            Logger.e("printHashKey", "SHA-1 generation: the key count not be generated: NameNotFoundException thrown");
        } catch (NoSuchAlgorithmException e) {
            Logger.e("printHashKey", "SHA-1 generation: the key count not be generated: NoSuchAlgorithmException thrown");
        }
        return null;
    }


    public static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

}
