package gk.gkcurrentaffairs.notification;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.onesignal.OneSignal;

import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.util.Logger;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

   @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

       if (BuildConfig.DEBUG) {
           FirebaseMessaging.getInstance().subscribeToTopic("DebugUser");
           OneSignal.sendTag("DebugUser", "DebugUser");
       } else {
           FirebaseMessaging.getInstance().subscribeToTopic("AllUser");
       }
        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token) {
//        Logger.e("token -- " + token );

    }
}