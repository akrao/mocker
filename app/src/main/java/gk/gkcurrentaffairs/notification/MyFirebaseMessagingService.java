package gk.gkcurrentaffairs.notification;

/**
 * Created by amit on 2/10/16.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.MainActivity;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification( remoteMessage);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        if ( remoteMessage != null && remoteMessage.getData() != null ) {

            String s = remoteMessage.getData().get(AppConstant.TYPE);
            int i = SupportUtil.isEmptyOrNull( s ) ? 0 : Integer.parseInt( s );

            if ( i == AppConstant.NOTIFICATION_API_URL && AppApplication.getInstance() != null ){
                s = remoteMessage.getData().get(AppConstant.DATA);
                if ( SupportUtil.isEmptyOrNull( s ) ) {
                    AppApplication.getInstance().invalidateApiUrl(null , true);
                }else {
                    AppPreferences.setBaseUrl( AppApplication.getInstance() , s );
                }
                return;
            }else {
                intent.putExtra( AppConstant.TYPE , s );
                s = remoteMessage.getData().get(AppConstant.DATA);
                intent.putExtra(AppConstant.DATA, s );

                s = remoteMessage.getData().get(AppConstant.CATEGORY);
                intent.putExtra(AppConstant.CATEGORY, s );
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT );
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource( getResources() , R.mipmap.ic_launcher ))
                .setContentTitle( getResources().getString( R.string.app_name ) )
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}