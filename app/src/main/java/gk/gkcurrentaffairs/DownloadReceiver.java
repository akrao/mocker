package gk.gkcurrentaffairs;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 3/27/2017.
 */

public class DownloadReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case DownloadManager.ACTION_DOWNLOAD_COMPLETE:
                Intent intent1 = new Intent(AppConstant.CLICK_ITEM_ARTICLE);
                context.sendBroadcast( intent1 );
                break;
        }
    }
}
