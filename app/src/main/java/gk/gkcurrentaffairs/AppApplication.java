package gk.gkcurrentaffairs;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import com.adssdk.AdsSDK;
import com.device_info.analytics.AnalyticsUtil;
import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.login.LoginSdk;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.multidex.MultiDexApplication;
//import gk.gkcurrentaffairs.analytics.AnalyticsUtil;
import gk.gkcurrentaffairs.bean.CatBean;
import gk.gkcurrentaffairs.bean.LocalMockData;
import gk.gkcurrentaffairs.bean.LocalMockList;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.config.RetrofitGenerator;
import gk.gkcurrentaffairs.onesignal.ResultNotificationOpenedHandler;
import gk.gkcurrentaffairs.onesignal.ResultNotificationReceivedHandler;
import gk.gkcurrentaffairs.payment.model.UserConfig;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Retrofit;

import static gk.gkcurrentaffairs.util.SupportUtil.handlePreVersionLogin;

/**
 * Created by Amit on 1/30/2017.
 */
public class AppApplication extends MultiDexApplication {

    String TAG = "amit";
    private static AppApplication AppApplication;
    private static UserConfig userConfig;
    private DbHelper dbHelper;
    /*
        private ApiEndpointInterface apiEndpointInterface;
        private ApiEndpointInterface apiEndpointInterface_1;
        private ApiPayInterface apiPayInterface;
    */
    private Picasso picasso;

    public Picasso getPicasso() {
        return picasso;
    }

    /*
        public ApiEndpointInterface getApiEndpointInterface2() {
            return apiEndpointInterface2;
        }

        private ApiEndpointInterface apiEndpointInterface2;
    */
    private static boolean showArticleAd;
    private static boolean showNCERTAd;
    private static boolean sessionAd;

    public static boolean isShowNCERTAd() {
        return showNCERTAd;
    }

    public static void setShowNCERTAd(boolean showNCERTAd) {
        gk.gkcurrentaffairs.AppApplication.showNCERTAd = showNCERTAd;
    }

    public static boolean isSessionAd() {
        return sessionAd;
    }

    public static void setSessionAd(boolean sessionAdA) {
        sessionAd = sessionAdA;
    }

    public static boolean isShowArticleAd() {
        return AppApplication.showArticleAd;
    }

    public static void setShowArticleAd(boolean showArticleAd) {
        AppApplication.showArticleAd = showArticleAd;
    }

    public SharedPreferences getDefaultSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    public static AppApplication getInstance() {
        return AppApplication;
    }

    public DbHelper getDBObject() {
        return dbHelper;
    }


    private Typeface typeface;

    public Typeface getTypeface() {
        return typeface;
    }

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppApplication = this;

//        MobileAds.initialize(this, getString(R.string.ad_id));
        initFlurry();
        dbHelper = DbHelper.getInstance(this);
        typeface = Typeface.createFromAsset(getAssets(), "rr.ttf");
        handleVersion();
        initNetworking();
        initVersion();
        setShowArticleAd(true);
        setShowNCERTAd(true);
        handleVersion();
        initOneSignal();
        picasso = Picasso.get();
        configManager = ConfigManager.getInstance();
        handlePaidMcqEnglish();
    }

    public void setPicasso() {
        this.picasso = Picasso.get();
    }

    private void handlePaidMcqEnglish() {
        if (SupportUtil.isEnglish(this)) {
            if (!SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_PAID_MCQ_SETTINGS)) {
                SharedPrefUtil.setBoolean(AppConstant.PAID_QUESTIONS_LANG, true);
                SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_PAID_MCQ_SETTINGS, true);
            }
        }
    }

    private LoginSdk loginSdk = null;

    public LoginSdk getLoginSdk() {
        return loginSdk;
    }

    private void initLoginSDK() {
        if (loginSdk == null) {
            Retrofit retrofit = null;
            if (configManager.getHostAlias() != null && configManager.getHostAlias().get(ConfigConstant.HOST_LOGIN) != null) {
                retrofit = RetrofitGenerator.getClient(configManager.getHostAlias()
                        .get(ConfigConstant.HOST_LOGIN));
            }
            loginSdk = LoginSdk.getInstance(this, retrofit, false
                    , true, true, getPackageName());
            LoginSdk.getInstance().setUserImageUrl(SupportUtil.getUserImageUrl());
            LoginSdk.getInstance().setCallConfig(new LoginSdk.CallConfig() {
                @Override
                public void callConfig(boolean isMain, boolean isBug, String bug) {
                    configManager.callConfig(isMain, isBug, bug);
                }
            });
            handlePreVersionLogin();
        }
    }

    private AdsSDK adsSDK = null;

    public void initAds() {
        if (adsSDK == null && AppConstant.IS_ADS_ENABLED) {
            adsSDK = new AdsSDK(this, BuildConfig.DEBUG, getPackageName());
            adsSDK.setIS_ADS_ENABLED(AppConstant.IS_ADS_ENABLED);
            AdsSDK.getInstance().setPageCount(adsSDK.getPAGE_COUNT_SHOW_ADS());
        }
    }

    public AdsSDK getAdsSDK() {
        return adsSDK;
    }

    private ConfigManager configManager;

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public void initOperations() {
        configManager.loadConfig();
        initAds();
        initLoginSDK();
    }

    public void syncData() {
//        getCatData();
        if (configManager.getHostAlias() != null && configManager.getHostAlias().get(ConfigConstant.HOST_LOGIN) != null) {
            LoginSdk.getInstance().setApiInterface(RetrofitGenerator.getClient(configManager.getHostAlias()
                    .get(ConfigConstant.HOST_LOGIN)));
        }
        if ( configManager != null
                && configManager.getHostAlias() != null
                && configManager.getHostAlias().size()  > 0
                && configManager.getHostAlias().get(ConfigConstant.HOST_ANALYTICS) != null ){
//            AnalyticsUtil.init(this , configManager.getHostAlias().get(ConfigConstant.HOST_ANALYTICS));
            AnalyticsUtil.getInstance(BuildConfig.BUILD_TYPE ,"")
                    .init(this, configManager.getHostAlias().get(ConfigConstant.HOST_ANALYTICS));
        }
//        syncLocalData();
    }

    private void initFlurry() {
        if (!SupportUtil.isEmptyOrNull(getString(R.string.flurry_id))) {
//            FlurryAgent.init(this, getString(R.string.flurry_id));
            new FlurryAgent.Builder()
                    .withListener(new FlurryAgentListener() {
                        @Override
                        public void onSessionStarted() {
                        }
                    })
                    .build(this, getString(R.string.flurry_id));

        }
    }

    private void initNetworking() {
/*
        apiEndpointInterface = RetrofitServiceGenerator.getClient(this, false).create(ApiEndpointInterface.class);
        apiPayInterface = RetrofitServiceGenerator.getPaymentClient(this, false).create(ApiPayInterface.class);
        apiEndpointInterface_1 = RetrofitServiceGenerator.getClient_2(this, false).create(ApiEndpointInterface.class);
        apiEndpointInterface2 = RetrofitServiceGenerator.getClient_3(this, false).create(ApiEndpointInterface.class);
*/
    }

    private void handleVersion() {
        if (!AppPreferences.isHostnameFlush(this)) {
            AppPreferences.getSharedPreferenceObj(this).edit().remove(AppPreferences.API_URL).apply();
            AppPreferences.setHostnameFlush(this, true);
        }
    }

    private void initVersion() {
        FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.VERSION_NAME);
    }

    public void invalidateApiUrl(final SupportUtil.OnCustomResponse onCustomResponse, boolean isDefault) {
/*
        Call<NetworkConfigBean> call = apiEndpointInterface.getNetworkConfig(getUrl(isDefault) + getPackageName() + ((isDefault) ? "-" + AppConstant.VERSIONING : ""));
        call.enqueue(new Callback<NetworkConfigBean>() {
            @Override
            public void onResponse(Call<NetworkConfigBean> call, Response<NetworkConfigBean> response) {
                if (response != null && response.body() != null) {
                    NetworkConfigBean networkConfigBean = response.body();
                    updateNetwork(networkConfigBean);
                    if (onCustomResponse != null)
                        onCustomResponse.onCustomResponse(true);


                    try {
                        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        if (pInfo.versionCode < networkConfigBean.getVersionCode()) {
                            LocalBroadcastManager.getInstance(AppApplication.this).sendBroadcast(new Intent(AppConstant.APP_UPDATE));
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (onCustomResponse != null)
                    onCustomResponse.onCustomResponse(false);
            }

            @Override
            public void onFailure(Call<NetworkConfigBean> call, Throwable t) {
                if (onCustomResponse != null)
                    onCustomResponse.onCustomResponse(false);
            }
        });
*/
    }


    private void initOneSignal() {
        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new ResultNotificationReceivedHandler())
                .setNotificationOpenedHandler(new ResultNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .filterOtherGCMReceivers(true)
                .init();

        OneSignal.addSubscriptionObserver(new OSSubscriptionObserver() {
            @Override
            public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
                if (!stateChanges.getFrom().getSubscribed() &&
                        stateChanges.getTo().getSubscribed()) {
                /*new AlertDialog.Builder(MainActivity.this)
                        .setMessage("You've successfully subscribed to push notifications!")
                        .show();*/
                    // get player ID
                    String playerId = stateChanges.getTo().getUserId();
                    SharedPrefUtil.setString(AppConstant.SharedPref.PLAYER_ID, playerId);
//                    Log.d("amit" , " lpay" + playerId) ;
                    syncToServer(playerId);
                }
            }
        });
        if (BuildConfig.DEBUG) {
            OneSignal.sendTag("DebugUser", "DebugUser");
        }
    }

    private void syncToServer(String playerId) {
        Map<String, String> map = new HashMap<>(4);
        map.put("device_id", SupportUtil.getDeviceId());
        map.put("player_id", playerId);
        map.put("apps", SupportUtil.installedApps());
        map.put("application_id", getPackageName());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                ApiEndPoint.STORE_FCM_TOKEN
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                    }
                });

/*
        AppApplication.getInstance().getNetworkObjectPay().syncToServer(SupportUtil.getDeviceId(), playerId
                , SupportUtil.installedApps(), getPackageName()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
*/

    }


    private void insertCatEngMock() {
        String[] strings = getResources().getStringArray(R.array.eng_mock_cat);
        int[] ints = getResources().getIntArray(R.array.eng_mock_cat_ids);
        final List<CatBean> catBeen = new ArrayList<>(ints.length);
        for (int i = 0; i < ints.length; i++) {
            catBeen.add(new CatBean(strings[i], ints[i], AppConstant.ENGLISH_MOCK_TEST_ID));
        }
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.insertCat(catBeen);
                return null;
            }
        });
    }

    private String getUrl(boolean isDef) {
        return isDef ? "http://jogindersharma.in/get-app-config/gk-hindi/?id=" : AppPreferences.getBaseUrl(this) + AppConstant.VERSIONING + "get-config?id=";
    }

    private void syncLocalData() {
        if (!AppPreferences.isLOCAL_DATA(this)) {
            Map<String, String> map = new HashMap<>(1);
            map.put("id", getPackageName());
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                    ApiEndPoint.GK_MOCK_DATA
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    final List<LocalMockData> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<LocalMockData>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        dbHelper.callDBFunction(new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                dbHelper.insertMockTestLocal(list);
                                                AppPreferences.setLOCAL_DATA(AppApplication.getApplicationContext(), true);
                                                return null;
                                            }
                                        });
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
/*
            apiEndpointInterface.localMockData(getPackageName()).enqueue(new Callback<List<LocalMockData>>() {
                @Override
                public void onResponse(Call<List<LocalMockData>> call, final Response<List<LocalMockData>> response) {
                    if (response != null && response.body() != null && response.body().size() > 0) {
                    }
                }

                @Override
                public void onFailure(Call<List<LocalMockData>> call, Throwable t) {

                }
            });
*/
        }
        if (!AppPreferences.isLOCAL_DATAList(this)) {
            Map<String, String> map = new HashMap<>(1);
            map.put("id", getPackageName());
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_MAIN,
                    ApiEndPoint.GK_MOCK_LIST
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    final List<LocalMockList> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<LocalMockList>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        dbHelper.callDBFunction(new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                dbHelper.insertMockTestListLocal(list);
                                                AppPreferences.setLOCAL_DATAList(AppApplication.getApplicationContext(), true);
                                                return null;
                                            }
                                        });
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
/*
            apiEndpointInterface.localMockList(getPackageName()).enqueue(new Callback<List<LocalMockList>>() {
                @Override
                public void onResponse(Call<List<LocalMockList>> call, final Response<List<LocalMockList>> response) {
                    if (response != null && response.body() != null && response.body().size() > 0) {
                        dbHelper.callDBFunction(new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                dbHelper.insertMockTestListLocal(response.body());
                                AppPreferences.setLOCAL_DATAList(AppApplication.getApplicationContext(), true);
                                return null;
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<List<LocalMockList>> call, Throwable t) {

                }
            });
*/
        }
    }
}