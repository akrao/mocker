package gk.gkcurrentaffairs.ncert.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adssdk.SingleNativeAdapter;

import java.util.ArrayList;
import java.util.Random;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.SubjectModel;


/**
 * Created by amit on 23/4/17.
 */

public class SubjectsAdapter extends SingleNativeAdapter {
    private ArrayList<SubjectModel> subjectModels;
    private Context context;
    private boolean isTypePDF = false ;

    public void setTypePDF(boolean typePDF) {
        isTypePDF = typePDF;
    }

    public interface OnClick {
        void onCustomItemClick(int position, boolean isBasic);
    }

    private OnClick onClick ;
    public SubjectsAdapter(Context context, ArrayList<SubjectModel> subjectModels , OnClick onClick) {
        super(subjectModels, R.layout.subject_native_list_ad_app_install, R.layout.subject_native_ad_listad_content, null );
        this.subjectModels = subjectModels;
        this.context=context;
        this.onClick=onClick;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.list_subects, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if ( holder instanceof ViewHolder ) {
            ViewHolder viewHolder = (ViewHolder) holder ;
            viewHolder.position = i ;
            viewHolder.tv_class_circle_text.setText(String.valueOf(subjectModels.get(i).getTitle().charAt(0)));
            viewHolder.tv_class_text.setText(subjectModels.get(i).getTitle());
            ((GradientDrawable)viewHolder.ll_circle_color.getBackground()).setColor(Color.parseColor( colors[ getRandomNum() ] ));
            if(subjectModels.get(i).getisDownloaded())
                viewHolder.card.setCardBackgroundColor(Color.parseColor("#C0C0C0"));
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_class_circle_text,tv_class_text;
        private LinearLayout ll_circle_color,ll_background;
        private CardView card;
        int position ;
        public ViewHolder(View view) {
            super(view);
            tv_class_circle_text = (TextView)view.findViewById(R.id.tv_class_circle_text);
            tv_class_text = (TextView)view.findViewById(R.id.tv_class_text);
            ll_circle_color = (LinearLayout) view.findViewById(R.id.ll_circle_color);
            ll_background = (LinearLayout) view.findViewById(R.id.ll_background);
            card=(CardView)view.findViewById(R.id.cardview1);
            if ( isTypePDF ) {
                view.findViewById(R.id.ll_pdf).setVisibility(View.VISIBLE);
                view.findViewById(R.id.bt_basic).setOnClickListener(this);
                view.findViewById(R.id.bt_advance).setOnClickListener(this);
            }
            view.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            if ( onClick != null ) {
                onClick.onCustomItemClick(position, view.getId() == R.id.bt_basic);
            }
        }

    }

    private int getRandomNum(){
        Random r = new Random();
        return r.nextInt(7);
    }

    private String[] colors = {
            "#13c4a5","#10a4b8","#8a63b3","#3b5295","#fdbd57","#f6624e","#e7486b","#9c4274"};

}