package gk.gkcurrentaffairs.ncert.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.ncert.CheckForSDCard;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;

public class NCERTBrowserActivity extends PageAdsAppCompactActivity {

    // button to show progress dialog
    WebView wv_onlinepdf;


    private static final String TAG = "Download Task";
    File apkStorage = null;
    File outputFile = null;
    String downloadFileName="";
    int BookId=0;

    // Progress Dialog
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    // File url to download
    private String file_url = "";
    private boolean isFileExist ;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        //get data from intent
        getDataIntent();

        // initilization of views
        init();

        if (shouldAskPermissions()) {
            askPermissions();
        }

        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + downloadFileName;
        File file = new File(filePath);
        OnlinePdfView();
        isFileExist = file.exists();
        if ( isFileExist )
            downloadAndOpen();
/*
        if(file.exists()){
            Toast.makeText(this,"file already exit",Toast.LENGTH_SHORT).show();
            checkvisibility(true);
            downloadAndOpen();
        }else {
            checkvisibility(false);
            file_url = AppPreferences.getBaseUrl_3(AppApplication.getInstance()) + "download-pdf/" + BookId + "/" + downloadFileName;
            btnonlinepdf.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // starting new Async Task
                }
            });
            btnShowProgress.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // starting new Async Task
                    new DownloadFileFromURL().execute(file_url);
                }
            });
        }
*/

        SupportUtil.initAds( (RelativeLayout) findViewById( R.id.ll_ad ) , this , AppConstant.ADS_BANNER );
    }

    void init(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        wv_onlinepdf = (WebView) findViewById(R.id.webView);
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    MenuItem menuItem ;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.menu_pdf_save ,menu);
        if (isFileExist) {
            menuItem = menu.findItem(R.id.action_pdf_save);
            menuItem.setIcon(R.drawable.downloaded);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            this.finish();
            return true ;
        }else if( id == R.id.action_share ){
            SupportUtil.share( "" , this );
            return true ;
        }else if( id == R.id.action_pdf_save ){
            file_url = AppPreferences.getBaseUrl_3(AppApplication.getInstance()) + "download-pdf/" + BookId + "/" + downloadFileName;
            if (isFileExist) {
                downloadAndOpen();
            } else {
                new DownloadFileFromURL().execute(file_url);
            }
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }


    void checkvisibility(Boolean isPdfDownloaded){

/*
        if(isPdfDownloaded) {
            btnShowProgress.setVisibility(View.GONE);
            btnonlinepdf.setVisibility(View.GONE);
            wv_onlinepdf.setVisibility(View.VISIBLE);
        }else {
            btnShowProgress.setVisibility(View.VISIBLE);
            btnonlinepdf.setVisibility(View.VISIBLE);
            wv_onlinepdf.setVisibility(View.GONE);
        }

*/
    }

    void OnlinePdfView(){
        wv_onlinepdf.getSettings().setJavaScriptEnabled(true);
        wv_onlinepdf.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv_onlinepdf.setWebViewClient( new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                view.loadUrl(url);
                return true ;
            }

        });
        wv_onlinepdf.loadUrl(AppPreferences.getBaseUrl_3(AppApplication.getInstance())+"show-pdf/"+BookId+"/"+downloadFileName);
        checkvisibility(true);

    }

    private void grantAllUriPermissions(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    void downloadAndOpen(){

        String filePath = Environment.getExternalStorageDirectory() + "/" + AppConstant.downloadDirectory + "/" + downloadFileName;

        File file = new File(filePath);
        if (file.exists()) {

            Intent intent ;
            intent = new Intent(Intent.ACTION_VIEW);
            // Uri path = Uri.fromFile(file);

            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkURI = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file);
                intent.setDataAndType(apkURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                grantAllUriPermissions(this, intent, apkURI);

            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
            }

            //intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
                this.finish();
            } catch (ActivityNotFoundException e) {
                showLocationDialog();
                Toast.makeText(this,
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    private void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle(getString(R.string.dialog_title));
        builder.setMessage(getString(R.string.dialog_message));

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        SupportUtil.downloadPdfReader(NCERTBrowserActivity.this);
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        checkvisibility(false);
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

            //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + AppConstant.downloadDirectory);
                } else
                    Toast.makeText(NCERTBrowserActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(outputFile);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            try {
                dismissDialog(progress_bar_type);
                pDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            downloadAndOpen();
            checkvisibility(true);
            Toast.makeText(NCERTBrowserActivity.this,"download complete",Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected boolean shouldAskPermissions() {

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        }
        return false;

    }

    @TargetApi(23)
    protected void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    public void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            BookId=bundle.getInt(AppConstant.BOOKID);
            downloadFileName=bundle.getString(AppConstant.imageName);
        }else {
            finish();
        }

    }


 }
