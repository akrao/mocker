package gk.gkcurrentaffairs.ncert.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.DataAdapter;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

public class ClassesActivity extends PageAdsAppCompactActivity {


    private SectionsPagerAdapter adapter;
    private ViewPager mViewPager;
    private String BookType="";
    private InterstitialAd mInterstitialAd ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);

        getDataIntent();

        setupToolbar();

        setupViewPager();

        initFullAds();
        RelativeLayout rlAds = (RelativeLayout) findViewById( R.id.ll_ad ) ;
        SupportUtil.initAds( rlAds , this , AppConstant.ADS_BANNER );

    }

    private void initFullAds(){
        if ( AppConstant.IS_ADS_ENABLED && AppApplication.isShowNCERTAd()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
            AdRequest.Builder builder = new AdRequest.Builder();
            builder.addTestDevice("5E254AC1CF02E640413645E46C8A1A64");
            mInterstitialAd.loadAd(builder.build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    ClassesActivity.this.finish();
                }
            });
        }
    }

    private void showInterstitial() {
        if ( mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            AppApplication.setShowNCERTAd(false);
        }else
            ClassesActivity.this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_classes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       if(id==android.R.id.home){
             showInterstitial();
             return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showInterstitial();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        private String[] classes;
        int[] classesIdsArray;
        int[] hindiclassesIdsArray;
        int[] urduclassesIdsArray;
        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_classes, container, false);

            classes= new String[] {"Class XII","Class XI","Class X","Class IX","Class VIII","Class VII","Class VI"};

            if(getArguments().getInt(AppConstant.LANG)==AppConstant.NCERTEnglishId)
                classesIdsArray = getActivity().getResources().getIntArray(R.array.english_ncert_cat_id);
            if(getArguments().getInt(AppConstant.LANG)==AppConstant.NCERTHindiId)
                classesIdsArray = getActivity().getResources().getIntArray(R.array.hindi_ncert_cat_id);
            if(getArguments().getInt(AppConstant.LANG)==AppConstant.NCERTUrduId)
                classesIdsArray = getActivity().getResources().getIntArray(R.array.urdu_ncert_cat_id);
            if(getArguments().getInt(AppConstant.LANG)==AppConstant.NCERTSOLUCTIONENGLISHId)
                classesIdsArray = getActivity().getResources().getIntArray(R.array.sol_ncert_cat_id);

            initViews(rootView);
            return rootView;
        }

        private void initViews(View rootView){
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.card_recycler_view);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),3);
            recyclerView.setLayoutManager(layoutManager);
            RecyclerView.Adapter adapter = new DataAdapter(getActivity(),classes);
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

                    @Override public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }

                });
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    View child = rv.findChildViewUnder(e.getX(), e.getY());
                    if(child != null && gestureDetector.onTouchEvent(e)) {
                        int position = rv.getChildAdapterPosition(child);
                        if(classes.length!=2) {
                            Intent categoryIntent = new Intent(getActivity(), SubjectsActivity.class);
                            categoryIntent.putExtra(AppConstant.classId, classesIdsArray[position]);
                            startActivity(categoryIntent);
                        }else {
                            Intent categoryIntent = new Intent(getActivity(), BooksActivity.class);
                            categoryIntent.putExtra(AppConstant.SUBJECTID, classesIdsArray[position]);
                            categoryIntent.putExtra(AppConstant.SUBJECTNAME, classes[position]);
                            startActivity(categoryIntent);
                        }
                    }
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
        }
    }

    private void setupViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViewPager(ViewPager viewPager) {

            adapter = new SectionsPagerAdapter(getSupportFragmentManager());

                Bundle ebundle = new Bundle();
                PlaceholderFragment bornfargment = new PlaceholderFragment();

                if(BookType.equalsIgnoreCase(AppConstant.NCERTBOOKS))
                    ebundle.putInt(AppConstant.LANG, AppConstant.NCERTEnglishId);
                else if(BookType.equalsIgnoreCase(AppConstant.NCERTSOLUTIONENGLISH))
                    ebundle.putInt(AppConstant.LANG, AppConstant.NCERTSOLUCTIONENGLISHId);

                bornfargment.setArguments(ebundle);
                adapter.addFrag(bornfargment, AppConstant.ENGLUSH);


            if(BookType.equalsIgnoreCase(AppConstant.NCERTBOOKS)) {
                Bundle hbundle = new Bundle();
                PlaceholderFragment diedfargment = new PlaceholderFragment();
                hbundle.putInt(AppConstant.LANG, AppConstant.NCERTHindiId);
                diedfargment.setArguments(hbundle);
                adapter.addFrag(diedfargment, AppConstant.HINDI);
            }

            if(BookType.equalsIgnoreCase(AppConstant.NCERTBOOKS)) {
                Bundle ubundle = new Bundle();
                PlaceholderFragment urdufargment = new PlaceholderFragment();
                ubundle.putInt(AppConstant.LANG, AppConstant.NCERTUrduId);
                urdufargment.setArguments(ubundle);
                adapter.addFrag(urdufargment, AppConstant.URDU);
            }


        viewPager.setAdapter(adapter);

    }

    static class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    public void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            BookType=bundle.getString(AppConstant.BOOKTYPE);
        }else {
            finish();
        }

    }
}
