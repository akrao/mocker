package gk.gkcurrentaffairs.ncert.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.AndroidDownloadFileByProgressBarActivity;
import gk.gkcurrentaffairs.bean.SubjectModel;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.ncert.adapter.SubjectsAdapter;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

public class BooksActivity extends PageAdsAppCompactActivity implements SupportUtil.OnCustomResponseNet , SubjectsAdapter.OnClick {

    private int subjectId=0;
    private String subjectName="";
    private ArrayList<SubjectModel> beanArrayList = new ArrayList<>();
    private SubjectsAdapter adapter;
    private View llNoData;
    private Boolean isFirstHit=true;
    private DbHelper dbHelper ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getDataIntent();

        dbHelper = AppApplication.getInstance().getDBObject();
        if(!SupportUtil.isNotConnected(this)) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    int maxId=dbHelper.fetchMaxBookId(subjectId);
                    SupportUtil.fetchBooksPdfName(subjectId, maxId, BooksActivity.this);
                    return null;
                }
            });
        }else {
            Toast.makeText(this,"Internet is not working Please check your internat",Toast.LENGTH_SHORT).show();
        }
        init();
        RelativeLayout rlAds = (RelativeLayout) findViewById( R.id.ll_ad ) ;
        SupportUtil.initAds( rlAds , this , AppConstant.ADS_BANNER );
    }

    @Override
    protected void onResume() {
        super.onResume();
        new DataFromDB().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_classes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
         if(id==android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void init(){

        llNoData = findViewById(R.id.ll_no_data);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter= new SubjectsAdapter(this,beanArrayList,this);
//        adapter.setTypePDF(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCustomItemClick(int position, boolean isBasic) {
        Intent categoryIntent = new Intent(this, AndroidDownloadFileByProgressBarActivity.class);
        categoryIntent.putExtra(AppConstant.imageName, beanArrayList.get(position).getPdf());
        categoryIntent.putExtra(AppConstant.BOOKID, beanArrayList.get(position).getId());
        categoryIntent.putExtra(AppConstant.isAdvanced, false);
        categoryIntent.putExtra(AppConstant.CATEGORY, AppPreferences.getBaseUrl_3(this));
        categoryIntent.putExtra(AppConstant.HOST, ConfigConstant.HOST_TRANSLATOR);
        startActivity(categoryIntent);
    }

    public void getDataIntent() {

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            subjectId=bundle.getInt(AppConstant.SUBJECTID);
            subjectName=bundle.getString(AppConstant.SUBJECTNAME);
        }else {
            finish();
        }

    }

    @Override
    public void onCustomResponse(boolean result, String string) {
        isFirstHit=false;
        new DataFromDB().execute();
    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.fetchBooksPdfNames(beanArrayList,subjectId);
                    return null;
                }
            });
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (beanArrayList.size() > 0) {
                llNoData.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            } else if (!isFirstHit){
                findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                findViewById(R.id.player_progressbar).setVisibility(View.GONE);
            }
        }
    }
}
