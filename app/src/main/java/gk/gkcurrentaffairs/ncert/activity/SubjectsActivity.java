package gk.gkcurrentaffairs.ncert.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.adssdk.PageAdsAppCompactActivity;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.SubjectModel;
import gk.gkcurrentaffairs.ncert.adapter.SubjectsAdapter;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;


public class SubjectsActivity extends PageAdsAppCompactActivity implements SupportUtil.OnCustomResponseNet , SubjectsAdapter.OnClick{

    int classId=0;
    private ArrayList<SubjectModel> beanArrayList = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private View llNoData;
    private Boolean isFirstHit=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getDataIntent();

        new DataFromDB().execute();

        if(!SupportUtil.isNotConnected(this))
            SupportUtil.fetchSubjectTitle(classId,this,this);

        init();
        RelativeLayout rlAds = (RelativeLayout) findViewById( R.id.ll_ad ) ;
        SupportUtil.initAds( rlAds , this , AppConstant.ADS_BANNER );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_classes, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
         if(id==android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void init(){

        llNoData = findViewById(R.id.ll_no_data);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter= new SubjectsAdapter(this,beanArrayList , this);
        recyclerView.setAdapter(adapter);
    }

    public void getDataIntent() {

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            classId=bundle.getInt(AppConstant.classId);
        }else {
            finish();
        }

    }

    @Override
    public void onCustomResponse(boolean result, String string) {
        isFirstHit=false;
        new DataFromDB().execute();
    }

    @Override
    public void onCustomItemClick(int position, boolean isBasic) {
        Intent categoryIntent = new Intent(SubjectsActivity.this, BooksActivity.class);
        categoryIntent.putExtra(AppConstant.SUBJECTID, beanArrayList.get(position).getId());
        categoryIntent.putExtra(AppConstant.SUBJECTNAME, beanArrayList.get(position).getTitle());
        startActivity(categoryIntent);

    }

    class DataFromDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            final DbHelper dbHelper = AppApplication.getInstance().getDBObject() ;
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.fetchQuoteData(beanArrayList,classId);
                    return null;
                }
            });
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (beanArrayList.size() > 0) {
                llNoData.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            } else if (!isFirstHit){
                findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                findViewById(R.id.player_progressbar).setVisibility(View.GONE);
            }
        }
    }
}
