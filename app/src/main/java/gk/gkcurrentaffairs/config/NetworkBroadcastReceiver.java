package gk.gkcurrentaffairs.config;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import gk.gkcurrentaffairs.activity.MainActivity;

/**
 * Created by Amit on 4/13/2018.
 */

public class NetworkBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ( MainActivity.active ){
            ConfigManager.getInstance().refreshConfig();
        }
    }
}
