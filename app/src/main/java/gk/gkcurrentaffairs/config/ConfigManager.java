package gk.gkcurrentaffairs.config;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Amit on 3/28/2018.
 */

public class ConfigManager {

    private static ConfigManager configManager;
    private Context context;
    private ConfigPreferences configPreferences;
    private int backupConfigCallCount = 0;
    private HashMap<String, String> hostAlias;
    private HashMap<String, ApiInterface> apiInterfaceHashMap;
    private HashMap<String, HashMap<String, String>> apiHostHashMap;
    private boolean isConfigLoaded = false;

    public HashMap<String, String> getHostAlias() {
        return hostAlias;
    }

    /**
     * It Return ApiInterFace Object. It can return null so please check the null before using it.
     *
     * @param host     -- Its the HostSection Title
     * @param endPoint -- Its the API name
     * @return
     */
    public ApiInterface getApiInterface(String host, String endPoint) {
        ApiInterface apiInterface = null;
        if (apiHostHashMap != null) {
            if (apiHostHashMap.get(host) != null && apiHostHashMap.get(host).get(endPoint) != null) {
                String endPointHost = apiHostHashMap.get(host).get(endPoint);
                apiInterface = getHostInterface(endPointHost);
            } else if (connectHostHashMap.get(host) != null && connectHostHashMap.get(host) && hostAlias.get(host) != null) {
                String endPointHost = hostAlias.get(host);
                apiInterface = getHostInterface(endPointHost);
            }
        }
        return apiInterface;
    }

    private ApiInterface getHostInterface(String endPointHost) {
        if (apiInterfaceHashMap.get(endPointHost) != null) {
            return apiInterfaceHashMap.get(endPointHost);
        } else {
            return RetrofitGenerator.getClient(endPointHost).create(ApiInterface.class);
        }
    }

    public boolean isConfigLoaded() {
        return isConfigLoaded;
    }

    public static ConfigManager getInstance() {
        if (configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }

    private void sendBroadCast() {
        context.sendBroadcast(new Intent(context.getPackageName() + ConfigConstant.CONFIG_LOADED));
    }

    private void sendBroadCastFailure() {
        context.sendBroadcast(new Intent(context.getPackageName() + ConfigConstant.CONFIG_FAILURE));
    }

    private ConfigManager() {
        init(AppApplication.getInstance());
    }

    private void init(Context context) {
        if (context != null) {
            this.context = context;
            configPreferences = new ConfigPreferences(context);
        }
    }

    public void loadConfig() {
        refreshConfig();
    }

    public void refreshConfig() {
        callConfig(true, false, null);
    }

    private void callBackUpConfig(String error) {
        callConfig(false, true, error);
    }

    private String getHostConfigPath() {
        String s = configPreferences.getString(ConfigConstant.CONFIG_HOST);
        if (SupportUtil.isEmptyOrNull(s)) {
            s = ConfigConstant.CONFIG_HOST_URL;
        }
        return s;
    }

    private String getBackupHostConfigPath() {
        String s = configPreferences.getString(ConfigConstant.CONFIG_HOST_BACKUP);
        if (SupportUtil.isEmptyOrNull(s)) {
            s = ConfigConstant.CONFIG_HOST_BACKUP_URL;
        }
        return s;
    }

    private boolean isConfigLoading = false;

    public void callConfig(final boolean isMain, boolean isBug, final String bug) {
        if (SupportUtil.isConnected(context) && !isConfigLoading) {
            isConfigLoading = true;
            String host = isMain ? getHostConfigPath() : getBackupHostConfigPath();
            Retrofit retrofit = RetrofitGenerator.getClient(host);
            Call<ConfigModel> call = null;
            if (isMain) {
                call = retrofit.create(ApiConfig.class).getConfig(context.getPackageName(), getAppVersion());
            } else if (isMain && isBug) {
                call = retrofit.create(ApiConfig.class).getConfigBug(context.getPackageName(), bug, getAppVersion());
            } else if (backupConfigCallCount <= ConfigConstant.BACKUP_CONFIG_CALL_COUNT) {
                call = retrofit.create(ApiConfigBackup.class).getConfig(context.getPackageName(), bug, getAppVersion());
                backupConfigCallCount++;
            }

            if (call != null) {
                Logger.e("Config api called");
                call.enqueue(new Callback<ConfigModel>() {
                    @Override
                    public void onResponse(Call<ConfigModel> call, Response<ConfigModel> response) {
                        Logger.e("Config onResponse");
                        Logger.e("Config onResponse code - " + response.code());
                        isConfigLoading = false;
                        if (response != null & response.code() != 0) {
                            int responseCode = response.code();
                            if (responseCode == 200) {
                                handleConfigResponse(response.body());
                                sendBroadCast();
                            } else if (responseCode == INTERNAL_SERVER_ERROR || responseCode == NOT_FOUND
                                    || responseCode == BAD_GATEWAY || responseCode == SERVICE_UNAVAILABLE
                                    || responseCode == GATEWAY_TIMEOUT) {
                                if (isMain) {
                                    callBackUpConfig(getMainConfigError(responseCode, response.message(), bug, getRequestBody(call)));
                                }else {
                                    sendBroadCastFailure();
                                }
                            }else
                                sendBroadCastFailure();
                        }else
                            sendBroadCastFailure();
                    }

                    @Override
                    public void onFailure(Call<ConfigModel> call, Throwable t) {
                        isConfigLoading = false;
                        if (t instanceof JSONException) {
                            if (isMain) {
                                callBackUpConfig(getMainConfigError(JSON_EXCEPTION, t.getMessage(), bug, getRequestBody(call)));
                            }else
                                sendBroadCastFailure();
                        }else
                            sendBroadCastFailure();
                    }
                });
            } else {
                isConfigLoading = false;
                sendBroadCastFailure();
            }
        }
    }

    public String getRequestBody(Call call) {
        String api = "";
        if (call != null && call.request() != null)
            api = call.request().toString();
        return api;

    }

    private String getAppVersion() {
        String appVersion = "0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersion;
    }

    public static final int JSON_EXCEPTION = 1000;

    private String getMainConfigError(int code, String msg, String apiMsg, String api) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt("response_code", code);
            object.putOpt("response_msg", getErrorTypeMsg(code));
            object.putOpt("timestamp", getFormatDate(System.currentTimeMillis()));
            object.putOpt("device_id", SupportUtil.getDeviceId());
            object.putOpt("msg", msg);
            object.putOpt("api_failure", apiMsg);
            object.putOpt("api", api);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    SimpleDateFormat simpleDateFormat;

    private String getFormatDate(long time) {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(time));
    }


    public String getApiConfigError(int code, String msg, String apiMsg) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt("response_code", code);
            object.putOpt("response_msg", getErrorTypeMsg(code));
            object.putOpt("timestamp", DateFormat.getInstance().format(new Date(System.currentTimeMillis())));
            object.putOpt("device_id", SupportUtil.getDeviceId());
            object.putOpt("msg", msg);
            object.putOpt("api_failure", apiMsg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    private static Gson gson = new Gson();

    public static Gson getGson() {
        return gson;
    }

    public void getData(int type, String host, final String endPoint, Map<String, String> param
            , final OnNetworkCall onNetworkCall) {
        if (BuildConfig.DEBUG) {
            getDataDebug1(type, host, endPoint, param, onNetworkCall);
        } else {
            getDataRelease(type, host, endPoint, param, onNetworkCall);
        }
    }

    private ApiInterface debugApiInterface;

    public void getDataDebug(int type, String host, final String endPoint, Map<String, String> param
            , final OnNetworkCall onNetworkCall) {
        if (debugApiInterface == null) {
            debugApiInterface = RetrofitGenerator.getClient("https://topcoaching.in/api/debug/").create(ApiInterface.class);
        }
        if (SupportUtil.isConnected(context) && isConfigLoaded) {
            ApiInterface apiInterface;
            if (host.equalsIgnoreCase(ConfigConstant.HOST_PAID)) {
                apiInterface = debugApiInterface;
            } else {
                apiInterface = getApiInterface(host, endPoint);
            }
            if (apiInterface != null) {
                Logger.e("getData -- " + endPoint);
                Call<BaseModel> call = getCall(type, apiInterface, endPoint, param);
                call.enqueue(new ResponseCallBack(onNetworkCall, endPoint));
            } else if (onNetworkCall != null) {
                onNetworkCall.onComplete(false, "");
            }
        } else if (onNetworkCall != null) {
            onNetworkCall.onComplete(false, "");
        }
    }

    public void getDataRelease(int type, String host, final String endPoint, Map<String, String> param
            , final OnNetworkCall onNetworkCall) {
//        if (SupportUtil.isConnected(context) && isConfigLoaded) {
        if (SupportUtil.isConnected(context) ) {
            ApiInterface apiInterface = getApiInterface(host, endPoint);
            if (apiInterface != null) {
                Logger.e("getData -- " + endPoint);
                Call<BaseModel> call = getCall(type, apiInterface, endPoint, param);
                call.enqueue(new ResponseCallBack(onNetworkCall, endPoint));
            } else if (onNetworkCall != null) {
                onNetworkCall.onComplete(false, "");
            }
        } else if (onNetworkCall != null) {
            onNetworkCall.onComplete(false, "");
        }
    }
    public void getDataDebug1(int type, String host, final String endPoint, Map<String, String> param
            , final OnNetworkCall onNetworkCall) {
//        if (SupportUtil.isConnected(context) && isConfigLoaded) {
        if (SupportUtil.isConnected(context) ) {
            ApiInterface apiInterface = getApiInterface(host, endPoint);
            if (apiInterface != null) {
                Logger.e("getData -- " + endPoint);
                Call<BaseModel> call = getCall(type, apiInterface, endPoint, param);
                call.enqueue(new ResponseCallBack(onNetworkCall, endPoint));
            } else if (onNetworkCall != null) {
                onNetworkCall.onComplete(false, "");
            }
        } else if (onNetworkCall != null) {
            onNetworkCall.onComplete(false, "");
        }
    }

    private Call<BaseModel> getCall(int callType, ApiInterface apiInterface, String endPoint, Map<String, String> param) {
        Call<BaseModel> call;
        switch (callType) {
            case ConfigConstant.CALL_TYPE_POST:
                call = apiInterface.postData(endPoint, param);
                break;
            case ConfigConstant.CALL_TYPE_POST_FORM:
                call = apiInterface.postDataForm(endPoint, param);
                break;
            case ConfigConstant.CALL_TYPE_GET:
            default:
                call = apiInterface.getData(endPoint, param);
                break;
        }
        return call;
    }

    private String getErrorTypeMsg(int code) {
        String msg;
        switch (code) {
            case INTERNAL_SERVER_ERROR:
                msg = "INTERNAL_SERVER_ERROR";
                break;
            case NOT_FOUND:
                msg = "NOT_FOUND";
                break;
            case BAD_GATEWAY:
                msg = "BAD_GATEWAY";
                break;
            case SERVICE_UNAVAILABLE:
                msg = "SERVICE_UNAVAILABLE";
                break;
            case GATEWAY_TIMEOUT:
                msg = "GATEWAY_TIMEOUT";
                break;
            case JSON_EXCEPTION:
                msg = "JSON_EXCEPTION";
                break;
            default:
                msg = "error";
                break;
        }
        return msg;
    }

    private HashMap<String, String> apiJsonException;
    private HashMap<String, String> apiErrorCode;
    private static boolean isCallConfig = false;

    public static boolean isCallConfig() {
        return isCallConfig;
    }

    public static void setIsCallConfig(boolean isCallConfig) {
        ConfigManager.isCallConfig = isCallConfig;
    }

    public interface OnNetworkCall {
        void onComplete(boolean status, String data);
    }

    private void handleConfigResponse(ConfigModel model) {
        if (model != null) {
            isConfigLoaded = true;
            configPreferences.putString(ConfigConstant.CONFIG_HOST, model.getConfig_host());
            configPreferences.putString(ConfigConstant.CONFIG_HOST_BACKUP, model.getBackup_host());
            handleHostSection(model.getHost_section());
            Logger.e("Config is loaded");
            apiJsonException = new HashMap<>();
            apiErrorCode = new HashMap<>();

            handleAppVersion(model.getApp_version());
        }
    }

    private void handleAppVersion(AppVersionModel model) {
        if (model != null) {
            int myVersion = Integer.parseInt(getAppVersion());
            try {
                int minSupportVersion = SupportUtil.isEmptyOrNull(model.getMin_version()) ? 0 : Integer.parseInt(model.getMin_version());
                if (myVersion != 0 && minSupportVersion != 0 && myVersion <= minSupportVersion) {
                    sendBroadCast(model, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String s = model.getNot_supported_version();
                if (!SupportUtil.isEmptyOrNull(s)) {
                    if (s.contains(",")) {
                        String[] arr = s.split(",");
                        for (String s1 : arr) {
                            int notSupportVersion = SupportUtil.isEmptyOrNull(s1) ? 0 : Integer.parseInt(s1);
                            if (myVersion != 0 && notSupportVersion != 0 && myVersion == notSupportVersion) {
                                sendBroadCast(model, false);
                            }
                        }
                    } else {
                        int notSupportVersion = SupportUtil.isEmptyOrNull(model.getNot_supported_version()) ? 0 : Integer.parseInt(model.getNot_supported_version());
                        if (myVersion != 0 && notSupportVersion != 0 && myVersion == notSupportVersion) {
                            sendBroadCast(model, false);
                        }
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            int currentVersion = 0;
            try {
                currentVersion = SupportUtil.isEmptyOrNull(model.getCurrent_version()) ? 0 : Integer.parseInt(model.getCurrent_version());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (myVersion != 0 && myVersion < currentVersion) {
                sendBroadCast(model, true);
            }
        }
    }

    private void sendBroadCast(AppVersionModel model, boolean type) {
        Intent intent = new Intent(context.getPackageName() + AppConstant.APP_UPDATE);
        intent.putExtra(AppConstant.TITLE, SupportUtil.isEmptyOrNull(model.getMsg()) ? AppConstant.MSG_UPDATE : model.getMsg());
        intent.putExtra(AppConstant.TYPE, type);
        context.sendBroadcast(intent);
    }

    private HashMap<String, Boolean> connectHostHashMap;

    private void handleHostSection(List<HostSectionModel> models) {
        if (models != null && models.size() > 0) {
            hostAlias = new HashMap<>(models.size());
            connectHostHashMap = new HashMap<>(models.size());
            apiInterfaceHashMap = new HashMap<>();
            ApiInterface apiInterface;
            apiHostHashMap = new HashMap<>();
            for (HostSectionModel model : models) {
                connectHostHashMap.put(model.getTitle(), model.getConnect_to_host().equalsIgnoreCase(ConfigConstant.TRUE));
                if (!SupportUtil.isEmptyOrNull(model.getConnect_to_host())
                        && model.getConnect_to_host().equalsIgnoreCase(ConfigConstant.TRUE)) {
                    hostAlias.put(model.getTitle(), model.getHost());
                    Logger.e("HOST : " + model.getTitle());
                    if (model.getTitle().equalsIgnoreCase(ConfigConstant.HOST_DOWNLOAD_PDF)) {
                        Logger.e("HOST_DOWNLOAD_PDF : " + model.getHost());
                        AppPreferences.setBaseUrl(context, model.getHost());
                        Logger.e("HOST_DOWNLOAD_PDF Pref : " + AppPreferences.getBaseUrl(context));
                    }
                    if (model.getTitle().equalsIgnoreCase(ConfigConstant.HOST_TRANSLATOR)) {
                        AppPreferences.setBaseUrl_3(context, model.getHost());
                    }
                    configPreferences.putString(model.getTitle(), model.getHost());
                    apiInterface = RetrofitGenerator.getClient(model.getHost()).create(ApiInterface.class);
                    apiInterfaceHashMap.put(model.getHost(), apiInterface);
                    handleApiHost(model.getTitle(), model.getApi_host());
                }
            }
        }
    }

    public HashMap<String, String> getApiJsonException() {
        return apiJsonException;
    }

    public HashMap<String, String> getApiErrorCode() {
        return apiErrorCode;
    }

    private void handleApiHost(String title, List<ApiHostModel> models) {

        if (models != null && models.size() > 0) {
            HashMap<String, String> map = new HashMap<>(models.size());
            for (ApiHostModel model : models) {
                map.put(model.getApi_name(), model.getApi_host());
            }
            apiHostHashMap.put(title, map);
        }
    }

    private final int NOT_FOUND = 404;
    private final int INTERNAL_SERVER_ERROR = 500;
    private final int BAD_GATEWAY = 502;
    private final int SERVICE_UNAVAILABLE = 503;
    private final int GATEWAY_TIMEOUT = 504;
}
