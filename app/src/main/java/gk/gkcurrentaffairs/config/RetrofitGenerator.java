package gk.gkcurrentaffairs.config;

import java.io.IOException;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.util.SupportUtil;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Amit on 3/28/2018.
 */

public class RetrofitGenerator {

    public static Retrofit getClient( String host ) {
        Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl( host )
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient().build())
                    .build();
        return retrofit;
    }

    private static OkHttpClient.Builder getHttpClient() {
        if ( BuildConfig.DEBUG ) {
            return new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor( interceptor );
        } else {
            return new OkHttpClient.Builder()
                    .addInterceptor( interceptor );
        }
    }

    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    private static Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Authorization", SupportUtil.getSecurityCode(AppApplication.getInstance()))
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    };

}
