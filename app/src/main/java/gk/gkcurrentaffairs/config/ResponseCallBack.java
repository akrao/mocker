package gk.gkcurrentaffairs.config;

import com.google.gson.Gson;

import org.json.JSONException;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.payment.model.Config;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gk.gkcurrentaffairs.config.ConfigManager.JSON_EXCEPTION;

/**
 * Created by Amit on 4/10/2018.
 */

public class ResponseCallBack implements Callback<BaseModel> {

    private ConfigManager.OnNetworkCall onNetworkCall ;
    private String endPoint ;

    public ResponseCallBack(ConfigManager.OnNetworkCall onNetworkCall, String endPoint ) {
        this.onNetworkCall = onNetworkCall;
        this.endPoint = endPoint;
    }

    private ResponseCallBack() {
    }

    @Override
    public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
        Logger.e( "onResponse" );
        Logger.e( "onResponse code - " + response.code() );
        if (response != null & response.code() != 0) {
            int responseCode = response.code();
            if (responseCode == 200) {
                if ( response.body() != null  && response.body() instanceof BaseModel ) {
                    BaseModel baseModel = ( BaseModel ) response.body();
                    boolean callConfig = !SupportUtil.isEmptyOrNull(baseModel.getCall_config())
                            && baseModel.getCall_config().equals(ConfigConstant.TRUE);
                    if (callConfig && !ConfigManager.isCallConfig()) {
                        ConfigManager.setIsCallConfig(true);
                        String bug = ConfigManager.getInstance().getApiConfigError(responseCode ,
                                "Call_config=" + baseModel.getCall_config()
                                , ConfigManager.getInstance().getRequestBody(call));
                        ConfigManager.getInstance().callConfig(true, true, bug);
                    }
                    boolean status = !callConfig && !SupportUtil.isEmptyOrNull(baseModel.getStatus())
                            && baseModel.getStatus().equals(ConfigConstant.SUCCESS);
                    String s = ConfigManager.getGson().toJson( baseModel.getData() ) ;
                    if ( onNetworkCall != null ) {
                        Logger.e( "onResponse s -- " + s );
                        onNetworkCall.onComplete(status, status ? s : "");
                    }
                }
            } else if (responseCode == INTERNAL_SERVER_ERROR || responseCode == NOT_FOUND
                    || responseCode == BAD_GATEWAY || responseCode == SERVICE_UNAVAILABLE
                    || responseCode == GATEWAY_TIMEOUT) {
                Logger.e("OnError : " + ConfigManager.getInstance().getRequestBody(call));
                if ( onNetworkCall != null ) {
                    onNetworkCall.onComplete(false, "");
                }
                String apiCall = ConfigManager.getInstance().getApiErrorCode().get(endPoint);
                if (apiCall == null) {
                    String bug = ConfigManager.getInstance().getApiConfigError(responseCode, response.message()
                            , ConfigManager.getInstance().getRequestBody(call));
                    ConfigManager.getInstance().getApiErrorCode().put(endPoint, bug);
                    if ( !ConfigManager.getInstance().isConfigLoaded() ) {
                        ConfigManager.getInstance().callConfig(true, true, bug);
                    }
                }
            }
        }else {
            if ( onNetworkCall != null ) {
                onNetworkCall.onComplete(false, "");
            }
        }
    }

    @Override
    public void onFailure(Call<BaseModel> call, Throwable t) {
        Logger.e("onFailure : " + ConfigManager.getInstance().getRequestBody(call));
        Logger.e( "onFailure" );
        if ( onNetworkCall != null ) {
            onNetworkCall.onComplete(false, "");
        }
        if (t instanceof JSONException) {
            String apiCall = ConfigManager.getInstance().getApiJsonException().get(endPoint);
            if (apiCall == null) {
                String bug = ConfigManager.getInstance().getApiConfigError(JSON_EXCEPTION, t.getMessage()
                        , ConfigManager.getInstance().getRequestBody(call));
                ConfigManager.getInstance().getApiJsonException().put(endPoint, bug);
                if ( !ConfigManager.getInstance().isConfigLoaded() ) {
                    ConfigManager.getInstance().callConfig(true, true, bug);
                }
            }

        }
    }

    private final int NOT_FOUND = 404;
    private final int INTERNAL_SERVER_ERROR = 500;
    private final int BAD_GATEWAY = 502;
    private final int SERVICE_UNAVAILABLE = 503;
    private final int GATEWAY_TIMEOUT = 504;

}
