package gk.gkcurrentaffairs.config;

/**
 * Created by Amit on 3/29/2018.
 */

public interface ApiEndPoint {

    String GET_HOME_PAGE_EXTRA_DATA = "get-home-page-extra-data";
    String GET_HOME_DATA = "get-home-data";
    String GET_PREVIOUS_CONTENT_BY_CAT_ID = "get-previous-content-by-cat-id";
    String GET_CONTENT_BY_PRODUCT_IDS_ARRAY = "get-content-by-product-ids-array";
    String GET_UNIQUE_TITLE_BY_CAT_ID = "get-unique-title-by-cat-id" ;
    String GET_MOCK_TEST_BY_MIN_ID = "get-mock-test-by-min-id" ;
    String GET_CONTENT_BY_ID = "get-content-by-id" ;
    String GET_USER_RANK = "get-user-rank" ;
    String SAVE_GAME_RESULT = "save-game-result" ;
    String GET_USER_LEADER_BOARD_RANKING= "get-user-leaderboard-rankingrank";
    String GET_IMP_UPDATES_IDS = "get-important-update-ids";
    String GET_CONTENT_AND_SUB_CAT_IDS_BY_CAT_ID = "get-content-and-subcat-ids-by-cat-id";
    String STORE_FCM_TOKEN = "store-fcm-token";
    String GK_MOCK_DATA = "gk-mock-data";
    String GK_MOCK_LIST = "gk-mock-list";
    String GET_SUB_CATS_TITLES_WITH_IDS = "get-sub-cats-titles-with-ids";
    String POST_USER_PAID_CATEGORIES = "post-user-paid-categories";
    String GET_NCRT_CONTENT= "get-ncrt-content";
    String SEND_ERROR = "send-error" ;
    String SEND_FEEDBACK = "send-feedback" ;
    String GET_CONTENT_FOR_DATE_BY_SUB_CAT_ID= "get-content-for-date-by-sub-cat-id" ;
    String GET_LATEST_MOCK_TEST = "get-latest-mock-test" ;
    String GET_FAQ_CAT = "get-faq-categories" ;
    String GET_FAQ_BY_CAT_ID = "get-faq-by-category-id" ;

    String POINTS_PLAN = "points-plan";
    String ADS_FREE_SLAB_CONFIG = "ads-free-slab-config";

}
