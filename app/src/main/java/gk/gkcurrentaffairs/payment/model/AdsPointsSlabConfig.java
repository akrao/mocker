package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Amit on 8/12/2017.
 */

public class AdsPointsSlabConfig {

    @SerializedName("rvadsweight")
    @Expose
    private Integer rvadsweight;
    @SerializedName("rvpointsweight")
    @Expose
    private Integer rvpointsweight;
    @SerializedName("slab")
    @Expose
    private List<AdFreeSlab> slab = null;
    @SerializedName("user")
    @Expose
    private User user;

    public Integer getRvpointsweight() {
        return rvpointsweight;
    }

    public void setRvpointsweight(Integer rvpointsweight) {
        this.rvpointsweight = rvpointsweight;
    }

    public Integer getRvadsweight() {
        return rvadsweight;
    }

    public void setRvadsweight(Integer rvadsweight) {
        this.rvadsweight = rvadsweight;
    }

    public List<AdFreeSlab> getSlab() {
        return slab;
    }

    public void setSlab(List<AdFreeSlab> slab) {
        this.slab = slab;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
