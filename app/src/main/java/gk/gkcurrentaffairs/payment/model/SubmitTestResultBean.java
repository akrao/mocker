package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 7/20/2018.
 */

public class SubmitTestResultBean {
    @SerializedName("test_id")
    @Expose
    private Integer testId;
    @SerializedName("have_already_attempted")
    @Expose
    private Integer haveAlreadyAttempted;

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public Integer getHaveAlreadyAttempted() {
        return haveAlreadyAttempted;
    }

    public void setHaveAlreadyAttempted(Integer haveAlreadyAttempted) {
        this.haveAlreadyAttempted = haveAlreadyAttempted;
    }
}
