package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Amit on 9/6/2017.
 */

public class PaidMockTest implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("no_of_questions")
    @Expose
    private Integer noOfQuestions;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private int deletedAt;
    @SerializedName("sid1")
    @Expose
    private Integer sid1;
    @SerializedName("sid2")
    @Expose
    private Integer sid2;
    @SerializedName("sid3")
    @Expose
    private Integer sid3;
    @SerializedName("sid4")
    @Expose
    private int sid4;
    @SerializedName("sid5")
    @Expose
    private int sid5;
    @SerializedName("sid6")
    @Expose
    private int sid6;
    @SerializedName("sid7")
    @Expose
    private int sid7;
    @SerializedName("sid8")
    @Expose
    private int sid8;
    @SerializedName("sid9")
    @Expose
    private int sid9;
    @SerializedName("sid10")
    @Expose
    private int sid10;
    @SerializedName("has_user_attempted")
    @Expose
    private String hasUserAttempted;
    @SerializedName("cat1")
    @Expose
    private PaidTestCat cat1;
    @SerializedName("cat2")
    @Expose
    private PaidTestCat cat2;
    @SerializedName("cat3")
    @Expose
    private PaidTestCat cat3;
    @SerializedName("cat4")
    @Expose
    private PaidTestCat cat4;
    @SerializedName("cat5")
    @Expose
    private PaidTestCat cat5;

    private int testTime = 0 ;

    public int getTestTime() {
        return testTime;
    }

    public void setTestTime(int testTime) {
        this.testTime = testTime;
    }

    private int secId ;

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public PaidTestCat getCat1() {
        return cat1;
    }

    public void setCat1(PaidTestCat cat1) {
        this.cat1 = cat1;
    }

    public PaidTestCat getCat2() {
        return cat2;
    }

    public void setCat2(PaidTestCat cat2) {
        this.cat2 = cat2;
    }

    public PaidTestCat getCat3() {
        return cat3;
    }

    public void setCat3(PaidTestCat cat3) {
        this.cat3 = cat3;
    }

    public PaidTestCat getCat4() {
        return cat4;
    }

    public void setCat4(PaidTestCat cat4) {
        this.cat4 = cat4;
    }

    public PaidTestCat getCat5() {
        return cat5;
    }

    public void setCat5(PaidTestCat cat5) {
        this.cat5 = cat5;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getHasUserAttempted() {
        return hasUserAttempted;
    }

    public void setHasUserAttempted(String hasUserAttempted) {
        this.hasUserAttempted = hasUserAttempted;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(int deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getSid1() {
        return sid1;
    }

    public void setSid1(Integer sid1) {
        this.sid1 = sid1;
    }

    public Integer getSid2() {
        return sid2;
    }

    public void setSid2(Integer sid2) {
        this.sid2 = sid2;
    }

    public Integer getSid3() {
        return sid3;
    }

    public void setSid3(Integer sid3) {
        this.sid3 = sid3;
    }

    public int getSid4() {
        return sid4;
    }

    public void setSid4(int sid4) {
        this.sid4 = sid4;
    }

    public int getSid5() {
        return sid5;
    }

    public void setSid5(int sid5) {
        this.sid5 = sid5;
    }

    public int getSid6() {
        return sid6;
    }

    public void setSid6(int sid6) {
        this.sid6 = sid6;
    }

    public int getSid7() {
        return sid7;
    }

    public void setSid7(int sid7) {
        this.sid7 = sid7;
    }

    public int getSid8() {
        return sid8;
    }

    public void setSid8(int sid8) {
        this.sid8 = sid8;
    }

    public int getSid9() {
        return sid9;
    }

    public void setSid9(int sid9) {
        this.sid9 = sid9;
    }

    public int getSid10() {
        return sid10;
    }

    public void setSid10(int sid10) {
        this.sid10 = sid10;
    }

}
