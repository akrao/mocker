package gk.gkcurrentaffairs.payment.model;

import com.adssdk.BaseAdModelClass;

import java.io.Serializable;

import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 9/8/2017.
 */

public class PaidMockTestResult extends BaseAdModelClass implements Serializable{

    private int id , status , mockId , secId , actualAns ;
    private double firstView , lastVisit , timeTaken;
    private boolean isMarkReview ;

    public boolean isMarkReview() {
        return isMarkReview;
    }

    public void setMarkReview(boolean markReview) {
        isMarkReview = markReview;
    }

    public PaidMockTestResult(int id , int ans) {
        this.id = id;
        this.actualAns = ans;
        this.status = AppConstant.PAID_QUE_NOT_VISIT ;
        timeTaken = 0 ;
        isMarkReview = false ;
    }

    public int getActualAns() {
        return actualAns;
    }

    public void setActualAns(int actualAns) {
        this.actualAns = actualAns;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMockId() {
        return mockId;
    }

    public void setMockId(int mockId) {
        this.mockId = mockId;
    }

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public double getFirstView() {
        return firstView;
    }

    public void setFirstView(double firstView) {
        this.firstView = firstView;
    }

    public double getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(double lastVisit) {
        this.lastVisit = lastVisit;
    }

    public double getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(double timeTaken) {
        this.timeTaken = timeTaken;
    }
}
