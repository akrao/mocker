package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 11/2/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionHistoryBean {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_type")
    @Expose
    private Integer productType;
    @SerializedName("transtion_id")
    @Expose
    private String transtionId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("is_refund")
    @Expose
    private Integer isRefund;
    @SerializedName("is_enquire")
    @Expose
    private Integer isEnquire;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sdays")
    @Expose
    private Integer sdays;
    @SerializedName("scost")
    @Expose
    private Integer scost;
    @SerializedName("discription")
    @Expose
    private String discription;
    @SerializedName("discount_percentage")
    @Expose
    private Integer discountPercentage;
    @SerializedName("offer_discription")
    @Expose
    private String offerDiscription;
    @SerializedName("regular_price")
    @Expose
    private Integer regularPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getTranstionId() {
        return transtionId;
    }

    public void setTranstionId(String transtionId) {
        this.transtionId = transtionId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getIsRefund() {
        return isRefund;
    }

    public void setIsRefund(Integer isRefund) {
        this.isRefund = isRefund;
    }

    public Integer getIsEnquire() {
        return isEnquire;
    }

    public void setIsEnquire(Integer isEnquire) {
        this.isEnquire = isEnquire;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSdays() {
        return sdays;
    }

    public void setSdays(Integer sdays) {
        this.sdays = sdays;
    }

    public Integer getScost() {
        return scost;
    }

    public void setScost(Integer scost) {
        this.scost = scost;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Integer getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getOfferDiscription() {
        return offerDiscription;
    }

    public void setOfferDiscription(String offerDiscription) {
        this.offerDiscription = offerDiscription;
    }

    public Integer getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Integer regularPrice) {
        this.regularPrice = regularPrice;
    }

}