package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 9/7/2017.
 */

public class PaidMockResult {

    @SerializedName("has_user_subscribed")
    @Expose
    private boolean hasUserSubscribed ;
    @SerializedName("status")
    @Expose
    private boolean status ;
    @SerializedName("message")
    @Expose
    private String message ;
    @SerializedName("data")
    @Expose
    private PaidMockTest paidMockTest ;

    public boolean isHasUserSubscribed() {
        return hasUserSubscribed;
    }

    public void setHasUserSubscribed(boolean hasUserSubscribed) {
        this.hasUserSubscribed = hasUserSubscribed;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaidMockTest getPaidMockTest() {
        return paidMockTest;
    }

    public void setPaidMockTest(PaidMockTest paidMockTest) {
        this.paidMockTest = paidMockTest;
    }
}
