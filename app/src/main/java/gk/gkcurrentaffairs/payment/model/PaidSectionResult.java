package gk.gkcurrentaffairs.payment.model;

import com.adssdk.BaseAdModelClass;
import com.github.mikephil.charting.data.PieData;

import java.io.Serializable;

/**
 * Created by Amit on 9/16/2017.
 */

public class PaidSectionResult extends BaseAdModelClass implements Serializable{

    private String title , time , score , percentile , correctAnsTime , wrongAnsTime ;
    private int correct , wrong , unattended , position ;
//    private ArrayList<PieHelper> pieHelperArrayList ;
    private PieData pieData ;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public PieData getPieData() {
        return pieData;
    }

    public void setPieData(PieData pieData) {
        this.pieData = pieData;
    }

/*
    public ArrayList<PieHelper> getPieHelperArrayList() {
        return pieHelperArrayList;
    }

    public void setPieHelperArrayList(ArrayList<PieHelper> pieHelperArrayList) {
        this.pieHelperArrayList = pieHelperArrayList;
    }
*/

    public String getCorrectAnsTime() {
        return correctAnsTime;
    }

    public void setCorrectAnsTime(String correctAnsTime) {
        this.correctAnsTime = correctAnsTime;
    }

    public String getWrongAnsTime() {
        return wrongAnsTime;
    }

    public void setWrongAnsTime(String wrongAnsTime) {
        this.wrongAnsTime = wrongAnsTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getPercentile() {
        return percentile;
    }

    public void setPercentile(String percentile) {
        this.percentile = percentile;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getUnattended() {
        return unattended;
    }

    public void setUnattended(int unattended) {
        this.unattended = unattended;
    }
}
