package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/25/2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUser {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("permissions")
        @Expose
        private List<Object> permissions = null;
        @SerializedName("last_login")
        @Expose
        private Object lastLogin;
        @SerializedName("first_name")
        @Expose
        private Object firstName;
        @SerializedName("last_name")
        @Expose
        private Object lastName;
        @SerializedName("dob")
        @Expose
        private Object dob;
        @SerializedName("bio")
        @Expose
        private Object bio;
        @SerializedName("gender")
        @Expose
        private Object gender;
        @SerializedName("country")
        @Expose
        private Object country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("postal")
        @Expose
        private String postal;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("device_token")
        @Expose
        private Object deviceToken;
        @SerializedName("fcm_token")
        @Expose
        private Object fcmToken;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("photo_url")
        @Expose
        private String photoUrl;
        @SerializedName("refresh_token")
        @Expose
        private Object refreshToken;
        @SerializedName("email_verified")
        @Expose
        private Integer emailVerified;
        @SerializedName("provider_id")
        @Expose
        private String providerId;
        @SerializedName("provider_uid")
        @Expose
        private Object providerUid;
        @SerializedName("user_role")
        @Expose
        private String userRole;
        @SerializedName("user_points")
        @Expose
        private Integer userPoints;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public List<Object> getPermissions() {
            return permissions;
        }

        public void setPermissions(List<Object> permissions) {
            this.permissions = permissions;
        }

        public Object getLastLogin() {
            return lastLogin;
        }

        public void setLastLogin(Object lastLogin) {
            this.lastLogin = lastLogin;
        }

        public Object getFirstName() {
            return firstName;
        }

        public void setFirstName(Object firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public Object getDob() {
            return dob;
        }

        public void setDob(Object dob) {
            this.dob = dob;
        }

        public Object getBio() {
            return bio;
        }

        public void setBio(Object bio) {
            this.bio = bio;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public Object getCountry() {
            return country;
        }

        public void setCountry(Object country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPostal() {
            return postal;
        }

        public void setPostal(String postal) {
            this.postal = postal;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(Object deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Object getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(Object fcmToken) {
            this.fcmToken = fcmToken;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhotoUrl() {
            return photoUrl;
        }

        public void setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
        }

        public Object getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(Object refreshToken) {
            this.refreshToken = refreshToken;
        }

        public Integer getEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(Integer emailVerified) {
            this.emailVerified = emailVerified;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public Object getProviderUid() {
            return providerUid;
        }

        public void setProviderUid(Object providerUid) {
            this.providerUid = providerUid;
        }

        public String getUserRole() {
            return userRole;
        }

        public void setUserRole(String userRole) {
            this.userRole = userRole;
        }

        public Integer getUserPoints() {
            return userPoints;
        }

        public void setUserPoints(Integer userPoints) {
            this.userPoints = userPoints;
        }

    }
