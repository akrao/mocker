package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 10/27/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SectionResultBean implements Serializable {

    @SerializedName("section_total_marks")
    @Expose
    private Double sectionTotalMarks;
    @SerializedName("section_number")
    @Expose
    private Integer sectionNumber;
    @SerializedName("correct_answer")
    @Expose
    private Integer correctAnswer;
    @SerializedName("wrong_answer")
    @Expose
    private Integer wrongAnswer;
    @SerializedName("unattempted")
    @Expose
    private Integer unattempted;
    @SerializedName("section_rank")
    @Expose
    private Integer sectionRank;
    @SerializedName("section_state_rank")
    @Expose
    private Integer sectionStateRank;
    @SerializedName("section_info")
    @Expose
    private PaidTestCat paidTestCat;

    public PaidTestCat getPaidTestCat() {
        return paidTestCat;
    }

    public void setPaidTestCat(PaidTestCat paidTestCat) {
        this.paidTestCat = paidTestCat;
    }

    public Double getSectionTotalMarks() {
        return sectionTotalMarks;
    }

    public void setSectionTotalMarks(Double sectionTotalMarks) {
        this.sectionTotalMarks = sectionTotalMarks;
    }

    public Integer getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Integer correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(Integer wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public Integer getUnattempted() {
        return unattempted;
    }

    public void setUnattempted(Integer unattempted) {
        this.unattempted = unattempted;
    }

    public Integer getSectionRank() {
        return sectionRank;
    }

    public void setSectionRank(Integer sectionRank) {
        this.sectionRank = sectionRank;
    }

    public Integer getSectionStateRank() {
        return sectionStateRank;
    }

    public void setSectionStateRank(Integer sectionStateRank) {
        this.sectionStateRank = sectionStateRank;
    }

}