package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 11/3/2017.
 */

public class ExamBean {

    @SerializedName("id")
    @Expose
    private int id ;
    @SerializedName("title")
    @Expose
    private String title ;
    @SerializedName("has_subscribe")
    @Expose
    private String hasSubscribe ;
    @SerializedName("image")
    @Expose
    private String image ;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHasSubscribe() {
        return hasSubscribe;
    }

    public void setHasSubscribe(String hasSubscribe) {
        this.hasSubscribe = hasSubscribe;
    }
}
