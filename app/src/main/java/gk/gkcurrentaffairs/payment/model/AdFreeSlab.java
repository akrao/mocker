package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AdFreeSlab implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("benefits")
    @Expose
    private Integer benefits;
    @SerializedName("cost")
    @Expose
    private float cost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBenefits() {
        return benefits;
    }

    public void setBenefits(Integer benefits) {
        this.benefits = benefits;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

}