package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 9/6/2017.
 */

import com.adssdk.BaseAdModelClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaidQuestion extends BaseAdModelClass implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("title_eng")
    @Expose
    private String titleEng;
    @SerializedName("description_eng")
    @Expose
    private String descriptionEng;
    @SerializedName("option_question_eng")
    @Expose
    private String optionQuestionEng;
    @SerializedName("option1_eng")
    @Expose
    private String option1Eng;
    @SerializedName("option2_eng")
    @Expose
    private String option2Eng;
    @SerializedName("option3_eng")
    @Expose
    private String option3Eng;
    @SerializedName("option4_eng")
    @Expose
    private String option4Eng;
    @SerializedName("option5_eng")
    @Expose
    private String option5Eng;
    @SerializedName("option_answer_eng")
    @Expose
    private Integer optionAnswerEng;
    @SerializedName("answer_description_eng")
    @Expose
    private String answerDescriptionEng;
    @SerializedName("title_hin")
    @Expose
    private String titleHin;
    @SerializedName("description_hin")
    @Expose
    private String descriptionHin;
    @SerializedName("option_question_hin")
    @Expose
    private String optionQuestionHin;
    @SerializedName("option1_hin")
    @Expose
    private String option1Hin;
    @SerializedName("option2_hin")
    @Expose
    private String option2Hin;
    @SerializedName("option3_hin")
    @Expose
    private String option3Hin;
    @SerializedName("option4_hin")
    @Expose
    private String option4Hin;
    @SerializedName("option5_hin")
    @Expose
    private String option5Hin;
    @SerializedName("option_answer_hin")
    @Expose
    private Integer optionAnswerHin;
    @SerializedName("answer_description_hin")
    @Expose
    private String answerDescriptionHin;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public void setTitleEng(String titleEng) {
        this.titleEng = titleEng;
    }

    public String getDescriptionEng() {
        return descriptionEng;
    }

    public void setDescriptionEng(String descriptionEng) {
        this.descriptionEng = descriptionEng;
    }

    public String getOptionQuestionEng() {
        return optionQuestionEng;
    }

    public void setOptionQuestionEng(String optionQuestionEng) {
        this.optionQuestionEng = optionQuestionEng;
    }

    public String getOption1Eng() {
        return option1Eng;
    }

    public void setOption1Eng(String option1Eng) {
        this.option1Eng = option1Eng;
    }

    public String getOption2Eng() {
        return option2Eng;
    }

    public void setOption2Eng(String option2Eng) {
        this.option2Eng = option2Eng;
    }

    public String getOption3Eng() {
        return option3Eng;
    }

    public void setOption3Eng(String option3Eng) {
        this.option3Eng = option3Eng;
    }

    public String getOption4Eng() {
        return option4Eng;
    }

    public void setOption4Eng(String option4Eng) {
        this.option4Eng = option4Eng;
    }

    public String getOption5Eng() {
        return option5Eng;
    }

    public void setOption5Eng(String option5Eng) {
        this.option5Eng = option5Eng;
    }

    public Integer getOptionAnswerEng() {
        return optionAnswerEng;
    }

    public void setOptionAnswerEng(Integer optionAnswerEng) {
        this.optionAnswerEng = optionAnswerEng;
    }

    public String getAnswerDescriptionEng() {
        return answerDescriptionEng;
    }

    public void setAnswerDescriptionEng(String answerDescriptionEng) {
        this.answerDescriptionEng = answerDescriptionEng;
    }

    public String getTitleHin() {
        return titleHin;
    }

    public void setTitleHin(String titleHin) {
        this.titleHin = titleHin;
    }

    public String getDescriptionHin() {
        return descriptionHin;
    }

    public void setDescriptionHin(String descriptionHin) {
        this.descriptionHin = descriptionHin;
    }

    public String getOptionQuestionHin() {
        return optionQuestionHin;
    }

    public void setOptionQuestionHin(String optionQuestionHin) {
        this.optionQuestionHin = optionQuestionHin;
    }

    public String getOption1Hin() {
        return option1Hin;
    }

    public void setOption1Hin(String option1Hin) {
        this.option1Hin = option1Hin;
    }

    public String getOption2Hin() {
        return option2Hin;
    }

    public void setOption2Hin(String option2Hin) {
        this.option2Hin = option2Hin;
    }

    public String getOption3Hin() {
        return option3Hin;
    }

    public void setOption3Hin(String option3Hin) {
        this.option3Hin = option3Hin;
    }

    public String getOption4Hin() {
        return option4Hin;
    }

    public void setOption4Hin(String option4Hin) {
        this.option4Hin = option4Hin;
    }

    public String getOption5Hin() {
        return option5Hin;
    }

    public void setOption5Hin(String option5Hin) {
        this.option5Hin = option5Hin;
    }

    public Integer getOptionAnswerHin() {
        return optionAnswerHin;
    }

    public void setOptionAnswerHin(Integer optionAnswerHin) {
        this.optionAnswerHin = optionAnswerHin;
    }

    public String getAnswerDescriptionHin() {
        return answerDescriptionHin;
    }

    public void setAnswerDescriptionHin(String answerDescriptionHin) {
        this.answerDescriptionHin = answerDescriptionHin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

}