package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 5/12/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaidSlidersBean implements Serializable{

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("android_app_id")
    @Expose
    private String androidAppId;
    @SerializedName("ios_app_id")
    @Expose
    private String iosAppId;
    @SerializedName("web_url")
    @Expose
    private String webUrl;
    @SerializedName("page_id")
    @Expose
    private int pageId;
    @SerializedName("page_type")
    @Expose
    private Integer pageType;
    @SerializedName("app_page_id")
    @Expose
    private Integer appPageId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;

    public String getIosAppId() {
        return iosAppId;
    }

    public void setIosAppId(String iosAppId) {
        this.iosAppId = iosAppId;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAndroidAppId() {
        return androidAppId;
    }

    public void setAndroidAppId(String androidAppId) {
        this.androidAppId = androidAppId;
    }

    public Integer getPageType() {
        return pageType;
    }

    public void setPageType(Integer pageType) {
        this.pageType = pageType;
    }

    public Integer getAppPageId() {
        return appPageId;
    }

    public void setAppPageId(Integer appPageId) {
        this.appPageId = appPageId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

}