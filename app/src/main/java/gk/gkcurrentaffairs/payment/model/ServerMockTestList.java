package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 9/19/2017.
 */

import com.adssdk.BaseAdModelClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import gk.gkcurrentaffairs.AppApplication;

public class ServerMockTestList extends BaseAdModelClass {

    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("result_id")
    @Expose
    private Integer resultId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("created_at")
    @Expose
    private String updatedAt;
    @SerializedName("package_title")
    @Expose
    private String packageTitle;

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdatedAt() {
        return AppApplication.getInstance().getDBObject().convertToDateFormat(updatedAt);
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public void setPackageTitle(String packageTitle) {
        this.packageTitle = packageTitle;
    }

}