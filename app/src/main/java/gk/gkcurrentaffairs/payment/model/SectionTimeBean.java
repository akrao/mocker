package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 10/27/2017.
 */

public class SectionTimeBean {

    private String time , title ;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SectionTimeBean(String time, String title) {
        this.time = time;
        this.title = title;
    }
}
