package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 10/27/2017.
 */

public class ServerTestResultBean {
    @SerializedName("test_id")
    @Expose
    private Integer testId;
    @SerializedName("have_already_attempted")
    @Expose
    private Integer haveAlreadyAttempted;

    @SerializedName("test_result")
    @Expose
    private TestResultBean testResultBean;

    public Integer getHaveAlreadyAttempted() {
        return haveAlreadyAttempted;
    }

    public void setHaveAlreadyAttempted(Integer haveAlreadyAttempted) {
        this.haveAlreadyAttempted = haveAlreadyAttempted;
    }

    public Integer getTestId() {
        return testId;
    }

    private TimeDistributionBean timeDistributionBean ;
    private BaseResultBean baseResultBean;

    public TimeDistributionBean getTimeDistributionBean() {
        return timeDistributionBean;
    }

    public void setTimeDistributionBean(TimeDistributionBean timeDistributionBean) {
        this.timeDistributionBean = timeDistributionBean;
    }

    public BaseResultBean baseResultBean() {
        return baseResultBean;
    }

    public void setBaseResultBean(BaseResultBean baseResultBean) {
        this.baseResultBean = baseResultBean;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public TestResultBean getTestResultBean() {
        return testResultBean;
    }

    public void setTestResultBean(TestResultBean testResultBean) {
        this.testResultBean = testResultBean;
    }
}
