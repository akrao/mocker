package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import gk.gkcurrentaffairs.bean.NetworkConfigBean;

public class Config extends NetworkConfigBean{

    @SerializedName("rvadfree")
    @Expose
    private Boolean rvadfree;
    @SerializedName("rvpoints")
    @Expose
    private Boolean rvpoints;
    @SerializedName("rvadsweight")
    @Expose
    private Integer rvadsweight;
    @SerializedName("rvpointsweight")
    @Expose
    private Integer rvpointsweight;


    public Boolean getRvadfree() {
        return rvadfree;
    }

    public void setRvadfree(Boolean rvadfree) {
        this.rvadfree = rvadfree;
    }

    public Boolean getRvpoints() {
        return rvpoints;
    }

    public void setRvpoints(Boolean rvpoints) {
        this.rvpoints = rvpoints;
    }

    public Integer getRvadsweight() {
        return rvadsweight;
    }

    public void setRvadsweight(Integer rvadsweight) {
        this.rvadsweight = rvadsweight;
    }

    public Integer getRvpointsweight() {
        return rvpointsweight;
    }

    public void setRvpointsweight(Integer rvpointsweight) {
        this.rvpointsweight = rvpointsweight;
    }

}
