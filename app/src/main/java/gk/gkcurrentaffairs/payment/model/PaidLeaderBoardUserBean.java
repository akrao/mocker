package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 5/17/2018.
 */
import com.adssdk.BaseAdModelClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaidLeaderBoardUserBean extends BaseAdModelClass {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("total_marks")
    @Expose
    private Double totalMarks;
    @SerializedName("max_marks")
    @Expose
    private Double outOfMarks;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("photo_url")
    @Expose
    private String photoUrl;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    private int rank ;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Double getOutOfMarks() {
        return outOfMarks;
    }

    public void setOutOfMarks(Double outOfMarks) {
        this.outOfMarks = outOfMarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Double totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}