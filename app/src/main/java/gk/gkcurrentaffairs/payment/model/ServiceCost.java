package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/16/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceCost {

    @SerializedName("product_cost")
    @Expose
    private Integer productCost;
    @SerializedName("user_points")
    @Expose
    private Integer userPoints;
    @SerializedName("need_to_buy")
    @Expose
    private Integer needToBuy;
    @SerializedName("need_to_spend")
    @Expose
    private Integer needToSpend;
    @SerializedName("wallet_deduction")
    @Expose
    private Integer walletDeduction;
    @SerializedName("is_wallet_transtion")
    @Expose
    private boolean isWalletTransition;

    public Integer getWalletDeduction() {
        return walletDeduction;
    }

    public void setWalletDeduction(Integer walletDeduction) {
        this.walletDeduction = walletDeduction;
    }

    public boolean isWalletTransition() {
        return isWalletTransition;
    }

    public void setWalletTransition(boolean walletTransition) {
        isWalletTransition = walletTransition;
    }

    public Integer getProductCost() {
        return productCost;
    }

    public void setProductCost(Integer productCost) {
        this.productCost = productCost;
    }

    public Integer getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(Integer userPoints) {
        this.userPoints = userPoints;
    }

    public Integer getNeedToBuy() {
        return needToBuy;
    }

    public void setNeedToBuy(Integer needToBuy) {
        this.needToBuy = needToBuy;
    }

    public Integer getNeedToSpend() {
        return needToSpend;
    }

    public void setNeedToSpend(Integer needToSpend) {
        this.needToSpend = needToSpend;
    }

}