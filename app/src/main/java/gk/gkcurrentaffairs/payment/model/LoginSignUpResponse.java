package gk.gkcurrentaffairs.payment.model;

/**
 * Created by joginder on 2/6/17.
 */

public class LoginSignUpResponse {

    private int status;
    private String message;
    private AppUser user;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppUser getAppUser() {
        return user;
    }

    public void setAppUser(AppUser appUser) {
        this.user = appUser;
    }
}
