package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 5/8/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubscriptionOffersBean implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sdays")
    @Expose
    private Integer sdays;
    @SerializedName("scost")
    @Expose
    private Integer scost;
    @SerializedName("discription")
    @Expose
    private String discription;
    @SerializedName("discount_percentage")
    @Expose
    private Integer discountPercentage;
    @SerializedName("offer_discription")
    @Expose
    private String offerDiscription;
    @SerializedName("regular_price")
    @Expose
    private Integer regularPrice;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSdays() {
        return sdays;
    }

    public void setSdays(Integer sdays) {
        this.sdays = sdays;
    }

    public Integer getScost() {
        return scost;
    }

    public void setScost(Integer scost) {
        this.scost = scost;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Integer getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getOfferDiscription() {
        return offerDiscription;
    }

    public void setOfferDiscription(String offerDiscription) {
        this.offerDiscription = offerDiscription;
    }

    public Integer getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Integer regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

}