package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Amit on 5/17/2018.
 */

public class PaidLeaderBoardBean {

    @SerializedName("total_count")
    @Expose
    private int total_count;
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("top_users")
    @Expose
    private List<PaidLeaderBoardUserBean> topUsers ;
    @SerializedName("users_ranking")
    @Expose
    private List<PaidLeaderBoardUserBean> usersRanking ;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public List<PaidLeaderBoardUserBean> getTopUsers() {
        return topUsers;
    }

    public void setTopUsers(List<PaidLeaderBoardUserBean> topUsers) {
        this.topUsers = topUsers;
    }

    public List<PaidLeaderBoardUserBean> getUsersRanking() {
        return usersRanking;
    }

    public void setUsersRanking(List<PaidLeaderBoardUserBean> usersRanking) {
        this.usersRanking = usersRanking;
    }
}
