package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Amit on 9/5/2017.
 */

public class PaidHomeBean {

    @SerializedName("data")
    @Expose
    private List<SectionData> sectionDataList ;
    @SerializedName("image_path")
    @Expose
    private String imagePath ;

    @SerializedName("subscription_time")
    @Expose
    private long subscriptionTime ;

    @SerializedName("sliders")
    @Expose
    private List<PaidSlidersBean> slidersBeanList ;

    public long getSubscriptionTime() {
        return subscriptionTime;
    }

    public void setSubscriptionTime(long subscriptionTime) {
        this.subscriptionTime = subscriptionTime;
    }

    public List<SectionData> getSectionDataList() {
        return sectionDataList;
    }

    public void setSectionDataList(List<SectionData> sectionDataList) {
        this.sectionDataList = sectionDataList;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<PaidSlidersBean> getSlidersBeanList() {
        return slidersBeanList;
    }

    public void setSlidersBeanList(List<PaidSlidersBean> slidersBeanList) {
        this.slidersBeanList = slidersBeanList;
    }
}
