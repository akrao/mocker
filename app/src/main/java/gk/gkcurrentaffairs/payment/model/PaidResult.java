package gk.gkcurrentaffairs.payment.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amit on 9/15/2017.
 */

public class PaidResult implements Serializable , Cloneable {

    private String packageTitle , title , date ;
    private int packageId , mockId , status , sectionCount , id , lastSectionPos , lastQuePos ;
    private List<List<PaidMockTestResult>> paidMockTestResults ;
    private long endTimeStamp, startTimeStamp , timeTaken;
    private List<PaidTestCat> testCats ;
    private boolean isPracticeTest ;

    public int getLastSectionPos() {
        return lastSectionPos;
    }

    public void setLastSectionPos(int lastSectionPos) {
        this.lastSectionPos = lastSectionPos;
    }

    public int getLastQuePos() {
        return lastQuePos;
    }

    public void setLastQuePos(int lastQuePos) {
        this.lastQuePos = lastQuePos;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public PaidResult() {
        this.id = 0 ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public PaidResult getClone () throws CloneNotSupportedException{
        return ( PaidResult ) clone() ;
    }

    public boolean isPracticeTest() {
        return isPracticeTest;
    }

    public void setPracticeTest(boolean practiceTest) {
        isPracticeTest = practiceTest;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public int getSectionCount() {
        return sectionCount;
    }

    public void setSectionCount(int sectionCount) {
        this.sectionCount = sectionCount;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public void setPackageTitle(String packageTitle) {
        this.packageTitle = packageTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getMockId() {
        return mockId;
    }

    public void setMockId(int mockId) {
        this.mockId = mockId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<List<PaidMockTestResult>> getPaidMockTestResults() {
        return paidMockTestResults;
    }

    public void setPaidMockTestResults(List<List<PaidMockTestResult>> paidMockTestResults) {
        this.paidMockTestResults = paidMockTestResults;
    }

    public List<PaidTestCat> getTestCats() {
        return testCats;
    }

    public void setTestCats(List<PaidTestCat> testCats) {
        this.testCats = testCats;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }
}
