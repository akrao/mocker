package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserConfig {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("is_ads_free")
    @Expose
    private Boolean isAdsFree;
    @SerializedName("config")
    @Expose
    private Config config;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIsAdsFree() {
        return isAdsFree;
    }

    public void setIsAdsFree(Boolean isAdsFree) {
        this.isAdsFree = isAdsFree;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

}