package gk.gkcurrentaffairs.payment.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amit on 9/26/2017.
 */

public class TempResult implements Serializable{

    private int packageId , mockId , status , sectionCount , id , lastSectionPos , lastQuePos ;
    private List<List<PaidMockTestResult>> paidMockTestResults ;
    private long endTimeStamp, startTimeStamp , timeTaken;
    private boolean isPractice ;

    public boolean isPractice() {
        return isPractice;
    }

    public void setPractice(boolean practice) {
        isPractice = practice;
    }

    public int getLastSectionPos() {
        return lastSectionPos;
    }

    public void setLastSectionPos(int lastSectionPos) {
        this.lastSectionPos = lastSectionPos;
    }

    public int getLastQuePos() {
        return lastQuePos;
    }

    public void setLastQuePos(int lastQuePos) {
        this.lastQuePos = lastQuePos;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getMockId() {
        return mockId;
    }

    public void setMockId(int mockId) {
        this.mockId = mockId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSectionCount() {
        return sectionCount;
    }

    public void setSectionCount(int sectionCount) {
        this.sectionCount = sectionCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<List<PaidMockTestResult>> getPaidMockTestResults() {
        return paidMockTestResults;
    }

    public void setPaidMockTestResults(List<List<PaidMockTestResult>> paidMockTestResults) {
        this.paidMockTestResults = paidMockTestResults;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }
}
