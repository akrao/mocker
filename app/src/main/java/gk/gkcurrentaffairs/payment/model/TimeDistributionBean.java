package gk.gkcurrentaffairs.payment.model;

import java.util.ArrayList;

/**
 * Created by Amit on 10/27/2017.
 */

public class TimeDistributionBean {

    private String correctAnsTime , wrongAnsTime , skippedAnsTime , totalTime ;
    private ArrayList<SectionTimeBean> sectionTimeBeen ;

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getCorrectAnsTime() {
        return correctAnsTime;
    }

    public void setCorrectAnsTime(String correctAnsTime) {
        this.correctAnsTime = correctAnsTime;
    }

    public String getWrongAnsTime() {
        return wrongAnsTime;
    }

    public void setWrongAnsTime(String wrongAnsTime) {
        this.wrongAnsTime = wrongAnsTime;
    }

    public String getSkippedAnsTime() {
        return skippedAnsTime;
    }

    public void setSkippedAnsTime(String skippedAnsTime) {
        this.skippedAnsTime = skippedAnsTime;
    }

    public ArrayList<SectionTimeBean> getSectionTimeBeen() {
        return sectionTimeBeen;
    }

    public void setSectionTimeBeen(ArrayList<SectionTimeBean> sectionTimeBeen) {
        this.sectionTimeBeen = sectionTimeBeen;
    }
}
