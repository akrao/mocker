package gk.gkcurrentaffairs.payment.model;

import java.util.ArrayList;
import java.util.List;

import gk.gkcurrentaffairs.bean.ListBean;

/**
 * Created by Amit on 10/27/2017.
 */

public class BaseResultBean {
    public ArrayList<String> getStringList() {
        return stringList;
    }

    public void setStringList(ArrayList<String> stringList) {
        this.stringList = stringList;
    }

    private ArrayList<String> stringList ;
}
