package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 8/30/2017.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SectionData implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("children")
    @Expose
    private List<Child> children = null;

    private List<PaidSlidersBean> slidersBeanList ;

    public List<PaidSlidersBean> getSlidersBeanList() {
        return slidersBeanList;
    }

    public void setSlidersBeanList(List<PaidSlidersBean> slidersBeanList) {
        this.slidersBeanList = slidersBeanList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }
}
