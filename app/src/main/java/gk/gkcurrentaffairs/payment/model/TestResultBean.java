package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 10/27/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TestResultBean implements Serializable {

    @SerializedName("max_marks")
    @Expose
    private int max_marks;
    @SerializedName("total_marks")
    @Expose
    private String totalMarks;
    @SerializedName("percentile")
    @Expose
    private String percentile;
    @SerializedName("correct_answer")
    @Expose
    private String correctAnswer;
    @SerializedName("wrong_answer")
    @Expose
    private String wrongAnswer;
    @SerializedName("unattempt_questions")
    @Expose
    private String unattemptQuestions;
    @SerializedName("user_rank")
    @Expose
    private String userRank;
    @SerializedName("state_rank")
    @Expose
    private String stateRank;
    @SerializedName("max_value")
    @Expose
    private String maxValue;
    @SerializedName("avg_value")
    @Expose
    private String avgValue;
    @SerializedName("is_practice_test")
    @Expose
    private int isPracticeTest;
    @SerializedName("section_result")
    @Expose
    private List<SectionResultBean> sectionResultBeen;
    @SerializedName("raw_data")
    @Expose
    private String rawData;

    public int getMax_marks() {
        return max_marks;
    }

    public void setMax_marks(int max_marks) {
        this.max_marks = max_marks;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getPercentile() {
        return percentile;
    }

    public void setPercentile(String percentile) {
        this.percentile = percentile;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getAvgValue() {
        return avgValue;
    }

    public void setAvgValue(String avgValue) {
        this.avgValue = avgValue;
    }

    public List<SectionResultBean> getSectionResultBeen() {
        return sectionResultBeen;
    }

    public void setSectionResultBeen(List<SectionResultBean> sectionResultBeen) {
        this.sectionResultBeen = sectionResultBeen;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(String wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public String getUnattemptQuestions() {
        return unattemptQuestions;
    }

    public void setUnattemptQuestions(String unattemptQuestions) {
        this.unattemptQuestions = unattemptQuestions;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public String getStateRank() {
        return stateRank;
    }

    public void setStateRank(String stateRank) {
        this.stateRank = stateRank;
    }

    public int getIsPracticeTest() {
        return isPracticeTest;
    }

    public void setIsPracticeTest(int isPracticeTest) {
        this.isPracticeTest = isPracticeTest;
    }

}