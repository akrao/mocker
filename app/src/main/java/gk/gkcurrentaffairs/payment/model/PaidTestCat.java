package gk.gkcurrentaffairs.payment.model;

/**
 * Created by Amit on 9/6/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PaidTestCat implements Serializable , Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name_internal_use")
    @Expose
    private String nameInternalUse;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("test_time")
    @Expose
    private Integer testTime;
    @SerializedName("test_markes")
    @Expose
    private Integer testMarkes;
    @SerializedName("instructions")
    @Expose
    private String instructions;
    @SerializedName("quest_marks")
    @Expose
    private Integer questMarks;
    @SerializedName("negetive_marking")
    @Expose
    private Double negetiveMarking;
    @SerializedName("category_type")
    @Expose
    private Integer categoryType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("contents")
    @Expose
    private List<PaidQuestion> paidQuestions;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public PaidTestCat getClone () throws CloneNotSupportedException{
        return ( PaidTestCat ) clone() ;
    }

    private int noOfQuestions;

    public int getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public List<PaidQuestion> getPaidQuestions() {
        return paidQuestions;
    }

    public void setPaidQuestions(List<PaidQuestion> paidQuestions) {
        this.paidQuestions = paidQuestions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNameInternalUse() {
        return nameInternalUse;
    }

    public void setNameInternalUse(String nameInternalUse) {
        this.nameInternalUse = nameInternalUse;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public Integer getTestTime() {
        return testTime;
    }

    public void setTestTime(Integer testTime) {
        this.testTime = testTime;
    }

    public Integer getTestMarkes() {
        return testMarkes;
    }

    public void setTestMarkes(Integer testMarkes) {
        this.testMarkes = testMarkes;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public Integer getQuestMarks() {
        return questMarks;
    }

    public void setQuestMarks(Integer questMarks) {
        this.questMarks = questMarks;
    }

    public Double getNegetiveMarking() {
        return negetiveMarking;
    }

    public void setNegetiveMarking(Double negetiveMarking) {
        this.negetiveMarking = negetiveMarking;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}