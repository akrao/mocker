package gk.gkcurrentaffairs.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 5/18/2018.
 */

public class CompareBean {

    @SerializedName("max_val")
    @Expose
    private float maxVal;
    @SerializedName("user_val")
    @Expose
    private float userVal;
    @SerializedName("avg_val")
    @Expose
    private String avgVal;

    public float getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(float maxVal) {
        this.maxVal = maxVal;
    }

    public float getUserVal() {
        return userVal;
    }

    public void setUserVal(float userVal) {
        this.userVal = userVal;
    }

    public String getAvgVal() {
        return avgVal;
    }

    public void setAvgVal(String avgVal) {
        this.avgVal = avgVal;
    }
}
