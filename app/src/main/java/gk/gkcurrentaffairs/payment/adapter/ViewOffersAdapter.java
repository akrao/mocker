package gk.gkcurrentaffairs.payment.adapter;

import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.SubscriptionOffersBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/8/2018.
 */

public class ViewOffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SubscriptionOffersBean> children ;
    private OnCustomClick onCustomClick ;

    public ViewOffersAdapter(List<SubscriptionOffersBean> children , OnCustomClick onCustomClick ) {
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position , boolean isDesc);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_view_offers, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if ( holder1 instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) holder1 ;
            holder.position = position ;
            SubscriptionOffersBean child = children.get( position );
            if ( child != null ){
                if (!SupportUtil.isEmptyOrNull(child.getTitle())) {
                    holder.tvTitle.setText( child.getTitle() );
                    holder.tvTitle.setVisibility(View.VISIBLE);
                }else {
                    holder.tvTitle.setVisibility(View.GONE);
                }
                if ( child.getDiscountPercentage() > 0 ){
                    holder.tvDiscountDesc.setVisibility(View.VISIBLE);
                    holder.tvRegularPrice.setVisibility(View.VISIBLE);
                    holder.tvDiscount.setText("SAVE "+child.getDiscountPercentage()+"%");
                    holder.tvDiscountDesc.setText("₹"+child.getRegularPrice());
                    holder.tvRegularPrice.setText(child.getDiscountPercentage()+"% off");
                }else {
                    holder.tvRegularPrice.setVisibility(View.GONE);
                    holder.tvDiscount.setText("");
                    holder.tvDiscountDesc.setVisibility(View.GONE);
                }
                holder.tvDesc.setText(Html.fromHtml("<b>₹"+child.getScost()+"</b> for " + child.getSdays()+" Days Pass" ) );
                holder.tvDay.setText( child.getSdays()+" " + AppConstant.OFFER_DAYS);
                holder.tvOfferDesc.setText( child.getOfferDiscription() );
            }
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle , tvDay , tvDiscount , tvDesc , tvOfferDesc , tvDiscountDesc , tvRegularPrice ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position , v.getId() != R.id.adp_bt_buy_pass );
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.adp_tv_title );
            tvDay = ( TextView ) itemView.findViewById( R.id.adp_tv_days );
            tvDiscount = ( TextView ) itemView.findViewById( R.id.adp_tv_discount );
            tvDesc = ( TextView ) itemView.findViewById( R.id.adp_tv_desc );
            tvOfferDesc = ( TextView ) itemView.findViewById( R.id.adp_tv_offer_desc );
            tvDiscountDesc = ( TextView ) itemView.findViewById( R.id.adp_tv_disc_desc );
            tvRegularPrice = ( TextView ) itemView.findViewById( R.id.adp_tv_regular_price );
            tvDiscountDesc.setPaintFlags(tvDiscountDesc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemView.findViewById( R.id.adp_bt_buy_pass ).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
    }

}
