package gk.gkcurrentaffairs.payment.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.TransactionHistoryBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/2/2017.
 */

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder> {

    private List<TransactionHistoryBean> children;
    private OnCustomClick onCustomClick;
    private OnLoadMore onLoadMore;

    public TransactionHistoryAdapter(List<TransactionHistoryBean> children, OnCustomClick onCustomClick, OnLoadMore onLoadMore) {
        this.children = children;
        this.onCustomClick = onCustomClick;
        this.onLoadMore = onLoadMore;
    }

    public interface OnLoadMore {
        void onLoadMore();
    }

    public interface OnCustomClick {
        void onCustomClick(int position , boolean isFeedback);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_transaction_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        TransactionHistoryBean child = children.get(position);
        if (child != null) {
            holder.tvTitle.setText(child.getTitle() + "    ₹" + child.getAmount());
            holder.tvDate.setText(child.getUpdatedAt());
            if (!SupportUtil.isEmptyOrNull(child.getPaymentStatus())) {
                boolean isSuccess = child.getPaymentStatus().equalsIgnoreCase("TXN_SUCCESS");
                holder.tvStatus.setText("Your order has been " + (isSuccess ? "Successful." : "Failed!"));
                holder.tvStatus.setTextColor(SupportUtil.getColor(isSuccess ? R.color.green_mcq_paid : R.color.wrong_red, AppApplication.getInstance()));
                holder.llFeedback.setVisibility(isSuccess ? View.GONE : View.VISIBLE);
                holder.vDivider.setVisibility(isSuccess ? View.GONE : View.VISIBLE);
            }
            holder.tvOrder.setText("Order Id : " + child.getOrderId());
        }
        if (onLoadMore != null && position >= children.size())
            onLoadMore.onLoadMore();
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        TextView tvTitle, tvDate, tvStatus, tvOrder;
        View llFeedback , vDivider ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position, v.getId()==R.id.ll_feedback);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvStatus = (TextView) itemView.findViewById(R.id.adp_tv_status);
            tvOrder = (TextView) itemView.findViewById(R.id.tv_order_id);
            llFeedback = itemView.findViewById(R.id.ll_feedback);
            llFeedback.setOnClickListener(this);
            vDivider = itemView.findViewById(R.id.v_divider);
            itemView.setOnClickListener(this);
        }
    }

}
