package gk.gkcurrentaffairs.payment.adapter;

import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.AdsSDK;
import com.adssdk.BaseAdModelClass;
import com.adssdk.NativeUnifiedAdsViewHolder;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.SectionResultBean;
import gk.gkcurrentaffairs.payment.model.SectionTimeBean;
import gk.gkcurrentaffairs.payment.model.ServerTestResultBean;
import gk.gkcurrentaffairs.payment.model.TestResultBean;
import gk.gkcurrentaffairs.payment.model.TimeDistributionBean;
import gk.gkcurrentaffairs.util.SupportUtil;

import static com.adssdk.AdsSDK.NATIVE_ADS_MODEL_ID;

/**
 * Created by Amit on 9/15/2017.
 */

public class PaidResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ServerTestResultBean serverTestResultBean;
    private Activity context;
    private int VIEW_TYPE_HEADER_RESULT = 0;
    private int VIEW_TYPE_HEADER_MARKS = 1;
    private int VIEW_TYPE_HEADER_TIME = 2;
    private int VIEW_TYPE_SECTION = 3;
    public static final int ITEM_NATIVE_AD = 10;
    public static int HEADER_COUNT = 4;
    private boolean isGlobal = false;
    private List<PaidSectionResult> sectionResults;
    private OnCustomClick onCustomClick;
    private LayoutInflater inflater;

    public void setServerTestResultBean(ServerTestResultBean serverTestResultBean) {
        this.serverTestResultBean = serverTestResultBean;
    }

    public PaidResultAdapter(Activity context, List<PaidSectionResult> sectionResults, ServerTestResultBean serverTestResultBean, OnCustomClick onCustomClick) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.serverTestResultBean = serverTestResultBean;
        this.sectionResults = sectionResults;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick {
        void onCustomClick(int position);
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_NATIVE_AD ) {
            RelativeLayout relativeLayout = new RelativeLayout(parent.getContext());
            View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.ads_native_unified_card , parent, false);
            relativeLayout.removeAllViews();
            relativeLayout.addView( view );
            return new NativeUnifiedAdsViewHolder(relativeLayout);
        }else if (viewType == VIEW_TYPE_HEADER_RESULT) {
            View view;
            if (isGlobal) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_result_header_result, parent, false);
            } else {
                view = new View(parent.getContext());
            }
            return new HeaderResultViewHolder(view);
        } else if (viewType == VIEW_TYPE_HEADER_MARKS) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_result_header_marks, parent, false);
            return new HeaderMarksViewHolder(view);
        } else if (viewType == VIEW_TYPE_HEADER_TIME) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_result_header_time, parent, false);
            return new HeaderTimeViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_result_section, parent, false);
            return new SectionViewHolder(view);
        }
    }

    Handler handler = new Handler();
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof NativeUnifiedAdsViewHolder) {
            final NativeUnifiedAdsViewHolder adsViewHolder = (NativeUnifiedAdsViewHolder) viewHolder ;
            if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AdsSDK.getInstance().getAdsNative().bindUnifiedNativeAd(adsViewHolder , false);
                    }
                }, 100);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AdsSDK.getInstance().getAdsNative().cacheNativeAd(false);
                    }
                }, 100);
            }
        }else if ( viewHolder instanceof ViewHolder ){
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position;
            if (holder instanceof SectionViewHolder) {
                int actualPosition = position - HEADER_COUNT ;
                int sectionTestPosition = position - HEADER_COUNT ;
                if ( sectionTestPosition > 0 )
                    sectionTestPosition-- ;
                SectionResultBean testResultBean = serverTestResultBean.getTestResultBean() == null ? null
                        : serverTestResultBean.getTestResultBean().getSectionResultBeen().get(sectionTestPosition);
                setSectionData((SectionViewHolder) holder, sectionResults.get(actualPosition), testResultBean);
            } else if (holder instanceof HeaderResultViewHolder && isGlobal) {
                setResultHeader((HeaderResultViewHolder) holder);
            } else if (holder instanceof HeaderMarksViewHolder) {
                if (serverTestResultBean != null && serverTestResultBean.baseResultBean() != null
                        && serverTestResultBean.baseResultBean().getStringList() != null) {
                    setTextInView(((HeaderMarksViewHolder) holder).tvOption, serverTestResultBean.baseResultBean().getStringList());
                }
            } else if (holder instanceof HeaderTimeViewHolder) {
                setDataTimeDistribution((HeaderTimeViewHolder) holder);
            }
        }
    }

    private void setResultHeader(HeaderResultViewHolder holder) {
        TestResultBean bean = serverTestResultBean.getTestResultBean();
        holder.tvOption[0].setText(bean.getTotalMarks());
        holder.tvOption[1].setText(String.format("%.2f" , SupportUtil.stringToFloat(bean.getPercentile())));
        holder.tvOption[2].setText(SupportUtil.stringToInt(bean.getUserRank())+"");
        int state = SupportUtil.stringToInt(bean.getStateRank());
        if (state > 0) {
            holder.tvOption[3].setText( state + "");
            isStateRank = false;
        } else {
            holder.tvOption[3].setText("?");
            isStateRank = true;
        }
        holder.tvOption[4].setText(bean.getMaxValue());
        holder.tvOption[5].setText(String.format("%.2f" , SupportUtil.stringToFloat(bean.getAvgValue())));

    }

    private boolean isStateRank = false;

    private void setDataTimeDistribution(HeaderTimeViewHolder holder) {
        TimeDistributionBean bean = serverTestResultBean.getTimeDistributionBean();
        if (bean != null) {
            holder.tvOption[0].setText(bean.getTotalTime());
            holder.tvOption[1].setText(bean.getCorrectAnsTime());
            holder.tvOption[2].setText(bean.getWrongAnsTime());
            holder.tvOption[3].setText(bean.getSkippedAnsTime());
            if ( ((LinearLayout)holder.linearLayout).getChildCount() == 5 && bean.getSectionTimeBeen() != null ) {
                for (SectionTimeBean timeBean : bean.getSectionTimeBeen()) {
                    View inflatedLayout = inflater.inflate(R.layout.item_header_section_time, holder.linearLayout, false);
                    ((TextView) inflatedLayout.findViewById(R.id.tv_section)).setText(timeBean.getTitle());
                    ((TextView) inflatedLayout.findViewById(R.id.tv_time)).setText(timeBean.getTime());
                    holder.linearLayout.addView(inflatedLayout);
                }
            }
        }
    }

    private void setSectionData(SectionViewHolder sectionViewHolder, PaidSectionResult result, SectionResultBean sectionResultBean) {
        sectionViewHolder.tvTitle.setText(result.getTitle());
        sectionViewHolder.tvTime.setText(result.getTime());
        sectionViewHolder.tvScore.setText(result.getScore());
        sectionViewHolder.correctAnsTime.setText(result.getCorrectAnsTime());
        sectionViewHolder.wrongAnsTime.setText(result.getWrongAnsTime());
        sectionViewHolder.tvCorrect.setText(result.getCorrect() + " CORRECT");
        sectionViewHolder.tvWrong.setText(result.getWrong() + " WRONG");
        sectionViewHolder.tvUnAttended.setText(result.getUnattended() + " UNATTEMPTED");
//        sectionViewHolder.pieView.setData(result.getPieHelperArrayList());
        sectionViewHolder.pieChart.setData(result.getPieData());
        Description description = new Description();
        description.setText(result.getTitle() + " Chart");
        sectionViewHolder.pieChart.setDescription(description);
        sectionViewHolder.vDivider.setVisibility(isGlobal ? View.VISIBLE : View.GONE);
        if (isGlobal) {
            sectionViewHolder.vPercent.setVisibility(View.VISIBLE);
            sectionViewHolder.tvPercentile.setText(result.getPercentile());
            if (sectionResultBean != null) {
                sectionViewHolder.tvRank.setText(sectionResultBean.getSectionRank() + "");
                sectionViewHolder.tvStateRank.setText(sectionResultBean.getSectionStateRank() + "");
            }
        }
    }

    private void setTextInView(TextView[] view, ArrayList<String> strings) {
        for (int i = 0; i < view.length; i++) {
            view[i].setText(strings.get(i));
        }
    }

    @Override
    public int getItemCount() {
        return sectionResults.size() + HEADER_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        int type;
        switch (position) {
            case 0:
                type = VIEW_TYPE_HEADER_RESULT;
                break;
            case 1:
                type = ITEM_NATIVE_AD;
                break;
            case 2:
                type = VIEW_TYPE_HEADER_MARKS;
                break;
            case 3:
                type = VIEW_TYPE_HEADER_TIME;
                break;
            default:
                if ( sectionResults != null && sectionResults.size() > (position-HEADER_COUNT) && sectionResults.get(position-HEADER_COUNT) instanceof BaseAdModelClass) {
                    type = ((BaseAdModelClass) sectionResults.get(position-HEADER_COUNT)).getModelId() == NATIVE_ADS_MODEL_ID ? ITEM_NATIVE_AD : VIEW_TYPE_SECTION;
                } else {
                    type = VIEW_TYPE_SECTION ;
                }
//                type = VIEW_TYPE_SECTION;
                break;
        }
        return type;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        int position;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class SectionViewHolder extends ViewHolder implements View.OnClickListener {

        TextView tvTitle, tvTime, tvCorrect, tvWrong, tvUnAttended, tvScore, tvPercentile, tvRank, tvStateRank, correctAnsTime, wrongAnsTime;
//        PieView pieView;
        View vDivider, vPercent;
        PieChart pieChart ;

        public SectionViewHolder(View itemView) {
            super(itemView);
            tvRank = (TextView) itemView.findViewById(R.id.adapter_tv_rank);
            tvStateRank = (TextView) itemView.findViewById(R.id.adapter_tv_state_rank);
            correctAnsTime = (TextView) itemView.findViewById(R.id.tv_correct_ans_time);
            wrongAnsTime = (TextView) itemView.findViewById(R.id.tv_wrong_ans_time);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvCorrect = (TextView) itemView.findViewById(R.id.tv_correct);
            tvWrong = (TextView) itemView.findViewById(R.id.tv_wrong);
            tvUnAttended = (TextView) itemView.findViewById(R.id.tv_unattended);
            tvScore = (TextView) itemView.findViewById(R.id.tv_score);
            tvPercentile = (TextView) itemView.findViewById(R.id.tv_percentile);
//            pieView = (PieView) itemView.findViewById(R.id.pv_sec);
            pieChart = (PieChart) itemView.findViewById(R.id.pc_sec);
            pieChart.setDrawHoleEnabled(false);
            vDivider = itemView.findViewById(R.id.ll_rank);
            vPercent = itemView.findViewById(R.id.ll_percent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(sectionResults.get(position-HEADER_COUNT).getPosition());
        }
    }

    class HeaderResultViewHolder extends ViewHolder {
        TextView[] tvTitle, tvOption;

        public HeaderResultViewHolder(View itemView) {
            super(itemView);
            if (isGlobal) {
                tvTitle = new TextView[5];
                tvOption = new TextView[6];
                itemView.findViewById(R.id.ll_state_rank).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( isStateRank ){
                            SupportUtil.openDialogStateRank(context);
                        }
                    }
                });
                for (int i = 0; i < tvTitleId.length; i++) {
                    tvTitle[i] = (TextView) itemView.findViewById(tvTitleId[i]);
                }
                for (int i = 0; i < tvOptionId.length; i++) {
                    tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
                }
            }
        }

        private int[] tvOptionId = {
                R.id.tv_opt_1,
                R.id.tv_opt_2,
                R.id.tv_opt_3,
                R.id.tv_opt_4,
                R.id.tv_opt_5,
                R.id.tv_opt_6
        };
        private int[] tvTitleId = {
                R.id.tv_title_1,
                R.id.tv_title_2,
                R.id.tv_title_3,
                R.id.tv_title_5,
                R.id.tv_title_4
        };
    }

    class HeaderMarksViewHolder extends ViewHolder {
        TextView[] tvOption;

        public HeaderMarksViewHolder(View itemView) {
            super(itemView);
            tvOption = new TextView[6];
            for (int i = 0; i < tvOptionId.length; i++) {
                tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
            }
        }

        private int[] tvOptionId = {
                R.id.tv_opt_1,
                R.id.tv_opt_2,
                R.id.tv_opt_3,
                R.id.tv_opt_4,
                R.id.tv_opt_5,
                R.id.tv_opt_6
        };
    }

    class HeaderTimeViewHolder extends ViewHolder {
        TextView[] tvOption;
        LinearLayout linearLayout;

        public HeaderTimeViewHolder(View itemView) {
            super(itemView);
            tvOption = new TextView[6];
            for (int i = 0; i < tvOptionId.length; i++) {
                tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
            }
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_body);
        }

        private int[] tvOptionId = {
                R.id.tv_opt_1,
                R.id.tv_opt_2,
                R.id.tv_opt_3,
                R.id.tv_opt_4,
                R.id.tv_opt_5,
                R.id.tv_opt_6
        };
    }

    private String[] titleString = {"OVERALL RESULT", "OVERALL SCORE", "PERCENTILE", "NATIONAL RANK", "STATE RANK"};
}