package gk.gkcurrentaffairs.payment.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.ExamBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/3/2017.
 */

public class ExamSelectionAdapter extends RecyclerView.Adapter<ExamSelectionAdapter.ViewHolder> {

    private List<ExamBean> children ;
    private OnCustomClick onCustomClick ;
    private String imagepath ;

    public ExamSelectionAdapter(List<ExamBean> children , OnCustomClick onCustomClick , String imagepath ) {
        this.children = children;
        this.onCustomClick = onCustomClick;
        this.imagepath = imagepath ;
    }

    public interface OnCustomClick{
        void onCustomClick(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_exam_selection, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position ;
        ExamBean child = children.get( position );
        if ( child != null ){
            holder.tvTitle.setText( child.getTitle() );
            holder.aSwitch.setChecked( !SupportUtil.isEmptyOrNull( child.getHasSubscribe() ));
            if ( !SupportUtil.isEmptyOrNull( imagepath ) && !SupportUtil.isEmptyOrNull(child.getImage()) ){
                AppApplication.getInstance().getPicasso().load(imagepath + child.getImage())
                        .placeholder(R.drawable.exam_place_holder).into(holder.ivTitle);
            }
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle ;
        Switch aSwitch ;
        ImageView ivTitle ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            aSwitch = ( Switch ) itemView.findViewById( R.id.switch_status );
            ivTitle = ( ImageView ) itemView.findViewById( R.id.iv_title );
            itemView.setOnClickListener(this);
        }
    }

}
