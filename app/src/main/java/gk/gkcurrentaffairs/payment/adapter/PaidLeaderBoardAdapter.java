package gk.gkcurrentaffairs.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidLeaderBoardUserBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/17/2018.
 */

public class PaidLeaderBoardAdapter extends NativeAdsAdapter {

    private List<PaidLeaderBoardUserBean> children;

    public PaidLeaderBoardAdapter(List<PaidLeaderBoardUserBean> children) {
        super(children, R.layout.ads_native_unified_card, null);
        this.children = children;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_leader_board, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        if (holder1 instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) holder1;
            holder.position = position;
            PaidLeaderBoardUserBean userBean = children.get(position);
            if (userBean != null) {
                SupportUtil.loadUserImage(userBean.getPhotoUrl(), holder.ivMain, R.drawable.profile_black);
                String name = userBean.getName();
                if (SupportUtil.isEmptyOrNull(name))
                    name = "User";
                holder.tvTitle.setText(name);
//                holder.tvCount.setText("" + (position+1));
                holder.tvCount.setText("" + userBean.getRank());
                holder.tvPoints.setText(userBean.getTotalMarks() + "/" + userBean.getOutOfMarks());
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        TextView tvTitle, tvCount, tvPoints;
        ImageView ivMain;

        @Override
        public void onClick(View v) {
//            onCustomClick.onCustomClick(position , v.getId() != R.id.adp_bt_buy_pass );
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.item_tv_title);
            tvCount = (TextView) itemView.findViewById(R.id.adp_tv_count);
            tvPoints = (TextView) itemView.findViewById(R.id.item_tv_points);
            ivMain = (ImageView) itemView.findViewById(R.id.item_iv_main);
            itemView.setOnClickListener(this);
        }
    }

}
