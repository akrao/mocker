package gk.gkcurrentaffairs.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.ServerMockTestList;

import static gk.gkcurrentaffairs.payment.adapter.MyMockTestAdapter.OnCustomClick.CLICK_TYPE_GLOBAL_RESULT;
import static gk.gkcurrentaffairs.payment.adapter.MyMockTestAdapter.OnCustomClick.CLICK_TYPE_MOCK;

/**
 * Created by Amit on 9/19/2017.
 */

public class MyMockTestAdapter extends NativeAdsAdapter {

    private List<ServerMockTestList> children ;
    private OnCustomClick onCustomClick ;

    public MyMockTestAdapter(List<ServerMockTestList> children , OnCustomClick onCustomClick ) {
        super(children , R.layout.ads_native_unified_card , null );
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position , int type);
        int CLICK_TYPE_MOCK = 0 ;
        int CLICK_TYPE_GLOBAL_RESULT = 1 ;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_mock_test, parent, false);
        return new MyMockTestAdapter.ViewHolder( view );
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if ( viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position ;
            ServerMockTestList child = children.get( position );
            if ( child != null ){
                holder.tvTitle.setText( child.getTitle() );
                holder.tvDate.setText( child.getUpdatedAt() );
                holder.tvPackage.setText( child.getPackageTitle() );
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle , tvDate , tvPackage;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position , v.getId() == R.id.bt_result ? CLICK_TYPE_GLOBAL_RESULT : CLICK_TYPE_MOCK  );
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvDate = ( TextView ) itemView.findViewById( R.id.tv_date );
            tvPackage = ( TextView ) itemView.findViewById( R.id.tv_package );
            itemView.findViewById(R.id.bt_result).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
    }

}

