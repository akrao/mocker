package gk.gkcurrentaffairs.payment.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.CatBean;

/**
 * Created by Amit on 11/8/2017.
 */

public class HelpSupportAdapter extends RecyclerView.Adapter<HelpSupportAdapter.ViewHolder> {

    private List<CatBean> children ;
    private OnCustomClick onCustomClick ;

    public HelpSupportAdapter(List<CatBean> children , OnCustomClick onCustomClick ) {
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_help_support, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position ;
        CatBean child = children.get( position );
        if ( child != null ){
            holder.tvTitle.setText( child.getTitle() );
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            itemView.setOnClickListener(this);
        }
    }
}
