package gk.gkcurrentaffairs.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.payment.model.MockInfo;

/**
 * Created by Amit on 9/6/2017.
 */

public class PaidMockAdapter extends NativeAdsAdapter {

    private List<MockInfo> children ;
    private OnCustomClick onCustomClick ;

    public PaidMockAdapter(List<MockInfo> children , OnCustomClick onCustomClick ) {
        super(children , R.layout.ads_native_unified_card , null );
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position , boolean isRefresh);
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_mock, parent, false);
        return new PaidMockAdapter.ViewHolder( view );
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if ( viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position ;
            MockInfo child = children.get( position );
            if ( child != null ){
                holder.tvTitle.setText( child.getTitle() );
                holder.tvDate.setText( "Live on - " + child.getReleaseDate() );
                if ( child.getPrice() == 0 || child.isDownloaded() ){
                    holder.tvLock.setBackgroundResource( R.drawable.circle_green );
                    holder.tvLock.setText((child.getPosition()+1)+"");
                }else if ( PaidHomeActivity.getInstance() != null &&
                        PaidHomeActivity.getInstance().isSubscribed()  ){
                    holder.tvLock.setBackgroundResource( R.drawable.circle_blue );
                    holder.tvLock.setText((child.getPosition()+1)+"");
                }else {
                    holder.tvLock.setBackgroundResource( R.drawable.locked_padlock );
                }
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle , tvDate , tvLock;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(children.get( position ).getPosition(), v.getId() == R.id.iv_refresh);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvDate = ( TextView ) itemView.findViewById( R.id.tv_date );
            tvLock = ( TextView ) itemView.findViewById( R.id.tv_lock );
            itemView.findViewById( R.id.iv_refresh ).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
    }

}
