package gk.gkcurrentaffairs.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/25/2017.
 */

public class PaidResultListAdapter extends RecyclerView.Adapter<PaidResultListAdapter.ViewHolder> {
    
    private List<PaidResult> lists ;
    private OnCustomClick onCustomClick ;
    private String title ;

    public PaidResultListAdapter(List<PaidResult> lists, OnCustomClick onCustomClick , String title) {
        this.lists = lists;
        this.onCustomClick = onCustomClick;
        this.title = title ;
    }

    public interface OnCustomClick{
        void onCustomClick(int position , int type);
    }

    @Override
    public PaidResultListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_paid_result_list, parent, false);
        return new PaidResultListAdapter.ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(PaidResultListAdapter.ViewHolder holder, int position) {
        holder.position = position ;
        PaidResult result = lists.get(position);
        if ( result != null ) {
            holder.tvTitle.setText( result.getTitle() );
            holder.tvPackageTitle.setText( result.getPackageTitle() );
            holder.tvDate.setText( result.getDate() );
            if ( result.getStatus() == AppConstant.INT_TRUE ) {
                holder.tvStatus.setText( "Completed" );
                holder.tvStatus.setBackgroundColor(SupportUtil.getColor( R.color.green_mcq_paid , AppApplication.getInstance()));
                holder.llBottom.setVisibility(View.VISIBLE);
            } else {
                if (result.isPracticeTest()) {
                    holder.tvStatus.setText( "Resume" );
                    holder.tvStatus.setBackgroundColor(SupportUtil.getColor( R.color.wrong_red , AppApplication.getInstance()));
                } else {
                    holder.tvStatus.setText( "Global Resume" );
                    holder.tvStatus.setBackgroundColor(SupportUtil.getColor( R.color.graph_yellow    , AppApplication.getInstance()));
                }
                holder.llBottom.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position ;
        TextView tvTitle , tvPackageTitle , tvDate , tvStatus;
        View llBottom ;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            tvPackageTitle = ( TextView ) itemView.findViewById( R.id.tv_package );
            tvDate = ( TextView ) itemView.findViewById( R.id.tv_date );
            tvStatus= ( TextView ) itemView.findViewById( R.id.tv_status );
            llBottom= itemView.findViewById( R.id.ll_bottom );
            ((TextView)itemView.findViewById(R.id.adapter_tv_global_result)).setText( title );
            itemView.findViewById(R.id.adapter_tv_global_result).setOnClickListener(this);
            itemView.findViewById(R.id.adapter_tv_solution).setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
        
        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position , v.getId() == R.id.adapter_tv_solution ? AppConstant.INT_TRUE : AppConstant.INT_FALSE );
        }
    }

}
