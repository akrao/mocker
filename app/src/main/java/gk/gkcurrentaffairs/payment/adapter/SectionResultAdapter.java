package gk.gkcurrentaffairs.payment.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.adssdk.NativeAdsAdapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.activity.QuestionSolutionActivity;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/20/2017.
 */

public class SectionResultAdapter extends NativeAdsAdapter {

    private List<PaidMockTestResult> paidMockTestResults ;
    private List<PaidQuestion> paidQuestions ;
    private boolean isLangEng ;
    Context context ;
    public SectionResultAdapter(List<PaidQuestion> paidQuestions  , List<PaidMockTestResult> paidMockTestResults , boolean isLangEng , Context context) {
        super(paidQuestions , R.layout.ads_native_unified_card , null );
        this.paidQuestions = paidQuestions ;
        this.paidMockTestResults = paidMockTestResults ;
        this.isLangEng = isLangEng ;
        this.context = context ;
    }

    public void setLangEng(boolean langEng) {
        isLangEng = langEng;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_section_result, parent, false);
        return new SectionResultAdapter.ViewHolder( view );
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolder ) {
            ViewHolder holder = (ViewHolder) viewHolder ;
            holder.position = position ;
            holder.tvQue.setText(SupportUtil.fromHtml( getTitle(position)) );
            if ( paidMockTestResults.get(position).getTimeTaken() > 0 ) {
                holder.tvDate.setText( getFormattedTime( (long) paidMockTestResults.get(position).getTimeTaken() ) );
            }
            holder.tvStatus.setText( getStatus( paidMockTestResults.get(position).getStatus() , paidMockTestResults.get(position).getActualAns() ) );
            setDataWebView( holder.webView , getQuestion(position) );
        }
    }

    private String getTitle( int position ){
        if ( isLangEng )
            return paidQuestions.get(position).getTitleEng() ;
        else
            return paidQuestions.get(position).getTitleHin() ;
    }

    private String getQuestion( int position ){
        if ( isLangEng )
            return paidQuestions.get(position).getOptionQuestionEng() ;
        else
            return paidQuestions.get(position).getOptionQuestionHin() ;
    }

    private String getStatus( int st , int actualAns ){
        String status = "Success" ;
        if ( st == AppConstant.PAID_QUE_NOT_VISIT)
            status = "Not Visited" ;
        else if ( st == AppConstant.PAID_QUE_VISIT)
            status = "Visited" ;
        else if ( st == AppConstant.PAID_QUE_REVIEW)
            status = "Mark Review" ;
        else if ( st == actualAns )
            status = "Success" ;
        else if ( st != actualAns && st < AppConstant.PAID_QUE_NOT_VISIT )
            status = "Wrong" ;
        return status ;
    }

    private String getFormattedTime( long l ){
        return String.format("%d min : %d sec",
                TimeUnit.MILLISECONDS.toMinutes(l),
                TimeUnit.MILLISECONDS.toSeconds(l) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));

    }

    public void setDataWebView(WebView webView, String data) {
        webView.loadDataWithBaseURL("http://localhost", htmlData(data) , "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:large; }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener , View.OnTouchListener{
        int position ;
        TextView tvQue , tvDate , tvStatus;
        WebView webView ;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQue = ( TextView ) itemView.findViewById( R.id.tv_que );
            tvDate = ( TextView ) itemView.findViewById( R.id.tv_time );
            tvStatus= ( TextView ) itemView.findViewById( R.id.tv_status );
            webView= ( WebView ) itemView.findViewById( R.id.wv_que );
            webView.setOnTouchListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                openQuestion();
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            openQuestion();
        }

        private void openQuestion(){
            Intent intent = new Intent( context , QuestionSolutionActivity.class );
            intent.putExtra( AppConstant.DATA , paidQuestions.get(position) );
            intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , paidMockTestResults.get(position) );
            context.startActivity( intent );
        }
    }

}
