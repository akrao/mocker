package gk.gkcurrentaffairs.payment.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.AdsSDK;
import com.adssdk.NativeUnifiedAdsViewHolder;
import com.github.mikephil.charting.charts.PieChart;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.fragment.PaidResultSectionFragment;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.SectionTimeBean;
import gk.gkcurrentaffairs.payment.model.ServerTestResultBean;
import gk.gkcurrentaffairs.payment.model.TestResultBean;
import gk.gkcurrentaffairs.payment.model.TimeDistributionBean;
import gk.gkcurrentaffairs.payment.utils.WrappingViewPager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

public class PaidResultScrollHandler {

    private ServerTestResultBean serverTestResultBean;
    private Activity context;
    public static int HEADER_COUNT = 4;
    private boolean isGlobal = false;
    private List<PaidSectionResult> sectionResults;
    public OnCustomClick onCustomClick;
    private LayoutInflater inflater;
    private Fragment fragment ;

    public void setServerTestResultBean(ServerTestResultBean serverTestResultBean) {
        this.serverTestResultBean = serverTestResultBean;
    }

    private static PaidResultScrollHandler paidResultScrollHandler ;

    public static PaidResultScrollHandler getInstance() {
        return paidResultScrollHandler;
    }

    public PaidResultScrollHandler(Activity context, List<PaidSectionResult> sectionResults, ServerTestResultBean serverTestResultBean, OnCustomClick onCustomClick) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.serverTestResultBean = serverTestResultBean;
        this.sectionResults = sectionResults;
        this.onCustomClick = onCustomClick;
        paidResultScrollHandler = this ;
    }

    private View view ;

    public void setView(View view) {
        this.view = view;
    }

    public void init(){
        loadNative();
        if (isGlobal) {
            loadHeaderResult();
            loadSubjectAnalysis();
        }else {
            if ( view.findViewById(R.id.ll_header_result)!= null ){
                view.findViewById(R.id.ll_header_result).setVisibility(View.GONE);
            }
            if ( view.findViewById(R.id.ll_subject_analysis)!= null ){
                view.findViewById(R.id.ll_subject_analysis).setVisibility(View.GONE);
            }
        }
        loadHeaderMark();
        loadTimeAnalysis();
    }

    private void loadHeaderResult(){
        setResultHeader(new HeaderResultViewHolder(view));
    }

    private void loadHeaderMark(){
        if (serverTestResultBean != null && serverTestResultBean.baseResultBean() != null
                && serverTestResultBean.baseResultBean().getStringList() != null) {
            HeaderMarksViewHolder headerMarksViewHolder = new HeaderMarksViewHolder(view) ;
            setTextInView((headerMarksViewHolder).tvOption, serverTestResultBean.baseResultBean().getStringList());
        }
    }

    private void loadTimeAnalysis(){
        setDataTimeDistribution(new HeaderTimeViewHolder(view));
    }

    private void loadSubjectAnalysis(){
        new SubjectWiseAnalysisViewHolder(view , sectionResults.size());
    }

    private RelativeLayout rlNativeAd ;
    private void loadNative(){
        rlNativeAd = view.findViewById(R.id.rl_native_ad_1);
        View view = LayoutInflater.from(rlNativeAd.getContext()).inflate( R.layout.ads_native_unified_card , rlNativeAd, false);
        rlNativeAd.removeAllViews();
        rlNativeAd.addView( view );
        final NativeUnifiedAdsViewHolder adsViewHolder = new NativeUnifiedAdsViewHolder(rlNativeAd);
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AdsSDK.getInstance().getAdsNative().bindUnifiedNativeAd(adsViewHolder , false);
                }
            }, 100);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    AdsSDK.getInstance().getAdsNative().cacheNativeAd(false);
                }
            }, 100);
        }
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public interface OnCustomClick {
        void onCustomClick(int position);
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }
    Handler handler = new Handler();

    private void setResultHeader(HeaderResultViewHolder holder) {
        TestResultBean bean = serverTestResultBean.getTestResultBean();
        holder.tvOption[0].setText(bean.getTotalMarks()+"");
        holder.tvOption[1].setText(String.format("%.2f" , SupportUtil.stringToFloat(bean.getPercentile())));
        holder.tvOption[2].setText(SupportUtil.stringToInt(bean.getUserRank())+"");
        int state = SupportUtil.stringToInt(bean.getStateRank());
        if (state > 0) {
            holder.tvOption[3].setText( state + "");
            isStateRank = false;
        } else {
            holder.tvOption[3].setText("?");
            isStateRank = true;
        }
        holder.tvOption[4].setText(bean.getMaxValue());
        holder.tvOption[5].setText(String.format("%.2f" , SupportUtil.stringToFloat(bean.getAvgValue())));

    }

    private boolean isStateRank = false;

    private void setDataTimeDistribution(HeaderTimeViewHolder holder) {
        TimeDistributionBean bean = serverTestResultBean.getTimeDistributionBean();
        if (bean != null) {
            holder.tvOption[0].setText(bean.getTotalTime());
            holder.tvOption[1].setText(bean.getCorrectAnsTime());
            holder.tvOption[2].setText(bean.getWrongAnsTime());
            holder.tvOption[3].setText(bean.getSkippedAnsTime());
            if ( ((LinearLayout)holder.linearLayout).getChildCount() == 6 && bean.getSectionTimeBeen() != null ) {
                for (SectionTimeBean timeBean : bean.getSectionTimeBeen()) {
                    View inflatedLayout = inflater.inflate(R.layout.item_header_section_time, holder.linearLayout, false);
                    ((TextView) inflatedLayout.findViewById(R.id.tv_section)).setText(timeBean.getTitle());
                    ((TextView) inflatedLayout.findViewById(R.id.tv_time)).setText(timeBean.getTime());
                    holder.linearLayout.addView(inflatedLayout);
                }
            }
        }
    }


    private void setTextInView(TextView[] view, ArrayList<String> strings) {
        for (int i = 0; i < view.length; i++) {
            view[i].setText(strings.get(i));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        int position;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class SubjectWiseAnalysisViewHolder extends ViewHolder {

        public SubjectWiseAnalysisViewHolder(View itemView , int child) {
            super(itemView);
            this.child = child ;
            viewPager = (WrappingViewPager) itemView.findViewById(R.id.viewpager);
            setupViewPager();
            TabLayout tabLayout = (TabLayout) itemView.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setTabMode( child > 3 ? TabLayout.MODE_SCROLLABLE : TabLayout.MODE_FIXED );
        }

        int child ;
        ViewPagerAdapter adapter;
        private WrappingViewPager viewPager ;

        public void setupViewPager() {
            if ( fragment != null && fragment.isAdded() && !fragment.isDetached() ) {
                adapter = new ViewPagerAdapter(fragment.getChildFragmentManager() , child);
                Bundle bundle ;
                PaidResultSectionFragment paidResultSectionFragment ;
                for ( int i = 0 ; i < child ; i++ ){
                    if ( sectionResults != null && sectionResults.size() > i
                            && serverTestResultBean != null
                            &&serverTestResultBean.getTestResultBean() != null
                            && serverTestResultBean.getTestResultBean().getSectionResultBeen() != null
                            && serverTestResultBean.getTestResultBean().getSectionResultBeen().size() > i ) {
                        paidResultSectionFragment = new PaidResultSectionFragment();
                        bundle = new Bundle();
                        bundle.putSerializable(AppConstant.DATA , sectionResults.get(i) );
                        bundle.putSerializable(AppConstant.CAT_DATA
                                , serverTestResultBean.getTestResultBean().getSectionResultBeen().get(i) );
                        bundle.putBoolean(AppConstant.TYPE , isGlobal );
                        bundle.putInt(AppConstant.POSITION , i );
                        paidResultSectionFragment.setArguments(bundle);
                        adapter.addFrag(paidResultSectionFragment, sectionResults.get(i).getTitle());
                    }
                }
                if ( viewPager != null && adapter != null && adapter.getCount() > 0 ){
                    viewPager.setAdapter(adapter);
                    viewPager.setOffscreenPageLimit(child);
                }
            }
        }

    }

    class SectionViewHolder extends ViewHolder implements View.OnClickListener {

        TextView tvTitle, tvTime, tvCorrect, tvWrong, tvUnAttended, tvScore, tvPercentile, tvRank, tvStateRank, correctAnsTime, wrongAnsTime;
        //        PieView pieView;
        View vDivider, vPercent;
        PieChart pieChart ;

        public SectionViewHolder(View itemView) {
            super(itemView);
            tvRank = (TextView) itemView.findViewById(R.id.adapter_tv_rank);
            tvStateRank = (TextView) itemView.findViewById(R.id.adapter_tv_state_rank);
            correctAnsTime = (TextView) itemView.findViewById(R.id.tv_correct_ans_time);
            wrongAnsTime = (TextView) itemView.findViewById(R.id.tv_wrong_ans_time);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvCorrect = (TextView) itemView.findViewById(R.id.tv_correct);
            tvWrong = (TextView) itemView.findViewById(R.id.tv_wrong);
            tvUnAttended = (TextView) itemView.findViewById(R.id.tv_unattended);
            tvScore = (TextView) itemView.findViewById(R.id.tv_score);
            tvPercentile = (TextView) itemView.findViewById(R.id.tv_percentile);
//            pieView = (PieView) itemView.findViewById(R.id.pv_sec);
            pieChart = (PieChart) itemView.findViewById(R.id.pc_sec);
            pieChart.setDrawHoleEnabled(false);
            vDivider = itemView.findViewById(R.id.ll_rank);
            vPercent = itemView.findViewById(R.id.ll_percent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(sectionResults.get(position-HEADER_COUNT).getPosition());
        }
    }

    class HeaderResultViewHolder extends ViewHolder {
        TextView[] tvTitle, tvOption;

        public HeaderResultViewHolder(View itemView) {
            super(itemView);
            if (isGlobal) {
                tvTitle = new TextView[5];
                tvOption = new TextView[6];
                itemView.findViewById(R.id.ll_state_rank).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( isStateRank ){
                            SupportUtil.openDialogStateRank(context);
                        }
                    }
                });
                for (int i = 0; i < tvTitleId.length; i++) {
                    tvTitle[i] = (TextView) itemView.findViewById(tvTitleId[i]);
                }
                for (int i = 0; i < tvOptionId.length; i++) {
                    tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
                }
            }
        }

        private int[] tvOptionId = {
                R.id.tv_opt_1,
                R.id.tv_opt_2,
                R.id.tv_opt_3,
                R.id.tv_opt_4,
                R.id.tv_opt_5,
                R.id.tv_opt_6
        };
        private int[] tvTitleId = {
                R.id.tv_title_1,
                R.id.tv_title_2,
                R.id.tv_title_3,
                R.id.tv_title_5,
                R.id.tv_title_4
        };
    }

    class HeaderMarksViewHolder extends ViewHolder {
        TextView[] tvOption;

        public HeaderMarksViewHolder(View itemView) {
            super(itemView);
            tvOption = new TextView[6];
            for (int i = 0; i < tvOptionId.length; i++) {
                tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
            }
        }

        private int[] tvOptionId = {
                R.id.tv_op_1,
                R.id.tv_op_2,
                R.id.tv_op_3,
                R.id.tv_op_4,
                R.id.tv_op_5,
                R.id.tv_op_6
        };
    }

    class HeaderTimeViewHolder extends ViewHolder {
        TextView[] tvOption;
        LinearLayout linearLayout;

        public HeaderTimeViewHolder(View itemView) {
            super(itemView);
            tvOption = new TextView[6];
            for (int i = 0; i < tvOptionId.length; i++) {
                tvOption[i] = (TextView) itemView.findViewById(tvOptionId[i]);
            }
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_body);
        }

        private int[] tvOptionId = {
                R.id.tv_opt_t_1,
                R.id.tv_opt_t_2,
                R.id.tv_opt_t_3,
                R.id.tv_opt_t_4,
                R.id.tv_opt_5,
                R.id.tv_opt_6
        };
    }

    private String[] titleString = {"OVERALL RESULT", "OVERALL SCORE", "PERCENTILE", "NATIONAL RANK", "STATE RANK"};

}
