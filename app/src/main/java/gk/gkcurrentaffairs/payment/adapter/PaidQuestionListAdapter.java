package gk.gkcurrentaffairs.payment.adapter;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 9/8/2017.
 */

public class PaidQuestionListAdapter extends RecyclerView.Adapter<PaidQuestionListAdapter.ViewHolder> {

    private List<PaidMockTestResult> children;
    private OnCustomClick onCustomClick;

    public PaidQuestionListAdapter(List<PaidMockTestResult> children, OnCustomClick onCustomClick) {
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick {
        void onCustomClick(int position);
    }

    @Override
    public PaidQuestionListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dlg_list_mock, parent, false);
        return new PaidQuestionListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaidQuestionListAdapter.ViewHolder holder, int position) {
        holder.position = position;
        PaidMockTestResult child = children.get(position);
        if (child != null) {
            int c = position  + 1 ;
            int res = R.drawable.circle_green ;
            int textColor = Color.BLACK ;
            if ( child.getStatus() == AppConstant.PAID_QUE_NOT_VISIT ){
                res = R.drawable.circle_black_border ;
                textColor = Color.BLACK ;
            }else if ( child.getStatus() == AppConstant.PAID_QUE_VISIT ){
                res = R.drawable.circle_red ;
                textColor = Color.WHITE ;
            }

            if ( child.isMarkReview() && child.getStatus() < AppConstant.PAID_QUE_NOT_VISIT ){
                res = R.drawable.circle_yellow ;
                textColor = Color.BLACK ;
            }else if ( child.isMarkReview() ){
                res = R.drawable.circle_blue ;
                textColor = Color.WHITE ;
            }
            holder.tvTitle.setText( c + "" );
            holder.tvTitle.setBackgroundResource( res );
            holder.tvTitle.setTextColor( textColor );
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        TextView tvTitle ;
 
        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            itemView.setOnClickListener(this);
        }
    }
}
