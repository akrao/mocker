package gk.gkcurrentaffairs.payment.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.HelpQuestionBean;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/8/2017.
 */

public class HelpQuestionAdapter extends RecyclerView.Adapter<HelpQuestionAdapter.ViewHolder> {

    private List<HelpQuestionBean> children ;
    private OnCustomClick onCustomClick ;
    private int selectedPos ;
    private boolean isSelection = false;

    public int getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public void setIsSelection(boolean isSelection) {
        this.isSelection = isSelection;
    }

    public boolean isSelection() {
        return isSelection;
    }

    public HelpQuestionAdapter(List<HelpQuestionBean> children , OnCustomClick onCustomClick ) {
        this.children = children;
        this.onCustomClick = onCustomClick;
    }

    public interface OnCustomClick{
        void onCustomClick(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_help_question, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position ;
        HelpQuestionBean child = children.get( position );
        if ( child != null ){
            holder.tvTitle.setText(SupportUtil.fromHtml(AppApplication.getInstance().getDBObject().removePadding(child.getQuestion()) ) );
            if ( isSelection && position == selectedPos ){
                setDataWebView( holder.webView , child.getAnswer());
                holder.imageView.setImageResource( R.drawable.minus );
                holder.vBottom.setVisibility(View.VISIBLE);
            }else {
                holder.vBottom.setVisibility(View.GONE);
                holder.imageView.setImageResource( R.drawable.plus );
            }
        }
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        int position ;
        TextView tvTitle ;
        View vBottom ;
        WebView webView ;
        ImageView imageView ;

        @Override
        public void onClick(View v) {
            onCustomClick.onCustomClick(position);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = ( ImageView ) itemView.findViewById( R.id.iv_add );
            tvTitle = ( TextView ) itemView.findViewById( R.id.tv_title );
            webView = ( WebView) itemView.findViewById( R.id.wv_ans );
            vBottom = itemView.findViewById( R.id.ll_bottom );
            itemView.setOnClickListener(this);
        }
    }

    public void setDataWebView(WebView webView, String data) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("http://localhost", htmlData(data, "#030"), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: " + color + "; font-size:large; }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

}
