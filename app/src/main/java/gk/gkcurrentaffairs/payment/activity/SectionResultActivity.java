package gk.gkcurrentaffairs.payment.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.SectionResultAdapter;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/16/2018.
 */

public class SectionResultActivity extends AppCompatActivity implements SupportUtil.OnCustomResponse {

    private MenuItem menuLang;
    private List<PaidQuestion> paidQuestions ;
    private List<PaidMockTestResult> paidMockTestResults ;
    private boolean isLangEng ;
    private SectionResultAdapter sectionResultAdapter ;
    private String title ;
    private Activity activity ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_list_no_data);
        try {
            activity = this ;
            initDataFromArgs();
            setDataInList();
            setupToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    public void refreshData()throws Exception{
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        sectionResultAdapter.setLangEng(isLangEng);
        sectionResultAdapter.notifyDataSetChanged();
    }

    private void initData(){
        PaidResult paidResult = ( PaidResult ) getIntent().getSerializableExtra(AppConstant.DATA);
        PaidSectionResult paidSectionResult = ( PaidSectionResult ) getIntent().getSerializableExtra(AppConstant.CLICK_ITEM_ARTICLE);
        int position = getIntent().getIntExtra( AppConstant.POSITION , 0 );
        paidMockTestResults = new ArrayList<>(paidResult.getPaidMockTestResults().get(position).size());
        paidQuestions = new ArrayList<>(paidResult.getTestCats().get(position).getPaidQuestions().size());
        paidQuestions.addAll(paidResult.getTestCats().get(position).getPaidQuestions());
        paidMockTestResults.addAll(paidResult.getPaidMockTestResults().get(position));

    }
    private void initDataFromArgs() throws Exception{
        title = getIntent().getStringExtra(AppConstant.TITLE);
        paidQuestions = (List<PaidQuestion>)getIntent().getSerializableExtra(AppConstant.DATA);
        paidMockTestResults = (List<PaidMockTestResult>)getIntent().getSerializableExtra(AppConstant.CLICK_ITEM_ARTICLE);
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        if ( paidMockTestResults == null || paidQuestions == null ) {
            SupportUtil.showToastCentre( activity , "Error, please try again" );
            activity.finish();
        }
    }

    private void setDataInList()throws Exception{
        findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
        RecyclerView recyclerView = (RecyclerView) findViewById( R.id.itemsRecyclerView );
        recyclerView.setLayoutManager( new LinearLayoutManager( activity.getApplicationContext() ) );
        sectionResultAdapter = new SectionResultAdapter( paidQuestions , paidMockTestResults , isLangEng , activity ) ;
        recyclerView.setAdapter( sectionResultAdapter );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lang, menu);
        menuLang = menu.findItem(R.id.action_lang);
        setMenuTitle();
        return true;
    }

    private void setMenuTitle() {
        menuLang.setTitle(isLangEng ? "En" : "Hi");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.action_lang) {
            SupportUtil.openLangDialog(this, this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomResponse(boolean result) {
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        setMenuTitle();
        try {
            refreshData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
