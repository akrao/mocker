///*
//package gk.gkcurrentaffairs.payment.activity;
//
//import android.app.ProgressDialog;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.gson.JsonSyntaxException;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPGService;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.HashMap;
//import java.util.Map;
//
//import gk.gkcurrentaffairs.AppApplication;
//import gk.gkcurrentaffairs.R;
//import gk.gkcurrentaffairs.config.ConfigConstant;
//import gk.gkcurrentaffairs.config.ConfigManager;
//import gk.gkcurrentaffairs.network.ApiPayInterface;
//import gk.gkcurrentaffairs.payment.Constants;
//import gk.gkcurrentaffairs.payment.model.AdFreeSlab;
//import gk.gkcurrentaffairs.payment.model.MockInfo;
//import gk.gkcurrentaffairs.payment.model.PackageInfo;
//import gk.gkcurrentaffairs.payment.model.PaymentResponse;
//import gk.gkcurrentaffairs.payment.model.PaymentResult;
//import gk.gkcurrentaffairs.payment.model.ServiceCost;
//import gk.gkcurrentaffairs.payment.model.SubscriptionOffersBean;
//import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
//import gk.gkcurrentaffairs.util.AppConstant;
//import gk.gkcurrentaffairs.util.Logger;
//import gk.gkcurrentaffairs.util.SupportUtil;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//
//public class ConfirmationActivity extends AppCompatActivity implements View.OnClickListener {
//
//    private PaymentResponse paymentResponse;
//    private String user_id, device_id, email, amount;
//    private TextView tvConfirmPay, tvBenefits, tvCosts, tvTotalPoints, tvWalletPoints, tvBalancePoints;
//    private ApiPayInterface apiPayInterface;
//    private AdFreeSlab adFreeSlab;
//    private ProgressDialog pDialog;
//    private String SERVICE_TYPE;
//    private ImageView ivAdFreeRow;
//    private ServiceCost serviceCost;
//    private View walletInfo, resultInfo;
//    private PaymentResult paymentResult;
//    private int productId;
//    private PackageInfo packageInfo ;
//    private MockInfo mockInfo ;
//    private SubscriptionOffersBean subscriptionOffersBean ;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_confirmation);
//        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
//            initView();
//            initDialog();
//            initDataSet();
//            initDataFromArgs();
////        fetchPayableAmount();
//            showData();
//        }
//    }
//
//    private void showData(){
//        ivAdFreeRow.setImageResource( R.drawable.test );
//        tvBenefits.setText(subscriptionOffersBean.getSdays() +" " + AppConstant.OFFER_DAYS);
//        tvCosts.setText("₹"+ subscriptionOffersBean.getScost());
//        tvConfirmPay.setText("Pay now Rs " + subscriptionOffersBean.getScost() );
//    }
//
//    private void showData1() {
//        String payString = serviceCost.getNeedToSpend() > 0 ? "Pay now Rs " + serviceCost.getNeedToSpend() : "From Wallet Point- " + serviceCost.getWalletDeduction();
//        tvConfirmPay.setText(payString);
//        if (SERVICE_TYPE.equalsIgnoreCase(AppConstant.SERVICE_ADS)) {
//            ivAdFreeRow.setImageResource(R.drawable.point);
//            tvBenefits.setText(adFreeSlab.getBenefits() + AppConstant.BENEFITS_AD_FREE);
//            tvCosts.setText(adFreeSlab.getCost() + AppConstant.COST_AD_FREE);
//            tvTotalPoints.setText(serviceCost.getProductCost() + "");
//            tvWalletPoints.setText(serviceCost.getUserPoints() + "");
//            tvBalancePoints.setText((serviceCost.getUserPoints() - serviceCost.getWalletDeduction()) + "");
//        } else if (SERVICE_TYPE.equalsIgnoreCase(AppConstant.SERVICE_PACKAGE_MOCK_TEST) || SERVICE_TYPE.equalsIgnoreCase(AppConstant.SERVICE_PACKAGE_SUB_TEST)) {
//            ivAdFreeRow.setImageResource(R.drawable.paid_package);
//            tvBenefits.setText( packageInfo.getTitle() );
//            tvCosts.setText(packageInfo.getPrice() + AppConstant.COST_AD_FREE);
//            tvTotalPoints.setText(serviceCost.getProductCost() + "");
//            tvWalletPoints.setText(serviceCost.getUserPoints() + "");
//            tvBalancePoints.setText((serviceCost.getUserPoints() - serviceCost.getWalletDeduction()) + "");
//        } else if (SERVICE_TYPE.equalsIgnoreCase(AppConstant.SERVICE_MOCK_TEST) ) {
//            ivAdFreeRow.setImageResource(R.drawable.paidmock_test);
//            tvBenefits.setText( mockInfo.getTitle() );
//            tvCosts.setText(mockInfo.getPrice() + AppConstant.COST_AD_FREE);
//            tvTotalPoints.setText(serviceCost.getProductCost() + "");
//            tvWalletPoints.setText(serviceCost.getUserPoints() + "");
//            tvBalancePoints.setText((serviceCost.getUserPoints() - serviceCost.getWalletDeduction()) + "");
//        } else {
//            ivAdFreeRow.setImageResource(R.drawable.point);
//            tvBenefits.setText(adFreeSlab.getBenefits() + AppConstant.COST_AD_FREE);
//            tvCosts.setText(adFreeSlab.getCost() + AppConstant.CURRENCY);
//            walletInfo.setVisibility(View.GONE);
//        }
//    }
//
//    private void initView() {
//        tvConfirmPay = (TextView) findViewById(R.id.tv_confirm_pay);
//        tvCosts = (TextView) findViewById(R.id.tv_ad_free_row_amount);
//        tvBenefits = (TextView) findViewById(R.id.tv_ad_free_row);
//        tvTotalPoints = (TextView) findViewById(R.id.tv_total_points);
//        tvWalletPoints = (TextView) findViewById(R.id.tv_wallet_points);
//        tvBalancePoints = (TextView) findViewById(R.id.tv_bal_points);
//        ivAdFreeRow = (ImageView) findViewById(R.id.iv_ad_free_row);
//        tvConfirmPay.setOnClickListener(this);
//        getSupportActionBar().setTitle("Order Confirmation");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        walletInfo = findViewById(R.id.rl_wallet_info);
//        resultInfo = findViewById(R.id.ll_result);
//    }
//
//    private void initDialog() {
//        pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Fetching Data....");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(false);
//    }
//
//    private void showDialog() {
//        try {
//            pDialog.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void hideDialog() {
//        try {
//            pDialog.dismiss();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == android.R.id.home) {
//            finish();
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    private void initDataSet() {
//            user_id = AppApplication.getInstance().getLoginSdk().getUserId();
//            email = AppApplication.getInstance().getLoginSdk().getEmailId();
//        device_id = SupportUtil.getDeviceId();
//    }
//
//    private void initDataFromArgs() {
//        SERVICE_TYPE = getIntent().getStringExtra(AppConstant.TYPE);
//        switch (SERVICE_TYPE) {
//            case AppConstant.SERVICE_ADS:
//            case AppConstant.SERVICE_POINT:
//                adFreeSlab = (AdFreeSlab) getIntent().getSerializableExtra(AppConstant.DATA);
//                productId = adFreeSlab.getId() ;
//                break;
//            case AppConstant.SERVICE_PACKAGE_MOCK_TEST :
//            case AppConstant.SERVICE_PACKAGE_SUB_TEST :
//                packageInfo = (PackageInfo) getIntent().getSerializableExtra(AppConstant.DATA);
//                productId = packageInfo.getId() ;
//                break;
//            case AppConstant.SERVICE_MOCK_TEST :
//                mockInfo = (MockInfo) getIntent().getSerializableExtra(AppConstant.DATA);
//                productId = mockInfo.getId() ;
//                break;
//            case AppConstant.SERVICE_SUBSCRIBE :
//                subscriptionOffersBean = (SubscriptionOffersBean) getIntent().getSerializableExtra( AppConstant.DATA );
//                productId = subscriptionOffersBean.getId() ;
//                amount = subscriptionOffersBean.getScost() + "";
//                break;
//         }
//    }
//
//    private void fetchPayableAmount() {
//        showDialog();
//        apiPayInterface.getServiceCost(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO), productId , SERVICE_TYPE).enqueue(new Callback<ServiceCost>() {
//            @Override
//            public void onResponse(Call<ServiceCost> call, Response<ServiceCost> response) {
//                hideDialog();
//                if (response != null && response.body() != null) {
//                    serviceCost = response.body();
//                    amount = serviceCost.getNeedToBuy() + "";
//                    showData();
//                } else
//                    Toast.makeText(ConfirmationActivity.this, "Error", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<ServiceCost> call, Throwable t) {
//                hideDialog();
//                Toast.makeText(ConfirmationActivity.this, "Error", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void handleResult() {
//        walletInfo.setVisibility(View.GONE);
//        resultInfo.setVisibility(View.VISIBLE);
//        TextView transId = (TextView) findViewById(R.id.tv_trans_id);
//        TextView orderId = (TextView) findViewById(R.id.tv_order_id);
//        TextView result = (TextView) findViewById(R.id.tv_pay_result);
//
//        transId.setText("Transaction ID : " + paymentResult.getTransitionId());
//        orderId.setText("Order ID       : " + paymentResult.getOrderId());
//        result.setText(paymentResult.getMessage());
//        result.setBackgroundColor(paymentResult.getStatus().equalsIgnoreCase("success") ? Color.GREEN : Color.RED);
//
//        getSupportActionBar().setTitle("Finish");
//        tvConfirmPay.setText("Finish");
//        if ( paymentResult.getStatus().equalsIgnoreCase("success") && paymentResult != null && paymentResult.getUserPoints() != null ) {
//            SharedPrefUtil.setInt(AppConstant.SharedPref.USER_POINTS, paymentResult.getUserPoints());
//        }
//    }
//
//    private void initWalletPayRequest() {
//        showData();
//        apiPayInterface.initWalletPaymentRequest(
//                user_id,
//                device_id,
//                AppConstant.DEVICE_TYPE_ID
//                , email,
//                adFreeSlab.getId(),
//                SERVICE_TYPE,
//                serviceCost.getWalletDeduction()).enqueue(new Callback<PaymentResult>() {
//            @Override
//            public void onResponse(Call<PaymentResult> call, Response<PaymentResult> response) {
//                if (response != null && response.body() != null) {
//                    paymentResult = response.body();
//                    handleResult();
//                } else
//                    hideDialog();
//            }
//
//            @Override
//            public void onFailure(Call<PaymentResult> call, Throwable t) {
//                hideDialog();
//            }
//        });
//    }
//
//    private void initPaymentRequestPaytm() {
//        showData();
//        showDialog();
//        Logger.e("initPaymentRequestTest" );
//        Map<String, String> map = new HashMap<>(9);
//        map.put("payment_brand_id" , AppConstant.PAYMENT_BRAND_ID);
//        map.put("user_id" , user_id);
//        map.put("device_id" , device_id);
//        map.put("device_type" , AppConstant.DEVICE_TYPE_ID);
//        map.put("email" , email);
//        map.put("product_id" , productId+"");
//        map.put("product_type" , SERVICE_TYPE);
//        map.put("amount" , amount);
//        map.put("application_id" , getPackageName());
//        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_POST, ConfigConstant.HOST_PAID
//                , Constants.INIT_PAYMENT+AppConstant.TEST, map, new ConfigManager.OnNetworkCall() {
//                    @Override
//                    public void onComplete(boolean status, String data) {
//                        hideDialog();
//                        if (status && !SupportUtil.isEmptyOrNull(data)) {
//                            try {
//                                PaymentResponse packageInfo1 = ConfigManager.getGson().fromJson(data , PaymentResponse.class);
//                                if ( packageInfo1 != null && !SupportUtil.isEmptyOrNull( packageInfo1.getORDERID() ) ){
//                                    paymentResponse = packageInfo1;
//                                    startTransaction();
//                                }else
//                                    hideDialog();
//                            } catch (JsonSyntaxException e) {
//                                e.printStackTrace();
//                                hideDialog();
//                            }
//                        }else
//                            hideDialog();
//                    }
//                });
//
//*/
///*
//        apiPayInterface.initPaymentRequestTest(AppConstant.PAYMENT_BRAND_ID,
//                user_id,
//                device_id,
//                AppConstant.DEVICE_TYPE_ID
//                , email,
//                productId,
//                SERVICE_TYPE,
//                serviceCost.getWalletDeduction(),
//                serviceCost.getNeedToSpend()).enqueue(new Callback<PaymentResponse>() {
//            @Override
//            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
//                if (response != null && response.body() != null && !SupportUtil.isEmptyOrNull( response.body().getORDERID() )) {
//                    paymentResponse = response.body();
//                    startTransaction();
//                } else
//                    hideDialog();
//            }
//
//            @Override
//            public void onFailure(Call<PaymentResponse> call, Throwable t) {
//                hideDialog();
//            }
//        });
//*//*
//
//    }
//
//
//    public void paytmCallBackPayment(String data) {
//        showDialog();
//        Logger.e("paytmCallBackPayment" );
//        Map<String, String> map = new HashMap<>(2);
//        map.put("data" , data);
//        map.put("application_id" , getPackageName());
//        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_POST_FORM, ConfigConstant.HOST_PAID
//                , Constants.PAY_TM_CALLBACK+AppConstant.TEST, map, new ConfigManager.OnNetworkCall() {
//                    @Override
//                    public void onComplete(boolean status, String data) {
//                        hideDialog();
//                        if (status && !SupportUtil.isEmptyOrNull(data)) {
//                            try {
//                                PaymentResult paymentResult1 = ConfigManager.getGson().fromJson(data , PaymentResult.class);
//                                if ( paymentResult1 != null ){
//                                    paymentResult = paymentResult1 ;
//                                    handleResult();
//                               }else
//                                    hideDialog();
//                            } catch (JsonSyntaxException e) {
//                                e.printStackTrace();
//                                hideDialog();
//                            }
//                        }else
//                            hideDialog();
//                    }
//                });
// */
///*       apiPayInterface.PaytmCallbackRequestTest(data).enqueue(new Callback<PaymentResult>() {
//            @Override
//            public void onResponse(Call<PaymentResult> call, Response<PaymentResult> response) {
//                if (response != null && response.body() != null) {
//                    paymentResult = response.body();
//                    handleResult();
////					Toast.makeText( ConfirmationActivity.this , "Success" , Toast.LENGTH_SHORT ).show();
//                } else
//                    hideDialog();
//            }
//
//            @Override
//            public void onFailure(Call<PaymentResult> call, Throwable t) {
//                hideDialog();
//            }
//        });
// *//*
//
//    }
//
//    //This is to refresh the order id: Only for the Sample App's purpose.
//    @Override
//    protected void onStart() {
//        super.onStart();
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//    }
//
//
//    public void startTransaction() {
//        PaytmPGService Service;
//        if ( SupportUtil.isEmptyOrNull(AppConstant.TEST )) {
//            Service = PaytmPGService.getProductionService();
//        } else {
//            Service = PaytmPGService.getStagingService();
//        }
////        PaytmPGService Service = PaytmPGService.getStagingService();
//        Map<String, String> paramMap = new HashMap<String, String>();
//        paramMap.put("MID", paymentResponse.getMID());
//        paramMap.put("ORDER_ID", paymentResponse.getORDERID());
//        paramMap.put("CUST_ID", paymentResponse.getCUSTID());
//        paramMap.put("INDUSTRY_TYPE_ID", paymentResponse.getINDUSTRYTYPEID());
//        paramMap.put("CHANNEL_ID", paymentResponse.getCHANNELID());
//        paramMap.put("TXN_AMOUNT", paymentResponse.getTXNAMOUNT());
//        paramMap.put("WEBSITE", paymentResponse.getWEBSITE());
//        paramMap.put("CALLBACK_URL", paymentResponse.getCALLBACKURL());
//        paramMap.put("CHECKSUMHASH", paymentResponse.getCHECKSUMHASH());
//        PaytmOrder Order = new PaytmOrder(paramMap);
//
//        Service.initialize(Order, null);
//
//        Service.startPaymentTransaction(this, true, true,
//                new PaytmPaymentTransactionCallback() {
//
//                    @Override
//                    public void someUIErrorOccurred(String inErrorMessage) {
//                        Log.d("LOG", "networkNotAvailable " + inErrorMessage);
//                    }
//
//                    @Override
//                    public void onTransactionResponse(Bundle inResponse) {
//                        Log.d("LOG", "Payment Transaction : " + inResponse);
//                        if (inResponse == null) {
//                            return;
//                        }
//                        JSONObject jsonObject = new JSONObject();
//                        for (String key : inResponse.keySet()) {
//                            try {
//                                if ( key.equalsIgnoreCase("CHECKSUMHASH") && inResponse.get(key) != null ) {
//                                    try {
//                                        jsonObject.put(key, URLEncoder.encode(inResponse.get(key).toString(), "utf-8"));
//                                    } catch (UnsupportedEncodingException e) {
//                                        e.printStackTrace();
//                                        jsonObject.put(key, inResponse.get(key));
//                                    }
//                                } else {
//                                    jsonObject.put(key, inResponse.get(key));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                        paytmCallBackPayment(jsonObject.toString());
//
//                        Log.d("status", "Payment Transaction : " + inResponse.get("data"));
////						Toast.makeText(getApplicationContext(), "Payment Transaction response "+inResponse.toString(), Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void networkNotAvailable() {
//                        Log.d("LOG", "networkNotAvailable ");
//                    }
//
//                    @Override
//                    public void clientAuthenticationFailed(String inErrorMessage) {
//                        Log.d("LOG", "Payment Transaction back " + inErrorMessage);
//                    }
//
//                    @Override
//                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
//                        Log.i("onErrorLoadingWebPage", "onErrorLoadingWebPage");
//                    }
//
//                    // had to be added: NOTE
//                    @Override
//                    public void onBackPressedCancelTransaction() {
//                        // TODO Auto-generated method stub
//                        Log.d("LOG", "Payment Transaction back ");
//                    }
//
//                    @Override
//                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
//                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
//                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
//                    }
//
//                });
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.tv_confirm_pay:
//                if (paymentResult == null) {
//*/
///*
//                    if (serviceCost.isWalletTransition()) {
//                        initWalletPayRequest();
//                    } else {
//*//*
//
//                        initPaymentRequestPaytm();
////                    }
//                } else {
//                    finish();
//                }
//                break;
//        }
//    }
//}
//*/
