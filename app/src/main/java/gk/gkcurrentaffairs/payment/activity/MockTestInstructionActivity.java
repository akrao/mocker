package gk.gkcurrentaffairs.payment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.TempResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/1/2018.
 */

public class MockTestInstructionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_test_instruction );
        setupToolBar();
        initDataFromIntent();
        initView();
    }

    private PaidMockTest paidMockTest ;
    private String packageInfoTitle ;
    private int packageInfoId ;
    private void initDataFromIntent() {
        paidMockTest = (PaidMockTest) getIntent().getSerializableExtra(AppConstant.DATA);
        packageInfoId = getIntent().getIntExtra( AppConstant.CAT_ID , 0 );
        packageInfoTitle = getIntent().getStringExtra( AppConstant.TITLE );
        dbHelper = AppApplication.getInstance().getDBObject() ;
    }

    private void initView(){
        WebView webView = (WebView) findViewById( R.id.web_view );
        setDataWebView( webView , paidMockTest.getDescription() );
        findViewById( R.id.tv_start ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SupportUtil.openLangDialog(MockTestInstructionActivity.this, true , new SupportUtil.OnCustomResponse() {
                    @Override
                    public void onCustomResponse(boolean result) {
                        openMcq();
                    }
                });
            }
        });
    }

    public void setDataWebView(WebView webView, String data ) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data ), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent ) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:large; font-family:roboto_regular;"
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    private DbHelper dbHelper ;

    private void openMcq(){
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                TempResult result = null ;
                if ( dbHelper.isPaidGlobalResultPending(paidMockTest.getId())) {
                    PaidResult paidResult = dbHelper.fetchPaidResult(paidMockTest.getId() , true);
                    paidResult = dbHelper.updatePaidResult(paidResult , paidMockTest );
                    result = getResult(paidResult);
                }
                Intent intent = new Intent(MockTestInstructionActivity.this , PaidMCQActivity.class );
                intent.putExtra( AppConstant.DATA , paidMockTest.getId() );
                intent.putExtra( AppConstant.CLICK_ITEM_ARTICLE , result );
                intent.putExtra( AppConstant.TITLE , packageInfoTitle );
                intent.putExtra( AppConstant.CAT_ID , packageInfoId );
                startActivity(intent);
                onBackPressed();
                return null;
            }
        });
    }

    private TempResult getResult(PaidResult result){
        TempResult tempResult = new TempResult();
        tempResult.setLastQuePos( result.getLastQuePos() );
        tempResult.setLastSectionPos( result.getLastSectionPos() );
        tempResult.setMockId( result.getMockId() );
        tempResult.setId( result.getId() );
        tempResult.setEndTimeStamp( result.getEndTimeStamp() );
        tempResult.setSectionCount( result.getSectionCount() );
        tempResult.setStartTimeStamp( result.getStartTimeStamp() );
        tempResult.setTimeTaken( result.getTimeTaken() );
        tempResult.setPaidMockTestResults( result.getPaidMockTestResults() );
        return tempResult ;
    }


    private void setupToolBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Instruction");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            onBackPressed();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

}
