package gk.gkcurrentaffairs.payment.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.network.ApiPayInterface;
import gk.gkcurrentaffairs.payment.adapter.HelpSupportAdapter;
import gk.gkcurrentaffairs.payment.model.CatBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/8/2017.
 */

public class HelpSupportActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener , HelpSupportAdapter.OnCustomClick , View.OnClickListener{

    private View tvNoData , pbLoading , vNoData ;
    private List<CatBean> lists = new ArrayList<>();
    private ApiPayInterface apiPayInterface ;
    private HelpSupportAdapter adapter ;
    private SwipeRefreshLayout swipeRefreshLayout ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support);
        initViews();
        fetchData( true );
    }

    private void initViews(){
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        vNoData = findViewById( R.id.ll_no_data );
        tvNoData = findViewById( R.id.tv_no_data );
        findViewById(R.id.tv_contact_us ).setOnClickListener(this);
        pbLoading = findViewById( R.id.player_progressbar );
//        apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
        getSupportActionBar().setTitle( "FAQ" );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showData(){
        swipeRefreshLayout.setRefreshing(false);
        if ( adapter == null ) {
            vNoData.setVisibility( View.GONE );
            RecyclerView recyclerView = (RecyclerView) findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager( new LinearLayoutManager( this ) );
            adapter = new HelpSupportAdapter( lists , this );
            recyclerView.setAdapter( adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void fetchData(final boolean isLatestData ){
        Map<String, String> map = new HashMap<>(1);
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID,
                ApiEndPoint.GET_FAQ_CAT
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        Logger.e( "fetchLatestData" );
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                List<CatBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<CatBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    lists.addAll( list );
                                    showData();
                                } else {
                                    showNoData();
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        }else
                            showNoData();
                    }
                });

/*
        if ( !SupportUtil.isNotConnected( this ) ) {
            apiPayInterface.getHelpSupportCat().enqueue(new Callback<List<CatBean>>() {
                @Override
                public void onResponse(Call<List<CatBean>> call, Response<List<CatBean>> response) {
                    if ( response != null && response.body() != null
                            && response.body().size() > 0){
                        lists.addAll( response.body() );
                        showData();
                    }else
                        showNoData();
                }

                @Override
                public void onFailure(Call<List<CatBean>> call, Throwable t) {
                    showNoData();
                }
            });
        } else {
            SupportUtil.showToastInternet(this);
            showNoData();
        }
*/
    }

    private void showNoData(){
        swipeRefreshLayout.setRefreshing(false);
        if ( lists.size() < 1 ){
            pbLoading.setVisibility( View.GONE );
            tvNoData.setVisibility( View.VISIBLE );
        }
    }

    @Override
    public void onCustomClick(int position) {
        Intent intent = new Intent( this , HelpQuestionActivity.class );
        intent.putExtra(AppConstant.CAT_ID , lists.get(position).getId());
        intent.putExtra(AppConstant.TITLE , lists.get(position).getTitle());
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        fetchData(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",AppConstant.EMAIL_HELP_SUPPORT, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Help And Support");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
}
