package gk.gkcurrentaffairs.payment.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.payment.fragment.PackageDetailFragment;
import gk.gkcurrentaffairs.payment.fragment.PackageMockFragment;
import gk.gkcurrentaffairs.payment.model.Package;
import gk.gkcurrentaffairs.payment.model.PackageInfo;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Login;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/5/2017.
 */

public class PackageActivity extends AppCompatActivity implements View.OnClickListener , ViewPager.OnPageChangeListener {

//    private ImageView ivProfile ;
    private TextView tvBuyNow , tvGetFreeMock , tvHeadTitle ;
    private Package mPackage ;
    private String imagePath ;
    private ViewPager viewPager ;
    private TabLayout tabLayout ;
    private ProgressDialog pDialog ;
    private PackageInfo packageInfo ;
    private PackageMockFragment packageMockFragment ;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package );
        initDataFromArg();
        setUpToolbar();
        toolbarTextAppernce();
        initView();
        setData();
        initDialog();
        fetchData();
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void setUpToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar_transparent);
        tvHeadTitle = ( TextView ) findViewById( R.id.tv_title_head );
    }

    private void initView(){
        tvBuyNow = ( TextView ) findViewById( R.id.tv_buy_now );
        tvGetFreeMock = ( TextView ) findViewById( R.id.tv_get_free_mock );
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tvBuyNow.setOnClickListener(this);
        tvGetFreeMock.setOnClickListener(this);
    }

    private int packageId ;
    private String title , image ;
    private void initDataFromArg(){
        mPackage = ( Package ) getIntent().getSerializableExtra(AppConstant.DATA);
        imagePath =  getIntent().getStringExtra(AppConstant.IMAGE);
        packageId =  getIntent().getIntExtra(AppConstant.CAT_ID , 0);
    }

    private void setData(){
        if ( mPackage != null ) {
            title = mPackage.getTitle() ;
            image = mPackage.getImage() ;
            packageId = mPackage.getId() ;
            setDataInView();
        }
    }

    private void setDataInView(){
        collapsingToolbarLayout.setTitle(title );
        tvHeadTitle.setText(title);
        tvHeadTitle.setBackground(getGradDrawable());
//        SupportUtil.loadImage( ivProfile , image , imagePath );
    }
    private Drawable getGradDrawable(){
        int position = getRandomNumberInRange(0,7);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[] {
                        Color.parseColor(colorPrimary[position])
                        ,Color.parseColor(colorSecondary[position])
                });
        gd.setCornerRadius(0f);
        return gd ;
    }
    private int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    private String[] colorPrimary = {
            "#8E2DE2" , "#396afc" , "#C33764" , "#43C6AC" , "#71B280" , "#c94b4b" , "#C33764" , "#44A08D"
    };
    private String[] colorSecondary = {
            "#4A00E0" , "#2948ff" , "#1D2671" , "#191654" , "#134E5E" , "#4b134f" , "#1D2671" , "#093637" ,
    };

    private void fetchData(){
        showDialog();
        Map<String, String> map = new HashMap<>(1);
        map.put("id" , packageId+"");
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.GET_PACKAGE_DETAILS, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                PackageInfo packageInfo1 = ConfigManager.getGson().fromJson(data , PackageInfo.class);
                                if ( packageInfo1 != null ){
                                    packageInfo = packageInfo1;
                                    title = packageInfo.getTitle();
                                    image = packageInfo.getImage();
                                    setDataInView();
                                    showData();
                                }else
                                    showNoData();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        }else
                            showNoData();
                    }
                });
/*
        apiPayInterface.getPackageDetails( mPackage.getId() ).enqueue(new Callback<PackageInfo>() {
            @Override
            public void onResponse(Call<PackageInfo> call, Response<PackageInfo> response) {
                hideDialog();
                if ( response != null && response.body() != null ){
                    packageInfo = response.body() ;
                    showData();
                }else
                    showNoData();
            }

            @Override
            public void onFailure(Call<PackageInfo> call, Throwable t) {
                hideDialog();
                showNoData();
            }
        });
*/
    }

    private void showData(){
        setupViewPager();
    }

    private void showNoData(){
        Toast.makeText( this , "Error, Please try Again" , Toast.LENGTH_SHORT ).show();
        finish();
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager() , 2);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(adapter);
        viewPager.setAdapter(adapter);
    }

    private void initDialog(){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Fetching Data....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
    }

    private void showDialog(){
        try {
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPagerAdapter adapter ){
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.DATA , packageInfo );
        PackageDetailFragment packageDetailFragment = new PackageDetailFragment();
        packageDetailFragment.setArguments(bundle);
        packageMockFragment = new PackageMockFragment();
        packageMockFragment.setArguments(bundle);
        adapter.addFrag( packageMockFragment , "Content" );
        adapter.addFrag( packageDetailFragment , "Details" );
    }

    private void hideDialog(){
        try {
            if ( pDialog != null && pDialog.isShowing() )
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(final View v) {
        Login.handleLogin(this, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                switch ( v.getId() ){
                    case R.id.tv_buy_now:
                        buyPackage();
                        break ;
                    case R.id.tv_get_free_mock :
                        getFreeMock();
                        break ;
                }
                return null;
            }
        });
    }

    private void getFreeMock(){
        packageMockFragment.handleFreeMock();
    }

    private void buyPackage(){
        SupportUtil.openSubscriptionPlan(this);
    }
    private void buyPackage1(){
/*
        Intent paymentMerchentIntent = new Intent(this, ConfirmationActivity.class);
        paymentMerchentIntent.putExtra(AppConstant.DATA, packageInfo);
        paymentMerchentIntent.putExtra(AppConstant.TYPE, packageInfo.getPackageType()+"" );
        startActivity(paymentMerchentIntent);
*/
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
