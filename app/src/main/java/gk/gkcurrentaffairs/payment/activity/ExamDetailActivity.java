package gk.gkcurrentaffairs.payment.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.fragment.PackageListFragment;
import gk.gkcurrentaffairs.payment.fragment.PaidHomeListFragment;
import gk.gkcurrentaffairs.payment.model.Child;
import gk.gkcurrentaffairs.payment.model.Package;
import gk.gkcurrentaffairs.payment.model.SectionData;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/15/2018.
 */

public class ExamDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_cat);
        setupToolbar();
        getDataFromIntent();
    }

    private boolean isTypeExam = true ;
    private void getDataFromIntent(){
        if ( getIntent() != null ) {
            id = getIntent().getIntExtra(AppConstant.DATA , 0 );
            isTypeExam = getIntent().getBooleanExtra( AppConstant.TYPE , true );
            if ( id > 0 ) {
                initDialog();
                showDialog();
                fetchData();
            } else {
                showNoData();
            }
        }
    }


    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showNoData(){
        SupportUtil.showToastCentre( this , "Error, please try later." );
        finish();
    }

    private void loadData(final SectionData sectionData , final Child child){
        if ( sectionData != null && !SupportUtil.isEmptyOrNull(sectionData.getTitle()) )
            getSupportActionBar().setTitle(sectionData.getTitle());
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = isTypeExam ? new PaidHomeListFragment() : new PackageListFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstant.DATA , isTypeExam ? sectionData : child );
                bundle.putSerializable(AppConstant.IMAGE , imagePath );
                fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, fragment).commitAllowingStateLoss();
            }
        });
    }

    private int id ;
    private String imagePath ;
    private void fetchData() {
        Map<String, String> map = new HashMap<>(1);
        map.put("id" , id + "" ) ;
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , isTypeExam ? Constants.GET_EXAM_DATA_BY_ID : Constants.GET_PACKAGES_BY_CAT_ID, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                JSONObject jsonObject = new JSONObject(data) ;
                                imagePath = jsonObject.optString("image_path");
                                if ( !SupportUtil.isEmptyOrNull(jsonObject.optString("data")) ) {
                                    if ( isTypeExam ) {
                                        SectionData sectionData = ConfigManager.getGson().fromJson(jsonObject.optString("data"), SectionData.class);
                                        if (sectionData != null
                                                && sectionData.getChildren() != null
                                                && sectionData.getChildren().size() > 0) {
                                            loadData(sectionData , null);
                                        } else
                                            showNoData();
                                    }else {
                                        List<Package> packageList = ConfigManager.getGson().fromJson(jsonObject.optString("data"), new TypeToken<List<Package>>() {}.getType());
                                        if (packageList != null
                                                && packageList.size() > 0) {
                                            Child child = new Child();
                                            child.setPackages(packageList);
                                            loadData(null , child);
                                        } else
                                            showNoData();
                                    }
                                }else
                                    showNoData();

                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }catch (JSONException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        } else
                            showNoData();
                    }
                });
    }

    private ProgressDialog pDialog ;

    private void initDialog(){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Fetching Data....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    private void showDialog(){
        try {
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideDialog(){
        try {
            if ( pDialog != null && pDialog.isShowing() )
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
