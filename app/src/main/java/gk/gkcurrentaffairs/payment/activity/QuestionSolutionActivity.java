package gk.gkcurrentaffairs.payment.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.AdsAppCompactActivity;
import com.adssdk.AdsSDK;

import androidx.annotation.Nullable;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/22/2017.
 */

public class QuestionSolutionActivity extends AdsAppCompactActivity implements SupportUtil.OnCustomResponse {

    private MenuItem menuLang;
    private WebView wvQue, wvSol;
    private WebView[] webViews;
    private static TextView tvDir;
    private static WebView wvDir;
    private PaidQuestion currentQue;
    private PaidMockTestResult paidMockTestResult;
    private boolean isLangEng;
    private String direction, que, ansOne, ansTwo, ansThree, ansFour, ansFive, solution;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_que_solution);
        initDataFromArgs();
        setupToolBar();
        initView();
        setDataInView();
        SupportUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, AppConstant.ADS_BANNER);
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void initDataFromArgs() {
        currentQue = (PaidQuestion) getIntent().getSerializableExtra(AppConstant.DATA);
        paidMockTestResult = (PaidMockTestResult) getIntent().getSerializableExtra(AppConstant.CLICK_ITEM_ARTICLE);
        if (currentQue == null) {
            SupportUtil.showToastCentre(this, "Error, please try again");
            finish();
        }
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
    }

    private void initView() {
        tvDir = (TextView) findViewById(R.id.tv_direction);
        wvDir = (WebView) findViewById(R.id.wv_direction);
        wvDir.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (v.getId() == R.id.wv_direction) {
                        hideFullDesc();
                    }
                }
                return false;
            }
        });
        wvQue = (WebView) findViewById(R.id.wv_que);
        wvSol = (WebView) findViewById(R.id.wv_sol);
        webViews = new WebView[5];
        webViews[0] = (WebView) findViewById(R.id.wv_ans_one);
        webViews[1] = (WebView) findViewById(R.id.wv_ans_two);
        webViews[2] = (WebView) findViewById(R.id.wv_ans_three);
        webViews[3] = (WebView) findViewById(R.id.wv_ans_four);
        webViews[4] = (WebView) findViewById(R.id.wv_ans_five);
        rlNativeAd = (RelativeLayout) findViewById(R.id.rl_native_ad_1);

        colorGreen = SupportUtil.getColor(R.color.green_mcq_paid, this);
        colorRed = SupportUtil.getColor(R.color.wrong_red, this);
    }

    private void setDataInView() {
        hideFullDesc();
        setLangBasedData();
        setQuestionData();
        setCorrectAns();
        loadNativeAds();
    }

    private void loadNativeAds() {
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null && rlNativeAd != null) {
            SupportUtil.loadNativeAds(rlNativeAd, R.layout.ads_native_unified_card, true);

        }
    }

    RelativeLayout rlNativeAd;

    int colorGreen, colorRed;

    private void setCorrectAns() {
        int ans = paidMockTestResult.getActualAns() - 1;
        if (webViews.length > ans && ans >= 0) {
            webViews[ans].setBackgroundColor(colorGreen);
            if (paidMockTestResult.getStatus() < AppConstant.PAID_QUE_NOT_VISIT && paidMockTestResult.getStatus() != paidMockTestResult.getActualAns()) {
                int sel = paidMockTestResult.getStatus() - 1;
                webViews[sel].setBackgroundColor(colorRed);
            }
        }
    }

    private void setLangBasedData() {
        if (isLangEng)
            setDataEng();
        else
            setDataHindi();
    }

    private void setQuestionData() {
        if (!SupportUtil.isEmptyOrNull(direction)) {
            tvDir.setVisibility(View.VISIBLE);
            tvDir.setText(SupportUtil.fromHtml(AppApplication.getInstance().getDBObject().removePadding(direction)));
            setDataWebView(wvDir, direction + " <span style='color:#303F9F;' >-- View Less </span>", "#e6e8e8");
            makeTextViewResizable(tvDir, 2, "View More", true);
        } else
            tvDir.setVisibility(View.GONE);

        setDataWebView(wvQue, que);
        setDataWebView(wvSol, "<b>Solution : </b>" + solution);
        setDataWebView(webViews[0], ansOne);
        setDataWebView(webViews[1], ansTwo);
        setDataWebView(webViews[2], ansThree);
        setDataWebView(webViews[3], ansFour);
        if (!SupportUtil.isEmptyOrNull(ansFive)) {
            webViews[4].setVisibility(View.VISIBLE);
            setDataWebView(webViews[4], ansFive);
        } else {
            webViews[4].setVisibility(View.GONE);
        }
    }

    private void hideFullDesc() {
        wvDir.setVisibility(View.GONE);
        tvDir.setVisibility(View.VISIBLE);
    }

    private static void showFullDesc() {
        wvDir.setVisibility(View.VISIBLE);
        tvDir.setVisibility(View.GONE);
    }

    private String getQueTitle() {
        if ( currentQue != null ) {
            if (isLangEng)
                return currentQue.getTitleEng();
            else
                return currentQue.getTitleHin();
        }
        return "";
    }


    private void setDataEng() {
        direction = currentQue.getDescriptionEng();
        que = currentQue.getOptionQuestionEng();
        ansOne = currentQue.getOption1Eng();
        ansTwo = currentQue.getOption2Eng();
        ansThree = currentQue.getOption3Eng();
        ansFour = currentQue.getOption4Eng();
        ansFive = currentQue.getOption5Eng();
        solution = currentQue.getAnswerDescriptionEng();
    }

    private void setDataHindi() {
        direction = currentQue.getDescriptionHin();
        que = currentQue.getOptionQuestionHin();
        ansOne = currentQue.getOption1Hin();
        ansTwo = currentQue.getOption2Hin();
        ansThree = currentQue.getOption3Hin();
        ansFour = currentQue.getOption4Hin();
        ansFive = currentQue.getOption5Hin();
        solution = currentQue.getAnswerDescriptionHin();
    }

    private void setupToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getQueTitle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lang, menu);
        menuLang = menu.findItem(R.id.action_lang);
        setMenuTitle();
        return true;
    }

    private void setMenuTitle() {
        menuLang.setTitle(isLangEng ? "En" : "Hi");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_lang) {
            SupportUtil.openLangDialog(this, this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomResponse(boolean result) {
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        setMenuTitle();
        setDataInView();
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                String textString = tv.getText().toString();
                String text = textString;
                if (!SupportUtil.isEmptyOrNull(textString) && tv != null) {
                    if (maxLine == 0) {
                        lineEndIndex = tv.getLayout().getLineEnd(0);
                        if (textString.length() >= lineEndIndex - expandText.length() + 1) {
                            text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        }
                    } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                        lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                        if (textString.length() >= lineEndIndex - expandText.length() + 1) {
                            text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        }
                    } else {
                        lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        if (textString.length() >= lineEndIndex) {
                            text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        }
                    }
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(Color.DKGRAY);
            }

            @Override
            public void onClick(View widget) {
                showFullDesc();
            }
        }, 0, str.indexOf(spanableText) + spanableText.length(), 0);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(true);
                    ds.setColor(Color.parseColor("#303F9F"));
                }

                @Override
                public void onClick(View widget) {
                    showFullDesc();
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    private void openDirectionDialog() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_explanation);
            TextView textView = (TextView) dialog.findViewById(R.id.tv_title_dlg_expl);
            textView.setText("Direction");
            WebView webView = (WebView) dialog.findViewById(R.id.dlg_wv_exp);
            setDataWebView(webView, direction);
            (dialog.findViewById(R.id.dlg_tv_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
        }
    }

    public void setDataWebView(WebView webView, String data, String color) {
//        webView.loadDataWithBaseURL("http://localhost", htmlData(data), "text/html", "UTF-8", null);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data, color), "text/html", "UTF-8", null);
    }

    public void setDataWebView(WebView webView, String data) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:15px; font-family:roboto_regular;"
                + (SupportUtil.isEmptyOrNull(color) ? "" : "background-color: " + color + ";")
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    private String htmlData(String myContent) {
        String s = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:large; font-family:roboto_regular;"
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";
        return s;
    }


}
