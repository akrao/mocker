package gk.gkcurrentaffairs.payment.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.bean.SuccessBean;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.SupportUtil.showToastCentre;

/**
 * Created by Amit on 6/12/2018.
 */

public class PaymentFeedbackActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText bugReport , email ;
    private ProgressDialog progressDialog ;
    private String que ;
    private int id ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_bug_report );
        initView();
        initData();
    }

    String orderId ;
    private void initData(){
        que = getIntent().getStringExtra( AppConstant.TITLE );
        orderId = getIntent().getStringExtra( AppConstant.CAT_ID );
//        id = SupportUtil.isEmptyOrNull( queId ) ? 0 : Integer.parseInt( queId ) ;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");
    }

    private String bugTitle = "Error in <b><font color='#3F51B5'>Transaction?</font></b><br>" +
            "Report <b><font color='#3F51B5'>here?</font></b><br>" +
            "Else <b><font color='#3F51B5'>share</font></b> your feeling here!";

    private void initView(){
        ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml( bugTitle ));
        bugReport = ( EditText ) findViewById( R.id.et_bug_report );
        email = ( EditText ) findViewById( R.id.et_email );
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null &&
                !SupportUtil.isEmptyOrNull(AppApplication.getInstance().getLoginSdk().getEmailId() ) ){
            email.setText( AppApplication.getInstance().getLoginSdk().getEmailId() );
        }
        findViewById( R.id.bt_submit ).setOnClickListener( this );
        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Submitting..." );
    }

    @Override
    public void onClick(View v) {
        String bug = bugReport.getText().toString();
        if (SupportUtil.isEmptyOrNull( bug )){
            Toast.makeText( this , "Please enter the feedback details." , Toast.LENGTH_LONG ).show();
            return;
        }
        String emailId = email.getText().toString();
        if (SupportUtil.isEmptyOrNull( emailId )){
            Toast.makeText( this , "Please enter the email id." , Toast.LENGTH_LONG ).show();
            return;
        }
        handleSubmit( bug , emailId );
    }

    private void handleSubmit( String bug , String emailID ){
        progressDialog.show();
        Map<String, String> map = new HashMap<>(4);
        map.put("message", bug);
        map.put("email_id", emailID);
        map.put("order_id", orderId);
        map.put("app_id", getPackageName());
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_POST, ConfigConstant.HOST_PAID,
                ApiEndPoint.SEND_FEEDBACK
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        progressDialog.dismiss();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                final SuccessBean statusBean = ConfigManager.getGson().fromJson(data, SuccessBean.class);
                                if (statusBean != null && !SupportUtil.isEmptyOrNull( statusBean.getIsinsert() )
                                        && statusBean.getIsinsert().equalsIgnoreCase( "1" )) {
                                    showToastCentre(AppApplication.getInstance(), "Your Feedback is submitted successfully");
                                } else {
                                    showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                                }
                            } catch (JsonSyntaxException e) {
                                showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                                e.printStackTrace();
                            }
                        }else {
                            showToastCentre(AppApplication.getInstance(), "Error, please try later.");
                        }
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            onBackPressed();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

}

