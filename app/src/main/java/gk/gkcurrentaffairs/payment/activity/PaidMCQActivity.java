package gk.gkcurrentaffairs.payment.activity;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.activity.BugReportActivity;
import gk.gkcurrentaffairs.activity.RoughEditorActivity;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.PaidQuestionListAdapter;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidTestCat;
import gk.gkcurrentaffairs.payment.model.SubmitTestResultBean;
import gk.gkcurrentaffairs.payment.model.TempResult;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/8/2017.
 */

public class PaidMCQActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener
        , View.OnTouchListener, PaidQuestionListAdapter.OnCustomClick, Animator.AnimatorListener {

    private PaidMockTest paidMockTest;
    private TextView tvTime, tvQueMarks, tvNegativeMarks, tvQueCount, tvLang;
    private static TextView tvDir;
    //    private WebView wvDir , wvQue ;
    private WebView wvQue;
    private static WebView wvDir;
    private WebView[] webViews;
    private int secPosition = 1, quePosition = 0, minute, seconds, currentTestSeconds = 0, sectionCount, packageId;
    private PaidTestCat currentSec;
    private String direction, que, ansOne, ansTwo, ansThree, ansFour, ansFive;
    private PaidQuestion currentQue;
    static boolean active = false;
    private HashMap<Integer, List<PaidMockTestResult>> resultMap;
    private HashMap<Integer, Integer> sectionLastPosition;
    private ProgressDialog pDialog;
    private Spinner spSec, spDlgSec;
    private String[] sectionsName;
    private RecyclerView recyclerView;
    private View vDlg, vOptionHolder;
    private PaidQuestionListAdapter paidQuestionListAdapter;
    private boolean isLangEng, isOptionLayoutVisible = false;
    private double currentQueInitTime;
    private ImageView ivOption, ivReview;
    private String packageTitle;
    private PaidResult result;
    private TempResult previousResult;
    private DbHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_mcq);
        try {
            active = true ;
            initDataFromArgs();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private boolean isTimerPause = false ;
    @Override
    protected void onStart() {
        super.onStart();
        active = true;
        try {
            if (isTimerPause) {
                startCountDown();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startCountDown() throws Exception {
        if (seconds > 0) {
            long totalMillis = seconds * 1000;
            new CountDownTimer(totalMillis, 1000) {
                public void onTick(long millis) {
                    currentTestSeconds++;
                    seconds--;
                    if (active) {
                        isTimerPause = false ;
                        if ( tvTime != null ) {
                            tvTime.setText(String.format(Locale.ENGLISH,"%2d : %2d",
                                    TimeUnit.MILLISECONDS.toMinutes(millis),
                                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
                        }
                    } else {
                        isTimerPause = true ;
                        cancel();
                    }
                }

                public void onFinish() {
                    if (active) {
                        SupportUtil.showToastCentre(PaidMCQActivity.this, "Submitting your test.");
                        proceedForResult(AppConstant.INT_TRUE);
                    }
                }
            }.start();
        } else {
            proceedForResult(AppConstant.INT_TRUE);
        }
    }

    private void initResultMap() throws Exception {
        resultMap = new HashMap<>(sectionCount);
        sectionLastPosition = new HashMap<>(sectionCount);
        sectionsName = new String[sectionCount];
        if (previousResult == null) {
            initResultMapList(paidMockTest.getCat1(), 0);
            initResultMapList(paidMockTest.getCat2(), 1);
            initResultMapList(paidMockTest.getCat3(), 2);
            initResultMapList(paidMockTest.getCat4(), 3);
            initResultMapList(paidMockTest.getCat5(), 4);
        } else {
            setDataWithPreviousResult(paidMockTest.getCat1(), 0);
            setDataWithPreviousResult(paidMockTest.getCat2(), 1);
            setDataWithPreviousResult(paidMockTest.getCat3(), 2);
            setDataWithPreviousResult(paidMockTest.getCat4(), 3);
            setDataWithPreviousResult(paidMockTest.getCat5(), 4);
            sectionLastPosition.put(getCurrentSection().getId(), quePosition);
        }
    }

    private void setDataWithPreviousResult(PaidTestCat paidTestCat, int position) {
        if (paidTestCat != null) {
            if (paidTestCat.getNoOfQuestions() == 0 && paidTestCat.getPaidQuestions() != null)
                paidTestCat.setNoOfQuestions(paidTestCat.getPaidQuestions().size());
            if (paidTestCat.getNoOfQuestions() > 0) {
                sectionsName[position] = paidTestCat.getTitle();
                if (previousResult.getPaidMockTestResults() != null && previousResult.getPaidMockTestResults().size() > position) {
                    resultMap.put(paidTestCat.getId(), previousResult.getPaidMockTestResults().get(position));
                } else {
                    List<PaidMockTestResult> list = new ArrayList<>(paidTestCat.getNoOfQuestions());
                    for (PaidQuestion question : paidTestCat.getPaidQuestions()) {
                        list.add(new PaidMockTestResult(question.getId(), question.getOptionAnswerEng()));
                    }
                    resultMap.put(paidTestCat.getId(), list);
                }
                sectionLastPosition.put(paidTestCat.getId(), 0);
            }
        }
    }

    private void initResultMapList(PaidTestCat paidTestCat, int position) {
        if (paidTestCat != null) {
            if (paidTestCat.getNoOfQuestions() == 0 && paidTestCat.getPaidQuestions() != null)
                paidTestCat.setNoOfQuestions(paidTestCat.getPaidQuestions().size());
            if (paidTestCat.getNoOfQuestions() > 0) {
                sectionsName[position] = paidTestCat.getTitle();
                List<PaidMockTestResult> list = new ArrayList<>(paidTestCat.getNoOfQuestions());
                for (PaidQuestion question : paidTestCat.getPaidQuestions()) {
                    list.add(new PaidMockTestResult(question.getId(), question.getOptionAnswerEng()));
                }
                resultMap.put(paidTestCat.getId(), list);
                sectionLastPosition.put(paidTestCat.getId(), 0);
            }
        }
    }

    private void proceedForResult(int status) {
        setDataForResult(status);
        if (status == AppConstant.INT_TRUE) {
            sendUserResult(status);
        } else {
            saveData(status);
        }
    }

    private void saveData(int status) {
        result.setStatus(status);
        if (status == AppConstant.INT_FALSE) {
            SupportUtil.showToastCentre(this, "Saving Current Data");
        }
        new InsertResult(status).execute();
    }

    private void openResult(boolean isGlobal) {
        Intent intent = new Intent(PaidMCQActivity.this, PaidResultActivity.class);
        intent.putExtra(AppConstant.DATA, result);
        intent.putExtra(AppConstant.TYPE, isGlobal);
        startActivity(intent);
        closeActivity(AppConstant.INT_FALSE);
    }

    private final int IS_GLOBAL = 0;

    private void sendUserResult(final int statusFlag) {
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
        showDialog("Syncing Result.....");
        String s = createJSON();
        Map<String, String> map = new HashMap<>(3);
        map.put("application_id", getPackageName());
        map.put("data", s);
        map.put("is_practice_test", "1");
        map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_POST_FORM, ConfigConstant.HOST_PAID
                , Constants.SAVE_USER_TEST_RESULT_NEW, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                SubmitTestResultBean submitTestResultBean = ConfigManager.getGson().fromJson(data, SubmitTestResultBean.class);
                                if (submitTestResultBean != null) {
                                    boolean isGlobal = submitTestResultBean.getHaveAlreadyAttempted() == IS_GLOBAL;
                                    if (isGlobal) {
                                        deleteGlobalData();
                                        openResult(isGlobal);
                                    } else {
                                        result.setPracticeTest(!isGlobal);
                                        saveData(statusFlag);
                                    }
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                SupportUtil.showToastCentre(PaidMCQActivity.this, "Saving Current Data");
                            }
                        } else {
                            SupportUtil.showToastCentre(PaidMCQActivity.this, "Saving Current Data");
                        }
                    }
                });
        } else {
            SupportUtil.showToastCentre(this , "Please try later.");
            saveData(AppConstant.INT_FALSE);
        }
    }

    private void deleteGlobalData() {
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.deletePaidResult(result.getId());
                return null;
            }
        });
    }

    private String createJSON() {
        Gson gson = new Gson();
        String json = null;
        try {
            PaidResult resultClone = result.getClone();
            resultClone.setTestCats(null);
            json = gson.toJson(resultClone);
            resultClone = null;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return json;
    }

    class InsertResult extends AsyncTask<Void, Void, Void> {

        int status;

        public InsertResult(int status) {
            this.status = status;
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.insertPaidResult(result);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( status == AppConstant.INT_TRUE ){
                openResult(false);
            }
            closeActivity(status);
        }
    }

    private void closeActivity(int flag) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, flag == AppConstant.INT_TRUE ? 1000 : 1);
    }

    private void setDataForResult(int status) {
        if ( result != null ) {
            result.setLastQuePos(quePosition);
            result.setLastSectionPos(secPosition);
            result.setSectionCount(sectionCount);
            result.setMockId(paidMockTest.getId());
            result.setPackageId(packageId);
            result.setPackageTitle(packageTitle);
            result.setTitle(paidMockTest.getTitle());
            result.setEndTimeStamp(System.currentTimeMillis());
            result.setTimeTaken(getTakenTime());
            result.setPaidMockTestResults(new ArrayList<List<PaidMockTestResult>>(sectionCount));
            result.setTestCats(new ArrayList<PaidTestCat>(sectionCount));
            insertListForResult(result, paidMockTest.getCat1());
            insertListForResult(result, paidMockTest.getCat2());
            insertListForResult(result, paidMockTest.getCat3());
            insertListForResult(result, paidMockTest.getCat4());
            insertListForResult(result, paidMockTest.getCat5());
        }
    }

    private long getTakenTime() {
        long pre = previousResult == null ? 0 : previousResult.getTimeTaken();
        long l = pre + (currentTestSeconds * 1000);
//        long l = pre + System.currentTimeMillis() - result.getStartTimeStamp();
        return l;
    }

    private void insertListForResult(PaidResult result, PaidTestCat cat) {
        if (cat != null && resultMap.get(cat.getId()) != null) {
            cat.setPaidQuestions(null);
            result.getTestCats().add(cat);
            result.getPaidMockTestResults().add(resultMap.get(cat.getId()));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    private Activity activity ;
    private void initDataFromArgs() throws Exception {
        activity = this ;
        int mockId = getIntent().getIntExtra(AppConstant.DATA,0);
        previousResult = (TempResult) getIntent().getSerializableExtra(AppConstant.CLICK_ITEM_ARTICLE);
        packageTitle = getIntent().getStringExtra(AppConstant.TITLE);
        packageId = getIntent().getIntExtra(AppConstant.CAT_ID, 0);
        dbHelper = AppApplication.getInstance().getDBObject();
        new DataFromDb().execute(mockId);
    }

    private class DataFromDb extends AsyncTask<Integer,Void,Void>{
        @Override
        protected Void doInBackground(final Integer... voids) {
            if (dbHelper != null && voids != null && voids.length > 0 && voids[0] > 0 ) {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        paidMockTest = dbHelper.fetchPaidMock(voids[0]);
                        return null;
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (paidMockTest == null || paidMockTest.getCat1() == null || paidMockTest.getNoOfQuestions() <= 0) {
                SupportUtil.showToastCentre(activity, "Error, Please Try Again");
                finish();
            }else {
                if ( previousResult == null ) {
                    paidMockTest.setHasUserAttempted(paidMockTest.getId() + "");
                }
                init();
            }
        }
    }

    private void init(){
        try {
            initData();
            setStatusColor();
            initView();
            updateDataInView();
            updateSpinner();
            initDialog();
            handleOptionLayoutAnimation();
            startCountDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() throws Exception {
        colorYellow = Color.parseColor("#b4b5ff");
        result = new PaidResult();
        result.setStartTimeStamp(System.currentTimeMillis());
        minute = paidMockTest.getTestTime();
        seconds = minute * 60;
        if (previousResult == null) {
//            result.setStartTimeStamp(System.currentTimeMillis());
            result.setPracticeTest(!SupportUtil.isEmptyOrNull(paidMockTest.getHasUserAttempted()));
/*
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    boolean b = dbHelper.isPaidGlobalResult(paidMockTest.getId());
                    result.setPracticeTest(!b);
                    return null;
                }
            });
*/
        } else {
//            result.setStartTimeStamp(previousResult.getStartTimeStamp());
            result.setPracticeTest(previousResult.isPractice());
            int min = getPreviousResultTime();
            minute = min > 0 ? minute - min : minute;
            int sec = (int) (previousResult.getTimeTaken() / 1000);
            seconds = (sec > 0) ? seconds - sec : seconds;
            result.setId(previousResult.getId());
            secPosition = previousResult.getLastSectionPos();
            quePosition = previousResult.getLastQuePos();
        }
        initSecCount();
        initResultMap();
    }

    private int getPreviousResultTime() {
        double v = 0;
        if (previousResult != null && previousResult.getPaidMockTestResults() != null
                && previousResult.getPaidMockTestResults().size() > 0) {
            for (List<PaidMockTestResult> list : previousResult.getPaidMockTestResults()) {
                if (list != null && list.size() > 0) {
                    for (PaidMockTestResult mockTestResult : list) {
                        if (mockTestResult != null) {
                            v += mockTestResult.getTimeTaken();
                        }
                    }
                }
            }
        }
        return (int) TimeUnit.MILLISECONDS.toMinutes((long) v);
    }

    private void setStatusColor() throws Exception {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(SupportUtil.getColor(R.color.colorPrimaryDark , this));
        }
    }

    private void initSecCount() throws Exception {
        if (paidMockTest.getCat5() != null)
            sectionCount = 5;
        else if (paidMockTest.getCat4() != null)
            sectionCount = 4;
        else if (paidMockTest.getCat3() != null)
            sectionCount = 3;
        else if (paidMockTest.getCat2() != null)
            sectionCount = 2;
        else if (paidMockTest.getCat1() != null)
            sectionCount = 1;
    }

    private void initView() throws Exception {
        tvTime = (TextView) findViewById(R.id.tv_total_time);
        tvQueCount = (TextView) findViewById(R.id.tv_total_que);
        tvQueMarks = (TextView) findViewById(R.id.tv_total_mark);
        tvNegativeMarks = (TextView) findViewById(R.id.tv_negative_marks);
        tvLang = (TextView) findViewById(R.id.tv_lang);
        tvDir = (TextView) findViewById(R.id.tv_direction);
        tvDir.setTypeface(AppApplication.getInstance().getTypeface());
        ivOption = (ImageView) findViewById(R.id.iv_option);
        ivReview = (ImageView) findViewById(R.id.iv_review);
        wvDir = (WebView) findViewById(R.id.wv_direction);
        wvQue = (WebView) findViewById(R.id.wv_que);
        webViews = new WebView[5];
        webViews[0] = (WebView) findViewById(R.id.wv_ans_one);
        webViews[1] = (WebView) findViewById(R.id.wv_ans_two);
        webViews[2] = (WebView) findViewById(R.id.wv_ans_three);
        webViews[3] = (WebView) findViewById(R.id.wv_ans_four);
        webViews[4] = (WebView) findViewById(R.id.wv_ans_five);
        spSec = (Spinner) findViewById(R.id.sp_sec);
        spDlgSec = (Spinner) findViewById(R.id.sp_dlg_sec);
        recyclerView = (RecyclerView) findViewById(R.id.rv_dlg);
        vDlg = findViewById(R.id.ll_dlg);
        vOptionHolder = findViewById(R.id.ll_option_holder);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        spSec.setOnItemSelectedListener(this);
        spDlgSec.setOnItemSelectedListener(this);

        wvDir.setOnTouchListener(this);
        for (WebView webView : webViews) {
            setWebViewSetting(webView);
            webView.setOnTouchListener(this);
        }

        findViewById(R.id.dlg_fb_close).setOnClickListener(this);
        findViewById(R.id.iv_submit).setOnClickListener(this);
        findViewById(R.id.iv_pause).setOnClickListener(this);
        findViewById(R.id.tv_pre).setOnClickListener(this);
        findViewById(R.id.tv_next).setOnClickListener(this);
        findViewById(R.id.ll_list).setOnClickListener(this);
        findViewById(R.id.ll_option).setOnClickListener(this);
        findViewById(R.id.ll_review).setOnClickListener(this);
        findViewById(R.id.ll_bug).setOnClickListener(this);
        findViewById(R.id.ll_lang).setOnClickListener(this);
        findViewById(R.id.ll_rough).setOnClickListener(this);
        rlNativeAd = (RelativeLayout) findViewById(R.id.rl_native_ad_1);
    }

    RelativeLayout rlNativeAd ;

    private void updateSpinner() throws Exception {
        ArrayAdapter adapterSpSec = new ArrayAdapter(this, R.layout.adapter_spinner_white, sectionsName);
        adapterSpSec.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter adapterSpSecDlg = new ArrayAdapter(this, android.R.layout.simple_spinner_item, sectionsName);
        adapterSpSecDlg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSec.setAdapter(adapterSpSec);
        spDlgSec.setAdapter(adapterSpSecDlg);
        spSec.setSelection(secPosition - 1);
    }

    private void updateDataInView() throws Exception {
        setCurrentSection();
        if (currentSec != null && currentSec.getPaidQuestions() != null && currentSec.getPaidQuestions().size() > 0) {
            setSectionData();
            loadLangData();
            updateList();
        } else {
            SupportUtil.showToastCentre(this, "Error in Data. Please refresh the Test.");
        }
    }

    private void setSectionData() throws Exception {
        tvNegativeMarks.setText("-" + currentSec.getNegetiveMarking());
        tvQueMarks.setText("+" + currentSec.getQuestMarks());
    }

    private void updateList() throws Exception {
        paidQuestionListAdapter = new PaidQuestionListAdapter(resultMap.get(currentSec.getId()), this);
        recyclerView.setAdapter(paidQuestionListAdapter);
    }

    private void loadLangData() throws Exception {
        hideFullDesc();
        ivReview.setImageResource(R.drawable.non_review);
        currentQueInitTime = System.currentTimeMillis();
        clearBackAllAns();
        if (resultMap != null && resultMap.size() > 0 && resultMap.get(currentSec.getId()) != null) {
            if (resultMap.get(currentSec.getId()).get(quePosition).getStatus() == AppConstant.PAID_QUE_NOT_VISIT) {
                resultMap.get(currentSec.getId()).get(quePosition).setStatus(AppConstant.PAID_QUE_VISIT);
                resultMap.get(currentSec.getId()).get(quePosition).setFirstView(System.currentTimeMillis());
            } else if (resultMap.get(currentSec.getId()).get(quePosition).getStatus() < AppConstant.PAID_QUE_NOT_VISIT) {
                boolean isRev = resultMap.get(currentSec.getId()).get(quePosition).isMarkReview();
                webViews[resultMap.get(currentSec.getId()).get(quePosition).getStatus() - 1]
                        .setBackgroundColor(isRev ? Color.YELLOW : colorYellow);
                if (isRev)
                    ivReview.setImageResource(R.drawable.review);
            }
        }
        if (paidQuestionListAdapter != null) {
            paidQuestionListAdapter.notifyDataSetChanged();
        }
        currentQue = currentSec.getPaidQuestions().get(quePosition);
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        setLangBasedData();
        setQuestionData();
        SupportUtil.loadNativeAds(rlNativeAd , R.layout.ads_native_unified_card , true);
    }

    private void setLangBasedData() {
        tvLang.setText(isLangEng ? "En" : "Hi");
        if (isLangEng)
            setDataEng();
        else
            setDataHindi();
    }


    private void handleTotalTakenTimeQue() {
        if (currentSec != null && resultMap.get(currentSec.getId()) != null
                && resultMap.get(currentSec.getId()).size() > quePosition
                && resultMap.get(currentSec.getId()).get(quePosition) != null) {
            double preTime = resultMap.get(currentSec.getId()).get(quePosition).getTimeTaken();
            preTime = (System.currentTimeMillis() - currentQueInitTime) + preTime;
            resultMap.get(currentSec.getId()).get(quePosition).setTimeTaken(preTime);
            resultMap.get(currentSec.getId()).get(quePosition).setLastVisit(System.currentTimeMillis());
        }
    }

    private void setQuestionData() {
        if (!SupportUtil.isEmptyOrNull(direction)) {
            tvDir.setVisibility(View.VISIBLE);
            tvDir.setText(SupportUtil.fromHtml(AppApplication.getInstance().getDBObject().removePadding(direction)));
            setDataWebView(wvDir, direction + " <span style='color:#303F9F;' >-- View Less </span>", "#e6e8e8");
//            makeTextViewResizable(tvDir, 2, "Read More");
            makeTextViewResizable(tvDir, 2, "View More", true);
        } else
            tvDir.setVisibility(View.GONE);
//        setDataWebView( wvDir , direction );
        tvQueCount.setText((quePosition + 1) + "/" + currentSec.getNoOfQuestions());
        setDataWebView(wvQue, que);
        setDataWebView(webViews[0], ansOne);
        setDataWebView(webViews[1], ansTwo);
        setDataWebView(webViews[2], ansThree);
        setDataWebView(webViews[3], ansFour);
        if (!SupportUtil.isEmptyOrNull(ansFive)) {
            webViews[4].setVisibility(View.VISIBLE);
            setDataWebView(webViews[4], ansFive);
        } else {
            webViews[4].setVisibility(View.GONE);
        }
    }

    private void clearBackAllAns() {
        for (WebView view : webViews) {
            view.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void setDataEng() {
        direction = currentQue.getDescriptionEng();
//        que = currentQue.getTitleEng() + currentQue.getOptionQuestionEng();
        que = currentQue.getOptionQuestionEng();
        ansOne = currentQue.getOption1Eng();
        ansTwo = currentQue.getOption2Eng();
        ansThree = currentQue.getOption3Eng();
        ansFour = currentQue.getOption4Eng();
        ansFive = currentQue.getOption5Eng();
    }

    private void setDataHindi() {
        direction = currentQue.getDescriptionHin();
//        que = currentQue.getTitleHin() + currentQue.getOptionQuestionHin();
        que = currentQue.getOptionQuestionHin();
        ansOne = currentQue.getOption1Hin();
        ansTwo = currentQue.getOption2Hin();
        ansThree = currentQue.getOption3Hin();
        ansFour = currentQue.getOption4Hin();
        ansFive = currentQue.getOption5Hin();
    }

    private void setCurrentSection() throws Exception {
        currentSec = getCurrentSection();
        quePosition = sectionLastPosition.get(currentSec.getId());
    }

    private PaidTestCat getCurrentSection() throws Exception {
        PaidTestCat paidTestCat;
        switch (secPosition) {
            case 1:
                paidTestCat = paidMockTest.getCat1();
                break;
            case 2:
                paidTestCat = paidMockTest.getCat2();
                break;
            case 3:
                paidTestCat = paidMockTest.getCat3();
                break;
            case 4:
                paidTestCat = paidMockTest.getCat4();
                break;
            case 5:
                paidTestCat = paidMockTest.getCat5();
                break;
            default:
                paidTestCat = paidMockTest.getCat1();
                break;
        }
        return paidTestCat;
    }

    @Override
    public void onBackPressed() {
        openSubmitDialog1(AppConstant.INT_FALSE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_submit:
                openSubmitDialog(AppConstant.INT_TRUE);
                break;
            case R.id.iv_pause:
                openSubmitDialog1(AppConstant.INT_FALSE);
                break;
            case R.id.dlg_fb_close:
                vDlg.setVisibility(View.GONE);
                break;
            case R.id.ll_list:
                vDlg.setVisibility(vDlg.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
            case R.id.ll_option:
                handleOption();
                break;
            case R.id.ll_rough:
                roughWork();
                break;
            case R.id.ll_bug:
                bugReport();
                break;
            case R.id.ll_review:
                handleReview();
                break;
            case R.id.ll_lang:

                if (!isLangEng) {
                    langChanged(true);
                } else {
                    langChanged(false);
                }

//            openLangDialog();
                break;
            case R.id.tv_pre:
                handleTotalTakenTimeQue();
                if (quePosition > 0) {
                    --quePosition;
                    try {
                        loadLangData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tv_next:
                if (quePosition < currentSec.getNoOfQuestions() - 1) {
                    handleTotalTakenTimeQue();
                    ++quePosition;
                    try {
                        loadLangData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialogNextSection();
                }
                break;
        }
    }

    private void showDialogNextSection() {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(this);
        }
        builder.setTitle("Question End for the current Section.");
        builder.setMessage("Please change the Section from the Top.")
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }

    private void roughWork() {
        startActivity(new Intent(this, RoughEditorActivity.class));
    }

    private void handleReview() {
        if (getCurrentResult().isMarkReview()) {
            ivReview.setImageResource(R.drawable.non_review);
            getCurrentResult().setMarkReview(false);
        } else {
            ivReview.setImageResource(R.drawable.review);
            getCurrentResult().setMarkReview(true);
        }
        if (paidQuestionListAdapter != null) {
            paidQuestionListAdapter.notifyDataSetChanged();
        }
    }

    private PaidMockTestResult getCurrentResult() {
        return resultMap.get(currentSec.getId()).get(quePosition);
    }

    private void bugReport() {
        Intent intent = new Intent(this, BugReportActivity.class);
        intent.putExtra(AppConstant.TITLE, currentQue.getOptionQuestionEng());
        intent.putExtra(AppConstant.CAT_ID, currentQue.getId());
        intent.putExtra(AppConstant.TYPE, AppConstant.SERVICE_MOCK_TEST);
        startActivity(intent);
    }

    private void handleOptionLayoutAnimation() throws Exception {
        float f = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 93, getResources().getDisplayMetrics());
        vOptionHolder.animate()
                .alpha(0.0f)
                .translationY(f);
        vOptionHolder.setVisibility(View.GONE);
    }

    private void handleOption() {
        vOptionHolder.setVisibility(View.VISIBLE);
        if (isOptionLayoutVisible) {
            vOptionHolder.animate()
                    .alpha(0.0f)
                    .translationY(vOptionHolder.getMeasuredHeight())
                    .setListener(this);
            ivOption.setImageResource(R.drawable.plus_white);
        } else {
            vOptionHolder.animate()
                    .alpha(1.0f)
                    .translationY(0)
                    .setListener(this);
            ivOption.setImageResource(R.drawable.cross);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        vOptionHolder.setVisibility(isOptionLayoutVisible ? View.GONE : View.VISIBLE);
        isOptionLayoutVisible = !isOptionLayoutVisible;
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    private int colorYellow;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (v.getId() != R.id.wv_direction) {
//                if (resultMap.get(currentSec.getId()).get(quePosition).getStatus() == AppConstant.PAID_QUE_VISIT) {
                clearBackAllAns();
                v.setBackgroundColor(colorYellow);
                int ans = 1;
                switch (v.getId()) {
                    case R.id.wv_ans_one:
                        ans = 1;
                        break;
                    case R.id.wv_ans_two:
                        ans = 2;
                        break;
                    case R.id.wv_ans_three:
                        ans = 3;
                        break;
                    case R.id.wv_ans_four:
                        ans = 4;
                        break;
                    case R.id.wv_ans_five:
                        ans = 5;
                        break;
                }
                resultMap.get(currentSec.getId()).get(quePosition).setStatus(ans);
                resultMap.get(currentSec.getId()).get(quePosition).setActualAns(isLangEng ? currentQue.getOptionAnswerEng() : currentQue.getOptionAnswerHin());
//                }
            } else {
                hideFullDesc();
            }
        }
        return false;
    }


    private void hideFullDesc() {
        wvDir.setVisibility(View.GONE);
        tvDir.setVisibility(View.VISIBLE);
    }

    private static void showFullDesc() {
        wvDir.setVisibility(View.VISIBLE);
        tvDir.setVisibility(View.GONE);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if ((position + 1) != secPosition) {
            spDlgSec.setSelection(position);
            spSec.setSelection(position);
            sectionLastPosition.put(currentSec.getId(), quePosition);
            handleTotalTakenTimeQue();
            secPosition = position + 1;
            try {
                updateDataInView();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void setDataWebView(WebView webView, String data) {
//        webView.loadDataWithBaseURL("http://localhost", htmlData(data), "text/html", "UTF-8", null);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data), "text/html", "UTF-8", null);
    }

    private void setWebViewSetting(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
    }

    public void setDataWebView(WebView webView, String data, String color) {
//        webView.loadDataWithBaseURL("http://localhost", htmlData(data), "text/html", "UTF-8", null);
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData(data, color), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent) {
        String s = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:large; font-family:roboto_regular;"
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";
        return s;
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: #000; font-size:15px; font-family:roboto_regular;"
                + (SupportUtil.isEmptyOrNull(color) ? "" : "background-color: " + color + ";")
                + " }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }

    private void initDialog() throws Exception {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Computing Data....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    private void showDialog(String msg) {
        if (pDialog != null) {
            pDialog.setMessage(msg);
            showDialog();
        }

    }

    private void showDialog() {
        try {
            if (pDialog != null && !pDialog.isShowing()) {
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideDialog() {
        try {
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCustomClick(int position) {
        vDlg.setVisibility(View.GONE);
        handleTotalTakenTimeQue();
        quePosition = position;
        try {
            loadLangData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                int lineEndIndex;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    tv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                String textString = tv.getText().toString();
                String text = textString ;
                if ( !SupportUtil.isEmptyOrNull(textString) && tv != null ) {
                    if (maxLine == 0) {
                        lineEndIndex = tv.getLayout().getLineEnd(0);
                        if ( textString.length() >= lineEndIndex - expandText.length() + 1 ) {
                            text = textString.subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        }
                    } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                        lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                        if ( textString.length() >= lineEndIndex - expandText.length() + 1 ) {
                            text = textString.subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        }
                    } else {
                        lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        if ( textString.length() >= lineEndIndex ) {
                            text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        }
                    }
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {

                ds.setUnderlineText(false);
                ds.setColor(Color.DKGRAY);

            }

            @Override
            public void onClick(View widget) {
                showFullDesc();
/*
                tv.setLayoutParams(tv.getLayoutParams());
                tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                tv.invalidate();
                if (viewMore) {
                    makeTextViewResizable(tv, -1, "View Less", false);
                } else {
                    makeTextViewResizable(tv, 2, "View More", true);
                }
*/

            }
        }, 0, str.indexOf(spanableText) + spanableText.length(), 0);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint ds) {

                    ds.setUnderlineText(true);
                    ds.setColor(Color.parseColor("#303F9F"));

                }

                @Override
                public void onClick(View widget) {
                    showFullDesc();
                    /*tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 2, "View More", true);
                    }
*/
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    private void openLangDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dlg_select_mode, null);
        TextView textView = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_title);
        textView.setText("Select Language");
        TextView opt1 = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_opt1);
        opt1.setText("English");
        TextView opt2 = (TextView) dialogView.findViewById(R.id.dlg_mode_tv_opt2);
        opt2.setText("Hindi");
        final CheckBox cbDay = (CheckBox) dialogView.findViewById(R.id.dlg_cb_day);
        final CheckBox cbNight = (CheckBox) dialogView.findViewById(R.id.dlg_cb_night);

        (isLangEng ? cbDay : cbNight).setChecked(true);
        dialogView.findViewById(R.id.dlg_rl_day).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLangEng) {
                    langChanged(true);
                    cbDay.setChecked(true);
                    cbNight.setChecked(false);
                }
            }
        });
        dialogView.findViewById(R.id.dlg_rl_night).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLangEng) {
                    langChanged(false);
                    cbDay.setChecked(false);
                    cbNight.setChecked(true);
                }
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void langChanged(boolean isEng) {
        SharedPrefUtil.setBoolean(AppConstant.PAID_QUESTIONS_LANG, isEng);
        isLangEng = isEng;
        setLangBasedData();
        setQuestionData();
    }

    private void openDirectionDialog() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_explanation);
            TextView textView = (TextView) dialog.findViewById(R.id.tv_title_dlg_expl);
            textView.setText("Direction");
            WebView webView = (WebView) dialog.findViewById(R.id.dlg_wv_exp);
            setDataWebView(webView, direction);
            (dialog.findViewById(R.id.dlg_tv_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
        }
    }


    private boolean isCompress = true;

    private void openSubmitDialog(final int flag) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_paid_mcq_submit);
        if (flag == AppConstant.INT_FALSE) {
            ((TextView) dialog.findViewById(R.id.dlg_tv_title)).setText("Are you sure you want to Pause the test?");
            ((Button) dialog.findViewById(R.id.dlg_bt_submit)).setText("PAUSE");
        }
        ((TextView) dialog.findViewById(R.id.dlg_tv_time)).setText(getTimeLeft());
        ((TextView) dialog.findViewById(R.id.dlg_tv_attempt)).setText(getAttempt());
        ((TextView) dialog.findViewById(R.id.dlg_tv_un_attempt)).setText(getUnAttempt());
        ((TextView) dialog.findViewById(R.id.dlg_tv_review)).setText(getReviewCount() + "");
        dialog.findViewById(R.id.dlg_bt_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceedForResult(flag);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.dlg_bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String getUnAttempt() {
        return "" + (paidMockTest.getNoOfQuestions() - attemptCount);
    }

    private String getAttempt() {
        return "" + getAttemptedQueCount();
    }

    int attemptCount = 0;

    private int getAttemptedQueCount() {
        attemptCount = 0;
        if (paidMockTest != null) {
            countAttemptCat(paidMockTest.getCat1());
            countAttemptCat(paidMockTest.getCat2());
            countAttemptCat(paidMockTest.getCat3());
            countAttemptCat(paidMockTest.getCat4());
            countAttemptCat(paidMockTest.getCat5());
        }
        return attemptCount;
    }

    private void countAttemptCat(PaidTestCat cat) {
        if (cat != null && resultMap != null) {
            if (resultMap.get(cat.getId()) != null) {
                for (int i = 0; i < cat.getNoOfQuestions(); i++) {
                    if (resultMap.get(cat.getId()).get(i) != null) {
                        int status = resultMap.get(cat.getId()).get(i).getStatus();
                        Logger.e("status -- " + status);
                        if (status != AppConstant.PAID_QUE_VISIT
                                && status != AppConstant.PAID_QUE_NOT_VISIT
                                && status != AppConstant.PAID_QUE_REVIEW) {
                            attemptCount++;
                        }
                    }
                }
            }
        }
    }

    int reviewCount = 0;

    private int getReviewCount() {
        reviewCount = 0;
        if (paidMockTest != null) {
            countReviewCat(paidMockTest.getCat1());
            countReviewCat(paidMockTest.getCat2());
            countReviewCat(paidMockTest.getCat3());
            countReviewCat(paidMockTest.getCat4());
            countReviewCat(paidMockTest.getCat5());
        }
        return reviewCount;
    }

    private void countReviewCat(PaidTestCat cat) {
        if (cat != null && resultMap != null && resultMap.get(cat.getId()) != null) {
            for (int i = 0; i < cat.getNoOfQuestions(); i++) {
                if (resultMap.get(cat.getId()).get(i) != null && resultMap.get(cat.getId()).get(i).isMarkReview()) {
                    reviewCount++;
                }
            }
        }
    }

    private String getTimeLeft() {
        long totalTime = paidMockTest.getTestTime() * 60 * 1000;
//        long timeTaken = System.currentTimeMillis() - result.getStartTimeStamp();
        long timeTaken = getTakenTime();
        long remainTime = totalTime - timeTaken;
        return AppApplication.getInstance().getDBObject().timeTaken(remainTime);
    }

    private void openSubmitDialog1(final int flag) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new android.app.AlertDialog.Builder(this);
        }
        builder.setTitle("Are you sure?");
        builder.setMessage((flag == AppConstant.INT_TRUE) ? R.string.mcq_submission : R.string.mcq_pause)
                .setPositiveButton((flag == AppConstant.INT_TRUE) ? "Proceed" : "Pause", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        proceedForResult(flag);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);
        builder.create().show();
    }

}
