package gk.gkcurrentaffairs.payment.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.payment.fragment.SectionResultFragment;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.PaidTestCat;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/20/2017.
 */

public class PaidSectionResultActivity extends AppCompatActivity implements SupportUtil.OnCustomResponse{

    private int position , sectionCount;
    private PaidResult paidResult ;
    private MenuItem menuLang;
    private boolean isLangEng ;
    private PaidSectionResult paidSectionResult ;
    ArrayList<PaidQuestion> paidQuestionsData ;
    ArrayList<PaidMockTestResult> paidMockTestResultsData ;
    private ViewPagerAdapter adapter ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_paid_home);
        initDataFromArgs();
        new TestCatFromDB().execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void initDataFromArgs(){
        paidResult = ( PaidResult ) getIntent().getSerializableExtra(AppConstant.DATA);
        paidSectionResult = ( PaidSectionResult ) getIntent().getSerializableExtra(AppConstant.CLICK_ITEM_ARTICLE);
        secId = getIntent().getIntExtra( AppConstant.CAT_ID , 0 );
        position = getIntent().getIntExtra( AppConstant.POSITION , 0 );
        if ( paidResult == null || secId == 0
                || paidResult.getPaidMockTestResults() == null
                || paidResult.getPaidMockTestResults().size() < 1 ) {
            SupportUtil.showToastCentre( this , "Error, please try again" );
            finish();
        }
        dbHelper = AppApplication.getInstance().getDBObject();
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        paidMockTestResultsData = new ArrayList<>(paidResult.getPaidMockTestResults().get(position).size());
        paidMockTestResultsData.addAll(paidResult.getPaidMockTestResults().get(position));
//        paidMockTestResultsData = paidResult.getPaidMockTestResults().get(position) ;
//        paidQuestionsData = paidResult.getTestCats().get(position).getPaidQuestions() ;
        sectionCount = getSectionCount() ;
    }

    private DbHelper dbHelper ;
    private int secId ;

    class TestCatFromDB extends AsyncTask<Void,Void , Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    PaidTestCat paidTestCat = dbHelper.insertQueListForResult(secId,paidResult.getMockId());
                    if ( paidTestCat != null ) {
                        paidQuestionsData = new ArrayList<>(paidTestCat.getPaidQuestions().size());
                        paidQuestionsData.addAll(paidTestCat.getPaidQuestions());
                    }
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ( paidQuestionsData != null && paidQuestionsData.size() > 0 ){
                showData();
            }else
                showNoData();
        }
    }

    private void showData(){
        setupToolBar();
        setDataInList();
    }

    private void showNoData(){
        SupportUtil.showToastCentre(AppApplication.getInstance() , "Error in Section Data");
    }

    private void setDataInList(){
        findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode( sectionCount > 3 ? TabLayout.MODE_SCROLLABLE : TabLayout.MODE_FIXED );
    }

    private void setupViewPager(ViewPager viewPager ){
        adapter = new ViewPagerAdapter(getSupportFragmentManager() , sectionCount);
        insertFrag( adapter , paidQuestionsData , paidMockTestResultsData , "All" );
        int totalCount = paidQuestionsData.size() ;
        if ( paidSectionResult.getCorrect() > 0 ){
            getCorrectData(adapter , "Correct" );
        }
        if ( paidSectionResult.getWrong() > 0 ){
            getWrongData(adapter , "Wrong" );
        }
        if ( paidSectionResult.getUnattended() > 0 ){
            getUnAttemptedData(adapter , "UnAttempted" );
        }
        int visit = ( totalCount - ( paidSectionResult.getCorrect() + paidSectionResult.getUnattended() + paidSectionResult.getWrong() ) ) ;
        if ( visit > 0 ){
            getOnlyVisited(adapter , "Visited" );
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(sectionCount);
    }

    private void getCorrectData( ViewPagerAdapter adapter , String s ){
        int length = paidMockTestResultsData.size() ;
        ArrayList<PaidQuestion> paidQuestions = new ArrayList<>() ;
        ArrayList<PaidMockTestResult> paidMockTestResults = new ArrayList<>() ;
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidMockTestResultsData.size() > i
                    && paidQuestionsData.size() > i
                    && paidMockTestResultsData.get(i).getStatus() == paidMockTestResultsData.get(i).getActualAns() ){
                paidMockTestResults.add(paidMockTestResultsData.get(i));
                paidQuestions.add(paidQuestionsData.get(i));
            }
        }
        insertFrag( adapter , paidQuestions , paidMockTestResults , s );
    }

    private void getWrongData( ViewPagerAdapter adapter , String s ){
        int length = paidMockTestResultsData.size() ;
        ArrayList<PaidQuestion> paidQuestions = new ArrayList<>() ;
        ArrayList<PaidMockTestResult> paidMockTestResults = new ArrayList<>() ;
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidMockTestResultsData.size() > i
                    && paidQuestionsData.size() > i
                    && paidMockTestResultsData.get(i).getStatus() < AppConstant.PAID_QUE_NOT_VISIT
                    && paidMockTestResultsData.get(i).getStatus() != paidMockTestResultsData.get(i).getActualAns() ){
                paidMockTestResults.add(paidMockTestResultsData.get(i));
                paidQuestions.add(paidQuestionsData.get(i));
            }
        }
        insertFrag( adapter , paidQuestions , paidMockTestResults , s );
    }

    private void getUnAttemptedData( ViewPagerAdapter adapter , String s ){
        int length = paidMockTestResultsData.size() ;
        ArrayList<PaidQuestion> paidQuestions = new ArrayList<>() ;
        ArrayList<PaidMockTestResult> paidMockTestResults = new ArrayList<>() ;
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidMockTestResultsData.size() > i
                    && paidQuestionsData.size() > i
                    && paidMockTestResultsData.get(i).getStatus() >= AppConstant.PAID_QUE_NOT_VISIT ){
                paidMockTestResults.add(paidMockTestResultsData.get(i));
                paidQuestions.add(paidQuestionsData.get(i));
            }
        }
        insertFrag( adapter , paidQuestions , paidMockTestResults , s );
    }

    private void getOnlyVisited( ViewPagerAdapter adapter , String s ){
        int length = paidMockTestResultsData.size() ;
        ArrayList<PaidQuestion> paidQuestions = new ArrayList<>() ;
        ArrayList<PaidMockTestResult> paidMockTestResults = new ArrayList<>() ;
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidMockTestResultsData.get(i).getStatus() > AppConstant.PAID_QUE_NOT_VISIT ){
                paidMockTestResults.add(paidMockTestResultsData.get(i));
                paidQuestions.add(paidQuestionsData.get(i));
            }
        }
        insertFrag( adapter , paidQuestions , paidMockTestResults , s );
    }


    private void insertFrag( ViewPagerAdapter adapter , ArrayList<PaidQuestion> paidQuestions
            , ArrayList<PaidMockTestResult> paidMockTestResults , String s ){
        SectionResultFragment fragment = new SectionResultFragment();
        Bundle bundle = new Bundle();
        bundle.putString( AppConstant.TITLE , s );
        bundle.putSerializable(AppConstant.DATA , paidQuestions );
//        bundle.putString(AppConstant.DATA , new Gson().toJson(paidQuestions ) );
        bundle.putSerializable(AppConstant.CATEGORY , paidMockTestResults );
//        bundle.putString(AppConstant.CATEGORY , new Gson().toJson(paidMockTestResults) );
        fragment.setArguments(bundle);
        adapter.addFrag( fragment , s + "(" + paidQuestions.size() + ")" );
    }

    private int getSectionCount(){
        int count = 1 ;
        if ( paidSectionResult.getCorrect() > 0 )
            ++count ;
        if ( paidSectionResult.getWrong() > 0 )
            ++count ;
        if ( paidSectionResult.getUnattended() > 0 )
            ++count ;
        return count + 1 ;
    }

    private void setupToolBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(paidResult.getTestCats().get(position).getTitle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.menu_lang , menu);
        menuLang = menu.findItem(R.id.action_lang);
        setMenuTitle();
        return true;
    }

    private void setMenuTitle(){
        menuLang.setTitle( isLangEng ? "En" : "Hi" );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }else if ( id == R.id.action_lang ){
            SupportUtil.openLangDialog( this , this );
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomResponse(boolean result) {
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        setMenuTitle();
        for ( int i = 0 ; i < sectionCount ; i++ ) {
            if ( adapter.getCount() > i ) {
                Fragment fragment = adapter.getItem(i);
                if (fragment != null && fragment instanceof SectionResultFragment) {
                    try {
                        ((SectionResultFragment) fragment).refreshData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
//        if ( correctFrag != null )
//            correctFrag.refreshData();
//        if ( wrongFrag != null )
//            wrongFrag.refreshData();
//        if ( unAttemptedFrag != null )
//            unAttemptedFrag.refreshData();
    }
}
