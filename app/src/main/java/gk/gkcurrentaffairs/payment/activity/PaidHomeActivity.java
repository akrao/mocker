package gk.gkcurrentaffairs.payment.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.login.LoginSdk;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.fragment.GkHomeFragment;
import gk.gkcurrentaffairs.fragment.PaidUserProfileFragment;
import gk.gkcurrentaffairs.payment.fragment.BaseFragment;
import gk.gkcurrentaffairs.payment.fragment.MyActivityFragment;
import gk.gkcurrentaffairs.payment.fragment.PaidHomeFragment;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Login;
import gk.gkcurrentaffairs.util.SupportUtil;

public class PaidHomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private static PaidHomeActivity paidHomeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paidHomeActivity = this;
        active = true;
        setContentView(R.layout.activity_paid_home);
        setupToolbar();

        initView();
        setupViewPager();

        registerReceiverCallBacks();
        initConfig();
        initMediationTest();
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void initMediationTest(){
//        MediationTestSuite.launch(this, "ca-app-pub-6913072320040860~4730202561");
    }


    private void showConnectingTextView() {
        tvConnection.setText("Connecting...");
        tvConnection.setVisibility(View.VISIBLE);
        tvConnection.setBackgroundColor(SupportUtil.getColor(R.color.graph_yellow, this));
    }

    private void showNotConnectTextView() {
        tvConnection.setText("Click here to Connect");
        tvConnection.setVisibility(View.VISIBLE);
        tvConnection.setBackgroundColor(SupportUtil.getColor(R.color.wrong_red, this));
    }

    private void showSuccessConnectionTextView() {
        tvConnection.setVisibility(View.GONE);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showSuccessConnectionTextView();
            if (AppApplication.getInstance() != null) {
                AppApplication.getInstance().syncData();
                initSync();
            }
        }
    };

    @Override
    public void finish() {
        super.finish();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            unregisterReceiver(broadcastReceiverConnectionFailed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initSync() {
    }

    private TextView tvConnection;
    BroadcastReceiver broadcastReceiverConnectionFailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showNotConnectTextView();
        }
    };

    private void registerReceiverCallBacks() {
        registerReceiver(receiver, new IntentFilter(getPackageName() + AppConstant.APP_UPDATE));
        registerReceiver(broadcastReceiver, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_LOADED));
        registerReceiver(broadcastReceiverConnectionFailed, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_FAILURE));
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            SupportUtil.openUpdateDialog(intent.getStringExtra(AppConstant.TITLE), intent.getBooleanExtra(AppConstant.TYPE, false), PaidHomeActivity.this);
        }
    };

    private void initConfig() {
        if (SupportUtil.isConnected(this)) {
            if (AppApplication.getInstance() != null &&
                    (AppApplication.getInstance().getConfigManager() == null
                            || !AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
                callConfig();
            } else if (AppApplication.getInstance() != null &&
                    (AppApplication.getInstance().getConfigManager() != null
                            || AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
                showSuccessConnectionTextView();
//                initDataFromArgs();
            }
        } else
            showNotConnectTextView();
    }

    private void callConfig() {
        if (SupportUtil.isConnected(this) && AppApplication.getInstance() != null ) {
            if (AppApplication.getInstance().getConfigManager() == null
                            || !AppApplication.getInstance().getConfigManager().isConfigLoaded() ) {
                showConnectingTextView();
                AppApplication.getInstance().initOperations();
            } else if (AppApplication.getInstance().getConfigManager() != null
                    && AppApplication.getInstance().getConfigManager().isConfigLoaded())
                showSuccessConnectionTextView();
        } else
            showNotConnectTextView();
    }

    public static PaidHomeActivity getInstance() {
        return paidHomeActivity;
    }

    public boolean isSubscribed() {
        return remainSec > 0;
    }

    private void initView() {
        tvConnection = (TextView) findViewById(R.id.tv_connection);
        tvConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SupportUtil.isConnected(PaidHomeActivity.this))
                    callConfig();
                else
                    SupportUtil.showToastCentre(PaidHomeActivity.this, "Please check your internet connection.");

            }
        });
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
        actionBar = getSupportActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_paid_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_exam) {
            if ( SupportUtil.isConnected(this)  ) {
                Login.handleLogin(PaidHomeActivity.this, new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        SupportUtil.openExamPage(PaidHomeActivity.this);
                        return null;
                    }
                });
            } else {
                SupportUtil.showToastInternet(this);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void handleLogin(View view) {
        LoginSdk.getInstance(this, getPackageName()).openLoginPageCallBack(this, false, LoginSdk.INTENT_LOGIN, false);
    }

    private String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LoginSdk.INTENT_LOGIN && resultCode == RESULT_OK) {
            SupportUtil.openExamPage(this);
        } else if (requestCode == AppConstant.INTENT_EXAM_UPDATE && resultCode == RESULT_OK) {
            for (int i = 0; i < adapter.getCount(); i++) {
                Fragment fragment = adapter.getItem(i);
                if (fragment != null && fragment instanceof PaidHomeFragment) {
                    ((PaidHomeFragment) fragment).fetchData();
                }
            }
        }
    }


    private ViewPagerAdapter adapter;
    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        PaidHomeFragment home = new PaidHomeFragment();

        GkHomeFragment gkHomeFragment = new GkHomeFragment();
        MyActivityFragment myActivityFragment = new MyActivityFragment();
        PaidUserProfileFragment profile = new PaidUserProfileFragment();

        adapter.addFrag(gkHomeFragment, titles[0]);
        adapter.addFrag(home, titles[1]);
        adapter.addFrag(myActivityFragment, titles[3]);
        adapter.addFrag(profile, titles[4]);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int p = SupportUtil.isConnected(PaidHomeActivity.this) ? 1 : 0;
                viewPager.setCurrentItem(p);
                if (p == 0) {
                    refreshFragment(p);
                }
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            getSupportActionBar().setTitle(item.getTitle());
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_test:
                    viewPager.setCurrentItem(1);
                    return true;
             case R.id.navigation_activity:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_profile:
                    viewPager.setCurrentItem(3);
                    return true;
            }
            return false;
        }

    };

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private ActionBar actionBar;

    @Override
    public void onPageSelected(int position) {
        if (remainSec <= 0) {
            actionBar.setTitle(titles[position]);
        }
        bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(position).getItemId());
        refreshFragment(position);
    }

    private void refreshFragment(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof BaseFragment) {
            ((BaseFragment) fragment).onRefreshFragment();
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>(4);
        private final List<String> mFragmentTitleList = new ArrayList<>(4);

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private String[] titles = {"Home", "Test", "Activity", "My Exam", "Profile"};
    private long remainSec = 0;

    public void setRemainSec(long remainSec) {
        this.remainSec = remainSec;
//        startCountDown();
    }

    private static boolean active = false;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        active = false;
    }

}
