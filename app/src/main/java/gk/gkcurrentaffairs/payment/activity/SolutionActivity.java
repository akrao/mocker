package gk.gkcurrentaffairs.payment.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.PaidTestCat;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/17/2018.
 */

public class SolutionActivity extends AppCompatActivity implements CategoryListAdapter.OnCustomClick{

    private ArrayList<CategoryBean> sectionListTitles;
    private ArrayList<PaidSectionResult> sectionResults;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_list_no_data);
        initData();
        setupToolbar();
    }

    PaidResult paidResult ;
    private void initData(){
        paidResult = (PaidResult) getIntent().getSerializableExtra(AppConstant.DATA);
        if ( paidResult != null && paidResult.getSectionCount() > 0 ) {
            sectionResults = new ArrayList<>(paidResult.getSectionCount());
            int l = paidResult.getSectionCount();
            CategoryBean  categoryBean ;
            sectionListTitles = new ArrayList<>();
            for (int i = 0; i < l; i++) {
                if (paidResult.getTestCats() != null && paidResult.getTestCats().get(i) != null
                        && paidResult.getPaidMockTestResults().get(i) != null
                        && paidResult.getPaidMockTestResults() != null
                        && paidResult.getPaidMockTestResults().get(i).size() > 0) {
                    PaidTestCat paidTestCat = paidResult.getTestCats().get(i);
                    List<PaidMockTestResult> list = paidResult.getPaidMockTestResults().get(i);
                    PaidSectionResult result = parseSectionData(paidTestCat, list);
                    sectionResults.add(result);
                    categoryBean = new CategoryBean();
                    categoryBean.setCategoryName(paidTestCat.getTitle());
                    sectionListTitles.add(categoryBean);
                }
            }
            if ( sectionListTitles.size() > 0 )
                initListView();
        }
    }


    private void initListView() {
        findViewById(R.id.ll_no_data).setVisibility(View.GONE);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        CategoryListAdapter adapter = new CategoryListAdapter(sectionListTitles, this, R.layout.item_list_image_text);
        adapter.setTextual(true);
        recyclerView.setAdapter(adapter);
    }

    private void setupToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Solution");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCustomItemClick(int position) {
        openDialog(position);
    }

    String title ;
    private void openSolution(){
        Intent intent = new Intent(this, SectionResultActivity.class);
//        Intent intent = new Intent(this, PaidSectionResultActivity.class);
        intent.putExtra(AppConstant.TITLE, title);
        intent.putExtra(AppConstant.DATA, paidQuestions);
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, paidMockTestResults);
/*
        intent.putExtra(AppConstant.DATA, paidResult);
        intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, sectionResults.get(position));
        intent.putExtra(AppConstant.POSITION, (position ));
*/
        startActivity(intent);

    }

    private void openDialog(final int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(sectionListTitles.get(position).getCategoryName())
                .setItems(titles, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        title = titles[i];
                        openSolution(position , i);
                    }
                })
        .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    private ArrayList<PaidQuestion> paidQuestions = new ArrayList<>();
    private ArrayList<PaidMockTestResult> paidMockTestResults = new ArrayList<>();
    private void openSolution(int parentId , int position){
        paidMockTestResults.clear();
        paidQuestions.clear();
        PaidSectionResult paidSectionResult = sectionResults.get(parentId) ;
        length = paidResult.getPaidMockTestResults().get(parentId).size() ;
        switch ( position ){
            case 0 :
                paidQuestions.addAll(paidResult.getTestCats().get(parentId).getPaidQuestions());
                paidMockTestResults.addAll(paidResult.getPaidMockTestResults().get(parentId));
                break;
            case 1 :
                if ( paidSectionResult.getCorrect() > 0 ){
                    getCorrectData(parentId);
                }
                break;
            case 2 :
                if ( paidSectionResult.getWrong() > 0 ){
                    getWrongData(parentId);
                }
                break;
            case 3 :
                if ( paidSectionResult.getUnattended() > 0 ){
                    getUnAttemptedData(parentId );
                }
                break;
        }
        if ( paidQuestions.size() > 0 ){
            openSolution();
        }else {
            SupportUtil.showToastCentre(this , "There is no data in " + titles[position]);
        }
    }

    private int length ;
    private void getCorrectData(int position){
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidResult.getPaidMockTestResults().get(position).get(i).getStatus() == paidResult.getPaidMockTestResults().get(position).get(i).getActualAns() ){
                paidMockTestResults.add(paidResult.getPaidMockTestResults().get(position).get(i));
                paidQuestions.add(paidResult.getTestCats().get(position).getPaidQuestions().get(i));
            }
        }
    }

    private void getWrongData( int position){
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidResult.getPaidMockTestResults().get(position).get(i).getStatus() < AppConstant.PAID_QUE_NOT_VISIT
                    && paidResult.getPaidMockTestResults().get(position).get(i).getStatus()
                    != paidResult.getPaidMockTestResults().get(position).get(i).getActualAns() ){
                paidMockTestResults.add(paidResult.getPaidMockTestResults().get(position).get(i));
                paidQuestions.add(paidResult.getTestCats().get(position).getPaidQuestions().get(i));
            }
        }
    }

    private void getUnAttemptedData( int position ){
        for ( int i = 0 ; i < length ; i++ ){
            if ( paidResult.getPaidMockTestResults().get(position).get(i).getStatus() == AppConstant.PAID_QUE_NOT_VISIT ){
                paidMockTestResults.add(paidResult.getPaidMockTestResults().get(position).get(i));
                paidQuestions.add(paidResult.getTestCats().get(position).getPaidQuestions().get(i));
            }
        }
    }


    private String[] titles = { "All" , "Correct" , "Wrong" , "UnAttempted"};

    private PaidSectionResult parseSectionData(PaidTestCat paidTestCat, List<PaidMockTestResult> list) {
        PaidSectionResult sectionResult = new PaidSectionResult();
        sectionResult.setTitle(paidTestCat.getTitle());
        double time = 0, secCorrectTime = 0, secWrongTime = 0;
        int correct = 0, wrong = 0, unAttended = 0;
        for (PaidMockTestResult mockTestResult : list) {
            time += mockTestResult.getTimeTaken();
            if (mockTestResult.getStatus() < AppConstant.PAID_QUE_NOT_VISIT) {
                if (mockTestResult.getStatus() == mockTestResult.getActualAns()) {
                    correct++;
                    secCorrectTime += mockTestResult.getTimeTaken();
                } else {
                    wrong++;
                    secWrongTime += mockTestResult.getTimeTaken();
                }
            } else {
                unAttended++;
            }
        }
        sectionResult.setCorrect(correct);
        sectionResult.setWrong(wrong);
        sectionResult.setUnattended(unAttended);
        return sectionResult;
    }

}
