package gk.gkcurrentaffairs.payment.activity;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.payment.fragment.BaseFragment;
import gk.gkcurrentaffairs.payment.fragment.GlobalResultFragment;
import gk.gkcurrentaffairs.payment.fragment.PaidLeaderBoardFragment;
import gk.gkcurrentaffairs.payment.fragment.PaidResultCompareFragment;
import gk.gkcurrentaffairs.payment.fragment.PaidResultFragment;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/15/2017.
 */

public class PaidResultActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private Bundle bundle ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataFromArgs();
        setContentView(isGlobal ? R.layout.activity_category : R.layout.activity_sub_cat);
        setupToolbar();
        if ( isGlobal ){
            getSupportActionBar().setTitle("Global Result");
            setupViewPager();
            setupCollapsingToolbar();
        }else {
            getSupportActionBar().setTitle("Practice Result");
            loadPractice();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void loadPractice(){
        Fragment fragment = new PaidResultFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, fragment).commit();
    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager() , 3);
        GlobalResultFragment resultFragment = new GlobalResultFragment();
        resultFragment.setArguments(bundle);
        adapter.addFrag(resultFragment , "Result");
        PaidResultCompareFragment compareFragment = new PaidResultCompareFragment();
        compareFragment.setArguments(bundle);
        adapter.addFrag(compareFragment , "Compare");
        PaidLeaderBoardFragment leaderBoardFragment = new PaidLeaderBoardFragment();
        leaderBoardFragment.setArguments(bundle);
        adapter.addFrag(leaderBoardFragment , "Leaderboard");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Practice Result");
    }

    PaidResult paidResult ;
    boolean isGlobal ;
    private void initDataFromArgs() {
        paidResult = (PaidResult) getIntent().getSerializableExtra(AppConstant.DATA);
        isGlobal = getIntent().getBooleanExtra( AppConstant.TYPE , true );
        if (paidResult == null) {
            SupportUtil.showToastCentre(this, "Error, Please try again");
            finish();
        }
        bundle = new Bundle();
        bundle.putBoolean( AppConstant.TYPE , isGlobal );
        bundle.putSerializable( AppConstant.DATA , paidResult );
    }

    private void setupCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof BaseFragment) {
            ((BaseFragment) fragment).onRefreshFragment();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
