package gk.gkcurrentaffairs.payment.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.TransactionHistoryAdapter;
import gk.gkcurrentaffairs.payment.model.TransactionHistoryBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/1/2017.
 */

public class TransactionHistoryActivity extends AppCompatActivity implements TransactionHistoryAdapter.OnCustomClick ,TransactionHistoryAdapter.OnLoadMore
        , SwipeRefreshLayout.OnRefreshListener{

    private View tvNoData , pbLoading , vNoData , viewLoadMore ;
    private List<TransactionHistoryBean> lists = new ArrayList<>();
    private boolean isNetWorkCall , canLoadMore = true ;
    private TransactionHistoryAdapter adapter ;
    private SwipeRefreshLayout swipeRefreshLayout ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_article_list);
        initViews();
        fetchData( true );
    }

    private void initViews(){
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        viewLoadMore = findViewById(R.id.ll_load_more);
        vNoData = findViewById( R.id.ll_no_data );
        tvNoData = findViewById( R.id.tv_no_data );
        pbLoading = findViewById( R.id.player_progressbar );
//        apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
        getSupportActionBar().setTitle( "Order History" );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showData(){
        swipeRefreshLayout.setRefreshing(false);
        canLoadMore = true ;
        isNetWorkCall = false ;
        if ( adapter == null ) {
            vNoData.setVisibility( View.GONE );
            RecyclerView recyclerView = (RecyclerView) findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager( new LinearLayoutManager( this ) );
            adapter = new TransactionHistoryAdapter( lists , this , this );
            recyclerView.setAdapter( adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void fetchData(final boolean isLatestData ){
        Map<String, String> map = new HashMap<>(3);
        map.put("user_id" , AppApplication.getInstance().getLoginSdk().getUserId());
        map.put("id" , (isLatestData ? 0 : lists.get(lists.size()-1).getId() )+ "" );
        map.put("application_id" , getPackageName());
        isNetWorkCall = true ;
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.GET_USER_ORDER_HISTORY, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        isNetWorkCall = true ;
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                List<TransactionHistoryBean> list = ConfigManager.getGson().fromJson(data , new TypeToken<List<TransactionHistoryBean>>() {
                                }.getType());
                                if ( list != null && list.size() > 0 ){
                                    if ( isLatestData )
                                        lists.clear();
                                    lists.addAll( list );
                                    showData();
                                }else
                                    showNoData();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        }else
                            showNoData();
                    }
                });
/*
        if ( !SupportUtil.isNotConnected( this ) ) {
            isNetWorkCall = true ;
            long id = isLatestData ? AppConstant.MAX_VALUE : lists.get(lists.size()-1).getId() ;
            apiPayInterface.getMyTransactionHistory( SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO) , id )
                    .enqueue(new Callback<List<TransactionHistoryBean>>() {
                        @Override
                        public void onResponse(Call<List<TransactionHistoryBean>> call, Response<List<TransactionHistoryBean>> response) {
                            if ( response != null && response.body() != null
                                    && response.body().size() > 0){
                                if ( isLatestData )
                                    lists.clear();
                                lists.addAll( response.body() );
                                showData();
                            }else
                                showNoData();
                        }

                        @Override
                        public void onFailure(Call<List<TransactionHistoryBean>> call, Throwable t) {
                                showNoData();
                        }
                    });
        } else {
            SupportUtil.showToastInternet(this);
            showNoData();
        }
*/
    }

    private void showNoData(){
        swipeRefreshLayout.setRefreshing(false);
        isNetWorkCall = false ;
        canLoadMore = false ;
        if ( lists.size() < 1 ){
            pbLoading.setVisibility( View.GONE );
            tvNoData.setVisibility( View.VISIBLE );
        }
    }

    @Override
    public void onLoadMore() {
        if (!SupportUtil.isNotConnected(this)) {
            if (!isNetWorkCall && viewLoadMore.getVisibility() == View.GONE && canLoadMore) {
                viewLoadMore.setVisibility(View.VISIBLE);
                fetchData(false);
            }
        } else {
            viewLoadMore.setVisibility(View.GONE);
        }

    }


    @Override
    public void onRefresh() {
        fetchData(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomClick(int position, boolean isFeedback) {
        if ( isFeedback ){
            openFeedback(position);
        }
    }

    private void openFeedback(int position){
        Intent intent = new Intent(this, PaymentFeedbackActivity.class);
        intent.putExtra(AppConstant.TITLE, lists.get(position).getPaymentStatus());
        intent.putExtra(AppConstant.CAT_ID, lists.get(position).getOrderId());
        intent.putExtra(AppConstant.TYPE, AppConstant.SERVICE_SUBSCRIBE);
        startActivity(intent);
    }
}
