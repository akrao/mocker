package gk.gkcurrentaffairs.payment.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/15/2018.
 */

public class OfferDescriptionActivity extends AppCompatActivity implements View.OnClickListener{

    private int id ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_description);
        setData();
    }

    private void setData(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Subscribe");
        findViewById(R.id.tv_buy_now).setOnClickListener(this);
        if ( getIntent() != null ) {
            if( !SupportUtil.isEmptyOrNull( getIntent().getStringExtra(AppConstant.DATA) )){
                ((WebView)findViewById(R.id.webview)).
                        loadData( getIntent().getStringExtra(AppConstant.DATA) ,"text/html", "UTF-8" );
            }
            id = getIntent().getIntExtra( AppConstant.CAT_ID , 0 );
        }
    }


    @Override
    public void onClick(View view) {
        if ( id > 0 ){

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
