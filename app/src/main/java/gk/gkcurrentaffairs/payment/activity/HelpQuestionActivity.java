package gk.gkcurrentaffairs.payment.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.adapter.HelpQuestionAdapter;
import gk.gkcurrentaffairs.payment.model.HelpQuestionBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 11/8/2017.
 */

public class HelpQuestionActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener , HelpQuestionAdapter.OnCustomClick {
    private View tvNoData , pbLoading , vNoData ;
    private List<HelpQuestionBean> lists = new ArrayList<>();
    private HelpQuestionAdapter adapter ;
    private SwipeRefreshLayout swipeRefreshLayout ;
    private String title ;
    private int id ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_article_list);
        initViews();
        initDataFromArgs();
    }

    private void initDataFromArgs(){
        Intent intent = getIntent();
        if ( intent != null ){
            id = intent.getIntExtra( AppConstant.CAT_ID , 0 );
            title = intent.getStringExtra( AppConstant.TITLE );
            getSupportActionBar().setTitle( title );
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            fetchData( true );
        }else
            finish();
    }

    private void initViews(){
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        vNoData = findViewById( R.id.ll_no_data );
        tvNoData = findViewById( R.id.tv_no_data );
        pbLoading = findViewById( R.id.player_progressbar );
//        apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
    }

    private void showData(){
        swipeRefreshLayout.setRefreshing(false);
        if ( adapter == null ) {
            vNoData.setVisibility( View.GONE );
            RecyclerView recyclerView = (RecyclerView) findViewById( R.id.itemsRecyclerView );
            recyclerView.setLayoutManager( new LinearLayoutManager( this ) );
            adapter = new HelpQuestionAdapter( lists , this );
            recyclerView.setAdapter( adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void fetchData(final boolean isLatestData ){
        Map<String, String> map = new HashMap<>(1);
        map.put("id", id+"");
        AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID,
                ApiEndPoint.GET_FAQ_BY_CAT_ID
                , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        Logger.e( "fetchLatestData" );
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                List<HelpQuestionBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<HelpQuestionBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    lists.addAll( list );
                                    showData();
                                } else {
                                    showNoData();
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        }else
                            showNoData();
                    }
                });
/*
        if ( !SupportUtil.isNotConnected( this ) ) {
            apiPayInterface.getHelpSupportQueByCat(id).enqueue(new Callback<List<HelpQuestionBean>>() {
                @Override
                public void onResponse(Call<List<HelpQuestionBean>> call, Response<List<HelpQuestionBean>> response) {
                    if ( response != null && response.body() != null
                            && response.body().size() > 0){
                        if ( isLatestData )
                            lists.clear();
                        lists.addAll( response.body() );
                        showData();
                    }else
                        showNoData();
                }

                @Override
                public void onFailure(Call<List<HelpQuestionBean>> call, Throwable t) {
                    showNoData();
                }
            });
        } else {
            SupportUtil.showToastInternet(this);
            showNoData();
        }
*/
    }

    private void showNoData(){
        swipeRefreshLayout.setRefreshing(false);
        if ( lists.size() < 1 ){
            pbLoading.setVisibility( View.GONE );
            tvNoData.setVisibility( View.VISIBLE );
        }
    }

    @Override
    public void onCustomClick(int position) {
        if ( adapter.isSelection() && adapter.getSelectedPos() == position ){
            adapter.setIsSelection(false);
        }else {
            adapter.setIsSelection(true);
            adapter.setSelectedPos(position);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        fetchData(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
