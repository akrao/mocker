package gk.gkcurrentaffairs.payment.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ApiEndPoint;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.ExamSelectionAdapter;
import gk.gkcurrentaffairs.payment.model.ExamBean;
import gk.gkcurrentaffairs.payment.model.StatusBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.AppPreferences;
import gk.gkcurrentaffairs.util.SupportUtil;

import static gk.gkcurrentaffairs.util.SupportUtil.showToastCentre;

/**
 * Created by Amit on 11/3/2017.
 */

public class ExamSelectionActivity extends AppCompatActivity implements ExamSelectionAdapter.OnCustomClick
        , SwipeRefreshLayout.OnRefreshListener {

    private View tvNoData, pbLoading, vNoData, vDlg;
    private List<ExamBean> lists = new ArrayList<>();
    private ExamSelectionAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isDataEdit = false;
    private String ids;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_select);
        activity = this;
        initViews();
        fetchData(true);
    }

    private void initViews() {
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setOnRefreshListener(this);
        vDlg = findViewById(R.id.rl_dlg);
        vNoData = findViewById(R.id.ll_no_data);
        tvNoData = findViewById(R.id.tv_no_data);
        pbLoading = findViewById(R.id.player_progressbar);
        getSupportActionBar().setTitle("My Exam");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showData() {
        swipeRefreshLayout.setRefreshing(false);
        if (adapter == null) {
            vNoData.setVisibility(View.GONE);
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new ExamSelectionAdapter(lists, this, imageUrl);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private String imageUrl;

    private void fetchData(final boolean isLatestData) {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null) {
            Map<String, String> map = new HashMap<>(4);
            map.put("application_id", getPackageName());
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_USER_PAID_CAT, map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    JSONObject object = new JSONObject(data);
                                    if (object != null && !SupportUtil.isEmptyOrNull(object.optString("data"))) {
                                        imageUrl = object.optString("image_path");
                                        List<ExamBean> list1 = ConfigManager.getGson().fromJson(object.optString("data"), new TypeToken<List<ExamBean>>() {
                                        }.getType());
                                        if (list1 != null && list1.size() > 0) {
                                            lists.clear();
                                            lists.addAll(list1);
                                            showData();
                                            ids = getToStringIds();
                                            if (!SupportUtil.isEmptyOrNull(ids)) {
                                                AppPreferences.setBoolean(AppApplication.getInstance(), AppConstant.IS_FIRST_PAID_EXAM_SELECT, true);
                                            }
                                        } else
                                            showNoData();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    showNoData();
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    showNoData();
                                }
                            } else
                                showNoData();
                        }
                    });
        } else
            showNoData();
    }

    private void showNoData() {
        swipeRefreshLayout.setRefreshing(false);
        if (lists.size() < 1) {
            pbLoading.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCustomClick(int position) {
        isDataEdit = true;
        lists.get(position).setHasSubscribe(SupportUtil.isEmptyOrNull(lists.get(position).getHasSubscribe())
                ? AppConstant.INT_TRUE + "" : null);
        adapter.notifyDataSetChanged();
        ids = getToStringIds();
    }

    @Override
    public void onBackPressed() {
        if (!SupportUtil.isEmptyOrNull(ids)) {
            if (isDataEdit) {
                syncExamData(ids);
            } else {
                setResult(RESULT_OK);
                super.onBackPressed();
            }
        } else {
            openDialog();
        }
    }

    private Activity activity;

    public void syncExamData(String s) {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null) {
            vDlg.setVisibility(View.VISIBLE);
            Map<String, String> map = new HashMap<>(2);
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            map.put("data", s);
            AppApplication.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_POST, ConfigConstant.HOST_PAID,
                    ApiEndPoint.POST_USER_PAID_CATEGORIES
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            AppPreferences.setBoolean(AppApplication.getInstance(), AppConstant.IS_FIRST_PAID_EXAM_SELECT, true);
                            vDlg.setVisibility(View.GONE);
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    final StatusBean statusBean = ConfigManager.getGson().fromJson(data, StatusBean.class);
                                    if (statusBean != null && statusBean.getStatus() == AppConstant.INT_TRUE) {
                                        showToastCentre(AppApplication.getInstance(), "Exam is synched.");
                                    } else {
                                        showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                                    }
                                } catch (JsonSyntaxException e) {
                                    showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                                    e.printStackTrace();
                                }
                            } else {
                                showToastCentre(AppApplication.getInstance(), "Error in synching the Exams.");
                            }
                            activity.setResult(RESULT_OK);
                            activity.finish();
                        }
                    });
        }
    }

    public String getToStringIds() {
        if (lists.size() < 1)
            return "null";

        StringBuilder b = new StringBuilder();
        int length = lists.size();
        for (int i = 0; i < length; i++) {
            if (!SupportUtil.isEmptyOrNull(lists.get(i).getHasSubscribe())) {
                if (!SupportUtil.isEmptyOrNull(b.toString()))
                    b.append(",");
                b.append(lists.get(i).getId());
            }
        }
        return b.toString();
    }

    protected void openDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("My Exam ")
                .setMessage("Please Select 1 Exam minimum.")
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.setIcon(R.drawable.paidmock_test);
        builder.setCancelable(false);
        builder.create().show();
    }


    @Override
    public void onRefresh() {
        fetchData(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.action_submit) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
