package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adssdk.AdsSDK;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.SectionResultAdapter;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidQuestion;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/21/2017.
 */

public class SectionResultFragment extends Fragment {

    private View view ;
    private Activity activity;
    private List<PaidQuestion> paidQuestions , paidQuestionsWithAds ;
    private List<PaidMockTestResult> paidMockTestResults , paidMockTestResultsWithAds ;
    private boolean isLangEng ;
    private SectionResultAdapter sectionResultAdapter ;
    private String title ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        try {
            initDataFromArgs();
            setDataInList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void refreshData()throws Exception{
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        sectionResultAdapter.setLangEng(isLangEng);
        sectionResultAdapter.notifyDataSetChanged();
    }

    private void initDataFromArgs() throws Exception{
        title = getArguments().getString(AppConstant.TITLE);
        paidQuestions = (List<PaidQuestion>)getArguments().getSerializable(AppConstant.DATA);
        paidMockTestResults = (List<PaidMockTestResult>)getArguments().getSerializable(AppConstant.CATEGORY);
        isLangEng = SharedPrefUtil.getBoolean(AppConstant.PAID_QUESTIONS_LANG);
        if ( paidMockTestResults == null || paidQuestions == null ) {
            SupportUtil.showToastCentre( activity , "Error, please try again" );
            activity.finish();
        }
    }

    private void setDataInList()throws Exception{
        view.findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
        RecyclerView recyclerView = (RecyclerView) view.findViewById( R.id.itemsRecyclerView );
        recyclerView.setLayoutManager( new LinearLayoutManager( activity.getApplicationContext() ) );
        setChildWithAds();
        sectionResultAdapter = new SectionResultAdapter( paidQuestionsWithAds , paidMockTestResultsWithAds , isLangEng , activity ) ;
        recyclerView.setAdapter( sectionResultAdapter );
    }

    private void setChildWithAds(){
        int length = paidQuestions.size();
        PaidQuestion baseAdModelClass ;
        PaidMockTestResult baseAdModelClass1 ;
        paidQuestionsWithAds = new ArrayList<>();
        paidMockTestResultsWithAds = new ArrayList<>();
        for ( int i = 0 ; i < length ; i++ ){
            paidQuestionsWithAds.add(paidQuestions.get(i));
            paidMockTestResultsWithAds.add(paidMockTestResults.get(i));
            if ( i == 0 || i == 4 ){
                baseAdModelClass = new PaidQuestion();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                baseAdModelClass1 = new PaidMockTestResult(0,0);
                baseAdModelClass1.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                paidQuestionsWithAds.add(baseAdModelClass);
                paidMockTestResultsWithAds.add(baseAdModelClass1);
            }
        }
    }
}
