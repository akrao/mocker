/*
package gk.gkcurrentaffairs.payment.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.BuildConfig;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.network.ApiEndpointInterface;
import gk.gkcurrentaffairs.network.ApiPayInterface;
import gk.gkcurrentaffairs.payment.model.AppUser;
import gk.gkcurrentaffairs.payment.model.LoginSignUpResponse;
import gk.gkcurrentaffairs.payment.utils.CircleTransform;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;

public class ProfileFragment extends Fragment implements View.OnClickListener  {

    private String TAG = "ProfileFragment";
//    private OnFragmentInteractionListener mListener;
    private static final int RC_SIGN_IN = 11;
    private static final int LOCATION_PERMISSION = 12;

    ApiPayInterface apiService;

    private final int USER_NOT_LOGGED_IN = 1;
    private final int USER_LOGGED_IN_BUT_REG_NOT_COMPLETE = 2;
    private final int USER_LOGGED_IN = 3;
    private final int USER_EDIT_PROFILE = 4;
    private final int USER_NOT_VERIFIED = 5;

    ImageView ivLogout;
    TextView bLogin;
    TextView tvSaveProfile;
    AppCompatImageView ivProfilePic;
    ImageView ivEditProfile;
    TextView tvUserName;

    TextView tvEmailValue;
    TextView tvPhoneValue;
    TextView tvAddressValue;
    TextView tvStateValue;
    TextView tvPostalCodeValue;

    TextView tvCompleteYourReg;
    TextView tvEditYourProfile;
    LinearLayout llEditBlock;
    LinearLayout llProfileInfoBlock;
    LinearLayout llNotLoggedInBlock;
    LinearLayout llLoggedInBlock;
    LinearLayout llNotVerifiedUser;
    ScrollView svProfileView;
    ScrollView svEditProfileBlock;

    EditText etName;
    EditText etEmail;
    EditText etPhone;
    EditText etAddress;
    Spinner spinnerState;
    EditText etPostalCode;

    LinearLayout progressBar;

    TextView tvAlreadyVerified, tvResendEmail, tvLoginAsDiffUser;
    private Activity activity ;


    public ProfileFragment() {

    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        activity = getActivity();
        initViews(rootView);
        checkUserCurrentStatus();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRetrofit();
    }

    public void initRetrofit() {
        apiService = AppApplication.getInstance().getNetworkObjectPay() ;//RetrofitServiceGenerator.createService(MyApiEndpointInterface.class);
    }

    private void checkUserCurrentStatus() {

        if (SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID)
                .equalsIgnoreCase("")) { //UserNotLoggedIn
            if(SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_USER_NOT_VERIFIED)) {
                userIsNotVerified();
            } else {
                Logger.e(TAG+ "checkUserCurrentStatus : UserNotLoggedIn");
                setProfileScreenView(USER_NOT_LOGGED_IN);
            }
        } else {
            Logger.e(TAG+ "checkUserCurrentStatus : UserLoggedIn");
            setProfileScreenView(USER_LOGGED_IN);
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private boolean isFirstGeocoder = true ;
    private String address , pincode ;
    public String getAddress(double lat, double lon)
    {
        Geocoder geocoder = new Geocoder(activity, Locale.ENGLISH);
        String ret = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    if (!SupportUtil.isEmptyOrNull(returnedAddress.getPostalCode()) ){
                        pincode = returnedAddress.getPostalCode();
                    }
                }
                ret = strReturnedAddress.toString();
            }
            else{
                ret = "No Address returned!";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
            if ( isFirstGeocoder ){
                isFirstGeocoder = false;
                ret = getAddress( lat , lon );
            }
        }
        return ret;
    }

    class AsyncLocation extends AsyncTask<Void , Void , Void>{

        private ProgressDialog progressDialog ;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage("Fetching Address");
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            requestForLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ( !SupportUtil.isEmptyOrNull( address ) ){
                etAddress.setText(address);
            }
            if ( !SupportUtil.isEmptyOrNull( pincode ) ){
                etPostalCode.setText(pincode);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == ResultCodes.OK) {
                registerUser();
                return;
            } else {
                // Sign in failed
                if (response == null) {
                    Logger.e(TAG, "AppUser pressed back button");
                    showSnackbar(R.string.sign_in_cancelled);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Logger.e(TAG, "no_internet_connection");
                    showSnackbar(R.string.no_internet_connection);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Logger.e(TAG, "UNKNOWN_ERROR");
                    showSnackbar(R.string.unknown_error);
                    return;
                }
            }

            Logger.e(TAG, "unknown_sign_in_response");
            showSnackbar(R.string.unknown_sign_in_response);
        }else if ( requestCode == LOCATION_PERMISSION ){
        }
    }

    private void handlePermission(){
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION);
        }else{
            new AsyncLocation().execute();
        }
    }

    private void requestForLocation(){
        LocationManager mLocationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        if ( mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            try {
                Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if ( location == null ) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1000, mLocationListener);
                    location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                     address = getAddress(location.getLatitude() , location.getLongitude()) ;
                }else
                    address = getAddress(location.getLatitude() , location.getLongitude()) ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else
            showSettingsAlert();
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
                }
            });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                }
            });
        alertDialog.show();
        }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new AsyncLocation().execute();

                } else {
                    // Permission Denied
                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }



    private void initViews(View view) {

        ivLogout = (ImageView) view.findViewById(R.id.ivLogout);
        ivEditProfile = (ImageView) view.findViewById(R.id.ivEditProfile);
        bLogin = (TextView) view.findViewById(R.id.bLogin);
        ivProfilePic = (AppCompatImageView) view.findViewById(R.id.ivProfilePic);
        tvUserName = (TextView) view.findViewById(R.id.tvUserName);
        tvCompleteYourReg = (TextView) view.findViewById(R.id.tvCompleteYourReg);
        tvEditYourProfile = (TextView) view.findViewById(R.id.tvEditYourProfile);

        tvEmailValue = (TextView) view.findViewById(R.id.tvEmailValue);
        tvPhoneValue = (TextView) view.findViewById(R.id.tvPhoneValue);
        tvAddressValue = (TextView) view.findViewById(R.id.tvAddressValue);
        tvStateValue = (TextView) view.findViewById(R.id.tvStateValue);
        tvPostalCodeValue = (TextView) view.findViewById(R.id.tvPostalCodeValue);

        tvAlreadyVerified = (TextView) view.findViewById(R.id.tvAlreadyVerified);
        tvResendEmail = (TextView) view.findViewById(R.id.tvResendEmail);
        tvLoginAsDiffUser = (TextView) view.findViewById(R.id.tvLoginAsDiffUser);

        tvSaveProfile = (TextView) view.findViewById(R.id.tvSaveProfile);
        llEditBlock = (LinearLayout) view.findViewById(R.id.llEditBlock);
        llProfileInfoBlock = (LinearLayout) view.findViewById(R.id.llProfileInfoBlock);
        llLoggedInBlock = (LinearLayout) view.findViewById(R.id.llLoggedInBlock);
        llNotLoggedInBlock = (LinearLayout) view.findViewById(R.id.llNotLoggedInBlock);
        llNotVerifiedUser = (LinearLayout) view.findViewById(R.id.llNotVerifiedUser);
        svEditProfileBlock = (ScrollView) view.findViewById(R.id.svEditProfileBlock);
        svProfileView = (ScrollView) view.findViewById(R.id.svProfileView);

        etName = (EditText) view.findViewById(R.id.etName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etPhone = (EditText) view.findViewById(R.id.etPhone);
        etAddress = (EditText) view.findViewById(R.id.etAddress);
        spinnerState = (Spinner) view.findViewById(R.id.spinnerState);
        etPostalCode = (EditText) view.findViewById(R.id.etPostalCode);

        progressBar = (LinearLayout) view.findViewById(R.id.progressBar);

        ivLogout.setOnClickListener(this);
        bLogin.setOnClickListener(this);
        ivEditProfile.setOnClickListener(this);
        tvSaveProfile.setOnClickListener(this);
        tvAlreadyVerified.setOnClickListener(this);
        tvResendEmail.setOnClickListener(this);
        tvLoginAsDiffUser.setOnClickListener(this);
    }

    public void logOutClick() {
        AuthUI.getInstance()
                .signOut(getActivity())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Logger.e(TAG, "LogOut Success");
                        SharedPrefUtil.clearPreferences();
                        checkUserCurrentStatus();
                    }
                });
    }

    public void logInClick() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setProviders(Arrays.asList(
                                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()
                        ))
                        .setLogo(R.mipmap.ic_launcher)
                        .setTosUrl("http://dexterdev.org/privacy-policy/result.html")
                        .setTheme(R.style.LoginScreenTheme)
                        .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                        .build(),
                RC_SIGN_IN
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivEditProfile:
                Logger.e(TAG, "ivEditProfile");
                setProfileScreenView(USER_EDIT_PROFILE);
                break;
            case R.id.ivLogout:
                logOutClick();
                break;
            case R.id.bLogin:
                logInClick();
                break;
            case R.id.tvSaveProfile:
                updateUserProfile();
                break;
            case R.id.tvAlreadyVerified:
                alreadyVerifiedEmail();
                break;
            case R.id.tvResendEmail:
                resendEmail();
                break;
            case R.id.tvLoginAsDiffUser:
                logOutClick();
                break;
        }
    }

    private void setProfileScreenView(int currentLevel) {
        switch (currentLevel) {
            case USER_NOT_LOGGED_IN:
                llNotLoggedInBlock.setVisibility(View.VISIBLE);
                svEditProfileBlock.setVisibility(View.GONE);
                svProfileView.setVisibility(View.GONE);
                llNotVerifiedUser.setVisibility(View.GONE);
                break;
            case USER_NOT_VERIFIED:
                llNotVerifiedUser.setVisibility(View.VISIBLE);
                llNotLoggedInBlock.setVisibility(View.GONE);
                svEditProfileBlock.setVisibility(View.GONE);
                svProfileView.setVisibility(View.GONE);
                break;
            case USER_LOGGED_IN_BUT_REG_NOT_COMPLETE:
                tvEditYourProfile.setVisibility(View.GONE);
                tvCompleteYourReg.setVisibility(View.VISIBLE);
                llNotLoggedInBlock.setVisibility(View.GONE);
                svEditProfileBlock.setVisibility(View.VISIBLE);
                svProfileView.setVisibility(View.GONE);
                llNotVerifiedUser.setVisibility(View.GONE);
                showUserEditProfile();
                break;
            case USER_LOGGED_IN:
                llNotLoggedInBlock.setVisibility(View.GONE);
                svEditProfileBlock.setVisibility(View.GONE);
                svProfileView.setVisibility(View.VISIBLE);
                llNotVerifiedUser.setVisibility(View.GONE);
                showUserProfile();
                break;
            case USER_EDIT_PROFILE:
                tvEditYourProfile.setVisibility(View.VISIBLE);
                tvCompleteYourReg.setVisibility(View.GONE);
                llNotLoggedInBlock.setVisibility(View.GONE);
                svEditProfileBlock.setVisibility(View.VISIBLE);
                svProfileView.setVisibility(View.GONE);
                llNotVerifiedUser.setVisibility(View.GONE);
                showUserEditProfile();
                handlePermission();
                break;
        }
    }

    private void showUserEditProfile() {

        Logger.e(TAG, "showUserEditProfile");
        etName.setText(SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME));
        etEmail.setText(SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL));
        etPhone.setText(SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHONE));
        etAddress.setText(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ADDRESS));
        etPostalCode.setText(SharedPrefUtil.getString(AppConstant.SharedPref.USER_POSTAL_CODE));
        int stateId = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_STATE);
        spinnerState.setSelection(stateId);
    }

    private void updateUserProfile() {

        progressBar.setVisibility(View.VISIBLE);

        String userName = etName.getText().toString();
        String userEmail = etEmail.getText().toString();
        String userPhone = etPhone.getText().toString();
        String userAddress = etAddress.getText().toString();
        String userPostalCode = etPostalCode.getText().toString();
        int userStateId = spinnerState.getSelectedItemPosition();

        String uid = SharedPrefUtil.getString(AppConstant.SharedPref.USER_UID);
        sendUserProfile( userEmail , uid , userName , userPhone , userAddress , userStateId , userPostalCode );

    }

    private void sendUserProfile( String userEmail , String uid , String userName , String userPhone ,
                                  String userAddress , int userStateId , String userPostalCode ){

        apiService.updateUserProfile(
                userEmail, uid, 0, userName,
                "", "", "", userPhone, userAddress,
                userStateId, userPostalCode
        ).enqueue(new Callback<LoginSignUpResponse>() {
            @Override
            public void onResponse(Call<LoginSignUpResponse> call, Response<LoginSignUpResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response != null && response.body() != null ) {
                    if(response.body().getStatus() == 4) {
                        userIsNotVerified();
                    } else {
                        saveUserToSharedPref(response.body().getAppUser(), false);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginSignUpResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void loginSignUp(FirebaseUser user) {

        progressBar.setVisibility(View.VISIBLE);

        String photoUrl = "";
        String providerId = "";
        int isEmailVerified = 0;

        if (user.getPhotoUrl() != null) {
            photoUrl = user.getPhotoUrl().toString();
        }

        if (user.getProviders() != null && user.getProviders().size() > 0) {
            providerId = user.getProviders().get(0);
        }

        if (user.isEmailVerified()) {
            isEmailVerified = 1;
        }

        apiService.loginSignUp(
                user.getEmail(), user.getUid(), isEmailVerified, user.getDisplayName(),
                photoUrl, providerId, "", "", "", "", 0, ""
        ).enqueue(new Callback<LoginSignUpResponse>() {
            @Override
            public void onResponse(Call<LoginSignUpResponse> call, Response<LoginSignUpResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response != null && response.body() != null ) {
                    if (response.body().getStatus() > 0) {
                        if(response.body().getStatus() == 4) {
                            userIsNotVerified();
                        } else {
                            saveUserToSharedPref(response.body().getAppUser(), true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginSignUpResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void registerUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            loginSignUp(user);
        }
    }

    private void saveUserToSharedPref(AppUser appUser, boolean isCalledByLoginMethod) {

        if(appUser.getProvider_id().equalsIgnoreCase("password")
                && appUser.isEmail_verified() == 0) {
            userIsNotVerified();
        } else {
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_UID, appUser.getUid());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_EMAIL, appUser.getEmail());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_ADDRESS, appUser.getAddress());
            SharedPrefUtil.setInt(AppConstant.SharedPref.USER_STATE, appUser.getState());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHONE, appUser.getMobile());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_POSTAL_CODE, appUser.getPostal());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_NAME, appUser.getName());
            SharedPrefUtil.setString(AppConstant.SharedPref.USER_PHOTO_URL, appUser.getPhoto_url());

            if(isCalledByLoginMethod) {
                setProfileScreenView(USER_LOGGED_IN_BUT_REG_NOT_COMPLETE);
            } else {
                setProfileScreenView(USER_LOGGED_IN);
            }
        }
    }

    private void showUserProfile() {

        String photoUrl = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHOTO_URL);
        String userName = SharedPrefUtil.getString(AppConstant.SharedPref.USER_NAME);
        String phone = SharedPrefUtil.getString(AppConstant.SharedPref.USER_PHONE);
        int stateId = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_STATE);
        String postalCode = SharedPrefUtil.getString(AppConstant.SharedPref.USER_POSTAL_CODE);
        String address = SharedPrefUtil.getString(AppConstant.SharedPref.USER_ADDRESS);
        String email = SharedPrefUtil.getString(AppConstant.SharedPref.USER_EMAIL);

        if(!photoUrl.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(photoUrl)
                    .resize(125, 125)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(ivProfilePic);
        } else {
            ivProfilePic.setImageResource(R.drawable.profile_black);
        }

        tvUserName.setText(userName);
        tvEmailValue.setText(email);
        tvPhoneValue.setText(phone);
        tvAddressValue.setText(address);
        tvPostalCodeValue.setText(postalCode);

        if(stateId > 0) {
            String stateName = getResources().getStringArray(R.array.state_names)[stateId];
            tvStateValue.setText(stateName);
        }
    }

    @MainThread
    private void showSnackbar(@StringRes int errorMessageRes) {
        Snackbar.make(progressBar, errorMessageRes, Snackbar.LENGTH_LONG).show();
    }

    private void userIsNotVerified() {

        SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_USER_NOT_VERIFIED, true);
        setProfileScreenView(USER_NOT_VERIFIED);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {

            if(!SharedPrefUtil.getBoolean(AppConstant.SharedPref.IS_VERIFICATION_EMAIL_SENT)) {
                user.sendEmailVerification();
                SharedPrefUtil.setBoolean(AppConstant.SharedPref.IS_VERIFICATION_EMAIL_SENT, true);
            }

            Logger.e(TAG, "--1" + user.getEmail());
            Logger.e(TAG, "--2" + user.getDisplayName());
        }
    }

    private void resendEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification();
            showSnackbar(R.string.email_resend_message);
        }
    }

    private void alreadyVerifiedEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.reload();
            if(user.isEmailVerified()) {
                registerUser();
            }
        }
    }
}
*/
