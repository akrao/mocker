package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adssdk.AdsSDK;
import com.adssdk.util.AdsCallBack;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.SectionAdapter;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.payment.model.Child;
import gk.gkcurrentaffairs.payment.model.SectionData;
import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 8/30/2017.
 */

public class PaidHomeListFragment extends Fragment implements AdsCallBack {

    private View view ;
    private Activity activity ;
    private RecyclerView recyclerView ;
    private SectionData sectionData ;
    private String imagePath ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_paid_home_list, container, false);
        activity = getActivity();
        initViews();
        initObjects();
        initNativeAds();
        return view;
    }

    private void initNativeAds(){
        if ( AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null ) {
            if (!AdsSDK.getInstance().getAdsNative().isNativeAdCache() && !isNativeCallBack  ) {
                AdsSDK.getInstance().setAdsCallBack(this.getClass().getName() + sectionData.getId() , this);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        adapter.setSubscribe(PaidHomeActivity.getInstance().isSubscribed());
        adapter.notifyDataSetChanged();
    }

    private void initViews(){
        recyclerView = ( RecyclerView ) view.findViewById( R.id.itemsRecyclerView );
        if ( getArguments() != null && getArguments().getSerializable(AppConstant.DATA ) != null ) {
            sectionData = (SectionData) getArguments().getSerializable(AppConstant.DATA);
            imagePath = getArguments().getString( AppConstant.IMAGE );
        }
    }

    private SectionAdapter adapter ;
    private void initObjects(){
        if ( sectionData != null ){
            recyclerView.setLayoutManager( new LinearLayoutManager( activity ) );
            adapter = new SectionAdapter( activity , getChildWithAds() , imagePath , sectionData.getSlidersBeanList() );
            recyclerView.setAdapter( adapter );
        }
    }

    private List<Child> getChildWithAds(){
        List<Child> children = new ArrayList<>();
        int length = sectionData.getChildren().size();
        Child baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            children.add(sectionData.getChildren().get(i));
            if ( (i == 0 || i == 4 )&& AppConstant.IS_ADS_ENABLED ){
                baseAdModelClass = new Child();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                children.add(baseAdModelClass);
            }
        }
        return children;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if ( adapter != null )
            adapter.setActive(false);
    }

    private boolean isNativeCallBack = false ;
    @Override
    public void onNativeAdsCache() {
        if ( !isNativeCallBack ) {
            isNativeCallBack = true;
            if ( AdsSDK.getInstance() != null ) {
                AdsSDK.getInstance().removeAdsCallBack(this.getClass().getName()+ sectionData.getId());
            }
            if ( adapter != null )
                adapter.notifyDataSetChanged();
        }
    }
}
