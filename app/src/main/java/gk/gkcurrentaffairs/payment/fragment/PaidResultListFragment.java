package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.activity.PaidResultActivity;
import gk.gkcurrentaffairs.payment.activity.SolutionActivity;
import gk.gkcurrentaffairs.payment.adapter.PaidResultListAdapter;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/20/2017.
 */

public class PaidResultListFragment extends BaseFragment implements PaidResultListAdapter.OnCustomClick {

    private View view, pbLoading, vNoData;
    private TextView tvNoData ;
    private Activity activity;
    private List<PaidResult> lists = new ArrayList<>();
    private DbHelper dbHelper;
    private PaidResultListAdapter resultListAdapter;
    private ProgressDialog pDialog;
    private boolean isResume;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_my_activity, container, false);
        activity = getActivity();
        isResume = getArguments().getBoolean(AppConstant.TYPE, false);
/*
        boolean shouldLoad = getArguments().getBoolean( AppConstant.SHOULD_LOAD , false );
        if ( shouldLoad )
            onRefreshFragment();
*/
        return view;
    }

    private boolean isLoaded = false;

    @Override
    public void onRefreshFragment() {
        if (!isLoaded) {
            init();
            isLoaded = true;
        }
    }

    private void init() {
        initViews();
        setEmptyList();
        new FetchDataFromDB().execute();
    }

    private RecyclerView recyclerView;
    private void setEmptyList() {
        resultListAdapter = new PaidResultListAdapter(lists, this, "Practice Result");
//        resultListAdapter = new PaidResultListAdapter(lists, this , isGlobal? "Global Result" : "Practice Result");
        recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(resultListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLoaded) {
            new FetchDataFromDB().execute();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = (TextView) view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
        dbHelper = AppApplication.getInstance().getDBObject();
//        apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
        initDialog();
    }

    private void showData() {
        vNoData.setVisibility(View.GONE);
        resultListAdapter.notifyDataSetChanged();
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void showNoData() {
        vNoData.setVisibility(View.VISIBLE);
        resultListAdapter.notifyDataSetChanged();
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText("No Data");
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onCustomClick(int position, int type) {
        if (lists.get(position).getStatus() == AppConstant.INT_FALSE) {
            SupportUtil.openMock(lists.get(position).getMockId(), activity
                    , lists.get(position).getPackageTitle(), lists.get(position).getPackageId());
        } else {
            openResult(position , type);
        }
//        handleMock( position , ( lists.get(position).getStatus() == AppConstant.INT_TRUE ) , type );
    }

    private void openResult(final int position , final int type) {
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                boolean isDownloaded = dbHelper.isPaidMockDownloaded(lists.get(position).getMockId());
                if (isDownloaded) {
                    PaidMockTest paidMockTest = dbHelper.fetchPaidMock(lists.get(position).getMockId());
                    fetchResultAndOpen(paidMockTest , type);
                } else{
                    SupportUtil.downloadMockTest(lists.get(position).getMockId(), new SupportUtil.DownloadMCQ() {
                        @Override
                        public void onResult(boolean result, PaidMockTest paidMockTest) {
                            if ( result ){
                                fetchResultAndOpen(paidMockTest , type);
                            }
                        }
                    }, activity);
                }
                return null;
            }
        });
    }

    private void fetchResultAndOpen(PaidMockTest paidMockTest , int type){
        PaidResult result = dbHelper.fetchPaidResult(paidMockTest.getId(), isGlobal);
        new FetchAllResult(result, paidMockTest, true, type).execute();

    }

    private boolean isGlobal = false;

    private void openResult(PaidResult result, int type) {
        Intent intent = new Intent(activity, PaidResultActivity.class);
        intent.putExtra(AppConstant.DATA, result);
        intent.putExtra(AppConstant.TYPE, isGlobal);
//        intent.putExtra( AppConstant.TYPE , type == AppConstant.INT_FALSE ? false : AppConstant.INT_TRUE );
        activity.startActivity(intent);
    }

    private void openSolution(PaidResult result) {
        Intent intent = new Intent(activity, SolutionActivity.class);
        intent.putExtra(AppConstant.DATA, result);
        activity.startActivity(intent);
    }

    private void initDialog() {
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Fetching Data....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    private void showDialog() {
        try {
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideDialog() {
        try {
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class FetchDataFromDB extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if (isResume) {
                        dbHelper.fetchPaidTestResume(lists);
                    } else {
                        dbHelper.fetchPaidResult(lists, isGlobal);
                    }
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideDialog();
            if (lists != null && lists.size() > 0)
                showData();
            else
                showNoData();
        }
    }

    class FetchAllResult extends AsyncTask<Void, Void, Void> {

        private PaidResult result;
        private PaidMockTest paidMockTest;
        private boolean isResult;
        private int type;

        public FetchAllResult(PaidResult result, PaidMockTest paidMockTest, boolean isResult, int type) {
            this.result = result;
            this.isResult = isResult;
            this.type = type;
            this.paidMockTest = paidMockTest;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    result = dbHelper.updatePaidResult(result, paidMockTest);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideDialog();
            if (type == AppConstant.INT_TRUE) {
                openSolution(result);
            } else if (isResult) {
                openResult(result, type);
            }
        }
    }
}
