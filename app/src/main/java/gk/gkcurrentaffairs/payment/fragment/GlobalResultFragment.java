package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adssdk.AdsSDK;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.CategoryListAdapter;
import gk.gkcurrentaffairs.bean.CategoryBean;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.PaidSectionResultActivity;
import gk.gkcurrentaffairs.payment.adapter.PaidResultAdapter;
import gk.gkcurrentaffairs.payment.adapter.PaidResultScrollHandler;
import gk.gkcurrentaffairs.payment.model.BaseResultBean;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidMockTestResult;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.PaidTestCat;
import gk.gkcurrentaffairs.payment.model.SectionResultBean;
import gk.gkcurrentaffairs.payment.model.SectionTimeBean;
import gk.gkcurrentaffairs.payment.model.ServerTestResultBean;
import gk.gkcurrentaffairs.payment.model.TestResultBean;
import gk.gkcurrentaffairs.payment.model.TimeDistributionBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.Logger;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 7/19/2018.
 */

public class GlobalResultFragment extends Fragment implements PaidResultAdapter.OnCustomClick, View.OnClickListener
        , CategoryListAdapter.OnCustomClick {

    private PaidResult paidResult;
    private String[] header = new String[4];
    private ArrayList<PaidSectionResult> sectionResults;
    private PaidResultAdapter paidResultAdapter;
    private ProgressDialog pDialog;
    private View rlDlg;
    private ArrayList<CategoryBean> sectionListTitles;
    private boolean isGlobal;
    private ServerTestResultBean serverTestResultBean;
    private TimeDistributionBean timeDistributionBean;
    private double totalTimeTaken = 0, correctAnsTime = 0, wrongAnsTime = 0, skipAnsTIme = 0;
    private BaseResultBean baseResultBean;
    private RecyclerView recyclerView;


    private View view;
    private Activity activity;
    private int colorYellow, colorGreen, colorRed;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_paid_result, container, false);
        activity = getActivity();
        initDataFromArgs();
        getUserResult();
        return view;
    }

    private void computeResultData() {
        serverTestResultBean = new ServerTestResultBean();
        serverTestResultBean.setTestResultBean(testResultBean);
        sectionResults = new ArrayList<>(sectionCount);
        timeDistributionBean = new TimeDistributionBean();
        timeDistributionBean.setSectionTimeBeen(new ArrayList<SectionTimeBean>(sectionCount));
        initBaseData();
        if (paidResult.getTestCats() == null || paidResult.getTestCats().size() == 0) {
            List<PaidTestCat> paidTestCats = new ArrayList<>(sectionCount);
            for (SectionResultBean bean : testResultBean.getSectionResultBeen()) {
                paidTestCats.add(bean.getPaidTestCat());
            }
            paidResult.setTestCats(paidTestCats);
        }
        if ( paidResult != null ) {
            computeSectionData();
            initTimeDistributionData();
        } else {
            SupportUtil.showToastCentre(activity , "Error , Please try later.");
            activity.finish();
        }
    }

    private void initBaseData() {
        baseResultBean = new BaseResultBean();
        baseResultBean.setStringList(new ArrayList<String>(6));

        float score = SupportUtil.stringToFloat(testResultBean.getTotalMarks());
        float totalMarks = SupportUtil.stringToFloat(testResultBean.getMaxValue());
        baseResultBean.getStringList().add("");
        baseResultBean.getStringList().add(testResultBean.getCorrectAnswer());
        baseResultBean.getStringList().add("");
        baseResultBean.getStringList().add(testResultBean.getWrongAnswer());
        baseResultBean.getStringList().add(String.format(Locale.ENGLISH , "%.2f/%d", score, (int) totalMarks));
        baseResultBean.getStringList().add(testResultBean.getUnattemptQuestions());
        serverTestResultBean.setBaseResultBean(baseResultBean);

    }
    private void initTimeDistributionData() {
        timeDistributionBean.setTotalTime(getTime((long) totalTimeTaken));
        timeDistributionBean.setCorrectAnsTime(getTime((long) correctAnsTime));
        timeDistributionBean.setWrongAnsTime(getTime((long) wrongAnsTime));
        timeDistributionBean.setSkippedAnsTime(getTime((long) skipAnsTIme));
        serverTestResultBean.setTimeDistributionBean(timeDistributionBean);
    }

    private void computeSectionData() {
        int l = paidResult.getSectionCount();
        sectionListTitles = new ArrayList<>(l);
        float correct = 0, wrong = 0, totalMarks = 0;
        int correctCount = 0, wrongCount = 0, totalCount = 0;
        CategoryBean categoryBean;
        for (int i = 0; i < l; i++) {
            if (paidResult.getTestCats() != null && paidResult.getTestCats().size() > i && paidResult.getTestCats().get(i) != null
                    && paidResult.getPaidMockTestResults().get(i) != null
                    && paidResult.getPaidMockTestResults() != null
                    && paidResult.getPaidMockTestResults().get(i).size() > 0) {
                PaidTestCat paidTestCat = paidResult.getTestCats().get(i);
                List<PaidMockTestResult> list = paidResult.getPaidMockTestResults().get(i);
                PaidSectionResult result = parseSectionData(paidTestCat, list);
                sectionResults.add(result);
                correct += (result.getCorrect() * paidTestCat.getQuestMarks());
                wrong += (result.getWrong() * paidTestCat.getNegetiveMarking());
                correctCount += result.getCorrect();
                wrongCount += result.getWrong();
                totalMarks += paidTestCat.getTestMarkes();
                totalCount += paidTestCat.getNoOfQuestions();
                categoryBean = new CategoryBean();
                categoryBean.setCategoryName(paidTestCat.getTitle());
                sectionListTitles.add(categoryBean);
            }
        }
        float score = correct - wrong;

        header[0] = String.format(Locale.ENGLISH , "%.2f/%d", score, (int)totalMarks);
        header[1] = totalMarks + "";
        header[2] = correct + "";
        header[3] = wrong + "";
        baseResultBean = new BaseResultBean();
        baseResultBean.setStringList(new ArrayList<String>(6));
        baseResultBean.getStringList().add(correct + "");
        baseResultBean.getStringList().add(correctCount + "");
        baseResultBean.getStringList().add(wrong + "");
        baseResultBean.getStringList().add(wrongCount + "");
        baseResultBean.getStringList().add(String.format(Locale.ENGLISH , "%.2f/%d", score, (int) totalMarks));
//        baseResultBean.getStringList().add(testResultBean.getMax_marks() + "");
        baseResultBean.getStringList().add((totalCount-wrongCount-correctCount )+ "");
        serverTestResultBean.setBaseResultBean(baseResultBean);
    }

    private PaidSectionResult parseSectionData(PaidTestCat paidTestCat, List<PaidMockTestResult> list) {
        if (paidTestCat.getNoOfQuestions() == 0) {
            paidTestCat.setNoOfQuestions(list.size());
        }
        PaidSectionResult sectionResult = new PaidSectionResult();
        sectionResult.setTitle(paidTestCat.getTitle());
        double time = 0, secCorrectTime = 0, secWrongTime = 0;
        int correct = 0, wrong = 0, unAttended = 0;
        for (PaidMockTestResult mockTestResult : list) {
            time += mockTestResult.getTimeTaken();
            if (mockTestResult.getStatus() < AppConstant.PAID_QUE_NOT_VISIT) {
                if (mockTestResult.getStatus() == mockTestResult.getActualAns()) {
                    correct++;
                    secCorrectTime += mockTestResult.getTimeTaken();
                } else {
                    wrong++;
                    secWrongTime += mockTestResult.getTimeTaken();
                }
            } else {
                unAttended++;
                skipAnsTIme += mockTestResult.getTimeTaken();
            }
        }
        totalTimeTaken += time;
        correctAnsTime += secCorrectTime;
        wrongAnsTime += secWrongTime;
        sectionResult.setCorrect(correct);
        sectionResult.setWrong(wrong);
        sectionResult.setUnattended(unAttended);
        sectionResult.setCorrectAnsTime(getTime((long) secCorrectTime));
        sectionResult.setWrongAnsTime(getTime((long) secWrongTime));
        double score = (correct > 0 ? correct * paidTestCat.getQuestMarks() : 0) -
                (wrong > 0 ? wrong * paidTestCat.getNegetiveMarking() : 0);
        sectionResult.setScore(String.format(Locale.ENGLISH , "%.2f/%d", score, paidTestCat.getTestMarkes()));
        String t = getTime((long) time);
        sectionResult.setTime(t);
        timeDistributionBean.getSectionTimeBeen().add(new SectionTimeBean(t, paidTestCat.getTitle()));
        float correctPercentage = getPercentage(correct, paidTestCat.getNoOfQuestions());
        float wrongPercentage = getPercentage(wrong, paidTestCat.getNoOfQuestions());
        float unAttendedPercentage = getPercentage(unAttended, paidTestCat.getNoOfQuestions());
        Logger.e("parseSectionData", "correct : " + correctPercentage);
        Logger.e("parseSectionData", "wrong : " + wrongPercentage);
        Logger.e("parseSectionData", "unAttended : " + unAttendedPercentage);
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(correctPercentage, ""));
        entries.add(new PieEntry(wrongPercentage, ""));
        entries.add(new PieEntry(unAttendedPercentage, ""));

        PieDataSet set = new PieDataSet(entries, sectionResult.getTitle());
        set.setColors(new int[]{colorGreen, colorRed, colorYellow});
        PieData data = new PieData(set);
        sectionResult.setPieData(data);

/*
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        if ( correctPercentage > 0 ) {
            pieHelperArrayList.add(new PieHelper(8 , colorGreen));
        }
        if (wrongPercentage > 0) {
            pieHelperArrayList.add(new PieHelper(0 , colorRed));
        }
        if ( unAttendedPercentage > 0 ) {
            pieHelperArrayList.add(new PieHelper(92 , colorYellow));
        }
        sectionResult.setPieHelperArrayList(pieHelperArrayList);
*/
        return sectionResult;
    }

    private String getTime(long time) {
        if (time > 0) {
            return String.format(Locale.ENGLISH , "%d min : %d sec",
                    TimeUnit.MILLISECONDS.toMinutes(time),
                    TimeUnit.MILLISECONDS.toSeconds(time) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        } else
            return "0 min : 0 sec";
    }

    private float getPercentage(int value, int numberQue) {
        return ((value * 100) / numberQue);
    }

    int mockTestId;

    private void initDataFromArgs() {
        paidResult = (PaidResult) getArguments().getSerializable(AppConstant.DATA);
        mockTestId = paidResult.getMockId();
        isGlobal = getArguments().getBoolean(AppConstant.TYPE, true);
        colorGreen = SupportUtil.getColor(R.color.green_mcq_paid, activity);
        colorRed = SupportUtil.getColor(R.color.wrong_red, activity);
        colorYellow = SupportUtil.getColor(R.color.graph_yellow, activity);
        dbHelper = AppApplication.getInstance().getDBObject() ;
    }

    private void setDataInView() {
        PaidResultScrollHandler paidResultScrollHandler = new PaidResultScrollHandler(activity, sectionResults, serverTestResultBean, new PaidResultScrollHandler.OnCustomClick() {
            @Override
            public void onCustomClick(int position) {
                onCustomItemClick(position);
            }
        });
        paidResultScrollHandler.setFragment(this);
        paidResultScrollHandler.setGlobal(true);
        paidResultScrollHandler.setView(view.findViewById(R.id.ll_main_view));
        paidResultScrollHandler.init();
    }

    private void setDataInView1() {
//        paidResultAdapter = new PaidResultAdapter(activity, sectionResults, serverTestResultBean, this);
        paidResultAdapter = new PaidResultAdapter(activity, getChildWithAds(), serverTestResultBean, this);
        paidResultAdapter.setGlobal(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_sec);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(paidResultAdapter);
    }

    private List<PaidSectionResult> getChildWithAds(){
        List<PaidSectionResult> children = new ArrayList<>();
        int length = sectionResults.size();
        PaidSectionResult baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            sectionResults.get(i).setPosition(i);
            children.add(sectionResults.get(i));
            if ( i == 0 ){
                baseAdModelClass = new PaidSectionResult();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                children.add(baseAdModelClass);
            }
        }
        return children;
    }

    private void initDlgView() {
        rlDlg = view.findViewById(R.id.rl_dlg);
        view.findViewById(R.id.dlg_close).setOnClickListener(this);
        view.findViewById(R.id.ll_bottom).setOnClickListener(this);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_section);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        CategoryListAdapter adapter = new CategoryListAdapter(sectionListTitles, this, R.layout.item_list_image_text);
        adapter.setTextual(true);
        recyclerView.setAdapter(adapter);
    }

    TestResultBean testResultBean;

    private void getUserResult() {
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
            Map<String, String> map = new HashMap<>(3);
            map.put("application_id", activity.getPackageName());
            map.put("paper_id", paidResult.getMockId() + "");
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_RESULT_BY_USER_AND_PAPER_ID, map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            hideDialog();
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    testResultBean = ConfigManager.getGson().fromJson(data, TestResultBean.class);
                                    if (testResultBean != null && testResultBean.getSectionResultBeen() != null
                                            && testResultBean.getSectionResultBeen().size() > 0) {
                                        showData();
                                    } else
                                        showNoData();
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    showNoData();
                                }
                            } else
                                showNoData();
                        }
                    });
        }
    }

    private void showData() {
        sectionCount = testResultBean.getSectionResultBeen().size();
        if (!SupportUtil.isEmptyOrNull(testResultBean.getRawData())) {
            handleRawData();
        } else {
            convertDataFormat();
        }

    }

    private void handleRawData() {
        try {
            paidResult = ConfigManager.getGson().fromJson(testResultBean.getRawData(), PaidResult.class);
            new ComputeResult().execute();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            showNoData();
        }
    }

    private int sectionCount;

    private void convertDataFormat() {
        serverTestResultBean = new ServerTestResultBean();
        sectionResults = new ArrayList<>(sectionCount);
        timeDistributionBean = new TimeDistributionBean();
        timeDistributionBean.setSectionTimeBeen(new ArrayList<SectionTimeBean>(sectionCount));
        computeSectionData();
        initTimeDistributionData();
    }

    private void showNoData() {
        SupportUtil.showToastCentre(activity, "No Data");
    }

    @Override
    public void onCustomClick(int position) {
        if (isDownloaded()) {
            if (position >= 0 && position < sectionResults.size()) {
                Intent intent = new Intent(activity, PaidSectionResultActivity.class);
                //            intent.putExtra(AppConstant.DATA, paidQuestions);
                //            intent.putExtra(AppConstant.CATEGORY, paidMockTestResult);
                intent.putExtra(AppConstant.DATA, paidResult);
                PaidSectionResult sectionResult = sectionResults.get(position ) ;
                sectionResult.setPieData(null);
                intent.putExtra(AppConstant.CLICK_ITEM_ARTICLE, sectionResult );
                intent.putExtra(AppConstant.POSITION, (position ));
                intent.putExtra(AppConstant.CAT_ID, paidResult.getTestCats().get((position)).getId());
                activity.startActivity(intent);
            }
        } else {
            downloadedMCQ(position);
        }
    }

    private DbHelper dbHelper;
    boolean isDownloaded;

    private boolean isDownloaded() {
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                isDownloaded = dbHelper.isPaidMockDownloaded(mockTestId);
                return null;
            }
        });
        return isDownloaded;
    }


    private void downloadedMCQ(final int position) {
        SupportUtil.downloadMockTest(mockTestId, new SupportUtil.DownloadMCQ() {
            @Override
            public void onResult(boolean result, PaidMockTest paidMockTest) {
                if (result ) {
                    onCustomClick(position);
                }
            }
        }, activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_bottom:
                rlDlg.setVisibility(View.VISIBLE);
                break;
            case R.id.dlg_close:
                rlDlg.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onCustomItemClick(int position) {
        onCustomClick(position );
    }

    class ComputeResult extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initDialog();
            showDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            computeResultData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setDataInView();
            initDlgView();
            hideDialog();
        }
    }

    private void initDialog() {
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Computing Result....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    private void showDialog() {
        try {
/*
            if ( !pDialog.isShowing() ) {
                pDialog.show();
            }
*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideDialog() {
        try {
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

