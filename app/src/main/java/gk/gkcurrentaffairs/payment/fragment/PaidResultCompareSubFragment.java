package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adssdk.AdsSDK;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.model.CompareBean;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/18/2018.
 */

public class PaidResultCompareSubFragment extends BaseFragment {
    private Activity activity;
    private View view, tvNoData, pbLoading, vNoData;

    private String mine = "Your %s - %.2f." ;
    private String topper = "Topper %s - %.2f." ;
    private String average = "Average %s - %s." ;
    private String mine_time = "%s by you - %s." ;
    private String topper_time = "%s by Topper - %s." ;
    private TextView tvMe , tvTopper , tvAverage ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_chart_no_data, container, false);
        activity = getActivity();
        initViews();
        initDataFromArgs();
        return view;
    }

    String paperId , type , key ;
    private void initDataFromArgs(){
        PaidResult paidResult = (PaidResult) getArguments().getSerializable(AppConstant.DATA);
        type = getArguments().getString(AppConstant.TYPE);
        key = getArguments().getString(AppConstant.TITLE);
        if ( paidResult != null ){
            paperId = paidResult.getMockId() + "" ;
            boolean isFetch = getArguments().getBoolean(AppConstant.CATEGORY , false );
            if ( isFetch )
                onRefreshFragment();
        }
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
        tvMe = (TextView) view.findViewById(R.id.tv_you);
        tvTopper = (TextView) view.findViewById(R.id.tv_topper);
        tvAverage = (TextView) view.findViewById(R.id.tv_average);
        rlNativeAd = (RelativeLayout) view.findViewById(R.id.rl_native_ad_1);
    }

    CompareBean compareBean;
    private void showData() {
        vNoData.setVisibility(View.GONE);
        createChart();
        setTextData();
        loadNativeAds();
    }
    private void loadNativeAds(){
        if (AdsSDK.getInstance() != null && AdsSDK.getInstance().getAdsNative() != null && rlNativeAd != null) {
            SupportUtil.loadNativeAds(rlNativeAd , R.layout.ads_native_unified_card , true);
        }
    }

    RelativeLayout rlNativeAd ;

    private void setTextData(){
        if (isTime()) {
            tvMe.setText(String.format( mine_time , key , getFormatTime(compareBean.getUserVal() )));
            tvTopper.setText(String.format( topper_time , key , getFormatTime(compareBean.getMaxVal() )));
            tvAverage.setText(String.format( average , key , getFormatTime(Float.parseFloat(compareBean.getAvgVal())) ));
        } else {
            tvMe.setText(String.format( mine , key , compareBean.getUserVal() ));
            tvTopper.setText(String.format( topper , key , compareBean.getMaxVal() ));
            tvAverage.setText(String.format( average , key , compareBean.getAvgVal() ));
        }
    }

    private String getFormatTime(float v){
        int i = (int)v / 1000 ;
        return String.format("%2d min %2d sec" , (i/60) , (i%60) );
    }

    private boolean isTime(){
        return type.equalsIgnoreCase("time");
    }

    private void showRankChart() {
        BarChart barChart1 = (BarChart) view.findViewById(R.id.bar_chart);
        barChart1.setVisibility(View.VISIBLE);
        ArrayList<BarEntry> yvalues4 = new ArrayList<BarEntry>();
//        yvalues4.add(new BarEntry(, 0));
        yvalues4.add(new BarEntry(compareBean.getMaxVal(), 1));
        yvalues4.add(new BarEntry(Float.parseFloat(compareBean.getAvgVal()), 2));
        BarDataSet dataSet = new BarDataSet(yvalues4, "");
        dataSet.setColors(new int[]{
                SupportUtil.getColor(R.color.green_mcq_paid , activity),
                SupportUtil.getColor(R.color.colorPrimary,activity)
                , SupportUtil.getColor(R.color.graph_yellow , activity)});
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("Me" );
        xVals.add("Topper" );
        xVals.add("Average");
        BarData data = new BarData(dataSet);
        barChart1.setData(data);
//        barChart1.setDescription("OverAll");
    }

    private void createChart(){
        BarChart chart = (BarChart) view.findViewById(R.id.bar_chart);
        chart.setVisibility(View.VISIBLE);
        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0f, compareBean.getUserVal() ));
        entries.add(new BarEntry(1f, compareBean.getMaxVal()));
        entries.add(new BarEntry(2f, Float.parseFloat(compareBean.getAvgVal())));

        BarDataSet set = new BarDataSet(entries, type.toUpperCase());
        set.setColors(new int[]{SupportUtil.getColor(R.color.green_mcq_paid , activity), SupportUtil.getColor(R.color.colorPrimary,activity)
                , SupportUtil.getColor(R.color.graph_yellow , activity)});
        BarData data = new BarData(set);
        chart.setData(data);
        chart.setFitBars(true);
        chart.invalidate();
        Description description = new Description();
        description.setText( type.toUpperCase() );
        chart.setDescription(description);
        final String[] quarters = new String[] { "Me", "Topper", "Average"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return quarters[(int) value];
            }
        };

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
    }

    private Drawable getDrawable(int res){
        Drawable drawable = getResources().getDrawable( res );
        return new ScaleDrawable(drawable , 0 , 40 , 40).getDrawable();
    }


    private void fetchData() {
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
            Map<String, String> map = new HashMap<>(4);
            map.put("application_id", activity.getPackageName());
            map.put("paper_id", paperId);
            map.put("type", type);
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_LEADER_BOARD_COMPARE , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    compareBean = ConfigManager.getGson().fromJson(data, CompareBean.class);
                                    if (compareBean != null) {
                                        showData();
                                    } else
                                        showNoData();
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    showNoData();
                                }
                            } else
                                showNoData();
                        }
                    });
        }else if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() == null ){
            AppApplication.getInstance().initOperations();
        }
    }

    private void showNoData() {
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefreshFragment() {
        if( compareBean == null && !SupportUtil.isEmptyOrNull(paperId) ){
            fetchData();
        }
    }
}

