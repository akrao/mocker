package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.model.PackageInfo;
import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 9/6/2017.
 */

public class PackageDetailFragment extends Fragment {

    private View view ;
    private Activity activity;
    private WebView webView ;
    private PackageInfo packageInfo ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_package_detail, container, false);
        activity = getActivity();
        initViews();
        return view;
    }

    private void initViews(){
        if ( getArguments() != null && getArguments().getSerializable(AppConstant.DATA) != null ){
            packageInfo = ( PackageInfo ) getArguments().getSerializable(AppConstant.DATA);
            initObjects();
        }else{
            Toast.makeText( activity , "Error, Please try Again" , Toast.LENGTH_SHORT ).show();
            activity.finish();
        }
    }

    private void initObjects(){
        webView = ( WebView ) view.findViewById( R.id.web_view );
        setDataWebView(webView , packageInfo.getDescription() );
    }

    public void setDataWebView(WebView webView, String data) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("http://localhost", htmlData(data, "#000"), "text/html", "UTF-8", null);
    }

    private String htmlData(String myContent, String color) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<html><head>" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                + "<style type=\"text/css\">body{color: " + color + "; font-size:large; }"
                + "</style>"
                + "<head><body>" + myContent + "</body></html>";

    }


}
