package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adssdk.AdsSDK;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.PackageAdapter;
import gk.gkcurrentaffairs.payment.model.Child;
import gk.gkcurrentaffairs.payment.model.Package;
import gk.gkcurrentaffairs.util.AppConstant;
/**
 * Created by Amit on 5/15/2018.
 */

public class PackageListFragment extends Fragment {
    private View view , tvNoData , pbLoading , vNoData ;
    private Activity activity;
    private List<Package> packageLists;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        initViews();
        getDataFromIntent();
        return view;
    }

    private void initViews(){
        vNoData = view.findViewById( R.id.ll_no_data );
        tvNoData = view.findViewById( R.id.tv_no_data );
        pbLoading = view.findViewById( R.id.player_progressbar );
    }

    private void showData(){
        vNoData.setVisibility( View.GONE );
        RecyclerView recyclerView = (RecyclerView) view.findViewById( R.id.itemsRecyclerView );
        recyclerView.setLayoutManager( new LinearLayoutManager( activity ) );
        recyclerView.setAdapter( new PackageAdapter( activity , getPackageListsWithAds() , imagePath , false , true));
    }

    private String imagePath ;
    private void getDataFromIntent(){
        Bundle arguments = getArguments() ;
        if ( arguments != null && arguments.getSerializable(AppConstant.DATA) != null ){
            Child child = (Child) arguments.getSerializable(AppConstant.DATA) ;
            if ( child != null ) {
                packageLists = child.getPackages();
                imagePath = arguments.getString(AppConstant.IMAGE);
                showData();
            }else {
                showNoData();
            }
        }
    }

    private List<Package> getPackageListsWithAds(){
        List<Package> children = new ArrayList<>();
        int length = packageLists.size();
        Package baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            children.add(packageLists.get(i));
            if ( i == 0 || i == 4 ){
                baseAdModelClass = new Package();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                children.add(baseAdModelClass);
            }
        }
        return children;
    }

    private void showNoData(){
        pbLoading.setVisibility( View.GONE );
        tvNoData.setVisibility( View.VISIBLE );
    }
}
