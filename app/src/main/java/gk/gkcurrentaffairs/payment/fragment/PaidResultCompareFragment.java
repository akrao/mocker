package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;

/**
 * Created by Amit on 5/17/2018.
 */

public class PaidResultCompareFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private Activity activity;
    private View view , vNoData , tvNoData , pbNoData ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_paid_home, container, false);
        activity = getActivity();
        return view;
    }

    @Override
    public void onRefreshFragment() {
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
    }

    private boolean isLoaded = false ;
    private void init(){
        initViews();
        initDataFromArgs();
    }

    private PaidResult paidResult ;
    private void initDataFromArgs(){
        if ( getArguments() != null && getArguments().getSerializable(AppConstant.DATA) != null
                && getArguments().getSerializable(AppConstant.DATA) instanceof PaidResult ) {
            paidResult = (PaidResult) getArguments().getSerializable(AppConstant.DATA);
            if ( paidResult != null & vNoData != null ){
                vNoData.setVisibility( View.GONE );
                setupViewPager();
            }
        }
    }

    private void initViews(){
        if ( view != null ) {
            vNoData = view.findViewById( R.id.ll_no_data );
            tvNoData = view.findViewById( R.id.tv_no_data );
            pbNoData = view.findViewById( R.id.player_progressbar );
        }else if ( activity != null ){
            activity.finish();
        }
    }

    ViewPagerAdapter adapter;
    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode( TabLayout.MODE_SCROLLABLE );
    }

    private void setupViewPager( ViewPager viewPager ){
        adapter = new ViewPagerAdapter(getChildFragmentManager() , TYPE.length);
        Bundle bundle ;
        for ( int i =0 ; i < TYPE.length ; i++ ) {
            Fragment fragment = new PaidResultCompareSubFragment();
            bundle = new Bundle();
            bundle.putSerializable(AppConstant.DATA , paidResult );
            bundle.putString(AppConstant.TYPE , TYPE[i] );
            bundle.putString(AppConstant.TITLE , TYPE_TITLE[i] );
            if ( TYPE[i].equalsIgnoreCase(TYPE[0]) ) {
                bundle.putBoolean(AppConstant.CATEGORY , true );
            }
            fragment.setArguments(bundle);
            adapter.addFrag( fragment , TYPE[i] );
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(TYPE.length);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof BaseFragment) {
            ((BaseFragment) fragment).onRefreshFragment();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    private final String[] TYPE = {"score","accuracy","correct","attempt","incorrect","time"};
    private final String[] TYPE_TITLE = {"Score","Accuracy","Correct Answer","Attempted Question","Incorrect Answer","Time Taken"};

}
