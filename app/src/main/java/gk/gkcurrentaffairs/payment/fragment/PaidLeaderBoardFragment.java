package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adssdk.AdsSDK;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.PaidLeaderBoardAdapter;
import gk.gkcurrentaffairs.payment.model.PaidLeaderBoardBean;
import gk.gkcurrentaffairs.payment.model.PaidLeaderBoardUserBean;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/17/2018.
 */

public class PaidLeaderBoardFragment extends BaseFragment {
    private Activity activity;
    private View view, tvNoData, pbLoading, vNoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        initViews();
        initDataFromArgs();
        return view;
    }

    String paperId ;
    private void initDataFromArgs(){
        PaidResult paidResult = (PaidResult) getArguments().getSerializable(AppConstant.DATA);
        if ( paidResult != null ){
            paperId = paidResult.getMockId() + "" ;
        }
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
    }

    PaidLeaderBoardBean paidLeaderBoardBean ;
    private void showData() {
        vNoData.setVisibility(View.GONE);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new PaidLeaderBoardAdapter(getChildWithAds()));
    }

    private List<PaidLeaderBoardUserBean> getChildWithAds(){
        List<PaidLeaderBoardUserBean> beans = getList() ;
        List<PaidLeaderBoardUserBean> children = new ArrayList<>();
        int length = beans.size();
        PaidLeaderBoardUserBean baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            children.add(beans.get(i));
            if ( i == 0 || i == 4 ){
                baseAdModelClass = new PaidLeaderBoardUserBean();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                children.add(baseAdModelClass);
            }
        }
        return children;
    }


    private List<PaidLeaderBoardUserBean> getList1(){
        List<PaidLeaderBoardUserBean> list = new ArrayList<>();
        if ( paidLeaderBoardBean != null ) {
            if ( paidLeaderBoardBean.getTopUsers() != null && paidLeaderBoardBean.getTopUsers().size() > 0 )
                list.addAll(paidLeaderBoardBean.getTopUsers());
            if ( paidLeaderBoardBean.getUsersRanking() != null && paidLeaderBoardBean.getUsersRanking().size() > 0 )
                list.addAll(paidLeaderBoardBean.getUsersRanking());
        }
        return list;
    }

    private List<PaidLeaderBoardUserBean> getList(){
        List<PaidLeaderBoardUserBean> userLeaderBoardBeen = new ArrayList<>();
        int userPlace = 0;
        List<PaidLeaderBoardUserBean> userLeaderBoardBeenList = paidLeaderBoardBean.getTopUsers();
        if (userLeaderBoardBeenList != null && userLeaderBoardBeenList.size() > 0) {
            userLeaderBoardBeen.addAll(userLeaderBoardBeenList);
        }
        for (int i = 0; i < userLeaderBoardBeenList.size(); i++) {
            userLeaderBoardBeen.get(i).setRank(1 + i);
        }

        if ( paidLeaderBoardBean.getUsersRanking() != null && paidLeaderBoardBean.getUsersRanking().size() > 0 ){
            List<PaidLeaderBoardUserBean> list = paidLeaderBoardBean.getUsersRanking();
            userPlace = getUserPosition(list);
            int l = list.size();
            int firstRank = paidLeaderBoardBean.getRank() - userPlace;
            for (int i = 0; i < l; i++) {
                list.get(i).setRank(firstRank + i);
            }
            userLeaderBoardBeenList.addAll(list);
        }
        return userLeaderBoardBeenList ;
    }

    private int getUserPosition(List<PaidLeaderBoardUserBean> userLeaderBoardBeen) {
        int position = 0;
        int l = userLeaderBoardBeen.size();
        for (int i = 0; i < l; i++) {
            if (userLeaderBoardBeen.get(i).getUserId() == Integer.parseInt(AppApplication.getInstance().getLoginSdk().getUserId()))
                position = i;
        }

        return position;
    }


    private void fetchData() {
        if ( AppApplication.getInstance() != null
                && AppApplication.getInstance().getLoginSdk() != null ) {
            Map<String, String> map = new HashMap<>(3);
            map.put("application_id", activity.getPackageName());
            map.put("paper_id", paperId);
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_LEADER_BOARD_DATA , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    paidLeaderBoardBean = ConfigManager.getGson().fromJson(data, PaidLeaderBoardBean.class);
                                    if (paidLeaderBoardBean != null && paidLeaderBoardBean.getTopUsers().size() > 0) {
                                        showData();
                                    } else
                                        showNoData();
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    showNoData();
                                }
                            } else
                                showNoData();
                        }
                    });
        }else {
            showNoData();
        }
    }

    private void showNoData() {
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefreshFragment() {
        if( paidLeaderBoardBean == null && !SupportUtil.isEmptyOrNull(paperId) ){
            fetchData();
        }
    }
}
