package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.SolutionActivity;
import gk.gkcurrentaffairs.payment.adapter.PaidResultListAdapter;
import gk.gkcurrentaffairs.payment.model.PaidMockResult;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/18/2018.
 */

public class SolutionListFragment extends Fragment implements PaidResultListAdapter.OnCustomClick {

    private View view, tvNoData, pbLoading, vNoData;
    private Activity activity;
    private List<PaidResult> lists = new ArrayList<>();
    private DbHelper dbHelper;
    private PaidResultListAdapter resultListAdapter;
    private ProgressDialog pDialog ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_my_activity, container, false);
        activity = getActivity();
        initViews();
        setEmptyList();
        return view;
    }

    private void setEmptyList() {
        resultListAdapter = new PaidResultListAdapter(lists, this , "Solution");
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(resultListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchDataFromDB().execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
        dbHelper = AppApplication.getInstance().getDBObject();
//        apiPayInterface = AppApplication.getInstance().getNetworkObjectPay();
        initDialog();
    }

    private void showData() {
        vNoData.setVisibility(View.GONE);
        resultListAdapter.notifyDataSetChanged();
    }

    private void showNoData() {
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCustomClick(int position , int type) {
        handleMock( position , ( lists.get(position).getStatus() == AppConstant.INT_TRUE ) , type );
    }

    private void handleMock( final int position , final boolean isResult , final int type ){
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                boolean isDownloaded = dbHelper.isPaidMockDownloaded( lists.get(position).getMockId() );
                if ( isDownloaded ){
                    PaidMockTest paidMockTest;
                    PaidResult result;
                    paidMockTest = dbHelper.fetchPaidMock( lists.get(position).getMockId() );
                    result = dbHelper.fetchPaidResult(paidMockTest.getId() , true);
//                    if ( type == AppConstant.INT_FALSE ) {
//                    if ( isGlobal ) {
//                        result = lists.get(position);
//                    } else {
//                        result = dbHelper.fetchPaidResult(paidMockTest.getId() , true);
//                    }
                    new FetchAllResult(result , paidMockTest ).execute();
                }
                else
                    downloadMockTest(position , isResult , type);
                return null;
            }
        });

    }

    private void downloadMockTest(final int position , final boolean isResult , final int type){
        showDialog();
        Map<String, String> map = new HashMap<>(3);
        map.put("application_id" , activity.getPackageName());
        map.put("id" , lists.get(position).getMockId()+"");
        map.put("user_id" , AppApplication.getInstance().getLoginSdk().getUserId());
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.DOWNLOAD_MOCK_TEST, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        hideDialog();
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                PaidMockResult paidMockResult = ConfigManager.getGson().fromJson(data , PaidMockResult.class);
                                if ( paidMockResult != null && paidMockResult.isStatus() ){
                                    new InsertMockInDB(position , isResult , type).execute(paidMockResult.getPaidMockTest());
                                }else
                                    SupportUtil.showToastCentre(activity , "Error in Downloading.");
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                SupportUtil.showToastCentre(activity , "Error in Downloading.");
                            }
                        }else
                            SupportUtil.showToastCentre(activity , "Error in Downloading.");
                    }
                });
    }

    private void openSolution( PaidResult result ){
        Intent intent = new Intent( activity , SolutionActivity.class );
        intent.putExtra( AppConstant.DATA , result );
        activity.startActivity(intent);
    }

    private void initDialog(){
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Fetching Data....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    private void showDialog(){
        try {
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideDialog(){
        try {
            if ( pDialog != null && pDialog.isShowing() )
                pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class InsertMockInDB extends AsyncTask<PaidMockTest , Void , Void> {

        private PaidMockTest paidMockTest;
        int position , type;
        boolean isResult ;
        public InsertMockInDB(int position, final boolean isResult , int type) {
            this.position = position;
            this.type = type;
            this.isResult = isResult;
        }

        @Override
        protected Void doInBackground(final PaidMockTest... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    paidMockTest = params[0];
                    dbHelper.insertPaidMock(paidMockTest);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            PaidResult result = lists.get(position);
            new FetchAllResult(result , paidMockTest ).execute();
        }
    }
    class FetchDataFromDB extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.fetchPaidSolutionList(lists);
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideDialog();
            if (lists != null && lists.size() > 0)
                showData();
            else
                showNoData();
        }
    }

    class FetchAllResult extends AsyncTask<Void, Void, Void> {

        private PaidResult result ;
        private PaidMockTest paidMockTest ;
        public FetchAllResult(PaidResult result , PaidMockTest paidMockTest ) {
            this.result = result;
            this.paidMockTest = paidMockTest;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    result = dbHelper.updatePaidResult(result , paidMockTest );
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideDialog();
            openSolution(result);
        }
    }

}
