package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.PaidResultScrollHandler;
import gk.gkcurrentaffairs.payment.model.PaidSectionResult;
import gk.gkcurrentaffairs.payment.model.SectionResultBean;
import gk.gkcurrentaffairs.util.AppConstant;

public class PaidResultSectionFragment extends BaseFragment {

    private boolean isLoaded = false , isGlobal ;
    private Activity activity;
    private View view;
    private SectionResultBean sectionResultBean ;
    private SectionViewHolder sectionViewHolder;
    private PaidSectionResult result ;
    private int position ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.adapter_paid_result_section, container, false);
        activity = getActivity();
        initDataFromArgs();
        init();
        return view;
    }

    private void init(){
        if ( view != null ) {
            sectionViewHolder = new SectionViewHolder(view);
            if ( result != null ){
                setSectionData(result , sectionResultBean);
            }
        }
    }

    private void initDataFromArgs() {
        if ( getArguments() != null ) {
            result = (PaidSectionResult) getArguments().getSerializable(AppConstant.DATA);
            if ( result != null ){
                result.setPieData(null);
            }
            sectionResultBean = (SectionResultBean) getArguments().getSerializable(AppConstant.CAT_DATA);
            isGlobal = getArguments().getBoolean(AppConstant.TYPE, true);
            position = getArguments().getInt(AppConstant.POSITION , 0);
        }
    }

    private void setSectionData(PaidSectionResult result, SectionResultBean sectionResultBean) {
        sectionViewHolder.tvTime.setText(result.getTime());
        sectionViewHolder.tvScore.setText(result.getScore());
        sectionViewHolder.correctAnsTime.setText(result.getCorrectAnsTime());
        sectionViewHolder.wrongAnsTime.setText(result.getWrongAnsTime());
        sectionViewHolder.tvCorrect.setText(result.getCorrect() + "");
        sectionViewHolder.tvWrong.setText(result.getWrong() + "");
        sectionViewHolder.tvUnAttended.setText(result.getUnattended() + "");
//        sectionViewHolder.pieView.setData(result.getPieHelperArrayList());
//        sectionViewHolder.pieChart.setData(result.getPieData());
//        Description description = new Description();
//        description.setText(result.getTitle() + " Chart");
//        sectionViewHolder.pieChart.setDescription(description);
        sectionViewHolder.vDivider.setVisibility(isGlobal ? View.VISIBLE : View.GONE);
        if (isGlobal) {
            sectionViewHolder.vPercent.setVisibility(View.VISIBLE);
            sectionViewHolder.tvPercentile.setText(result.getPercentile());
            if (sectionResultBean != null) {
                sectionViewHolder.tvRank.setText(sectionResultBean.getSectionRank() + "");
                sectionViewHolder.tvStateRank.setText(sectionResultBean.getSectionStateRank() + "");
            }
        }
    }

    @Override
    public void onRefreshFragment() {
        if ( !isLoaded ){
            isLoaded = true ;
            init();
        }
    }

    class SectionViewHolder implements View.OnClickListener {

        TextView tvTime, tvCorrect, tvWrong, tvUnAttended, tvScore, tvPercentile, tvRank, tvStateRank, correctAnsTime, wrongAnsTime;
        //        PieView pieView;
        View vDivider, vPercent;
//        PieChart pieChart ;

        public SectionViewHolder(View itemView) {
            tvRank = (TextView) itemView.findViewById(R.id.adapter_tv_rank);
            tvStateRank = (TextView) itemView.findViewById(R.id.adapter_tv_state_rank);
            correctAnsTime = (TextView) itemView.findViewById(R.id.tv_correct_ans_time);
            wrongAnsTime = (TextView) itemView.findViewById(R.id.tv_wrong_ans_time);
//            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvCorrect = (TextView) itemView.findViewById(R.id.tv_correct);
            tvWrong = (TextView) itemView.findViewById(R.id.tv_wrong);
            tvUnAttended = (TextView) itemView.findViewById(R.id.tv_unattended);
            tvScore = (TextView) itemView.findViewById(R.id.tv_score);
            tvPercentile = (TextView) itemView.findViewById(R.id.tv_percentile);
//            pieChart = (PieChart) itemView.findViewById(R.id.pc_sec);
//            pieChart.setDrawHoleEnabled(false);
            vDivider = itemView.findViewById(R.id.ll_rank);
            vPercent = itemView.findViewById(R.id.ll_percent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if ( PaidResultScrollHandler.getInstance() != null && PaidResultScrollHandler.getInstance().onCustomClick != null ) {
                PaidResultScrollHandler.getInstance().onCustomClick.onCustomClick(position);
            }
        }
    }
}
