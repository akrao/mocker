package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.adapter.PackageAdapter;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.payment.model.PackageList;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/11/2017.
 */

public class MyPackageFragment extends Fragment {

    private View view , tvNoData , pbLoading , vNoData ;
    private Activity activity;
    private PackageList packageList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        initViews();
        fetchData();
        return view;
    }

    private void initViews(){
        vNoData = view.findViewById( R.id.ll_no_data );
        tvNoData = view.findViewById( R.id.tv_no_data );
        pbLoading = view.findViewById( R.id.player_progressbar );
    }

    private void showData(){
        vNoData.setVisibility( View.GONE );
        RecyclerView recyclerView = (RecyclerView) view.findViewById( R.id.itemsRecyclerView );
        recyclerView.setLayoutManager( new LinearLayoutManager( activity ) );
        recyclerView.setAdapter( new PackageAdapter( activity , packageList.getPackages() , packageList.getImagePath() , false , PaidHomeActivity.getInstance().isSubscribed()));
    }

    private void fetchData(){
        Map<String, String> map = new HashMap<>(1);
        map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.GET_MY_PACKAGES , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                PackageList packageList1 = ConfigManager.getGson().fromJson(data , PackageList.class);
                                if ( packageList1 != null
                                        && packageList1.getPackages() != null
                                        && packageList1.getPackages().size() > 0){
                                    packageList = packageList1;
                                    showData();
                                }else
                                    showNoData();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        }else
                            showNoData();
                    }
                });

/*
        if ( !SupportUtil.isNotConnected( activity ) ) {
            apiPayInterface.getMyPackages(SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO)).enqueue(new Callback<PackageList>() {
                @Override
                public void onResponse(Call<PackageList> call, Response<PackageList> response) {
                    if ( response != null && response.body() != null
                            && response.body().getPackages() != null
                            && response.body().getPackages().size() > 0){
                        packageList = response.body();
                        showData();
                    }else
                        showNoData();
                }

                @Override
                public void onFailure(Call<PackageList> call, Throwable t) {
                    showNoData();
                }
            });
        } else {
            showNoData();
        }
*/
    }

    private void showNoData(){
        pbLoading.setVisibility( View.GONE );
        tvNoData.setVisibility( View.VISIBLE );
    }

}
