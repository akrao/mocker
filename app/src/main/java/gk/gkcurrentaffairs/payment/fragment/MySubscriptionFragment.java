package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/11/2017.
 */

public class MySubscriptionFragment extends Fragment {

    private View view ;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_my_subscription, container, false);
        activity = getActivity();
        setupViewPager();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
    }


    private void setupViewPager() {
        view.findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager( ViewPager viewPager ){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager() , 2);
        Fragment fragment = new MyPackageFragment();
        adapter.addFrag( fragment , "Package" );
        fragment = new MyMockTestFragment();
        adapter.addFrag( fragment , "Mock Test" );
        viewPager.setAdapter(adapter);
    }

}
