package gk.gkcurrentaffairs.payment.fragment;

import androidx.fragment.app.Fragment;

/**
 * Created by Amit on 5/17/2018.
 */

public abstract class BaseFragment extends Fragment {

    abstract public void onRefreshFragment();
}
