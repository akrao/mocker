package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 6/7/2018.
 */

public class MyActivityFragment extends BaseFragment implements ViewPager.OnPageChangeListener{

    private View view ;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_my_subscription, container, false);
        activity = getActivity();
        return view;
    }

    private boolean isLoaded = false ;
    @Override
    public void onRefreshFragment() {
        SupportUtil.handleLoginView(view);
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
    }

    private void init(){
        if ( view != null ) {
            setupViewPager();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = adapter.getItem(position);
        if (fragment != null && fragment instanceof BaseFragment) {
            ((BaseFragment) fragment).onRefreshFragment();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
    }


    private void setupViewPager() {
        if ( view.findViewById( R.id.ll_no_data ) != null
                && view.findViewById( R.id.viewpager ) !=null
                && view.findViewById( R.id.tabs ) !=null ) {
            view.findViewById( R.id.ll_no_data ).setVisibility(View.GONE);
            final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
            setupViewPager(viewPager);

            TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.addOnPageChangeListener(this);
        }
    }

    ViewPagerAdapter adapter ;
    private void setupViewPager( ViewPager viewPager ){
        adapter = new ViewPagerAdapter(getChildFragmentManager() , 3);
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstant.TYPE , true);
        bundle.putBoolean(AppConstant.SHOULD_LOAD , true);
/*
        Fragment fragment = new SolutionListFragment();
        adapter.addFrag( fragment , "Solution" );
*/
        Fragment fragment ;
        fragment = new MyMockTestFragment();
        adapter.addFrag( fragment , "Global" );
        fragment = new PaidResultListFragment();
        adapter.addFrag( fragment , "Resume Test" );
        fragment.setArguments(bundle);
        bundle = new Bundle();
        bundle.putBoolean(AppConstant.TYPE , false);
        fragment = new PaidResultListFragment();
        adapter.addFrag( fragment , "Practice Result" );
        fragment.setArguments(bundle);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

}
