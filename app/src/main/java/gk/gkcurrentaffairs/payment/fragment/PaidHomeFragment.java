package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.PaidHomeActivity;
import gk.gkcurrentaffairs.payment.adapter.ViewPagerAdapter;
import gk.gkcurrentaffairs.payment.model.PaidHomeBean;
import gk.gkcurrentaffairs.payment.model.SectionData;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 8/30/2017.
 */

public class PaidHomeFragment extends BaseFragment{

    private View view , vNoData , tvNoData , pbNoData ;
    private Activity activity;
    private PaidHomeBean paidHomeBean ;
    private boolean isFragLoaded = false ;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_paid_home, container, false);
        activity = getActivity();
        initViews();
        init();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            activity.unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void init(){
        if ( SupportUtil.isLoginInit() && AppApplication.getInstance().getConfigManager().isConfigLoaded() ) {
            fetchData();
        }else {
            showNoData();
        }
    }

    private void initViews(){
        vNoData = view.findViewById( R.id.ll_no_data );
        tvNoData = view.findViewById( R.id.tv_no_data );
        pbNoData = view.findViewById( R.id.player_progressbar );
        activity.registerReceiver(broadcastReceiver, new IntentFilter(activity.getPackageName() + ConfigConstant.CONFIG_LOADED));
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            init();
        }
    };


    private void loadData(){
        isFragLoaded = true ;
        if ( isAdded() ) {
            vNoData.setVisibility( View.GONE );
            setupViewPager();
        }
    }

    public void fetchData(){
        if ( activity != null ) {
            Map<String, String> map = new HashMap<>(1);
            map.put("application_id" , activity.getPackageName());
            map.put("user_id" , AppApplication.getInstance().getLoginSdk().getUserId());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_HOME_PAGE_DATA, map, new ConfigManager.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    if (status && !SupportUtil.isEmptyOrNull(data)) {
                        try {
                            PaidHomeBean paidHomeBean1 = ConfigManager.getGson().fromJson(data , PaidHomeBean.class);
                            if ( paidHomeBean1 != null
                                    && paidHomeBean1.getSectionDataList() != null
                                    && paidHomeBean1.getSectionDataList().size() > 0 ){
                                 paidHomeBean = paidHomeBean1;
                                loadData();
                                if (activity instanceof PaidHomeActivity){
                                    ((PaidHomeActivity)activity).setRemainSec( paidHomeBean.getSubscriptionTime() );
                                    ((PaidHomeActivity)activity).setImagePath( paidHomeBean.getImagePath() );
                                }
                            }else
                                showNoData();
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                            showNoData();
                        }
                    }else
                        showNoData();
                }
            });
        }

    }

    private void showNoData(){
        if ( pbNoData != null && tvNoData != null ) {
            pbNoData.setVisibility( View.GONE );
            tvNoData.setVisibility( View.VISIBLE );
        }
    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode( paidHomeBean.getSectionDataList().size() > 3 ? TabLayout.MODE_SCROLLABLE : TabLayout.MODE_FIXED );
    }

    private ViewPagerAdapter adapter ;
    private boolean isAdapterAttached ;
    private void setupViewPager( ViewPager viewPager ){
        if ( !isDetached() ) {
            if ( !isAdapterAttached ) {
                adapter = new ViewPagerAdapter(getChildFragmentManager() , paidHomeBean.getSectionDataList().size());
            } else {
                adapter.refreshDataSet(paidHomeBean.getSectionDataList().size());
            }
            Bundle bundle ;
            for ( SectionData child : paidHomeBean.getSectionDataList() ) {
                PaidHomeListFragment fragment = new PaidHomeListFragment();
                bundle = new Bundle();
                child.setSlidersBeanList( paidHomeBean.getSlidersBeanList() );
                bundle.putSerializable(AppConstant.DATA , child );
                bundle.putSerializable(AppConstant.IMAGE , paidHomeBean.getImagePath() );
                fragment.setArguments(bundle);
                adapter.addFrag( fragment , child.getTitle() );
            }
            if ( !isAdapterAttached ) {
                viewPager.setAdapter(adapter);
                isAdapterAttached = true ;
            } else {
                adapter.notifyDataSetChanged();
            }
//            viewPager.setOffscreenPageLimit(paidHomeBean.getSectionDataList().size());
        }
    }

    @Override
    public void onRefreshFragment() {
        if ( !isFragLoaded ){
            init();
        }
    }
}
