package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adssdk.AdsSDK;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.adapter.PaidMockAdapter;
import gk.gkcurrentaffairs.payment.model.MockInfo;
import gk.gkcurrentaffairs.payment.model.PackageInfo;
import gk.gkcurrentaffairs.payment.model.PaidMockTest;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.DbHelper;
import gk.gkcurrentaffairs.util.Login;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/6/2017.
 */

public class PackageMockFragment extends Fragment implements PaidMockAdapter.OnCustomClick {

    private View view;
    private Activity activity;
    private PackageInfo packageInfo;
    private ProgressDialog pDialog;
    private DbHelper dbHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        initViews();
        return view;
    }

    private void initViews() {
        if (getArguments() != null && getArguments().getSerializable(AppConstant.DATA) != null) {
            packageInfo = (PackageInfo) getArguments().getSerializable(AppConstant.DATA);
            dbHelper = AppApplication.getInstance().getDBObject();
            initObjects();
        } else {
            Toast.makeText(activity, "Error, Please try Again", Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }


    private void setupList() {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new PaidMockAdapter(getChildWithAds(), this));
        view.findViewById(R.id.ll_no_data).setVisibility(View.GONE);
    }

    private List<MockInfo> getChildWithAds(){
        List<MockInfo> children = new ArrayList<>();
        int length = packageInfo.getMockInfoList().size();
        MockInfo baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            if ( ( i == 0 || i == 6 ) && AppConstant.IS_ADS_ENABLED ){
                baseAdModelClass = new MockInfo();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                children.add(baseAdModelClass);
            }
            packageInfo.getMockInfoList().get(i).setPosition(i);
            children.add(packageInfo.getMockInfoList().get(i));
         }
        return children;
    }

    private void initObjects() {
        if (packageInfo != null && packageInfo.getMockInfoList() != null && packageInfo.getMockInfoList().size() > 0) {
            new fetchDataFromDB().execute();
        } else {
            showNoData();
        }
    }

    private void showNoData() {
        view.findViewById(R.id.player_progressbar).setVisibility(View.GONE);
        view.findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
    }

    public void handleFreeMock() {
        if (packageInfo.getMockInfoList().get(0).getPrice() == 0) {
            onCustomClick(0, false);
        } else {
            SupportUtil.showToastCentre(activity, "Sorry, No free test.");
        }
    }

    @Override
    public void onCustomClick(final int position, final boolean isRefresh) {
        Login.handleLogin(activity, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (isRefresh) {
                    refreshMock(position);
                } else {
                    SupportUtil.openMock(packageInfo.getMockInfoList().get(position).getId() , activity , packageInfo.getTitle() , packageInfo.getId());
                }
                return null;
            }
        });
    }

    private void refreshMock(final int position) {
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.deletePaidMock(packageInfo.getMockInfoList().get(position).getId());
                downloadMockTest(position, false);
                return null;
            }
        });
    }

/*
        apiPayInterface.downloadMockTest( packageInfo.getMockInfoList().get(position).getId() , SharedPrefUtil.getString(AppConstant.SharedPref.USER_ID_AUTO) )
                .enqueue(new Callback<PaidMockResult>() {
                    @Override
                    public void onResponse(Call<PaidMockResult> call, Response<PaidMockResult> response) {
                        if ( response != null && response.body() != null && response.body().isStatus() ){
                            new InsertMockInDB().execute(response.body().getPaidMockTest());
                        }else {
                            hideDialog();
                            openDialog(position);
                        }
                    }

                    @Override
                    public void onFailure(Call<PaidMockResult> call, Throwable t) {
                        hideDialog();
                    }
                });
*/

    private void downloadMockTest(final int position, final boolean startMockTest) {
        SupportUtil.downloadMockTest(packageInfo.getMockInfoList().get(position).getId(), new SupportUtil.DownloadMCQ() {
            @Override
            public void onResult(boolean result, PaidMockTest paidMockTest) {
            }
        } , activity);
    }

    class fetchDataFromDB extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(final Void... params) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateDownloadedTestList(packageInfo.getMockInfoList());
                    return null;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setupList();
        }
    }

}
