package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.login.LoginSdk;
import com.login.Util;

import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.payment.activity.HelpSupportActivity;
import gk.gkcurrentaffairs.payment.activity.TransactionHistoryActivity;
import gk.gkcurrentaffairs.payment.utils.SharedPrefUtil;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 8/29/2017.
 */

public class UserProfileFragment extends BaseFragment implements View.OnClickListener{

    private View view;
    private Activity activity;
    private AppCompatImageView ivProfilePic;
    private TextView tvUserName, tvPoints;
    private String photoUrl, userName;
    private int points ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_profile, container, false);
        activity = getActivity();
        return view;
    }

    private boolean isLoaded = false ;
    @Override
    public void onRefreshFragment() {
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
    }

    private void init(){
        if ( view != null ) {
            initViews();
            loadData();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        SupportUtil.handleLoginView(view);
    }

    private void initViews(){
        tvUserName = (TextView) view.findViewById(R.id.tvUserName);
        tvPoints = (TextView) view.findViewById(R.id.tvUserPoints);
        ivProfilePic = (AppCompatImageView) view.findViewById(R.id.ivProfilePic);
        view.findViewById( R.id.ll_exams ).setOnClickListener(this);
        view.findViewById( R.id.ll_payment_history ).setOnClickListener(this);
        view.findViewById( R.id.ll_edit_profile ).setOnClickListener(this);
        view.findViewById( R.id.ll_app_settings ).setOnClickListener(this);
        view.findViewById( R.id.ll_share_app ).setOnClickListener(this);
        view.findViewById( R.id.ll_faq ).setOnClickListener(this);
        view.findViewById( R.id.ll_help_support ).setOnClickListener(this);
    }

    private void loadData() {
        if ( AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null ) {
            userName = AppApplication.getInstance().getLoginSdk().getUserName();
            Util.loadUserImage(AppApplication.getInstance().getLoginSdk().getUserImage(), ivProfilePic);
            points = SharedPrefUtil.getInt(AppConstant.SharedPref.USER_POINTS);
            tvUserName.setText(userName);
            tvPoints.setText("Available points : " + points);
        }
/*
        if (!SupportUtil.isEmptyOrNull(photoUrl)) {
            if(photoUrl.startsWith("http://") || photoUrl.startsWith("https://")){}
            else
                photoUrl = SupportUtil.getUserImageUrl() + photoUrl;

            Picasso.with(activity)
                    .load(photoUrl)
                    .resize(125, 125)
                    .centerCrop()
                    .placeholder(R.drawable.profile_black)
                    .transform(new CircleTransform())
                    .into(ivProfilePic);
        } else {
            ivProfilePic.setImageResource(R.drawable.profile_black);
        }
*/
    }

    @Override
    public void onClick(View v) {
        switch ( v.getId() ){
            case R.id.ll_exams :
                SupportUtil.openExamPage(activity);
                break;
            case R.id.ll_payment_history :
                activity.startActivity( new Intent( activity , TransactionHistoryActivity.class));
                break;
            case R.id.ll_edit_profile :
                LoginSdk.getInstance(activity , activity.getPackageName()).openLoginPage(activity , true );
/*
                Intent intent = new Intent( activity , EditProfileActivity.class) ;
                intent.putExtra( AppConstant.TYPE , false );
                activity.startActivity( intent );
*/

                break;
            case R.id.ll_app_settings :
                break;
            case R.id.ll_share_app :
                SupportUtil.share( "" , activity );
                break;
            case R.id.ll_faq :
                activity.startActivity( new Intent( activity , HelpSupportActivity.class) );
                break;
            case R.id.ll_help_support :
                SupportUtil.openLinkInWebView(activity, "https://topcoaching.in/help-support" , "Help And Support");
                break;
        }
    }
}
