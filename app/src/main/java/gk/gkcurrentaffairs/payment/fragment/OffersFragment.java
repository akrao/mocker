package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.adapter.ViewOffersAdapter;
import gk.gkcurrentaffairs.payment.model.SubscriptionOffersBean;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.Login;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 5/8/2018.
 */

public class OffersFragment extends BaseFragment implements ViewOffersAdapter.OnCustomClick{

    private Activity activity;
    private View view, tvNoData, pbLoading, vNoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        getDataFromIntent();
        return view;
    }

    private void getDataFromIntent(){
        if ( getArguments() != null ) {
            boolean isLoad = getArguments().getBoolean( AppConstant.TYPE , false );
            if ( isLoad )
                init();
        }
    }

    private boolean isLoaded = false ;
    @Override
    public void onRefreshFragment() {
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
    }

    private void init(){
        if ( view != null ) {
            initViews();
            fetchData();
        }
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
    }

    List<SubscriptionOffersBean> list ;
    private void showData(List<SubscriptionOffersBean> list) {
        vNoData.setVisibility(View.GONE);
        this.list = list ;
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new ViewOffersAdapter(list, this));
    }

    private void fetchData() {
        Map<String, String> map = new HashMap<>(1);
        map.put("application_id", activity.getPackageName());
        ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                , Constants.GET_SUBSCRIPTION_PLANS , map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !SupportUtil.isEmptyOrNull(data)) {
                            try {
                                List<SubscriptionOffersBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<SubscriptionOffersBean>>() {
                                }.getType());
                                if (list != null && list.size() > 0) {
                                    showData(list);
                                } else
                                    showNoData();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                showNoData();
                            }
                        } else
                            showNoData();
                    }
                });
    }

    private void showNoData() {
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCustomClick(final int position, final boolean isDesc) {
        Login.handleLogin(activity, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                handleClick(position,isDesc);
                return null;
            }
        });
    }

    private void handleClick(int position, boolean isDesc){
        if ( isDesc && list != null && list.size() > position
                && !SupportUtil.isEmptyOrNull(list.get(position).getDiscription())){
            SupportUtil.openSubscriptionPlanDesc(activity , list.get(position).getDiscription() , list.get(position).getId() );
        }else {
/*
            Intent paymentMerchentIntent = new Intent(activity, ConfirmationActivity.class);
            paymentMerchentIntent.putExtra(AppConstant.DATA, list.get(position));
            paymentMerchentIntent.putExtra(AppConstant.TYPE, AppConstant.SERVICE_SUBSCRIBE );
            activity.startActivity(paymentMerchentIntent);
*/
        }

    }
}
