package gk.gkcurrentaffairs.payment.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adssdk.AdsSDK;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gk.gkcurrentaffairs.AppApplication;
import gk.gkcurrentaffairs.R;
import gk.gkcurrentaffairs.config.ConfigConstant;
import gk.gkcurrentaffairs.config.ConfigManager;
import gk.gkcurrentaffairs.payment.Constants;
import gk.gkcurrentaffairs.payment.activity.PaidResultActivity;
import gk.gkcurrentaffairs.payment.adapter.MyMockTestAdapter;
import gk.gkcurrentaffairs.payment.model.PaidResult;
import gk.gkcurrentaffairs.payment.model.ServerMockTestList;
import gk.gkcurrentaffairs.util.AppConstant;
import gk.gkcurrentaffairs.util.SupportUtil;

/**
 * Created by Amit on 9/11/2017.
 */

public class MyMockTestFragment extends BaseFragment implements MyMockTestAdapter.OnCustomClick {

    private View view, pbLoading, vNoData;
    private TextView tvNoData;
    private Activity activity;
    private List<ServerMockTestList> lists;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_list_no_data, container, false);
        activity = getActivity();
        initViews();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }

    private void initViews() {
        vNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = (TextView) view.findViewById(R.id.tv_no_data);
        pbLoading = view.findViewById(R.id.player_progressbar);
    }

    private void showData() {
        vNoData.setVisibility(View.GONE);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new MyMockTestAdapter(getChildWithAds(), this));
    }

    private List<ServerMockTestList> listWithAds ;
    private List<ServerMockTestList> getChildWithAds(){
        listWithAds = new ArrayList<>();
        int length = lists.size();
        ServerMockTestList baseAdModelClass ;
        for ( int i = 0 ; i < length ; i++ ){
            listWithAds.add(lists.get(i));
            if ( i == 0 || i == 4 ){
                baseAdModelClass = new ServerMockTestList();
                baseAdModelClass.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                listWithAds.add(baseAdModelClass);
            }
        }
        return listWithAds ;
    }

    private void fetchData() {
        if (AppApplication.getInstance() != null && AppApplication.getInstance().getLoginSdk() != null) {
            Map<String, String> map = new HashMap<>(2);
            map.put("user_id", AppApplication.getInstance().getLoginSdk().getUserId());
            map.put("application_id", activity.getPackageName());
            ConfigManager.getInstance().getData(ConfigConstant.CALL_TYPE_GET, ConfigConstant.HOST_PAID
                    , Constants.GET_MY_MOCK_TEST, map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !SupportUtil.isEmptyOrNull(data)) {
                                try {
                                    List<ServerMockTestList> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ServerMockTestList>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        lists = list;
                                        showData();
                                    } else
                                        showNoData();
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    showNoData();
                                }
                            } else
                                showNoData();
                        }
                    });
        }else
            showNoData();


    }

    private void showNoData() {
        pbLoading.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText("No Data");
    }

    @Override
    public void onCustomClick(int position, int type) {
        if (type == MyMockTestAdapter.OnCustomClick.CLICK_TYPE_GLOBAL_RESULT) {
            handleResult(position);
        } else {
            SupportUtil.openMock(listWithAds.get(position).getId(), activity
                    , listWithAds.get(position).getPackageTitle(), listWithAds.get(position).getPackageId());
        }
    }

    private void handleResult(final int position) {
        if ( position < listWithAds.size() ) {
            Intent intent = new Intent(activity, PaidResultActivity.class);
            PaidResult paidResult = new PaidResult();
            paidResult.setMockId(listWithAds.get(position).getId());
            intent.putExtra(AppConstant.DATA, paidResult);
            intent.putExtra(AppConstant.TYPE, true);
//        intent.putExtra( AppConstant.TYPE , type == AppConstant.INT_FALSE ? false : AppConstant.INT_TRUE );
            activity.startActivity(intent);
        }
    }

    @Override
    public void onRefreshFragment() {
/*
        if ( !isLoaded ){
            init();
            isLoaded = true ;
        }
*/
    }

    private void init() {
        initViews();
        fetchData();
    }
}
