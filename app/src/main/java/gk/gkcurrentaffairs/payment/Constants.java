package gk.gkcurrentaffairs.payment;

/**
 * Created by Amit on 5/8/2018.
 */

public interface Constants {
    String GET_SUB_CAT_TREE = "get-subcategories-tree";
    String GET_HOME_PAGE_DATA = "get-home-page-data";
    String GET_MY_PACKAGES = "get-my-packages";
    String GET_MY_MOCK_TEST= "get-my-mocktest" ;
    String GET_PACKAGE_DETAILS = "get-package-details";
    String DOWNLOAD_MOCK_TEST = "download-mock-test";
    String GET_SUBSCRIPTION_PLANS = "get-subscription-plans" ;
    String SAVE_USER_TEST_RESULT = "save-user-test-result" ;
    String SAVE_USER_TEST_RESULT_NEW = "save-user-test-result-new" ;
    String GET_RESULT_BY_USER_AND_PAPER_ID = "get-result-by-user-and-paper-id" ;
    String GET_EXAM_DATA_BY_ID = "get-exam-data-by-id" ;
    String GET_PACKAGES_BY_CAT_ID= "get-packages-by-category-id" ;
    String GET_LEADER_BOARD_DATA= "get-leaderboard-data" ;
    String GET_LEADER_BOARD_COMPARE= "get-leaderboard-compare" ;
    String GET_USER_PAID_CAT= "get-user-paid-categories" ;
    String INIT_PAYMENT = "init-payment" ;
    String PAY_TM_CALLBACK = "paytm-callback" ;
    String GET_USER_ORDER_HISTORY = "get-user-order-history";

    int SLIDER_PACKAGE = 1 ;
    int SLIDER_SUBSCRIPTION_PLAN_ALL = 2 ;
    int SLIDER_SUBSCRIPTION_PLAN_SINGLE = 3 ;
    int SLIDER_EXAM_PAGE = 4 ;
    int SLIDER_PLAY_STORE = 5 ;
    int SLIDER_WEB_URL = 6 ;
    int CATEGORY_TYPE_MOCK = 7;
    int CATEGORY_TYPE_ARTICLE = 8;
    int CATEGORY_TYPE_MOCK_ARTICLE = 9;
    int CATEGORY_TYPE_PDF = 10;
    int CATEGORY_TYPE_MOCK_ARTICLE_PDF = 11;
    int CATEGORY_TYPE_CALENDER = 12 ;
    int CATEGORY_TYPE_NCERT_CLASS = 13 ;
    int CATEGORY_TYPE_NEWS = 14 ;
    int CATEGORY_TYPE_CATEGORY = 15 ;
    int CATEGORY_TYPE_MOCK_ARTICLE_ONLY = 16;

}
